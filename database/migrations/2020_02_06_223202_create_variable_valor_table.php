<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVariableValorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variable_valor', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('id_referencia')->unsigned();
            $table->integer('indice')->unsigned();

            $table->integer('id_variable')->unsigned();
            $table->foreign('id_variable')->references('id')->on('variables');

            $table->string('valor', 191)->nullable();
            $table->text('descripcion');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variable_valor');
    }
}
