<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClimaJobPositionsPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clima_job_positions_pivot', function (Blueprint $table) {
            $table->integer('job_position_id')->unsigned();
            $table->foreign('job_position_id')->references('id')->on('job_positions');
            $table->integer('group_job_position_id')->unsigned();
            $table->foreign('group_job_position_id')->references('id')->on('clima_group_job_positions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clima_job_positions_pivot');
    }
}
