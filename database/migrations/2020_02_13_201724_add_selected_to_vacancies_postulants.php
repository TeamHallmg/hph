<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSelectedToVacanciesPostulants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vacancies_postulants', function (Blueprint $table) {
            $table->boolean('selected')->nullable()->after('aceptacion_postulacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vacancies_postulants', function (Blueprint $table) {
            $table->dropColumn('vacancies_postulants');
        });
    }
}
