<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClimaGroupJobPositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clima_group_job_positions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('Nombre del nuevo grupo de puestos que formaremos');
            $table->text('description')->nullable()->comment('Descripción informativa que le queramos dar al grupo');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clima_group_job_positions');
    }
}
