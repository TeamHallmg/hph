<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPuestoForeignToRequisitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('requisitions', 'puesto'))
        {
            Schema::table('requisitions', function (Blueprint $table)
            {
                $table->dropColumn('puesto');
            });
        }
        Schema::table('requisitions', function (Blueprint $table) {
            $table->unsignedInteger('puesto_id')->nullable()->after('current_id');
            $table->foreign('puesto_id')->references('id')->on('job_positions');

            $table->string('puesto_nuevo')->nullable()->after('puesto_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requisitions', function (Blueprint $table) {
            $table->dropForeign(['puesto_id']);
            $table->dropColumn('puesto_id');
            $table->dropColumn('puesto_nuevo');
        });
    }
}
