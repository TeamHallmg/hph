<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incidents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('from_id')->unsigned();
            $table->foreign('from_id')->references('id')->on('users');
            $table->integer('benefit_id')->unsigned();
            $table->foreign('benefit_id')->references('id')->on('benefits');
            $table->integer('event_id')->unsigned()->nullable();
            $table->foreign('event_id')->references('id')->on('events');
            $table->integer('week');
            $table->integer('time');
            $table->integer('amount')->default(0);
            $table->string('info',255);
            $table->string('comment',255);
            $table->enum('status',['pending', 'processing', 'accepted', 'processed', 'rejected', 'complete', 'canceled'])->default('pending');
            $table->boolean('value')->default(0);
            $table->boolean('manager')->default(0);
            $table->boolean('incapacity_id')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidents');
    }
}
