<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementTypePerViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcement_type_per_view', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('announcement_type_id')->unsigned();
            $table->foreign('announcement_type_id')->references('id')->on('announcement_types');
            $table->integer('view_id')->unsigned();
            $table->foreign('view_id')->references('id')->on('views');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcement_type_per_view');
    }
}
