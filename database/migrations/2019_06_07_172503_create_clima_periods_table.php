<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClimaPeriodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clima_periods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('status')->comment('Estatus del periodo [abierto, cerrado, preparatorio] el cual nos permite trabajar con el');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clima_periods');
    }
}
