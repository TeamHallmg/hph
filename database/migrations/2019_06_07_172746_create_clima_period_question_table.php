<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClimaPeriodQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clima_period_question', function (Blueprint $table) {
            $table->integer('period_id')->unsigned();
            $table->foreign('period_id')->references('id')->on('clima_periods');
            $table->integer('question_id')->unsigned();
            $table->foreign('question_id')->references('id')->on('clima_questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clima_period_question');
    }
}
