<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVariablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('etiqueta', 191)->nullable();
            $table->string('nombre', 191)->nullable();
            $table->text('descripcion')->nullable();
            $table->string('tipo', 191)->nullable();
            $table->integer('requerido')->unsigned();
            $table->integer('multiple')->unsigned();
            $table->integer('editable')->unsigned();
            $table->integer('seleccionable')->unsigned();
            $table->integer('estado')->unsigned();
            $table->integer('longitud')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variables');
    }
}
