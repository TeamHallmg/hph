<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeoMunicipiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('municipios');

        Schema::create('geo_municipios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('id_geo_departamento')->unsigned();
            $table->foreign('id_geo_departamento')->references('id')->on('geo_departamentos');
            $table->string('nombre_guatecompras')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('municipios', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->integer('estado_id');
            $table->integer('number');

            $table->timestamps();
        });

        Schema::dropIfExists('geo_municipios');
    }
}
