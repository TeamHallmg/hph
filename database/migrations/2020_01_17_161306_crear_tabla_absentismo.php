<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaAbsentismo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('absentismos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('class', 191)->nullable();
            $table->string('days', 191)->nullable();
            $table->string('description', 191)->nullable();
            $table->string('date', 191)->nullable();
            $table->string('observation', 191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('absentismos');
    }
}
