<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClimaQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clima_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('question');
            $table->tinyInteger('positive')->default(1);
            $table->integer('factor_id')->unsigned()->nullable();
            $table->foreign('factor_id')->references('id')->on('clima_factors');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clima_questions');
    }
}
