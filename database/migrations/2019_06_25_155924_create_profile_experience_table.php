<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileExperienceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_experience', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('profile_id')->unsigned();
            $table->foreign('profile_id')->references('id')->on('profile');

            $table->string('job', 100)->nullable();
            $table->string('company', 255)->nullable();
            $table->date('date_begin_experience')->nullable();
            $table->date('date_end_experience')->nullable();
            $table->float('salary', 10, 2)->nullable();
            $table->text('reason_separation')->nullable();
            $table->string('activity', 100)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_experience');
    }
}
