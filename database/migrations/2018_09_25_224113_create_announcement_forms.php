<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementForms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcement_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('announcement_type_id')->unsigned();
            $table->foreign('announcement_type_id')->references('id')->on('announcement_types');
            $table->integer('announcement_attribute_id')->unsigned();
            $table->foreign('announcement_attribute_id')->references('id')->on('announcement_attributes');
            $table->integer('view_order')->unsigned()->nullable();
            $table->boolean('required')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcement_forms');
    }
}
