<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_application', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('profile_id')->unsigned();
            $table->foreign('profile_id')->references('id')->on('profile');
            
            $table->boolean('family_working')->nullable()->default(false);
            $table->string('family_working_name', 100)->nullable();
            $table->boolean('availability_travel')->nullable()->default(false);
            $table->boolean('availability_shifts')->nullable()->default(false);
            $table->boolean('availability_residence')->nullable()->default(false);
            $table->boolean('unionized')->nullable()->default(false);
            $table->string('unionized_name', 255)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_application');
    }
}
