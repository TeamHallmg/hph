<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobPositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_positions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('Nombre del puesto en la empresa');
            $table->text('description')->nullable()->comment('Descripción que se pueda proporcionar sobre el puesto');
            $table->string('file')->nullable()->comment('Archivo descriptivo del puesto, por lo general la empresa tiene el puesto redactado en un archivo');
            $table->text('experience')->nullable()->comment('Experiencia del puesto');
            $table->text('knowledge')->nullable()->comment('Conocimientos del puesto');
            $table->text('comments')->nullable();

            $table->string('enterprise')->nullable();
            $table->string('benefits')->nullable();

            $table->integer('job_position_level_id')->unsigned()->nullable()->comment('Nivel que se le asigna para escalar el organigrama en la vista');
            $table->foreign('job_position_level_id')->references('id')->on('job_position_levels');

            $table->integer('job_position_boss_id')->unsigned()->nullable()->comment('Puesto jefe del puesto actual');
            $table->foreign('job_position_boss_id')->references('id')->on('job_positions');
            
            $table->integer('area_id')->unsigned()->nullable()->comment('Llave foranea del area al cual pertenece el puesto');
            $table->foreign('area_id')->references('id')->on('areas');

            $table->timestamps();
            $table->softDeletes();

            // $table->string('area');
            // $table->string('departamento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_positions');
    }
}
