<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcement_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('announcement_category_id')->unsigned()->nullable();
            $table->foreign('announcement_category_id')->references('id')->on('announcement_categories');
            /*$table->integer('announcement_type_id')->unsigned()->nullable();
            $table->foreign('announcement_type_id')->references('id')->on('announcement_types');*/
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcement_categories');
    }
}
