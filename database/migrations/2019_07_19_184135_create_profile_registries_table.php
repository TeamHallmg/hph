<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileRegistriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_registries', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('profile_id')->unsigned();
            $table->foreign('profile_id')->references('id')->on('profile');

            $table->string('curp_registry', 45)->nullable();
            $table->string('file_curp_registry', 255)->nullable();
            $table->string('infonavit_registry', 45)->nullable();
            $table->string('file_infonavit_registry', 255)->nullable();
            $table->string('rfc_registry', 45)->nullable();
            $table->string('file_rfc_registry', 255)->nullable();
            $table->string('fonacot_registry', 45)->nullable();
            $table->string('file_fonacot_registry', 255)->nullable();
            $table->string('imss_registry', 45)->nullable();
            $table->string('file_imss_registry', 255)->nullable();
            $table->string('clabe_registry', 45)->nullable();
            $table->string('file_clabe_registry', 255)->nullable();
            $table->string('ine_registry', 45)->nullable();
            $table->date('ine_exp_registry')->nullable();
            $table->string('file_ine_registry', 255)->nullable();
            $table->string('other_registry', 45)->nullable();
            $table->string('file_other_registry', 255)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_registries');
    }
}
