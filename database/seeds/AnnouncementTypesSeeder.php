<?php

use Illuminate\Database\Seeder;

class AnnouncementTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('announcement_types')->insert([
            [
                'name' => 'banner',
                'show_name' => 'Banner',
                'view_file' => 'banner',
                'max_quantity_showed' => 1,
                'max_quantity_stored' => 5
            ],[
                'name' => 'carrousel',
                'show_name' => 'Carrousel',
                'view_file' => 'carrousel',
                'max_quantity_showed' => 5,
                'max_quantity_stored' => 10
            ],[
                'name' => 'mosaico',
                'show_name' => 'Mosaico',
                'view_file' => 'mosaico',
                'max_quantity_showed' => 9,
                'max_quantity_stored' => 20
            ],[
                'name' => 'tabla_anuncios',
                'show_name' => 'Tabla anuncios',
                'view_file' => 'tabla_anuncios',
                'max_quantity_showed' => null,
                'max_quantity_stored' => null,
            ],[
                'name' => 'columna_anuncios',
                'show_name' => 'Columna anuncios',
                'view_file' => 'columna_anuncios',
                'max_quantity_showed' => 5,
                'max_quantity_stored' => 10
            ],[
                'name' => 'cuatro_columnas',
                'show_name' => 'Cuatro columnas',
                'view_file' => 'cuatro_columnas',
                'max_quantity_showed' => 4,
                'max_quantity_stored' => 10,
            ],[
                'name' => 'dynamic_categories',
                'show_name' => 'Por categorias',
                'view_file' => 'dynamic_categories',
                'max_quantity_showed' => null,
                'max_quantity_stored' => null,
            ],[
                'name' => 'gallery',
                'show_name' => 'Galería',
                'view_file' => 'gallery',
                'max_quantity_showed' => null,
                'max_quantity_stored' => null,
            ],[
                'name' => 'carrousel_secondary',
                'show_name' => 'Carrousel Secundario',
                'view_file' => 'carrousel_secondary',
                'max_quantity_showed' => null,
                'max_quantity_stored' => null,
            ],[
                'name' => 'banner_panel',
                'show_name' => 'Banner Panel',
                'view_file' => 'banner_panel',
                'max_quantity_showed' => null,
                'max_quantity_stored' => null
            ],[
                'name' => 'cards',
                'show_name' => 'Tarjetas',
                'view_file' => 'cards',
                'max_quantity_showed' => null,
                'max_quantity_stored' => null
            ],[
                'name' => 'panels',
                'show_name' => 'Paneles ',
                'view_file' => 'panels',
                'max_quantity_showed' => null,
                'max_quantity_stored' => null
            ],
        ]);
    }
}
