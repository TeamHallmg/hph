<?php

use Illuminate\Database\Seeder;

class AbsentismosSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {  
    	DB::table('absentismos')->insert([
        	[
        		'class' => 'De Ley',
				'days' => '2',
				'description' => 'Nacimiento Hijo',
				'date' => '',
				'observation' => '',

        	],[
        		'class' => 'De Ley',
				'days' => '5',
				'description' => 'Matrimonio',
				'date' => '',
				'observation' => '',

        	],[
        		'class' => 'De Ley',
				'days' => '84',
				'description' => 'Maternidad',
				'date' => '',
				'observation' => '',

        	],[
        		'class' => 'De Ley',
				'days' => '3',
				'description' => 'Fallecimiento',
				'date' => '',
				'observation' => '',

        	],[
        		'class' => 'De Ley',
				'days' => 'Indefinido',
				'description' => 'Asistencia al IGSS',
				'date' => '',
				'observation' => 'Colaborador deberá presentar constancia de asistencia',

        	],[
        		'class' => 'Beneficio Laboral',
				'days' => '0.5',
				'description' => 'Cumpleaños',
				'date' => '',
				'observation' => '03 meses para gozar este beneficio',

        	],[
        		'class' => 'Asuetos',
				'days' => '1',
				'description' => 'Año Nuevo',
				'date' => '01 de Enero',
				'observation' => '',

        	],[
        		'class' => 'Asuetos',
				'days' => '1',
				'description' => 'Feria Patronal Peten',
				'date' => '15 de Enero',
				'observation' => 'Solo colaboradores Peten',

        	],[
        		'class' => 'Asuetos',
				'days' => '3',
				'description' => 'Semana Santa',
				'date' => 'Jueves, viernes y sábado (Semana Santa)',
				'observation' => '',

        	],[
        		'class' => 'Asuetos',
				'days' => '1',
				'description' => 'Día del Trabajador',
				'date' => '01 de Mayo',
				'observation' => '',

        	],[
        		'class' => 'Asuetos',
				'days' => '1',
				'description' => 'Día de la Madre',
				'date' => '10 de Mayo',
				'observation' => 'Solo aplica para colaboradoras con hijos.',

        	],[
        		'class' => 'Asuetos',
				'days' => '1',
				'description' => 'Día del Ejército de Guatemala',
				'date' => '30 de Junio',
				'observation' => '',

        	],[
        		'class' => 'Asuetos',
				'days' => '1',
				'description' => 'Día de la Virgen de Asunción',
				'date' => '15 de Agosto',
				'observation' => 'Solo Capital',

        	],[
        		'class' => 'Asuetos',
				'days' => '1',
				'description' => 'Dia de la Independencia',
				'date' => '15 de Septiembre',
				'observation' => '',

        	],[
        		'class' => 'Asuetos',
				'days' => '1',
				'description' => 'Dia de la Revolución',
				'date' => '20 de Octubre',
				'observation' => '',

        	],[
        		'class' => 'Asuetos',
				'days' => '1',
				'description' => 'Dia de Todos los Santos',
				'date' => '01 de Noviembre',
				'observation' => '',

        	],[
        		'class' => 'Asuetos',
				'days' => '0.5',
				'description' => 'Noche Buena',
				'date' => '24 de Diciembre',
				'observation' => 'A partir de las 12:00 horas',

        	],[
        		'class' => 'Asuetos',
				'days' => '1',
				'description' => 'Navidad',
				'date' => '25 de Diciembre',
				'observation' => '',

        	],[
        		'class' => 'Asuetos',
				'days' => '0.5',
				'description' => 'Fin de año',
				'date' => '31 de Diciembre',
				'observation' => 'A partir de las 12:00 horas',

        	]
        ]);
    }
}
