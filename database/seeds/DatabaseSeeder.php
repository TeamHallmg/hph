<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(RegionsSeeder::class);
        // $this->call(AnnouncementRegionsSeeder::class);
        // $this->call(EmployeesTableSeeder::class);
        // $this->call(RequisitionConfigSeeder::class);
        // $this->call(EntidadesTableSeeder::class);
        // $this->call(MunicipiosTableSeeder::class);
        
        $this->call(UsersSeeder::class);
        $this->call(ViewsSeeder::class);
        
        $this->call(AnnouncementCategoriesSeeder::class);
        $this->call(AnnouncementTypesSeeder::class);
        $this->call(AnnouncementAttributesSeeder::class);
        $this->call(AnnouncementFormSeeder::class);
       
        $this->call(GeoDepartamentosTableSeeder::class);
        $this->call(GeoMunicipiosTableSeeder::class);

        $this->call([
            BenefitsSeeder::class,
            RegionsSeeder::class,
            ScheduleSeeder::class,
            WorkshiftFixedTableSeeder::class,
        ]);

        $this->call(RolesSeeder::class);
        $this->call(RoleUserSeeder::class);
        $this->call(PermissionsSeeder::class);
        $this->call(PermissionRoleSeeder::class);

        $this->call(EmployeesFormSeeder::class);
        $this->call(ClimaPermissionsSeeder::class);
    }
}
