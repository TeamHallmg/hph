<?php

use Illuminate\Database\Seeder;

class RequisitionConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() 
    {
        DB::table('requisitions_config')->insert([
        	[
                'user_id' => '1',
	            'orden' => '1',
                'tipo_vacante' => 'CREAR',
	            'rechaza' => '1',
        	],[
                'user_id' => '1',
        		'orden' => '1',
                'tipo_vacante' => 'CUBRIR',
            	'rechaza' => '1',
        	],[
                'user_id' => '1',
        		'orden' => '1',
                'tipo_vacante' => 'INCREMENTAR',
            	'rechaza' => '1',
            ]
    	]);
    }
}
