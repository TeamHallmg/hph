<?php

use Illuminate\Database\Seeder;

class GeoDepartamentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('geo_departamentos')->insert([
			'id' => '1',
			'nombre' =>  'Alta Verapaz'
		]);
        DB::table('geo_departamentos')->insert([
			'id' => '2',
			'nombre' =>  'Baja Verapaz'
		]);
        DB::table('geo_departamentos')->insert([
			'id' => '3',
			'nombre' =>  'Chimaltenango'
		]);
        DB::table('geo_departamentos')->insert([
			'id' => '4',
			'nombre' =>  'Chiquimula'
		]);
        DB::table('geo_departamentos')->insert([
			'id' => '5',
			'nombre' =>  'El Progreso'
		]);
        DB::table('geo_departamentos')->insert([
			'id' => '6',
			'nombre' =>  'Escuintla'
		]);
        DB::table('geo_departamentos')->insert([
			'id' => '7',
			'nombre' =>  'Guatemala'
		]);
        DB::table('geo_departamentos')->insert([
			'id' => '8',
			'nombre' =>  'Huehuetenango'
		]);
        DB::table('geo_departamentos')->insert([
			'id' => '9',
			'nombre' =>  'Izabal'
		]);
        DB::table('geo_departamentos')->insert([
			'id' => '10',
			'nombre' =>  'Jalapa'
		]);
        DB::table('geo_departamentos')->insert([
			'id' => '11',
			'nombre' =>  'Jutiapa'
		]);
        DB::table('geo_departamentos')->insert([
			'id' => '12',
			'nombre' =>  'Petén'
		]);
        DB::table('geo_departamentos')->insert([
			'id' => '13',
			'nombre' =>  'Quetzaltenango'
		]);
        DB::table('geo_departamentos')->insert([
			'id' => '14',
			'nombre' =>  'Quiché'
		]);
        DB::table('geo_departamentos')->insert([
			'id' => '15',
			'nombre' =>  'Retalhuleu'
		]);
        DB::table('geo_departamentos')->insert([
			'id' => '16',
			'nombre' =>  'Sacatepéquez'
		]);
        DB::table('geo_departamentos')->insert([
			'id' => '17',
			'nombre' =>  'San Marcos'
		]);
        DB::table('geo_departamentos')->insert([
			'id' => '18',
			'nombre' =>  'Santa Rosa'
		]);
        DB::table('geo_departamentos')->insert([
			'id' => '19',
			'nombre' =>  'Solola'
		]);
        DB::table('geo_departamentos')->insert([
			'id' => '20',
			'nombre' =>  'Suchitepéquez'
		]);
        DB::table('geo_departamentos')->insert([
			'id' => '21',
			'nombre' =>  'Totonicapán'
		]);
        DB::table('geo_departamentos')->insert([
			'id' => '22',
			'nombre' =>  'Zacapa'
		]);
    }
}
