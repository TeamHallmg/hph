<?php

use Illuminate\Database\Seeder;

class BenefitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $benefits = [
            ['context'=>'super','type'=>'time','code'=>'1004','shortname'=>'TE','name'=>'Tiempo Extra'],
            ['context'=>'super','type'=>'time','code'=>'1004','shortname'=>'R','name'=>'Retardos'],
            ['context'=>'super','type'=>'day','code'=>'1008','shortname'=>'F','name'=>'Faltas injustificadas'],
            ['context'=>'super','type'=>'day','code'=>'1008','shortname'=>'O','name'=>'Omisión'],
            ['context'=>'user','type'=>'pending','code'=>'1007','shortname'=>'V','name'=>'Vacaciones','group'=>'benefit'],
            ['context'=>'user','type'=>'pending','code'=>'1010','shortname'=>'C','name'=>'Comodin'],

            ['context'=>'admin','type'=>'day','code'=>'1009','shortname'=>'FR','name'=>'Festivo región'],
            ['context'=>'user','type'=>'day','code'=>'1005','shortname'=>'P/G','name'=>'Permiso con goce de sueldo'],
            ['context'=>'user','type'=>'day','code'=>'1006','shortname'=>'P/S','name'=>'Permiso sin goce de sueldo'],

            ['context'=>'super','type'=>'time','code'=>'1015','shortname'=>'TTP','name'=>'Sumar tiempo por tiempo'],
            ['context'=>'super','type'=>'time','code'=>'1016','shortname'=>'TTN','name'=>'Restar tiempo por tiempo'],

            ['context'=>'super','type'=>'day','code'=>'1017','shortname'=>'I','name'=>'Incapacidad'],
            ['context'=>'super','type'=>'day','code'=>'1018','shortname'=>'DL','name'=>'Descanso Laborado'],
            ['context'=>'super','type'=>'day','code'=>'1019','shortname'=>'EH','name'=>'Evento Herbalife'],
            ['context'=>'super','type'=>'day','code'=>'1020','shortname'=>'FL','name'=>'Festivo laborado'],
            ['context'=>'super','type'=>'day','code'=>'1021','shortname'=>'D','name'=>'Descanso'],
            ['context'=>'super','type'=>'day','code'=>'1022','shortname'=>'S','name'=>'Suspención'],
        ];

        foreach ($benefits as $benefit) {
			DB::table('benefits')->insert($benefit);
		}
    }
}
