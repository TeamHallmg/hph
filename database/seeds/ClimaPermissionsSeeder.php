<?php

use  App\User;
use  App\Models\Role;
use  App\Models\RoleUser;
use  App\Models\Permission;
use  App\Models\PermissionRole;
use Illuminate\Database\Seeder;

class ClimaPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $permissions_data = [
            ['action' => 'see_progress', 'name' => 'Ver Progresos', 'module' => 'Clima Organizacional'],
            ['action' => 'fill_survey', 'name' => 'Llenar Encuesta', 'module' => 'Clima Organizacional'],
            ['action' => 'see_reports', 'name' => 'Ver Reportes', 'module' => 'Clima Organizacional'],
            ['action' => 'see_factors', 'name' => 'Ver Factores', 'module' => 'Clima Organizacional'],
            ['action' => 'create_factors', 'name' => 'Crear Factores', 'module' => 'Clima Organizacional'],
            ['action' => 'edit_factors', 'name' => 'Editar Factores', 'module' => 'Clima Organizacional'],
            ['action' => 'destroy_factors', 'name' => 'Eliminar Facores', 'module' => 'Clima Organizacional'],
            ['action' => 'see_questions', 'name' => 'Ver Preguntas', 'module' => 'Clima Organizacional'],
            ['action' => 'create_questions', 'name' => 'Crear Preguntas', 'module' => 'Clima Organizacional'],
            ['action' => 'edit_factors', 'name' => 'Editar Preguntas', 'module' => 'Clima Organizacional'],
            ['action' => 'destroy_questions', 'name' => 'Eliminar Preguntas', 'module' => 'Clima Organizacional'],
            ['action' => 'see_periods', 'name' => 'Ver Periodos', 'module' => 'Clima Organizacional'],
            ['action' => 'create_periods', 'name' => 'Crear Periodos', 'module' => 'Clima Organizacional'],
            ['action' => 'edit_periods', 'name' => 'Editar Periodos', 'module' => 'Clima Organizacional'],
            ['action' => 'destroy_periods', 'name' => 'Eliminar Periodos', 'module' => 'Clima Organizacional']
        ];
        $role = [
            'name' => 'clima_admin', 'description' => 'Administrador Clima'
        ];
        $permissions = [];
        foreach($permissions_data as $row) {
            $permission = Permission::updateOrCreate([
                'action' => $row['action']
            ], [
                'name' => $row['name'],
                'module' => $row['module']
            ]);
            $permissions[] = $permission->id;
        }
        $role = Role::updateOrCreate([
            'name' => $role['name']
        ], [
           'description'  => $role['description']
        ]);
        foreach($permissions as $permission_id) {
            PermissionRole::updateOrCreate([
                'permision_id' => $permission_id,
                'role_id' => $role->id
            ]);
        }
        $user = User::where('email', 'soporte@hallmg.com')->first();
        if($user) {
            RoleUser::updateOrCreate([
                'user_id' => $user->id,
                'role_id' => $role->id
            ]);
        }
    }
}
