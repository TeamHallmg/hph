<?php

use Illuminate\Database\Seeder;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permission_role')->insert([
        	[
				'permision_id' => '1',
				'role_id' => '1'
            ], [
				'permision_id' => '2',
				'role_id' => '1'
            ], [
				'permision_id' => '3',
				'role_id' => '1'
            ], [
				'permision_id' => '4',
				'role_id' => '1'
            ], [
            'permision_id' => '5',
            'role_id' => '1'
            ]
    	]);
    }
}
