<?php

use Illuminate\Database\Seeder;

class AnnouncementAttributesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('announcement_attributes')->insert([
        	[
        		'name' => 'Título',
                'form_name' => 'title',
        		'attr_view' => 'text'
        	],[
        		'name' => 'Descripción',
                'form_name' => 'description',
        		'attr_view' => 'textarea'
        	],[
        		'name' => 'Enlace',
                'form_name' => 'link',
        		'attr_view' => 'text'
        	],[
        		'name' => 'Imagen',
                'form_name' => 'image',
        		'attr_view' => 'file'
        	],[
        		'name' => 'Autor',
                'form_name' => 'author',
        		'attr_view' => 'text'
        	],[
        		'name' => 'Etiqueta',
                'form_name' => 'tag',
        		'attr_view' => 'textarea'
        	],[
        		'name' => 'Documento',
                'form_name' => 'document',
        		'attr_view' => 'file'
        	],[
        		'name' => 'Galería',
                'form_name' => 'gallery',
        		'attr_view' => 'multi-file'
			],[
        		'name' => 'Fecha',
                'form_name' => 'date',
        		'attr_view' => 'date'
        	],[
        		'name' => 'Lugar',
                'form_name' => 'place',
        		'attr_view' => 'text'
        	],
        ]);
    }
}

/*,[
	'name' => '',
    'form_name' => '',
	'attr_view' => 
]*/