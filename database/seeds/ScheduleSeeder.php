<?php

use Illuminate\Database\Seeder;
use App\Models\Schedule;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schedules')->insert([
            ['shift' => 'Nocturno', 'in' => '23:00', 'out' => '07:00'],
            ['shift' => 'Matutino', 'in' => '07:00', 'out' => '15:00'],
            ['shift' => 'Vespertino', 'in' => '15:00', 'out' => '23:00'],
            ['shift' => 'Turno Central', 'in' => '09:00', 'out' => '18:00'],
        ]);
    }
}
