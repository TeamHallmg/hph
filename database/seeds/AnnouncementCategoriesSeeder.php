<?php

use Illuminate\Database\Seeder;

class AnnouncementCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('announcement_categories')->insert([
        	[
        		'name' => 'categoria 1',
        		'announcement_category_id' => null
        	],[
        		'name' => 'categoria 2',
        		'announcement_category_id' => null
        	],[
        		'name' => 'categoria 1.1',
        		'announcement_category_id' => 1
        	],[
        		'name' => 'categoria 1.2',
        		'announcement_category_id' => 1
        	],[
        		'name' => 'categoria 1.3',
        		'announcement_category_id' => 1
        	],[
        		'name' => 'categoria 2.1',
        		'announcement_category_id' => 2
        	],[
        		'name' => 'categoria 2.2',
        		'announcement_category_id' => 2
        	],[
        		'name' => 'categoria 2.3',
        		'announcement_category_id' => 2
        	]
        ]);
    }
}
