<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert([
        	[
                'idempleado' => 'AD-00004',
                'nombre' => 'MARCO TULIO',
                'paterno' => 'MARROQUIN',
                'materno' => 'JUAREZ',
                'fuente' => 'FENIX',
                'rfc' => '1506199',
                'curp' => '2598 44683 0101',
                'nss' => '157130667',
                'correoempresa' => '',
                'correopersonal' => '',
                'nacimiento' => '19570804',
                'sexo' => 'M',
                'civil' => 'VIUDO',
                'telefono' => '49736873',
                'extension' => null,
                'celular' => '49736873',
                'ingreso' => '20130403',
                'fechapuesto' => '20130403',
                'jefe' => '6',
                'direccion' => null,
                'departamento' => 'Desarrollo',
                // 'seccion' => '____',
                'puesto' => 'Desarrollador',
                // 'grado' => '____',
                // 'region' => '____',
                // 'sucursal' => '____',
                // 'idempresa' => '____',
                // 'empresa' => '____',
                // 'division' => '____',
                // 'marca' => '____',
                // 'centro' => '____',
                // 'checador' => '____',
                // 'turno' => '____',
                // 'tiponomina' => '____',
                // 'clavenomina' => '____',
                // 'nombrenomina' => '____',
                // 'generalista' => '____',
                // 'relacion' => '____',
                // 'contrato' => '____',
                // 'horario' => '____',
                // 'jornada' => '____',
                // 'calculo' => '____',
                // 'vacaciones' => '____',
                // 'flotante' => '____',
                // 'base' => '____',
                // 'rol' => '____',
                // 'password' => '____',
                // 'extra1' => '____',
                // 'extra2' => '____',
                // 'extra3' => '____',
                // 'extra4' => '____',
                // 'extra5' => '____',
                // 'fecha' => '____',
                'version' => '2.15'
        	]
    	]);
    }
}
