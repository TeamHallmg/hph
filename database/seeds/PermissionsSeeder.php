<?php

use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
        	[
				'action' => 'see_vacancies',
				'name' => 'Ver Vacantes',
				'module' => 'Vacantes',
				'description' => 'Ver Vacantes'	            
        	] ,[
				'action' => 'see_postulates',
				'name' => 'Ver Postulados',
				'module' => 'Vacantes',
				'description' => 'Ver Postulados'	            
        	] ,[
				'action' => 'generate_vacant',
				'name' => 'Generar Vacante',
				'module' => 'Vacantes',
				'description' => 'Generar Vacante'	            
        	] ,[
				'action' => 'add_remove_recruiter',
				'name' => 'Agregar/Eliminar Reclutador',
				'module' => 'Vacantes',
				'description' => 'Agregar/Eliminar Reclutador'	            
        	] ,[
				'action' => 'vacancies_admin',
				'name' => 'Admin',
				'module' => 'Vacantes',
				'description' => 'Admin Vacantes'	            
        	] 
    	]);
    }
}
