<?php

use Illuminate\Database\Seeder;

class WorkshiftFixedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('workshift_fixed')->insert([
        	[
        		'day' => 'Mon',
        		'labor' => true,
        		'region_id' => 1,
        		'schedule_id' => 1
        	],[
        		'day' => 'Tue',
        		'labor' => true,
        		'region_id' => 1,
        		'schedule_id' => 1
        	],[
        		'day' => 'Wed',
        		'labor' => true,
        		'region_id' => 1,
        		'schedule_id' => 1
        	],[
        		'day' => 'Thu',
        		'labor' => true,
        		'region_id' => 1,
        		'schedule_id' => 1
        	],[
        		'day' => 'Fri',
        		'labor' => true,
        		'region_id' => 1,
        		'schedule_id' => 1
        	],[
        		'day' => 'Sat',
        		'labor' => false,
        		'region_id' => 1,
        		'schedule_id' => 1
        	],[
        		'day' => 'Sun',
        		'labor' => false,
        		'region_id' => 1,
        		'schedule_id' => 1
			]
        ]);
    }
}
