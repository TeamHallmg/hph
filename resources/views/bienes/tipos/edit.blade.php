{{-- @extends('layouts.app')
@section('content') --}}
@extends('bienes.app')
@section('content')

<form action="{{ route('Tipobienes.update', $TipoBienes->id) }}" method="POST" enctype="multipart/form-data">
	@csrf
	@method('PUT')
<div class="container-fluid mt-5">
	
		<div class="d-flex justify-content-center">
			<h3 class="font-weight-bold text-danger">EDITAR TIPO DE BIEN</h3>
		</div>
		<hr style="border: 2px solid;">

		<div class="pull-right">
			<a class="btn btn-small btn-primary" href="{{ url('Tipobienes') }}">Regresar</a>
		</div>
		<br><br>


		<div class="form-row">
			<div class="form-group col-md-3">
				<label for="id_codigo">Código</label>
				<input type="text" class="form-control" name="codigo" id="id_codigo" placeholder="" value="{{ $TipoBienes->codigo }}">
			</div>
			<div class="form-group col-md-3">
				<label for="id_nombre">Nombre</label>
				<input type="text" class="form-control" name="nombre" id="id_nombre" placeholder="" value="{{ $TipoBienes->nombre }}">
			</div>
			<div class="form-group col-md-3">
				<label for="id_descripcion">Descripción</label>
				<input type="text" class="form-control" name="descripcion" id="id_descripcion" value="{{ $TipoBienes->descripcion }}">
			</div>
			<div class="col-md-3">
				<label for="id_estatus">Estado:</label>
				<select name="estatus" id="id_estatus" class="form-control">
					<option disabled value="" >Seleccione una opción...</option>
					<option value="1" selected>Disponible</option>
					<option value="0">No Disponible</option>
				</select>
			</div>
		</div>
		<br>


		<div class="row mt-4">
			<div class="col-12">
				<h4 class="font-weight-bold text-danger">Especificaciones</h4>
				<hr style="border: 1px solid #000000;">
			</div>
		</div>


		<div class="form-row">

			@foreach ($bienesVariables as $variable)

				<div class="form-check col-md-2 form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mt-2 mb-md-0">
					<label class="form-check-label" for="{{ $variable->etiqueta }}">{{ $variable->etiqueta }}</label>
					<input class="form-check-input" type="checkbox" name="variables[{{ $variable->id }}]" id="{{ $variable->etiqueta }}" value="{{ $variable->id }}" {{ count($variable->tipobienesdetalles) > 0 ? 'checked' : '' }} >
				</div>

			@endforeach

		</div>
					
		<br>

		<hr style="border: 2px solid #000000;">
		<div class="form-row">

			<div class="form-group col col-md-2">
			</div>

			<div class="form-group col col-md-2 ">
			</div>

			<div class="form-group col col-md-8 d-flex justify-content-end">

				<a class="btn btn-info mr-3" href="{{ url('Tipobienes') }}">Regresar</a>
				<button type="submit" class="btn btn-success ">Actualizar</button>

			</div>
		</div>
</div>
	</form>
@endsection

@section('scripts')
	<script type="text/javascript">
		$(function() {

			$("#id_estatus option[value='{{ $TipoBienes->estatus }}']").prop('selected', true);

		});
	</script>
@endsection