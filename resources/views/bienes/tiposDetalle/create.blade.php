@extends('bienes.app')
@section('content')

<div class="container-fluid">
	<div class="row mb-5">
		<div class="col-12">
			<h3 class="font-weight-bold text-danger">NUEVO DETALLE PARA UN TIPO DE BIEN</h3>
			<hr style="border: 2px solid #000000;">
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<h4 class="font-weight-bold text-danger">Datos Generales</h4>
			<hr style="border: 1px solid #000000;">
		</div>
		<div class="col-12">
			<form action="{{ url('TipoBienesDetalles') }}" method="POST">
				@csrf
				
				<div class="form-row">
					<div class="form-group col-md-2">
						<label for="id_etiqueta">Etiqueta</label>
						<input type="text" class="form-control" name="etiqueta" id="id_etiqueta" placeholder="" required="">
					</div>
					<div class="form-group col-md-2">
						<label for="id_descripcion">Descripción</label>
						<input type="text" class="form-control" name="descripcion" id="id_descripcion" placeholder="" required="">
					</div>
					<div class="form-group col-md-2">
						<label for="id_longitud">Longitud</label>
						<input type="text" class="form-control" name="longitud" id="id_longitud" placeholder="" required="">
					</div>
					<div class="col-md-2">
						<label for="id_tipo">Tipo:</label>
						<select name="tipo" id="id_tipo" class="form-control" required="">
							<option disabled value="" >Seleccione una opción...</option>
							<option value="text" selected>Texto</option>
							<option disabled value="int">Entero</option>
							<option disabled value="float">Decimal</option>
							<option disabled value="bool">Verdadero/Falso</option>
							<option disabled value="date">Fecha</option>
							<option disabled value="time">Hora</option>
							<option disabled value="select">Lista Seleccionable</option>
							<option disabled value="multiple">Select Multiple</option>
						</select>
					</div>
					<div class="col-md-2">
						<label for="id_estado">Estado:</label>
						<select name="estado" id="id_estado" class="form-control" required="">
							<option disabled value="" >Seleccione una opción...</option>
							<option value="1" selected>Disponible</option>
							<option value="0">No Disponible</option>
						</select>
					</div>
				</div>
				
				<div class="form-row">
					<div class="col-md-2">
						<input type="checkbox" name="requerido" value="0" id="id_requerido">
						<label for="id_requerido">Requerido</label>	
					</div>
					<div class="col-md-2">
						<input type="checkbox" name="multiple" value="0" id="id_multiple">
						<label for="id_multiple">Multiple</label>	
					</div>
					<div class="col-md-2">
						<input type="checkbox" name="editable" value="0" id="id_editable">
						<label for="id_editable">Editable</label>	
					</div>
					<div class="col-md-2">
						<input type="checkbox" name="seleccionable" value="0" id="id_seleccionable">
						<label for="id_seleccionable">Seleccionable</label>	
					</div>
				</div>
				
				<hr style="border: 2px solid #000000;">
				<div class="form-row">
					<div class="form-group col col-md-2">
						<a href="" class="btn btn-primary">Regresar</a>
					</div>
					<div class="form-group col col-md-2">
						<a href="" class="btn btn-danger">Cancelar</a>
					</div>
					<div class="form-group col col-md-8 d-flex justify-content-end">
						<button type="submit" class="btn btn-success">Guardar</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

	{{-- PARA MOSTRAR UNA VENTANA DE ERROR SINO SE ENCUENTRA EL ARCHIVO --}}
	@if (Session::has('flag'))
		<script type="text/javascript">
			$(function() {
				$('#modalError').modal('show');
			});
		</script>
	@endif

	<div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					{!! Session::get('flag') !!}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<style type="text/css">
	.clase_error {
		display: none;
		color: red;
	}
</style>
<script type="text/javascript">
	$(function() {
		$('form').submit(function() {
			$("button[type='submit']").prop('disabled',true);
		});
	});
</script>
@endsection