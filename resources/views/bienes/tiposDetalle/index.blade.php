@extends('bienes.app')
@section('content')

	<div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
	</div>
	<br>
	
	<div class="row">
		<div class="col-md-8">
			<h3 class="font-weight-bold">Tipos de detalles para Bienes</h3>
		</div>
		<div class="col-md-4" style="text-align: right;">
			
			<a class="btn btn-small btn-primary" href="{{ url('TipoBienesDetalles/create') }}">Nuevo</a>

		</div>
	</div>
	<hr style="border: 2px solid #000000;">
	<br><br>
	<table class="table table-striped table-bordered nowrap" id="tableBienes">
		<thead class="bg-red text-white">
			<tr>
				<th class="bg-red">Nombre</th>
				<th class="bg-red">Descripción</th>
				<th class="bg-red">Tipo</th>
				<th class="bg-red">Estatus</th>
				<th class="bg-red">Opciones</th>
				{{-- <th>ruta</th> --}}
			</tr>
		</thead>
		<tbody>
			
			@foreach($Variable as $Detalle)
				<tr>
					<td>{{ $Detalle->etiqueta }}</td>
					<td>{{ $Detalle->descripcion }}</td>
					<td>{{ $Detalle->tipo }}</td>
					<td>
	 					@if($Detalle->estado==1)
	                        Disponible
	                    @else
	                    	No Disponible
	                    @endif
					<td>
						<a class="btn btn-sm btn-success" href="{{ url('TipoBienesDetalles/' . $Detalle->id) }}">Ver</a>
						<a class="btn btn-sm btn-warning" href="{{ url('TipoBienesDetalles/' . $Detalle->id.'/edit') }}">Editar</a>
					</td>
				</tr>

			@endforeach

		</tbody>
	</table>

@endsection

@section('scripts')

<script type="text/javascript">
	$('#tableBienes').DataTable({
		language: {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
   	});
</script>
@endsection