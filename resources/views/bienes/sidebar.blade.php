
<div class="row">
        <div class="col-10">
            <h4 class="m-0 text-danger">
               Menú
            </h4>
        </div>
    </div>
    <hr>
    <ul class="nav flex-column">
        <li class="nav-item">
          <a class="nav-link text-decoration-none text-dark font-weight-bold" href="{{ url('Tipobienes') }}">Tipos de Bienes</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-decoration-none text-dark font-weight-bold" href="{{ url('TipoBienesDetalles') }}">Tipo de Detalles</a>
        </li>
        {{-- @if(Auth::user()->isRecruiter())
            <li class="nav-item">
                <a class="nav-link text-decoration-none text-dark font-weight-bold" href="{{ url('vacantes/reclutador') }}">Reclutador</a>
            </li>
        @endif --}}
      </ul>