@extends('layouts.app')
@section('content')

<div class="container-fluid">
	
	
<div class="row">
 
	<div class="col-md-12 sub_menu">
  
		<form id="savedTemplates" action="/template_saved" method="POST">
			@csrf
      <input type="hidden" name="id" value="{{$record->id}}">
      <textarea class="d-none" name="template" id="template"></textarea>
		<div class="card mt-3">
			<h5 class="card-header bg-info text-white font-weight-bolder">Plantila Dinámica: {{$record->name}} 
        <a href="{{url('/templates')}}" class="btn btn-sm float-right btn-success">Regresar</a>
              
      </h5>
			<div class="card-body">
				<div id="summernote">{!! isset($record->format)?$record->format->format:null!!}</div>
			</div>
			<div class="card-footer">
 
				<a href="{{url('/templates')}}" class="btn btn-primary mr-3">Regresar</a>
				
				<button type="submit" class="btn btn-success mr-3">Guardar</button>
				
				<a href="{{'/template/preview/'.$record->id}}" target="_blank" class="btn btn-warning mr-3 float-right">Previsualizar</a>

			</div>
			</div>
			
	</form>
			</div>
			</div>




			  

	</div> 
@endsection

@section('scripts')
 <script>
   $(document).ready(function(){
 
   
 $("#savedTemplates").submit(function(e) {
      var self = this;
      e.preventDefault();
  
      var estado = $('#summernote').summernote('code');
      $("#template").html(estado); //populate text area
 
  
      self.submit();
      return false; 
 
 
 });
 });
 </script>
@endsection