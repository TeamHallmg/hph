@extends('layouts.app')
@section('content')

	<div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
	</div>
	<br>
	
	
	<div class="card mt-3">
		<h5 class="card-header bg-info text-white font-weight-bolder">Administrar Plantillas Dinámicas
	<a href="{{url('/home')}}" class="btn btn-sm float-right btn-success">Regresar</a>
		  
  </h5>
		<div class="card-body">

<div class="row justify-content-center mb-3">
    <div class="col-md-4">
        <a  href="{{ url('templates/create') }}" class="btn btn-primary btn-block"> Nueva Plantilla</a>
    </div>
</div>

<div class="table-responsive">
		<div class="col-md-12 tableWrapper">
		<table class="table table-hover table-bordered" id="tableBienes">
			<thead  class="bg-purple text-white">
				<tr>
					<th>#</th>
					<th>Módulo</th>
					<th>Nombre</th>
					<th>Descripción</th>
					<th>Estatus</th>
					<th>Salida</th>
					<th>Formato</th>
					<th>Opciones</th>
					{{-- <th>ruta</th> --}}
				</tr>
			</thead>
		<tbody>
			
			@foreach($records as $record)
				<tr>
					<td>{{ $record->id }}</td>
					<td>{{ $record->modulo->name }}</td>
					<td>{{ $record->name }}</td> 
					<td>{{ $record->description }}</td> 
					<td>
	 					@if($record->status==1)
	                        Disponible
	                    @else
	                    	No Disponible
	                    @endif
					</td>
					<td>{{ $record->out }}</td>
					<td>
	 					@if($record->template_static)
	                        Estático
	                    @else
	                    	Dinámico
	                    @endif
					</td>
					<td nowrap>
						<a class="btn btn-sm btn-warning" href="{{ url('templates/' . $record->id.'/edit') }}" title="Editar datos"> <i class="fa fa-edit"></i> </a>
						<a class="btn btn-sm btn-info" href="{{ url('template/edit/' . $record->id) }}" title="Editar Formato"> <i class="fa fa-file-word"></i></a>
						<a href="{{ url('template/preview/' . $record->id) }}" target="_blank" class="btn btn-sm btn-success" title="Previsualizar Formato"> <i class="fa fa-eye"></i></a>
						<a href="{{ url('template/send_email/' . $record->id) }}" target="_blank" class="btn btn-sm btn-primary" title="Enviar Formato"> <i class="fa fa-envelope"></i></a>
						<a href="{{ url('template/duplicated/' . $record->id) }}" class="btn btn-sm btn-info" title="Duplicar Formato"> <i class="fa fa-clone"></i></a>
 
						<form class="d-inline" action="{{ url('templates/' . $record->id) }}" method="POST">
							@csrf
							<input type="hidden" name="_method" value="DELETE">
							<button type="submit" class="btn btn-sm btn-danger" title="Eliminar"><i class="fas fa-trash-alt fa-lg"></i></button>
						</form>	
					</td>
				</tr>

			@endforeach

		</tbody>
	</table>
		</div>
	</div>
	</div>
	</div>
@endsection

@section('scripts')

<script type="text/javascript">
	$('#tableBienes').DataTable({
		language: {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
   	});
</script>
@endsection