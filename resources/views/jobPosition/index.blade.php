@extends('layouts.app')
@section('content')

<div class="container-fluid">
	<div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{!! Session::get('alert-'.$msg) !!} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
	</div>
	
	<h3>PUESTOS</h3>
	<hr>
	<div class="row">
		<div class="col-md-12 pull-right" style="text-align: right;">
			<a class="btn btn-small btn-info" href="{{ url('jobPosition/create') }}">Nuevo</a>
		</div>
	</div>
	<br>
	<table id="tablejob" class="table table-striped table-bottom-border">
		<thead>
			<tr>
				<td>Folio</td>
				<td>Puesto</td>
				<td>Puesto Jefe</td>
				<td>Descripción</td>
				<td>Beneficios</td>
				<td>Acciones</td>
			</tr>
		</thead>
		<tbody>
			@foreach($jobs as $job)
				<tr class="soft_blue_background">
					<td>{{ $job->id }}</td>
					<td>{{ $job->name }}</td>
					<td>
						@if($job->job_position_boss_id == '')
							Sin Puesto Jefe
						@else
							{{ $job->BossJob->name }}
						@endif

					</td>
					<td>{{ ($job->description?$job->description:'N/A') }}</td>
					<td>{{ $job->benefits }}</td>
					<td>
						<form action="{{ route('jobPosition.destroy', $job->id) }}" method="POST">
							@csrf
							@method('DELETE')
							<button type="submit" class="btn btn-danger"><span class="fa fa-trash-alt"></span></button>
						</form>
						<a class="btn btn-success" href="{{ url('jobPosition/' . $job->id) }}"><span class="fa fa-eye"></span></a>
						<a class="btn btn-info" href="{{ url('jobPosition/' . $job->id . '/edit') }}"><span class="fa fa-pen"></span></a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endsection
	
@section('scripts')
	<script type="text/javascript">
	$(document).ready( function () {
		$('#tablejob').DataTable({
			"language": {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     '<span class="fa fa-arrow-right" aria-hidden="true"></span>',
					"sPrevious": '<span class="fa fa-arrow-left" aria-hidden="true"></span>',
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
		});
	});
	</script>
@endsection