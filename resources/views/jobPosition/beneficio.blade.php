@extends('layouts.app')
@section('content')

	<div class="col-sm-12 col-md-12 col-lg-12">
		<div class="container">
            @if(is_null($beneficio))
                <h4><center>ALGO SALIO MAL, INTENTA MAS TARDE</center></h4>
                <br>
                <h6><center>Su usuario no cuenta con beneficios</center></h6>
                <br>
            @else
                <div class="banner-gen text-center">
                    <img class="pos-img" src="{{ asset("img/beneficios/$beneficio.png") }}">
                </div>
            @endif
		</div>
	</div>

@endsection