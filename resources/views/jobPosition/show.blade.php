@extends('layouts.app')
@section('content')

<div class="container-fluid">
	<div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{!! Session::get('alert-'.$msg) !!} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
	</div>

	<a class="btn btn-info" href="{{ url('jobPosition') }}">Regresar</a>
	<h3 class="strong_blue">CONSULTA PUESTO</h3>
	<hr>

	<div class="row mb-5">
		<div class="col-md-4 offset-md-8">
			<label for="job_position_boss_id">Puesto Jefe:</label>
			
				@if($job->job_position_boss_id == '')

					<input type="text" name="name" id="name" class="form-control" value="Sin Puesto Jefe" readonly>

				@else

					 <input type="text" name="name" id="name" class="form-control" value="{{ $job->BossJob->name }}" readonly>
					
				@endif

		</div>
	</div>


	<div class="row">
		<div class="col-4">
			<label for="name" class="requerido">Nombre:</label>
			<input type="text" name="name" id="name" class="form-control" value="{{ $job->name }}" readonly>
		</div>
		<div class="col-4">
			<label for="file">Archivo:</label>
			@if (!is_null($job->file))
			<div class="row justify-content-center">
				<a class="btn btn-outline-secondary" href="{{ asset('puestos/'. $job->file) }}" target="_blank">
					<i class="fa fa-file-alt"></i> ABRIR/DESCARGAR
				</a>
			</div>
			@else
				<label for="" class="form-control" readonly>Sin archivo a mostrar</label>
			@endif
		</div>
		{{-- <div class="col-4">
			<label for="benefits">Beneficios:</label>
			@if (!is_null($job->benefits))
			<div class="row justify-content-center">
				<a class="btn btn-outline-secondary" href="{{ asset('puestos/'. $job->benefits) }}" target="_blank">
					<i class="fa fa-file-alt"></i> ABRIR/DESCARGAR
				</a>
			</div>
			@else
				<label for="" class="form-control" readonly>Sin archivo a mostrar</label>
			@endif
		</div> --}}
		<div class="col-4">
			<label for="benefits">Beneficios:</label>
			<input type="text" name="benefits" id="benefits" class="form-control" value="{{ $job->benefits }}" readonly>
		</div>
	</div>
	<div class="row mt-3">
		<div class="col-6">
			<label for="knowledge">Conocimientos:</label>
			<textarea name="knowledge" id="knowledge" cols="15" rows="5" class="form-control" style="resize: none;" readonly>{{ $job->knowledge }}</textarea>
		</div>
		<div class="col-6">
			<label for="experience">Funciones:</label>
			<textarea name="experience" id="experience" cols="15" rows="5" class="form-control" style="resize: none;" readonly>{{ $job->experience }}</textarea>
		</div>
	</div>
	<div class="row mt-3">
		<div class="col-6">
			<label for="comments">Objetivos:</label>
			<textarea name="comments" id="comments" cols="15" rows="5" class="form-control" style="resize: none;" readonly>{{ $job->comments }}</textarea>
		</div>
		<div class="col-6">
			<label for="description">Habilidades Requeridas:</label>
			<textarea name="description" id="description" cols="15" rows="5" class="form-control" style="resize: none;" readonly>{{ $job->description }}</textarea>
		</div>
	</div>

	<div class="row mt-4">
		<div class="col-12">
			<h4 class="font-weight-bold text-danger">Tipo de Bienes Disponibles para el puesto</h4>
			<hr style="border: 1px solid #000000;">
		</div>
	</div>

	<div class="row">



		@foreach ($TipoBienes as $TipoBiene)

			<div class="col-md-3 mt-2 ">
				
					<label class="form-check-label" for="{{ $TipoBiene->nombre }}">{{ $TipoBiene->nombre }}</label>

					<input type="text" name="" id="" class="form-control" readonly>


			</div>

			@endforeach

	</div>
	
	<div class="row mt-3">
		<a class="btn btn-info" href="{{ url('jobPosition') }}">Regresar</a>
	</div>

</div>
@endsection