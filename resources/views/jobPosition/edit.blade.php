@extends('layouts.app')
@section('content')

<div class="container-fluid">
	<div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{!! Session::get('alert-'.$msg) !!} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
	</div>

	<a class="btn btn-info" href="{{ url('jobPosition') }}">Regresar</a>
	<h3 class="strong_blue">EDITA PUESTO</h3>
	<hr>

	<form action="{{ route('jobPosition.update', $job->id) }}" method="POST" enctype="multipart/form-data">
		@csrf
		@method('PUT')

		<div class="row mb-5">
			<div class="col-md-4 offset-md-8">
				<label for="job_position_boss_id">Puesto Jefe:</label>
				<select name="job_position_boss_id" id="job_position_boss_id" class="form-control">
					
					<option value="" {{ ($job->job_position_boss_id=='') ? 'selected' : null }}>Sin Puesto Jefe</option>

					@foreach($jobs as $jobP)

					   <option value="{{ $jobP->id }}" {{ ($jobP->id==$job->job_position_boss_id) ? 'selected' : null }}> {{$jobP->name}}</option>

					@endforeach

				</select>
			</div>
		</div>

		<div class="row">
			<div class="col-4">
				<label for="name" class="requerido">Nombre:</label>
				<input type="text" name="name" id="name" class="form-control" value="{{ $job->name }}">
				<span class="text-danger"><strong>{{ $errors->first('name') }}</strong></span>
			</div>
			<div class="col-5">
				<label for="file">Archivo:</label>
				<div class="row">
					<div class="col-3">
						@if (!is_null($job->file))
							<a class="btn btn-outline-secondary" href="{{ asset('puestos/'. $job->file) }}" target="_blank">
								<i class="fa fa-file-alt"></i> VER
							</a>
						@else
							<label for="" class="form-control">...</label>
						@endif
					</div>
					<div class="col-9">
						<input type="file" name="file" id="file">
						<span class="text-danger"><strong>{{ $errors->first('file') }}</strong></span>
					</div>
				</div>
			</div>

			{{-- <div class="col-4">
				<label for="file">Beneficios:</label>
				<div class="row">
					<div class="col-3">
						@if (!is_null($job->benefits))
							<a class="btn btn-outline-secondary" href="{{ asset('puestos/'. $job->benefits) }}" target="_blank">
								<i class="fa fa-file-alt"></i> VER
							</a>
						@else
							<label for="" class="form-control">Sin archivo a mostrar</label>
						@endif
					</div>
					<div class="col-9">
						<input type="file" name="benefits" id="benefits">
						<span class="text-danger"><strong>{{ $errors->first('benefits') }}</strong></span>
					</div>
				</div>
			</div> --}}
			
			<div class="col-3">
				<label for="benefits">Beneficios:</label>
				<select name="benefits" id="benefits" class="form-control">
					<option value="" {{ is_null($job->benefits) ? 'hidden selected disabled' : '' }}>Selecciona un beneficio</option>
					<option value="A" {{ $job->benefits == 'A' ? 'selected' : '' }}>A</option>
					<option value="B" {{ $job->benefits == 'B' ? 'selected' : '' }}>B</option>
					{{-- <option value="C" {{ $job->benefits == 'C' ? 'selected' : '' }}>C</option> --}}
				</select>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-6">
				<label for="knowledge">Conocimientos:</label>
				<textarea name="knowledge" id="knowledge" cols="15" rows="5" class="form-control" style="resize: none;">{{ $job->knowledge }}</textarea>
			</div>
			<div class="col-6">
				<label for="experience">Funciones:</label>
				<textarea name="experience" id="experience" cols="15" rows="5" class="form-control" style="resize: none;">{{ $job->experience }}</textarea>
			</div>
		</div>
		
		<div class="row mt-3">

			<div class="col-6">
				<label for="comments">Objetivos:</label>
				<textarea name="comments" id="comments" cols="15" rows="5" class="form-control" style="resize: none;">{{ $job->comments }}</textarea>
			</div>

			<div class="col-6">
				<label for="description">Habilidades Requeridas:</label>
				<textarea name="description" id="description" cols="15" rows="5" class="form-control" style="resize: none;">{{ $job->description }}</textarea>
			</div>

		</div>

		<div class="row mt-4">
			<div class="col-12">
				<h4 class="font-weight-bold text-danger">Tipo de Bienes Disponibles para el puesto</h4>
				<hr style="border: 1px solid #000000;">
			</div>
		</div>
		<div class="form-row">

			@foreach ($TipoBienes as $TipoBiene)
				
				<div class="form-check col-md-3 form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mt-2 mb-md-0">

					<label class="form-check-label" for="{{ $TipoBiene->nombre }}">{{ $TipoBiene->nombre }}</label>
					<input class="form-check-input" type="checkbox" name="TipoBiene[]" id="{{ $TipoBiene->nombre }}" value="{{ $TipoBiene->id }}"  {{ count($TipoBiene->jobpositionsbienes) > 0 ? 'checked' : '' }} >

				</div>
									
			@endforeach
			
		</div>

		<div class="row mt-3">
			<button type="submit" class="btn btn-success mr-3">Actualizar</button>
			<a class="btn btn-info" href="{{ url('jobPosition') }}">Regresar</a>
		</div>
	</form>

</div>
@endsection