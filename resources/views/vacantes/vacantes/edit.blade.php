@extends('vacantes.app')
@section('content')

<div class="container-fluid">
	<div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
	</div>
	<br>
	
	<div class="d-flex justify-content-center">
		<h3 class="font-weight-bold text-danger">VACANTE A CUBRIR</h3>
	</div>
	<hr style="border: 2px solid #000000;">

	<div class="pull-right">
		<a class="btn btn-small btn-primary" href="{{ url('vacantes') }}">Regresar</a>
	</div>
	<br><br>

	<div class="row">
		<div class="col-md-3">
			<label for="tipo_vacante">Tipo de vacante</label>
			<select name="tipo_vacante" id="tipo_vacante" class="form-control" disabled>
				<option value="SUSTITUIR">Sustituir</option>
				<option value="INCREMENTAR">Incrementar Puesto Existente</option>
				<option value="CREAR">Nuevo Puesto</option>
			</select>
		</div>
	
		<div class="col-md-3">
			<label for="area">Departamento:</label>
			<input type="text" value="{{ $vacante->requisicion->area }}" name="area" class="form-control" readonly />
		</div>

		<div class="col-md-3">
			<label for="puesto">Puesto:</label>
			{{-- <input type="text" value="{{ $vacante->requisicion->puesto->name }}" name="puesto" class="form-control" readonly/> --}}
			@if($vacante->requisicion->tipo_vacante == 'CREAR')
				<input type="text" name="puesto_nuevo" id="puesto_nuevo" class="form-control" maxlength="60" readonly/>
				<input type="text" value="{{ $vacante->requisicion->puesto->name }}" name="puesto" class="form-control" hidden/>
			@else
				<input type="text" name="puesto_nuevo" id="puesto_nuevo" class="form-control" maxlength="60" hidden/>
				<input type="text" value="{{ $vacante->requisicion->puesto->name }}" name="puesto" class="form-control" readonly/>
			@endif
		</div>

	</div>
	<br>

	<div class="row">
		<div class="col-md-12">
			<label for="puesto_descripcion">Descripción del puesto:</label>
			<textarea name="puesto_descripcion" id="" cols="30" rows="5" class="form-control" style="resize:none;" readonly>{{ $vacante->requisicion->puesto_descripcion }}</textarea>
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-md-3">
			<label for="horario">Lunes a Viernes</label><br>
			<input type="radio" name="horario" value="0" disabled/>
		</div>

		<div class="col-md-3">
			<label for="horario">Lunes a Sábado</label><br>
			<input type="radio" name="horario" value="1" disabled/>
		</div>

		<div class="col-md-6">
			<label for="horario_descripcion">Horario descripción:</label>
			<input type="text" value="{{ $vacante->requisicion->horario_descripcion }}" name="horario_descripcion" class="form-control" readonly />
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-md-3">
			<label for="edad_min">Edad mínima (años):</label>
			<input type="number" value="{{ $vacante->requisicion->edad_min }}" name="edad_min" class="form-control" readonly />
		</div>

		<div class="col-md-3">
			<label for="edad_max">Edad máxima (años):</label>
			<input type="number" value="{{ $vacante->requisicion->edad_max }}" name="edad_max" class="form-control" readonly />
		</div>

		<div class="col-md-3">
			<label for="edo_civil">Estado civil:</label>
			<input type="text" name="edo_civil" value="{{ $vacante->requisicion->edo_civil }}" class="form-control" readonly/>
		</div>

		<div class="col-md-3">
			<label for="sexo">Sexo:</label>
			<input type="text" name="sexo" value="{{ $vacante->requisicion->sexo }}" class="form-control" readonly/>
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-md-12">
			<label for="objetivo">Objetivo:</label>
			<textarea name="objetivo" class="form-control" style="resize:none;" cols="30" rows="5" readonly>{{ $vacante->requisicion->objetivo }}</textarea>
		</div>
	</div>
	<br>
{{-- 
	<div class="row">
		<div class="col-md-12">
			<label for="rolesyresponsabilidades">Roles y responsabilidades:</label>
			<textarea name="rolesyresponsabilidades" class="form-control" style="resize:none;" cols="30" rows="5" readonly>{{ $vacante->requisicion->rolesyresponsabilidades }}</textarea>
		</div>
	</div> --}}
	
		<input type="hidden" name="rolesyresponsabilidades" value="">
	<br>

	<div class="row">
		<div class="col-md-2">
			<label for="experiencia">Requiere experiencia:</label>
		</div>

		<div class="col-md-1">
			<label for="experiencia">Si</label><br>
			<input type="radio" name="experiencia" value="1" disabled/>
		</div>

		<div class="col-md-1">
			<label for="experiencia">No</label><br>
			<input type="radio" name="experiencia" value="0" disabled/>
		</div>

		<div class="col-md-2">
			<label for="experiencia_anios">Tiempo de experiencia:</label>
			<input type="text" value="{{ $vacante->requisicion->experiencia_anios }}" name="experiencia_anios" class="form-control" readonly />
		</div>

		<div class="col-md-3">
			<label for="experiencia_grado_escolar">Nivel académico:</label>
			<input type="text" name="experiencia_grado_escolar" value="{{ $vacante->requisicion->experiencia_grado_escolar }}" class="form-control" readonly/>
		</div>

		<div class="col-md-3">
			<label for="experiencia_especializado">Especialidad:</label>
			<input type="text" value="{{ $vacante->requisicion->experiencia_especializado }}" name="experiencia_especializado" class="form-control" readonly />
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-md-12">
			<label for="experiencia_conocimientos">Conocimientos requeridos</label>
			<textarea name="experiencia_conocimientos" class="form-control" style="resize:none;" cols="30" rows="5" readonly>{{ $vacante->requisicion->experiencia_conocimientos }}</textarea>
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-md-12">
			<label for="experiencia_habilidades">Habilidades requeridas:</label>
			<textarea name="experiencia_habilidades" class="form-control" style="resize:none;" cols="30" rows="5" readonly>{{ $vacante->requisicion->experiencia_habilidades }}</textarea>
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-md-3">
			<input type="radio" name="lugar_trabajo" value="0" disabled/>
			<label for="lugar_trabajo">Oficina</label>
		</div>

		<div class="col-md-3">
			<input type="radio" name="lugar_trabajo" value="1" disabled/>
			<label for="lugar_trabajo">Laboratorio</label>
		</div>

		<div class="col-md-3">
			<input type="radio" name="lugar_trabajo" value="2" disabled/>
			<label for="lugar_trabajo">Mostrador (punto de venta)</label>
		</div>

		<div class="col-md-3">
			<input type="radio" name="lugar_trabajo" value="3" disabled/>
			<label for="lugar_trabajo">Otro</label>
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-md-3">
			<label>¿Maneja información confidencial?</label>
		</div>

		<div class="col-md-2">
			<label for="informacion_confidencial">Si</label>
			<input type="radio" name="informacion_confidencial" value="1" disabled/>
		</div>

		<div class="col-md-2">
			<label for="informacion_confidencial">No</label>
			<input type="radio" name="informacion_confidencial" value="0" disabled/>
		</div>

		<div class="col-md-5">
			<label style="background-color: #EEE;">Equipo a manjear (Maquinaria, equipo de oficina, vehículo, etc.)</label>
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-md-3">
			<label>¿Requiere equipo de cómputo?</label>
		</div>
		
		<div class="col-md-2">
			<label for="pc_requiere">Si</label>
			<input type="radio" name="pc_requiere" value="1" disabled/>
		</div>

		<div class="col-md-2">
			<label for="pc_requiere">No</label>
			<input type="radio" name="pc_requiere" value="0" disabled/>
		</div>

		<div class="col-md-5">
			<input type="text" name="pc_descripcion" class="form-control" value="{{ $vacante->requisicion->pc_descripcion }}" readonly>
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-md-3">
			<label>¿Requiere licencia sistema?</label>
		</div>

		<div class="col-md-2">
			<label for="sap_requiere">Si</label>
			<input type="radio" name="sap_requiere" value="1" disabled/>
		</div>

		<div class="col-md-2">
			<label for="sap_requiere">No</label>
			<input type="radio" name="sap_requiere" value="0" disabled/>
		</div>

		<div class="col-md-5">
			<input type="text" name="sap_descripcion" class="form-control" value="{{ $vacante->requisicion->sap_descripcion }}" readonly />
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-md-3">
			<label>¿Requiere viajar (con frecuiencia)?</label>
		</div>
		
		<div class="col-md-2">
			<label for="viajar_requiere">Si</label>
			<input type="radio" name="viajar_requiere" value="1" disabled/>
		</div>

		<div class="col-md-2">
			<label for="viajar_requiere">No</label>
			<input type="radio" name="viajar_requiere" value="0" disabled/>
		</div>

		<div class="col-md-5">
			<input type="text" name="viajar_descripcion" class="form-control" value="{{ $vacante->requisicion->viajar_descripcion }}" readonly />
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-md-3">
			<label>¿Requiere automóvil?</label>
		</div>

		<div class="col-md-2">
			<label for="carro_requiere">Si</label>
			<input type="radio" name="carro_requiere" value="1" disabled/>
		</div>

		<div class="col-md-2">
			<label for="carro_requiere">No</label>
			<input type="radio" name="carro_requiere" value="0" disabled/>
		</div>

		<div class="col-md-5">
			<input type="text" name="carro_descripcion" class="form-control" value="{{ $vacante->requisicion->carro_descripcion }}" readonly />
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-md-3">
			<label>¿Requiere teléfono?</label>
		</div>

		<div class="col-md-2">
			<label for="tel_requiere">Si</label>
			<input type="radio" name="tel_requiere" value="1" disabled/>
		</div>

		<div class="col-md-2">
			<label for="tel_requiere">No</label>
			<input type="radio" name="tel_requiere" value="0" disabled/>
		</div>

		<div class="col-md-5">
			<input type="text" name="tel_descripcion" class="form-control" value="{{ $vacante->requisicion->tel_descripcion }}" readonly />
		</div>
	</div>
	<br>

	{{-- <div class="row">
		<div class="col-md-8">
			<label for="prestaciones">Prestaciones:</label>
			<input type="text" value="{{ $vacante->requisicion->prestaciones }}" name="prestaciones" class="form-control" readonly/>
		</div>
	</div>
	<br> --}}

	<div class="row">
		<div class="col-md-12">
			<label for="comentarios">Comentarios</label>
			<textarea name="comentarios" class="form-control" cols="30" rows="5" style="resize:none;" readonly>{{ $vacante->requisicion->comentarios }}</textarea>
		</div>
	</div>
	<br>
	
	<div class="row">
		<div class="pull-right">
			@if(!$postulation_flag)
				<a class="btn btn-success" href="{{ url('encuesta/'.$vacante->id) }}">Postularme</a>
			@endif
			<a class="btn btn-primary" href="{{ url('vacantes') }}">Regresar</a>
		</div>
	</div>

	{{-- PARA MOSTRAR UNA VENTANA DE ERROR SINO SE ENCUENTRA EL ARCHIVO --}}
	<div class="modal fade" id="modalMotivo" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					No puedes postularte de nuevo a esta vacante, fuiste rechazado en tu anterior postulación
					<label for="motivo">MOTIVO:</label><br>
					{!! Session::get('motivo') !!}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		@if (Session::has('motivo'))
			$(function() {
				$('#modalMotivo').modal('show');
			});
		@endif

		$(function() {
			$("#tipo_vacante option[value='{{ $vacante->requisicion->tipo_vacante }}']").prop('selected', true);
			// $('#confidencial').prop('checked', {{ $vacante->requisicion->confidencial }});
			

			//radio button según el valor que viene de la informacion capturada en la tabla
			$("input[name=horario][value='{{ $vacante->requisicion->horario }}']").prop('checked',true);
			$("input[name=experiencia][value='{{ $vacante->requisicion->experiencia }}']").prop('checked',true);
			$("input[name=lugar_trabajo][value='{{ $vacante->requisicion->lugar_trabajo }}']").prop('checked',true);
			$("input[name=informacion_confidencial][value='{{ $vacante->requisicion->informacion_confidencial }}']").prop('checked',true);
			$("input[name=pc_requiere][value='{{ $vacante->requisicion->pc_requiere }}']").prop('checked',true);
			$("input[name=sap_requiere][value='{{ $vacante->requisicion->sap_requiere }}']").prop('checked',true);
			$("input[name=viajar_requiere][value='{{ $vacante->requisicion->viajar_requiere }}']").prop('checked',true);
			$("input[name=carro_requiere][value='{{ $vacante->requisicion->carro_requiere }}']").prop('checked',true);
			$("input[name=tel_requiere][value='{{ $vacante->requisicion->tel_requiere }}']").prop('checked',true);
			$("input[name=puesto_nuevo]").val('{{ $vacante->requisicion->puesto_nuevo }}');
		});
	</script>
@endsection