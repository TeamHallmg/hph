@extends('vacantes.app')
@section('content')

<div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
	</div>
	<br>

<div class="container-fluid">
	<div class="card">
		<button type="button" class="btn btn-info" data-toggle="collapse" data-target="#requisicion">Mostrar/Ocultar Requisición</button>
		
		<div id="requisicion" class="collapse">
			<div class="d-flex justify-content-center">
				<h3>REQUISICIÓN</h3>
			</div>
			<hr style="border: 1px solid #3E4095;">
			<div class="card-body">
				<div class="row">
					<div class="col-md-3">
						<label for="fecha_requerida">Fecha requerida</label>
						<input type="text" value="{{ $requisition->fecha_requerida }}" name="fecha_requerida" class='form-control' readonly/>
					</div>
				
					<div class="col-md-3">
						<label for="fecha_elaborada">Fecha elaboración</label>
						<input type="text" value="{{ $requisition->fecha_elaborada }}" name="fecha_elaborada" class="form-control" readonly />
					</div>
				
					<div class="col-md-3">
						<label for="estatus_requi">Estatus requisición</label>
						<input type="text" value="{{ $requisition->estatus_requi }}" name="estatus_requi" class="form-control" readonly />
					</div>
					
					<div class="col-md-3">
						<label for="disponibilidad">Disponibilidad:</label>
						<input type="text" name="disponibilidad" id="disponibilidad" value="{{ $requisition->disponibilidad }}" class="form-control" readonly>
					</div>
						
					{{-- <div class="col-md-3">
						<label for="confidencial">Confidencial</label><br>
						<input type="checkbox" name="confidencial" id="confidencial" class="form-control" disabled/>
					</div>	 --}}
				</div>
				<br>
			
				<div class="row">
					<div class="col-md-3">
						<label for="tipo_vacante">Tipo de vacante</label>
						<select name="tipo_vacante" id="tipo_vacante" class="form-control" disabled>
							<option value="SUSTITUIR">Sustituir</option>
							<option value="INCREMENTAR">Incrementar Puesto Existente</option>
							<option value="CREAR">Nuevo Puesto</option>
						</select>
					</div>
				
					<div class="col-md-3">
						<label for="cantidad">Número de plazas a autorizar:</label>
						<input type="number" value="{{ $requisition->cantidad }}" name="cantidad" class="form-control" readonly />
					</div>
				
					<div class="col-md-3">
						<label for="area">Departamento:</label>
						<input type="text" value="{{ $requisition->area }}" name="area" class="form-control" readonly />
					</div>
			
					<div class="col-md-3">
						<label for="puesto">Puesto:</label>
						{{-- <input type="text" value="{{ $requisition->puesto }}" name="puesto" class="form-control" readonly/> --}}
						@if($requisition->tipo_vacante == 'CREAR')
							<input type="text" name="puesto_nuevo" id="puesto_nuevo" class="form-control" maxlength="60" readonly/>
							<input type="text" value="{{ $requisition->puesto->name }}" name="puesto" class="form-control" hidden/>
						@else
							<input type="text" name="puesto_nuevo" id="puesto_nuevo" class="form-control" maxlength="60" hidden/>
							<input type="text" value="{{ $requisition->puesto->name }}" name="puesto" class="form-control" readonly/>
						@endif
					</div>
				</div>
				<br>
			
				<div class="row">
					<div class="col-md-12">
						<label for="puesto_descripcion">Descripción del puesto:</label>
						<textarea name="puesto_descripcion" id="" cols="30" rows="5" class="form-control" style="resize:none;" readonly>{{ $requisition->puesto_descripcion }}</textarea>
					</div>
				</div>
				<br>
			
				<div class="row">
					<div class="col-md-3">
						<label for="horario">Lunes a Viernes</label><br>
						<input type="radio" name="horario" value="0" disabled/>
					</div>
			
					<div class="col-md-3">
						<label for="horario">Lunes a Sábado</label><br>
						<input type="radio" name="horario" value="1" disabled/>
					</div>
			
					<div class="col-md-6">
						<label for="horario_descripcion">Horario descripción:</label>
						<input type="text" value="{{ $requisition->horario_descripcion }}" name="horario_descripcion" class="form-control" readonly />
					</div>
				</div>
				<br>
			
				<div class="row">
					<div class="col-md-3">
						<label for="edad_min">Edad mínima (años):</label>
						<input type="number" value="{{ $requisition->edad_min }}" name="edad_min" class="form-control" readonly />
					</div>
			
					<div class="col-md-3">
						<label for="edad_max">Edad máxima (años):</label>
						<input type="number" value="{{ $requisition->edad_max }}" name="edad_max" class="form-control" readonly />
					</div>
			
					<div class="col-md-3">
						<label for="edo_civil">Estado civil:</label>
						<input type="text" name="edo_civil" value="{{ $requisition->edo_civil }}" class="form-control" readonly/>
					</div>
			
					<div class="col-md-3">
						<label for="sexo">Sexo:</label>
						<input type="text" name="sexo" value="{{ $requisition->sexo }}" class="form-control" readonly/>
					</div>
				</div>
				<br>
			
				<div class="row">
					<div class="col-md-12">
						<label for="objetivo">Objetivo:</label>
						<textarea name="objetivo" class="form-control" style="resize:none;" cols="30" rows="5" readonly>{{ $requisition->objetivo }}</textarea>
					</div>
				</div>
				<br>
			{{-- 
				<div class="row">
					<div class="col-md-12">
						<label for="rolesyresponsabilidades">Roles y responsabilidades:</label>
						<textarea name="rolesyresponsabilidades" class="form-control" style="resize:none;" cols="30" rows="5" readonly>{{ $requisition->rolesyresponsabilidades }}</textarea>
					</div>
				</div> --}}
				
		<input type="hidden" name="rolesyresponsabilidades" value="">
				<br>
			
				<div class="row">
					<div class="col-md-2">
						<label for="experiencia">Requiere experiencia:</label>
					</div>
			
					<div class="col-md-1">
						<label for="experiencia">Si</label><br>
						<input type="radio" name="experiencia" value="1" disabled/>
					</div>
			
					<div class="col-md-1">
						<label for="experiencia">No</label><br>
						<input type="radio" name="experiencia" value="0" disabled/>
					</div>
			
					<div class="col-md-2">
						<label for="experiencia_anios">Tiempo de experiencia:</label>
						<input type="text" value="{{ $requisition->experiencia_anios }}" name="experiencia_anios" class="form-control" readonly />
					</div>
			
					<div class="col-md-3">
						<label for="experiencia_grado_escolar">Nivel académico:</label>
						<input type="text" name="experiencia_grado_escolar" value="{{ $requisition->experiencia_grado_escolar }}" class="form-control" readonly/>
					</div>
			
					<div class="col-md-3">
						<label for="experiencia_especializado">Especialidad:</label>
						<input type="text" value="{{ $requisition->experiencia_especializado }}" name="experiencia_especializado" class="form-control" readonly />
					</div>
				</div>
				<br>
			
				<div class="row">
					<div class="col-md-12">
						<label for="experiencia_conocimientos">Conocimientos requeridos</label>
						<textarea name="experiencia_conocimientos" class="form-control" style="resize:none;" cols="30" rows="5" readonly>{{ $requisition->experiencia_conocimientos }}</textarea>
					</div>
				</div>
				<br>
			
				<div class="row">
					<div class="col-md-12">
						<label for="experiencia_habilidades">Habilidades requeridas:</label>
						<textarea name="experiencia_habilidades" class="form-control" style="resize:none;" cols="30" rows="5" readonly>{{ $requisition->experiencia_habilidades }}</textarea>
					</div>
				</div>
				<br>
			
				<div class="row">
					<div class="col-md-3">
						<input type="radio" name="lugar_trabajo" value="0" disabled/>
						<label for="lugar_trabajo">Oficina</label>
					</div>
			
					<div class="col-md-3">
						<input type="radio" name="lugar_trabajo" value="1" disabled/>
						<label for="lugar_trabajo">Laboratorio</label>
					</div>
			
					<div class="col-md-3">
						<input type="radio" name="lugar_trabajo" value="2" disabled/>
						<label for="lugar_trabajo">Mostrador (punto de venta)</label>
					</div>
			
					<div class="col-md-3">
						<input type="radio" name="lugar_trabajo" value="3" disabled/>
						<label for="lugar_trabajo">Otro</label>
					</div>
				</div>
				<br>
			
				<div class="row">
					<div class="col-md-3">
						<label>¿Maneja información confidencial?</label>
					</div>
			
					<div class="col-md-2">
						<label for="informacion_confidencial">Si</label>
						<input type="radio" name="informacion_confidencial" value="1" disabled/>
					</div>
			
					<div class="col-md-2">
						<label for="informacion_confidencial">No</label>
						<input type="radio" name="informacion_confidencial" value="0" disabled/>
					</div>
			
					<div class="col-md-5">
						<label style="background-color: #EEE;">Equipo a manjear (Maquinaria, equipo de oficina, vehículo, etc.)</label>
					</div>
				</div>
				<br>
			
				<div class="row">
					<div class="col-md-3">
						<label>¿Requiere equipo de cómputo?</label>
					</div>
					
					<div class="col-md-2">
						<label for="pc_requiere">Si</label>
						<input type="radio" name="pc_requiere" value="1" disabled/>
					</div>
			
					<div class="col-md-2">
						<label for="pc_requiere">No</label>
						<input type="radio" name="pc_requiere" value="0" disabled/>
					</div>
			
					<div class="col-md-5">
						<input type="text" name="pc_descripcion" class="form-control" value="{{ $requisition->pc_descripcion }}" readonly>
					</div>
				</div>
				<br>
			
				<div class="row">
					<div class="col-md-3">
						<label>¿Requiere licencia sistema?</label>
					</div>
			
					<div class="col-md-2">
						<label for="sap_requiere">Si</label>
						<input type="radio" name="sap_requiere" value="1" disabled/>
					</div>
			
					<div class="col-md-2">
						<label for="sap_requiere">No</label>
						<input type="radio" name="sap_requiere" value="0" disabled/>
					</div>
			
					<div class="col-md-5">
						<input type="text" name="sap_descripcion" class="form-control" value="{{ $requisition->sap_descripcion }}" readonly />
					</div>
				</div>
				<br>
			
				<div class="row">
					<div class="col-md-3">
						<label>¿Requiere viajar (con frecuiencia)?</label>
					</div>
					
					<div class="col-md-2">
						<label for="viajar_requiere">Si</label>
						<input type="radio" name="viajar_requiere" value="1" disabled/>
					</div>
			
					<div class="col-md-2">
						<label for="viajar_requiere">No</label>
						<input type="radio" name="viajar_requiere" value="0" disabled/>
					</div>
			
					<div class="col-md-5">
						<input type="text" name="viajar_descripcion" class="form-control" value="{{ $requisition->viajar_descripcion }}" readonly />
					</div>
				</div>
				<br>
			
				<div class="row">
					<div class="col-md-3">
						<label>¿Requiere automóvil?</label>
					</div>
			
					<div class="col-md-2">
						<label for="carro_requiere">Si</label>
						<input type="radio" name="carro_requiere" value="1" disabled/>
					</div>
			
					<div class="col-md-2">
						<label for="carro_requiere">No</label>
						<input type="radio" name="carro_requiere" value="0" disabled/>
					</div>
			
					<div class="col-md-5">
						<input type="text" name="carro_descripcion" class="form-control" value="{{ $requisition->carro_descripcion }}" readonly />
					</div>
				</div>
				<br>
			
				<div class="row">
					<div class="col-md-3">
						<label>¿Requiere teléfono?</label>
					</div>
			
					<div class="col-md-2">
						<label for="tel_requiere">Si</label>
						<input type="radio" name="tel_requiere" value="1" disabled/>
					</div>
			
					<div class="col-md-2">
						<label for="tel_requiere">No</label>
						<input type="radio" name="tel_requiere" value="0" disabled/>
					</div>
			
					<div class="col-md-5">
						<input type="text" name="tel_descripcion" class="form-control" value="{{ $requisition->tel_descripcion }}" readonly />
					</div>
				</div>
				<br>
				
				<div class="row">
					<div class="col-md-12">
						<label for="comentarios">Comentarios</label>
						<textarea name="comentarios" class="form-control" cols="30" rows="5" style="resize:none;" readonly>{{ $requisition->comentarios }}</textarea>
					</div>
				</div>
				<br>
			
				<div class="row">
					<div class="col-md-4">
						<label for="sueldo">Salario Base</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<div class="input-group-text">Q</div>
							</div>
							<input type="text" value="{{ $requisition->sueldo }}" name="sueldo" class="form-control" readonly/>
						</div>
					</div>

					<div class="col-md-4">
						<label for="bonif_decret">Bonificación Decreto 78 - 89</label>
						<input type="text" value="{{ $requisition->bonif_decret }}" name="bonif_decret" class="form-control" readonly/>
					</div>
				
					<div class="col-md-4">
						<label for="bonif_produc">Bonificación Productividad</label>
						<input type="text" value="{{ $requisition->bonif_produc }}" name="bonif_produc" class="form-control" readonly/>
					</div>
{{-- 				
					<div class="col-md-4">
						<label for="ispt">ISPT</label>
						<input type="text" value="{{ $requisition->ispt }}" name="ispt" class="form-control" readonly/>
					</div>
				
					<div class="col-md-4">
						<label for="imss">IMSS</label>
						<input type="text" value="{{ $requisition->imss }}" name="imss" class="form-control" readonly/>
					</div>
				</div>
				<br>
			
				<div class="row">
					<div class="col-md-3">
						<label for="sd">SD:</label>
						<input type="text" value="{{ $requisition->sd }}" name="sd" class="form-control" readonly/>
					</div>
				
					<div class="col-md-3">
						<label for="sdi">SDI:</label>
						<input type="text" value="{{ $requisition->sdi }}" name="sdi" class="form-control" readonly/>
					</div>
				
					<div class="col-md-3">
						<label for="sueldo_aut">Sueldo autorizado:</label>
						<input type="text" value="{{ $requisition->sueldo_aut }}" name="sueldo_aut" class="form-control" readonly/>
					</div>
				 --}}
					{{-- <div class="col-md-8">
						<label for="prestaciones">Prestaciones:</label>
						<input type="text" value="{{ $requisition->prestaciones }}" name="prestaciones" class="form-control" readonly/>
					</div> --}}
				</div>
			</div>
		</div>
	</div>
	<br>

	<div class="row" id="rechazo">
		<div class="col-md-12">
			<form action="{{ url('vacantes/addRecruiter') }}" method="post">
				@csrf

				<input type="hidden" name="requisicion_id" value="{{ $requisition->id }}">
				<div class="row">	
					<div class="col-md-6">
						<label for="reclutador_id" class="requerido">Reclutador:</label>
						<select name="reclutador_id" id="reclutador_id" class="form-control" required>
							<option disabled value="" selected hidden>Seleccione una opción...</option>
							@foreach ($reclutadores as $id => $reclutador)
								<option value="{{ $reclutador->id }}"> {{ $reclutador->usuario->FullName }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6">
						{{-- <label for="paquete_id" class="requerido">Encuesta:</label> --}}
						<input type="hidden" name="paquete_id" id="paquete_id" class="form-control" value="1"/>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-12">
						<label for="comentarios">Comentarios:</label>
						<textarea name="comentarios" class="form-control" cols="30" rows="5" style="resize:none;"></textarea>
					</div>
				</div>
				<br>
				<div class="pull-right">
					<button type="submit" class="btn btn-success" id="btn_guardar_reclutador">Guardar</button>
					<a class="btn btn-info" href="{{ url('vacantes') }}">Regresar</a>
				</div>
			</form>
		</div>
	</div>
	
</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$(function() {
			$("#tipo_vacante option[value='{{ $requisition->tipo_vacante }}']").prop('selected', true);
			// $('#confidencial').prop('checked', {{ $requisition->confidencial }});
			

			//radio button según el valor que viene de la informacion capturada en la tabla
			$("input[name=horario][value='{{ $requisition->horario }}']").prop('checked',true);
			$("input[name=experiencia][value='{{ $requisition->experiencia }}']").prop('checked',true);
			$("input[name=lugar_trabajo][value='{{ $requisition->lugar_trabajo }}']").prop('checked',true);
			$("input[name=informacion_confidencial][value='{{ $requisition->informacion_confidencial }}']").prop('checked',true);
			$("input[name=pc_requiere][value='{{ $requisition->pc_requiere }}']").prop('checked',true);
			$("input[name=sap_requiere][value='{{ $requisition->sap_requiere }}']").prop('checked',true);
			$("input[name=viajar_requiere][value='{{ $requisition->viajar_requiere }}']").prop('checked',true);
			$("input[name=carro_requiere][value='{{ $requisition->carro_requiere }}']").prop('checked',true);
			$("input[name=tel_requiere][value='{{ $requisition->tel_requiere }}']").prop('checked',true);
			$("input[name=puesto_nuevo]").val('{{ $requisition->puesto_nuevo }}');
		});
	</script>
@endsection