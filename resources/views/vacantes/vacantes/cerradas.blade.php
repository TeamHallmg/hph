@extends('vacantes.app')
@section('content')
	<div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
	</div>

    <div class="row">
        <div class="col-md-12">
            <h4><strong style="color: #ED3237">VACANTES CERRADAS</strong><hr class="mt-1" style="border-bottom: 4px solid #373435;"></h4>
        </div>
    </div>
    <div class="container-fluid">
        <table class="table table-striped table-bordered nowrap w-100" id="tableCerradas">
            <thead class="bg-red text-white">
                <tr>
                    <th class="bg-red">Vacante</th>
                    <th class="bg-red">Reclutador</th>
                    <th class="bg-red">Requisicion</th>
                    <th class="bg-red">Fecha Cierre</th>
                    <th class="bg-red">Acciones</th>
                </tr>
            </thead>
            <tbody>
				@foreach($closed as $close)
                    <tr>
                        <td>{{ $close->id }}</td>
                        <td>{{ $close->reclutador->usuario->FullName }}</td>
                        <td><a class="btn btn-primary" href="{{ url('requisitions/' . $close->requisicion_id) }}">#{{ $close->requisicion_id }} <i class="far fa-eye"></i></a></td>
						<td>{{ $close->fecha_cierre }} </td>
						<td>
							<a class="btn btn-small btn-info" href="{{ url('postulante_cerrada/' . $close->id ) }}">POSTULANTES</a>
						</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
	
@endsection

@section('scripts')

<script type="text/javascript">
	$(document).ready(function (){
		$('#tableCerradas').DataTable({
			"scrollX": "true",
			"fixedColumns":   {
				"leftColumns": "1",
				"rightColumns": "1",
			},
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
		});
	});
	
</script>
@endsection