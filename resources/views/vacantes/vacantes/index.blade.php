@extends('vacantes.app')
@section('content')
	<div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
	</div>

	@if($mis_postulaciones->count() >= 1)
		<div class="row">
			<div class="col-md-12">
				<h4><strong style="color: #ED3237">MIS POSTULACIONES</strong><hr class="mt-1" style="border-bottom: 4px solid #373435;"></h4>
			</div>
		</div>
		<div class="container-fluid mb-5">
			<table class="table table-striped table-bordered nowrap w-100" id="tablePostulaciones">
				<thead class="bg-red text-white">
					<tr>
						<th  class="bg-red">Folio</th>
						<th  class="bg-red">Puesto</th>
						<th  class="bg-red">Departamento</th>
						<th  class="bg-red">Fecha Postulación</th>
						<th  class="bg-red">Estatus</th>
						<th  class="bg-red">Acciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($mis_postulaciones as $pos => $postulacion)
						<tr>
							<td>{{ $postulacion->vacante->requisicion->id }}</td>
							<td>
								@if($postulacion->vacante->requisicion->tipo_vacante == 'CREAR')
									{{ $postulacion->vacante->requisicion->puesto_nuevo }}
								@else
									{{ $postulacion->vacante->requisicion->puesto->name }}
								@endif
							</td>
							<td>{{ $postulacion->vacante->requisicion->area }}</td>
							<td>{{ date('d/m/Y', strtotime($postulacion->vacante->requisicion->created_at)) }}</td>
							<td> Activa </td> {{-- Hardcodeado --}}
							<td>
								{{-- <a class="btn btn-small btn-success" href="{{ url('vacantes/showExt/' . $vacante->id) }}">Ver</a> --}}
								<a class="btn btn-secondary" href="{{ url('vacantes/' . $postulacion->vacante->id. '/edit') }}"><i class="fas fa-eye"></i></a>
								{{-- <a class="btn btn-danger" href="#">Eliminar</a> --}}
								<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminar-modal{{ $postulacion->id }}">
									<i class="fa fa-trash"> Remover</i>
								</button>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	@endif

	@if($mis_postulaciones->count() <= 3)
		<div class="row">
			<div class="col-md-12">
				<h4><strong style="color: #ED3237">VACANTES DISPONIBLES</strong><hr class="mt-1" style="border-bottom: 4px solid #373435;"></h4>
			</div>
		</div>
		<div class="container-fluid">
			<table class="table table-striped table-bordered nowrap w-100" id="tableVacantes">
				<thead class="bg-red text-white">
					<tr>
						<th class="bg-red">Vacante</th>
						<th class="bg-red">Puesto</th>
						<th class="bg-red">Tipo Vacante</th>
						<th class="bg-red">Horario</th>
						<th class="bg-red">Acciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($vacantes as $pos => $vacante)
						<tr>
							<td>{{ $vacante->id }}</td>
							<td>
								@if($vacante->requisicion->tipo_vacante == 'CREAR')
									{{ $vacante->requisicion->puesto_nuevo }}
								@else
									{{ $vacante->requisicion->puesto->name }}
								@endif
							</td>
							<td>{{ $vacante->requisicion->tipo_vacante }}</td>
							<td>{{ $vacante->requisicion->horario_descripcion }} </td>
							{{--  we will also add show, edit, and delete buttons  --}}
							<td>
								{{-- <a class="btn btn-small btn-success" href="{{ url('vacantes/showExt/' . $vacante->id) }}">Ver</a> --}}
								<a class="btn btn-secondary" href="{{ url('vacantes/' . $vacante->id . '/edit') }}"><i class="fas fa-eye"></i></a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	@else
		<div class="row mt-5">
            <div class="col-12 mb-3 col-md-12">
                <p class="text-center text-white p-3 bg-dark"><strong>Ya tienes 3 postulaciones</strong></p>
            </div>
        </div>
	@endif
	
	{{-- modal para eliminar la postulación un nuevo usuario --}}
	@foreach($mis_postulaciones as $postulado)
		<div class="modal fade" id="eliminar-modal{{ $postulado->id }}" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form action="{{ route('postulante.destroy', $postulado->id) }}" method="POST">
						@csrf
						@method('DELETE')
						<div class="modal-header">
							REMOVER DE LA POSTULACIÓN
						</div>
						<div class="modal-body">
							<label for="motivo">Motivo: </label>
								<input type="hidden" name="id" value="{{ $postulado->id }}">
								<textarea name="motivo" id="motivo" cols="30" rows="5" style="resize:none;"></textarea>
						</div>
						<div class="modal-footer">
								<button type="submit" class="btn btn-danger">Eliminar</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	@endforeach
	
@endsection

@section('scripts')

<script type="text/javascript">

	$(document).ready(function (){
		$('#tableVacantes').DataTable({
			"scrollX": "true",
			"fixedColumns":   {
				"leftColumns": "1",
				"rightColumns": "1",
			},
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
		});

		$('#tablePostulaciones').DataTable({
			"scrollX": "true",
			"fixedColumns":   {
				"leftColumns": "1",
				"rightColumns": "1",
			},
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
		});
	});
	
</script>
@endsection