@extends('vacantes.app')
@section('content')
	<div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
	</div>
	<br>
	
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8">
				<h3 class="font-weight-bold text-danger">RECLUTADORES</h3>
			</div>
			<div class="col-md-4" style="text-align: right;">
				<button type="button" class="btn btn-success" data-toggle="modal" data-target="#crear-modal">
					<i class="fa fa-plus"></i> Agregar
				</button>
			</div>
		</div>
		<hr style="border: 2px solid #000000;">
		<table class="table table-striped table-bordered" id="tableReclutador">
			<thead class="bg-red text-white">
				<tr>
					<th>ID</th>
					<th>Reclutador</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach($reclutadores as $reclutador)
					<tr>
						<td>{{ $reclutador->id }}</td>
						<td>{{ $reclutador->usuario->FullName }}</td>
						<td>
							<button class="btn btn-danger delete_reclutador" id="delete_reclutador" data-id="{{ $reclutador->id }}" data-toggle="modal" data-target="#confirmar-modal">Eliminar</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	{{-- modal para agregar un nuevo reclutador --}}
	<div class="modal fade" id="crear-modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form action="{{ url('reclutador') }}" method="POST">
				@csrf
				<div class="modal-header">
					AGREGAR RECLUTADOR
				</div>
				<div class="modal-body">
						<label for="reclutador" class="requerido">Reclutador:</label>
						<select name="reclutador" id="reclutador" class="form-control" required>
							<option disabled value="" selected hidden>Seleccione una opción...</option>
							@foreach ($usuarios as $id => $usuario)
								<option value="{{ $id }}"> {{ $usuario }}</option>
							@endforeach
						</select>
				</div>
				<div class="modal-footer">
						<button type="submit" class="btn btn-default">Guardar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
				</form>
			</div>
		</div>
	</div>

	{{-- modal para agregar un nuevo reclutador --}}
	<div class="modal fade" id="confirmar-modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form action="{{ route('reclutador.destroy', 0) }}" method="POST">
				@csrf
				@method('DELETE')
				<input type="hidden" name="id_reclutador" id="id_id_reclutador">
				<div class="modal-header">
					ELIMINAR RECLUTADOR
				</div>
				<div class="modal-body">
					<P>
						Para poder Eliminar el reclutar debe indicar a qué reclutador se le asignan las vacantes...
					</P>
					<br>
						<label for="reclutador" class="requerido">Reclutador:</label>
						<select name="reclutador_sucesor" id="reclutador" class="form-control" required>
							<option disabled value="" selected hidden>Seleccione una opción...</option>
							@foreach($reclutadores as $reclutador)
								<option value="{{ $reclutador->id }}"> {{ $reclutador->usuario->FullName  }}</option>
							@endforeach
						</select>
				</div>
				<div class="modal-footer">
						<button type="submit" class="btn btn-danger">Eliminar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
				</form>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
<script type="text/javascript">


	$('.delete_reclutador').click(function(){
		console.log($(this).attr("data-id"));
		var id_reclutador = $(this).attr("data-id");
		$("input[name=id_reclutador]").val(id_reclutador);
	});


	$('#tableReclutador').DataTable({
		language: {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
   	});
</script>
@endsection