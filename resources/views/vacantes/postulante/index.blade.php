@extends('vacantes.app')
@section('content')
	<div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
	</div>
	<br>
	
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9">
				<h3 class="font-weight-bold text-danger">POSTULADOS A LA VACANTE</h3>
				@if($can > 0)
					<h4><center>{{ $postulados[0]->vacante->requisicion->puesto->name }}</center></h4>
					<button class="btn btn-small btn-warning mb-3" data-toggle="modal" data-target="#closeVacancie">Cerrar Vacante</button>
				@endif
			</div>
			<div class="col-md-3 mb-4" style="text-align: center;">
				@if(is_null($is_recruiter))
					<a class="btn btn-small btn-info mr-3" href="{{ url('vacantes/administrar') }}">Regresar</a>
				@else
					<a class="btn btn-small btn-info mr-3" href="{{ url('vacantes/reclutador') }}">Regresar</a>
				@endif
				<a class="btn btn-primary" href="{{ url('profile/0/'.$idVi.'/externo') }}">P. Externo</a>
			</div>
		</div>
		<hr style="border: 2px solid #000000;">
		<table class="table table-striped table-bordered" id="tableReclutador">
			<thead class="bg-red text-white">
				<tr>
					<th>id</th>
					<th>Postulado</th>
					<th>Tipo Postulación</th>
					<th>Reclutador</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach($postulados as $postulado)
					<tr>
						<td>{{ $postulado->id }}</td>
						@if ($postulado->perfil->user_id != 0)
							<td>{{ $postulado->usuario->FullName }}</td>
							@if ($postulado->perfil->external != 0)
								<td>Externo</td>
							@else
								<td>Interno</td>
							@endif
						@else
							<td>{{ $postulado->perfil->FullNamePerfil }}</td>
							<td>Externo</td>
						@endif
						<td>{{ $postulado->vacante->reclutador->usuario->FullName }}</td>
						<td>
							<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminar-modal{{ $postulado->id }}">
								<i class="fa fa-trash"> Remover</i>
							</button>
							@if(is_null($postulado->user_id))
								<a class="btn btn-small btn-primary" href="{{ url('profile/0/'.$postulado->profile_id.'/'.$postulado->vacante_id.'/consulta') }}">CV</a>
							@else
							<a class="btn btn-small btn-primary" href="{{ url('profile/'.$postulado->user_id.'/'.$postulado->profile_id.'/'.$postulado->vacante_id.'/consulta') }}">CV</a>
							@endif
							@if (is_null($postulado->aceptacion_postulacion))
								<a class="btn btn-small btn-success" href="{{ url('postulante/'.$postulado->profile_id.'/edit') }}">Guardar</a>
							@else
								<a class="btn btn-small btn-info" href="{{ url('postulante/'.$postulado->profile_id.'/edit') }}">Liberar</a>
							@endif
							@if($postulado->perfil->external != 1)
								<button type="button" class="btn btn-info text-white" data-toggle="modal" data-target="#cuestionario-modal{{ $postulado->id }}">
									Cuestionario
								</button>
							@endif
							{{-- @if ($postulado->perfil->user_id == 0 && $postulado->perfil->external == 1)
								<a class="btn btn-small btn-warning" href="{{ url('profile/'.$postulado->profile_id.'/'.$postulado->vacante_id.'/generar') }}">Generar Acceso</a>
							@endif --}}
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	@php
		$vacante->requisicion->cantidad = ($vacante->requisicion->cantidad == null?1:$vacante->requisicion->cantidad);
	@endphp

	<div class="modal fade" id="closeVacancie" tabindex="-1" role="dialog" aria-labelledby="closeVacancie">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5>
						CERRAR VACANTE ({!! $guardados->count().'/'.$vacante->requisicion->cantidad !!})
					</h5>
				</div>
				<div class="modal-body">
					<p>
						@if($guardados->isEmpty())
							No se ha guardado a nadie para continuar con el proceso.
						@elseif($guardados->count() > $vacante->requisicion->cantidad)
							No puedes continuar con el proceso ya que hay más personas guardadas ({{$guardados->count()}}) que las permitidas por la vacante ({{$vacante->requisicion->cantidad}}).
						@elseif($guardados->count() < $vacante->requisicion->cantidad)
							Puedes continuar con el proceso teniendo en cuenta que puedes reclutar {{$vacante->requisicion->cantidad - $guardados->count()}} persona(s) más.
						@else
							Deseas cerrar el proceso de selección?
						@endif
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
					@if(isset($postulados[0]))
						<a class="btn btn-danger {{ ($guardados->count() > $vacante->requisicion->cantidad?'disabled':'') }}" href="{{ url('vacantes/'.$postulados[0]->vacante_id.'/closeVacancie') }}">Continuar</a>
					@endif
				</div>
			</div>
		</div>
	</div>

	{{-- modal para agregar un nuevo reclutador --}}
	@foreach($postulados as $postulado)
		<div class="modal fade" id="eliminar-modal{{ $postulado->id }}" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
			<div class="modal-dialog" role="document">
					<div class="modal-content">
						<form action="{{ route('postulante.destroy', $postulado->id) }}" method="POST">
							@csrf
							@method('DELETE')
							<div class="modal-header">
								REMOVER POSTULANTE
							</div>
							<div class="modal-body">
								<label for="motivo">Motivo: </label>
									<input type="hidden" name="id" value="{{ $postulado->id }}">
									<textarea name="motivo" id="motivo" cols="30" rows="5" style="resize:none;"></textarea>
							</div>
							<div class="modal-footer">
									<button type="submit" class="btn btn-danger">Eliminar</button>
									<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
							</div>
						</form>
					</div>
			</div>
		</div>

		<div class="modal fade" id="cuestionario-modal{{ $postulado->id }}" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						CUESTIONARIO
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-12">
								<label for="question1">1.- ¿Por qué está buscando nuevo empleo?</label>
								<p>R= {{ $postulado->encuesta?$postulado->encuesta->question1:'N/A' }}</p>
							</div>
							<div class="col-12">
								<label for="question2">2.- ¿Por qué desea cambiarse de trabajo?</label>
								<p>R= {{ $postulado->encuesta?$postulado->encuesta->question2:'N/A' }}</p>
							</div>
							<div class="col-12">
								<label for="question3">3.- ¿Por qué le interesa esta plaza?</label>    
								<p>R= {{ $postulado->encuesta?$postulado->encuesta->question3:'N/A' }}</p>
							</div>
							<div class="col-12">
								<label for="question4">4.- ¿Considera que usted sería la persona idónea para la plaza? Y ¿Por qué</label>
								<p>R= {{ $postulado->encuesta?$postulado->encuesta->question4:'N/A' }}</p>
							</div>
							<div class="col-12">
								<label for="question5">5.- ¿Cuál es su mayor virtud?   </label>    
								<p>R= {{ $postulado->encuesta?$postulado->encuesta->question5:'N/A' }}</p>
							</div>
							<div class="col-12">
								<label for="question6">6.- ¿Cuál es su mayor defecto?</label>
								<p>R= {{ $postulado->encuesta?$postulado->encuesta->question6:'N/A' }}</p>
							</div>
							<div class="col-12">
								<label for="question7">7.- ¿Qué conoce sobre nosotros?</label>    
								<p>R= {{ $postulado->encuesta?$postulado->encuesta->question7:'N/A' }}</p>
							</div>
							<div class="col-12">
								<label for="question8">8.- ¿Meta a corto, mediano y largo plazo?</label>
								<p>R= {{ $postulado->encuesta?$postulado->encuesta->question8:'N/A' }}</p>
							</div>
							<div class="col-12">
								<label for="question9">9.- ¿Cómo se enfrenta al fracaso?</label>    
								<p>R= {{ $postulado->encuesta?$postulado->encuesta->question9:'N/A' }}</p>
							</div>
							<div class="col-12">
								<label for="question10">10.- ¿Cómo te sientes al momento de presentar estrés laboral?</label>
								<p>R= {{ $postulado->encuesta?$postulado->encuesta->question10:'N/A' }}</p>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
			</div>
		</div>
	@endforeach
@endsection

@section('scripts')
<style type="text/css">
	.requerido:after {
        content: '*';
        color: red;
        padding-left: 5px;
    }
</style>
<script type="text/javascript">
	$('#tableReclutador').DataTable({
		language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
   	});
</script>
@endsection