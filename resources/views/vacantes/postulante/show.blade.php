@extends('vacantes.app')
@section('content')
	<div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
	</div>
	<br>
	
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9">
				<h3 class="font-weight-bold text-danger">POSTULADOS A LA VACANTE CERRADA</h3>
			</div>
			<div class="col-md-3 mb-4" style="text-align: right;">
                <a class="btn btn-small btn-primary mr-3" href="{{ url('vacantes/closedVacancies') }}">Regresar</a>
			</div>
		</div>
		<hr style="border: 2px solid #000000;">
		<table class="table table-striped table-bordered" id="tableReclutador">
			<thead class="bg-red text-white">
				<tr>
					<th>id</th>
					<th>Postulado</th>
					<th>Tipo Postulación</th>
					<th>Reclutador</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach($postulados as $postulado)
					<tr @if(!is_null($postulado->selected)) class="bg-secondary" @endif>
						<td>{{ $postulado->id }}</td>
						@if ($postulado->user_id != 0)
							<td>{{ $postulado->usuario->FullName }}</td>
							<td>Interno</td>
						@else
							<td>{{ $postulado->perfil->FullNamePerfil }}</td>
							<td>Externo</td>
						@endif
						<td>{{ $postulado->vacante->reclutador->usuario->getFullNameAttribute() }}</td>
						<td>
							@if(is_null($postulado->user_id))
								<a class="btn btn-small btn-primary" href="{{ url('profile/0/'.$postulado->profile_id.'/'.$postulado->vacante_id.'/consulta') }}">CV</a>
							@else
							    <a class="btn btn-small btn-primary" href="{{ url('profile/'.$postulado->user_id.'/'.$postulado->profile_id.'/'.$postulado->vacante_id.'/consulta') }}">CV</a>
							@endif
							@if($postulado->perfil->external != 1)
								<button type="button" class="btn btn-info text-white" data-toggle="modal" data-target="#cuestionario-modal{{ $postulado->id }}">
									Cuestionario
								</button>
							@endif
							{{-- <a class="btn btn-small btn-warning disabled" href="{{ url('profile/'.$postulado->profile_id.'/'.$postulado->vacante_id.'/generar') }}">Generar Acceso</a> --}}
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	{{-- modal para agregar un nuevo reclutador --}}
	@foreach($postulados as $postulado)
		<div class="modal fade" id="cuestionario-modal{{ $postulado->id }}" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						CUESTIONARIO
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-12">
								<label for="question1">1.- ¿Por qué está buscando nuevo empleo?</label>
								<p>R= {{ $postulado->encuesta?$postulado->encuesta->question1:'N/A' }}</p>
							</div>
							<div class="col-12">
								<label for="question2">2.- ¿Por qué desea cambiarse de trabajo?</label>
								<p>R= {{ $postulado->encuesta?$postulado->encuesta->question2:'N/A' }}</p>
							</div>
							<div class="col-12">
								<label for="question3">3.- ¿Por qué le interesa esta plaza?</label>    
								<p>R= {{ $postulado->encuesta?$postulado->encuesta->question3:'N/A' }}</p>
							</div>
							<div class="col-12">
								<label for="question4">4.- ¿Considera que usted sería la persona idónea para la plaza? Y ¿Por qué</label>
								<p>R= {{ $postulado->encuesta?$postulado->encuesta->question4:'N/A' }}</p>
							</div>
							<div class="col-12">
								<label for="question5">5.- ¿Cuál es su mayor virtud?   </label>    
								<p>R= {{ $postulado->encuesta?$postulado->encuesta->question5:'N/A' }}</p>
							</div>
							<div class="col-12">
								<label for="question6">6.- ¿Cuál es su mayor defecto?</label>
								<p>R= {{ $postulado->encuesta?$postulado->encuesta->question6:'N/A' }}</p>
							</div>
							<div class="col-12">
								<label for="question7">7.- ¿Qué conoce sobre nosotros?</label>    
								<p>R= {{ $postulado->encuesta?$postulado->encuesta->question7:'N/A' }}</p>
							</div>
							<div class="col-12">
								<label for="question8">8.- ¿Meta a corto, mediano y largo plazo?</label>
								<p>R= {{ $postulado->encuesta?$postulado->encuesta->question8:'N/A' }}</p>
							</div>
							<div class="col-12">
								<label for="question9">9.- ¿Cómo se enfrenta al fracaso?</label>    
								<p>R= {{ $postulado->encuesta?$postulado->encuesta->question9:'N/A' }}</p>
							</div>
							<div class="col-12">
								<label for="question10">10.- ¿Cómo te sientes al momento de presentar estrés laboral?</label>
								<p>R= {{ $postulado->encuesta?$postulado->encuesta->question10:'N/A' }}</p>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
			</div>
		</div>
	@endforeach
@endsection

@section('scripts')
<style type="text/css">
	.requerido:after {
        content: '*';
        color: red;
        padding-left: 5px;
    }
</style>
<script type="text/javascript">
	$('#tableReclutador').DataTable({
		language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
   	});
</script>
@endsection