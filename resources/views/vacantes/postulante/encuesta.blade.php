@extends('vacantes.app')
@section('content')
    <div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
    </div>
    
    <form action="{{ url('postulante') }}" method="post">
        @csrf
        <input type="hidden" name="vacante_id" id="vacante_id" value={{ $id }}>
        
        <div class="container-fluid">
            <div class="row mb-5">
                <div class="col-12">
                    <h3 class="font-weight-bold text-danger">ENCUESTA PARA POSTULANTE</h3>
                    <hr style="border: 2px solid #000000;">
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-3">
                    <div class="form-group">
                        <label for="question1" style="font-size:1.5em"><span class="font-weight-bold">1.-</span> ¿Por qué está buscando nuevo empleo?</label>    
                        <textarea class="form-control" class="form-control" name="question1" id="question1" cols="30" rows="5"></textarea>
                    </div>
                </div>
                <div class="col-12 mb-3">
                    <div class="form-group">
                        <label for="question2" style="font-size:1.5em"><span class="font-weight-bold">2.-</span> ¿Por qué desea cambiarse de trabajo?</label>
                        <textarea class="form-control" name="question2" id="question2" cols="30" rows="5"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-3">
                    <div class="form-group">
                        <label for="question3" style="font-size:1.5em"><span class="font-weight-bold">3.-</span> ¿Por qué le interesa esta plaza?</label>    
                        <textarea class="form-control" name="question3" id="question3" cols="30" rows="5"></textarea>
                    </div>
                </div>
                <div class="col-12 mb-3">
                    <div class="form-group">
                        <label for="question4" style="font-size:1.5em"><span class="font-weight-bold">4.-</span> ¿Considera que usted sería la persona idónea para la plaza? Y ¿Por qué</label>
                        <textarea class="form-control" name="question4" id="question4" cols="30" rows="5"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-3">
                    <div class="form-group">
                        <label for="question5" style="font-size:1.5em"><span class="font-weight-bold">5.-</span> ¿Cuál es su mayor virtud?   </label>    
                        <textarea class="form-control" name="question5" id="question5" cols="30" rows="5"></textarea>
                    </div>
                </div>
                <div class="col-12 mb-3">
                    <div class="form-group">
                        <label for="question6" style="font-size:1.5em"><span class="font-weight-bold">6.-</span> ¿Cuál es su mayor defecto?</label>
                        <textarea class="form-control" name="question6" id="question6" cols="30" rows="5"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-3">
                    <div class="form-group">
                        <label for="question7" style="font-size:1.5em"><span class="font-weight-bold">7.-</span> ¿Qué conoce sobre nosotros?</label>    
                        <textarea class="form-control" name="question7" id="question7" cols="30" rows="5"></textarea>
                    </div>
                </div>
                <div class="col-12 mb-3">
                    <div class="form-group">
                        <label for="question8" style="font-size:1.5em"><span class="font-weight-bold">8.-</span> ¿Meta a corto, mediano y largo plazo?</label>
                        <textarea class="form-control" name="question8" id="question8" cols="30" rows="5"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-3">
                    <div class="form-group">
                        <label for="question9" style="font-size:1.5em"><span class="font-weight-bold">9.-</span> ¿Cómo se enfrenta al fracaso?</label>    
                        <textarea class="form-control" name="question9" id="question9" cols="30" rows="5"></textarea>
                    </div>
                </div>
                <div class="col-12 mb-3">
                    <div class="form-group">
                        <label for="question10" style="font-size:1.5em"><span class="font-weight-bold">10.-</span> ¿Cómo te sientes al momento de presentar estrés laboral?</label>
                        <textarea class="form-control" name="question10" id="question10" cols="30" rows="5"></textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 mb-3">
                    <div class="form-group">
                        <a class="btn btn-info" href="{{ url('vacantes') }}">Regresar</a>
                        <button type="submit" class="btn btn-success" id="btn_enviar">Postularme</button>
                    </div>
                </div>
            </div>
        </form>
    </div>




@endsection()