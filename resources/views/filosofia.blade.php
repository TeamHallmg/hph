{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            {{-- <img class="img-fluid" src="/img/mosaico.png" alt=""> --}
            @foreach ($display_announcements as $announcement)
				@include('announcement.announcement_types.displays.display_announcement', ['announcements' => $announcement])
			@endforeach
        </div>
    </div>
</div>
@endsection --}}

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            {{-- <img class="img-fluid" src="/img/mosaico.png" alt=""> --}}
            @foreach ($display_announcements as $announcement)
				@include('announcement.announcement_types.displays.display_announcement', ['announcements' => $announcement])
            @endforeach
            <br>
            @isset($display_announcements)
                @foreach ($display_announcements as $announcement)
                    {{-- <li class="nav-item"> --}}
                        <a class="btn btn-secondary btn-sm" href="{{ url('announcements?view=' . $announcement['view'] . '&type=' . $announcement['type']->id )}}">
                            <i class="fa fa-plus nav-icon"></i> <span>Administrar</span> {{ $announcement['type']->show_name }}
                        </a>
                    {{-- </li> --}}
                @endforeach
            @endif
        </div>
    </div>
</div>
@endsection
