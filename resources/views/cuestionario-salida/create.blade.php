@extends('layouts.app')

@section('content')


<div class="row mt-3">
    <div class="col-md-2 text-right">
        @include('cuestionario-salida/partials/sub-menu')
    </div>
    <div class="col-md-10" style="font-size: 16px;">
      <img class="img-fluid" src="/img/banner_entrevista_salida.png" alt="">

    </div>
</div>
<div class="row mt-3">
    
    @if (Session::has('error'))
        <div class="alert alert-danger" role="alert">
            {{ Session::get('error') }}
        </div>
    @endif


    <div class="card card-3 shadow-3 mt-3 col-12"> 
        <div class="card-body">
            <div class="container-fluid">

                <div class="row my-2">
                    <div class="col-md-12">
                        <h4>
                            <strong style="color: #406bb2">
                                Datos
                            </strong>
                            <hr class="mt-1" style="border-bottom: 4px solid #373435;"></h4>
                    </div>
                </div>
 
                <form action="{{ url('cuestionario-salida') }}" method="post">
                    @csrf

                    <div class="row my-3">
                        <div class="col-md-6"></div>
                        <div class="col-md-3">
                            <label for="nombre">No. de Colaborador:</label>
                            <input type="hidden" name="user_id" value="{{ auth()->user()->id }}"> 
                            <input class="form-control" type="text" value="{{ !is_null(auth()->user()->employee) ? auth()->user()->employee->idempleado : '' }}" readonly> 
                        </div>
                        <div class="col-md-3">
                            <label for="nombre">Fecha de Renuncia:</label>
                            <input class="form-control" type="text" value="{{ date('Y-m-d') }}" readonly>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-md-6">
                            <label for="nombre">Nombre de Colaborardor:</label>
                            <input class="form-control" type="text" value="{{ auth()->user()->FullName }}" readonly>
                        </div>
                        <div class="col-md-3">
                            <label for="nombre">Puesto:</label>
                            <input class="form-control" type="text" value="{{ !is_null(auth()->user()->employee) ? !is_null(auth()->user()->employee->jobPosition) ? auth()->user()->employee->jobPosition->name : '' : '' }}" readonly>
                        </div>
                        <div class="col-md-3">
                            <label for="nombre">Fecha de ingreso:</label>
                            <input class="form-control"  type="text" value="{{ !is_null(auth()->user()->employee) ? auth()->user()->employee->ingreso : '' }}" readonly>  
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-md-6">
                            <label for="nombre">Departamento:</label>
                            <input class="form-control" type="text" value="{{ !is_null(auth()->user()->employee) ? !is_null(auth()->user()->employee->jobPosition) ? !is_null(auth()->user()->employee->jobPosition->area) ? !is_null(auth()->user()->employee->jobPosition->area->department_wt) ? auth()->user()->employee->jobPosition->area->department_wt->name : '' : '' : '' : '' }}" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="nombre">Nombre de su jefe inmediato:</label>
                            <input class="form-control" type="text" value="{{ !is_null(auth()->user()->employee) ? !is_null(auth()->user()->employee->boss) ? auth()->user()->employee->boss->FullName : '' : '' }}" readonly>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-md-6">
                            <label for="nombre">Hospital:</label>
                            <input class="form-control" type="text" value="{{ !is_null(auth()->user()->employee) ? auth()->user()->employee->sucursal : '' }}" readonly>
                        </div> 

                    </div>
                    </div>
                    </div>
                    </div>

    <div class="card card-3 shadow-3 mt-5"> 
        <div class="card-body">
            <div class="container-fluid">



                <div class="row my-2">
                    <div class="col-md-12">
                        <h4>
                            <strong style="color: #406bb2">
                                ¿Cuál es el motivo de tu renuncia? 
                            </strong>
                            Seleccionar solamente una opción
                            <hr class="mt-1" style="border-bottom: 4px solid #373435;"></h4>
                    </div>
                </div>



                    <div class="row my-1">
                        @foreach($motivos as $key => $motivo)
                            <div class="col-md-3 my-3">
                                <input type="radio" id="{{$key}}" name="mas_gusto" value="{{$motivo}}"> <label for="{{$key}}"> {{$motivo}} </label> 
                            </div>
                        @endforeach
                    </div>
 
                    <div class="row my-3">
                        <div class="col-md-12">
                            <div class="form-group ">
                                <label for="motivo_renuncia">Explica</label>
                                <textarea id="motivo_renuncia" class="form-control" name="motivo_renuncia" rows="4" style="resize: none;">{{ old('motivo_renuncia') }}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row my-5">
                        <div class="col-md-3">
                            <div class="form-group ">
                                <label for="problema_con_jefe">¿Último día laboral?</label>
                                <input class="form-control" type="date" name="menos_gusto">
                            </div>
                        </div>
                    </div>

                    <div class="row my-5">
                        <div class="col-md-12">
                            <div class="form-group ">
                                <label for="problema_con_jefe">¿Nos puedes compartir a qué empresa te vas?</label>
                                <textarea id="problema_con_jefe" class="form-control" name="problema_con_jefe" rows="4" style="resize: none;">{{ old('motivo_renuncia') }}</textarea>
                            </div>
                        </div>
                    </div>


                    <div class="row my-2 mt-5">
                        <div class="col-md-12">
                            <h4>
                                <strong style="color: #406bb2">
                                    Evalúa los siguientes factores que influenciaron la renuncia laboral.
                                </strong>
                                <hr class="mt-1" style="border-bottom: 4px solid #373435;"></h4>
                        </div>
                    </div>

                    <div class="row my-3">
                        <div class="col-md-12">
                            <h4>En cada pregunta elige lo que mejor se ajuste a tu opinión.</h4>
                            <table class="table" id="cuestionarios">
                                <thead class="thead-light">
                                    <tr>
                                        <th class="text-center">Factores</th>
                                        @foreach ($nivel_influencia as $nivel)
                                            <th class="text-center">{{$nivel}}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($factores as $key_factor => $factor)
                                        <tr>
                                            <td>{{ $factor['name'] }}</td>                                            
                                            @foreach ($factor['niveles'] as $key_nivel => $nivel)
                                                <td class="text-center"><input required type="radio" id="renuncia_mejora_puesto_sueldo" name="{{$key_factor}}" value="{{$key_nivel}}" {{ old('problema_con_jefe') == 'Si' ? 'checked' : '' }}></td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                        
                    <div class="row my-5">
                        <div class="col-md-12">
                            <label for="">¿Buscaste tú trabajo o te contactaron de otras empresas?</label>
                            <br>
                              <input type="radio" id="problema_con_jefe_decripcion" name="problema_con_jefe_decripcion" value="Yo busqué trabajo"> Yo busqué trabajo <br>
                                <input type="radio" id="problema_con_jefe_decripcion" name="problema_con_jefe_decripcion" value="Me contactaron"> Me contactaron <br>
                                <input type="radio" id="problema_con_jefe_decripcion" name="problema_con_jefe_decripcion" value="No Aplica"> No Aplica
                        </div>
                    </div>


                    <div class="row my-5">
                        <div class="col-md-12">
                            <div class="form-group ">
                                <label for="piden_dinero_descripcion">¿Qué es lo que más te gustó de trabajar aqui?</label>
                                <textarea id="piden_dinero_descripcion" class="form-control" name="piden_dinero_descripcion" rows="4" style="resize: none;">{{ old('motivo_renuncia') }}</textarea>
                            </div>
                        </div>
                    </div>


                    <div class="row my-5">
                        <div class="col-md-12">
                            <div class="form-group ">
                                <label for="piden_dinero">¿Qué es lo que nos sugerirías cambiar en la empresa?</label>
                                <textarea id="piden_dinero" class="form-control" name="piden_dinero" rows="4" style="resize: none;">{{ old('motivo_renuncia') }}</textarea>
                            </div>
                        </div>
                    </div>


                    <div class="row my-5">
                        <div class="col-md-12">
                            <div class="form-group ">
                                <label for="comentarios">Comentarios adicionales</label>
                                <textarea id="comentarios" class="form-control" name="comentarios" rows="4" style="resize: none;">{{ old('comentarios') }}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 d-flex justify-content-end">
                            <button type="submit" class="btn btn-success mx-4">Enviar</button>
                            <a href="{{ url('/home') }}" class="btn btn-secondary">Cancelar</a>    
                        </div>
                    </div>
                </form>

                <div id="aviso" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header bg-success">
                                <h5 class="modal-title text-white" id="my-modal-title">AVISO</h5>
                            </div>
                            <div class="modal-body">
                                <p>Tu cuestionario se ha guardado correctamente</p>
                                <p>Folio: {{ Session::get('success') }} </p>
                            </div>
                            <div class="modal-footer">
                                <a href="{{ url('/home') }}" class="btn btn-outline-success">Aceptar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('scripts')

<script type="text/javascript">
    @if (Session::has('success'))
        $(function() {
            $('#aviso').modal('show');
        });
    @endif
</script>

@endsection