<h4 class="font-weight-bold  color-personalizado-azul" style="margin-top: 15px;">Entrevista de <br>Salida</h4>
<hr>
<ul class="nav nav-pills flex-column">
	<li class="nav-item">
		<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('cuestionario-salida/create') }}">
			Nueva Encuesta de Salida
		</a>
	</li>
	@if(auth()->user()->role == 'admin')
		<li class="nav-item">
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('cuestionario-salida') }}">
				Administrar Encuestas
			</a>
		</li>
		<li class="nav-item">
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('cuestionario-graficos/reporte') }}">
				Gráficos e informes
			</a>
		</li>
	@endif	
</ul>