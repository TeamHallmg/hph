<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Cuestionario</title>

    <style>
        /** Define the margins of your page **/
        @page {
            margin: 0cm 0cm;
            font-family: Arial;
        }

        body {
            margin: 4cm 2cm 2cm;
            font-family: 'Source Sans Pro', sans-serif;
        }

        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;
            background-color: #2a0927;
            color: white;
            text-align: center;
            line-height: 30px;
        }

        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;
            background-color: #ebebeb;
            color: black;
            text-align: center;
            line-height: 35px;
        }
        .firma {
            text-align: center;
            border-top: 1px solid #000;
            margin-top: 2rem;
        }
    </style>
</head>

<body align="center">

    <header>
        <img src="img/cabecera_entrevista_de_salida.png" alt="" style="width:100%">
    </header>

    <main>
        <table>
            <tbody>
                <tr width="100%">
                    <td align="center">
                        <table style="font-size:16px; border-radius:10px; background:#ebebeb">
                            <tbody>
                                <tr width="100%">
                                    <td align="center" style="padding: 10px">
                                        La presente encuesta tiene como finalidad mejorar nuestra Empresa, en cada uno
                                        de sus procesos y departamentos.<br>
                                        Por lo tanto, sus respuestas serán estrictamente confidenciales y de suma
                                        utilidad, de antemano nuestro atento agradecimiento.
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <br>
        <table>
            <tbody>
                <tr width="100%">
                    <td align="center">
                        <table style="font-size:18px;">
                            <tbody>
                                <tr width="100%">
                                    <td>
                                        <table>
                                            <tbody>
                                                <tr width="100%">
                                                    <td width="100">
                                                        <span style="color:#406BB2"> Folio:</span>
                                                    </td>
                                                    <td width="100">
                                                        {{ $cuestionario->id }}
                                                    </td>
                                                </tr>
                                                <tr width="100%">
                                                    <td width="100">
                                                        <span style="color:#406BB2"> Fecha de Renuncia:</span>
                                                    </td>
                                                    <td width="100">
                                                        {{
                                                        Carbon\Carbon::parse($cuestionario->created_at)->format('d-m-Y')
                                                        }}
                                                    </td>
                                                </tr>
                                                <tr width="100%">
                                                    <td width="100">
                                                        <span style="color:#406BB2"> No de Colaborador:</span>
                                                    </td>
                                                    <td width="100">
                                                        {{ !is_null($auth->employee) ? $auth->employee->idempleado : ''
                                                        }}
                                                    </td>
                                                </tr>
                                                <tr width="100%">
                                                    <td width="100">
                                                        <span style="color:#406BB2"> Nombre del Colaborador:</span>
                                                    </td>
                                                    <td width="100">
                                                        {{ $auth->FullName }}
                                                    </td>
                                                </tr>
                                                <tr width="100%">
                                                    <td width="100">
                                                        <span style="color:#406BB2"> Fecha de Ingreso:</span>
                                                    </td>
                                                    <td width="100">
                                                        {{ !is_null($auth->employee) &&
                                                        !is_null($auth->employee->ingreso) ?
                                                        Carbon\Carbon::parse($auth->employee->ingreso)->format('d-m-Y')
                                                        : '' }}
                                                    </td>
                                                </tr>
                                                <tr width="100%">
                                                    <td width="100">
                                                        <span style="color:#406BB2"> Nombre de su jefe Inmediato:</span>
                                                    </td>
                                                    <td width="100">
                                                        {{ !is_null($auth->employee) ? !is_null($auth->employee->boss) ?
                                                        $auth->employee->boss->FullName : '' : '' }}
                                                    </td>
                                                </tr>
                                                <tr width="100%">
                                                    <td width="100">
                                                        <span style="color:#406BB2"> Hospital:</span>
                                                    </td>
                                                    <td width="100">
                                                        {{ !is_null($auth->employee) ? $auth->employee->sucursal : '' }}
                                                    </td>
                                                </tr>
                                                <tr width="100%">
                                                    <td width="100">
                                                        <span style="color:#406BB2"> Departamento</span>
                                                    </td>
                                                    <td width="100">
                                                        {{ !is_null($auth->employee) ?
                                                        !is_null($auth->employee->jobPosition) ?
                                                        !is_null($auth->employee->jobPosition->area) ?
                                                        !is_null($auth->employee->jobPosition->area->department_wt) ?
                                                        $auth->employee->jobPosition->area->department_wt->name : '' :
                                                        '' : '' : '' }}
                                                    </td>
                                                </tr>
                                                <tr width="100%">
                                                    <td width="100">
                                                        <span style="color:#406BB2"> Puesto:</span>
                                                    </td>
                                                    <td width="100">
                                                        {{ !is_null($auth->employee) ?
                                                        !is_null($auth->employee->jobPosition) ?
                                                        $auth->employee->jobPosition->name : '' : '' }}
                                                    </td>
                                                </tr>
                                                <tr width="100%">
                                                    <td width="100">
                                                        <span style="color:#406BB2"> Último día laboral:</span>
                                                    </td>
                                                    <td width="100">
                                                        {{
                                                        Carbon\Carbon::parse($cuestionario->menos_gusto)->format('d-m-Y')
                                                        }}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <br>
        <br>
        <table>
            <tbody>
                <tr width="100%">
                    <td align="center">
                        <table style="font-size:18px;">
                            <tbody>
                                <tr width="100%">
                                    <td width="100%">
                                        <table>
                                            <tbody>
                                                <tr width="100%">
                                                    <td>
                                                        <span style="color:#406BB2"> ¿Cuál es el motivo de tu
                                                            renuncia?</span>
                                                    </td>
                                                </tr>
                                                <tr width="100%">
                                                    <td>
                                                        @foreach($motivos as $key => $motivo)
                                                        @if($cuestionario->mas_gusto == $motivo)
                                                        <div
                                                            style="border: 1px solid #86869f; border-radius:5px; padding:10px; min-height: 100px; min-width:600px">
                                                            {{ $motivo }}</div>
                                                        @endif
                                                        @endforeach
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <br>
        <table>
            <tbody>
                <tr width="100%">
                    <td align="center">
                        <table with="500" style="font-size:18px;">
                            <tbody>                                
                                <tr width="100%">
                                    <td width="100%">
                                        <table>
                                            <tbody>
                                                <tr width="100%">
                                                    <td>
                                                        <span style="color:#406BB2"> Explica</span>
                                                    </td>
                                                </tr>
                                                <tr width="100%">
                                                    <td>
                                                        <div
                                                            style="border: 1px solid #86869f; border-radius:5px; padding:10px; min-height: 100px; min-width:600px">
                                                            {{ $cuestionario->motivo_renuncia }}</div>
                                                    </td>

                                                </tr>
                                            </tbody>
                                        </table>

                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <br>
        <table>
            <tbody>
                <tr width="100%">
                    <td align="center">
                        <table with="500" style="font-size:18px;">
                            <tbody>
                                <tr width="100%">
                                    <td width="100%">
                                        <table>
                                            <tbody>
                                                <tr width="100%">
                                                    <td>
                                                        <span style="color:#406BB2"> ¿Nos puedes compartir a qué empresa
                                                            te vas? </span>
                                                    </td>
                                                </tr>
                                                <tr width="100%">
                                                    <td>
                                                        <div
                                                            style="border: 1px solid #86869f; border-radius:5px; padding:10px; min-height: 100px; min-width:600px">
                                                            {{ $cuestionario->problema_con_jefe }}</div>
                                                    </td>

                                                </tr>
                                            </tbody>
                                        </table>

                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <br>
        <table>
            <tbody>
                <tr width="100%">
                    <td align="center">
                        <table with="500" style="font-size:18px;">
                            <tbody>
                                <tr width="100%">
                                    <td width="100%">
                                        <table>
                                            <tbody>
                                                <tr width="100%">
                                                    <td>
                                                        <span style="color:#406BB2"> Evalúa los siguientes factores que
                                                            influenciaron la renuncia laboral. </span>
                                                    </td>
                                                </tr>
                                                <tr width="100%">
                                                    <td style="padding: 20px;"">

                                                        <table width="100%">
                                                            <thead style="background: #406BB2; color:#fff">
                                                                <tr>
                                                                    <th width="10%" style="padding: 5px;">#</th>
                                                                    <th width="50%" style="padding: 5px;">Factores</th>
                                                                    <th width="50%" style="padding: 5px;">Respuesta</th>

                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($factores as $factor)
                                                                <tr>
                                                                    <td style="padding: 5px; color:#406BB2">{{
                                                                        $factor['id'] }}</td>
                                                                    <td style="padding: 5px; color:#406BB2">{{
                                                                        $factor['name'] }}</td>

                                                                    @foreach ($factor['niveles'] as $nivel)
                                                                    @if($nivel['selected']==1)
                                                                    <td style="padding: 5px; color:#406BB2">
                                                                        {{$nivel['name']}}
                                                                    </td>
                                                                    @endif
                                                                    @endforeach
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>


                                                    </td>

                                                </tr>
                                            </tbody>
                                        </table>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <br>
        <table>
            <tbody>
                <tr width="100%">
                    <td align="center">
                        <table with="500" style="font-size:18px;">
                            <tbody>
                                <tr width="100%">
                                    <td width="100%">
                                        <table>
                                            <tbody>
                                                <tr width="100%">
                                                    <td>
                                                        <span style="color:#406BB2"> ¿Buscaste tú trabajo o te
                                                            contactaron de otras empresas? </span>
                                                    </td>
                                                </tr>
                                                <tr width="100%">
                                                    <td>
                                                        <div
                                                            style="border: 1px solid #86869f; border-radius:5px; padding:10px; min-height: 100px; min-width:600px">
                                                            {{ $cuestionario->problema_con_jefe_decripcion }}</div>
                                                    </td>

                                                </tr>
                                            </tbody>
                                        </table>

                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>

        <table style="margin-top: 30px;">
            <tbody>
                <tr width="100%">
                    <td align="center">
                        <table with="500" style="font-size:18px;">
                            <tbody>
                                <tr width="100%">
                                    <td width="100%">
                                        <table>
                                            <tbody>
                                                <tr width="100%">
                                                    <td>
                                                        <span style="color:#406BB2"> ¿Qué es lo que más te gustó de
                                                            trabajar aqui? </span>
                                                    </td>
                                                </tr>
                                                <tr width="100%">
                                                    <td>
                                                        <div
                                                            style="border: 1px solid #86869f; border-radius:5px; padding:10px; min-height: 100px; min-width:600px">
                                                            {{ $cuestionario->piden_dinero_descripcion }}</div>
                                                    </td>

                                                </tr>
                                            </tbody>
                                        </table>

                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <br>
        <table>
            <tbody>
                <tr width="100%">
                    <td align="center">
                        <table with="500" style="font-size:18px;">
                            <tbody>
                                <tr width="100%">
                                    <td width="100%">
                                        <table>
                                            <tbody>
                                                <tr width="100%">
                                                    <td>
                                                        <span style="color:#406BB2"> ¿Qué es lo que nos sugerirías
                                                            cambiar en la empresa? </span>
                                                    </td>
                                                </tr>
                                                <tr width="100%">
                                                    <td>
                                                        <div
                                                            style="border: 1px solid #86869f; border-radius:5px; padding:10px; min-height: 100px; min-width:600px">
                                                            {{ $cuestionario->piden_dinero }}</div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <br>
        <table>
            <tbody>
                <tr width="100%">
                    <td align="center">
                        <table with="500" style="font-size:18px;">
                            <tbody>
                                <tr width="100%">
                                    <td width="100%">
                                        <table>
                                            <tbody>
                                                <tr width="100%">
                                                    <td>
                                                        <span style="color:#406BB2"> Comentarios adicionales </span>
                                                    </td>
                                                </tr>
                                                <tr width="100%">
                                                    <td>
                                                        <div
                                                            style="border: 1px solid #86869f; border-radius:5px; padding:10px; min-height: 100px; min-width:600px">
                                                            {{ $cuestionario->comentarios }}</div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <br>
        <table>
            <tbody>
                <tr>
                    <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                </tr> 
                <tr>
                    <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                </tr> 
                <tr width="100%">
                    <td align="center">
                        <table with="500" style="font-size:18px;">
                            <tbody>
                                <tr width="100%">
                                    <td width="100%">
                                        <table>
                                            <tbody>
                                                <tr width="100%">
                                                    <td>
                                                        <span style="color:#406BB2"> Nombre del Colaborador </span>
                                                    </td>
                                                </tr>
                                                <tr width="100%">
                                                    <td>
                                                        <div
                                                            style="border: 1px solid #86869f; border-radius:5px; padding:10px; min-height: 100px; min-width:600px">
                                                            {{ $auth->FullName }}</div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <br>
        <br>
        <br>
        <br>
         
        <div class="firma" style="min-height: 100px; min-width:200px">
            Firma y huella del colaborador
        </div>    



    </main>

    <footer>
        Copyright ©
        <?php echo date("Y");?>
    </footer>


</body>

</html>
<script type="text/php">
    if ( isset($pdf) ) {
        $pdf->page_script('
            $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
            $pdf->text(270, 730, "Pagina $PAGE_NUM de $PAGE_COUNT", $font, 10);
        ');
    }
</script>