@extends('layouts.app')

@section('content')
<div class="row mt-3">
    <div class="col-md-2 text-right">
        @include('cuestionario-salida/partials/sub-menu')
    </div>
    <div class="col-md-10">
      <img class="img-fluid" src="/img/banner_entrevista_salida.png" alt="">

    </div>
</div>
<div class="row mt-3"> 
    <div class="col-12">
        <div class="flash-message" id="mensaje">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-'.$msg))
                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
            @endforeach
        </div> 
    </div>
    
    <div class="col-12">
<div class="card mt-3">
    <div class="card-header bg-client-primary text-white p-3">
        <h4 class="mb-0">Listado de personal</h4>
    </div>
    
    <div class="card-body">
      



        <table class="table" id="cuestionarios">
            <thead class="thead-light">
                <tr>
                    <th class="text-center">Folio</th>
                    <th class="text-center">Nombre <BR> de colaborador</th>
                    <th class="text-center">Puesto  <BR> de trabajo</th>
                    <th class="text-center">Departamento</th>
                    <th class="text-center">Fecha de <BR> ingreso</th>
                    <th class="text-center">Fecha de <BR> renuncia</th>
                    <th class="text-center">Fecha de <BR> último día</th>
                    <th class="text-center">Motivo renuncia</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($cuestionarios as $cuestionario)
                    <tr>
                        <td class="bg-secondary text-white text-center">{{ $cuestionario->id }}</td>
                        <td class="bg-secondary text-white text-center">{{ $cuestionario->user->first_name }}</td>
                        <td>{{ !is_null($cuestionario->user->employee) ? !is_null($cuestionario->user->employee->jobPosition) ? $cuestionario->user->employee->jobPosition->name : '' : '' }}</td>                    
                        <td>{{ !is_null($cuestionario->user->employee) ? !is_null($cuestionario->user->employee->jobPosition) ? !is_null($cuestionario->user->employee->jobPosition->area) ? !is_null($cuestionario->user->employee->jobPosition->area->department_wt) ? $cuestionario->user->employee->jobPosition->area->department_wt->name : '' : '' : '' : '' }}</td>
                        <td>{{ !is_null($cuestionario->user->employee) && !is_null($cuestionario->user->employee->ingreso) ? Carbon\Carbon::parse($cuestionario->user->employee->ingreso)->format('d-m-Y') : '' }}</td>
                        <td>{{ Carbon\Carbon::parse($cuestionario->created_at)->format('d-m-Y') }}</td>
                        <td>{{ Carbon\Carbon::parse($cuestionario->menos_gusto)->format('d-m-Y') }}</td>
                        <td>{{ $cuestionario->mas_gusto }}</td>
                        <td>{{ $cuestionario->estatus }}</td>
                        <td nowrap>
                            <a href="{{ url('cuestionario-salida', ['cuestionario-salida'=>$cuestionario->id]) }}" class="btn btn-success btn-sm" title="Ver Cuestionario"> <i class="fa fa-search"></i></a>
                            <a data-toggle="modal" data-target="#AdjuntarPdf{{ $cuestionario->id }}" class="btn btn-info btn-sm" title="Adjuntar Documento Digitalizado"><i class="fa fa-file-upload"></i> </a>
                            @if(!is_null($cuestionario->url_file))<a href="{{asset('uploads/cuestionarios-salidas/'.$cuestionario->url_file)}}" target="_blank" class="btn btn-primary btn-sm" title="Descargar Documento Digitalizado"><i class="fa fa-file-download"></i> </a>@endif
                            <div id="AdjuntarPdf{{ $cuestionario->id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header bg-success">
                                            <h5 class="modal-title text-white" id="my-modal-title">Adjuntar Documento Digitalizado</h5>
                                        </div>
                                        <form method="POST" action="/cuestionario-salida/updateDoc" enctype="multipart/form-data"> 
                                            {!! csrf_field() !!}	
                                            <input type="hidden" value="{{ $cuestionario->id }}" name="id">
                                        <div class="modal-body"> 
                                            <div class="form-group col-12 col-md-12">
                                                <label for="view_file" class="col-form-label font-weight-bold">Buscar Documento:</label>
                                                <input type="file" class="form-control" name="file" id="file" required>
                                            </div>
                                        </div>
                                        <div class="modal-footer">      
                                            <a href="{{ url('/cuestionario-salida') }}" class="btn btn-secondary" data-dismiss="modal">Cancelar</a>
                                            <button type="submit" class="btn btn-primary" id="getJobData">Aceptar</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </div>
</div>
</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(function() {
        $('#cuestionarios thead tr').clone(true).appendTo( '#cuestionarios thead' );
        $('#cuestionarios thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            var vacio = '';
			if (i != 12 )
                $(this).html('<input type="text" class="form-control col-md-12" />');
            else
                $(this).html(vacio);
    
            $('input', this).on('keyup change', function () {
                if (table.column(i).search() !== this.value) {
                    table.column(i).search( this.value ).draw();
                }
            });
        });

        var table = $('#cuestionarios').DataTable( {
            // responsive: true,
            orderCellsTop: true,
            // fixedHeader: true,
            language: {
                url: "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            order: [[ 0, "desc" ]],
            scrollX: true,
            // scrollY: "300px",
            // scrollCollapse: true,
            fixedColumns:   {
                leftColumns: 2,
                rightColumns: 1,
            },
            dom: 'Bfltrip',
            buttons: [{
                extend: 'excelHtml5',
                className: "btn btn-success",
                init: function( api, node, config) {
                    $(node).removeClass('btn-secondary')
                },
                exportOptions: {
                    columns: [ 0,1,2,3,4,5,6,7,8,9,10,11 ]
                },
                text: '<i class="fas fa-file-excel"></i> Exportar a Excel',
            }]
        });
	});
</script>
@endsection