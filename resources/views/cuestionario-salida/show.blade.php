@extends('layouts.app')

@section('content')

<div class="row mt-3">
    <div class="col-md-2 text-right">
        @include('cuestionario-salida/partials/sub-menu')
    </div>
    <div class="col-md-10" style="font-size: 16px;">
      <img class="img-fluid" src="/img/banner_entrevista_salida.png" alt="">
    </div>
</div>
<div class="row mt-3"  style="font-size: 16px;">
    {{-- @if (Session::has('success'))
        {{-- {{ Session::get('success') }} --}
        <script type="text/javascript">
            $(function() {
				$('#aviso').modal('show');
			});
        </script>
    @endif --}}

    <div class="card card-3 shadow-3 mt-3 col-12"> 
        <div class="card-body">
            <div class="container-fluid">
           
                <div class="row my-2">
                    <div class="col-md-12">
                        <h4>
                            <strong style="color: #406bb2">
                                Datos
                            </strong>
                            <hr class="mt-1" style="border-bottom: 4px solid #373435;"></h4>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-3">
                        <label for="">Folio:</label>
                        <input type="text" class="form-control" value="{{ $cuestionario->id }}" readonly>
                    </div>
                </div>
                
                <div class="row my-3">
                    <div class="col-md-6"></div>
                    <div class="col-md-3">
                        <label for="nombre">No. de Colaborador:</label>
                        <input type="hidden" name="user_id" value="{{ $auth->id }}"> 
                        <input class="form-control" type="text" value="{{ !is_null($auth->employee) ? $auth->employee->idempleado : '' }}" readonly> 
                    </div>
                    <div class="col-md-3">
                        <label for="nombre">Fecha de Renuncia:</label>
                        <input class="form-control" type="text" value="{{ date('Y-m-d') }}" readonly>
                    </div>
                </div>
                <div class="row my-3">
                    <div class="col-md-6">
                        <label for="nombre">Nombre de Colaborardor:</label>
                        <input class="form-control" type="text" value="{{ $auth->FullName }}" readonly>
                    </div>
                    <div class="col-md-3">
                        <label for="nombre">Puesto:</label>
                        <input class="form-control" type="text" value="{{ !is_null($auth->employee) ? !is_null($auth->employee->jobPosition) ? $auth->employee->jobPosition->name : '' : '' }}" readonly>
                    </div>
                    <div class="col-md-3">
                        <label for="nombre">Fecha de ingreso:</label>
                        <input class="form-control"  type="text" value="{{ !is_null($auth->employee) ? $auth->employee->ingreso : '' }}" readonly>  
                    </div>
                </div>
                <div class="row my-3">
                    <div class="col-md-6">
                        <label for="nombre">Departamento:</label>
                        <input class="form-control" type="text" value="{{ !is_null($auth->employee) ? !is_null($auth->employee->jobPosition) ? !is_null($auth->employee->jobPosition->area) ? !is_null($auth->employee->jobPosition->area->department_wt) ? $auth->employee->jobPosition->area->department_wt->name : '' : '' : '' : '' }}" readonly>
                    </div>
                    <div class="col-md-6">
                        <label for="nombre">Nombre de su jefe inmediato:</label>
                        <input class="form-control" type="text" value="{{ !is_null($auth->employee) ? !is_null($auth->employee->boss) ? $auth->employee->boss->FullName : '' : '' }}" readonly>
                    </div>
                </div>
                <div class="row my-3">
                    <div class="col-md-6">
                        <label for="nombre">Hospital:</label>
                        <input class="form-control" type="text" value="{{ !is_null($auth->employee) ? $auth->employee->sucursal : '' }}" readonly>
                    </div>

                </div>
                
            </div>
        </div>

    </div>

        <div class="card card-3 shadow-3 mt-5"> 
            <div class="card-body"> 
            <div class="container-fluid">



                <div class="row my-2">
                    <div class="col-md-12">
                        <h4>
                            <strong style="color: #406bb2">
                                ¿Cuál es el motivo de tu renuncia? 
                            </strong>
                            Seleccionar solamente una opción
                            <hr class="mt-1" style="border-bottom: 4px solid #373435;"></h4>
                    </div>
                </div>
        
                <div class="row my-1">
                    @foreach($motivos as $key => $motivo)
                        <div class="col-md-3 my-2">
                            @if($cuestionario->mas_gusto == $motivo)
                                <i class="fas fa-check-circle"></i>
                            @else
                                <i class="far fa-circle"></i>                           
                            @endif
                            <label for="{{$key}}"> {{$motivo}} </label> 
                        </div>
                    @endforeach
                </div>

                <div class="row my-3">
                    <div class="col-md-12">
                        <div class="form-group ">
                            <label for="motivo_renuncia">Explica</label>
                            <textarea id="motivo_renuncia" class="form-control"  disabled readonly name="motivo_renuncia" rows="4" style="resize: none;">{{ $cuestionario->motivo_renuncia }}</textarea>
                        </div>
                    </div>
                </div>

                <div class="row my-5">
                    <div class="col-md-3">
                        <div class="form-group ">
                            <label for="problema_con_jefe">¿Último día laboral?</label>
                            <input class="form-control" type="text" disabled readonly value="{{ $cuestionario->menos_gusto}}">
                        </div>
                    </div>
                </div>

                <div class="row my-5">
                    <div class="col-md-12">
                        <div class="form-group ">
                            <label for="problema_con_jefe">¿Nos puedes compartir a qué empresa te vas?</label>
                            <textarea id="problema_con_jefe" class="form-control"  disabled readonly name="problema_con_jefe" rows="4" style="resize: none;">{{ $cuestionario->problema_con_jefe }}</textarea>
                        </div>
                    </div>
                </div>


                <div class="row my-2 mt-5">
                    <div class="col-md-12">
                        <h4>
                            <strong style="color: #406bb2">
                                Evalúa los siguientes factores que influenciaron la renuncia laboral.
                            </strong>
                            <hr class="mt-1" style="border-bottom: 4px solid #373435;"></h4>
                    </div>
                </div>

                <div class="row my-3">
                    <div class="col-md-12">
                        <h4>En cada pregunta elige lo que mejor se ajuste a tu opinión.</h4>
                        <table class="table" id="cuestionarios">
                            <thead class="thead-light">
                                <tr>
                                    <th class="text-center">Factores</th>
                                    @foreach ($nivel_influencia as $nivel)
                                        <th class="text-center">{{$nivel['name']}}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($factores as $factor)
                                    <tr>
                                        <td>{{ $factor['name'] }}</td>
                                        
                                        @foreach ($factor['niveles'] as $nivel)
                                            <td class="text-center">
                                                
                                                @if($nivel['selected']==1)
                                                    <i class="fas fa-check-circle"></i>
                                                @else
                                                    <i class="far fa-circle"></i>                           
                                                @endif

                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                    
                <div class="row my-5">
                    <div class="col-md-12">
                        <label for="">¿Buscaste tú trabajo o te contactaron de otras empresas?</label>
                        <br>
                        
                        @if($cuestionario->problema_con_jefe_decripcion == 'Yo busqué trabajo')
                            <i class="fas fa-check-circle"></i>
                        @else
                            <i class="far fa-circle"></i>                           
                        @endif
                        Yo busqué trabajo
                        <br >

                        @if($cuestionario->problema_con_jefe_decripcion == 'Me contactaron')
                            <i class="fas fa-check-circle"></i>
                        @else
                            <i class="far fa-circle"></i>                           
                        @endif
                        Me contactaron
                        Yo busqué trabajo
                        <br >

                        @if($cuestionario->problema_con_jefe_decripcion == 'No Aplica')
                            <i class="fas fa-check-circle"></i>
                        @else
                            <i class="far fa-circle"></i>                           
                        @endif
                        No Aplica
  </div>
                </div>


                <div class="row my-5">
                    <div class="col-md-12">
                        <div class="form-group ">
                            <label for="piden_dinero_descripcion">¿Qué es lo que más te gustó de trabajar aqui?</label>
                            <textarea id="piden_dinero_descripcion" class="form-control" disabled readonly name="piden_dinero_descripcion" rows="4" style="resize: none;">{{ $cuestionario->piden_dinero_descripcion }}</textarea>
                        </div>
                    </div>
                </div>


                <div class="row my-5">
                    <div class="col-md-12">
                        <div class="form-group ">
                            <label for="piden_dinero">¿Qué es lo que nos sugerirías cambiar en la empresa?</label>
                            <textarea id="piden_dinero" class="form-control" name="piden_dinero" disabled readonly rows="4" style="resize: none;">{{ $cuestionario->piden_dinero }}</textarea>
                        </div>
                    </div>
                </div>


                <div class="row my-5">
                    <div class="col-md-12">
                        <div class="form-group ">
                            <label for="comentarios">Comentarios adicionales</label>
                            <textarea id="comentarios" class="form-control" name="comentarios" disabled readonly rows="4" style="resize: none;">{{ $cuestionario->comentarios }}</textarea>
                        </div>
                    </div>
                </div>

            
                <div class="row my-5">
                    <div class="col-md-6">
                        <label for="nombre">Nombre de Colaborardor</label>
                        <input class="form-control" type="text" value="{{ $auth->FullName }}" readonly>
                    </div>    
                    <div class="col-md-6">
                        <label for="nombre">Firma y huella del colaborador</label>
                        <input class="form-control" type="text" readonly>
                    </div>            
                </div>

                <div class="row my-5">
                    <div class="col-md-12 text-center">
                        <a href="{{ url('cuestionario-salida/pdf', ['id'=> $cuestionario->id]) }}" target="_blank" class="btn btn-primary w-25 px-3">Crear PDF</a> 
                    </div>           
                </div>
 
        </div>
        </div>
        </div>
 

<div class="card card-3 shadow-3 mt-5"> 
    <div class="card-body">
        <div class="container-fluid">
            
        <div class="row my-2">
            <div class="col-md-12">
                <h4>
                    <strong style="color: #406bb2">
                        Para uso exclusivo de Recursos Humanos
                    </strong>
                    <hr class="mt-1" style="border-bottom: 4px solid #373435;"></h4>
                    
                    Motivo validado para captura de motivo de renuncia en sistema:
            </div>
        </div>
  
        <div class="row my-1">
            @foreach($motivos as $key => $motivo)
                <div class="col-md-3 my-2">
                    @if (is_null($cuestionario->renuncia_mejora_puesto_sueldo))

                        <input type="radio" id="{{$key}}" name="renuncia_mejora_puesto_sueldo" value="{{$motivo}}" {{ $cuestionario->renuncia_mejora_puesto_sueldo == $motivo ? 'checked' : '' }}> 
                  
                    @else

                        @if($cuestionario->renuncia_mejora_puesto_sueldo == $motivo)
                            <i class="fas fa-check-circle"></i>
                        @else
                            <i class="far fa-circle"></i>                           
                        @endif

                    @endif
                  
                    <label for="{{$key}}"> {{$motivo}} </label> 
                </div>
            @endforeach
        </div>

            <div class="row my-5">
                <div class="col-md-6 ">
                    <label for="estatus">Nombre y firma de quíen aplicó la Entrevista de Salida </label>
                    <input class="form-control" type="text" value="{{ !is_null($cuestionario->userValidado) ? !is_null($cuestionario->userValidado->employee) ? $cuestionario->userValidado->employee->FullName : Auth::user()->FullName : Auth::user()->FullName }}" readonly>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 d-flex justify-content-end">
                    @if (is_null($cuestionario->renuncia_mejora_puesto_sueldo))
                        <button class="revisar_sin btn btn-outline-primary mr-3">Guardar</button>
                    @endif
                    <a href="{{ url('cuestionario-salida') }}" class="btn btn-secondary">Regresar</a>    
                </div>
            </div>

        </div>
    </div>
</div>

  

    <div id="aviso" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-success">
                    <h5 class="modal-title text-white" id="my-modal-title">AVISO</h5>
                </div>
                <div class="modal-body">
                    <p>El estatus se ha guardado correctamente</p>
                    <p>Folio: <input type="text" class="form-control" name="folio" id="folio" readonly></p>
                </div>
                <div class="modal-footer">
                    <a href="{{ url('cuestionario-salida') }}" class="btn btn-outline-success">Aceptar</a>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@endsection

@section('scripts')

<script type="text/javascript">

	$(function() {
 
        $('.revisar_sin').on('click', function(){
       
            var renuncia_mejora_puesto_sueldo = $('input:radio[name=renuncia_mejora_puesto_sueldo]:checked').val()
 
            $.ajax({
                type: "POST",
                url: "{{ url('cuestionario-salida/updateEstatus') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: '{{ $cuestionario->id }}',
                    estatus: 'Revisado',
                    renuncia_mejora_puesto_sueldo: renuncia_mejora_puesto_sueldo,
                },
                success: function (response) {
                    console.log(response);
                    $('#aviso').modal('show');
                    $('#folio').val(response.id);
                }
            });
        });

	});
</script>
@endsection