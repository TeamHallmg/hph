<div class="card-body">
     <div class="row">
         <div class="col-md-6 pb-3">
            <div class="form-group">
                <label for="">Mes</label>
                <select class="cambiando form-control _change" v-model="mes" @change="getRecords()">
                    <option :value="item.id" v-for="(item, index) in filtros.meses" :key="index"> @{{item.name}} </option> 
                </select>
            </div>
        </div>
        <div class="col-md-6 pb-3">
            <div class="form-group">
                <label for="">Acumulado</label>
                <select class="cambiando form-control _change" v-model="acumulado" @change="getRecords()">
                    <option :value="item.id" v-for="(item, index) in filtros.acumulados" :key="index"> @{{item.name}} </option> 
                </select>
            </div>
        </div>
        <div class="col-md-6 pb-3">
            <div class="form-group">
                <label for="">Hospital</label>
                <select class="cambiando form-control _change" v-model="region" @change="getRecords()">
                    <option value="">--TODOS--</option>
                    <option :value="item.id" v-for="(item, index) in filtros.regiones" :key="index"> @{{item.name}} </option> 
                </select>
            </div>
        </div>
        <div class="col-md-6 pb-3">
            <div class="form-group">
                <label for="">Departamento</label>
                <select class="cambiando form-control" v-model="dpto" @change="getRecords()">
                    <option value="">--TODOS--</option>
                    <option :value="item.id" v-for="(item, index) in filtros.dptos" :key="index"> @{{item.name}} </option> 
                </select>
            </div>
        </div>
        <div class="col-md-6 pb-3">
            <div class="form-group">
                <label for="">Puesto</label>
                <select class="cambiando form-control" v-model="puesto" @change="getRecords()">
                    <option value="">--TODOS--</option>
                    <option :value="item.id" v-for="(item, index) in filtros.puestos" :key="index"> @{{item.name}} </option> 
                </select>
            </div>
        </div>
    </div>
</div>

<div class="card-footer">
  <button class="btn btn-info _reset btn-sm float-right" @click="resetFiltros()"> Reiniciar Filtros</button>
</div>
