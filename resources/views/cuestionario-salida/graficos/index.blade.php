@extends('layouts.app')

@section('content')

	<h1>Gráficos</h1>
	<div class="row">
		{{-- <div class="col">
			<form action="" method="post" id="datos" novalidate="">
				<div class="d-flex flex-row my-2">
					<label for="anio" class="col-md-3">Año: <span style="color: red"> *</span></label>
					<select name="anio" id="anio" class="form-control col-md-3" required>
						<option value="" disabled hidden selected>Selecciona un año...</option>
						@for ($anio = date('Y'); $anio >= 2020; $anio--)
							<option value="{{ $anio }}" selected>{{ $anio }}</option>
						@endfor
					</select>
				</div>
				@php $months = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']; @endphp
				<div class="d-flex flex-row my-2">
					<label for="inicio" class="col-md-3">Mes inicial: <span style="color: red"> *</span></label>
					<select name="inicio" id="inicio" class="form-control col-md-3" required>
						<option value="" disabled hidden selected>Selecciona un mes...</option>
						@for ($mes = date('n'); $mes <= 12; $mes++)
							<option value="{{ $mes }}" {{ $mes==1 ? 'selected' : ''}}>{{ $months[$mes] }}</option>
						@endfor
					</select>
				</div>
				<div class="d-flex flex-row my-2">
					<label for="fin" class="col-md-3">Mes final: <span style="color: red"> *</span></label>
					<select name="fin" id="fin" class="form-control col-md-3" required>
						<option value="" disabled hidden selected>Selecciona un mes...</option>
						@for ($mes = date('n'); $mes <= 12; $mes++)
							<option value="{{ $mes }}" {{ $mes==12 ? 'selected' : ''}}>{{ $months[$mes] }}</option>
						@endfor
					</select>
				</div>
				<div class="d-flex flex-row my-2">
					<label for="area" class="col-md-3">Área:</label>
					<input type="text" name="area" id="area" class="form-control col-md-3">
				</div>
				<div class="d-flex flex-row my-2">
					<label for="depto" class="col-md-3">Departamento:</label>
					<input type="text" name="depto" id="depto" class="form-control col-md-3">
				</div>

				<button type="submit" id="btn_generar" class="btn btn-primary">Generar</button>
			</form>
		</div> --}}

		<div class="col">
			<form action="" method="post" id="datos" novalidate="">
				<div class="d-flex flex-row my-2">
					<label for="anio" class="col-md-3">Año: <span style="color: red"> *</span></label>
					<select name="anio" id="anio" class="form-control col-md-3" required>
						@foreach ($anios as $anio)
							<option value="{{ $anio }}" {{ $loop->first ? 'selected' : ''}}>{{ $anio }}</option>
						@endforeach
					</select>
				</div>
				@php $months = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']; @endphp
				<div class="d-flex flex-row my-2">
					<label for="inicio" class="col-md-3">Mes inicial: <span style="color: red"> *</span></label>
					<select name="inicio" id="inicio" class="form-control col-md-3" required>
						@foreach ($meses as $mes)
							<option value="{{ $mes }}" {{ $loop->first ? 'selected' : ''}}>{{ $months[$mes] }}</option>
						@endforeach
					</select>
				</div>
				<div class="d-flex flex-row my-2">
					<label for="fin" class="col-md-3">Mes final: <span style="color: red"> *</span></label>
					<select name="fin" id="fin" class="form-control col-md-3" required>
						@foreach ($meses as $mes)
							<option value="{{ $mes }}" {{ $loop->last ? 'selected' : ''}}>{{ $months[$mes] }}</option>
						@endforeach
					</select>
				</div>
				<div class="d-flex flex-row my-2">
					<label for="area" class="col-md-3">Área:</label>
					<select name="area" id="area" class="form-control col-md-3">
						<option selected value="">Sin área...</option>
						@foreach ($areas as $key => $area)
							<option value="{{ $area }}">{{ $area }}</option>
						@endforeach
					</select>
				</div>
				
				<div class="d-flex flex-row my-2">
					<label for="depto" class="col-md-3">Departamento:</label>
					<select name="depto" id="depto" class="form-control col-md-3">
						<option selected value="">Sin departamento...</option>
						@foreach ($deptos as $key => $depto)
							<option value="{{ $depto }}">{{ $depto }}</option>
						@endforeach
					</select>
				</div>

				<button type="submit" id="btn_generar" class="btn btn-primary">Generar</button>
			</form>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<div id="negativas" class="mt-5"></div>
		</div>
		<div class="col-md-6">
			<div id="positivas" class="mt-5"></div>
		</div>
	</div>

	{{-- Aviso para las opciones de búsqueda --}}
	<div id="aviso" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header alert-danger">
					<h5 class="modal-title text-uppercase font-weight-bold" id="my-modal-title">Aviso</h5>
					<button class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>El rango de fechas no se puede graficar</p>
				</div>
				<div class="modal-footer">
					<button type="button" id="btn_confirmar" class="btn btn-outline-danger" data-dismiss="modal">Aceptar</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		var datos;
		var anio;
		var inicio;
		var fin;
		var depto;

		$('#anio').on('change', function(e){
			e.preventDefault();		
			let anioThis = $(this).val();
			let ultimo;
			$.ajax({
				type: "POST",
				url: "{{ url('/cuestionario-graficos/fechas') }}",
				data: {
					_token: '{{ csrf_token() }}',
					anio: anioThis,
				},
				dataType: "json",
				success: function (response) {
					console.log(response);
					$('#inicio').empty();
					$.each(response['meses'], function(key, element) {
						$('#inicio').append('<option value='+key+'>'+element+'</option>');
					});
					$('#fin').empty();
					$.each(response['meses'], function(key, element) {
						$('#fin').append('<option value='+key+'>'+element+'</option>');
						ultimo = key;
					});
					$('#fin option[value="'+ultimo+'"]').prop('selected', true);

					$('#depto').empty();
					$('#depto').append('<option value="">Sin departamento...</option>');
					$.each(response['deptos'], function(key, element) {
						console.log(element);
						$('#depto').append('<option value="'+element+'">'+element+'</option>');
					});
				}
			});
		});

		$('#btn_generar').on('click', function(e){
			e.preventDefault();
			
			anio = $('#anio').val();
			inicio = $('#inicio').val();
			fin = $('#fin').val();
			depto = $('#depto').val();

			var form = $("#datos")[0];
			var isValid = form.checkValidity();
			
			if (!isValid) {
				e.preventDefault();
				e.stopPropagation();
				form.classList.add('was-validated');
			} 	
			form.classList.add('was-validated');
			
			/* if(inicio > fin) {
				$('#aviso').modal('show');
			} else {
				graficar();
			} */
			graficar();
		});
	
		function graficar() {
			$.ajax({
				type: "POST",
				url: "{{ url('/cuestionario-graficos/graficos') }}",
				data: {
					_token: '{{ csrf_token() }}',
					anio: anio,
					inicio: inicio,
					fin: fin,
					depto: depto,
				},
				dataType: "json",
				success: function (response) {
					datos = response;
					
					Highcharts.chart('negativas', {
						chart: {
							type: 'column'
						},
						credits: {
      						enabled: false
						},
						title: {
							text: 'Encuesta de Salida'
						},
						subtitle: {
							text: datos['subtitulo']
						},
						xAxis: {
							categories: datos['xCategorias'],
							crosshair: true
						},
						yAxis: {
							min: 0,
							title: {
								text: 'Cantidad'
							}
						},
						plotOptions: {
							column: {
								pointPadding: 0.2,
								borderWidth: 0
							}
						},
						series: [{
							name: 'Problemas con Jefe',
							data: datos['problema_jefe_cant'],
							color: '#ffff00',
						}, {
							name: 'Jefe Pidió Dinero',
							data: datos['piden_dinero_cant'],
							color: '#ff0000',
						}, {
							name: 'No contesto',
							data: datos['negativos_null'],
							color: '#34495E',
						}]
					});

					Highcharts.chart('positivas', {
						chart: {
							type: 'column'
						},
						credits: {
      						enabled: false
						},
						title: {
							text: 'Encuesta de Salida'
						},
						subtitle: {
							text: datos['subtitulo']
						},
						xAxis: {
							categories: datos['xCategorias'],
							crosshair: true
						},
						yAxis: {
							min: 0,
							title: {
								text: 'Cantidad'
							}
						},
						plotOptions: {
							column: {
								pointPadding: 0.2,
								borderWidth: 0
							}
						},
						series: [{
							name: 'Recomendaria a la empresa',
							data: datos['recomendaria_cant'],
							color: '#A9DFBF',
						}, {
							name: 'Volveria con Nosotros',
							data: datos['volveria_cant'],
							color: '#27AE60',
						},{
							name: 'No contesto',
							data: datos['positivos_null'],
							color: '#34495E',
						}]
					});
				}
			});
		}
		
	</script>
@endsection