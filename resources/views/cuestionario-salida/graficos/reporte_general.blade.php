@extends('clima-organizacional.app')

@section('title', 'Encuesta')

@section('content')
<div class="app">
    <div class="row">
    <div class="col-md-2 text-right">
        @include('cuestionario-salida/partials/sub-menu')
    </div>
    <div class="col-md-10">
        <img class="img-fluid" src="/img/banner_entrevista_salida.png" alt="">

        <div class="row">
            <div class="col-md-12"> 
                @include('cuestionario-salida.graficos.reporte_filtros')
            </div>
        </div> 


        <div class="row mt-5">
            <div class="col-md-12">
                <h4>
                    <strong style="color: #406bb2">
                        MOTIVO DE RENUNCIA 
                    </strong>
                    Validada
                    <hr class="mt-1" style="border-bottom: 4px solid #373435;"></h4>
            </div>
        </div>  
        <div class="row pb-5 ">
            <div class="col-12">
                
                <div class="row my-2">
                    <div class="col-md-12 text-center">
                        <h4>
                            <strong style="color: #406bb2">
                                Gráficas Semestral
                            </strong><br>
                            @{{bajas}} bajas
                        </h4>
                    </div>
                </div>  

                <highcharts :options="chartOptions" ></highcharts>
            </div>
        </div>

        
        <div class="row mt-5">
            <div class="col-md-12">
                <h4>
                    <strong style="color: #406bb2">
                        FACTORES QUE INFLUYERON
                    </strong>
                    <hr class="mt-1" style="border-bottom: 4px solid #373435;"></h4>
            </div>
        </div>  
        <div class="row my-3">
            <div class="col-md-12">
                <h4>En cada pregunta elige lo que mejor se ajuste a tu opinión.</h4>
                <table class="table" id="cuestionarios">
                    <thead class="thead-light">
                        <tr>
                            <th class="text-center">No.</th>
                            <th class="text-center">Factores</th>
                            <th class="text-center" v-for="(items, index) in nivel_influencia" :key="index">@{{items.name}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(items, index) in factores" :key="index">
                            <td>@{{items.id}}</td>
                            <td>@{{items.name}}</td>
                            <td class="text-center"  v-for="(nivel, index_niveles) in items.niveles" :key="index_niveles">
                                @{{ nivel.total}}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        

        <div class="row mt-5">
            <div class="col-md-12">
                <h4>
                    <strong style="color: #406bb2">
                        ¿BUSCASTE TÚ EL TRABAJO O SE CONTACTARON CONTIGO?
                    </strong>
                    <hr class="mt-1" style="border-bottom: 4px solid #373435;"></h4>
            </div>
        </div>  
        <div class="row pb-5">
            <div class="col-6"> 
                <highcharts :options="chartOptionsPie" ></highcharts>
            </div>
            <div class="col-6 d-flex align-items-center"> 
                <table width="100%">
                    <tr v-for="(items, index) in contacto" :key="index" class="h4">
                        <td  width="70%"> <i class="fa fa-circle" :style="'color:'+items.color"></i> @{{items.name}}</td>
                        <td>@{{items.total}}</td>
                    </tr>
                </table>
            </div>
        </div>
</div>
</div>
</div>
@endsection
@section('scripts_vuejs')

<script type="text/javascript">
 
 var pieColors = (function () {
    var colors = [],
        base = Highcharts.getOptions().colors[0],
        i;

    for (i = 0; i < 10; i += 1) {
        // Start out with a darkened base color (negative brighten), and end
        // up with a much brighter color
        colors.push(Highcharts.color(base).brighten((i - 3) / 7).get());
    }
    return colors;
}());

Vue.use(HighchartsVue.default) 
var app = new Vue({
  el: '#app',
    created: function(){
        this.getRecords();
    }, 
    data: {

        loading:true,
        
        filtros: {   
            meses : [],
            acumulados : [],
            puestos : [],
            regiones : [],
            dptos : [],
        }, 

        bajas : 0,  
        mes : '',  
        puesto : '',
        dpto : '',
        region : '',
        acumulado : '',
        nivel_influencia : [],
        factores : [],
        contacto : [],





        porct: '0',
    title: '',
    options: ['pie'],
    modo: 'spline',
    series_pie: [],
    series: [
        {   name: 'porc',
            innerSize: '50%',
            data: [
            ]
        }, 
        ],

    },
    computed: {
        chartOptions() { 
            return {
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                accessibility: {
                    announceNewData: {
                        enabled: true
                    }
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'Total de Salidas'
                    }

                },
                            series: this.series,       
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y}'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> <br/>'
                },
            }
        },
        
        chartOptionsPie() { 
            return {
                chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
        pointFormat: '<b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            colors: pieColors,
            dataLabels: {
                enabled: true,
                format: '{point.percentage:.1f} %',
                distance: -50,
                style: {
                    fontSize : '20px'
                },
                filter: {
                    property: 'percentage',
                    operator: '>',
                    value: 4
                }
            }
        }
    },
                series: this.series_pie,     
            }
        },
    },
    methods:{
 
        resetFiltros: function(){
            this.mes = '';
            this.puesto = '';
            this.region = '';
            this.dpto = '';
            this.acumulado = '';
            this.getRecords();
        },
        getRecords: function(){
            var seriesas = [];
            var series_pie = [];
            this.loading =true;
            var url = "/cuestionario-salida/reporte";
            axios.post(url,{
                mes:this.mes,
                puesto:this.puesto,
                region:this.region,
                dpto:this.dpto,
                acumulado:this.acumulado
            }).then(response=>{
                console.log(response.data);

                this.loading = false;
                var bajas = response.data.bajas;
                this.bajas = response.data.bajas;
                this.nivel_influencia = response.data.nivel_influencia;
                this.factores = response.data.factores;
                this.contacto = response.data.contacto;
                    
                $.each(response.data.motivos, function(key, value) {
                    seriesas.push({name:value.name,y:value.total});
                });
                
                this.series = [
                    {   name: 'porc',
                        innerSize: '50%',
                        data: seriesas
                    },
                ];

                $.each(response.data.contacto, function(key, value) {
                    var total = value.total * 100 / bajas;
                    series_pie.push({color:value.color,name:value.name,y:total});
                });
                
                this.series_pie = [
                    {    
                        data: series_pie
                    },
                ];


                if(this.mes==''){
                    this.filtros.meses = response.data.meses;
                }
                if(this.puesto==''){
                    this.filtros.puestos = response.data.puestos;
                }
                if(this.dpto==''){
                    this.filtros.dptos = response.data.dptos;
                }
                if(this.region==''){
                    this.filtros.regiones = response.data.regiones;
                }
                if(this.acumulado==''){
                    this.filtros.acumulados = response.data.acumulados;
                }                 
            })
        },

    },
})

</script>
@endsection