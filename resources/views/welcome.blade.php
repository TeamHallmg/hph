<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Hospitales Puerta de Hierro</title>
		<link rel="shortcut icon" type="image/png" href="img/favicon/favicon.ico"/>
		
    <meta name="csrf-token" content="{{ csrf_token() }}">
		<!-- Scripts -->
        {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
        {{-- <script src="{{ asset('css/login.js') }}" defer></script> --}}
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
		
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
			.bg {
				background-image: url('/img/fondo login.png') !important;
			}
            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
                z-index: 2;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;

            }

            .m-b-md {
                margin-bottom: 30px;
            }

            #cabecera{
                 width: 100%;
                 height: 500px;
            }
            
            #video {
                 position: absolute;
                 left: 0;
                 top: 0;
                 width: 100%;
                 height: auto;
                 z-index: 1;
                 visibility: visible;
            }

        </style>
{{-- 
        <script>
            document.getElementById('vid').play();
        </script>
 --}}
        <link rel="stylesheet" href="{{ asset('css/login.css') }}">

        <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/61b8d0cac82c976b71c16977/1fmsuv5r9';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

    </head>
    <body>
        <div class="bg flex-center position-ref full-height content-menu" id="app">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <!--<a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Registro</a>-->
                    @endauth
                </div>
            @endif 

            <div class="content">
              
                <div class="row d-flex justify-content-center align-items-center">
                    <div class="col-md-8">

                        <img src="{!! asset('img/logo login.png') !!}" class="img-fluid w-75">

                        <a href="{{ route('login') }}" class="btn rounded-pill text-white font-weight-bold m-5" style="background:#406BB2">INICIAR SESIÓN </a>

                         {{-- <div id="cabecera">
                             <!—El código del vídeo iría aquí-->
							 <a href="{{ route('login') }}">
                             <video id="video" autoplay muted>								 
                                <source src="{{URL::asset('/video/portada.mp4')}}" type="video/mp4">
                            </video>
							</a>
                        </div> --}}
                    </div>
                </div>
				{{--
              <div class="row d-flex justify-content-center align-items-center">
                  <div class="col-2">
                      <img src="{!! asset('img/footer_1.png') !!}" class="img-fluid w-100">
                  </div>
                  <div class="col-2">
                      <img src="{!! asset('img/footer_2.png') !!}" class="img-fluid w-100">
                  </div>
              </div>--}}
            </div>
        </div>
        <footer class="pb-3 pt-3 bg-dark fixed-bottom">
          <div class="container">
              <div class="row d-flex justify-content-center align-content-start">
                  <div class="text-white text-center font-weight-bold" style="color:#406BB2 !important">
                      Power by Hall MG
                  </div>
              </div>
          </div>
        </footer>
    </body>
</html>