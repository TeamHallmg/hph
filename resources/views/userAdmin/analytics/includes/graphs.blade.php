<div class="row">
    {{-- PLANTILLA DE PERSONAL --}}
    <div class="col-12 mt-5">
        <div class="card border-0">
            <div class="card-header bg-blue text-white font-weight-bold">
                PLANTILLA DE PERSONAL
            </div>
            <div class="card-body text-center">
                @if($type == 'month')
                    <div id="graph1"></div>
                @elseif($type == 'annual')
                    <div id="graph2"></div>
                @endif
            </div>
        </div>
    </div>
    {{-- ALTAS Y BAJAS --}}
    <div class="col-6 mt-5">
        <div class="card border-0">
            <div class="card-header bg-blue text-white font-weight-bold">
                ALTAS Y BAJAS
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col text-center">
                        <img class="img-fluid" src="{{ asset('img/graphs/graph-up.png') }}">
                        <div class="image-content-centered-graph text-white text-center text-weight-bold text-responsive">
                            <span id="{{ $type }}_user_flow_up">#</span>
                        </div>
                        <p class="font-weight-bold mt-3 h5">ALTAS</p>
                    </div>
                    <div class="col text-center">
                        <img class="img-fluid" src="{{ asset('img/graphs/graph-down.png') }}">
                        <div class="image-content-centered-graph text-white text-center text-weight-bold text-responsive">
                            <span id="{{ $type }}_user_flow_down">#</span>
                        </div>
                        <p class="font-weight-bold mt-3 h5">BAJAS</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Módulo de Incidencias --}}
    <div class="col-6 mt-5">
        <div class="card border-0">
            <div class="card-header bg-blue text-white font-weight-bold">
                &nbsp
            </div>
            <div class="card-body text-center">
                <div class="row">
                    <div class="col d-flex justify-content-center">
                        <div>
                            <div class="circle-graph-blue d-flex align-items-center">
                                <p class="font-weight-bold text-center w-100 h1">
                                    <span id="{{ $type }}_incidents_incapacities">#</span>
                                </p>
                            </div>
                            <p class="font-weight-bold mt-3 h5">INCAPACIDADES</p>
                        </div>
                    </div>
                    <div class="col d-flex justify-content-center">
                        <div>
                            <div class="circle-graph-gray d-flex align-items-center">
                                <p class="font-weight-bold text-center w-100 h1">
                                    <span id="{{ $type }}_incidents_vacations">#</span>
                                </p>
                            </div>
                            <p class="font-weight-bold mt-3 h5">VACACIONES</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- DISTRIBUCIÓN POR NIVELES --}}
    <div class="col-6 mt-5 d-none">
        <div class="card border-0">
            <div class="card-header bg-blue text-white">
                DISTRIBUCIÓN POR NIVELES
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered data-table data-table-mensual" id="data-table-fuentes-mensual">
                        <thead class="text-white bg-lightblue">
                            <tr>
                                <th>Niveles</th>
                                <th class="text-center">Activos</th>
                                <th class="text-center">Vacantes</th>
                            </tr>
                        </thead>
                        <tbody>
                            @for($i = 1; $i < 9; $i++)
                                <tr>
                                    <td>Lorem ipsum {{ $i }}</td>
                                    <td class="text-center">{{ $i }}</td>
                                    <td class="text-center">{{ $i + 14 }}</td>
                                </tr>
                            @endfor
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row mt-5">
    <div class="col-12">
        <div class="card border-0">
            <div class="card-header bg-blue text-white font-weight-bold">
                DEMOGRÁFICOS
            </div>
            <div class="card-body text-center">
                <div class="row justify-content-center">
                    <div class="col-4" style="border-right: 2px solid #BDBFC1;">
                        <div class="row mr-5">
                            <div class="col-3 d-flex px-0 my-auto">
                                <p class="font-weight-bold mt-2">
                                    <span class="h3 text-lightblue mr-3">
                                        <i class="fas fa-male"></i>
                                    </span>
                                    Hombre	
                                </p>
                            </div>
                            <div class="col-7 px-0 my-auto">
                                <div class="progress" style="height: 20px;">
                                    <div class="progress-bar bg-lightblue" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" id="{{ $type }}_demographics_percentage_mens"></div>
                                </div>
                            </div>
                            <div class="col-2">
                                <p class="mt-3 font-weight-bold">
                                    <span id="{{ $type }}_demographics_mens">#</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-4" style="border-left: 2px solid #BDBFC1;">
                        <div class="row ml-5">
                            <div class="col-3 d-flex pr-0 my-auto">
                                <p class="font-weight-bold mt-2">
                                    <span class="h3 text-pink mr-3">
                                        <i class="fas fa-female"></i>
                                    </span>
                                    Mujer	
                                </p>
                            </div>
                            <div class="col-7 px-0 my-auto">
                                <div class="progress" style="height: 20px;">
                                    <div class="progress-bar bg-pink" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" id="{{ $type }}_demographics_percentage_womens"></div>
                                </div>
                            </div>
                            <div class="col-2">
                                <p class="mt-3 font-weight-bold">
                                    <span id="{{ $type }}_demographics_womens">#</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    @foreach($tables as $table)
        <div class="col-6 mt-5">
            <div class="card border-0">
                <div class="card-header bg-secondary text-white text-center font-weight-bold">
                    {{ $table['name'] }}
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table text-center">
                            <thead class="text-white">
                                <tr>
                                    <th class="text-lightblue" width="10%">Hombre</th>
                                    <th width="25%"></th>
                                    <th width="25%"></th>
                                    <th width="25%"></th>
                                    <th class="text-pink" width="10%">Mujeres</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($table['rows'] as $key => $label)
                                    <tr>
                                        <td>
                                            <span id="{{ $type }}_{{ $table['key'] }}_m_{{ $key }}">#</span>
                                        </td>
                                        <td>
                                            <div class="progress justify-content-end progress-bar-left" style="height: 20px;">
                                                <div class="progress-bar bg-lightblue" id="{{ $type }}_{{ $table['key'] }}_m_percentage_{{ $key }}" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </td>
                                        <td>
                                            {{ $label }}
                                        </td>
                                        <td>
                                            <div class="progress progress-bar-right" style="height: 20px;">
                                                <div class="progress-bar bg-pink" id="{{ $type }}_{{ $table['key'] }}_f_percentage_{{ $key }}" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </td>
                                        <td>
                                            <span id="{{ $type }}_{{ $table['key'] }}_f_{{ $key }}">#</span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    {{-- Escolaridad --}}
    <div class="col-6 mt-5 d-none">
        <div class="card border-0">
            <div class="card-header bg-secondary text-white text-center">
                Escolaridad
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table text-center">
                        <thead class="text-white">
                            <tr>
                                <th class="text-lightblue" width="10%">Hombre</th>
                                <th width="25%"></th>
                                <th width="25%"></th>
                                <th width="25%"></th>
                                <th class="text-pink" width="10%">Mujeres</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>89</td>
                                <td>
                                    <div class="progress justify-content-end progress-bar-left" style="height: 20px;">
                                        <div class="progress-bar bg-lightblue" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>Solo Primaria</td>
                                <td>
                                    <div class="progress progress-bar-right" style="height: 20px;">
                                        <div class="progress-bar bg-pink" role="progressbar" style="width: 8%;" aria-valuenow="8" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>5</td>
                            </tr>
                            <tr>
                                <td>23</td>
                                <td>
                                    <div class="progress justify-content-end progress-bar-left" style="height: 20px;">
                                        <div class="progress-bar bg-lightblue" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>Solo Secundaria</td>
                                <td>
                                    <div class="progress progress-bar-right" style="height: 20px;">
                                        <div class="progress-bar bg-pink" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>32</td>
                            </tr>
                            <tr>
                                <td>12</td>
                                <td>
                                    <div class="progress justify-content-end progress-bar-left" style="height: 20px;">
                                        <div class="progress-bar bg-lightblue" role="progressbar" style="width: 11%;" aria-valuenow="11" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>Solo Preparatoria</td>
                                <td>
                                    <div class="progress progress-bar-right" style="height: 20px;">
                                        <div class="progress-bar bg-pink" role="progressbar" style="width: 10%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>10</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>
                                    <div class="progress justify-content-end progress-bar-left" style="height: 20px;">
                                        <div class="progress-bar bg-lightblue" role="progressbar" style="width: 4%;" aria-valuenow="4" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>Carerra Técnica</td>
                                <td>
                                    <div class="progress progress-bar-right" style="height: 20px;">
                                        <div class="progress-bar bg-pink" role="progressbar" style="width: 6%;" aria-valuenow="6" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>8</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>
                                    <div class="progress justify-content-end progress-bar-left" style="height: 20px;">
                                        <div class="progress-bar bg-lightblue" role="progressbar" style="width: 5%;" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>Carrera Universitaria</td>
                                <td>
                                    <div class="progress progress-bar-right" style="height: 20px;">
                                        <div class="progress-bar bg-pink" role="progressbar" style="width: 3%;" aria-valuenow="3" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>3</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>
                                    <div class="progress justify-content-end progress-bar-left" style="height: 20px;">
                                        <div class="progress-bar bg-lightblue" role="progressbar" style="width: 5%;" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>Maestría</td>
                                <td>
                                    <div class="progress progress-bar-right" style="height: 20px;">
                                        <div class="progress-bar bg-pink" role="progressbar" style="width: 3%;" aria-valuenow="3" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>3</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>
                                    <div class="progress justify-content-end progress-bar-left" style="height: 20px;">
                                        <div class="progress-bar bg-lightblue" role="progressbar" style="width: 5%;" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>Doctorado</td>
                                <td>
                                    <div class="progress progress-bar-right" style="height: 20px;">
                                        <div class="progress-bar bg-pink" role="progressbar" style="width: 3%;" aria-valuenow="3" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>3</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{-- Estado Civil --}}
    <div class="col-6 mt-5 d-none">
        <div class="card border-0">
            <div class="card-header bg-secondary text-white text-center">
                Estado Civil
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table text-center">
                        <thead class="text-white">
                            <tr>
                                <th class="text-lightblue" width="10%">Hombre</th>
                                <th width="25%"></th>
                                <th width="25%"></th>
                                <th width="25%"></th>
                                <th class="text-pink" width="10%">Mujeres</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>89</td>
                                <td>
                                    <div class="progress justify-content-end progress-bar-left" style="height: 20px;">
                                        <div class="progress-bar bg-lightblue" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>Soltero</td>
                                <td>
                                    <div class="progress progress-bar-right" style="height: 20px;">
                                        <div class="progress-bar bg-pink" role="progressbar" style="width: 8%;" aria-valuenow="8" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>5</td>
                            </tr>
                            <tr>
                                <td>23</td>
                                <td>
                                    <div class="progress justify-content-end progress-bar-left" style="height: 20px;">
                                        <div class="progress-bar bg-lightblue" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>Soltero con hijos</td>
                                <td>
                                    <div class="progress progress-bar-right" style="height: 20px;">
                                        <div class="progress-bar bg-pink" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>32</td>
                            </tr>
                            <tr>
                                <td>12</td>
                                <td>
                                    <div class="progress justify-content-end progress-bar-left" style="height: 20px;">
                                        <div class="progress-bar bg-lightblue" role="progressbar" style="width: 11%;" aria-valuenow="11" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>Casado</td>
                                <td>
                                    <div class="progress progress-bar-right" style="height: 20px;">
                                        <div class="progress-bar bg-pink" role="progressbar" style="width: 10%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>10</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>
                                    <div class="progress justify-content-end progress-bar-left" style="height: 20px;">
                                        <div class="progress-bar bg-lightblue" role="progressbar" style="width: 4%;" aria-valuenow="4" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>Casado con hijos</td>
                                <td>
                                    <div class="progress progress-bar-right" style="height: 20px;">
                                        <div class="progress-bar bg-pink" role="progressbar" style="width: 6%;" aria-valuenow="6" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>8</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{-- Padecimiento crónico --}}
    <div class="col-6 mt-5 d-none">
        <div class="card border-0">
            <div class="card-header bg-secondary text-white text-center">
                Padecimiento Crónico
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table text-center">
                        <thead class="text-white">
                            <tr>
                                <th class="text-lightblue" width="10%">Hombre</th>
                                <th width="25%"></th>
                                <th width="25%"></th>
                                <th width="25%"></th>
                                <th class="text-pink" width="10%">Mujeres</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>89</td>
                                <td>
                                    <div class="progress justify-content-end progress-bar-left" style="height: 20px;">
                                        <div class="progress-bar bg-lightblue" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>Diabetes</td>
                                <td>
                                    <div class="progress progress-bar-right" style="height: 20px;">
                                        <div class="progress-bar bg-pink" role="progressbar" style="width: 8%;" aria-valuenow="8" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>5</td>
                            </tr>
                            <tr>
                                <td>23</td>
                                <td>
                                    <div class="progress justify-content-end progress-bar-left" style="height: 20px;">
                                        <div class="progress-bar bg-lightblue" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>Hipertensión</td>
                                <td>
                                    <div class="progress progress-bar-right" style="height: 20px;">
                                        <div class="progress-bar bg-pink" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>32</td>
                            </tr>
                            <tr>
                                <td>12</td>
                                <td>
                                    <div class="progress justify-content-end progress-bar-left" style="height: 20px;">
                                        <div class="progress-bar bg-lightblue" role="progressbar" style="width: 11%;" aria-valuenow="11" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>Obesidad</td>
                                <td>
                                    <div class="progress progress-bar-right" style="height: 20px;">
                                        <div class="progress-bar bg-pink" role="progressbar" style="width: 10%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>10</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>
                                    <div class="progress justify-content-end progress-bar-left" style="height: 20px;">
                                        <div class="progress-bar bg-lightblue" role="progressbar" style="width: 4%;" aria-valuenow="4" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>Trastornos metabólicos</td>
                                <td>
                                    <div class="progress progress-bar-right" style="height: 20px;">
                                        <div class="progress-bar bg-pink" role="progressbar" style="width: 6%;" aria-valuenow="6" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>8</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>
                                    <div class="progress justify-content-end progress-bar-left" style="height: 20px;">
                                        <div class="progress-bar bg-lightblue" role="progressbar" style="width: 4%;" aria-valuenow="4" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>Otras enfermedades crónicas</td>
                                <td>
                                    <div class="progress progress-bar-right" style="height: 20px;">
                                        <div class="progress-bar bg-pink" role="progressbar" style="width: 6%;" aria-valuenow="6" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>8</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
</div>