@extends('layouts.app')

@section('title', 'TABLERO ANALÍTICO DE PERSONAL')

@section('content')

<div class="row my-5">
	<div class="col">
		<div class="flash-message" id="mensaje">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
				@if(Session::has($msg))
					<p class="alert alert-{{ $msg }}">{{ Session::get($msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
				@endif
			@endforeach
		</div>

		
		<div class="card border-0">
			<div class="card-header bg-blue">
				<h1 class="my-auto font-weight-bold text-center text-white">TABLERO ANALÍTICO DE PERSONAL</h1>
			</div>
			<div class="card-body">

				<ul class="nav nav-pills my-3 title-border" id="pills-tab" role="tablist">
					<li class="nav-item nav-item-mr">
						<a class="btn-tab font-weight-bold active blue_header" id="avance-mensual-tab" data-toggle="pill" href="#avance-mensual" role="tab" aria-controls="avance-mensual" aria-selected="true">
							Avance Mensual
						</a>
					</li>
					<li class="nav-item nav-item-mr">
						<a class="btn-tab font-weight-bold blue_header" id="avance-anual-tab" data-toggle="pill" href="#avance-anual" role="tab" aria-controls="avance-anual" aria-selected="false">
							Avance Anual
						</a>
					</li>
				</ul>

				<div class="tab-content" id="myTabContent">

					<div class="tab-pane fade show active" id="avance-mensual" role="tabpanel" aria-labelledby="avance-mensual-tab">				
						{{-- Cambio de Mes --}}
						<div class="row justify-content-center">
							<div class="col-1 d-flex align-items-center justify-content-end">
								<button class="btn btn-primary cambiar_mes_mensual prev" data-filtro="tab-mensual">
									<i class="fas fa-caret-left fa-3x"></i>
								</button>
							</div>
							<div class="nowrap col-4">
								<div class="my-2 rounded-lg text-center bg-blue">
									<div class="d-flex justify-content-start text-white px-2 pt-2 rounded-lg">
										<div class="bg-lightblue mr-3 rounded-lg align-middle h1 pt-2 text-center" style="width:60px; height: 60px;">
											<i class="fas fa-calendar-day"></i>
										</div>
										<h5 class="pt-2 text-left text-white h6">
											<strong>Mes</strong><br> <span class="h3"> <span class="getMonthMensual"></span> <span class="getMonthAno"></span> </span>
										</h5>
									</div>
								</div>
							</div>
							<div class="col-1 d-flex align-items-center justify-content-start">
								<button class="btn btn-primary cambiar_mes_mensual next" data-filtro="tab-mensual">
									<i class="fas fa-caret-right fa-3x"></i>
								</button>
							</div>
						</div>
						@include('userAdmin.analytics.includes.graphs', ['type' => 'month'])
					</div>

					<div class="tab-pane fade" id="avance-anual" role="tabpanel" aria-labelledby="avance-anual-tab">
						{{-- Cambio de Año --}}
						<div class="row justify-content-center">
							<div class="col-1 d-flex align-items-center justify-content-end">
								<button class="btn btn-primary cambiar_anual prev" data-filtro="tab-anual">
									<i class="fas fa-caret-left fa-3x"></i>
								</button>
							</div>
							<div class="nowrap col-4">
								<div class="my-2 rounded-lg text-center bg-blue">
									<div class="d-flex justify-content-start text-white px-2 pt-2 rounded-lg">
										<div class="bg-lightblue mr-3 rounded-lg align-middle h1 pt-2 text-center" style="width:60px; height: 60px;">
											<i class="fas fa-calendar-day"></i>
										</div>
										<h5 class="pt-2 text-left text-white h6">
											<strong>Año</strong><br> <span class="h3"> <span class="getAnnual"></span> </span>
										</h5>
									</div>
								</div>
							</div>
							<div class="col-1 d-flex align-items-center justify-content-start">
								<button class="btn btn-primary cambiar_anual next" data-filtro="tab-anual">
									<i class="fas fa-caret-right fa-3x"></i>
								</button>
							</div>
						</div>
						@include('userAdmin.analytics.includes.graphs', ['type' => 'annual'])
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function () {

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		const MONTH_NAMES = ["ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE" ];
		var antiquity = ['less_than_a_year', 'one_to_five_years', 'five_to_ten_years', 'ten_to_fifteen_years', 'more_than_fifteen_years'];
		var year_range = ['18_to_25_years', '26_to_35_years', '36_to_45_years', '46_to_60_years', 'more_than_60_years'];

		var global_now = new Date();
		var now = new Date();
		var strgetMonth = now.getMonth();
		var strDateyear = now.getFullYear();
		updateMonthData(now);
		updateAnnualData(now);

		function updateMonthData(now) {
			strgetMonth = now.getMonth();
			strDateyear = now.getFullYear();
			var now_bkp = new Date(now);
			var past = now_bkp.setMonth(now_bkp.getMonth() - 1, 1);
			var future = now_bkp.setMonth(now_bkp.getMonth() + 2, 1);

			if(future > global_now) {
				$(".cambiar_mes_mensual.next").attr("disabled", true);
				$(".cambiar_mes_mensual.next").removeClass("btn-primary");
				$(".cambiar_mes_mensual.next").addClass("btn-secondary");
			} else {
				$(".cambiar_mes_mensual.next").attr("disabled", false);
				$(".cambiar_mes_mensual.next").addClass("btn-primary");
				$(".cambiar_mes_mensual.next").removeClass("btn-secondary");
			}

			$('.getMonthMensual').html(MONTH_NAMES[strgetMonth]);
			$('.getMonthAno').html(strDateyear);
			$(".cambiar_mes_mensual.prev").attr("data-mes", past);
			$(".cambiar_mes_mensual.next").attr("data-mes", future);

			setMonthData();
		}

		function updateAnnualData(now) {
			strDateyear = now.getFullYear();
			var now_bkp = new Date(now);
			var past = now_bkp.setFullYear(now_bkp.getFullYear() - 1, 1);
			var future = now_bkp.setFullYear(now_bkp.getFullYear() + 2, 1);

			if(future > global_now) {
				$(".cambiar_anual.next").attr("disabled", true);
				$(".cambiar_anual.next").removeClass("btn-primary");
				$(".cambiar_anual.next").addClass("btn-secondary");
			} else {
				$(".cambiar_anual.next").attr("disabled", false);
				$(".cambiar_anual.next").addClass("btn-primary");
				$(".cambiar_anual.next").removeClass("btn-secondary");
			}

			$('.getAnnual').html(strDateyear);
			$(".cambiar_anual.prev").attr("data-mes", past);
			$(".cambiar_anual.next").attr("data-mes", future);
			setAnnualData();
		}

		function setAnnualData() {
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: '/get_user_analytics',
				data: {
					'_token': $('input[name="csrf-token"]').val(),
					'date': formatDate(now),
					'month': strgetMonth,
					'year': strDateyear,
					'type': 'annual',
				},
				beforeSend: function () {
				},
				success: function (response) {
					// Altas/Bajas
					$('#annual_user_flow_up').html(response.user_flow['created']);
					$('#annual_user_flow_down').html(response.user_flow['deleted']);
					// Módulo Incidentes
					$('#annual_incidents_incapacities').html(response.incidents['incapacities']);
					$('#annual_incidents_vacations').html(response.incidents['vacations']);
					// Demográficos
					$('#annual_demographics_mens').html(response.demographics['mens']);
					$('#annual_demographics_womens').html(response.demographics['womens']);
					$('#annual_demographics_percentage_mens').css('width', response.demographics['percentage_mens']+'%');
					$('#annual_demographics_percentage_womens').css('width', response.demographics['percentage_womens']+'%');
					// Antiguedad
					$.each( antiquity, function(index, value) {
						$('#annual_antiquity_m_'+value).html(response.antiquity['M'][value]);
						$('#annual_antiquity_m_percentage_'+value).css('width', response.antiquity['M']['percentage_'+value]+'%');
						$('#annual_antiquity_f_'+value).html(response.antiquity['F'][value]);
						$('#annual_antiquity_f_percentage_'+value).css('width', response.antiquity['F']['percentage_'+value]+'%');
					});
					// Rango de Edad
					$.each( year_range, function(index, value) {
						$('#annual_year_range_m_'+value).html(response.year_range['M'][value]);
						$('#annual_year_range_m_percentage_'+value).css('width', response.year_range['M']['percentage_'+value]+'%');
						$('#annual_year_range_f_'+value).html(response.year_range['F'][value]);
						$('#annual_year_range_f_percentage_'+value).css('width', response.year_range['F']['percentage_'+value]+'%');
					});

					graph2.update({
						series: [{
							type: 'pie',
							name: 'Plantilla de Personal',
							innerSize: '50%',
							data: [
								['Cubierta', response.personal.covered],
								['Pendiente', response.personal.pending],
							]
						}]
					});

				},
				error: function (jqXHR) {
					console.log('boo!');
				}
			});
		}

		function setMonthData() {
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: '/get_user_analytics',
				data: {
					'_token': $('input[name="csrf-token"]').val(),
					'date': formatDate(now),
					'month': strgetMonth,
					'year': strDateyear,
					'type': 'month',
				},
				beforeSend: function () {
				},
				success: function (response) {
					// Altas/Bajas
					$('#month_user_flow_up').html(response.user_flow['created']);
					$('#month_user_flow_down').html(response.user_flow['deleted']);
					// Módulo Incidentes
					$('#month_incidents_incapacities').html(response.incidents['incapacities']);
					$('#month_incidents_vacations').html(response.incidents['vacations']);
					// Demográficos
					$('#month_demographics_mens').html(response.demographics['mens']);
					$('#month_demographics_womens').html(response.demographics['womens']);
					$('#month_demographics_percentage_mens').css('width', response.demographics['percentage_mens']+'%');
					$('#month_demographics_percentage_womens').css('width', response.demographics['percentage_womens']+'%');
					// Antiguedad
					$.each( antiquity, function(index, value) {
						$('#month_antiquity_m_'+value).html(response.antiquity['M'][value]);
						$('#month_antiquity_m_percentage_'+value).css('width', response.antiquity['M']['percentage_'+value]+'%');
						$('#month_antiquity_f_'+value).html(response.antiquity['F'][value]);
						$('#month_antiquity_f_percentage_'+value).css('width', response.antiquity['F']['percentage_'+value]+'%');
					});
					// Rango de Edad
					$.each( year_range, function(index, value) {
						$('#month_year_range_m_'+value).html(response.year_range['M'][value]);
						$('#month_year_range_m_percentage_'+value).css('width', response.year_range['M']['percentage_'+value]+'%');
						$('#month_year_range_f_'+value).html(response.year_range['F'][value]);
						$('#month_year_range_f_percentage_'+value).css('width', response.year_range['F']['percentage_'+value]+'%');
					});

					graph1.update({
						series: [{
							type: 'pie',
							name: 'Plantilla de Personal',
							innerSize: '50%',
							data: [
								['Cubierta', response.personal.covered],
								['Pendiente', response.personal.pending],
							]
						}]
					});

				},
				error: function (jqXHR) {
					console.log('boo!');
				}
			});
		}

		function formatDate(date) {
			var d = new Date(date),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();

			if (month.length < 2) 
				month = '0' + month;
			if (day.length < 2) 
				day = '0' + day;

			return [year, month, day].join('-');
		}

		$('button.cambiar_mes_mensual').on('click', function(e) {
			e.preventDefault();
			var btn = $(this);
			var now_i = btn.attr("data-mes");
			now = new Date(parseInt(now_i));
			updateMonthData(now);
		});

		$('button.cambiar_anual').on('click', function(e) {
			e.preventDefault();
			var btn = $(this);
			var now_i = btn.attr("data-mes");
			now = new Date(parseInt(now_i));
			updateAnnualData(now);
		});

		const graph1 = Highcharts.chart('graph1', {
			title:false,
			plotOptions: {
				pie: {
					dataLabels: {
						enabled: true,
						// distance: -50,
						format: '<b>{point.name}</b>:<br>{point.percentage:.1f} %<br>total: {point.total}',
						style: {
							fontWeight: 'bold',
							color: 'black'
						}
					},
					startAngle: -90,
					endAngle: 90,
					center: ['50%', '75%'],
					size: '125%',
					showInLegend: true
				}
			},
			legend: {
				backgroundColor: 'white',
				itemWidth: 90,
				itemStyle: {
					fontSize: '14px',
					color: 'black',
					textOverflow: "allow"
				}
			},
			series: [{
				type: 'pie',
				name: 'Plantilla de Personal',
				innerSize: '50%',
				data: [
					['Cubierta', 0],
					['Pendiente', 0],
				]
			}]
		});

		const graph2 = Highcharts.chart('graph2', {
			title:false,
			plotOptions: {
				pie: {
					dataLabels: {
						enabled: true,
						distance: -50,
						style: {
							fontWeight: 'bold',
							color: 'white'
						}
					},
					startAngle: -90,
					endAngle: 90,
					center: ['50%', '75%'],
					size: '125%',
					showInLegend: true
				}
			},
			legend: {
				backgroundColor: 'white',
				itemWidth: 90,
				itemStyle: {
					fontSize: '14px',
					color: 'black',
					textOverflow: "allow"
				}
			},
			series: [{
				type: 'pie',
				name: 'Plantilla de Personal',
				innerSize: '50%',
				data: [
					['Cubierta', 0],
					['Pendiente', 0],
				]
			}]
		});

	});
</script>
@endsection