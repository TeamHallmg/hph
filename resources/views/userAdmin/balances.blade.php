@extends('layouts.app')

@section('title', 'Administrar Usuarios')

@section('content')

<div class="row mb-3">
    <div class="col-12 blue_header rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> CONTROL DE INCIDENCIAS</h2>
    </div>
</div>

<div class="row mb-2">
    <div class="col-12 py-3">
        <h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid #898788">Resumen de saldos</h3>
    </div>
</div>

@include('vacations.common.ub')

@endsection