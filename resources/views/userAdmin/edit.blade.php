@extends('layouts.app')

@section('title', 'Editar Usuario')

@section('content')

<div class="container my-5">
    <div class="row my-5">
        <div class="col">
                @if($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger alert-dismissible fade show card-1" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <strong>{{$error}}</strong>
                    </div>
                @endforeach
            @elseif(session()->has('success'))
                <div class="alert alert-success alert-dismissible fade show card-1" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <strong>¡El usuario con el RFC: {{$usuario->employee->rfc}} fue editado con éxito!</strong>
                </div>
            @endif
            <div class="card card-3">
                <div class="card-header bg-blue text-white text-center">
                    <h2 class="my-auto font-weight-bold">Editar Usuario</h2>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('admin-de-usuarios.update',$usuario->id) }}" id="update">
                        {!! method_field('PUT') !!}
                        {!! csrf_field() !!}
                        <div class="form-row">
                            {{-- <div class="form-group col-12 col-md-6">
                                <label for="user_type" class="col-form-label font-weight-bold requerido">Tipo de Usuario:</label>
                                <select name="user_type" id="user_type" class="selectpicker form-control" data-live-search="true" title="Elije un tipo de usuario..." required>
                                    <option value="internal" {{ $usuario->employee->external == 0?'selected':'' }}>Interno</option>
                                    <option value="external" {{ $usuario->employee->external == 1?'selected':'' }}>Externo</option>
                                </select>
                            </div> --}}
                            {{--dd($usuario->employee)--}}
                        @foreach($fields as $key => $input)
                            @if($input['column'] == 'fechapuesto')
                            <div class="form-group col-12 col-md-6">
                                <label for="{{$input['column']}}" class="col-form-label font-weight-bold">Fecha de asignación de puesto:</label>
                                <input type="date" class="form-control" name="{{$input['column']}}" id="{{$input['column']}}" value="{{$input['value']}}">
                            </div>
                            @elseif($input['column'] == 'nacimiento' || $input['column'] == 'ingreso')
                            <div class="form-group col-12 col-md-6">
                                <label for="{{$input['column']}}" class="col-form-label font-weight-bold requerido">{{ $input['nameShow'] }}:</label>
                                <input type="date" class="form-control" name="{{$input['column']}}" id="{{$input['column']}}" value="{{$input['value']}}" required>
                            </div>
                            @elseif($input['type'] == 'date')
                            <div class="form-group col-12 col-md-6">
                                <label for="{{$input['column']}}" class="col-form-label font-weight-bold">{{ucfirst($input['column'])}}:</label>
                                <input type="date" class="form-control" name="{{$input['column']}}" id="{{$input['column']}}" value="{{$input['value']}}">
                            </div>
                            @elseif($input['column'] == 'curp')
                            <div class="form-group col-12 col-md-6">
                                <label for="{{$input['column']}}" class="col-form-label font-weight-bold requerido">CURP:</label>
                                <input type="text" class="form-control" name="{{$input['column']}}" id="{{$input['column']}}" value="{{$input['value']}}" required>
                            </div>
                            @elseif($input['column'] == 'idempresa')
                            <div class="form-group col-12 col-md-6">
                                <label for="{{$input['column']}}" class="col-form-label font-weight-bold">{{ucfirst($input['column'])}}:</label>
                                <input type="text" class="form-control" name="{{$input['column']}}" id="{{$input['column']}}" value="{{$input['value']}}" readonly>
                            </div>
                            @elseif($input['column'] == 'password')
                            <div class="form-group col-12 col-md-6">
                                <label for="password" class="col-form-label font-weight-bold">Contraseña:</label>
                                <input type="password" class="form-control" name="{{$input['column']}}" id="{{$input['column']}}" autocomplete="new-password">
                                <input type="text" class="form-control" name="oldPassword" id="oldPassword" value="{{$input['value']}}" readonly hidden>
                            </div>
                            @elseif($input['column'] == 'correoempresa')
                            <div class="form-group col-12 col-md-6">
                                <label for="{{$input['column']}}" class="col-form-label font-weight-bold requerido">Correo Empresarial:</label>
                                <input type="email" class="form-control" name="{{$input['column']}}" id="{{$input['column']}}" value="{{$input['value']}}" required>
                                <input type="email" class="form-control" name="old_email" id="old_email" value="{{$input['value']}}" readonly hidden>
                            </div>
                            @elseif($input['column'] == 'correopersonal')
                            <div class="form-group col-12 col-md-6">
                                <label for="{{$input['column']}}" class="col-form-label font-weight-bold">Correo Personal:</label>
                                <input type="email" class="form-control" name="{{$input['column']}}" id="{{$input['column']}}" value="{{$input['value']}}">
                            </div>
                            @elseif($input['column'] == 'rol')
                            <div class="form-group col-12 col-md-6">
                                <label for="{{$input['column']}}" class="col-form-label font-weight-bold">Rol:</label>
                                <select class="custom-select" name="{{$input['column']}}">
                                    <option value="employee" 
                                    @if($input['value'] == 'employee' || $input['value'] == '' || $input['value'] == null)
                                    selected
                                    @endif
                                    >Empleado</option>
                                </select>
                            </div>
                            @elseif($input['column'] == 'sexo')
                                <div class="form-group col-12 col-md-6">
                                    <label for="{{$input['column']}}" class="col-form-label font-weight-bold">Sexo:</label>
                                    <select class="custom-select" name="{{$input['column']}}">
                                        <option value="F"
                                        @if($input['value'] == 'F')
                                        selected
                                        @endif
                                        >F</option>
                                        <option value="M" 
                                        @if($input['value'] == 'M')
                                        selected
                                        @endif
                                        >M</option>
                                    </select>
                                </div>
                            @elseif($input['column'] == 'direccion')
                                {{-- <div class="form-group col-12 col-md-6">
                                    <label for="direccion" class="col-form-label font-weight-bold">Dirección:</label>
                                    <select name="{{$input['column']}}" id="direction" class="selectpicker form-control" data-live-search="true" title="Elije una Dirección...">
                                        @foreach($direcciones as $id => $direction)
                                            <option value="{{$id}}" {{ $id==$input['value'] ? 'selected' : '' }}>{{$direction}}</option>
                                        @endforeach
                                    </select>
                                </div> --}}
                            @elseif($input['column'] == 'division')
                                {{-- <div class="form-group col-12 col-md-6">
                                    <label for="division" class="col-form-label font-weight-bold">Departamento:</label>
                                    <select name="division" id="division" class="selectpicker form-control" data-live-search="true" title="Elije una departamento...">
                                        @foreach($departamentos as $id => $depto)
                                            <option value="{{ $id }}" {{ $id==$input['value'] ? 'selected' : '' }}>{{$depto}}</option>
                                        @endforeach
                                    </select>
                                </div> --}}
                            @elseif($input['column'] == 'seccion')
                                {{-- <div class="form-group col-12 col-md-6">
                                    <label for="seccion" class="col-form-label font-weight-bold">Área:</label>
                                    <select name="seccion" id="seccion" class="selectpicker form-control" data-live-search="true" title="Elije un área...">
                                        @foreach($areas as $id => $area)
                                            <option value="{{ $id }}" {{ $id==$input['value'] ? 'selected' : '' }}>{{$area}}</option>
                                        @endforeach
                                    </select>
                                </div> --}}
                            {{-- @elseif($input['column'] == 'job_position_id') --}}
                                <div class="form-group col-12 col-md-6">
                                    <label for="jobPosition" class="col-form-label font-weight-bold requerido">Estructura <span class="text-muted">(Puesto)</span></label>
                                    <select name="job_position_id" id="jobPosition" class="selectpicker form-control" data-live-search="true" title="Elije un puesto por departamento..." required>
                                        @foreach($puestos as $puesto)
                                        <option value="{{$puesto->id}}" {{(isset($usuario->employee->jobPosition)?($usuario->employee->jobPosition->id == $puesto->id ? 'selected' : '' ) : '')}}>
                                            {{-- {{ $puesto->getDepartmentName() }} - {{ $puesto->name }} --}}
                                            {{ $puesto->getStructureNames() ?? 'SIN ESTRUCTURA' }} / {{ $puesto->name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            @elseif($input['column'] == 'jefe')
                                <div class="form-group col-12 col-md-6">
                                    <label for="{{$input['column']}}" class="col-form-label font-weight-bold">Jefe:</label>
                                    <select class="selectpicker form-control" data-live-search="true" title="Elije un Jefe..." name="{{$input['column']}}">
                                        @foreach($users as $user)
                                        <option value="{{$user->employee->idempleado}}" data-subtext="{{$user->employee->idempleado}}" {{($user->employee->idempleado == $usuario->employee->jefe?'selected':'')}}>
                                                {{$user->employee->nombre.' '.$user->employee->paterno.' '.$user->employee->materno}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            @elseif($input['column'] == 'enterprise_id')
                                <div class="form-group col-12 col-md-6">
                                    <label for="{{$input['column']}}" class="col-form-label font-weight-bold">Empresa:</label>
                                    <select class="selectpicker form-control" data-live-search="true" title="Elije una empresa..." name="{{$input['column']}}">
                                        @foreach($enterprises as $enterprise)
                                            <option value="{{$enterprise->id}}" {{($usuario->employee->enterprise_id == $enterprise->id?'selected':'')}}>{{$enterprise->name}}</option>
                                        @endforeach
                                    </select>
                                    
                                </div>
                            @elseif($input['column'] == 'region_id')
                                <div class="form-group col-12 col-md-6">
                                    <label for="region_id" class="col-form-label font-weight-bold">{{$input['nameShow']}}:</label>
                                    <select name="{{$input['column']}}" id="region_id" class="selectpicker form-control" data-live-search="true" title="Elije una Región...">
                                        @foreach($regions as $region)
                                        <option value="{{$region->id}}" {{($usuario->employee->region_id == $region->id?'selected':'')}}>
                                            {{$region->name}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            @elseif($input['column'] == 'seccion' || $input['column'] == 'division')
                            @else
                                <div class="form-group col-12 col-md-6">
                                    <label for="{{$input['column']}}" class="col-form-label font-weight-bold {{$input['class']}}">{{$input['nameShow']}}:</label>
                                    <input type="text" class="form-control" name="{{$input['column']}}" id="{{$input['column']}}" value="{{$input['value']}}" {{$input['mandatory']}}>
                                </div>
                            @endif
                            
                            @endforeach
                        </div>
                        <div class="row justify-content-end">
                            <div class="form-group col col-md-6 mt-2 text-right">
                                <a href="{{asset('admin-de-usuarios')}}" class="btn btn-secondary mb-md-0 card-1"><i class="fas fa-arrow-alt-circle-left"></i> Volver</a>
                                <button type="button" class="btn btn-primary text-white" data-toggle="modal" data-target="#modalPassword" ><i class="fas fa-key"></i> Actualizar Contraseña</button>
                                <button type="submit" class="btn btn-success card-1"><i class="fas fa-save"></i> Actualizar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalPassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form method="post" id="updtPassword" action="{{ url('update_password') }}" enctype="multipart/form-data" autocomplete="off">
                @csrf
                <div class="modal-header bg-blue">
                    <h5 class="modal-title text-white" id="exampleModalLabel">Actualizar Contraseña</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="user_id" value="{{ $usuario->id }}">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="password" class="requerido">Nueva Contraseña</label>
                                <input type="password" class="form-control" name="password" placeholder="Contraseña..." minlength="6" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="confirm_password" class="requerido">Confirmar Contraseña</label>
                                <input type="password" class="form-control" name="confirm_password" placeholder="Contraseña..." minlength="6" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary text-white" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-success text-white">Confirmar</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
{{-- @section('scripts')
<script>
    function getDepartments(id){
        $.ajax({
            url: "{{url('departments')}}/"+id,
            Type:'GET',
            success: function(result){

                $('#area').empty();
                $('#area').selectpicker('refresh');
                $('#department').empty();

                $.each(result, function(i, item){
                    if("{{isset($job->area->department->id)?$job->area->department->id:''}}" == item.id){
                        $('#department').append($('<option>', { 
                            value: item.id,
                            text : item.name 
                        })).selectpicker('refresh');
                        $('#department').selectpicker('val',item.id);
                        getAreas("{{isset($job->area->department_id)?$job->area->department_id:''}}");
                        getJobs("{{isset($job->area_id)?$job->area_id:''}}");
                    }else{
                        $('#department').append($('<option>', {
                            value: item.id,
                            text : item.name 
                        })).selectpicker('refresh');
                    }  
                });
            }
        });
    }

    $('#direction').change(function(){
        var id = $(this).val();
        getDepartments(id);

        $('#jobPosition').empty();
        $('#jobPosition').selectpicker('refresh');
    });

    function getAreas(id){
        //console.log('Holi');
        $.ajax({
            url: "{{url('area')}}/"+id,
            Type:'GET',
            success: function(result){
                $('#area').empty();
                $.each(result, function(i, item){
                    if("{{isset($job->area->id)?$job->area->id:''}}" == item.id){
                        $('#area').append($('<option>', { 
                        value: item.id,
                        text : item.name 
                        })).selectpicker('refresh');
                        $('#area').selectpicker('val',item.id);
                        getJobs("{{isset($job->area_id)?$job->area_id:''}}");
                    }else{
                        $('#area').append($('<option>', { 
                        value: item.id,
                        text : item.name 
                        })).selectpicker('refresh');
                    }
                   
                })
                
            }
        });
    }

    $('#department').change(function(){
        var id = $(this).val();
        getAreas(id);
    });

    function getJobs(id){
        $.ajax({
            url: "{{url('jobpositions')}}/"+id,
            Type:'GET',
            success: function(result){
                //console.log(id,result);
                $('#jobPosition').empty();
                $.each(result, function(i, item){
                    if("{{isset($job->id)?$job->id:''}}" == item.id){
                        $('#jobPosition').append($('<option>', { 
                        value: item.id,
                        text : item.name 
                        })).selectpicker('refresh');
                        $('#jobPosition').selectpicker('val',item.id);
                    }else{
                        $('#jobPosition').append($('<option>', { 
                        value: item.id,
                        text : item.name 
                        })).selectpicker('refresh');
                    }
                });
            }
        });
    }

    $('#area').change(function(){
        var id = $(this).val();
        getJobs(id);
    });

    $(document).ready(function(){
        getJobs("{{isset($job->area_id)?$job->area_id:''}}");
        getDepartments("{{isset($job->area->department->direction->id)?$job->area->department->direction->id:''}}");
        getAreas("{{isset($job->area->id)?$job->area->id:''}}");
    });
</script>
@endsection --}}