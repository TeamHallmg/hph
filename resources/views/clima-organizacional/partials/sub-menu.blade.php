<h4 class="font-weight-bold  color-personalizado-azul" style="margin-top: 15px;">Clima Laboral</h4>
<hr>
<ul class="nav nav-pills flex-column">
	<li class="nav-item"><a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('clima-organizacional/que-es') }}">Información General</a></li>
	<li class="nav-item">

		@if (auth()->user()->role != 'admin' && auth()->user()->role != 'supervisor')
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('clima-organizacional/evaluaciones') }}">Encuesta</a></li>
		@endif
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_encuestas']))
     		<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('clima-organizacional/encuestas') }}">Preguntas Abiertas</a>
		@endif
		@if (auth()->user()->role == 'admin' || auth()->user()->role == 'supervisor' || isset($userPermissions['see_progress']) || auth()->user()->hasClimaPermissions(2))
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('clima-organizacional/avances') }}">Avances</a></li>
		@endif
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_reports']) || auth()->user()->hasClimaPermissions(3))
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('clima-organizacional/reporte-grafico') }}">Reportes</a>
		@endif
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_reports']) || auth()->user()->hasClimaPermissions(4))
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('clima-organizacional/heat-report') }}">Mapa de Calor</a>
		@endif
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_reports']) || auth()->user()->hasClimaPermissions(5))
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('clima-organizacional/heat-report-by-regions') }}">Mapa de Calor por Hospitales</a>
		@endif
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_reports']) || auth()->user()->hasClimaPermissions(5))
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('clima-organizacional/heat-report-by-supervisor') }}">Mapa de Calor por Supervisor</a>
		@endif
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_reports']) || auth()->user()->hasClimaPermissions(5))
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('clima-organizacional/heat-report-by-gerente') }}">Mapa de Calor por Gerente</a>
		@endif
			<!--<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('clima-organizacional/heat-report-by-areas-groups') }}">Mapa de Calor 2</a>-->
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_factors']) || auth()->user()->hasClimaPermissions(6))
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('clima-organizacional/factores') }}">Dimensiones</a></li>
		@endif
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_questions']) || auth()->user()->hasClimaPermissions(7))
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('clima-organizacional/preguntas') }}">Preguntas</a></li>
			@endif
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_questions']) || auth()->user()->hasClimaPermissions(8))
		<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('clima-organizacional/etiquetas') }}">Etiquetas</a></li>
		@endif
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_periods']) || auth()->user()->hasClimaPermissions(9))
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('clima-organizacional/periodos') }}">Periodos</a></li>
		@endif
		@if (auth()->user()->role == 'admin')
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('clima-organizacional/permissions') }}">Permisos</a></li>
		@endif

		@if (auth()->user()->role == 'admin' || auth()->user()->role == 'supervisor' || auth()->user()->hasClimaPermissions(10))
		<hr>
		<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('clima-organizacional/lista-empleados') }}">Listado de Colaboradores</a></li>
		@endif
		@if (auth()->user()->role == 'admin' || auth()->user()->hasClimaPermissions(11))
		<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('clima-organizacional/grupos_areas') }}">Grupos Areas</a></li>
		@endif
		@if (auth()->user()->role == 'admin' || auth()->user()->hasClimaPermissions(12))
		<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('clima-organizacional/grupos_departamentos') }}">Grupos Departamentos</a></li>
		@endif
		@if (auth()->user()->role == 'admin' || auth()->user()->hasClimaPermissions(13))
		<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('clima-organizacional/grupos_puestos') }}">Grupos Puestos</a></li>
		@endif
		@if (auth()->user()->role == 'admin')
		<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('clima-organizacional/invitaciones-masivas') }}">Invitaciones Masivas</a></li>
		@endif
		@if (auth()->user()->role == 'admin' || auth()->user()->hasClimaPermissions(14))
		<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('clima-organizacional/plan-correos') }}">Plan de Correos</a></li>
		@endif
	{{-- @endif --}}
</ul>