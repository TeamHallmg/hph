@extends('layouts.app')

@section('title', 'Importar Evaluador/Evaluado por Archivo')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10 form_container">
		<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }}" alt="">
		<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Importador de usuarios</h3>
		<hr>

		{!! Form::open(['method' => 'POST', 'onSubmit' => 'return validaFormulario()', 'files' => true]) !!}
			<div class="row margin-top-20">
				<div class="col-md-12 margin-top-20 text-center">
					<div class="text-center">
						<span>Periodo</span>
						<select class="periodo_matriz form-control" style="display: inline-block; width: auto">

						@foreach ($periodos as $periodo)

							<option value="{{$periodo->id}}" <?php if (!empty($_GET['id_periodo']) && $periodo->id == $_GET['id_periodo']){ ?>selected="selected"<?php } ?>>{{$periodo->descripcion}}</option>
						@endforeach
						</select>
					</div>
					<br>
					<br>
					<div class="col-md-4">
						{!! Form::hidden('mostrar', 'mostrar', ['class' => 'form-control']) !!}
						{!! Form::hidden('id_periodo', (!empty($_GET['id_periodo']) ? $_GET['id_periodo'] : (!empty($_POST['id_periodo']) ? $_POST['id_periodo'] : $periodos[0]->id)), ['class' => 'form-control']) !!}
					</div>
					<div class="col">
						{!! Form::file('archivo', ['class' => 'archivo_import', 'required' => 'required']) !!}
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col text-center">
					<div class="col my-4">
						{{ Form::button('<i class="far fa-eye"></i> Mostrar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
					</div>
				</div>
			</div>
		{!! Form::close() !!}
		<br>
		<br>
		{{--@if(!empty($csvData) || $countCsvData > 1)
			<div class="col-md-12">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					{!! Form::select('accion', ['reemplazar' => 'Reemplazar', 'nuevo' => 'Cargar Nuevo Archivo'], 'reemplazar', ['class' => 'form-control']) !!}
				</div>
				<div class="col-md-4"></div>
				<br>
				<br>
				<table width="100%" style="font-size: 13px">
					<thead>
						<tr>
							<th style="background-color: #555759; color: white; padding: 5px 10px; text-align: center; border: 1px solid white;">No. de Filas</th>
							<th style="background-color: #555759; color: white; padding: 5px 10px; text-align: center; border: 1px solid white;">ID Evalaudor</th>
							<th style="background-color: #555759; color: white; padding: 5px 10px; text-align: center; border: 1px solid white;">Nombre Evaluador</th>
							<th style="background-color: #555759; color: white; padding: 5px 10px; text-align: center; border: 1px solid white;">ID Evaluado</th>
							<th style="background-color: #555759; color: white; padding: 5px 10px; text-align: center; border: 1px solid white;">Nombre Evaluado</th>
						</tr>
					</thead>
					<tbody>
						@foreach($csvData as $key => $data)
							@if($key != 0)
								<tr>
									<td>{{ $key }}</td>
									@foreach($usuarios as $user)
										@if($data[0] == $user->id)
											<td>{{ $data[0] }}</td>
											<td>{{ $user->first_name }} {{ $user->last_name }}</td>
										@endif
									@endforeach
									@foreach($usuarios as $user)
										@if($data[1] == $user->id)
											<td>{{ $data[1] }}</td>
											<td>{{ $user->first_name }} {{ $user->last_name }}</td>
										@endif
									@endforeach
								</tr>
							@endif
						@endforeach
					</tbody>
				</table>
			</div>
		@endif--}}
	</div>
</div>
<form action="/importacion-evaluadores-csv" method="get" class="matriz_evaluacion">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="hidden" name="id_periodo" value="<?php echo (!empty($_GET['id_periodo']) ? $_GET['id_periodo'] : (!empty($_POST['id_periodo']) ? $_POST['id_periodo'] : $periodos[0]->id))?>" class="id_periodo">
  <input type="submit" style="display: none">
</form>

<!-- Modal -->
<div id="modalContainer" class="modal fade">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          
          <h4 class="modal-title text-center text-warning"><span class="glyphicon glyphicon-warning-sign"></span>&nbsp;&nbsp;&nbsp;ADVERTENCIA</h4>
        </div>
        <div class="modal-body">
          <p id="modalMessage"></p>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
@endsection

@section('scripts')
<script>

	function validaFormulario(){

		var archivo = $('.archivo_import').val();

		if(archivo == ""){
			$('#modalMessage').html('No debe de estar el campo vacio del archivo.');
			$('#modalContainer').modal('show');
			return false;
		}else{
			return true;
		}
	}

	/*$('button.agregar_archivo').on('click', function(e) {
		e.preventDefault();

		var archivo = $('.archivo_import').val();

		var btn = $(this);
		var form = $('#importar-evaluador-evaluado');
		var url = form.attr('action');
		var data = form.serialize();


		if(archivo == ""){
			$('#modalMessage').html('No debe de estar el campo vacio del archivo.');
			$('#modalContainer').modal('show');
		}else{
			$.post(url, data, function (reply){
					if(reply.success){
						/*$('#modalMessage').html(reply.msn);
						$('#modalContainer').modal('show');
						location.reload(true);
					}else{
						$('#modalMessage').html(reply.msn);
						$('#modalContainer').modal('show');
					}
				});
		}
	});*/

	$(document).ready(function(){

		$('select.periodo_matriz').change(function(){

			window.location = 'importacion-evaluadores-csv?id_periodo=' + $(this).val();
		});

		$('.estadisticas').DataTable({
      'order': [[0,'asc']]
   	});
  });
</script>
@endsection