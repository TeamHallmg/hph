@extends('clima-organizacional.app')

@section('title', 'Dimensiones')

@section('content')

<div class="row mt-3">
	<div class="col-md-2 text-right">
		@include('clima-organizacional/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
<h3 class="titulos-evaluaciones mt-4 font-weight-bold">Dimensiones</h3>
<hr>
<a href="{{ url('clima-organizacional/factores/create') }}" id='agregarFactor' class="btn btn-success"><span class="fas fa-plus-circle"></span> Agregar Dimensión</a>

<div class="my-3">
	<table class="table table-striped" id="factores">
		<thead class="bg-purple text-white">
			<tr>
				<th>ID</th>
				<th>Nombre</th>
				<th>Descripción</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>

			@foreach ($factores as $factor)
				
				<tr>
					<td>{{$factor->id}}</td>
					<td>{{$factor->name}}</td>
					<td>{{$factor->description}}</td>
					<td>
						<a href="{{ url('clima-organizacional/factores/' . $factor->id . '/edit') }}" class="btn btn-primary"><i class="fas fa-edit"></i> Editar</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>
</div>
</div>

@endsection
@section('scripts')
<script>

	$(document).ready(function(){

		$('#factores').DataTable({
			language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
    	}
		});
	});
</script>
@endsection