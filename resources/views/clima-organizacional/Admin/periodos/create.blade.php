@extends('clima-organizacional.app')

@section('title', 'Crear Periodo')

@section('content')

<div class="row mt-3">
  <div class="col-md-2 text-right">
    @include('clima-organizacional/partials/sub-menu')
  </div>
  <div class="col-md-10">
    <img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
<h3 class="titulos-evaluaciones mt-4 font-weight-bold">Periodos</h3>
<hr>

<form action="{{ url('clima-organizacional/periodos') }}" method="post" class="periodos">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="card">
        <div class="card-header">
            Crear Periodo
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="nombre" class="font-weight-bold requerido">Nombre</label>
                        <input type="text" class="form-control" name="name" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="estado" class="font-weight-bold">Estado</label>
                        <select class="form-control" name="status">
                        <option value="Preparatorio">Preparatorio</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label for="no_aplica" class="font-weight-bold">No aplica (N/A) aparece en la encuesta</label>
                <select class="form-control" name="no aplica">
                  <option value="0">No</option>
                  <option value="1">Si</option>
                </select>
              </div>
                <div class="col-md-6">

                  @if (count($enterprises) > 0)

                    <div class="form-group">
                        <label for="enterprise_id" class="font-weight-bold">Empresa</label>
                        <select class="form-control enterprises" name="enterprise_id">
                          <option value="0">-- Todas --</option>

                    @foreach ($enterprises as $key => $enterprise)
                    
                          <option value="{{$enterprise->id}}">{{$enterprise->name}}</option>
                    @endforeach

                        </select>
                    </div>
                  @endif

                </div>
            </div>
    </div>

    <h3 class="titulos-evaluaciones mt-4 font-weight-bold">Preguntas</h3>
<hr>
    <table class="table table-striped" id="table-preguntas">
        <thead>
            <tr>
            <th>ID</th>
            <th>Pregunta</th>
            <th>Dimensión</th>
            <th class="mx-auto">Todas <input type="checkbox" class="select_all"></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($preguntas as $pregunta)
            <tr>
                <td>{{ $pregunta->id }}</td>
                <td>{{ $pregunta->question }}</td>
                <td>{{ (!empty($pregunta->factor) ? $pregunta->factor->name : '') }}</td>
                <td>
                <input type="checkbox" value="{{ $pregunta->id }}" class="preguntas">
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <h3 class="titulos-evaluaciones mt-4 font-weight-bold">Evaluadores</h3>
<hr>
    <table class="table table-striped" id="table-evaluados">
        <thead>
            <tr>
            <th>ID</th>
            <th>Evaluado</th>
            <th>Empresa</th>
            <th>Departamento</th>
            <th>Área</th>
            <th>Puesto</th>
            <th>Jefe</th>
            <th class="mx-auto">Todos <input type="checkbox" class="select_all_evaluados"></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($evaluados as $key => $evaluado)
            <tr>
                <td>{{ $evaluado->employee->idempleado }}</td>
                <td>{{ $evaluado->fullname }}</td>
                <td>{{ (isset($evaluado->employee->enterprise) ? $evaluado->employee->enterprise->name : '') }}</td>
                <td>{{ (!empty($evaluado->employee->jobPosition) && !empty($evaluado->employee->jobPosition->area) && !empty($evaluado->employee->jobPosition->area->department) ? $evaluado->employee->jobPosition->area->department->name : '') }}</td>
                <td>{{ $evaluado->employee->direccion }}</td>
                <td>{{ (!empty($evaluado->employee->jobPosition) ? $evaluado->employee->jobPosition->name : '') }}</td>
                <td>{{ (isset($evaluado->employee->boss->user) ? $evaluado->employee->boss->user->fullname : '') }}</td>
                <td>
                <input type="checkbox" value="{{ $evaluado->id }}" class="evaluados">
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-12">
            <div class="float-right">
                <button class="btn btn-success mr-3" type="submit" style="color: white"><span class="fas fa-check-circle"></span> Crear</button> 
                <a class="btn btn-danger" href="{{ url('clima-organizacional/periodos') }}"><span class="fas fa-times-circle"></span> Regresar</a>
            </div>
        </div>
    </div>
</form>
</div>
</div>

@endsection

@section('scripts')
  <script>

    var questions_table = '';
    var users_table = '';
	  
    $(document).ready(function(){

      questions_table = $('#table-preguntas').DataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
      });

      users_table = $('#table-evaluados').DataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
      });

        $('.select_all').click(function(){
          //var cells = questions_table.cells().nodes();
          var cells = questions_table.rows({ search: 'applied' }).nodes();
            if ($(this).prop('checked')){
                //$('.preguntas').prop('checked', true);
                $(cells).find(':checkbox').prop('checked', true);
            }else{
                //$('.preguntas').prop('checked', false);
                $(cells).find(':checkbox').prop('checked', false);
            }
        });

        $('.select_all_evaluados').click(function(){
          //var cells = users_table.cells().nodes();
          var cells = users_table.rows({ search: 'applied' }).nodes();
            if ($(this).prop('checked')){
                //$('.evaluados').prop('checked', true);
                $(cells).find(':checkbox').prop('checked', true);
            }else{
                //$('.evaluados').prop('checked', false);
                $(cells).find(':checkbox').prop('checked', false);
            }
        });

        // Handle form submission event
   $('form.periodos').on('submit', function(e){
      var form = this;

      //checkboxes should have a general class to traverse
      var rowcollection = questions_table.$(".preguntas:checked", {"page": "all"});

      var checkbox_value = 0;

      //Now loop through all the selected checkboxes to perform desired actions
      rowcollection.each(function(index,elem){
      //You have access to the current iterating row
        checkbox_value = $(elem).val();
        $(form).append(
             $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'questions[]')
                .val(checkbox_value)
         );
      });

      rowcollection = users_table.$(".evaluados:checked", {"page": "all"});

      //Now loop through all the selected checkboxes to perform desired actions
      rowcollection.each(function(index,elem){
      //You have access to the current iterating row
        checkbox_value = $(elem).val();
        $(form).append(
             $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'users[]')
                .val(checkbox_value)
         );
      });
   });

   $('select.enterprises').change(function(){

      var enterprise_id = $(this).val();
      var enterprise_name = $(this).find('option[value="' + enterprise_id + '"]').text();

      if (enterprise_name == '-- Todas --'){

        users_table.column(2).search('').draw();
      }

      else{

        users_table.column(2).search("^" + enterprise_name + "$", true, false, true).draw();
      }
    });
  });
  </script>
@endsection
