@extends('layouts.app')

@section('title', 'Matriz de Competencias')

@section('content')

<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	
		<div class="col-md-10">

			<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }} " alt="">
			<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Matriz de Competencias</h3>
			<hr class="mb-5">

			<table class="table table-striped table-bordered list dt-responsive">
				<thead style="background-color: #222B64; color:white;">
					<tr>
						<th>#</th>
						<th>Nombre</th>
						<th>Descripción</th>
						<th>Nombre para Organización</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>

				@for ($i = 0; $i < count($familias_factores); $i++)

					<tr>
						<td class="text-center">{{$familias_factores[$i]->id}}</td>
						<td class="text-center">{{$familias_factores[$i]->nombre}}</td>
						<td class="text-center">{{$familias_factores[$i]->descripcion}}</td>
						<td class="text-center">
							<span class="nombre_de_organizacion">{{$familias_factores[$i]->nombre_para_organizacion}}</span>
							<input type="text" class="nombre_para_organizacion" value="{{$familias_factores[$i]->nombre_para_organizacion}}" style="display: none">
						</td>
						<td class="text-center">
							<button type="button" class="btn btn-primary editar"><i class="fas fa-edit"></i> Editar</button>
							<form method="post" action="editar-familia-factores" class="familia_factores_form">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="hidden" name="id" value="{{$familias_factores[$i]->id}}">
								<input type="hidden" name="nombre_para_organizacion" class="nombre_organizacion">
								<button type="submit" class="btn btn-success" style="display: none"><i class="fas fa-check-circle"></i> Guardar</button>
							</form>
						</td>
					</tr>
				@endfor
				</tbody>
			</table>
		</div>
	</div>
@endsection

@section('scripts')
	<script>

	$('.list').DataTable({
		language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
   	});
		
	$(document).ready(function(){

    $('body').on('click', 'button.editar', function(){

      	$('input.nombre_para_organizacion').hide();
      	$('span.nombre_de_organizacion').show();
      	$('button[type="submit"]').hide();
      	$('button[type="button"]').show();
      	$(this).parent().parent().find('span.nombre_de_organizacion').hide();
      	$(this).parent().parent().find('input.nombre_para_organizacion').show();
      	$(this).hide();
      	$(this).parent().parent().find('button[type="submit"]').show();
    });

      $('form.familia_factores_form').submit(function(){

				$(this).find('.nombre_organizacion').val($(this).parent().parent().find('.nombre_para_organizacion').val());
			});
		});
	</script>
@endsection
