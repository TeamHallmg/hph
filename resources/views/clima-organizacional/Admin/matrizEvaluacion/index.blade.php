@extends('layouts.app')

@section('title', 'Red de Interacción')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }}" alt="">
		<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Red de Interacción</h3>
		<hr>

	
	<div class="col-md-12">
		<span>Periodo</span>
		<select class="periodo_matriz form-control" style="display: inline-block; width: auto">

		@foreach ($periodos as $periodo)

			<option value="{{$periodo->id}}" <?php if ($periodo->id == $_GET['id_periodo']){ ?>selected="selected"<?php } ?>>{{$periodo->descripcion}}</option>
		@endforeach
		</select>
	</div>

<div class="row">
	<div class="col-md-12 text-center my-4">
		<a href="" id='agregarEvaluadorEvaluado' class="btn btn-primary"><i class="fas fa-plus-circle"></i> Agregar Evaluador/Evaluado</a>
	</div>
	<br>
	<br>
{{-- , 'onSubmit' => 'return validaFormulario()' --}}

	<div class="row mx-auto" id="formEvaluacion">
		{!! Form::open(['id' => 'form-evaluador-evaluado', 'action' => 'EvaluacionDesempeno\Admin\MatrizEvaluacionController@store', 'method' => 'POST']) !!}
		<div class="row">
			<div class="col form-group" align="center">
				
					<label>Evaluador</label>
      		<br>
      		<!--<select class="selectpicker" data-show-subtext="true" data-live-search="true" name="evaluador" id="idEvaluador">-->
			<select class="form-control" name="evaluador" id="idEvaluador">
      			<option data-subtext="" value="0">Selecciona una opcion</option>
						@foreach($usuarios as $user)
							@if($user->id == 1)
							@else
								<option data-subtext="{{ $user->id }}" value="{{ $user->id }}">{{ $user->fullname }}</option>
							@endif
						@endforeach
	      	</select>
	    	
			</div>
			<div class="col form-group" align="center">
				
					<label>Evaluado</label>
					<br>
      		<!--<select class="selectpicker" data-show-subtext="true" data-live-search="true" name="evaluado" id="idEvaluado">-->
			<select class="form-control" name="evaluado" id="idEvaluado">
      			<option data-subtext="" value="0">Selecciona una opcion</option>
					@foreach($usuarios as $user)
						@if($user->id == 1)
						@else
						<option data-subtext="{{ $user->id }}" value="{{ $user->id }}">{{ $user->fullname }}</option>
						@endif
					@endforeach
	      	</select>
	    	
			</div>
			
		</div>
		<div class="form-group" align="center">
				<input type="hidden" name="periodo" value="{{$_GET['id_periodo']}}">
				{{--  
				{!! Form::submit('Guardar Evaluador/Evaluado', ['class' => 'btn btn-primary'])!!}
				--}}
				<button class="btn btn-primary btn-Finvivir-yellow agregar_eval" value="agregarEval" name="btnMatrizEval"><i class="fas fa-plus-circle"></i> Agregar Evaluador/Evaluado</button>
			</div>
		{!! Form::close() !!}
	</div>
		<br>
		<br>
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover matrizEvaluacion dt-responsive">
			<thead style="background-color: #222B64; color:white;">
				<tr>
					<th>ID Evaluador</th>
					<th>Nombre Evaluador</th>
					<th>ID Evaluado</th>
					<th>Nombre Evaluado</th>
					<th>Periodo</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				@if(empty($matrizEva) || is_null($matrizEva) || $countPeriodos == 0)
					<tr>
						<td colspan="6" class="text-center" style="padding: 10px 5px">Esta sección solo esta disponible cuando hay periodos Preparatorios o Abiertos</td>
					</tr>
				@else
					@foreach($matrizEva as $matriz)
						<tr>
							<td class="text-center" style="padding: 10px 5px">
								{{ $matriz->id_evaluador }}
							</td>
							<td class="text-center" style="padding: 10px 5px">
								{{ (!empty($matriz->nameCompleteUserEvaluador->fullname) ? str_replace('Ã‘','Ñ',$matriz->nameCompleteUserEvaluador->fullname) : '') }}
							</td>
							<td class="text-center" style="padding: 10px 5px">
								{{ $matriz->id_evaluado }}
							</td>
							<td class="text-center" style="padding: 10px 5px">
								{{ (!empty($matriz->nameCompleteUserEvaluado->fullname) ? str_replace('Ã‘','Ñ',$matriz->nameCompleteUserEvaluado->fullname) : '')}}
							</td>
							<td class="text-center" style="padding: 10px 5px">
								{{ $matriz->periodoName->Name }}
							</td>
							<td class="text-center" style="padding: 10px 5px">
								<div class="row">
									<!--<div class="col-md-6" align="center">
										<form action="{{'/Matriz-Evaluacion/' . $matriz->id . '/edit'}}" method="post">
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<input type="hidden" value="{{$_GET['id_periodo']}}" name="id_periodo">
											<input type="submit" class="btn btn-primary" value="Editar">
										</form>-->
										<!--<a href="{{ url('Matriz-Evaluacion/' . $matriz->id . '/edit') }}" class="btn btn-primary btn-Finvivir-yellow">Editar</a>-->
									<!--</div>-->
									<div class="col-md-12" align="center">
										{!! Form::open(['route' => ['Matriz-Evaluacion.destroy', $matriz->id], 'method' => 'DELETE'])!!}
										@if ($matriz->status == 'No Iniciada')
                			{!! Form::button('<i class="fas fa-trash-alt"></i>', array('class'=>'btn btn-danger', 'type'=>'submit', 'onclick' => 'return confirm("¿Desea eliminarla?")')) !!}
                		@else
                			{!! Form::button('<i class="fas fa-trash-alt"></i>', array('class'=>'btn btn-danger', 'type'=>'submit', 'onclick' => 'return confirm("Esta evaluación ya ha sido iniciada. ¿Desea eliminarla?")')) !!}
                		@endif	
              			{!! Form::close() !!}
              		</div>
								</div>
							</td>
						</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
</div>
<form action="/Matriz-Evaluacion" method="get" class="matriz_evaluacion">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="hidden" name="id_periodo" value="<?php echo $_GET['id_periodo']?>" class="id_periodo">
  <input type="submit" style="display: none">
</form>

<!-- Modal -->
<div id="modalContainer" class="modal fade">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          
          <h4 class="modal-title text-center text-warning"><span class="glyphicon glyphicon-warning-sign"></span>&nbsp;&nbsp;&nbsp;ADVERTENCIA</h4>
        </div>
        <div class="modal-body">
          <p id="modalMessage"></p>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
</div>
</div>
@endsection
@section('scripts')
<script>
	var contPeriodos = {{ $countPeriodos }};

	if(contPeriodos == 0){
		$('#agregarEvaluadorEvaluado').hide();
	}

	$('button.agregar_eval').on('click', function(e) {
		e.preventDefault();

		var evaluador = $('select[name="evaluador"]').val();
		var evaluado = $('select[name="evaluado"]').val();
		var periodo = $('select[name="periodo"]').val();

		var btn = $(this);
		var form = $('#form-evaluador-evaluado');
		var url = form.attr('action');
		var data = form.serialize();

		if(contPeriodos > 1){
			if(periodo == 0){
				$('#modalMessage').html('Debes seleccionar un periodo.');
				$('#modalContainer').modal('show');
			}else if(evaluador == 0){
				$('#modalMessage').html('Debes seleccionar a un evaluador.');
				$('#modalContainer').modal('show');
			}else if(evaluado == 0){
				$('#modalMessage').html('Debes seleccionar a un evaluado.');
				$('#modalContainer').modal('show');
			}else{
				$.post(url, data, function (reply){
					if(reply.success){
						//$('#modalMessage').html(reply.msn);
						//$('#modalContainer').modal('show');
						alert('Se agregó la evaluación');
						location.reload(true);
					}else{
						$('#modalMessage').html(reply.msn);
						$('#modalContainer').modal('show');
					}
				});
			}
		}else{
			if(evaluador == 0){
				$('#modalMessage').html('Debes seleccionar a un evaluador.');
				$('#modalContainer').modal('show');
			}else if(evaluado == 0){
				$('#modalMessage').html('Debes seleccionar a un evaluado.');
				$('#modalContainer').modal('show');
			}else{
				$.post(url, data, function (reply){
					if(reply.success){
						//$('#modalMessage').html(reply.msn);
						//$('#modalContainer').modal('show');
						alert('Se agregó la evaluación');
						location.reload(true);
					}else{
						$('#modalMessage').html(reply.msn);
						$('#modalContainer').modal('show');
					}
				});
			}
		}

				/*if(periodo == 0 && contPeriodos > 1){
					$('#modalMessage').html('Debes seleccionar un periodo.');
					$('#modalContainer').modal('show');
				}else if(evaluador == 0){
					$('#modalMessage').html('Debes seleccionar a un evaluador.');
					$('#modalContainer').modal('show');
				}else if(evaluado == 0){
					$('#modalMessage').html('Debes seleccionar a un evaluado.');
					$('#modalContainer').modal('show');
				}else{
					$.post(url, data, function (reply){
						if(reply.success){
							$('#modalMessage').html(reply.msn);
							$('#modalContainer').modal('show');
							location.reload(true);
						}else{
							$('#modalMessage').html(reply.msn);
							$('#modalContainer').modal('show');
						}
					});
				}*/
		});

	$(document).ready(function(){
		$('#formEvaluacion').hide();

		$(document).on('click', '#agregarEvaluadorEvaluado', function(e){
			e.preventDefault();
			$('#formEvaluacion').toggle('slow');
			$('#agregarEvaluadorEvaluado').hide();
		});

		$('select.periodo_matriz').change(function(){

			window.location = 'Matriz-Evaluacion?id_periodo=' + $(this).val();
		});

		if(contPeriodos > 1){
			$('.matrizEvaluacion').DataTable({
      	'order': [[5,'DESC']],
      	language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
  		});
		}else{
			$('.matrizEvaluacion').DataTable({
      	'order': [[0,'DESC']],
      	language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
  		});
		}
		
	});
	
</script>
@endsection