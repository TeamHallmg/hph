@extends('layouts.app')

@section('title', 'Editar grupo de areas')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-12">
		<img class="img-fluid" src="{{ asset('img/clima-organizacional/banner.png') }} " alt="">
		<h3 class="titulos-evaluaciones mt-3 font-weight-bold">Editar grupo de áreas</h3>
		<hr class="mb-3">
		<form method="post" action="/editar-grupo-area/">
			{{ csrf_field() }}
			<input type="hidden" name="id" value="{{$id}}">
			<div class="form-group">
				<label for="nombreGrupoArea">Nombre del grupo de áreas:</label>
				<input type="text" class="form-control" required name="nombreGrupoArea" value="{{$nombreGrupoAreas}}">
			</div>
			<div class="form-group">
				<div class="table-responsive">
					<table class="table table-striped table-bordered" id="table-areas">
						<thead style="background-color: #002C49; color:white;">
							<tr>
								<th>
									ID
								</th>
								<th>
									Nombre
								</th>
								<th>
									Acción
								</th>
							</tr>
						</thead>
						<tbody>
							
							@if($areas->isEmpty())
								<tr>
									<td colspan="3">No hay áreas para seleccionar.</td>
								</tr>
							@else
							@foreach($areas as $area)
								@if(in_array($area->id, $selected))
								<tr>
									<td>{{ $area->id }}</td>
									<td>{{ $area->nombre }}</td>
									<td>
										<input type="checkbox" name="id_areas[]" value="{{$area->id}}" class="field" <?php if (in_array($area->id, $selected)){ ?>checked="checked"<?php } ?>>
									</td>
								</tr>
								@endif
							@endforeach
							@foreach($areasOtros as $areasOtro)
								<tr>
									<td>{{ $areasOtro->id_areas }}</td>
									<td>{{ $areasOtro->Areas->Name }}</td>
									<td>
										<input type="checkbox" name="id_areas[]" value="{{$areasOtro->id_areas}}" class="field" <?php if (in_array($areasOtro->id_areas, $selected)){ ?>checked="checked"<?php } ?>>
									</td>
								</tr>
							@endforeach
							@endif
						</tbody>
					</table>
				</div>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-success">Actualizar</button>
				<a class="btn btn-primary" href="{{ URL::previous() }}">Regresar</a>
			</div>
			</form>
	</div>
</div>
@endsection

@section('scripts')
<script>
	$('#table-areas tbody tr').click(function (e) {
    if(!$(e.target).is('#table-areas td input:checkbox'))
    $(this).find('input:checkbox').trigger('click');
});
</script>
@endsection