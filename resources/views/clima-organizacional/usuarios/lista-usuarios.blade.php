@extends('layouts.app')

@section('title', 'Listado de Colaboradores')

@section('content')
<style type="text/css">
	table.listado {width: 100% !important;}
</style>
<div class="row">

	<div class="col-md-2 text-right">
		@include('clima-organizacional/partials/sub-menu')
	</div>

	<div class="col-md-10">
			<img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
		
			<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Listado de Colaboradores</h3>
			<hr class="mb-5">

			<table class="table table-striped table-bordered listado dt-responsive" style="width: auto !important">
				<thead style="background-color: #222B64; color:white;">
					<tr>
						<th>ID</th>
						<th>Nombre</th>
						<th>Correo</th>
						<!--<th style="padding: 5px 10px; text-align: center; border: 1px solid white;">Correo</th>-->
						<th>Puesto</th>
						<th>Región</th>
						<th>Departamento</th>
						<th>Jefe</th>

				@if (auth()->user()->role == 'admin')		
						
						<th>Accion</th>
				@endif

					</tr>
				</thead>
				<tbody>
						
				@if (auth()->user()->role == 'admin')

					@foreach($users as $user)
							
						@if (!empty($user->employee))
					<tr>
						<td class="text-center">{{ $user->employee->idempleado }}</td>
						<td class="text-center">{{ str_replace('Ã‘','Ñ',$user->fullname) }}</td>
						<td class="text-center">{{ str_replace('Ã‘','Ñ',$user->employee->correoempresa) }}</td>
						<td class="text-center">{{ str_replace('Ã‘','Ñ',$user->employee->jobPosition->name) }}</td>
						<td class="text-center">{{ (isset($user->employee->region) ? $user->employee->region->name : '') }}</td>
						<td class="text-center">{{ (isset($user->employee->jobPosition->area->department) ? $user->employee->jobPosition->area->department->name : '') }}</td>
						<td class="text-center">{{(isset($user->employee->boss->user) ? str_replace('Ã‘','Ñ',$user->employee->boss->user->fullname) : '') }}</td>
						<td class="text-center">
							<!--<a class="btn btn-primary" href="/cambiar-jefe/{{$user->id}}"><i class="fas fa-edit"></i> Cambiar Jefe</a>-->
							<a class="btn btn-primary" href="/cambiar-contrasena/{{$user->id}}">Cambiar Contraseña</a>
						</td>
					</tr>
						@endif
					@endforeach
				@else

		<?php $permissions = auth()->user()->getClimaRegions(10); ?>

					@foreach($users as $user)
							
						@if (!empty($user->employee) && (in_array(0, $permissions) || in_array($user->employee->region_id, $permissions)))
					<tr>
						<td class="text-center">{{ $user->employee->idempleado }}</td>
						<td class="text-center">{{ str_replace('Ã‘','Ñ',$user->fullname) }}</td>
						<td class="text-center">{{ str_replace('Ã‘','Ñ',$user->employee->correoempresa) }}</td>
						<td class="text-center">{{ str_replace('Ã‘','Ñ',$user->employee->jobPosition->name) }}</td>
						<td class="text-center">{{ (isset($user->employee->region) ? $user->employee->region->name : '') }}</td>
						<td class="text-center">{{ (isset($user->employee->jobPosition->area->department) ? $user->employee->jobPosition->area->department->name : '') }}</td>
						<td class="text-center">{{(isset($user->employee->boss->user) ? str_replace('Ã‘','Ñ',$user->employee->boss->user->fullname) : '') }}</td>
					</tr>
						@endif
					@endforeach
				@endif

				</tbody>
			</table>
		
	</div>
</div>
@endsection

@section('scripts')
<script>

	$(document).ready(function(){

	$('.listado').DataTable({
		dom: 'Blftip',
      'order': [[0,'asc']],
		language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  },
      buttons: [
			{
				extend: 'excel',
				text: '<i class="fa fa-file-pdf"></i> Exportar a Excel',
				titleAttr: 'Exportar a Excel',
				title: 'Colaboradores HPH',
			}
		]
   });
	});
</script>
@endsection