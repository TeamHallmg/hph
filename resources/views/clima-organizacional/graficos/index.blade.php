@extends('clima-organizacional.app')

@section('content')

<div class="card mt-3">
    <div class="card-header p-3">
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <input type="radio" name="order_radio" class="_change o_radio" id="r_factor" value="factor" checked>
                    <label for="">Por Factores</label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">                    
                    <input type="radio" name="order_radio" class="_change o_radio" id="r_progressive" value="progressive">
                    <label for="">Progresivo</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Factor</label>
                    <select class="form-control _change" id="select_factor">
                        <option value="">--TODO--</option>
                        @foreach($factors as $factor)
                            <option value="{{ $factor->id }}">{{ $factor->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Dirección</label>
                    <select class="form-control _change" id="select_direction">
                        <option value="">--TODO--</option>
                        @foreach($directions as $direction)
                            <option value="{{ $direction->name }}">{{ $direction->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Departamento</label>
                    <select class="form-control _change" id="select_department">
                        <option value="">--TODO--</option>
                        @foreach($departments as $department)
                            <option value="{{ $department->name }}">{{ $department->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Área</label>
                    <select class="form-control _change" id="select_area">
                        <option value="">--TODO--</option>
                        @foreach($areas as $area)
                            <option value="{{ $area->name }}">{{ $area->name }}</option>
                        @endforeach
                    </select> 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Puestos</label>
                    <select class="form-control _change" id="select_job">
                        <option value="">--TODO--</option>
                        @foreach($jobs as $job)
                            <option value="{{ $job->name }}">{{ $job->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Nivel</label>
                    <select class="form-control _change" id="select_job_level">
                        <option value="">--TODO--</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card my-3">
    <div class="card-header p-3">
    </div>
    <div class="card-body">        
        <div id="container1" style="width: 100%; height: 400px; margin: 0 auto"></div>
    </div>
</div>

@endsection


@section('scripts')
<script>

$(function (){
    $('._change').on('change', function(){
        axios.post('{{ route("clima.reports.post") }}', {
            order: $('.o_radio:checked').val(),
            factor: $('#select_factor').val(),
            direction: $('#select_direction').val(),
            department: $('#select_department').val(),
            area: $('#select_area').val(),
            job: $('#select_job').val(),
            job_level: $('#select_job_level').val(),
        })
        .then(function (response){
            var aux_chart = {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Promedio calificación por Factores'
                },
                subtitle: {
                    text: 'General'
                },
                xAxis: {
                    categories: response.data.categories,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    }
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: response.data.data
            }
            var chart = new Highcharts.chart('container1', aux_chart); 
        })
        .catch(function (error){

        });
    });
});
// tooltip: {
//         headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
//         pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
//             '<td style="padding:0"><b class="ml-2">{point.y:.1f}</b></td></tr>',
//         footerFormat: '</table>',
//         shared: true,
//         useHTML: true
//     },
// Create the chart
var graf = Highcharts.chart('container1', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Promedio calificación por Factores'
    },
    subtitle: {
        text: 'General'
    },
    xAxis: {
        categories: {!! json_encode($categories) !!},
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: {!! json_encode($factorData) !!}
});
</script>
@endsection