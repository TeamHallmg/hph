@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
	@include('clima-organizacional/partials/sub-menu')
	</div>
	<div class="col-md-10">
    <img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">

    <h3 class="titulos-evaluaciones mt-4 font-weight-bold">REPORTE DE MAPA DE CALOR</h3>
    <hr>

    <div class="row justify-content-center">
        

        @include('clima-organizacional.reportes.filtros')



    </div>
 
    <div class="row">
      <div class="col-md-12">
        <hr>
        <div class="table-responsive">
        <table class="table  heat">
          <thead>
            <th style="color: #7C75B7"><b></b></th>
            <th style="color: #7C75B7"><b>DIRECCIONES - DEPARTAMENTOS</b></th>

          @foreach ($clima_factors as $key => $factor)
        
            <th class="text-center" style="background-color: #7C75B7; color: white"><b>{{$factor}}</b></th>
          @endforeach

            <th class="text-center" style="background-color: #7C75B7; color: white"><b>Total general</b></th>

          </thead>
          <tbody>

    <?php $current_department = '';
          $current_factor = 0;
          $current_region = 0;
          $department_total = 0;
          $department_factors = 0;
          $totales_factores = $contadores_factores = array(); ?>

          @foreach ($results as $key => $result)

            @if ($current_department != $result->name)

              @if ($current_department != '')

                @while ($current_factor < count($clima_factors))

              <td></td>
              <td></td>

            <?php $current_factor++; ?>
                @endwhile

          <?php //$total_by_department = $department_total / $department_factors;
                $total_by_department = $results_by_departments[$current_region][$current_department];
                $color = 0;

                if (count($etiquetas) > 0){

                  for ($i = 0;$i < count($etiquetas);$i++){

                    if ($total_by_department >= $etiquetas[$i]->valor){

                      if (!empty($etiquetas[$i]->color)){

                        $color = $etiquetas[$i]->color;
                      }

                      break;
                    }
                  }
                }

                if (empty($color)){ ?>

              <td class="text-center" style="background-color: {{($total_by_department > 8.333333333333333 ? '#67C080' : ($total_by_department > 6.666666666666667 ? '#D0DE87' : ($total_by_department > 5 ? '#FAE886' : ($total_by_department > 3.333333333333333 ? '#FFCE7D' : ($total_by_department > 1.666666666666667 ? '#F69779' : '#F9726C')))))}}; border-color: {{($total_by_department > 8.333333333333333 ? '#67C080' : ($total_by_department > 6.666666666666667 ? '#D0DE87' : ($total_by_department > 5 ? '#FAE886' : ($total_by_department > 3.333333333333333 ? '#FFCE7D' : ($total_by_department > 1.666666666666667 ? '#F69779' : '#F9726C')))))}}">{{number_format($total_by_department, 2)}}</td>
            </tr>
          <?php }

            else{ ?>

              <td class="text-center" style="background-color: {{$color}}; border-color: {{$color}}">{{number_format($total_by_department, 1)}}</td>
      <?php } ?>
              @endif

            <tr>

        <?php $current_department = $result->name;
              $current_factor = 0;
              $department_total = 0;
              $department_factors = 0;
              $current_region = $result->region_id; ?>

              <td>{{$result->region}}</td>
              <td>{{$result->name}}</td>
            @endif

            @while (!empty($clima_factors[$current_factor]) && $clima_factors[$current_factor] != $result->factor_name)

        <?php $current_factor++; ?>

              <td></td>
              <td></td>
            @endwhile

            @if (!isset($totales_factores[$current_factor]))

        <?php $totales_factores[$current_factor] = 0;
              $contadores_factores[$current_factor] = 0; ?>
            @endif

      <?php $totales_factores[$current_factor] += $result->average;
            $contadores_factores[$current_factor]++; 
            $current_factor++;
            $department_total += $result->average;
            $department_factors++;
            $color = 0;

            if (count($etiquetas) > 0){

              for ($i = 0;$i < count($etiquetas);$i++){

                if ($result->average >= $etiquetas[$i]->valor){

                  if (!empty($etiquetas[$i]->color)){

                    $color = $etiquetas[$i]->color;
                  }

                  break;
                }
              }
            }

            if (empty($color)){ ?>

              <td class="text-center" style="background-color: {{($result->average > 8.333333333333333 ? '#67C080' : ($result->average > 6.666666666666667 ? '#D0DE87' : ($result->average > 5 ? '#FAE886' : ($result->average > 3.333333333333333 ? '#FFCE7D' : ($result->average > 1.666666666666667 ? '#F69779' : '#F9726C')))))}}; border-color: {{($result->average > 8.333333333333333 ? '#67C080' : ($result->average > 6.666666666666667 ? '#D0DE87' : ($result->average > 5 ? '#FAE886' : ($result->average > 3.333333333333333 ? '#FFCE7D' : ($result->average > 1.666666666666667 ? '#F69779' : '#F9726C')))))}}">{{number_format($result->average, 2)}}</td>
      <?php }

            else{ ?>

              <td class="text-center" style="background-color: {{$color}}; border-color: {{$color}}">{{number_format($result->average, 1)}}</td>
      <?php } ?>
          @endforeach
          
          @if ($current_department != '')

            @while ($current_factor < count($clima_factors))

              <td></td>
              <td></td>

        <?php $current_factor++; ?>
            @endwhile

      <?php //$total_by_department = $department_total / $department_factors;
            $total_by_department = $results_by_departments[$current_region][$current_department];
            $color = 0;

            if (count($etiquetas) > 0){

              for ($i = 0;$i < count($etiquetas);$i++){

                if ($total_by_department >= $etiquetas[$i]->valor){

                  if (!empty($etiquetas[$i]->color)){

                    $color = $etiquetas[$i]->color;
                  }

                  break;
                }
              }
            }

            if (empty($color)){ ?>

              <td class="text-center" style="background-color: {{($total_by_department > 8.333333333333333 ? '#67C080' : ($total_by_department > 6.666666666666667 ? '#D0DE87' : ($total_by_department > 5 ? '#FAE886' : ($total_by_department > 3.333333333333333 ? '#FFCE7D' : ($total_by_department > 1.666666666666667 ? '#F69779' : '#F9726C')))))}}; border-color: {{($total_by_department > 8.333333333333333 ? '#67C080' : ($total_by_department > 6.666666666666667 ? '#D0DE87' : ($total_by_department > 5 ? '#FAE886' : ($total_by_department > 3.333333333333333 ? '#FFCE7D' : ($total_by_department > 1.666666666666667 ? '#F69779' : '#F9726C')))))}}">{{number_format($total_by_department, 2)}}</td>
      <?php }

            else{ ?>

              <td class="text-center" style="background-color: {{$color}}; border-color: {{$color}}">{{number_format($total_by_department, 1)}}</td>
      <?php } ?>

            </tr>
          @endif

            <tr>

              <td>zzzz</td>
              <td class="text-center" style="background-color: #7C75B7; color: white"><b>Total general</b></td>


      <?php $total_de_totales = 0;
            $contador_de_totales = 0; ?>

          @foreach ($clima_factors AS $key => $value)

            @if (isset($totales_factores[$key]))

        <?php //$total_de_total = $totales_factores[$key] / $contadores_factores[$key];
              $total_de_total = $results_by_factors[$value];
              $total_de_totales += $total_de_total;
              $contador_de_totales++;
              $color = 0;

              if (count($etiquetas) > 0){

                for ($i = 0;$i < count($etiquetas);$i++){

                  if ($total_de_total >= $etiquetas[$i]->valor){

                    if (!empty($etiquetas[$i]->color)){

                      $color = $etiquetas[$i]->color;
                    }

                    break;
                  }
                }
              }

              if (empty($color)){ ?>

              <td class="text-center" style="background-color: {{($total_de_total > 8.333333333333333 ? '#67C080' : ($total_de_total > 6.666666666666667 ? '#D0DE87' : ($total_de_total > 5 ? '#FAE886' : ($total_de_total > 3.333333333333333 ? '#FFCE7D' : ($total_de_total > 1.666666666666667 ? '#F69779' : '#F9726C')))))}}; border-color: {{($total_de_total > 8.333333333333333 ? '#67C080' : ($total_de_total > 6.666666666666667 ? '#D0DE87' : ($total_de_total > 5 ? '#FAE886' : ($total_de_total > 3.333333333333333 ? '#FFCE7D' : ($total_de_total > 1.666666666666667 ? '#F69779' : '#F9726C')))))}}">{{number_format($total_de_total, 2)}}</td>
        <?php }

              else{ ?>

              <td class="text-center" style="background-color: {{$color}}; border-color: {{$color}}">{{number_format($total_de_total, 1)}}</td>
        <?php } ?>
            @else

              <td></td>
              <td></td>
            @endif
          @endforeach

          @if ($contador_de_totales > 0)

      <?php //$total_de_totales = $total_de_totales / $contador_de_totales;
            $total_de_totales = $general_result[0]->average;
            $color = 0;

            if (count($etiquetas) > 0){

              for ($i = 0;$i < count($etiquetas);$i++){

                if ($total_de_totales >= $etiquetas[$i]->valor){

                  if (!empty($etiquetas[$i]->color)){

                    $color = $etiquetas[$i]->color;
                  }

                  break;
                }
              }
            }

            if (empty($color)){ ?>

              <td class="text-center" style="background-color: {{($total_de_totales > 8.333333333333333 ? '#67C080' : ($total_de_totales > 6.666666666666667 ? '#D0DE87' : ($total_de_totales > 5 ? '#FAE886' : ($total_de_totales > 3.333333333333333 ? '#FFCE7D' : ($total_de_totales > 1.666666666666667 ? '#F69779' : '#F9726C')))))}}; border-color: {{($total_de_totales > 8.333333333333333 ? '#67C080' : ($total_de_totales > 6.666666666666667 ? '#D0DE87' : ($total_de_totales > 5 ? '#FAE886' : ($total_de_totales > 3.333333333333333 ? '#FFCE7D' : ($total_de_totales > 1.666666666666667 ? '#F69779' : '#F9726C')))))}}">{{number_format($total_de_totales, 2)}}</td>
      <?php }

            else{ ?>

              <td class="text-center" style="background-color: {{$color}}; border-color: {{$color}}">{{number_format($total_de_totales, 1)}}</td>
      <?php } ?>
          @else

              <td></td>
              <td></td>
          @endif

            </tr>
          </tbody>
        </table>
      </div>
      </div>
    </div>
  </div>
</div>

<form action="/clima-organizacional/heat-report" method="post" class="change_period_id">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="period_id" class="periodo">
  <input type="hidden" name="sucursal" class="sucursal">
  <input type="hidden" name="direccion" class="direccion">
  <input type="hidden" name="job" class="job">
  <input type="hidden" name="edad" class="edad">
  <input type="hidden" name="ingreso" class="ingreso">
  <input type="hidden" name="sexo" class="sexo">
  <input type="hidden" name="centro_trabajo" class="centro_trabajo">
  <input type="hidden" name="turno" class="turno">
  <input type="hidden" name="regiones" class="regiones">
  <input type="submit" style="display: none">
</form>

@endsection

@section('scripts')
	<script>
		
		$(document).ready(function(){

        $(document).ready(function(){  
            // Cambia el periodo
            $('._change').click(function(){
              // Recarga la pagina con el nuevo periodo
              $('form.change_period_id .periodo').val($('#select_periodo').val());
              $('form.change_period_id .sucursal').val($('#select_sucursal').val());
              $('form.change_period_id .direccion').val($('#select_direccion').val());
              $('form.change_period_id .job').val($('#select_job').val());
              $('form.change_period_id .edad').val($('#select_edad').val());
              $('form.change_period_id .ingreso').val($('#select_inicio').val());
              $('form.change_period_id .sexo').val($('#select_sexo').val());
              $('form.change_period_id .centro_trabajo').val($('#select_centro_trabajo').val());
              $('form.change_period_id .turno').val($('#selects_turno').val());
              $('form.change_period_id .regiones').val($('#select_regiones').val());
              $('form.change_period_id').submit();
            });
        });


     //  $('.heat').DataTable({
				 //  dom: 'Blfrtip',
     //        scrollX: true,
     //        scrollCollapse: true,
     //        paging: false,
   		// 	    'order': [[0,'asc']], 
     //        info: false,
     //        fixedColumns:   {
     //              leftColumns: 1
     //        },
     //        buttons: [
     //        {
     //          extend: 'excel',
     //          text: 'Exportar a Excel',
     //          titleAttr: 'Exportar a Excel',
     //          title: 'REPORTE DE MAPA DE CALOR'
     //        }
     //        ],
   		// 	language: {
		 		// 'url': '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
     //  		  },
     //  });


    var groupColumn = 0;
    var table = $('.heat').DataTable({
            scrollX: true,
            scrollCollapse: true,
            paging: false,
            info: false,
            fixedColumns:   {
                  leftColumns: 1
            },
          dom: 'Blfrtip',
            buttons: [
            {
              extend: 'excel',
              text: 'Exportar a Excel',
              titleAttr: 'Exportar a Excel',
              title: 'REPORTE DE MAPA DE CALOR'
            }
            ],
       language: {
        'url': '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
           },

        "columnDefs": [
            { "visible": false, "targets": groupColumn },
            { orderable: false, targets: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14] }

             /* true or false */
        ],
        "order": [[ groupColumn, 'asc' ],[ 1, 'asc' ]],
        "displayLength": 50,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                  if (group=='zzzz') {
                   $(rows).eq( i ).before(
                        '<tr class="group bg-info"><td colspan="15"; style="color: transparent;"><strong>'+group+'</strong></td></tr>'
                    );

                  }else{
                   $(rows).eq( i ).before(
                        '<tr class="group bg-info"><td colspan="15"><strong>'+group+'</strong></td></tr>'
                    );
 
                  }
                    
                    last = group;
                }
            } );
        }
    } );

			// $('.heat').DataTable({ 
   		// 	'order': [[0,'asc']],
   		// 	'scrollX': true,
   		// 	language: {
		 	// 	'url': '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
      // 		  },
      // 	buttons: [
      //   {
      //     extend: 'excel',
			// 		text: 'Exportar a Excel',
			// 		titleAttr: 'Exportar a Excel',
			// 		title: 'Reporte Desempeño Completo'
			// 	}
      //   ]
   		// });


		});
	</script>
  
<script type="text/javascript">

  $(document).ready(function(){

    $('select.period_id').change(function(){

      $('form#changePlanForm').submit();
    });
  });
    
</script>
@endsection