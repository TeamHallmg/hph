<div class="card-body">
     <div class="row">
         <div class="col-md-3">
            <div class="form-group">
                <label for="">Periodo</label>
                <select class="cambiando form-control" id="select_periodo" name="periodo">
                    @foreach($periods as $periodo)
                        <option value="{{ $periodo->id }}" {{ $periodo->id==$period_id?'selected':'' }} >{{ $periodo->name }} ({{ $periodo->status }})</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Hospital</label>
                <select class="cambiando form-control " id="select_regiones" name="regiones">
                    <option value="">--TODO--</option>
                    @foreach($filtros_ejecutados['regiones'] as $region)
                        <option value="{{ $region['id'] }}"  {{ isset($aplicar_filtros['regiones_id'])?$region['id']==$aplicar_filtros['regiones_id']?'selected':'':'' }}>{{ $region['nombre'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Centros de Trabajos</label>
                <select class="cambiando form-control" id="select_centro_trabajo" name="centro_trabajo">
                    <option value="">--TODO--</option>
                    @foreach($filtros_ejecutados['centros_trabajo'] as $centro_trabajo)
                        <option value="{{ $centro_trabajo['id'] }}"  {{ isset($aplicar_filtros['centro_trabajo_id'])?$centro_trabajo['id']==$aplicar_filtros['centro_trabajo_id']?'selected':'':'' }}>{{ $centro_trabajo['nombre'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Areas</label>
                <select class="cambiando form-control" id="select_direccion" name="area">
                    <option value="">--TODO--</option>
                    @foreach($filtros_ejecutados['directions'] as $direction)
                        <option value="{{ $direction['id'] }}"  {{ isset($aplicar_filtros['area_id'])?$direction['id']==$aplicar_filtros['area_id']?'selected':'':'' }}>{{ $direction['nombre'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Departamento</label> 
                <select class="cambiando form-control" id="select_sucursal" name="sucursal">
                    <option value="">--TODO--</option> 
                    @foreach($filtros_ejecutados['sucursales'] as $sucursal)
                        <option value="{{ $sucursal['id'] }}"  {{ isset($aplicar_filtros['sucursal_id'])?$sucursal['id']==$aplicar_filtros['sucursal_id']?'selected':'':'' }}>{{ $sucursal['nombre'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Categoría de Puestos</label>
                <select class="cambiando form-control" id="select_job" name="puesto">
                    <option value="">--TODO--</option>
                    @foreach($filtros_ejecutados['jobs'] as $job)
                        <option value="{{ $job->id }}"  {{ isset($aplicar_filtros['puesto_id'])?$job->id==$aplicar_filtros['puesto_id']?'selected':'':'' }}>{{ $job->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Edad</label>
                <select class="cambiando form-control" id="select_edad" name="edad">
                    <option value="">--TODO--</option>
                    @foreach($filtros_ejecutados['ages'] as $age)
                        <option value="{{ $age['id'] }}"  {{ isset($aplicar_filtros['edad_id'])?$age['id']==$aplicar_filtros['edad_id']?'selected':'':'' }}>{{ $age['nombre'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Antigüedad</label>
                <select class="cambiando form-control" id="select_inicio" name="inicio">
                    <option value="">--TODO--</option>
                    @foreach($filtros_ejecutados['starts'] as $start)
                        <option value="{{ $start['id'] }}"  {{ isset($aplicar_filtros['inicio_id'])?$start['id']==$aplicar_filtros['inicio_id']?'selected':'':'' }}>{{ $start['nombre'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Sexo</label>
                <select class="cambiando form-control" id="select_sexo" name="sexo">
                    <option value="">--TODO--</option>
                    @foreach($filtros_ejecutados['sexs'] as $sex)
                        <option value="{{ $sex['id'] }}"  {{ isset($aplicar_filtros['sexo_id'])?$sex['id']==$aplicar_filtros['sexo_id']?'selected':'':'' }}>{{ $sex['nombre'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Turnos</label>
                <select class="cambiando form-control" id="selects_turno" name="turnos">
                    <option value="">--TODO--</option>  
                    @foreach($filtros_ejecutados['turnos'] as $turno)
                        <option value="{{ $turno['id'] }}"  {{ isset($aplicar_filtros['turnos_id'])?$turno['id']==$aplicar_filtros['turnos_id']?'selected':'':'' }}>{{ $turno['name'] }}</option>
                    @endforeach                 
                </select>
            </div>
        </div>
    </div>
</div>

<div class="card-footer">
    {{-- <button class="btn btn-info _reset btn-sm float-right" > Reiniciar Filtros</button> --}}
  <button class="btn btn-warning _change btn-sm float-right mr-3    " > Ejecutar Consulta </button>
</div>
