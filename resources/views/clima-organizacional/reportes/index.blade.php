@extends('layouts.app')

@section('title', 'Evaluación de Desempeño')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-responsive" src="{{ asset('img/evaluacion_desempeno.png') }}" alt="">
	
		<div class="col-md-12">
			
			<table class="table table-striped table-bordered">
				<caption align="center">Reportes</caption>
				<thead>
					<tr>
						<th class="cabeceras-tablas-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">ID</th>
						<th class="cabeceras-tablas-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Nombre</th>
						<th class="cabeceras-tablas-evaluaciones" style="padding: 5px 10px; text-align: center; border: 1px solid white;">Acción</th>
					</tr>
				</thead>
				<tbody>
					@if(empty($empleados) || is_null($empleados))
						<tr>
							<td colspan="3" align="center">
								No estan disponibles los reportes.
							</td>
						</tr>
					@else
						@foreach($empleados as $empleado)
							{{--  @if(auth()->user()->id === $empleado->id)
							@else--}}
								<tr>
									<td>{{ $empleado->id }}</td>
									<td>{{ $empleado->fullname }}</td>
									<td>
										<a class="btn btn-primary" href="{{ url('reportes/' . $empleado->id ) }}">
											Ver Reporte
										</a>
									</td>
								</tr>
							{{--  @endif--}}
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection