<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SoySepanka') }}</title>

    <!-- Scripts -->
    <!--<script src="{{ asset('js/app.js') }}" defer></script>-->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vacations.css') }}" rel="stylesheet">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-58386377-41"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-58386377-41');
    </script>
</head>
<body class="bg-white">
    <div id="app">
        <header class="bg-white">   
            <div class="container py-2">
                <div class="row d-flex justify-content-around align-content-center">
                    <div class="col-6">
                        <a><img class="img-fluid" src="/img/logo.png" alt=""></a>
                    </div>
                    <div class="col-6 row">
                        <div class="col-md-8 text-right mt-4">
                            <h5><strong>Bienvenido</strong></h5>
                            <div class="dropdown">
                                <a>
                                    <span class="align-middle mr-2"><i class="fas fa-th text-danger fa-2x"></i></span> <strong>{{ Auth::user()->getFullNameAttribute() }}</strong>
                                </a>
                            </div>
                            <a class="text-danger" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fas fa-power-off"></i> Cerrar sesión</a>
                        </div>
                        <div class="col-md-4 d-flex align-items-center">
                            <a>
                                <div class="photo-container shadow-1"
                                style="
                                background-image:url({!! asset(Auth::user()->getProfileImage()) !!});
                                background-size:cover;
                                background-position:center;
                                ">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <nav class="navbar navbar-expand-md navbar-dark navbar-laravel shadow-3">
            <div class="container">
                {{--<a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>--}}
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse text-white" id="navbarSupportedContent">
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{asset('login')}}">Iniciar Sesión</a>
                            </li>
                        @else
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <div class="row justify-content-center">
                <div class="col-md-8 text-center">
                    @include('flash::message')
                </div>
            </div>
            <div class="col-md-10 offset-md-1">
                {{-- @yield('mainContent') --}}
                @yield('content')
            </div>
        </main>
    </div>
</body>
<footer class="pb-5 pt-2 bg-dark">
    <div class="container">
        <div class="row d-flex justify-content-center align-content-start">
            <div class="text-white text-center">
                Gustavo Molina Grupo
            </div>
        </div>
    </div>
</footer>
</html>

{{-- @yield('mainScripts') --}}
<script src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    });
</script>

@yield('scripts')