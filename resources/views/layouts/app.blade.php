<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SoySepanka') }}</title>
    <link rel="shortcut icon" type="image/png" href="{{ asset('img/favicon.png') }}"/>

    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script> --}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vacations.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
 
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-58386377-44"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-58386377-44');
    </script>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

 {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">  --}}

<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">

    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/61b8d0cac82c976b71c16977/1fmsuv5r9';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<style>
    .bg-client-secondary{
        background: #0c2f83 !important;
    }
	.circle-icon-sm {
    background: #3EBAC8;
    width: 40px;
    height: 40px;
    border-radius: 50%;
    text-align: center;
    line-height: 40px;
    vertical-align: middle;
    padding: 0px;
    font-size: 18px;
}

.loadingSpinner {
    /*visibility: hidden;
    opacity: 0;*/
    height: 80px;
    width: 80px;
    min-width: 80px;
    min-height: 80px;
    transform-origin: 40px 40px 40px;
    transition: 0.5s;
    animation: spinnerAnimation 1.3s infinite cubic-bezier(0.53, 0.21, 0.29, 0.67);
}

  @keyframes spinnerAnimation {
      0% {
          transform: rotate(0deg);
      }

      100% {
          transform: rotate(360deg);
      }
  }
        [v-cloak] > * {
        display:none;
      }
      [v-cloak]:before {

          background: url(dist/img/new-loader.gif) no-repeat #fffef2;
          background-position: center center;
          background-size: 13%;
          position: fixed;
          left: 0;
          top: 0;
        width: 100%;
        height: 100vh;
          z-index: 12345;
        content: '';
      }
      
	</style>
</head>
<body class="bg-white">
    <div id="app">
        <header class="bg-white">   
            <div class="container-fluid col-md-10  py-4">
                <div class="row d-flex justify-content-around align-items-center">
                    <div class="col-6">
                            <a href="{{ url('home') }}"><img class="img-fluid w-50" src="/img/logo.png" alt=""></a>
                    </div>
                    <div class="col-6 row">
                        <div class="col-md-10 text-right my-auto">
                            <h5><strong>Bienvenido</strong></h5>
                            <h6 class="text-dark">
                                <strong>{{ Auth::user()->getFullNameAttribute() }}</strong>
                            </h6>
                            <a class="" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fas fa-power-off color-personalizado-verde1"></i> Cerrar sesión</a>
                        </div>
                        <div class="col-md-2 d-flex align-items-center">
                            {{-- <a href="{{ url('curriculum') }}"> --}}
                                <div class="photo-container shadow-1"
                                style="
                                background-image:url({!! asset('img/profile.png') !!});
                                background-size:cover;
                                background-position:center;
                                ">
                                </div>
                            {{-- </a> --}}
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <nav class="navbar navbar-expand-md navbar-dark navbar-laravel shadow-3" style="background:#406BB2 !important">
            <div class="container-fluid col-md-10">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse text-white" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav d-flex justify-content-between w-100">
                        {{-- <li class="nav-item dropdown col">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                Nuestra Empresa <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-center bg-gray border-0 rounded-0 w-100" aria-labelledby="navbarDropdown">
                                <a href="{{ url('filosofia') }}" class="dropdown-item text-white dropdown-item-dark">Misión y Visión</a>
                                <a href="{{ url('organigrama-puestos') }}" class="dropdown-item text-white dropdown-item-dark">Plantilla</a>
                                <a href="{{ url('cumpleaños') }}" class="dropdown-item text-white dropdown-item-dark">Cumpleaños</a>
                                <a href="{{ url('organigrama') }}" class="dropdown-item text-white dropdown-item-dark">Organigrama</a>
                            </div>
                        </li> --}}
                        @if (empty(auth()->user()->external) && (auth()->user()->role == 'admin' || auth()->user()->role == 'supervisor' || auth()->user()->hasClimaPermissions()))
                          <li class="nav-item {{(auth()->user()->role == 'supervisor' ? 'col text-right' : '')}}">
                            <a class="nav-link" href="{{url('clima-organizacional/que-es')}}">Clima Laboral</a>
                          </li>
                        @endif
                        {{-- <li class="nav-item dropdown col">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                Administración <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right bg-gray border-0 rounded-0 w-100" aria-labelledby="navbarDropdown">
                                @if(Auth::user()->hasRolePermission('user_admin'))
                                    <a href="{{ url('admin-de-usuarios') }}" class="dropdown-item text-white dropdown-item-dark">Administración de Usuarios</a>
                                @endif                                
                            </div>
                        </li> --}}
                      @if (empty(auth()->user()->external))
                        @if (auth()->user()->role == 'admin'  || auth()->user()->hasNpsPermissions(7))
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Plantillas <span class="caret"></span>

                                </a>
                                <div class="dropdown-menu dropdown-menu-center" aria-labelledby="navbarDropdown">
                                    <a href="{{ url('templates') }}" class="dropdown-item">Admin Plantillas</a>
                                    <a href="{{ url('templates-fields') }}" class="dropdown-item">Admin Campos Dinámicos</a>
                                </div>
                            </li>
                        @endif
                        @if (auth()->user()->role == 'admin' || auth()->user()->role == 'supervisor' || auth()->user()->role == 'supervisor' || auth()->user()->hasNpsPermissions())
                            <li class="nav-item">
                              <a class="nav-link" href="{{url('nps/que-es')}}">eNPS</a>
                            </li>
                        @endif
                        @if (auth()->user()->role == 'admin' || auth()->user()->role == 'supervisor' || auth()->user()->role == 'supervisor' || auth()->user()->hasSociogramaPermissions())
                            <li class="nav-item">
                              <a class="nav-link" href="{{url('sociograma/que-es')}}">Sociometría</a>
                            </li>
                        @endif

                        <li class="nav-item {{(auth()->user()->role != 'admin' && auth()->user()->role != 'supervisor' && !auth()->user()->hasClimaPermissions() && !auth()->user()->hasNpsPermissions() && !auth()->user()->hasSociogramaPermissions() ? 'col text-right' : '')}}">
                              <a class="nav-link" href="{{url('cuestionarios/evaluaciones')}}">Cuestionarios</a>
                            </li>
                            
                        {{-- opciones de menú para cuestionario de salida --}}
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                Encuesta de Salida <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-center" aria-labelledby="navbarDropdown">
                                <a href="{{ url('cuestionario-salida/create') }}" class="dropdown-item">Nueva</a>
                                @if(auth()->user()->role == 'admin')
                                    <a href="{{ url('cuestionario-salida') }}" class="dropdown-item">Admin</a>
                                    <a href="{{ url('/cuestionario-graficos/reporte') }}" class="dropdown-item">Gráficos</a>
                                @endif
                            </div>
                        </li>

                            <li class="nav-item dropdown {{(auth()->user()->role != 'admin' ? 'col text-left' : '')}}">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                NOM-035 <span class="caret"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-center" aria-labelledby="navbarDropdown">
                <a href="{{ url('nom035/politicas') }}" class="dropdown-item">Políticas</a>
                <a href="{{ url('nom035/evaluaciones') }}" class="dropdown-item">Encuesta</a>
                <a href="{{ url('nom035/informe') }}" class="dropdown-item">Informe Personal</a>
                


            @if(auth()->user()->role == 'admin' || auth()->user()->role == 'supervisor' || isset($userPermissions['see_progress']) || auth()->user()->hasNom035Permissions(2))
                <a href="{{ url('nom035/avances') }}" class="dropdown-item">Avances</a>
            @endif
            @if(auth()->user()->role == 'admin' || isset($userPermissions['see_reports']) || auth()->user()->hasNom035Permissions(3))
            <a href="{{ url('nom035/reporte_nom') }}" class="dropdown-item">Reporte General</a>
            @endif
            @if(auth()->user()->role == 'admin' || isset($userPermissions['see_reports']) || auth()->user()->hasNom035Permissions(4)) 
                <a href="{{ url('nom035/plan_individual') }}" class="dropdown-item">Plan Individual</a>
            @endif
            @if(auth()->user()->role == 'admin' || isset($userPermissions['see_reports']) || auth()->user()->hasNom035Permissions(5)) 
                <a href="{{ url('nom035/plan_accion') }}" class="dropdown-item">Plan de Acción</a>
            @endif
            @if(auth()->user()->role == 'admin' || isset($userPermissions['see_periods']) || auth()->user()->hasNom035Permissions(6))
                <a href="{{ url('nom035/periodos') }}" class="dropdown-item">Periodos</a>
            @endif
            @if (auth()->user()->role == 'admin')
                <a href="{{ url('nom035/permissions') }}" class="dropdown-item">Permisos</a>
            @endif
            @if(auth()->user()->role == 'admin' || auth()->user()->hasNom035Permissions(7))
                <a href="{{ url('nom035/usuarios-verificados') }}" class="dropdown-item">Usuarios Verificados</a>
            @endif
            </div>
        </li>
                      @endif

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{asset('login')}}">Iniciar Sesión</a>
                            </li>
                        @else
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <div class="row justify-content-center">
                <div class="col-md-8 text-center">
                    @include('flash::message')
                </div>
            </div>

            @if(View::hasSection('sidebar'))
                <div class="container-fluid col-md-10">
                    <div class="row">
                        <div class="col-md-2">
                            @yield('sidebar')
                        </div>
                        <div class="col-md-10">
                            @yield('content')
                        </div>
                    </div>
                </div>
            @else
                <div class="container-fluid col-md-10">
                    {{-- @yield('mainContent') --}}
                    @yield('content')
                </div>
            @endif
            
        </main>
    </div>
</body>
<!--
<footer style="background: #E6E7E8">
    <div class="container col-10">
        <div class="row d-flex justify-content-between p-5">
            <div class="col-md-6 col-sm-12">
                <div class="d-flex flex-row">
                    <i class="fas fa-map-marked-alt color-personalizado-verde1 pr-4 font-weight-bold" style="font-size: 2rem;"></i>
                    <p class="color-personalizado-azul h5">
                        Av. Américas 1297 Col. Providencia<br>
                        Guadalajara, Jalisco<br>
                        (33) 3540 1800
                    </p>
                </div>
             </div>

          {{--    <div class="col-2">
              <img src="{!! asset('img/footer_1.png') !!}" class="img-fluid w-100">
          </div>
          <div class="col-2">
              <img src="{!! asset('img/footer_2.png') !!}" class="img-fluid w-100">
          </div>
           --}}
             <div class="col-md-6 col-sm-12 text-md-right text-center text-white mt-sm-5 mt-md-0 mt-sm mt-5">
                {{-- <h5 class="text-md-right text-sm-center">Nuestras Redes Sociales</h5> --}}
                  <img src="{!! asset('img/footer_1.png') !!}" class="img-fluid w-25 mr-5"> 
                <img src="{!! asset('img/footer_2.png') !!}" class="img-fluid w-25"> 
            </div>
        </div>

        <div class="row d-flex align-items-end py-4">
            <div class="col-md-12 color-personalizado-azul text-center font-weight-bold">
                Copyright &reg;2021 by HallMG Internet Solutions.
            </div>
        </div>
    </div>
</footer>
-->
	<footer style="background-color: #406BB2;">
    <div class="container col-10">
        <div class="row d-flex justify-content-between p-5">
            <div class="col-md-6 col-sm-12">
                <div class="d-flex flex-row">
                    <i class="fas fa-map-marked-alt text-company-secondary pr-4" style="font-size: 2rem;"></i>
                    <p class="text-white h5">
                        Contacto Av. Empresarios No. 305<br>
                        Zapopan, Jalisco<br>
                        México<br>
                        Tel. 33 3848 55 85<br>
                        ana.mendez@cmpdh.mx
                    </p>
                </div>
             </div>
             <div class="col-md-6 col-sm-12 text-md-right text-center text-white mt-sm-5 mt-md-0 mt-sm mt-5">
                {{-- <h5 class="text-md-right text-sm-center">Nuestras Redes Sociales</h5> --}}
                <ul class="list-unstyled list-inline text-md-right text-center mt-2">
                    <li class="list-inline-item">
                        <a class="mx-1" href="https://www.instagram.com/hospitalespuertadehierro" target="_blank">
                            <i class="fab fa-instagram circle-icon-sm text-white"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="mx-1" href="https://www.facebook.com/HospitalesPuertaDeHierro" target="_blank">
                            <i class="fab fa-facebook-f circle-icon-sm text-white"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="mx-1" href="https://www.linkedin.com/company/hospitales-puerta-de-hierro" target="_blank">
                            <i class="fab fa-linkedin-in circle-icon-sm text-white"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row d-flex align-items-end py-4">
            <div class="col-md-12 text-white text-center">
                Copyright &reg;2021 by HallMG Internet Solutions.
            </div>
        </div>
    </div>
</footer>
</html>

{{-- @yield('mainScripts') --}}
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/highcharts.js') }}"></script>
<script src="{{ asset('/js/jquery.autocomplete.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>

{{-- 
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script> --}}

<script type="text/javascript">
    $(document).ready( function () {
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);


        $( ".videoModal" ).click(function() {
            $("#video-frame").attr("src", $(this).data('video')); 
            $("#modal-video").modal('show');
        });

        $("#modal-video").on('hidden.bs.modal', function (e) { 
            $("#video-frame").attr("src", ""); 
        });
    });
</script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
<script src="https://cdn.jsdelivr.net/npm/highcharts-vue@1.4.0/dist/highcharts-vue.min.js"></script>
<script src="{{ asset('js/lodash.js') }}"></script>
<script src="https://unpkg.com/vue-select@latest"></script>
<link rel="stylesheet" href="https://unpkg.com/vue-select@latest/dist/vue-select.css">
@yield('scripts_vuejs')
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>-->
<script>
    $('#summernote').summernote({
        callbacks: {
        onImageUpload: function(files, editor, welEditable) {
            sendFile(files[0], editor, welEditable);
        }
    }, 
    hint: {
        mentions: ['nombreCompleto', 'fechaNacimiento'],
        match: /\B@(\w*)$/,

        users: function(keyword, callback) {
                $.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                });
                $.ajax({
                    url: '/labels',
                    type: 'get',
                    async: true //This works but freezes the UI
                }).done(callback);
            },
            search: function (keyword, callback) {
                this.users(keyword, callback); //callback must be an array
            },
        content: function (item) {
        return '@' + item;
        }    
    },
  
      placeholder: '',
      tabsize: 2,
      height: 400,
      fontSizes: ['8', '9', '10', '11', '12', '13', '14', '15', '16', '18', '20', '21' , '24', '28', '32', '36', '40', '48'],
      fontNames: ['Calibri','Arial', 'Arial Black', 'Comic Sans MS', 'Courier New', 'Helvetica', 'Impact', 'Tahoma', 'Times New Roman', 'Verdana', 'Roboto'],
      toolbar: [
        ['style', ['style']],
        ['hr', ['hr']],
        ['fontsize', ['fontsize']],
        ['font', ['bold', 'underline', 'clear']],
        ['fontname', ['fontname']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']],
      ]
    });
    function sendFile(file, editor, welEditable) {
    data = new FormData();
    data.append("file", file);
    $.ajaxSetup({
 		headers: {
     	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    		}
	});
    $.ajax({
      data: data,
      type: "POST",
      url: "/update_file",
      cache: false,
      contentType: false,
      processData: false,
      success: function(url) {
          console.log(url);
          var image = $('<img>').attr({
                src:url, 
                title:"",
                alt:"",
            });
            $('#summernote').summernote("insertNode", image[0]);

        // editor.insertImage(welEditable, url);
      }
    });
  }
</script>
@yield('scripts')