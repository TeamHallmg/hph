<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <title>SoySepanka</title>
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
    {{-- <script src="{{ asset('js/app.js') }}"></script> --}}

    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/61b8d0cac82c976b71c16977/1fmsuv5r9';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-58386377-44"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-58386377-44');
    </script>

</head>
<body class=" h-100">
<div class="bg d-flex justify-content-center align-items-center">
    @yield('content')
    
</div>
<footer class="pb-3 pt-3 bg-dark fixed-bottom">
  <div class="container">
      <div class="row d-flex justify-content-center align-content-start">
          <div class="text-white text-center font-weight-bold">
              Power by Hall MG
          </div>
      </div>
  </div>
</footer>
</body>
</html>
