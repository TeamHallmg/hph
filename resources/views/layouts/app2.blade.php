<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SoySepanka') }}</title>
    <link rel="shortcut icon" type="image/png" href="{{ asset('img/favicon.png') }}"/>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vacations.css') }}" rel="stylesheet">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-58386377-44"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-58386377-44');
    </script>

    <!--Start of Tawk.to Script-->
  <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/60896d9f5eb20e09cf3771db/1f4cd8dta';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
  })();
  </script>
  <!--End of Tawk.to Script-->

</head>
<body class="bg-white">
    <div id="app">
        <header class="bg-white">   
            <div class="container-fluid col-md-10">
                <div class="row d-flex justify-content-around align-items-center">
                    <div class="col-6">
                        @if (auth()->user()->role == 'admin')
                            <a href="{{ url('home') }}"><img class="img-fluid w-25" src="/img/logo_login.png" alt=""></a>
                        @else
                            <a href="#"><img class="img-fluid w-25" src="/img/logo_login.png" alt=""></a>
                        @endif
                    </div>
                    <div class="col-6 row">
                        <div class="col-md-10 text-right my-auto">
                            <h5><strong>Bienvenido</strong></h5>
                            <h6 class="text-dark">
                                <strong>{{ Auth::user()->getFullNameAttribute() }}</strong>
                            </h6>
                            <a class="" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fas fa-power-off color-personalizado-verde1"></i> Cerrar sesión</a>
                        </div>
                        <div class="col-md-2 d-flex align-items-center">
                            {{-- <a href="{{ url('curriculum') }}"> --}}
                                <div class="photo-container shadow-1"
                                style="
                                background-image:url({!! asset('img/profile.png') !!});
                                background-size:cover;
                                background-position:center;
                                ">
                                </div>
                            {{-- </a> --}}
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <nav class="navbar navbar-expand-md navbar-dark navbar-laravel shadow-3">
            <div class="container-fluid col-md-10">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse text-white" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav d-flex justify-content-between w-100">
                        {{-- <li class="nav-item dropdown col">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                Nuestra Empresa <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-center bg-gray border-0 rounded-0 w-100" aria-labelledby="navbarDropdown">
                                <a href="{{ url('filosofia') }}" class="dropdown-item text-white dropdown-item-dark">Misión y Visión</a>
                                <a href="{{ url('organigrama-puestos') }}" class="dropdown-item text-white dropdown-item-dark">Plantilla</a>
                                <a href="{{ url('cumpleaños') }}" class="dropdown-item text-white dropdown-item-dark">Cumpleaños</a>
                                <a href="{{ url('organigrama') }}" class="dropdown-item text-white dropdown-item-dark">Organigrama</a>
                            </div>
                        </li> --}}
                        @if (empty(auth()->user()->external))
                          <li class="nav-item">
                            <a class="nav-link" href="{{url('clima-organizacional/que-es')}}">Clima Laboral</a>
                          </li>
                        @endif
                        {{-- <li class="nav-item dropdown col">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                Administración <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right bg-gray border-0 rounded-0 w-100" aria-labelledby="navbarDropdown">
                                @if(Auth::user()->hasRolePermission('user_admin'))
                                    <a href="{{ url('admin-de-usuarios') }}" class="dropdown-item text-white dropdown-item-dark">Administración de Usuarios</a>
                                @endif                                
                            </div>
                        </li> --}}
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{asset('login')}}">Iniciar Sesión</a>
                            </li>
                        @else
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <div class="row justify-content-center">
                <div class="col-md-8 text-center">
                    @include('flash::message')
                </div>
            </div>

            @if(View::hasSection('sidebar'))
                <div class="container-fluid col-md-10">
                    <div class="row">
                        <div class="col-md-2">
                            @yield('sidebar')
                        </div>
                        <div class="col-md-10">
                            @yield('content')
                        </div>
                    </div>
                </div>
            @else
                <div class="container-fluid col-md-10">
                    {{-- @yield('mainContent') --}}
                    @yield('content')
                </div>
            @endif
            
        </main>
    </div>
</body>

<footer style="background: #E6E7E8">
    <div class="container col-10">
        <div class="row d-flex justify-content-between p-5">
            <div class="col-md-6 col-sm-12">
                <div class="d-flex flex-row">
                    <i class="fas fa-map-marked-alt color-personalizado-verde1 pr-4 font-weight-bold" style="font-size: 2rem;"></i>
                    <p class="color-personalizado-azul h5">
                        Av. Américas 1297 Col. Providencia<br>
                        Guadalajara, Jalisco<br>
                        (33) 3540 1800
                    </p>
                </div>
             </div>

          {{--    <div class="col-2">
              <img src="{!! asset('img/footer_1.png') !!}" class="img-fluid w-100">
          </div>
          <div class="col-2">
              <img src="{!! asset('img/footer_2.png') !!}" class="img-fluid w-100">
          </div>
           --}}
             <div class="col-md-6 col-sm-12 text-md-right text-center text-white mt-sm-5 mt-md-0 mt-sm mt-5">
                {{-- <h5 class="text-md-right text-sm-center">Nuestras Redes Sociales</h5> --}}
                  <img src="{!! asset('img/footer_1.png') !!}" class="img-fluid w-25 mr-5"> 
                <img src="{!! asset('img/footer_2.png') !!}" class="img-fluid w-25"> 
            </div>
        </div>

        <div class="row d-flex align-items-end py-4">
            <div class="col-md-12 color-personalizado-azul text-center font-weight-bold">
                Copyright &reg;2021 by HallMG Internet Solutions.
            </div>
        </div>
    </div>
</footer>
</html>

{{-- @yield('mainScripts') --}}
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/highcharts.js') }}"></script>

<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>

<script type="text/javascript">
    $(document).ready( function () {
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    });
</script>

@yield('scripts')