@extends('layouts.app')

@section('content')
  <style type="text/css">
    .img-mosaico {padding: 0; margin: 0; width: 100%;}
  </style>
    <div class="row justify-content-center">
      <div class="col-md-12">
        @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['carrousel'],'section' => 'Banner'])
      </div>
      <br>
      <div class="col-md-12">
        @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['carrousel'],'section' => 'bienvenida'])
      </div>
      <div class="col-md-12 mt-3">
          <div class="row">
            <div class="col-8 px-0">
              @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['carrousel'],'section' => 'cuestionario'])
            </div>{{-- Se cierra div cuestionario --}}
            <div class="col-4 px-0">
              @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['carrousel'],'section' => 'nom035'])
            </div>{{-- Se cierra div nom 035 --}}
            <div class="col-md-12">
              @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['carrousel'],'section' => 'climalaboral'])
            </div>{{-- Se cierra div clima laboral  --}}
          </div>{{-- Se cierra div  row --}}
        </div>
    </div>
    @include('modals.video')
@endsection
