@extends('layouts.app')

@section('title', 'Organigrama')

@section('content')
	<div class="row my-5">
        <div class="col-md-12">
            <h4><strong style="color: #ED3237">{{ $titulo }}</strong><hr class="mt-1" style="border-bottom: 4px solid #373435;"></h4>
        </div>
		<div class="col">
			<div class="card shadow-2">
				{{-- <div class="card-header">
					<h2 class="my-auto font-weight-bold">Organigrama</h2>
				</div> --}}
				<div class="card-body " id="accordionExample">
					<div class="row justify-content-end">
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control" name="buscador" id="buscador" placeholder="Buscar">
                            </div>
                        </div>
                    </div>
					<hr>
                    <div class="row mb-5" id="accordionExample">
						{!!$list!!}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
<style type="text/css">
    [hidden] {
  display: none !important;
}
 /*ORGANIGRAMA*/
 div.treeview ul:first-child:before {
    display: none;
}
.treeview, .treeview ul {
    margin:0;
    padding:0;
    list-style:none;
    
    color: #369;
}
.treeview ul {
    margin-left:1em;
    position:relative
}
.treeview ul ul {
    margin-left:.5em
}
.treeview ul:before {
    content:"";
    display:block;
    width:0;
    position:absolute;
    top:0;
    left:0;
    border-left:1px solid;
    
    /* creates a more theme-ready standard for the bootstrap themes */
    bottom:15px;
}
.treeview li {
    margin:0;   
    padding:0 1em;
    line-height:2em;
    font-weight:700;
    position:relative
}

.treeview ul .c1:before{
    width:10px;
}

.treeview ul .c2:before{
    width:32px;
}

.treeview ul .c3:before{
    width:55px;
}

.treeview ul .c4:before{
    width:85px;
}

.treeview ul .c5:before{
    width:125px;
}

.treeview ul .c6:before{
    width:175px;
}

.treeview ul .c7:before{
    width:228px;
}

.treeview ul .c8:before{
    width:250px;
}

.treeview ul li:before {
    content:"";
    display:block;
    height:0;
    border-top:1px dashed;
    border-width: 3px;
    margin-top:-1px;
    position:absolute;
    top:1em;
    left:0
}
.tree-indicator {
    margin-right:5px;
    
    cursor:pointer;
}
.treeview li a {
    text-decoration: none;
    /*color:inherit;*/
    
    cursor:pointer;
}
.treeview li button, .treeview li button:active, .treeview li button:focus {
    text-decoration: none;
    color:inherit;
    border:none;
    background:transparent;
    margin:0px 0px 0px 0px;
    padding:0px 0px 0px 0px;
    outline: 0;
}

.orgChart{
    margin-bottom: 50px;
}

.img-orgChart {
    display: block;
    max-width: 50% !important;
    height: auto;
    float:left;
}
.popover{
    display: inline-block;
    white-space: nowrap;
    min-width: 300px;
    font-size: 1rem;
    box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
    border: 0;
}
.popover-header{
    background-color: #000000;
    color: #002C48;
    padding:5px;
}
.img-org{
    max-width: 100%;
    height: auto;
    border-radius: 10px;
}
</style>

<script type="text/javascript">

    $(document).ready(function(){
      $("#buscador").on("keyup", function() {
        var value = $(this).val().toLowerCase();

        $("#accordionExample .encont").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
          

        });
          
        if($(".encont").filter(":visible").length == 0){
                   $("#no_resultado").css("display", "block");
        }else{
                $("#no_resultado").css("display", "none");
         }


      });

    });

    
    /*var tmp = $.fn.popover.Constructor.prototype.show;
    $.fn.popover.Constructor.prototype.show = function() {
        tmp.call(this); if (this.options.callback) {
            this.options.callback();
        }
    }*/
    
    $(function(){
        $('[data-toggle="popover"]').popover({
            trigger:'click',
            html: true,
            callback: function () {
            $("#img_user").addClass("img-rounded img-responsive");
            
            //console.log("entro");
    }
        });
    });
    /**
 *  BootTree Treeview plugin for Bootstrap.
 *                       $("#img_user").addClass("img-rounded img-responsive");
                        console.log("entro");
 *  Based on BootSnipp TreeView Example by Sean Wessell
 *  URL:    http://bootsnipp.com/snippets/featured/bootstrap-30-treeview
 *
 *  
 *
 */
$.fn.extend({
    treeview:   function() {
        return this.each(function() {
            // Initialize the top levels;
            var tree = $(this);
            
            tree.addClass('treeview-tree');
            tree.find('li').each(function() {
                var stick = $(this);
            });

			tree.find('li').each(function () {


				var branch = $(this); //li with children ul
                //console.log($(this).children('ul').children('li').attr('class'));
				if($(this).children('ul').length > 0){

					// branch.prepend("<i class='tree-indicator glyphicon glyphicon-folder-open'></i>");
					branch.prepend("<i class='tree-indicator fas fa-users'></i>");
					branch.addClass('tree-branch');
					branch.on('click', function (e) {
						if (this == e.target) {
							var icon = $(this).children('i:first');
							
							icon.toggleClass("<i class=fas fa-chevron-down");
							$(this).children().children().toggle();
						}

					})
					branch.children().children().toggle();
					
					/**
					 *  The following snippet of code enables the treeview to
					 *  function when a button, indicator or anchor is clicked.
					 *
					 *  It also prevents the default function of an anchor and
					 *  a button from firing.
					 */
					branch.children('.tree-indicator, button, i').click(function(e) {
						branch.click();
						e.preventDefault();
					});

				}else{
					branch.prepend("<i class='tree-indicator ' style='color: blue !important'></i>");
				}

			 });
			
        });
    }
});

/**
 *  The following snippet of code automatically converst
 *  any '.treeview' DOM elements into a treeview component.
 */
$(window).on('load', function () {
   
    $('.treeview').each(function () {
        var tree = $(this);
        tree.treeview();
    });
    $(document).ready(function(){
    $("#open").trigger("click");
});
});
</script>
@endsection