@extends('layouts.app')

@section('content')
<h4 class="font-weight-bold my-4">CUMPLEAÑOS PRÓXIMOS 30 DÍAS</h4>
<div class="card card-2">
    <div class="card-header bg-white text-white p-3">
    </div>
    <div class="card-body">
        <div class="container-fluid">
            <div class="row">
                @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['banner']])
            </div>
            <div class="input-group mt-3 mb-3 card-2">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
                <input type="text" id="search" class="form-control" placeholder="Buscar Cumpleaños" aria-label="Recipient's username" aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <span class="input-group-text" id="basic-addon2"><i class="fas fa-search"></i></span>
                </div>
            </div>

            <div class="row my-5">
                @forelse($users as $user)
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="row">
                            <div class="col-3 p-0 mb-3" style="background-image:url('{{asset($user->photo)}}');
                                background-size:cover;
                                background-position:center;">
                                {{--<img class="img-fluid w-100 rounded" src="{{asset('/img/'.$user->photo)}}" alt="">--}}
                            </div>
                            <div class="col-8 col-md-9 pl-0 mb-3">
                                <p class="text-uppercase bg-yellow font-weight-bold pl-2"> {{isset($user->nombre)?$user->nombre:''}} {{isset($user->paterno)?$user->paterno:''}} {{isset($user->materno)?$user->materno:''}}</p>
                                <span class="pl-2">{{isset($user->job_position_id)?$user->job_position_id:'-'}}</span><br>
                                <span class="pl-2">{{isset($user->department_name)?$user->department_name:'-'}}</span><br>
                                <span class="pl-2">{{isset($user->correoempresa)?$user->correoempresa:'-'}}</span><br>
                                <span class="pl-2">Ofic. {{isset($user->telefono)?$user->telefono:'N/A'}}</span><br>
                                <span class="pl-2">Cel. {{isset($user->celular)?$user->celular:'N/A'}}</span><br>
                                <span class="pl-2">Fecha: {{Carbon\Carbon::parse(isset($user->nacimiento)?$user->nacimiento:'')->formatLocalized('%d de %B')}}</span><br>
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="col-12 col-md-12 text-center">
                        {{-- <div class="row text-center"> --}}
                            <h3>No se encontraron cumpleaños</h3>
                        {{-- </div> --}}
                    </div>
                @endforelse
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script>
$(function() {
    var data = [];
            @foreach($uSearch as $user )
                data.push('{{ $user->uS }}{{Carbon\Carbon::parse($user->nacimiento)->formatLocalized('%d de %B')}}');
            @endforeach
    $( "#search" ).autocomplete({
        source: data
    });
});
</script>
@endsection
