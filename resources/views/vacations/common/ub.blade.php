<div class="card my-4">
  <div class="card-header">
    <h3>Vacaciones</h3>
  </div>
  <div class="card-body">
      <h4>Fecha de ingreso: @if(isset($user)) {{$user->started_at}} @else {{Auth::user()->started_at}} @endif </h4>
      <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>Periodo</th>
                  <th>Fecha Inicio</th>
                  <th>Vigencia</th>
                  <th>Saldo</th>
                  <th>Autorizados</th>
                  <th>Solicitados</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
              @foreach ($vacations['rows'] as $vacation)
                <tr>
                  <td>{{$vacation->year - 1}} - {{$vacation->year}}</td>
                  <td>{{ date('d-m-Y', strtotime($vacation->created_at)) }}</td>
                  <td>{{ $vacation->until?date('d-m-Y', strtotime($vacation->until)):'' }}</td>
                  <td>{{$vacation->pending}}</td>
                  <td>{{$vacation->amount-$vacation->solicited}}</td>
                  <td>{{$vacation->solicited}}</td>
                  {{-- <td>{{$vacation->pending}}</td> --}}
                  <td>{{$vacation->pending - $vacation->amount}}</td>
                </tr>
              @endforeach
              </tbody>
            </table>
      </div>
      <h4 class="pull-right">Total: {{$vacations['diff']}} </h4>
  </div>
</div>
{{-- <div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        <h3>Días</h3>
      </div>        
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>Concepto</th>
                <th>Total</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($balances as $balance)
                @if($balance->benefit->type == 'day')
                  <tr>
                    <td>{{$balance->benefit->name}}</td>
                    <td>{{$balance->pending}}</td>
                  </tr>
                @endif
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        <h3>Tiempo</h3>  
      </div>    
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>Concepto</th>
                <th>Total</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($balances as $balance)
                @if($balance->benefit->type == 'time')
                  <tr>
                    <td>{{$balance->benefit->name}}</td>
                    <td>{{($balance->pending>60)?round($balance->pending/60,2).' H':($balance->pending).' M'}}</td>
                  </tr>
                @endif
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div> --}}