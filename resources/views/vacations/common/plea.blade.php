@extends('vacations.app')

@section('content')
<h2 class="text-danger bolder">CREAR SOLICITUD</h2>
<hr class="title_hr big_hr">
<form action="{{ url('common/plea') }}" method="POST">
    @csrf 
    <div class="form-group">
        <label for="benefit">Concepto</label>
        <select name="benefit_id" id="benefit" class="form-control" required="required">
            <option selected="selected" value="">Seleccione...</option>
            @if(!is_null($vacations['benefit']))
                @if($vacations['diff'] > 0)
                    <option value="{{$vacations['benefit']->id}}">{{$vacations['benefit']->name}} (Días restantes: {{$vacations['diff']}})</option>
                @else
                    <option value="{{$vacations['benefit']->id}}">{{$vacations['benefit']->name}}
                     </option>
                @endif
            @endif
            @foreach ($balances as $balance)
                @if($balance->benefit->context == 'user')
                    @if($balance->benefit->type=='day')
                        <option  value="{{$balance->benefit_id}}">
                            {{ $balance->benefit->name }}
                            @if($balance->benefit->continuous)
                                @if($balance->benefit->days === 0)
                                    {{ '(Indefinido)' }}
                                @else
                                (Saldo en días: {{ $balance->benefit->shortname === "BDAY"?"0.5)":'' . $balance->benefit->days . ')'}}
                                @endif
                            @endif
                        </option>
                    @elseif($balance->benefit->type=='pending' && $balance->benefit->id != 5)
                        @if(($balance->pending - $balance->amount)<=0)
                        <option value="{{$balance->benefit_id}}">
                            {{$balance->benefit->name}} 
                        </option>
                        @else <option value="{{$balance->benefit_id}}">{{$balance->benefit->name}} @if($balance->pending>0)(Días restantes: {{$balance->pending - $balance->amount}})@endif</option>
                        @endif
                    @endif
                @endif
            @endforeach
        </select>
    </div>       
    <div class="form-group">
        <label for="title">Descripción</label>
        <input type="text" name="title" class="form-control">
    </div>
    <div class="row">
        <div class="col-md-6">

            <label for="start">Fecha Inicio</label>
            <input type="date" id="start" name="start" class="form-control" required>
        </div>
        <div class="col-md-6">
            <label for="end">Fecha Final </label>
            <input type="date" id="end" name="end" class="form-control" required>
        </div>
    </div>
    <div class="my-4">
        <input type="submit" value="Crear" class="btn btn-primary">
    </div>





        <h2 class="text-danger bolder mt-5">TIPOLOGÍA DE ABSENTISMOS</h2>
        <hr class="title_hr big_hr">

        <div class="table-responsive">
            <table class="table table-hover" id="incidentsTable">
                <thead class="red_header">
                    <tr>
                    <th>Absentismo </th>
                    <th>Cantidad</th>
                    <th>Descripción</th>
                    <th>Fecha</th>
                    <th>Observaciones</th>
                    </tr>
                </thead>
                <tbody id="tbody">
                    @foreach($absentismos as $absentismo)
                        <tr>
                            <td>{{$absentismo->class}}</td>
                            <td>{{$absentismo->days}}</td>
                            <td>{{$absentismo->description}}</td>
                            <td>{{$absentismo->date}}</td>
                            <td>{{$absentismo->observation}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</form>

<div class="container my-4">
    <div class="card">
        <div class="card-body">
        <div id="calendar"></div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $(document).ready( function() {

           $('#calendar').fullCalendar({
            locale: 'es', 
            themeSystem: 'bootstrap4',
            themeName: 'journal',
            buttonText:{
                today:    'Hoy',
                month:    'Mes',
                week:     'Semana',
                day:      'Día',
                list:     'Lista'
            },
            eventSources: [
                {
                    url: '{{ url("getScheduleWithData") }}', // use the `url` property
                    type: 'GET',
                    color: '#a5d6a7',    // an option!
                    textColor: 'black',  // an option!
                    error: function() {
                        alert('Error al cargar el horario');
                    },
                },
            ],
            timeFormat: 'hh:mm A  -  ',
            fixedWeekCount: false,
            showNonCurrentDates: false,
        });
    });
</script>
@endsection