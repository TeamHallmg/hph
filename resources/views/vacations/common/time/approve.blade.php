@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        <h3 class="panel-title">Solicitudes de Tiempo Extra</h3>
    </div>

    <div class="card-body">        
        <form action="{{ route('common.time.approve.store') }}" method="POST">
            @csrf
            <table class="table" id="incidents">
                <thead>
                    <tr>
                        <th># Folio</th>
                        <th>Empleado</th>
                        <th>Dia</th>
                        <th>Turno</th>
                        <th>Motivo</th>
                        <th>Estatus</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($incidents as $incident)
                    <tr>
                        <td> {{ $incident->id }} </td>
                        <td> {{ $incident->from->FullName }} </td>
                        <td> {{ $incident->incidentOvertime->date }} </td>
                        <td> {{ $incident->incidentOvertime->schedule->shift }} </td>
                        <td> {{ $incident->comment }} </td>
                        <td>
                            <select name="requests[{{ $incident->id }}]" class="selectpicker">
                                <option value="">{{ __('incidents.'.$incident->status) }}</option>
                                <option value="accepted">Aprobar</option>
                                <option value="rejected">Rechazar</option>
                            </select>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <input type="submit" value="Guardar" class="btn btn-block btn-primary">
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('#incidents').DataTable({
        colReorder: true,
        responsive: true,
    });

</script>
@endsection