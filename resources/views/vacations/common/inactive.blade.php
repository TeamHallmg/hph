@extends('vacations.app')

@section('content')

<h2 class="text-danger bolder">HISTORIAL</h2>
<hr class="title_hr big_hr">
<h3 class="mt-4 text-danger bolder">Mis Solicitudes Inactivas</h3>
<hr class="title_hr small_hr">
<h4 class="mt-5 text-danger bolder">Solicitudes</h4>
<hr class="title_hr small_hr">
<div class="table-responsive">
    <table class="table table-hover" id="incidentsTable">
    <thead class="red_header">
        <tr>
            <th>Solicitud</th>
            <th>Estado</th>
        </tr>
    </thead>
    <tbody>
        @foreach($events as $event)
            <tr>
                <td>{{$event->title}}</td>
                <td>{{$event->status}}</td>
            </tr>
        @endforeach
    </tbody>
    </table>
</div>

@endsection

@section('scripts')
<script>
    var incidentsTable = $('#incidentsTable').DataTable();
</script>
@endsection