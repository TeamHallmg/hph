@extends('layouts.app')

@section('content')

<form action="{{ url('swapshift') }}" method="POST">
@csrf
<div class="row align-items-center">
	<div class="col-md-4">
		<div class="form-group text-center">
			<label for="userA">Usuario</label>
			<select name="userA" id="userA" class="selectpicker" data-live-search="true" required>
				@foreach ($myUsers as $user)
					<option value="{{ $user->id }}">{{ $user->FullName }}</option>
				@endforeach
			</select>
		</div>
		{{--  <div class="text-center my-4">
			<a class="btn btn-primary" data-toggle="collapse" href="#collapseA" role="button" aria-expanded="false" aria-controls="collapseA">
				{{ $myUsers[0]->FullName }}
			</a>
		</div>
		<div class="collapse" id="collapseA">
			<div class="card mb-4">
				<div class="card-body">
					<p>Nombre: {{ $myUsers[0]->FullName }}</p>
					<p>No. Empleado: {{ $myUsers[0]->number }}</p>
					<p>Correo: {{ $myUsers[0]->email }}</p>
				</div>
			</div>
		</div>  --}}
		<div id="calendarA" class=""></div>
	</div>
	<div class="col-md-4">
		<div class="text-center my-3">
			<button class="btn btn-primary">Generar Intercambio</button>
		</div>
		<h3 class="text-center"> Fechas para el intercambio </h3>
		<div class="card">
			<div class="card-header">
				<p><span class="empleadoA">{{ isset($myUsers[0])?$myUsers[0]->FullName:'' }}</span> intercambiara turno con '<span class="empleadoB">{{ isset($users[0])?$users[0]->FullName:'' }}</span>'</p>
			</div>
			<div class="card card-body">
				<p><span class="empleadoA">{{ isset($myUsers[0])?$myUsers[0]->FullName:'' }}</span> cubrira el turno de <span class="empleadoB">{{ isset($users[0])?$users[0]->FullName:'' }}</span> en la fecha</p>
				<input type="date" class="form-control readonly" name="dateB" id="dateB" required>
			</div>
			<div class="card card-body">
				<p><span class="empleadoB">{{ isset($users[0])?$users[0]->FullName:'' }}</span> cubrira el turno de <span class="empleadoA">{{ isset($myUsers[0])?$users[0]->FullName:'' }}</span> en la fecha</p>
				<input type="date" class="form-control readonly" name="dateA" id="dateA" required>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group text-center">
			<label for="userB">Usuario</label>
			<select id="userB" class="selectpicker" name="userB" data-live-search="true" required>
				@foreach ($users as $user)
					<option value="{{ $user->id }}">{{ $user->FullName }}</option>
				@endforeach
			</select>
		</div>
		{{--  <div class="text-center my-4">
			<a class="btn btn-primary" data-toggle="collapse" href="#collapseB" role="button" aria-expanded="false" aria-controls="collapseB">
				{{ $users[0]->FullName }}
			</a>
		</div>
		<div class="collapse" id="collapseB">
			<div class="card mb-4">
				<div class="card-body">
					<p>Nombre: {{ $users[0]->FullName }}</p>
					<p>No. Empleado: {{ $users[0]->number }}</p>
					<p>Correo: {{ $users[0]->email }}</p>
				</div>
			</div>
		</div>  --}}
		<div id="calendarB"></div>
	</div>
</div>
</form>

{{--  <style>
	.btn-date{
		display: block;
		width: 100%;
		margin: .5rem 0px;
	}

	.date-shift-Nocturno:hover{
		background: #0288D1;
	}
	.date-shift-Nocturno{
		background: #03A9F4;
	}
	.date-shift-Vespertino:hover{
		background: #F57C00;
	}
	.date-shift-Vespertino{
		background: #FF9800;
	}
	
	.date-shift-Matutino:hover{
		background: #FBC02D;
	}
	.date-shift-Matutino{
		background: #FFEB3B;
	}
	.date-labor-0:hover{
		background: #757575;
	}
	.date-labor-0{
		background: #BDBDBD;
	}

</style>

<div class="card">
    <div class="card-header">
        <h3>Crear solicitud</h3>
    </div>
    <div class="card-body">
		<div class="row">
			<div class="col-md-8">
	            <div class="form-group col-md-6">
	            	<h4>Datos Empleado Intercambio</h4>
	            	<input type="hidden" id="user">
	            	<div class="form-group">
						<label for="name">Nombre</label>
						<input type="text" name="name" class="form-control" readonly id="nameid">
	            	</div>
	            	<div class="form-group">
						<label for="email">Correo</label>
						<input type="text" name="email" class="form-control" readonly id="emailid">
	            	</div>
	            	<div class="form-group">
						<label for="department">Departamento</label>
						<input type="text" name="department" class="form-control" readonly id="depid">
		            </div>
	            	<div class="form-group">
						<label for="position">Puesto</label>
						<input type="text" name="position" class="form-control" readonly id="posid">
		            </div>
	            </div>
	            <div class="form-group col-md-6">
					<h4 id="h4Horario">Horario</h4>
					<button class="btn" style="width: 100%" disabled>-7 dias</button>
					<div id="divDates">
					</div>
					<button class="btn" style="width: 100%" disabled>+7 dias</button>
	            </div>
            </div>
            <div class="form-group col-md-4">
            	<h4>Empleados</h4>
            	<div class="form-group">
					<label for="user">Buscar Empleado</label>
					<select name="user" class="form-control selectpicker" data-live-search='true'>
						@foreach ($users as $key => $user)
							<option value="{{ $key }}"> {{ $user }} </option>
						@endforeach
					</select>
			
				</div>
				<div class="form-group">
					<label for="date">Fecha</label>
					<input type="date" name="date" id="datePick" class="form-control">						
				</div>
				<button class="btn btn-success" id="btn-buscar">Traer info</button>
            </div>
        </div>
    </div>
    <div class="text-center">
		<form action="{{ url('swapshift_preview') }}" method="POST" id="form">
			<input type="hidden" name="user" id="huser">
    		<input type="hidden" name="date" id="hdate">    		
    	</form>
    </div>
</div>  --}}

@endsection

@section('scripts')
<script>
	$('#calendarA').fullCalendar({
		locale: 'es', 
            themeSystem: 'bootstrap4',
            themeName: 'journal',
            buttonText:{
                today:    'Hoy',
                month:    'Mes',
                week:     'Semana',
                day:      'Día',
                list:     'Lista'
            },
            eventSources: [
                {
                    url: '{{ url("getSchedule") }}', // use the `url` property
					type: 'GET',
					data: function() {
						return {
							userid: $('#userA').val(),
						};
					},
                    color: '#a5d6a7',    // an option!
                    textColor: 'black',  // an option!
                    error: function() {
                        alert('Error al cargar el horario');
                    },
                },
			],
			allDaySlot: false,
            timeFormat: 'hh:mm A  -  ',
            fixedWeekCount: false,
			showNonCurrentDates: false,
			dayClick: function(date, jsEvent, view) {
				$('#spanA').html(date.format());
				$('#dateA').val(date.format());
			},
			navLinks: true,
			navLinkDayClick: function(date, jsEvent) {
				$('#spanA').html(date.format());
				$('#dateA').val(date.format());
			},
	});
	$('#calendarB').fullCalendar({
		locale: 'es', 
            themeSystem: 'bootstrap4',
            themeName: 'journal',
            buttonText:{
                today:    'Hoy',
                month:    'Mes',
                week:     'Semana',
                day:      'Día',
                list:     'Lista'
            },
            eventSources: [
                {
                    url: '{{ url("getSchedule") }}', // use the `url` property
					type: 'GET',
					data: function() {
						return {
							userid: $('#userB').val(),
						};
					},
                    color: '#a5d6a7',    // an option!
                    textColor: 'black',  // an option!
                    error: function() {
                        alert('Error al cargar el horario');
                    },
                },
			],
			dayClick: function(date, jsEvent, view) {
				$('#spanB').html(date.format());
				$('#dateB').val(date.format());
			},
			navLinks: true,
			navLinkDayClick: function(date, jsEvent) {
				$('#spanB').html(date.format());
				$('#dateB').val(date.format());
			},
			allDaySlot: false,
            timeFormat: 'hh:mm A  -  ',
            fixedWeekCount: false,
            showNonCurrentDates: false,
	});
	$('#userB').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
		$('.empleadoB').html($(this).find("option:selected").text());
		$('#calendarB').fullCalendar('refetchEvents');
	});
	$('#userA').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
		$('.empleadoA').html($(this).find("option:selected").text());
		$('#calendarA').fullCalendar('refetchEvents');
	});

</script>
	{{--  <script>
		var select = $('.selectpicker').selectpicker();

		$('#divDates').on('click', '.btn-date', function() {
			var proceed = confirm('Desea elegir este Dia para el intercambio');
			if(proceed){
				$('#hdate').val(this.id);
				$('#form').submit();
			}
		})

		$('#btn-buscar').on('click', function(){

			var userid = select.selectpicker('val');
			var date = $('#datePick').val();
			if(date.length > 0){
	            $.ajax({
	                headers: { 'X-CSRF-TOKEN': '{{csrf_token()}}' },
	                type: 'POST',
	                url: '{{url('userInfo')}}',
	                async: false,
	                data: {userid, date},
	                success: function(data){
	                    var val = JSON.parse(data);
	                    if (val.success) {
	                    	$('#huser').val(val.id);
	                    	$('#user').val(val.id);
	                        $('#nameid').val(val.name);
	                        // $('#numid').val(val.number);
	                        // $('#typeid').val(val.type);
	                        // $('#cateid').val(val.category);
	                        $('#emailid').val(val.email);
	                        $('#deptid').val(val.department);
	                        $('#posid').val(val.position);
	                        // $('#myTurn').val(val.myShift);
	                        // $('#turns').empty();
	                        // $.each(val.shifts ,function(key,value){
	                        //     $('#turns').append('<option value=' + key + '>' + value + '</option>')
	                        // });
	                    }else{
	                        alert(val.error);
	                    }
	                }
	            });

	            $.ajax({
	                headers: { 'X-CSRF-TOKEN': '{{csrf_token()}}' },
	                type: 'POST',
	                url: '{{url('userShiftWeek')}}',
	                async: false,
	                data: {userid, date},
	                success: function(data){
	                    var val = JSON.parse(data);
	                    if (val.success) {
	                    	$('#divDates').empty();
	                    	// $('#h4Horario').html($('#h4Horario').html() + " " + ;
	                    	$.each(val.data, function (i, item){
	                    		var data = (item['labor'] == 1)?item['shift']:'Descanso';
	                    		$('#divDates').prepend(
	                    			'<button '
	                    			+'class="btn btn-date date-labor-' + item['labor']
	                    			+' date-shift-' + item['shift']
	                    			+ '" id = "' + i
	                    			+ '">'
	                    			+ item['date'] + '   (' + data +')'
	                    			+ '</button>'
	                    		);
	                    	});
	                    }else{
	                        alert(val.error);
	                    }
	                }
	            });
			}
		});		
	</script>  --}}

@endsection