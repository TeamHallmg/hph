@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        <h3>Saldos</h3>
    </div>
    <div class="card-body">
        Resumen de acumulados del año ({{ date('Y') }})
    </div>
</div>

@include('vacations.common.ub')

@endsection
