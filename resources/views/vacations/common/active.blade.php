@extends('vacations.app')

@section('content')

<h2 class="text-danger bolder">MIS SOLICITUDES</h2>
<hr class="title_hr big_hr">
<h3 class="mt-4 text-danger bolder">Mis Solicitudes Activas</h3>
<hr class="title_hr small_hr">
<div id="alert"></div>
<h3 class="mt-5 text-danger bolder">Solicitudes</h3>
<hr class="title_hr small_hr">
<div class="table-responsive">
    <table class="table table-hover" id="incidentsTable">
        <thead class="red_header">
            <tr>
            <th>Solicitud</th>
            <th>Estado</th>
            <th>Cancelar</th>
            </tr>
        </thead>
        <tbody id="tbody">
            @foreach($events as $event)
                <tr>
                    <td>{{$event->title}}</td>
                    <td>{{$event->status}}</td>
                    <td>
                        <button data-id="{{ $event->id }}" data-title="{{ $event->title }}" class="btn btn-outline-danger rmvIncident">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection

@section('scripts')
<script>
    var incidentsTable = $('#incidentsTable').DataTable();
    var destroyUrl = '{{ url("common/plea/") }}';
    $(document).ready(function () {
        $('#tbody').on('click', '.rmvIncident', function(){
            var r = confirm('¿Esta seguro de eliminar: ' + $(this).data('title') + '?');
            if(r){
                var id = $(this).data('id');
                var self = this;
                axios
                    .delete(destroyUrl + '/' + id)
                    .then( function(res) {
                        incidentsTable
                            .row( $(self).parents('tr') )
                            .remove()
                            .draw();
                    })
                    .catch( function(error){
                        alert('No se pudo borar la incidencia');
                    });
            }            
        });
    });

    function destroy(id){
        var title = $('#row'+id+' td:first-child').text();
        var r = confirm('¿Esta seguro de eliminar: '+title+'?');
        if(r){
            $.ajax({
                headers: { 'X-CSRF-TOKEN': '{{csrf_token()}}' },
                url: "/common/plea/"+id,
                async: false,
                method: "DELETE",
                dataType: "json"
            }).done(function( data ) {
                if(data.success == 1) {
                    $('#alert').html(
                        '<div class="alert alert-warning alert-dismissible fade in" role="alert">'+
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                            '<span aria-hidden="true">&times;</span>'+
                        '</button>'+
                        '<strong>Registro'+data.error+' cancelado.</strong>'+
                        '</div>'
                    );
                    $("#row"+id).css('display','none');
                } else {
                    $('#alert').html(
                        '<div class="alert alert-warning alert-dismissible fade in" role="alert">'+
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                            '<span aria-hidden="true">&times;</span>'+
                        '</button>'+
                        '<strong>Hubo un error al calcelar el registro.'+data.error+'</strong>'+
                        '</div>'
                    );
                }
            });
        }
    }
</script>
@endsection
