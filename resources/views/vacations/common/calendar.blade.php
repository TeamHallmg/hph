@extends('layouts.app')

@section('content')

<div class="panel panel-default panel-custom">
    <div class="panel-heading panel-heading-custom">
        <span class="panel-custom-first-{{config('config.theme')}} calendar-icon"><h3 class="panel-title"> Calendario</h3></span><span class="panel-custom-second-{{config('config.theme')}}"></span>
        
    </div>

    <div class="panel-body panel-body-custom">
        @include('common.cdesc')
    </div>
</div>

<div id="app">yhf
	<calendar lang="" startdate="" firstday=""></calendar>
</div>
@include('common.cmodal')

@endsection
