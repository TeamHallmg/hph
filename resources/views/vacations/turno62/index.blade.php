@extends('layouts.app')

@section('content')
	
	<style>
		td:hover{
			background-color: #BABABA;
		}
		tbody > tr:hover{
			background-color: #FF961B;
		}
	</style>

@php
	$cont = 0;
@endphp

<div class="card card-3">
    <div class="card-header">
    	<h3 class="panel-title"> Crear calendario <i class="far fa-calendar-alt"></i></h3>
    </div>
    <div class="card-body">
		<div style="margin-bottom: 2rem">
			<h4>Calendario de la jornada laboral "6 x 2" para el año {{date('Y')}}</h4>
		</div>
		<div class="row">
			<div class="col-md-6">
				<a class="btn btn-primary btn-block" href="{{url('workshift/create')}}">Crear Calendario</a>
			</div>
			<div class="col-md-6">
				<form action="{{ url('workshift/'. date('Y')) }}" method="POST">
					@csrf
					<input type="hidden" name="_method" value="DELETE">
					<input type="submit" value="Eliminar Calendario" class="btn btn-danger btn-block" onclick="return confirm('Quieres Borrar el Calendario ??')" {{ $year?'':'disabled' }}>		
				</form>
			</div>
		</div>
    </div>

</div>
@if($year)
<div class="card card-3 py-3 my-4">
	<div class="text-center">
		@foreach($year as $monthName => $month)
			<button class="btn btn-outline-primary btn-month" id="{{ $monthName }}" role="button" data-toggle="collapse" href="#collapseExample{{ $monthName }}" aria-expanded="false" aria-controls="collapseExample">
				{{ $listMonths[$monthName - 1] }}
			</button>
		@endforeach
	</div>
</div>
<div>
	<div>
	@foreach($year as $monthName => $month)
		<a class="btn btn-primary btn-inner-month" role="button" id="btn-{{ $monthName }}" data-toggle="collapse" href="#collapseExample{{ $monthName }}" aria-expanded="false" aria-controls="collapseExample" style="display: none">
			<h4>{{ $listMonths[$monthName - 1] }}</h4>
		</a>
		<div class="collapse" id="collapseExample{{ $monthName }}" style="overflow-x: auto">
			<table class="table">
				<thead class="thead-dark">
					<tr>
						<th>Lunes</th>
						<th>Martes</th>
						<th>Miercoles</th>
						<th>Jueves</th>
						<th>Viernes</th>
						<th>Sabado</th>
						<th>Domingo</th>
					</tr>
				</thead>
				<tbody>
					@foreach($month as $week)
						<tr>
							@foreach($week as $number => $day)
								@php
									if(explode('-', $day[0]->date)[2] == '01'){
										$wk = date('N', strtotime($day[0]->date)) - 1;
										for($i = 0; $i < $wk; $i++){
											echo "<td></td>";
										}
									}
								@endphp
								<td>
									@foreach($day as $ker => $line)
										@if($line->labor)
											<div style="background-color: {{$colors[$line->region_id]}}">{{$line->date}}</div>
										@else
											<div class="{{$line->region_id}}" style="background-color: #99a394;">Dia de Descanso</div>
										@endif	
									@endforeach
								</td>							
							@endforeach
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	@endforeach
	</div>
</div>
@else
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8 text-center">
			<div class="alert alert-info">
				<h4><strong>Info!</strong> Por el momento no ha creado un calendario.</h4>
			</div>
		</div>	
		<div class="col-md-2"></div>
	</div>
@endif
@endsection
@section('scripts')
	<script>
		$(document).ready(function () {
			var role = "0";
			$('#selectRole').change(function (){
				$('.'+role).css('display','none');
				var actualRole = $(this).val();
				$('.'+actualRole).css('display','');
				role = actualRole;
			});

			$('.btn-month').on('click', function (){
				if($('#btn-' + this.id).css('display') == 'none'){
					$('#btn-' + this.id).show();
				}else{
					$('#btn-' + this.id).hide();
				}
			});

			$('.btn-inner-month').on('click', function (){
				if($(this).css('display') == 'none'){
					$(this).show();
				}else{
					$(this).hide();
				}
			});

		});
	</script>
@endsection