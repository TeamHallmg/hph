@extends('layouts.app')

@section('content')
    
<table class="table" id="clocklog">
    <thead>
        <tr>
            <th>Checada</th>
            <th>Hora más cercana</th>
            <th>Diferencía (Min)</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($clocklogs as $clocklog)
            @if($clocklog->diff < 0)
                <tr class="table-success">
            @elseif($clocklog->diff >= 10)
                <tr class="table-danger">
            @else
                <tr class="table-warning">
            @endif
                <td>{{ $clocklog->date }}</td>
                <td>{{ $clocklog->closest }}</td>
                <td>{{ $clocklog->diff }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

@endsection

@section('scripts')
<script>
    var clocklog = $('#clocklog').DataTable();
</script>
@endsection