<li class="nav-item dropdown">
  <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="navbarDropdownAdmin" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> @lang('menus.administration')</a>
  <div class="dropdown-menu" aria-labelledby="navbarDropdownAdmin">
    <a class="dropdown-item" href="{{ url('/super/approve/requests') }}">
      <span class="glyphicon glyphicon-check" aria-hidden="true"></span> 
      @lang('menus.appruverequest')
    </a>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="{{url('authorization_groups')}}">
      <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
      Proceso de Autorización
    </a>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="{{url('workshift')}}">
      <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
      Ver Jornada 6x2
    </a>
    <a class="dropdown-item" href="{{url('admin/schedule')}}">
      <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
      Ver Jornada 5x2
    </a>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="{{url('admin/export')}}">
      <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
      Exportar Nominpaq
    </a>
    <a class="dropdown-item" href="{{url('admin/export/extras')}}">
      <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
      Exportar Horas Extras
    </a>
    <div class="dropdown-divider"></div>
    {{--  <a class="dropdown-item" href="{{ url('super/view/incidents') }}">
      <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
      @lang('menus.viewincidents')
    </a>  --}}
    <a class="dropdown-item" href="{{ url('super/view/requests') }}">
      <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
      @lang('menus.viewrequests')
    </a>
    <a class="dropdown-item" href="{{ url('regions') }}">
      <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
      Ver Regiones
    </a>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="{{url('groups')}}">
      <span class="glyphicon glyphicon-tasks" aria-hidden="true"></span>
      Grupos
    </a>
    <a class="dropdown-item" href="{{ url('super/users') }}">
      <span class="glyphicon glyphicon-tasks" aria-hidden="true"></span>
      @lang('menus.users')
    </a>
    <a class="dropdown-item" href="{{ route('admin.time')}}">
      <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
      Tiempos Extras
    </a>
    {{--  <a class="dropdown-item" href="{{ url('admin/masive') }}">
      <span class="glyphicon glyphicon-tasks" aria-hidden="true"></span>
      @lang('menus.masive')
    </a>  --}}
    <a class="dropdown-item" href="{{ url('admin/incidents') }}">
      <span class="glyphicon glyphicon-tasks" aria-hidden="true"></span>
      @lang('menus.incidents')
    </a>
  </div>
</li>
<li class="nav-item dropdown">
  <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="navbarDropdownAdmin" role="button" aria-haspopup="true" aria-expanded="false">
    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
    Eventos
  </a>
  <div class="dropdown-menu" aria-labelledby="navbarDropdownAdmin">
      <a class="dropdown-item" href="{{ url('super/events/create') }}">
          Crear nuevo
      </a>
      <a class="dropdown-item" href="{{ url('super/events') }}">
          Cancelar
      </a>
  </div>
</li>