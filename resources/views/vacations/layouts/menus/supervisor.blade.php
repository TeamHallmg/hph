<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="navbarDropdownAdmin" role="button" aria-haspopup="true" aria-expanded="false">
      <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
      @lang('menus.supervise')
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownAdmin">
        <a class="dropdown-item" href="{{ url('super/approve/requests') }}">
            @lang('menus.appruverequest')
        </a>
        <div class="dropdown-divider"></div>
        {{-- <a class="dropdown-item" href="{{ url('super/view/incidents') }}">
            @lang('menus.viewincidents')
        </a> --}}
        <a class="dropdown-item" href="{{ url('super/view/requests') }}">
            @lang('menus.viewrequests')
        </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="{{ url('super/users') }}">
            @lang('menus.users')
        </a>
    </div>
</li>
<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="navbarDropdownAdmin" role="button" aria-haspopup="true" aria-expanded="false">
      <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
      Eventos
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownAdmin">
        <a class="dropdown-item" href="{{ url('super/events/create') }}">
            Crear nuevo
        </a>
        <a class="dropdown-item" href="{{ url('super/events') }}">
            Cancelar
        </a>
    </div>
</li>

{{--  <li class="nav-item dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span>  @lang('menus.supervise')<span class="caret"></span></a>
    <ul class="dropdown-menu">
    <li><a href="{{App::make('url')->to('/')}}/super/approve/requests"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> @lang('menus.appruverequest')</a></li>
    <li><a href="{{App::make('url')->to('/')}}/super/approve/incidents"><span class="glyphicon glyphicon-check" aria-hidden="true"></span>  @lang('menus.appruveincident')</a></li>
    <li role="separator" class="divider"></li>
    <li><a href="{{App::make('url')->to('/')}}/super/substitute"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>  @lang('menus.substitute')</a></li>
    <li><a href="{{App::make('url')->to('/')}}/super/view/incidents"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> @lang('menus.viewincidents')</a></li>
    <li role="separator" class="divider"></li>
    <li><a href="{{App::make('url')->to('/')}}/super/users"><span class="glyphicon glyphicon-tasks" aria-hidden="true"></span> @lang('menus.users')</a></li>
    <li><a href="{{App::make('url')->to('/')}}/super/view/requests"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> @lang('menus.viewrequests')</a></li>
    </ul>
</li>
<li class="nav-item dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> @lang('menus.timerequest')<span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li><a href="{{App::make('url')->to('/')}}/common/time/create">Crear</a></li>
        <li><a href="{{App::make('url')->to('/')}}/common/time">Historial</a></li>
    </ul>
</li>
<li class="nav-item dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> @lang('menus.events')<span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li><a href="{{App::make('url')->to('/')}}/super/events/create"> @lang('menus.createnew')</a></li>
        <li><a href="{{App::make('url')->to('/')}}/super/events"> @lang('menus.cancel')</a></li>
    </ul>
</li>      --}}