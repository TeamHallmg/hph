@include('layouts.header')

@include('layouts.navbar')

<!-- app -->
<div class="my-4 mx-5" id="app">
    @include('flash::message')
    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif
    @yield('content')
</div>
<!-- /app -->

@include('layouts.footer')
