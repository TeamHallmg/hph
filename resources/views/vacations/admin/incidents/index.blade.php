@extends('layouts.app')

@section('content')

<div class="card card-3">
    <div class="card-header">
        Incidencias
    </div>
    <div class="card-body">
        <div class="row justify-content-center mb-4">
            <div class="col-md-4">
                <a href="{{ url('admin/incidents/create') }}" class="btn btn-block btn-primary">Crear</a>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-hover datatable">
                <thead>
                    <tr>
                    <th>Nombre corto</th>
                    <th>Descripción</th>
                    <th>Operaciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($benefits as $benefit)
                        <tr>
                            <td>{{$benefit->shortname}}</td>
                            <td>{{$benefit->name}}</td>
                            <td>
                                <a title="Editar" alt="Editar" class="btn btn-success" href="{{ url('admin/incidents/' . $benefit->id . '/edit') }}">
                                    Editar
                                </a>
                                <a title="Eliminar" alt="Eliminar" class="btn btn-danger" href="{{ url('admin/incidents/' . $benefit->id) }}">
                                    Eliminar
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
