<div class="row my-3">
    <div class="col-md-12">
        <label for='name'>Texto</label>
        <input type="text" name='name' class='form-control' value="{{ isset($benefit)?$benefit->name:old('name') }}" required>
        <input type="hidden" name='relationship' value="1">
        <input type="hidden" name='retype' value=''>
    </div>
</div>
<div class="row my-3">
    <div class="col-md-3">
        <label for='code'>Código</label>
        <input type="text" name='code' class='form-control' value="{{ isset($benefit)?$benefit->code:old('code') }}" required maxlength='10'>
    </div>
    <div class="col-md-3">
        <label for='shortname'>Nombre corto</label>
        <input type="text" name='shortname' class='form-control' value="{{ isset($benefit)?$benefit->shortname:old('shortname') }}" required maxlength='10'>
    </div>
    <div class="col-md-3">
        <label for='context'>Contexto</label>
        @if(isset($benefit))
            <select name="context" class="form-control">
                <option value="admin" {{ $benefit->context == 'admin'?'selected':'' }}>Administrador</option>
                <option value="user" {{ $benefit->context == 'user'?'selected':'' }}>Usuario</option>
                <option value="super" {{ $benefit->context == 'super'?'selected':'' }}>Supervisor</option>
            </select>
        @else
            <select name="context" class="form-control">
                <option value="admin" {{ old('context') == 'admin'?'selected':'' }}>Administrador</option>
                <option value="user" {{ old('context') == 'user'?'selected':'' }}>Usuario</option>
                <option value="super" {{ old('context') == 'super'?'selected':'' }}>Supervisor</option>
            </select>
        @endif
    </div>
    <div class="col-md-3">
        <label for='type'>Comportamiento</label>
        @if(isset($benefit))
            <select name="type" class="form-control">
                <option value="time" {{ $benefit->type == 'time'?'selected':'' }}>Tiempo</option>
                <option value="day" {{ $benefit->type == 'day'?'selected':'' }}>Día</option>
                <option value="pending" {{ $benefit->type == 'pending'?'selected':'' }}>Pendiente</option>
            </select>
        @else
            <select name="type" class="form-control">
                <option value="time" {{ old('type') == 'time'?'selected':'' }}>Tiempo</option>
                <option value="day" {{ old('type') == 'day'?'selected':'' }}>Día</option>
                <option value="pending"{{ old('type') == 'pending'?'selected':'' }} >Pendiente</option>
            </select>
        @endif

    </div>
</div>
<div class="row mt-4 justify-content-center">
    @if(isset($benefit))
        <div class="col-md-2">
            <input type="hidden" name='continuous' value="0">
            <input type="checkbox" name="continuous" value="1" {{ $benefit->continuous == '1'?'checked':'' }}>
            <label for='continuous'>Continuo</label>
        </div>
        <div class="col-md-2">
            <input type="hidden" name='cronjob' value="0">
            <input type="checkbox" name='cronjob' value="1" {{ $benefit->cronjob == '1'?'checked':'' }}>
            <label for='cronjob'>Cron</label>
        </div>
        <div class="col-md-2">
            <input type="hidden" name='remove' value="0">
            <input type="checkbox" name='remove' value="1" {{ $benefit->remove == '1'?'checked':'' }}>
            <label for='remove'>Vence</label>
        </div>
        <div class="col-md-2">
            <input type="hidden" name='blocked' value="0">
            <input type="checkbox" name='blocked' value="1" {{ $benefit->blocked == '1'?'checked':'' }}>
            <label for='blocked'>Bloquea</label>
        </div>
        <div class="col-md-2"> 
            <input type="hidden" name='report' value="0">
            <input type="checkbox" name='report' value="1" {{ $benefit->report == '1'?'checked':'' }}>
            <label for='report'>Reporta</label>
            @if(config('config.export') == 'flexiform')
                <label for='retype'>Tipo</label>
                <select name="retype">
                    <option value="sum" {{ $benefit->retype == 'sum'?'selected':'' }}>Suma</option>
                    <option value="week" {{ $benefit->retype == 'week'?'selected':'' }}>Semana</option>
                </select>
            @endif
        </div>
    @else
        <div class="col-md-2">
            <input type="hidden" name='continuous' value="0">
            <input type="checkbox" name="continuous" value="1" {{ old('continuous') == '1'?'checked':'' }}>
            <label for='continuous'>Continuo</label>
        </div>
        <div class="col-md-2">
            <input type="hidden" name='cronjob' value="0">
            <input type="checkbox" name='cronjob' value="1" {{ old('cronjob') == '1'?'checked':'' }}>
            <label for='cronjob'>Cron</label>
        </div>
        <div class="col-md-2">
            <input type="hidden" name='remove' value="0">
            <input type="checkbox" name='remove' value="1" {{ old('remove') == '1'?'checked':'' }}>
            <label for='remove'>Vence</label>
        </div>
        <div class="col-md-2">
            <input type="hidden" name='blocked' value="0">
            <input type="checkbox" name='blocked' value="1" {{ old('blocked') == '1'?'checked':'' }}>
            <label for='blocked'>Bloquea</label>
        </div>
        <div class="col-md-2"> 
            <input type="hidden" name='report' value="0">
            <input type="checkbox" name='report' value="1" {{ old('report') == '1'?'checked':'' }}>
            <label for='report'>Reporta</label>
            @if(config('config.export') == 'flexiform')
                <label for='retype'>Tipo</label>
                <select name="retype">
                    <option value="sum" {{ old('retype') == 'sum'?'selected':'' }}>Suma</option>
                    <option value="week" {{ old('retype') == 'week'?'selected':'' }}>Semana</option>
                </select>
            @endif
        </div>
    @endif
</div>
<div class="row my-3">
    <div class="col-md-3">
        <label for='days'>Días</label>
        <input type="text" name='days' class='form-control' value="{{ isset($benefit)?$benefit->days:old('days') }}" required maxlength='10'>
    </div>
    <div class="col-md-3">
        <label for='group'>Grupo</label>
        @if(isset($benefit))
            <select name="group" class="form-control">
                <option value="incident" {{ $benefit->group == 'incident'?'selected':'' }}>Incidencia</option>
                <option value="benefit" {{ $benefit->group == 'benefit'?'selected':'' }}>Prestación</option>
                <option value="incapacity" {{ $benefit->group == 'incapacity'?'selected':'' }}>Incapacidad</option>
            </select>
        @else
            <select name="group" class="form-control">
                <option value="incident" {{ old('group') == 'incident'?'selected':'' }}>Incidencia</option>
                <option value="benefit" {{ old('group') == 'benefit'?'selected':'' }}>Prestación</option>
                <option value="incapacity" {{ old('group') == 'incapacity'?'selected':'' }}>Incapacidad</option>
            </select>
        @endif
    </div>
    <div class="col-md-3">
        <label for='frequency'>Frecuencia</label>
        @if(isset($benefit))
            <select name="frequency" class="form-control">
                <option value="yearly" {{ $benefit->frequency == 'yearly'?'selected':'' }}>Anual</option>
                <option value="monthly" {{ $benefit->frequency == 'monthly'?'selected':'' }}>Mensual</option>
                <option value="weekly" {{ $benefit->frequency == 'weekly'?'selected':'' }}>Semanal</option>
                <option value="daily" {{ $benefit->frequency == 'daily'?'selected':'' }}>Diario</option>
            </select>
        @else
            <select name="frequency" class="form-control">
                <option value="yearly" {{ old('frequency') == 'yearly'?'selected':'' }}>Anual</option>
                <option value="monthly" {{ old('frequency') == 'monthly'?'selected':'' }}>Mensual</option>
                <option value="weekly" {{ old('frequency') == 'weekly'?'selected':'' }}>Semanal</option>
                <option value="daily" {{ old('frequency') == 'daily'?'selected':'' }}>Diario</option>
            </select>
        @endif
    </div>
    <div class="col-md-3">
        <label for='gender'>Sexo</label>
        @if(isset($benefit))
            <select name="gender" class="form-control">
                <option value="x" {{ $benefit->gender == 'x'?'selected':'' }}>Indistinto</option>
                <option value="m" {{ $benefit->gender == 'm'?'selected':'' }}>Masculino</option>
                <option value="f" {{ $benefit->gender == 'f'?'selected':'' }}>Femenino</option>
            </select>
        @else
            <select name="gender" class="form-control">
                <option value="x" {{ old('gender') == 'x'?'selected':'' }}>Indistinto</option>
                <option value="m" {{ old('gender') == 'm'?'selected':'' }}>Masculino</option>
                <option value="f" {{ old('gender') == 'f'?'selected':'' }}>Femenino</option>
            </select>
        @endif
    </div>
</div>
