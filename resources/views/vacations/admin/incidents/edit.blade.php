@extends('layouts.app')

@section('content')

<div class="card card-3">
    <div class="card-header">
        Editar incidencias
    </div>
    <div class="card-body">
        <form action="{{ url('admin/incidents/' . $benefit->id) }}" method="POST">
            @csrf
            <input type="hidden" name="_method" value="PUT">
            @include('admin.incidents.form')
            <div class="text-right">
                <input type="submit" value="Guardar" class="btn btn-success">
                <a href="{{ url('admin/incidents') }}" class="btn btn-primary">Regresar</a>
            </div>
        </form>
    </div>
</div>

@endsection
