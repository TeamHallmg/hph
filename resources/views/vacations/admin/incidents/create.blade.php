@extends('layouts.app')

@section('content')

<div class="card card-3 my-4">
    <div class="card-header">
        <h3>Crear</h3>
    </div>
    <div class="card-body">
        <form action="{{ url('admin/incidents') }}" method="POST">
            @csrf
            @include('admin.incidents.form')
            <div class="text-right">
                <input type="submit" value="Crear" class="btn btn-success">
                <a href="{{ url('admin/incidents') }}" class="btn btn-primary">Regresar</a>
            </div>
        </form>
    </div>
</div>

@endsection
