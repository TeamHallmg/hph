@extends('layouts.app')

@section('content')

<div class="panel panel-default panel-custom">
    <div class="panel-heading panel-heading-custom">
        <span class="panel-custom-first-{{config('config.theme')}} incidents-icon"><h3 class="panel-title"> Incidencias</h3></span><span class="panel-custom-second-{{config('config.theme')}}"></span>

    </div>

    <div class="panel-body panel-body-custom">
        <p class="lead">Info</p>
        <div class="row">
            {!! Form::model($benefit,['method'=>'DELETE','url' => '/admin/incidents/'.$benefit->id]) !!}
            <div class="col-md-4 col-md-offset-4">
                {!!Form::submit('Eliminar',['class'=>'btn btn-block btn-danger'])!!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

    <div class="panel panel-default">
        <div class="panel-heading panel-heading-custom-{{config('config.theme')}}"><h3  class="panel-title">Registro</h3></div>
        <div class="panel-body">
            {!! Form::model($benefit,['method'=>'GET','url' => '/admin/incidents/'.$benefit->id]) !!}
            <div class="row">
                <div class="col-md-12">
                    {!! Form::label('name', 'Texto') !!}<br />
                    {!! Form::text('name', null, ['class' => 'form-control','readonly'=>'readonly']) !!}
                </div>
            </div>
            <div class="row"><br /></div>
            <div class="row">

                <div class="col-md-3">
                    {!! Form::label('code', 'Código') !!}<br />
                    {!! Form::text('code', null, ['class' => 'form-control ','maxlength'=>'10','readonly'=>'readonly']) !!}
                </div>
                <div class="col-md-3">
                    {!! Form::label('shortname', 'Nombre corto') !!}<br />
                    {!! Form::text('shortname', null, ['class' => 'form-control','maxlength'=>'10','readonly'=>'readonly']) !!}
                </div>
                <div class="col-md-3">
                    {!! Form::label('context', 'Contexto') !!}<br />
                    {!! Form::text('context',null,['class' => 'form-control','maxlength'=>'10','readonly'=>'readonly']) !!}
                </div>
                <div class="col-md-3">
                    {!! Form::label('type', 'Comportamiento') !!}<br />
                    {!! Form::text('type',null,['class' => 'form-control','maxlength'=>'10','readonly'=>'readonly']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection
