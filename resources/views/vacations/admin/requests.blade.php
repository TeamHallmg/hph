@extends('layouts.app')

@section('content')

<h3 class="text-danger bolder">Ver Solicitudes</h3>
<hr class="title_hr big_hr">
<div id="alert" class="mt-4"></div>
<h3 class="mt-4 text-danger bolder">Solicitudes</h3>
<hr class="title_hr small_hr">

<div class="table-responsive">
    <table id="tableRequest" class="table">
        <thead class="red_header">
            <tr>
                <th>Jefe</th>
                <th>Empleado</th>
                <th>Tipo</th>
                <th>Estatus</th>
                <th>Nota</th>
                <th>Registro</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($requests as $request)
                <tr>
                    <td>{{ $request->user->FullName }}</td>
                    <td>{{ $request->from->FullName }}</td>
                    <td>{{ $request->benefit->name }}</td>
                    <td>{{ __('bd.' . $request->status) }}</td>
                    <td>{{ $request->info }}</td>
                    <td>{{ date('d-m-Y', $request->time) }}</td>
                    <td>{{ date('Y-m-d', $request->event->start) }}</td>
                    <td>{{ date('Y-m-d', $request->event->end) }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
    function cleanDateInputs() {
		document.getElementById('date_ubegin').value = '';
		document.getElementById('date_uend').value = '';
		tableRequest.draw();
	}

    var tableRequest = $('#tableRequest').DataTable({
        colReorder: true,
        // responsive: true,
        dom: 'B' +
            '<"row"<"col-md-3"><"col-md-6"<"#toolbar">><"col-md-3"f>>rtip',
            "columnDefs": [
            {
                "targets": [ 6, 7 ],
                "visible": true,
                "searchable": true,
            }
        ],
        buttons: [
            {
                extend: 'csvHtml5',
                title: 'Listado_incidencias',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
        ]
    });

    {{--  $('#datepicker').datepicker({
        language: "es",
        format: "yyyy-mm-dd",
        autoclose: true,
        weekStart: 0
    });  --}}

    $(function(){
        $("div#toolbar").html('<div class="input-daterange input-group" id="datepicker">' +
            '<input type="date" class="form-control date_users datepicker-class" id="date_ubegin" name="start" required="required" />' +
            '<span class="input-group-text" id="basic-addon1">al</span>' +
            '<input type="date" class="form-control date_users datepicker-class" id="date_uend" name="end" required="required" />' +
            '<button class="btn btn-primary" onclick="cleanDateInputs()">Limpiar</button>'+
            '</div>' 
        );

        $.fn.dataTable.ext.search.push(
		    function( settings, data, dataIndex ) {
		        var min = new Date( $('#date_ubegin').val() );
		        var max = new Date( $('#date_uend').val() );
		        var start = new Date( data[6] ) || 0; // Fecha de Inicio
                var end = new Date( data[7] ) || 0; // Fecha de Fin
		 		if( isNaN(min) && isNaN(max) ||
                    min <= start && start <= max ||
                    min <= end && end <= max ||
                    (start <=  min && max <= end && start <= max && min <= max)){
		 			return true;
		 		}
		        return false;
		    }
		);

        $('.date_users').on('change', function () {
			var dateBegin = new Date($('#date_ubegin').val());
			var dateEnd = new Date($('#date_uend').val());
			if(!isNaN(dateBegin) && !isNaN(dateEnd)){
				if(dateEnd < dateBegin){
					$('#date_uend').val($('#date_ubegin').val());
				}
			}else if(!isNaN(dateBegin) && isNaN(dateEnd)){
				$('#date_uend').val($('#date_ubegin').val());

			}else if(isNaN(dateBegin) && !isNaN(dateEnd)){
				$('#date_ubegin').val($('#date_uend').val());
			}
            console.log($('#date_ubegin').val());
            console.log($('#date_uend').val());
			tableRequest.draw();
		});
    });
    
    function destroy(button,id){

        var r = confirm('¿Esta seguro de eliminar la solicitud?');
        if(r){
            $.ajax({
                headers: { 'X-CSRF-TOKEN': '{{csrf_token()}}' },
                url: "/common/plea/"+id,
                async: false,
                method: "DELETE",
                dataType: "json"
            }).done(function( data ) {
                if(data.success == 1) {
                    $('#alert').html(
                        '<div class="alert alert-warning alert-dismissible fade in" role="alert">'+
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                            '<span aria-hidden="true">&times;</span>'+
                        '</button>'+
                        '<strong>Registro cancelado.</strong>'+
                        '</div>'
                    );
                } else {
                    $('#alert').html(
                        '<div class="alert alert-warning alert-dismissible fade in" role="alert">'+
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                            '<span aria-hidden="true">&times;</span>'+
                        '</button>'+
                        '<strong>'+data.error+'</strong>'+
                        '</div>'
                    );
                }
            });
        }
    }
</script>

@endsection
