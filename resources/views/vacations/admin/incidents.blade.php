@extends('layouts.app')

@section('content')

<div class="panel panel-default panel-custom">
    <div class="panel-heading panel-heading-custom">
        <span class="panel-custom-first-{{config('config.theme')}} requests-icon"><h3 class="panel-title"> Solicitudes</h3></span><span class="panel-custom-second-{{config('config.theme')}}"></span>

    </div>

    <div class="panel-body panel-body-custom">
        <p class="lead">Info</p>
        <div class="row">
            <div class="col-md-2 col-md-offset-2">
                <a class="btn btn-custom-{{config('config.theme')}}" href="{{App::make('url')->to('/')}}/super/excel/requests"><span class="glyphicon glyphicon-plus"></span> Descargar Excel</a>
            </div>
            <div class="col-md-2 col-md-offset-2">
                <a class="btn btn-custom-{{config('config.theme')}}" href="{{App::make('url')->to('/')}}/super/pdf/requests"><span class="glyphicon glyphicon-plus"></span> Descargar PDF</a>
            </div>
        </div>
    </div>
</div>
<div id="alert"></div>
<div class="panel panel-default">
    <div class="panel-heading panel-heading-custom-{{config('config.theme')}}"><h3 class="panel-title">Solicitudes</h3></div>
    <div class="panel-body">
        <div class="table-responsive">
            <div id="toolbar" class="form-inline">
                <select id="field" class="form-control input-sm">
                    <option value="">Seleccione...</option>
                    <option value="boss">Jefe</option>
                    <option value="benefit">Tipo de solicitud</option>
                </select>
                <div class="col-md-6 input-daterange input-group" id="datepicker">
                    <input type="text" class="input-sm form-control datepicker-class" id="start" name="start" required="required" />
                    <span class="input-group-addon">al</span>
                    <input type="text" class="input-sm form-control datepicker-class" id="end" name="end" required="required" />
                </div>
            </div>
              <table id="table"
                    class="table table-hover"
                    data-toggle="table"
                    data-toolbar="#toolbar"
                    data-pagination="true"
                    data-side-pagination="server"
                    data-page-list="[5, 10, 20, 50, 100]"
                    data-search="true"
                    data-data-field="data"
                    data-show-refresh="true"
                    data-url="/super/tablerequests">
                <thead>
                  <tr>
                    <th class="tid" data-field="bossname">Jefe</th>
                    <th class="tid" data-field="username">Empleado</th>
                    <th class="tid" data-field="benefitname">*Tipo</th>
                    <th class="tid" data-field="status" data-sortable="true">*Estado</th>
                    <th class="tid" data-field="description">Nota</th>
                    <th class="tid" data-field="created_at" data-sortable="true">Registro</th>
                    <th class="tid" data-field="operations" data-sortable="true">Operaciones</th>
                  </tr>
                </thead>
              </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script type="text/javascript">

    var $table = $('#table');

    function initTable() {
        $table.bootstrapTable({
            queryParams: function(params) {
                params.field = $('#field').val();
                params.start = $('#start').val();
                params.end = $('#end').val();
                return(params);
            },
            icons: {refresh: 'glyphicon-search icon-refresh'}
        });
        // sometimes footer render error.
        setTimeout(function () {
            $table.bootstrapTable('resetView');
        }, 200);
    }

    initTable();

    $('#datepicker').datepicker({
        language: "es",
        format: "dd-mm-yyyy",
        autoclose: true,
        weekStart: 0
    });

    function destroy(button,id){
        var r = confirm('¿Esta seguro de eliminar la solicitud?');
        if(r){
            $.ajax({
                headers: { 'X-CSRF-TOKEN': '{{csrf_token()}}' },
                url: "/common/plea/"+id,
                async: false,
                method: "DELETE",
                dataType: "json"
            }).done(function( data ) {
                if(data.success == 1) {
                    $('#alert').html(
                        '<div class="alert alert-warning alert-dismissible fade in" role="alert">'+
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                            '<span aria-hidden="true">&times;</span>'+
                        '</button>'+
                        '<strong>Registro cancelado.</strong>'+
                        '</div>'
                    );
                    $table.bootstrapTable('refresh');
                } else {
                    $('#alert').html(
                        '<div class="alert alert-warning alert-dismissible fade in" role="alert">'+
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                            '<span aria-hidden="true">&times;</span>'+
                        '</button>'+
                        '<strong>'+data.error+'</strong>'+
                        '</div>'
                    );
                }
            });
        }
    }
</script>

@endsection
    