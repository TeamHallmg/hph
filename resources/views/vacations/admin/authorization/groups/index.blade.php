@extends('layouts.app')

@section('content')

<div class="card card-3">
	<div class="card-header">
		<h3>Lineas de Autorización</h3>
	</div>
	<div class="card-body">
		<div class="row justify-content-center mb-4">
			<div class="col-sm-4">
				<a href="{{url('groups/create')}}" class="btn btn-primary btn-block">Crear Grupo</a>
			</div>
		</div>
		<table class="table" id="groupsTable">
				<thead>
					<tr>
						<th>Grupo</th>
						@for($i = 0; $i < $max; $i++)
							<th>Nivel #{{$i + 1}}</th>
						@endfor
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($groups as $group)
						<tr>
							<td>{{$group->name}}</td>
							@foreach($group->authorizationRoutes as $route)
								<td>
									<ul class="text-left">
									@foreach($route->usersInRoute as $userRoute)
										<li style="margin: 0"> {{ $userRoute->user->FullName }} | {{ $userRoute->user->number }} </li>
									@endforeach
									</ul>
								</td>
							@endforeach
		
							@for($i = $group->authorizationRoutes->count(); $i < $max; $i++)
								<td></td>
							@endfor
							<td>	
								<a href="{{url('authorization_groups/'.$group->id.'/edit')}}" class="btn btn-outline-primary">
									<i class="fa fa-edit fa-lg" aria-hidden="true"></i>
									Editar
								</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
	</div>
</div>
	
</div>

@endsection

@section('scripts')
	<script>
		var groupsTable = $('#groupsTable').DataTable();

	</script>
@endsection