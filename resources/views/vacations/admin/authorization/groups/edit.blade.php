@extends('layouts.app')

@section('content')

<div class="card card-3">
	<div class="card-header">
		<h3>Ruta de autorizacion</h3>
	</div>
	<div class="card-body">
		<div class="card-title">
			<h5>Añadir Nivel de autorización <button id="addLevel" class="btn btn-outline-success">Añadir</button></h5>
		</div>
		<form action="{{ url('authorization_groups/' . $group->id) }}" method="POST">	
			@csrf
			<input type="hidden" name="_method" value="PUT">
			<div class="card-footer">
				<div class="row justify-content-center">
					<div class="col-md-4">
						<input type="submit" value="Guardar" class="btn btn-primary btn-block" id="btnSubmit" disabled>
					</div>
				</div>
			</div>
			<div id="body" class="card-body">
				@foreach($group->authorizationRoutes as $route)
					<div class="mb-4 levels" id="divLevel-{{ $loop->iteration }}">
						<h4>Nivel #{{$route->level}}</h4>
						<div class="row">
							@foreach($route->usersInRoute as $userRoute)
								<div class="col-md-5">
									<input type="text" class="form-control" name="name[]" value="{{ $route->name }}">
								</div>
								<div class="col-md-5">
									<select name="users[]" class="selectpicker form-control">
										@foreach ($users as $key => $user)
											<option value="{{ $key }}" {{ ($key == $userRoute->user->id)?'selected':'' }}>
												{{ $user }}
											</option>
										@endforeach
									</select>
								</div>
								<div class="col-md-2">
									<button class="btn btn-danger removeLevel">Quitar Nivel</button>
								</div>
							@endforeach
						</div>
					</div>
				@endforeach
			</div>
		</form>
	</div>
</div>

<div class="mb-4" id="divLevel" style="display: none">
	<h4>Nivel #</h4>
	<div class="row">
		<div class="col-md-5">
			<input type="text" class="form-control" name="name[]">
		</div>
		<div class="col-md-5">
			<select name="users[]" class="form-control" data-live-search="true">
				@foreach ($users as $key => $user)
					<option value="{{ $key }}">
						{{ $user }}
					</option>
				@endforeach
			</select>
		</div>
		<div class="col-md-2">
			<button class="btn btn-danger removeLevel">Quitar Nivel</button>
		</div>
	</div>
</div>

@endsection

@section('scripts')
	<script>
		$(document).ready( function () {

			activeFormButton();
			$("#addLevel").on('click', function (){
				var divLevel = $('#divLevel').clone();
				divLevel.show();
				divLevel.addClass('levels');
				divLevel.find('select').selectpicker();
				$('#body').append(divLevel);
				reprintLevels();
				activeFormButton()
			});

			$("#body").on('click', '.removeLevel', function (event) {
				event.preventDefault();
				$(this).closest('.levels').remove();
				reprintLevels();
				activeFormButton();
			});
		});

		function activeFormButton(){
			var numItems = $('.levels').length;
			if(numItems > 0){
				$("#btnSubmit").removeAttr('disabled');
			}else{
				$("#btnSubmit").attr('disabled','disabled');
			}
		}


		function reprintLevels(){
			var cont = 1;
			$('.levels').each( function () {
				console.log(cont);
				$(this).attr('id', 'divLevel-' + cont);
				var hLevel = $(this).find('h4');
				var text = hLevel.html();
				console.log(text);
				text = text.substring(0 ,text.indexOf("#") + 1);;
				hLevel.html(text + cont);
				cont++;
			});
		}
	</script>
@endsection