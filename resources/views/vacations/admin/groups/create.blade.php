@extends('layouts.app')

@section('content')

<div class="card card-3">
	<div class="card-header">
		<h3>Crear Grupo</h3>
	</div>
	<div class="card-body">
		<form action="{{ url('groups') }}" method="POST" id="form">
			@csrf
			<div>
				<div class="row">
					<div class="form-group col-md-6">
						<label for="name">Nombre del Grupo</label>
						<input type="text" name="name" class="form-control" required>
					</div>
					<div class="form-group col-md-6">
						<label for="recommended_staff">Personal Recomendado</label>
						<input type="number" name="recommended_staff" class="form-control" required min="0">
					</div>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-sm-4">
					<input type="submit" id="btn_cra" value="Crear Grupo" class="btn btn-primary btn-block">
				</div>
			</div>	
		</form>
	</div>
</div>
<div class="card card-3 my-4">
	<div class="card-header">
		<h3>Empleados sin grupo</h3>
	</div>
	<div class="card-body">
		<table class="table" id="usersTable">
			<thead>
				<tr>
					<th># Empleado</th>
					<th>Nombre</th>
					<th>Region</th>
					<th>Area</th>
					<th>Departamento</th>
					<th>Puesto de trabajo</th>
					<th>Puesto</th>
					<th>Linea</th>
					<th><input type="checkbox" class="check_all" id="users_check"></th>
				</tr>
			</thead>
			<tbody class="body">
				@foreach($users as $user)
					<tr>
						<td>{{$user->number}}</td>
						<td>{{$user->FullName}}</td>
						<td>{{$user->region->name}}</td>
						<td>{{$user->area->name}}</td>
						<td>{{$user->department->name}}</td>
						<td>{{$user->jobPosition->name}}</td>
						<td>{{$user->position->name}}</td>
						<td>{{$user->productionLine->name}}</td>
						<td><input type="checkbox" name="{{$user->id}}" class="chbox chbox-users"></td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection

@section('scripts')
	<script>
		var usersTable = $('#usersTable').DataTable({
			"language": {
			"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
			},
			"columnDefs": [
	            {
	                "targets": [ 8 ],
	                "searchable": false,
	                "orderable": false,
	            }
	        ],
			stateSave: true,
        	drawCallback: function() {
    			this.api().state.clear();
  			}
		});

		$(document).ready(function(){
			$('.check_all').on('change', function (){
				var checked = $(this).is(':checked');
				var allPstarted_ats = usersTable.rows({ search: 'applied' }).nodes();
				if(checked){
	            	$('input[type="checkbox"]', allPstarted_ats).prop('checked', true);
				}else{
					$('input[type="checkbox"]', allPstarted_ats).prop('checked', false);
				}
			});

			usersTable.on('search.dt', function(){
				if($('#users_check').is(':checked')){
					$('#users_check').prop('checked',false);
				}
				var allPstarted_atsItemsRemoved = usersTable.rows({ search: 'removed' }).nodes();
				$('input[type="checkbox"]', allPstarted_atsItemsRemoved).prop('checked', false);
			});

			$('.body').on('change', '.chbox', function (){
				if(!$(this).is(':checked')){
					$('.check_all').prop('checked',false);
				}
			});

			$('#form').on('submit', function(){
				var users_array = usersTable.$('input').serializeArray();
				var users = [];
				$.each(users_array, function(i, field){
					users.push(this.name);
				});
				console.log(users);
				if(users.length > 0){
					$(this).append(
						$('<input>')
							.attr('type', 'hidden')
							.attr('name', 'users')
							.val(users)
					);
				}
			});
		});
	</script>
@endsection