@extends('layouts.app')

@section('content')

<div class="card">
	<div class="card-header">
		<h3>Información del Grupo</h3>
	</div>
	<div class="card-body">
		<form action="{{ url('groups/' . $group->id ) }}" method="POST">
			@csrf
			<input type="hidden" name="_method" value="PUT">
			<div class="row">
				<div class="col-md-6">
					<label for="name">Nombre</label>
					<input type="text" name="name" value="{{ $group->name }}" class="form-control" required>
				</div>
				<div class="col-md-6">
					<label for="recommended_staff">Cantidad de personal Recomendado</label>
					<input type="number" name="recommended_staff" value="{{ $group->recommended_staff }}" class="form-control" required min="0">
				</div>
				<div class="col-md-12">
					<input type="submit" value="Guardar" class="btn btn-primary">
				</div>
			</div>
		</form>
	</div>
</div>
<div class="card my-4">
	<div class="card-header">
		<h3>Miebros Activos del Grupo</h3>
	</div>
	<div class="card-body">
		<button class="btn btn-danger mb-4" id="removeSelected">Remover Seleccionados <i class="fas fa-users fa-lg"></i></button>
		<table class="table" id="activeTable">
			<thead>
				<tr>
					<th># Empleado</th>
					<th>Nombre</th>
					<th>Correo</th>
					<th>Region</th>
					<th>Area</th>
					<th>Departamento</th>
					<th>Puesto de trabajo</th>
					<th>Puesto</th>
					<th>Linea</th>
					<th>Acciones</th>
					<th><input type="checkbox" class="check_all" id="actives_check"></th>
				</tr>
			</thead>
			<tbody id="bodyActive" class="body">
				@foreach($group->users as $user)
					<tr>
						<td>{{$user->number}}</td>
						<td>{{$user->FullName}}</td>
						<td>{{$user->email}}</td>
						<td>{{$user->region->name}}</td>
						<td>{{$user->area->name}}</td>
						<td>{{$user->department->name}}</td>
						<td>{{$user->jobPosition->name}}</td>
						<td>{{$user->position->name}}</td>
						<td>{{$user->productionLine->name}}</td>
						<td><button class="btn btn-danger btn_eliminar" id="{{$user->id}}"> <i class="fas fa-user-minus fa-lg"></button></td>
						<td><input type="checkbox" name="{{$user->id}}" class="chbox actives_check"></td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
<div class="card">
	<div class="card-header">
		<h3>Agregar nuevos miembros</h3>
	</div>
	<div class="card-body">
		<button class="btn btn-primary mb-4" id="addSelected">Agregar Seleccionados <i class="fas fa-users fa-lg"></i></button>
		<table class="table" id="usersTable">
			<thead>
				<tr>
					<th># Empleado</th>
					<th>Nombre</th>
					<th>Correo</th>
					<th>Region</th>
					<th>Area</th>
					<th>Departamento</th>
					<th>Puesto de trabajo</th>
					<th>Puesto</th>
					<th>Linea</th>
					<th>Acciones</th>
					<th><input type="checkbox" class="check_all " id="users_check"></th>
				</tr>
			</thead>
			<tbody id="bodyUser" class="body">
				@foreach($users as $user)
					<tr>
						<td>{{$user->number}}</td>
						<td>{{$user->FullName}}</td>
						<td>{{$user->email}}</td>
						<td>{{$user->region->name}}</td>
						<td>{{$user->area->name}}</td>
						<td>{{$user->department->name}}</td>
						<td>{{$user->jobPosition->name}}</td>
						<td>{{$user->position->name}}</td>
						<td>{{$user->productionLine->name}}</td>
						<td><button id="A-{{$user->id}}" class="btn btn-primary btn_agregar"><i class="fas fa-user-plus fa-lg"></button></td>
						<td><input type="checkbox" name="{{$user->id}}" class="chbox users_check"></td>
					</tr>
				@endforeach
			</tbody>
		</table>	
	</div>
</div>

<div class="">
	<h3>Ruta de Autorización</h3>

</div>

@endsection

@section('scripts')
	<script>
		var urlDelete = "{{url('groups/remove/')}}/";
		var urlUpdate = "{{url('groups/update/')}}/";
		var urlMasiveDelete = "{{url('groups/masive/remove')}}/";
		var urlMasiveUpdate = "{{url('groups/masive/update')}}/";

		var activeTable = $('#activeTable').DataTable({
			"language": {
			"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
			},
			"columnDefs": [
	            {
	                "targets": [ 9, 10 ],
	                "searchable": false,
	                "orderable": false,
	            }
	        ],
			stateSave: true,
        	drawCallback: function() {
    			this.api().state.clear();
  			}
		});

		var usersTable = $('#usersTable').DataTable({
			"language": {
			"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
			},
			"columnDefs": [
	            {
	                "targets": [ 9, 10 ],
	                "searchable": false,
	                "orderable": false,
	            }
	        ],
			stateSave: true,
        	drawCallback: function() {
    			this.api().state.clear();
  			}
		});

		$(document).ready(function(){
			$('.check_all').on('change', function (){
				var checked = $(this).is(':checked');
				if($(this).attr('id') == 'actives_check'){
					var allPstarted_ats = activeTable.rows({ search: 'applied' }).nodes();
				}else{
					var allPstarted_ats = usersTable.rows({ search: 'applied' }).nodes();
				}
				if(checked){
	            	$('input[type="checkbox"]', allPstarted_ats).prop('checked', true);
				}else{
					$('input[type="checkbox"]', allPstarted_ats).prop('checked', false);
				}
			});

			activeTable.on('search.dt', function(){
				if($('#actives_check').is(':checked')){
					$('#actives_check').prop('checked',false);
				}
				var allPstarted_atsItemsRemoved = activeTable.rows({ search: 'removed' }).nodes();
				$('input[type="checkbox"]', allPstarted_atsItemsRemoved).prop('checked', false);
			});

			usersTable.on('search.dt', function(){
				if($('#users_check').is(':checked')){
					$('#users_check').prop('checked',false);
				}
				var allPstarted_atsItemsRemoved = usersTable.rows({ search: 'removed' }).nodes();
				$('input[type="checkbox"]', allPstarted_atsItemsRemoved).prop('checked', false);
			});

			$('.body').on('change', '.chbox', function (){
				var cls = $(this).attr('class').split(" ")[1];
				var checked = $('.'+cls).is(':checked');
				if(!$(this).is(':checked')){
					$('#'+cls).prop('checked',false);
				}
			});

			$('#bodyActive').on('click', '.btn_eliminar', function(){
				var proceed = confirm('¿Quiere remover este usuario del grupo?');
				if(proceed){
					var id = this.id;
					var self = this;
					axios
						.get(urlDelete + id)
						.then(function (res) {
							var currentRow = activeTable.row($(self).parents('tr'));
							var data = currentRow.data();
							data[9] = '<button id="A-'+id+'" class="btn btn-primary btn_agregar"><i class="fas fa-user-plus fa-lg"></button>';
							data[10] = '<input type="checkbox" name="'+id+'" class="chbox users_check">';
							usersTable.row.add(data).draw();
							currentRow.remove().draw();
							alert('Eliminado del grupo exitosamente');
						})
						.catch(function (res){
							alert('No se pudo eliminar del grupo');
						});
				}
			});

			$('#bodyUser').on('click', '.btn_agregar', function(){
				var proceed = confirm('¿Quiere añadir este usuario al grupo?');
				if(proceed){
					var id = this.id.split('-')[1];
					var group = {{$group->id}};
					var self = this;
					axios
						.put(urlUpdate + id, {
							group: group,
						})
						.then(function (res){
							console.log('hola');	
							var currentRow = usersTable.row($(self).parents('tr'));
							var data = currentRow.data();
							data[9] = "<button class='btn btn-danger btn_eliminar' id='"+id+"'><i class='fas fa-user-minus fa-lg'></button>";
							data[10] = '<input type="checkbox" name="'+id+'" class="chbox actives_check">';
							activeTable.row.add(data).draw();
							currentRow.remove().draw();
							alert('Añadido al grupo exitosamente');
						})
						.catch(function (res){							
							alert('Error al intentar añadir al grupo');
						});
				}
			});


			$('#addSelected').on('click', function(){
				var proceed = confirm('¿Quiere añadir los usuarios al grupo?');
				if(proceed){
					var users = {};
					var datas = {};
					var rows = usersTable.rows($( usersTable.$('input[type="checkbox"]').map(function () {
  						return $(this).prop("checked") ? $(this).closest('tr') : null;
					})));
					rows.every( function ( rowIdx, tableLoop, rowLoop ) {
						let id = ($('input[type="checkbox"]', this.node()).attr('name'));
						let data = this.data();
						users[rowIdx] = [id, data[1]];
						data[9] = "<button class='btn btn-danger btn_eliminar' id='"+id+"'><i class='fas fa-user-minus fa-lg'></button>";
	                    data[10] = '<input type="checkbox" name="'+id+'" class="chbox actives_check">';
	                    datas[rowIdx] = data;
					});
					axios
						.put(urlMasiveUpdate + "{{ $group->id }}", {
							users: users,
						})
						.then(function (res){
							var errors = Object.values(res.data.error);
							if(_.size(res.data.users) > 0){
								$.each(res.data.users, function(k, v) {
									activeTable.row.add(datas[v]);
								});
								usersTable.rows(res.data.users).remove();
								activeTable.draw();
								alert('Usuarios añadidos exitosamente');
								if(errors.length > 0){
									alert(errors.join('\n'));
								}
							}else{
								if(errors.length > 0){
									alert(errors.join('\n'));
								}
							}
							usersTable.draw();
						})
						.catch(function (res){
							alert('Error al intentar añadir al grupo');
						});	
				}
			});

			$('#removeSelected').on('click', function(){
				var proceed = confirm('¿Quiere remover estos usuarios del grupo?');
				if(proceed){
					var users = {};
					var datas = {};
					var rows = activeTable.rows($( activeTable.$('input[type="checkbox"]').map(function () {
  						return $(this).prop("checked") ? $(this).closest('tr') : null;
					})));
					rows.every( function ( rowIdx, tableLoop, rowLoop ) {
						let id = ($('input[type="checkbox"]', this.node()).attr('name'));
						let data = this.data();
						users[rowIdx] = [id, data[1]];
						data[9] = '<button id="A-'+id+'" class="btn btn-primary btn_agregar"><i class="fas fa-user-plus fa-lg"></button>';
	                    data[10] = '<input type="checkbox" name="'+id+'" class="chbox users_check">';
	                    datas[rowIdx] = data;
	                    // usersTable.row.add(data).draw();
					});
					// rows.remove().draw();
					var self = this;
					axios
						.put(urlMasiveDelete + "{{ $group->id }}", {
							users: users,
						})
						.then(function (res){
							var errors = Object.values(res.data.error);
							if(_.size(res.data.users) > 0){
								$.each(res.data.users, function(k, v) {
									usersTable.row.add(datas[v]);
								});
								activeTable.rows(res.data.users).remove();
								activeTable.draw();
								alert('Usuarios removidos exitosamente');
								if(errors.length > 0){
									alert(errors.join('\n'));
								}
							}else{
								if(errors.length > 0){
									alert(errors.join('\n'));
								}
							}
							usersTable.draw();
						})
						.catch(function(){
							alert('Error al intentar remover del grupo');
						});
				}
			});
		});
	</script>
@endsection