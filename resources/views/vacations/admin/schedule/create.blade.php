@extends('layouts.app')

@section('content')

<div class="card card-3">
    <div class="card-header">
        <h3>Crear Horario</h3>
    </div>
    <div class="card-body">
        @foreach ($days as $key => $day)
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="day" class="w-25 d-inline mx-2">Día</label>
                        <input type="text" class="form-control w-75 d-inline" value="{{ $day }}" disabled>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="day" class="w-25 d-inline mx-2">Día</label>
                        <input type="checkbox" class="w-75 d-inline" name="labor[{{ $key }}]">
                    </div>
                </div>
                <div class="col-sm-6">
                    <label for="schedule" class="w-25 d-inline mx-2">Horario</label>
                    <select name="schedule[{{ $key }}]" class="form-control w-75 d-inline">
                        @foreach ($schedules as $schedule)
                            <option value="{{ $schedule->id }}">{{ $schedule->shift }}  /  {{ $schedule->in }} - {{ $schedule->out }}</option>
                        @endforeach
                        <option value=""></option>
                    </select>
                </div>
            </div>
        @endforeach
    </div>
</div>

@endsection