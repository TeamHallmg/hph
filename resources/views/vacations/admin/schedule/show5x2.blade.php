@extends('layouts.app')

@section('content')

<style>
.calendar{
	border: 1px solid black;
}

@media (min-width: 1281px) {
  .calendar{
  	height: 11rem;
  }
  
}

/* 
  ##Device = Laptops, Desktops
  ##Screen = B/w 1025px to 1280px
*/

@media (min-width: 1025px) and (max-width: 1280px) {
  .calendar{
  	height: 8rem;
  }
  
}

/* 
  ##Device = Tablets, Ipads (portrait)
  ##Screen = B/w 768px to 1024px
*/

@media (min-width: 768px) and (max-width: 1024px) {
  .calendar{
  	height: 7rem;
  }
  
}

/* 
  ##Device = Tablets, Ipads (landscape)
  ##Screen = B/w 768px to 1024px
*/

@media (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) {
  .calendar{
  	height: 7rem;
  }
  
}

/* 
  ##Device = Low Resolution Tablets, Mobiles (Landscape)
  ##Screen = B/w 481px to 767px
*/

@media (min-width: 481px) and (max-width: 767px) {
  .calendar{
  	height: 5rem;
  }
  
}

/* 
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/

@media (min-width: 320px) and (max-width: 480px) {
  .calendar{
  	height: 4rem;
  }
  
}
</style>

<div class="panel panel-default">  
    <div class="panel-heading panel-heading-custom-{{config('config.theme')}}"><h3  class="panel-title">Región: {{$region->name}}</h3></div>
    <div class="panel-body">
        <div class="table-responsive">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>Día</th>
                    <th>Entrada</th>
                    <th>Salida</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($region->workshiftFixed as $day)
                        @if($day->labor)
                        <tr>
                            
                            <td>@lang('bd.'.$day->day)</td>
                            <td>{{$day->schedule->in}}</td>
                            <td>{{$day->schedule->out}}</td>
                        </tr>
                        @endif
                    @endforeach
                </tbody>
              </table>
        </div>
        <div class="pull-right">
            <a title="Editar" alt="Editar" href="{{App::make('url')->to('/')}}/admin/schedule/{{$region->id}}/edit">
                <span class="glyphicon glyphicon-pencil"></span>
            </a>
        </div>
    </div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-6"></div>
		<div class="col-md-6"></div>
	</div>
</div>


<div class="panel panel-default">  
    <div class="panel-heading panel-heading-custom-{{config('config.theme')}}"><h3  class="panel-title">$currMonth</h3></div>
    <div class="panel-body">
    	@for($i = 0; $i < $totalDays + $dayNumber; $i++)
    		@if($i % 7 == 0 && $i != 0)
				</div>
			@endif
    		@if($i % 7 == 0 )
				<div class="row">
			@endif
    			@if($i < $dayNumber)
    				<div style="width: 14%; display: block; float: left;" class="calendar">
    					Adios
    				</div>
    			@else
					<div style="width: 14%; display: block; float: left;" class="calendar">
						{{$i - $dayNumber + 1}}
					</div>
    			@endif
    	@endfor
		

	</div>
</div>


@endsection
