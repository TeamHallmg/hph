@extends('layouts.app')

@section('content')

<div class="card card-3">
    <div class="card-header">
        <h3>Horarios</h3>
    </div>
    <div class="card-body">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <a href="{{ url('admin/schedule/create') }}" class="btn btn-block btn-primary">Crear</a>
            </div>
        </div>
    </div>
</div>

<div class="card card-3 mt-4">
    <div class="card-header">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">
            @foreach($region5x2 as $region)
                <li class="nav-item">
                    <a class="nav-link {{ $loop->first?'active':'' }}" id="pills-{{ $region->id }}-tab" data-toggle="pill" href="#pills-{{ $region->id }}" role="tab" aria-controls="pills-{{ $region->id }}" aria-selected="{{ $loop->first?'true':'false' }}"><h4>{{ $region->name }}</h4></a>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="card-body">
        <div class="tab-content" id="pills-tabContent">
            @foreach($region5x2 as $region)                
                <div class="tab-pane fade {{ $loop->first?'show active':'' }}" id="pills-{{ $region->id }}" role="tabpanel" aria-labelledby="pills-{{ $region->id }}-tab">
                    <div class="row justify-content-center mb-4">
                        <div class="col-md-4">
                            <a href="{{ url('admin/schedule/' . $region->id . '/edit') }}" class="btn btn-primary btn-block">Editar</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Día</th>
                                <th>Entrada</th>
                                <th>Salida</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($region->workshiftFixed as $day)
                                    @if($day->labor)
                                    <tr>
                                        
                                        <td>@lang('bd.'.$day->day)</td>
                                        <td>{{$day->schedule->in}}</td>
                                        <td>{{$day->schedule->out}}</td>
                                    </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="pull-right">
                        <a title="Ver" alt="Ver" href="{{App::make('url')->to('/')}}/admin/schedule/{{$region->id}}">
                            <span class="glyphicon glyphicon-eye-open"></span>
                        </a> &nbsp;&nbsp;
                        <a title="Editar" alt="Editar" href="{{App::make('url')->to('/')}}/admin/schedule/{{$region->id}}/edit">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

{{--  <div class="panel panel-default">
    <div class="panel-heading panel-heading-custom-{{config('config.theme')}}"><h3  class="panel-title">Regiones con horario 6x2</h3></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <td>Nombre</td>
                                <td>Entrada</td>
                                <td>Salida</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($schedules6x2 as $schedule)
                            <tr>
                                <td>{{$schedule->shift}}</td>
                                <td>{{$schedule->in}}</td>
                                <td>{{$schedule->out}}</td>
                                <td style="background-color: {{$colors[$loop->index]}}"></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-6"></div>
        </div>
    @foreach($region6x2 as $region)
        <div class="panel panel-default">
            <div class="panel-heading panel-heading-custom-{{config('config.theme')}}"><h3  class="panel-title">Región: {{$region->name}}</h3></div>
            @if($region->hasCalendarCreated())
            <div class="panel-body">
                <div class="table-responsive">
                      <table class="table table-hover">
                        <thead>
                          <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                            <tr>
                            @foreach($region->getWeekOfDay('-2') as $day)
                                @if($day['date'] == date('Y-m-d'))
                                    @if($day['labor'])
                                        <td align="center" style="background-color: {{$colors[$day['schedule'] - 1] }}; border: 3px solid black;">@lang('bd.'.$day['dayName']) {{$day['dayNumber']}}</td>
                                    @else
                                        <td align="center" style="background-color: grey; border: 3px solid black;">@lang('bd.'.$day['dayName']) {{$day['dayNumber']}}</td>
                                    @endif
                                @else
                                    @if($day['labor'])
                                        <td align="center" style="background-color: {{$colors[$day['schedule'] - 1] }};">@lang('bd.'.$day['dayName']) {{$day['dayNumber']}}</td>
                                    @else
                                        <td align="center" style="background-color: grey;">@lang('bd.'.$day['dayName']) {{$day['dayNumber']}}</td>
                                    @endif
                                @endif
                            @endforeach
                            </tr>
                        </tbody>
                      </table>
                </div>
                <div class="pull-right">
                    <a title="Ver" alt="Ver" href="{{App::make('url')->to('/')}}/admin/schedule/{{$region->id}}">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a> &nbsp;&nbsp;
                    <a title="Editar" alt="Editar" href="{{App::make('url')->to('/')}}/admin/schedule/{{$region->id}}/edit">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                </div> 
            </div>
            @endif
        </div>
    @endforeach
    </div>
</div>  --}}
    
@endsection
