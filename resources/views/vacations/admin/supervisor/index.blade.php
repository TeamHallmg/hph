@extends('layouts.app')

@section('content')

<div align="right"><a href='{{url('admin/supervisor/create')}}' class="btn btn-primary">Agregar</a></div>
<div class="panel panel-default">  
    <div class="panel-heading panel-heading-custom-{{config('config.theme')}}"><h3  class="panel-title">Supervisores</h3></div>
    <div class="panel-body">
        <div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Numero</th>
						<th>Area</th>
						<th>Departamento</th>
						<th>Puesto</th>
					</tr>
				</thead>
				<tbody>
					@foreach($supervisors as $supervisor)
						<tr>
							<td>{{$supervisor->getName()}}</td>
							<td>{{$supervisor->number}}</td>
							<td>{{$supervisor->area->name}}</td>
							<td>{{$supervisor->department->name}}</td>
							<td>{{$supervisor->position->name}}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

@endsection