@extends('layouts.app')

@section('sidebar')
    @include('vacations.sidebar')
@endsection

@section('mainScripts')
    @yield('scripts')
@endsection