@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        <h3> @lang('pages.eventcreatetitle')</h3>
    </div>
    <div class="card-body">
        <form action="{{ url('super/events') }}" method="POST">
            @csrf
            <input type="hidden" name="type" value="global">
            <input type="hidden" name="status" value="global">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="title"> @lang('pages.ecf_title') </label>                        
                        <input type="text" name="title" class="form-control" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="start">Fecha Inicio</label>
                                <input type="date" id="start" name="start" class="form-control" required>
                            </div>
                            <div class="col-md-6">
                                <label for="end">Fecha Final </label>
                                <input type="date" id="end" name="end" class="form-control" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="benefit_id"> @lang('pages.ecf_incident')</label>
                        <select name="benefit_id" required="required" class="form-control">
                            <option selected="selected" value="">@lang('pages.ecf_select')</option>
                            @foreach ($benefits as $benefit)
                                <option value="{{ $benefit->id}}">{{ $benefit->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title"> Bloquear Dias</label>
                        <input type="checkbox" name="blocked" class="mx-2" value="1">
                    </div>                    
                </div>                
            </div>
            @if(Auth::user()->role == 'supervisor')
                <div class="row">
                    <div class="col-md-6">                        
                        <strong>@lang('pages.ecf_users', ['number' => $total])</strong>
                    </div>
                </div>
                <div class="row my-3">
                    @if($total > 1)
                        <div class="col-md-6">
                            @for($i = 0; $i < $total / 2; $i++)
                            <div class="checkbox">
                                <label>
                                    <input name="users[]" type="checkbox" class="mx-2" value="{{ $users[$i]->id }}">
                                    {{ $users[$i]->firstname}}
                                </label>
                            </div>
                            @endfor
                        </div>
                        <div class="col-md-6">
                            @for($i; $i < $total; $i++)
                            <div class="checkbox">
                                <label>
                                    <input name="users[]" type="checkbox" class="mx-2" value="{{ $users[$i]->id }}">
                                    {{ $users[$i]->firstname }}
                                </label>
                            </div>
                            @endfor
                        </div>
                    @else
                        <div class="col-md-6">
                            @for($i=0 ;$i < $total ; $i++)
                            <div class="checkbox">
                                <label><input name="users[]" type="checkbox" value="{{ $users[$i]->id }}"> {{ $users[$i]->firstname }}</label>
                            </div>
                            @endfor
                        </div>
                    @endif
                </div>
            @elseif(Auth::user()->role == 'admin')
                <div class="col-md-6">
                    <p>Regiones</p>
                    @foreach($regions as $region)
                    <div class="checkbox">
                        <label><input name="regions[]" type="checkbox" value="{{ $region->id }}"> {{ $region->name }}</label>
                    </div>
                    @endforeach
                </div>
            @endif            
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <input type="submit" value="@lang('pages.create')" class="btn btn-block btn-primary">
                </div>
            </div>
        </form>
    </div>
</div>

@endsection
