@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        <h3 class="panel-title">@lang('pages.eventscanceltitle')</h3>
    </div>
    <div class="card-body">
        @lang('pages.eventscontent')
    </div>
</div>


<div class="card my-4">
    <div id="alert"></div>
    <div class="card-header">
        <h3 class="panel-title">@lang('pages.ect_header')</h3>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table id="table" class="table">
                <thead>
                    <tr>
                        <th>Titulo</th>
                        <th>Inicio</th>
                        <th>Fin</th>
                        <th>Bloquea</th>
                        <th>Region</th>
                        <th>Operaciones</th>                
                    </tr>
                </thead>
                <tbody>
                    @foreach ($events as $event)
                        <tr>
                            <td>{{ $event->title }}</td>
                            <td>{{ date('Y-m-d', $event->start) }}</td>
                            <td>{{ date('Y-m-d', $event->end) }}</td>
                            <td>{{ ($event->blocked)?'Sí':'No' }}</td>
                            <td>{{ $event->region->name }}</td>
                            <td></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
	
</script>
@endsection
