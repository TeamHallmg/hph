@extends('vacations.app')

@section('content')

<h2 class="text-danger bolder">APROBAR SOLICITUDES</h2>
<hr class="title_hr big_hr">
<h3 class="mt-4 text-danger bolder">Mis Solicitudes Inactivas</h3>
<hr class="title_hr small_hr">

<h4 class="mt-5 text-danger bolder">Solicitudes de los empleados</h4>
<hr class="title_hr small_hr">
    
<form action="{{ route('super.requests') }}" method="POST">
    @csrf
    <div class="table-responsive">
        <table class="table table-hover" id="incidentsTable">
            <thead class="red_header">
                <tr>
                    <th>Empleado</th>
                    <th>Descripción</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($requests as $request)
                    <tr>
                        <td>{{$request->from->FullName}}</td>
                        <td>{!!$request->info!!}</td>
                        <td>
                            <select name="b[{{ $request->id }}][v]" class="selectpicker" data-container="body" data-style="btn-info">
                                <option value="-1" style="background: #f0ad4e; color: #fff;">Posponer</option>
                                <option value="0" style="background: #d9534f; color: #fff;">Rechazar</option>
                                <option value="1" style="background: #5cb85c; color: #fff;">Aprobar</option>
                            </select>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <input type="submit" value="Guardar" class="btn btn-primary">
</form>

@endsection

@section('scripts')
<script>
    var incidentsTable = $('#incidentsTable').DataTable();
</script>
@endsection
