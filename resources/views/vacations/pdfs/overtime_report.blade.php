<style>
	.grey{
		background-color: #CBCBCB !important;
	}
	.el12{
		font-size: 12px;
	}
	.center{
		text-align: center;
	}

	.row{
		widows: 100%;
		overflow: auto;
	}

	.col-6{
		margin: 0px;
		width: 50%;
		display: inline-block;
	}

	.col-8{
		margin: 0px;
		width: 66.6666666667%;
		display: inline-block;
	}

	.col-4{
		margin: 0px;
		width: 33.3333333333%;
		display: inline-block;
	}

	.right{
		text-align: right;
	}

	.margin{
		border: 1px solid black;
	}

	.underline{
		border-bottom: 1px solid black;
	}

	.firma{
		margin: 0 25%;
		border-top: 3px solid black;
	}

	.linea_punteada{
		border-top: 1px dashed black;
	}		

	.firma3{
		border-top: 1px solid black;
	}

	.firma2{
		border-bottom: 3px solid black;
	}

	.col{
		display: inline;
	}

	.tt-collapse{
		border-collapse: collapse;
	}

	.tt-ab tbody tr, .tt-ab tbody tr td, .tt-ab thead tr th{
		border: 1px solid black;
	}


	.tt, .trh, .trd{
		border: 1px solid black;
	}

	.tbd td{
		border: 1px solid black;	
	}

	.tbd_underline-a tr th, .tbd_underline-a tr td {
		border-bottom: 1px solid black;	
	}

	.tbd_underline tr .td-underline {
		border-bottom: 1px solid black;	
	}

	table{
		width: 100%;
	}

	.p1{
		padding: 1rem;
	}
	.p1-lr{
		padding: 0 1rem;
	}
	.p2{
		padding: 1.5rem;
	}
	.p3{
		padding: 2rem;
	}

	.hr-dot{
		border:none;
		border-top:1px dashed #000;
		color:#fff;
		background-color:#fff;
		height:1px;
		width:100%;
	}

</style>

<div class="right">
	<h2>Reporte de Tiempo Extra / OVERTIME REPORT</h2>
</div>
<div class="row">
	<table>
		<tbody>
			<tr>
				<td style="width: 66.6666666667%">
					<table class="p1">
						<thead>
							<tr>
								<th style="width: 35%"></th>
								<th></th>
							</tr>
						</thead>
						<tbody class="tbd_underline">
							<tr>
								<td>Nombre / Name</td>
								<td class="td-underline">{{ $incident->from->FullName }}</td>
							</tr>
							<tr>
								<td>Categoría / Category</td>
								<td class="td-underline">{{ $incident->from->area->name }}</td>
							</tr>
							<tr>
								<td>Departamento / Department</td>
								<td class="td-underline">{{ $incident->from->department->name }}</td>
							</tr>
						</tbody>
					</table>
				</td>
				<td>
					<table class="tt-collaps">
						<tbody class="tbd_underline">
							<tr class="trh">
								<td class="{{ ($incident->from->unionized != 'S')?'grey':'' }} trd p1-lr">NO SINDICATO</td>
								<td class="{{ ($incident->from->unionized == 'S')?'grey':'' }} trd p1-lr">SINDICATO</td>
							</tr>
							<tr>
								<td>NO EMPLEADO / CODE</td>
								<td class="td-underline">{{ $incident->from->number }}</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="row">
	<table class="tt-collapse tt-ab">
		<thead>
			<tr>
				<th>Fecha / Date</th>
				<th>Justificacion / Remarks</th>
				<th>Hora inicio / In</th>
				<th>Hora salida / Out</th>
				<th>Total Tiempo Extra / Total Overtime</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>{{ $incident->incidentOvertime->date }}</td>
				<td>{{ $incident->comment  }}</td>
				<td>{{ $incident->incidentOvertime->timeIn }}</td>
				<td>{{ $incident->incidentOvertime->timeOut }}</td>
				<td>{{ $incident->amount / 60}} horas</td>
			</tr>
		</tbody>
	</table>
</div>
<br>
<div class="row">
	<table class="">
		<thead class="tbd_underline">
			<tr>
				<th class="td-underline" style="width: 25%">{{ $incident->user->FullName }}</th>
				<th style="width: 20%"></th>
				<th class="td-underline" style="width: 25%"></th>
				<th style="width: 5%"></th>
				<th class="td-underline" style="width: 25%"></th>
			</tr>
		</thead>
		<tbody>
			<tr style="vertical-align:top">
				<td>NOMBRE DEL SUPERVISOR:</td>
				<td></td>
				<td>
					APROBADO POR:
					<p>COORDINADOR / GERENTE</p>
				</td>
				<td></td>
				<td>
					APROBADO POR:
					<p>RECURSOS HUMANOS</p>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<hr class="hr-dot">
<div class="row">
	<table>
		<tbody>
			<tr>
				<td style="width: 70%">
					<div class="center">
						<h3>VALE DE ALIMENTOS</h3>
					</div>
				</td>
				<td style="width: 30%">
					<table class="tt-collapse">
						<tbody class="tbd_underline">
							<tr class="trh">
								<td class="trd p1">DECTO. NOMINA</td>
								<td class="trd p1">T. EXTRA</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="row">
	<table>
		<tbody>
			<tr>
				<td style="width: 70%">
					<table>
						<tbody>
							<tr>
								<td class="trd">
									union
								</td>
								<td class="trd">
									JUSTIFICACIÓN / REMARKS
								</td>
								<td class="trd">
									ALIMENTO SOLICITADO
								</td>
							</tr>
							<tr>
								<td class="trd">staff</td>
								<td rowspan="5" class="trd"></td>
								<td rowspan="5" class="trd"></td>
							</tr>
							<tr>
								<td></td>
							</tr>
							<tr>
								<td>FECHA</td>
							</tr>
							<tr>
								<td class="underline" style="height: 14px;"></td>
							</tr>
							<tr>
								<td>NO EMPLEADO</td>
							</tr>
							<tr style="min-height: 14px;">
								<td class="underline center">{{ $incident->from->number }}</td>
								<td>DESCUENTO DE NOMINA PARA:</td>
								<td class="underline"></td>
							</tr>
						</tbody>
					</table>
				</td>
				<td style="width: 30%">
					<table>
						<tbody>
							<tr>
								<td class="underline" style="height: 30px;"></td>
							</tr>
							<tr>
								<td class="center">
									TRABAJADOR SOLICITANTE
								</td>
							</tr>
							<tr>
								<td class="underline" style="height: 50px;"></td>
							</tr>
							<tr>
								<td class="center">
									FIRMA SUPERVISOR / COORDINADOR
								</td>
							</tr>
							<tr>
								<td class="underline" style="height: 50px;"></td>
							</tr>
							<tr>
								<td class="center">
									SEGURIDAD FISICA
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</div>
