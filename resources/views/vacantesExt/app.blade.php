@extends('layouts.app')

@section('moduleSidebar')
    @include('vacantesExt.sidebar')
@endsection

@section('mainContent')
    {{-- <img src="{{ asset('img/banner_clima_laboral.png') }}" alt=""> --}}
    <div class="mx-4">
        @yield('content')
    </div>
@endsection

@section('mainScripts')
    @yield('scripts')
@endsection