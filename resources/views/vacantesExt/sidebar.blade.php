{{--<li class="nav-item has-treeview">
    <a href="#" class="nav-link">
            <i class="nav-icon fas fa-temperature-low"></i>
        <p>
        Vacantes
        <i class="right fa fa-angle-left"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a class="nav-link" href="{{ url('requisitions') }}">
                <i class="fa fa-plus nav-icon"></i>
                <p><span>Requisiciones</span></p>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('reclutador') }}">
                <i class="fa fa-plus nav-icon"></i>
                <p><span>Reclutadores</span></p>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('vacantes/administrar') }}">
                <i class="fa fa-plus nav-icon"></i>
                <p><span>Admin Vacantes</span></p>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('vacantes') }}">
                <i class="fa fa-plus nav-icon"></i>
                <p><span>Vacantes</span></p>
            </a>
        </li>
        
    </ul>
</li>--}}

<div class="row">
        <div class="col-10">
            <h4 class="m-0 text-danger">
                Requisición de Personal
            </h4>
        </div>
        <div class="col-2 d-flex align-items-end">
            <div class="dropdown">
                @if(Auth::user()->hasRolePermission('vacancies_admin'))
                    <a href="#" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><h4 class="m-0 text-danger"><i class="fas fa-cog"></i></h4></a>
                    <div class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton">
                        @if(Auth::user()->hasRolePermission('see_vacancies'))
                            @if(Auth::user()->isRecruiter() && !Auth::user()->hasRolePermission('generate_vacant'))
                                <a class="dropdown-item text-white" href="{{ url('vacantes/reclutador') }}">Admin Vacantes</a>
                            @else
                                <a class="dropdown-item text-white" href="{{ url('vacantes/administrar') }}">Admin Vacantes</a>
                            @endif
                        @endif
                        @if(Auth::user()->hasRolePermission('add_remove_recruiter'))
                            <a class="dropdown-item text-white" href="{{ url('reclutador') }}">Reclutadores</a>
                        @endif
                        {{-- <a class="dropdown-item text-white" href="#">Another action</a>
                        <a class="dropdown-item text-white" href="#">Something else here</a> --}}
                    </div>
                @endif
            </div>
        </div>
    </div>
    <hr>
    <ul class="nav flex-column">
        <li class="nav-item">
          <a class="nav-link text-decoration-none text-dark font-weight-bold" href="{{ url('requisitions') }}">Requisiciones</a>
        </li>
        {{-- <li class="nav-item">
          <a class="nav-link text-decoration-none text-dark font-weight-bold" href="{{ url('reclutador') }}">Reclutadores</a>
        </li> --}}
        {{-- @if(Auth::user()->hasRolePermission('see_vacancies'))
            <li class="nav-item">
                <a class="nav-link text-decoration-none text-dark font-weight-bold" href="{{ url('vacantes/administrar') }}">Admin Vacantes</a>
            </li>
        @endif --}}
        <li class="nav-item">
          <a class="nav-link text-decoration-none text-dark font-weight-bold" href="{{ url('curriculum') }}">Currículum</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-decoration-none text-dark font-weight-bold" href="{{ url('vacantes') }}">Vacantes</a>
        </li>

        {{-- @if(Auth::user()->isRecruiter())
            <li class="nav-item">
                <a class="nav-link text-decoration-none text-dark font-weight-bold" href="{{ url('vacantes/reclutador') }}">Reclutador</a>
            </li>
        @endif --}}
      </ul>