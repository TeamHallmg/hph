@extends('clima-organizacional.app')

@section('title', 'Encuesta')

@section('content')
<div class="app">
<div class="row mt-3">
    <div class="col-md-2 text-right">
        @include('nps/partials/sub-menu')
    </div>
    <div class="col-md-10">
        <img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
        <h3 class="titulos-evaluaciones mt-4 font-weight-bold">RESULTADO eNPS</h3>
        <hr>



        <div class="row">
            <div class="col-md-12"> 
                @include('nps.reporte_filtros')
            </div>
        </div> 

<div class="row">
    <div class="col-md-12"> 
        <div class="card mt-3">
            <h5 class="card-header bg-info text-white font-weight-bolder">
                INDICADOR DE FIDELIZACIÓN
                <button class="btn btn-success float-right" @click="exportar()">Exportar Respuestas</button>
            </h5>
            <div class="card-body text-center" v-cloak v-if="loading">
                <img :src="'/img/ajax-loader.gif'" alt="">
            </div>
            <div class="card-body" v-cloak v-else>
                <div class="row pb-5">
                    <div class="col" :class="'col-md-'+item.cols" v-for="(item, index) in indicadores" :key="index">
                        
                        <div class="row  pr-3">
                            <div class="col text-center mx-0 px-0 py-2" v-for="(nivel, index_nivel) in item.niveles" :key="index_nivel">
                                
                                <img :src="'/img/nps/'+item.img" alt="" style="max-width: 50px !important;">

                                <h5 class="py-3"><span class="badge badge-pill badge-secondary"> @{{ nivel.valor }} </span></h5>


                                <h5 class="mb-0 py-2"  :class="[item.bg,(nivel.id==0||nivel.id==7||nivel.id==9)?'rounded-pill-right':(nivel.id==6||nivel.id==8||nivel.id==10)?'rounded-pill-left':null]"><span class="badge badge-pill"  :class="item.bg_badge"> @{{ nivel.nivel }} </span></h5>

                            </div>
                            
                        </div>

                        <div class="row">
                            <div class="col-12 text-center  font-weight-bolder h5">
                                @{{ item.name }}
                            </div>
                        </div> 

                    </div>
                </div>


                
                <div class="row pb-5 ">
                    <div class="col-3">




                        <highcharts :options="chartOptions" ></highcharts>










                    </div>
                    <div class="col-9">
                        <div class="card mt-3">
                            <h5 class="card-header text-black font-weight-bolder">
                                INPS = @{{porct}}
                                {{-- INPS = <span style="color:#42bd8c"> %PROMOTORES </span> - <span style="color:#ed3237">%DETRACTORES </span> --}}
                            </h5>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-9">
                                        <div class="row  py-2  d-flex align-items-center" v-for="(items, index) in indicadores" :key="index">
                                            <div class="col-6">
                                                <h4> <span class="w-100 badge badge-pill py-2"  :class="items.bg"> @{{ items.name }} </span></h4>
                                            </div>
                                            <div class="col-2">
                                                <h4> <span class="btn rounded-circle"  :class="items.bg_badge"> @{{ items.total_grupo }} </span></h4>
                                            </div>
                                            <div class="col-4">
                                                <h4> <span class="w-100 badge badge-pill py-2"  :class="items.bg"> @{{ items.porc_total_grupo }}% </span></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="row  py-2  d-flex align-items-center"> 
                                            <div class="col-12">
                                                <h4>
                                                    <span class="w-100 badge badge-pill badge-secondary py-2"> PARTICIPANTES </span>
                                                </h4>
                                            </div>
                                            <div class="col-11 text-center ">
                                                <div class="card mt-3 badge-info rounded-circle d-flex justify-content-center h3" style="
                                                align-content: center;
                                                min-width: 100%;
                                                min-height: 110px;
                                                align-items: initial;
                                            ">
                                            <div class="text-white font-weight-bolder">
                                                @{{total}}
                                            </div>
                                        </div>
                                    </div>                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



            </div>

            <hr>

            <div class="row pb-5">

                <div class="col-4">

                    <div class="card mt-3 h-100">
                        <h5 class="card-header text-white font-weight-bolder" style="background: #ed3237">
                            DETRACTORES
                        </h5>
                        <div class="card-body font-weight-bolder"  style="background: #ff8c84">
                            - Puntaje entre 0 - 6
                            <br>
                            <br>
                            - Requieren seguiminento proactivo para mitigar se dañe la marca empleadora
                            <br>
                            <br>
                            - No se encuentran satisfechos con la marca empleadora
                        </div>

                    </div>

                </div>
                <div class="col-4">

                    <div class="card mt-3 h-100">
                        <h5 class="card-header text-white font-weight-bolder" style="background: #ffbd18">
                            PASIVOS
                        </h5>
                        <div class="card-body font-weight-bolder" style="background: #efde7b">
                            - Puntaje entre 7 - 8
                            <br>
                            <br>
                            - Son susceptibles a ofertas competitivas
                            <br>
                            <br>
                            - Se dejan fuera de los cálculos del NPS
                        </div>

                    </div>

                </div>
                <div class="col-4">

                    <div class="card mt-3 h-100">
                        <h5 class="card-header text-white font-weight-bolder" style="background: #42bd8c">
                            PROMOTORES
                        </h5>
                        <div class="card-body font-weight-bolder" style="background: #73b58c">
                            - Puntaje entre 9 - 10
                            <br>
                            <br>
                            - Son leales y se sienten orgullosos de pertenecer a la organización
                            <br>
                            <br>
                            - Sus comentarios favorecen a la marca empleadora
                        </div>

                    </div>

                </div>

            </div>




        </div>
    </div>
    
   
    
</div>



</div>
</div>
</div>
</div>
@endsection
@section('scripts_vuejs')

<style>    
    .highcharts-title{
        font-size: 27px !important;
    }
    .highcharts-credits{
        display: none;
    }
    .rounded-pill-left{
        border-radius: 0rem 50rem 50rem 0rem !important;
    }
    .rounded-pill-right{
        border-radius: 50rem 0rem 0rem 50rem !important;
    }
    .bg-detractores {
        background-color: #ed3237 !important;
        color: #fff;
    }
    .bg-detractores-light {
        background: #ffadc6 !important;
    }
    .bg-pasivos {
        background-color: #ffbd10 !important;
        color: #fff;
    }
    .bg-pasivos-light {
        background: #ffefa5 !important;
    }
    .bg-promotores {
        background-color: #39bd84 !important;
        color: #fff;
    }
    .bg-promotores-light {
        background: #8cdec6 !important;
    }
</style>
<script type="text/javascript">
 
Vue.use(HighchartsVue.default) 
var app = new Vue({
  el: '#app',
    created: function(){
        this.getRecords();
    }, 
  data: {
    loading:true,
    id_periodo:'',
    total:0,
    indicadores: [
        {
            id:0,
            name:'DETRACTORES',
            total_grupo:'0',
            porc_total_grupo:'0',
            cols:'8',
            bg: 'bg-detractores-light',
            bg_badge: 'bg-detractores',
            img: 'detractores.png',
            niveles:[
                {
                    id:0,
                    nivel:0,
                    valor:0
                },
                {
                    id:1,
                    nivel:1,
                    valor:0
                },
                {
                    id:2,
                    nivel:2,
                    valor:0
                },
                {
                    id:3,
                    nivel:3,
                    valor:0
                },
                {
                    id:4,
                    nivel:4,
                    valor:0
                },
                {
                    id:5,
                    nivel:5,
                    valor:0
                },
                {
                    id:6,
                    nivel:6,
                    valor:0
                },
            ]
        },
        {
            id:0,
            name:'PASIVOS',
            total_grupo:'0',
            porc_total_grupo:'0',
            cols:'2',
            img: 'pasivos.png',
            bg: 'bg-pasivos-light',
            bg_badge: 'bg-pasivos',
            niveles:[
                {
                    id:7,
                    nivel:7,
                    valor:0
                },
                {
                    id:8,
                    nivel:8,
                    valor:0
                },
            ]
        },
        {
            id:0,
            name:'PROMOTORES',
            total_grupo:'0',
            porc_total_grupo:'0',
            cols:'2',
            img: 'promotores.png',
            bg: 'bg-promotores-light',
            bg_badge: 'bg-promotores',
            niveles:[
                {
                    id:9,
                    nivel:9,
                    valor:0
                },
                {
                    id:10,
                    nivel:10,
                    valor:0
                },
            ]
        }
    ],
    
    porct: '0',
    title: '',
    options: ['pie'],
    modo: 'spline',
    series: [
        {   name: 'porc',
            innerSize: '50%',
            data: [
            ]
        }, 
        ],
    filtros: {   
        periodos : [],
        dptos : [],
        areas : [],
        turnos : [],
        antiguedades : [],
        puestos : [],
        centros_trabajo : [],
        regiones : [],
        }, 
        dpto : '',
        turno : '',
        area : '',
        antiguedad : '',
        centro_trabajo : '',
        puesto : '',
        region : ''
    
  },
    computed: {
        chartOptions() { 
            return {
                chart: {  type: 'pie'},
                title: {  text: this.title  },
                series: this.series,                    
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: false
                    }
                },
                title: {
                    text: this.porct,
                    align: 'center',
                    verticalAlign: 'middle',
                    y: 10
                },
            }
        },
    },
    methods:{
 
        exportar: function(id_periodo_encuesta,id) {   
                    
            var url = "/nps/exportar_respuesta";
            console.log('url', url);
            axios.post(url,{
                id_periodo:this.id_periodo,
                dpto:this.dpto,
                area:this.area,
                centro_trabajo:this.centro_trabajo,
                puesto:this.puesto,
                region:this.region,
                antiguedad:this.antiguedad,
                turno:this.turno
            },{
                responseType: 'blob'
            }).then(response => {
                const url = URL.createObjectURL(new Blob([response.data], {
                    type: 'application/vnd.ms-excel'
                }))
                const link = document.createElement('a')
                link.href = url
                link.setAttribute('download', 'Respuestas Encuesta')
                document.body.appendChild(link)
                link.click();

                    

            })
        },
        resetFiltros: function(){
            this.centro_trabajo = '';
            this.dpto = '';
            this.area = '';
            this.puesto = '';
            this.region = '';
            this.antiguedad = '';
            this.turno = '';
            this.getRecords();
        },
        getRecords: function(){
            var dpto = '';
            var seriesas = [];
            this.loading =true;
            var url = "/nps/reporte/fidelizacion";

            if (this.area!='') {
                dpto = this.dpto;
            }else{ 
                var dpto_id = _.find(this.filtros.dptos, ['id', this.dpto]);
                if (dpto_id!=undefined) {
                    dpto = dpto_id['name'];
                }
                // var dpto = 'mmmm';  
            }
            console.log(dpto);


            axios.post(url,{
                id_periodo:this.id_periodo,
                dpto:dpto,
                area:this.area,
                centro_trabajo:this.centro_trabajo,
                puesto:this.puesto,
                region:this.region,
                antiguedad:this.antiguedad,
                turno:this.turno
            }).then(response=>{

                // console.log(response.data);


                this.loading =false;
                this.indicadores =response.data.indicadores;
                this.id_periodo =response.data.id_periodo;
                this.total =response.data.total;

                this.porct = parseFloat(this.indicadores['PROMOTORES']['porc_total_grupo'] - this.indicadores['DETRACTORES']['porc_total_grupo']).toFixed(1);

                $.each(response.data.indicadores, function(key, value) {
                    seriesas.push({color:value.color,name:value.name,y:value.porc_total_grupo});
                });
                
                this.series = [
                    {   name: 'porc',
                        innerSize: '50%',
                        data: seriesas
                    },
                ];

                this.filtros.periodos =response.data.periodos;                
                if(this.centro_trabajo==''){
                    this.filtros.centros_trabajo = response.data.centros_trabajo;

                }
                if(this.turno==''){
                    this.filtros.turnos = response.data.turnos;
                }
                if(this.dpto==''){
                    this.filtros.dptos = response.data.dptos;
                }
                if(this.area==''){
                    this.filtros.areas = response.data.areas;
                }
                if(this.puesto==''){
                    this.filtros.puestos = response.data.puestos;
                } 
                if(this.region==''){
                    this.filtros.regiones = response.data.regiones;
                }
                if(this.antiguedad==''){
                    this.filtros.antiguedades = response.data.antiguedades;
                }
 
                
            })




        },

    },
})

</script>
@endsection