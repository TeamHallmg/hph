@extends('layouts.app')

@section('title', 'Reporte Evaluación de Desempeño')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/evaluacion_desempeno.png" alt="">
	</div>
</div>
<div class="row margin-top-20">
	<div class="col-md-12">
		<h3 class="titulos-evaluaciones" style="margin-bottom: 0">Reporte Evaluación de Desempeño</h3>
	</div>
</div>

@if (!empty($periodos))

<div class="row margin-top-20">
	<div class="col-md-12">
		<table class="data-table-grafica">
			<thead>
				<tr>
					<th>Grupo de Area</th>
					<th>Nivel de Puesto</th>
					<th>Departamento</th>
					<th>Puesto</th>

	<?php foreach ($factores as $key => $factor){ ?>
			
					<th>{{$factor->nombre}}</th>
	<?php } ?>

				</tr>
			</thead>
			<tbody>
				<tr class="valores">
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="row margin-top-20">
	<div class="col-md-12">
		<div>Periodo: <select class="periodos form-control" style="display: inline-block; width: auto">

			    	<?php foreach ($periodos as $key => $periodo){ ?>
			                                      	
			              <option value="<?php echo $periodo->id?>" <?php if ($id_periodo == $periodo->id){ ?>selected="selected"<?php } ?>><?php echo $periodo->descripcion?></option>
			   		<?php } ?>
								 																 
								 	</select>
		</div>
	</div>
</div>

<div class="row margin-top-20">
	<div class="col-md-6">
		<div>
			<select class="grupo_areas form-control">
				<option value="-1">-- Seleccione Grupo de Area --</option>

<?php foreach ($grupo_areas as $key => $grupo_area){ ?>
			                                      	
			  <option value="<?php echo $grupo_area->id?>"><?php echo $grupo_area->nombre?></option>
			<?php } ?>
								 																 
			</select>
		</div>
	</div>
	<div class="col-md-6">
		<div>
			<select class="niveles_puestos form-control">
				<option value="-1">-- Seleccione Nivel de Puesto --</option>

<?php foreach ($niveles_puestos as $key => $nivel_puesto){ ?>
			                                      	
			  <option value="<?php echo $nivel_puesto->id?>"><?php echo $nivel_puesto->nombre?></option>
<?php } ?>
								 																 
			</select>
		</div>
	</div>
</div>

<div class="row margin-top-20">
	<div class="col-md-6">
		<div>
			<select class="areas form-control">
				<option value="-1">-- Seleccione Departamento --</option>

<?php foreach ($areas as $key => $area){ ?>
			                                      	
			  <option value="<?php echo $area->id?>"><?php echo $area->nombre?></option>
			<?php } ?>
								 																 
			</select>
		</div>
	</div>
	<div class="col-md-6">
		<div>
			<select class="puestos form-control">
				<option value="-1">-- Seleccione Puesto --</option>

<?php foreach ($puestos as $key => $puesto){ ?>
			                                      	
			  <option value="<?php echo $puesto->id?>"><?php echo $puesto->puesto?></option>
<?php } ?>
								 																 
			</select>
		</div>
	</div>
</div>

<div class="row margin-top-20">
	<div class="col-md-12">
		<div id="container" style="width: 100%; margin: 0 auto"></div>
	</div>
</div>
<form action="/reporte-graficas/{{$id_periodo}}" method="post" class="ver_reporte_grafico">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="submit" style="display: none">
</form>

@else

<div class="row">
	<div class="col-md-12 text-center">
		<h4 class="margin-top-20">No hay periodos cerrados ni abiertos</h4>
	</div>
</div>
@endif
@endsection

@section('scripts')
<script>

	var resultados = <?php echo json_encode($resultados)?>;
	var factores = <?php echo json_encode($factores)?>;
	var competencias = [];
	var contador = 0;

	$(document).ready(function(){

			for (var i = 0;i < factores.length;i++){

				competencias[i] = factores[i].nombre;
			}

			generar_grafica();

   		$('select.periodos').change(function(){

   			window.location = '/reporte-graficas/' + $(this).val();
   		});

   		$('select.grupo_areas').change(function(){

   			generar_grafica();
   		});

   		$('select.niveles_puestos').change(function(){

   			generar_grafica();
   		});

   		$('select.areas').change(function(){

   			generar_grafica();
   		});

   		$('select.puestos').change(function(){

   			generar_grafica();
   		});
		});

		function generar_grafica(){

			var calificaciones = [];
			var actual_factor = 0;
			var contador_factores = 0;
			var contador_evaluaciones = 0;
			var promedio = 0;
			var calificacion = 0;
			var area = $('select.areas').val();
			var puesto = $('select.puestos').val();
			var grupo_area = $('select.grupo_areas').val();
			var nivel_puesto = $('select.niveles_puestos').val();
			var band = true;

			for(var i = 0;i < resultados.length;i++){

				if (actual_factor != resultados[i].id_factor){

					if (actual_factor != 0){

						calificacion = promedio / contador_evaluaciones;
						calificaciones[contador_factores] = promedio / contador_evaluaciones;
						calificaciones[contador_factores] = parseInt(calificaciones[contador_factores] * 100 / 4);
						contador_factores++;
					}

					actual_factor = resultados[i].id_factor;
					contador_evaluaciones = 0;
					promedio = 0;

					while(factores[contador_factores].id != actual_factor){

						calificaciones[contador_factores] = 0;
						contador_factores++;
					}
				}

				band = false;

				if (area != -1 && resultados[i].area != area){

					band = true
				}

				if (!band && puesto != -1 && resultados[i].puesto != puesto){

					band = true
				}

				if (!band && grupo_area != -1 && resultados[i].grupo_area != grupo_area){

					band = true
				}

				if (!band && nivel_puesto != -1 && resultados[i].nivel_puesto != nivel_puesto){

					band = true
				}

				if (!band){

					promedio += resultados[i].total_evaluacion / resultados[i].numero_evaluaciones;
					contador_evaluaciones++;
				}
			}

			if (actual_factor != 0){

				calificacion = promedio / contador_evaluaciones;
				calificaciones[contador_factores] = promedio / contador_evaluaciones;
				calificaciones[contador_factores] = parseInt(calificaciones[contador_factores] * 100 / 4);
				contador_factores++;
			}

			for (var i = contador_factores;i < factores.length;i++){

				calificaciones[i] = 0;
			}

			var chart = {
      	renderTo: 'container',
      	type: 'column',
      	options3d: {
        	enabled: true,
        	alpha: 0,
        	beta: 0,
        	depth: 50,
        	viewDistance: 25
      	}
   		};

   		var title = {
      	text: ''   
   		};
   
   		var plotOptions = {
      	column: {
        	depth: 25,
        	zones: [
        {
          value: 75,
          color: 'rgb(0,0,255)'
        },
        {
          value: 100,
          color: 'rgb(0,0,255)'
        },
        {
          color: 'rgb(0,0,255)'
        }
       	]
      	}
   		};

    	var series = [{
      	data: calificaciones,
      	colorByPoint: true
   		}];
		
			var yAxis = [{
				labels: {
        	style: {
          	fontSize:'12px',
          	fontWeight:'bold'
        	},
        	formatter: function(){
       			return this.value + "%";
    			}
      	},
      	max: 100,
      	tickInterval: 25
			}];

   		var xAxis = [{
      	categories: competencias,
	  		labels: {
          style: {
            fontSize:'8px',
            fontWeight:'bold'
          },
          step: 1
        },
    	}];     
      
   		var json = {};
   		json.chart = chart;
   		json.title = title;
   		json.series = series;
   		json.plotOptions = plotOptions;
   		json.xAxis = xAxis;
			json.yAxis = yAxis;
   		new Highcharts.Chart(json);
   		var valores = '';
			var grupo_area = $('select.grupo_areas').val();
			var nivel_puesto = $('select.niveles_puestos').val();
			var area = $('select.areas').val();
			var puesto = $('select.puestos').val();

   		if (grupo_area != -1){

   			valores += '<td>' + $('select.grupo_areas option[value="' + grupo_area + '"]').text() + '</td>';
   		}

   		else{

   			valores += '<td></td>';
   		}

   		if (nivel_puesto != -1){

   			valores += '<td>' + $('select.niveles_puestos option[value="' + nivel_puesto + '"]').text() + '</td>';
   		}

   		else{

   			valores += '<td></td>';
   		}

   		if (area != -1){

   			valores += '<td>' + $('select.areas option[value="' + area + '"]').text() + '</td>';
   		}

   		else{

   			valores += '<td></td>';
   		}

   		if (puesto != -1){

   			valores += '<td>' + $('select.puestos option[value="' + puesto + '"]').text() + '</td>';
   		}

   		else{

   			valores += '<td></td>';
   		}

   		for (var i = 0;i < calificaciones.length;i++){

   			if (isNaN(calificaciones[i])){

   				valores += '<td>0%</td>';
   			}

   			else{

   				valores += '<td>' + calificaciones[i] + '%</td>';
   			}
   		}

   		$('tr.valores').html(valores);

   		if (contador > 0){

   			$('table.data-table-grafica').DataTable().destroy();
   		}

   		$('table.data-table-grafica').DataTable({
      	dom: 'Brt',
				language: {
		 			'url': '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
      	},
	  		buttons: [
        {
          extend: 'excel',
					text: 'Exportar a Excel',
					titleAttr: 'Exportar a Excel',
					title: 'Reporte Gráfica'
				}
        ]
  		});

  		contador++;
		}
	</script>
@endsection
