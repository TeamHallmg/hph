@extends('layouts.app')

@section('title', 'Reporte Evaluación de Desempeño')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/evaluacion_desempeno.png" alt="">
	</div>
</div>
<div class="row margin-top-20">
	<div class="col-md-12">
		<h3 class="titulos-evaluaciones" style="margin-bottom: 20px">Reporte Evaluación de Desempeño</h3>
	</div>
</div>

@if (!empty($periodos))

<div class="row margin-top-20">
	<div class="col-md-12">
		<div>Periodo: <select class="periodos form-control" style="display: inline-block; width: auto">

			      <?php foreach ($periodos as $key => $periodo){ ?>
			                                      	
			            	<option value="<?php echo $periodo->id?>" <?php if (!empty($id_periodo) && $id_periodo == $periodo->id){ ?>selected="selected"<?php } ?>><?php echo $periodo->descripcion?></option>
			      <?php } ?>
								 																 
								 	</select>
		</div>
	</div>
</div>

@if (!empty($resultados))

<div class="row margin-top-20">
	<div class="col-md-12">
		<table width="100%" class="data-table table table-striped">
			<thead class="cabeceras-tablas-evaluaciones">
				<tr>
					<th>ID</th>
					<th>Evaluado</th>
					<th>Departamento</th>
					<th>Puesto</th>
					<th>Evaluador</th>
					<th>Tipo de Evaluación</th>

	<?php foreach ($factores as $key => $factor){ ?>
		
					<th>{{$factor->nombre}}</th>
	<?php } ?>
					
					<th>Promedio</th>
				</tr>
			</thead>
			<tbody>

 <?php 	$actual_evaluador = 0;
 				$actual_evaluado = 0;
 				$actual_columna = 0;
 				$num_factores = 0;
 				$i = 0;
 				$calificaciones = 0;

 				foreach ($resultados as $key => $resultado){

 					if ($actual_evaluador != $resultado->id_evaluador || $actual_evaluado != $resultado->id_evaluado){

 						if ($actual_evaluador != 0 || $actual_evaluado != 0){

 							for ($i = $actual_columna;$i < count($factores);$i++){ ?>

					<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
				<?php }

							$promedio = $calificaciones / $num_factores; ?>

						<td class="text-center" style="vertical-align: middle; font-weight: bold; font-size: 20px; border: 1px solid #DDD; background-color: {{($promedio >= 3.5 ? '#008000' : ($promedio >= 3 ? '#73CB77' : ($promedio >= 2.5 ? '#FFFF00' : ($promedio >= 2 ? '#FF4343' : '#C00000'))))}}">{{number_format($promedio, 1, '.', ',')}}</td>
 				</tr>
 			
 			<?php }

 						$actual_evaluador = $resultado->id_evaluador;
 						$actual_evaluado = $resultado->id_evaluado;
 						$actual_columna = 0;
 						$num_factores = 0;
 						$calificaciones = 0; ?>

				<tr>
					<td class="text-left" style="border: 1px solid #DDD">{{$resultado->id_evaluado}}</td>
					<td class="text-left" style="border: 1px solid #DDD">{{str_replace('Ã‘','Ñ',$resultado->first_name)}}</td>
					<td class="text-left" style="border: 1px solid #DDD">{{str_replace('Ã‘','Ñ',$resultado->division)}}</td>
					<td class="text-left" style="border: 1px solid #DDD">{{str_replace('Ã‘','Ñ',$resultado->subdivision)}}</td>
					<td class="text-left" style="border: 1px solid #DDD">{{(!empty($resultado->evaluador) ? str_replace('Ã‘','Ñ',$resultado->evaluador) : '')}}</td>
					<td class="text-center" style="border: 1px solid #DDD; vertical-align: middle;">{{($resultado->id_evaluador == $resultado->id_evaluado ? 'AUTOEVALUACIÓN' : ($resultado->boss_id == $resultado->id_evaluador ? 'JEFE' : ($resultado->id_evaluado == $resultado->id_jefe_evaluador ? 'COLABORADOR' : 'PAR')))}}</td>
		<?php }

					for ($i = $actual_columna;$i < count($factores);$i++){
						
						if ($factores[$i]->id == $resultado->id_factor){

							break;
						} ?>

					<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
		<?php }

					$actual_columna = $i;
					$actual_columna++;
					$num_factores++;
					$calificaciones += $resultado->nivel_desempeno; ?>

					<td class="text-center" style="vertical-align: middle; font-weight: bold; font-size: 20px; border: 1px solid #DDD; background-color: {{($resultado->nivel_desempeno >= 3.5 ? '#008000' : ($resultado->nivel_desempeno >= 3 ? '#73CB77' : ($resultado->nivel_desempeno >= 2.5 ? '#FFFF00' : ($resultado->nivel_desempeno >= 2 ? '#FF4343' : '#C00000'))))}}">{{number_format($resultado->nivel_desempeno, 1, '.', ',')}}</td>
	<?php }

				if ($actual_evaluador != 0 || $actual_evaluado != 0){

					for ($i = $actual_columna;$i < count($factores);$i++){ ?>

					<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
		<?php }

					$promedio = $calificaciones / $num_factores; ?>

					<td class="text-center" style="vertical-align: middle; font-weight: bold; font-size: 20px; border: 1px solid #DDD; background-color: {{($promedio >= 3.5 ? '#008000' : ($promedio >= 3 ? '#73CB77' : ($promedio >= 2.5 ? '#FFFF00' : ($promedio >= 2 ? '#FF4343' : '#C00000'))))}}">{{number_format($promedio, 1, '.', ',')}}</td>
 				</tr>
 	<?php } ?>

			</tbody>
		</table>
	</div>
</div>

@else

<div class="row">
	<div class="col-md-12 text-center">
		<h4 class="margin-top-20 no_subordinados">Aún no hay resultados en la evaluación del periodo</h4>
	</div>
</div>
@endif

@else

<div class="row">
	<div class="col-md-12 text-center">
		<h4 class="margin-top-20">No Hay Periodos Abiertos o Cerrados</h4>
	</div>
</div>
@endif
@endsection

@section('scripts')
	<script>
		
		$(document).ready(function(){

			$('.data-table').DataTable({
				dom: 'Blfrtip',
   			'order': [[0,'asc']],
   			'scrollX': true,
   			language: {
		 		'url': '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
      		  },
      	buttons: [
        {
          extend: 'excel',
					text: 'Exportar a Excel',
					titleAttr: 'Exportar a Excel',
					title: 'Reporte Desempeño Sabana'
				}
        ]
   		});

   		$('select.periodos').change(function(){

   			window.location = '/reporte-sabana/' + $(this).val();
   		});
		});
	</script>
@endsection
