@extends('layouts.app')

@section('content')
<div class="row mt-3">
  <div class="col-md-2 text-right">
    @include('clima-organizacional/partials/sub-menu')
  </div>
  <div class="col-md-10">
    <img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
    <h3 class="titulos-evaluaciones mt-4 font-weight-bold">GRUPO ÁREAS</h3>
    <hr>
    <form action="{{ url('clima-organizacional/grupos_areas/' . $group->id ) }}" method="POST" class="areas">
        @csrf
        <input type="hidden" name="_method" value="PUT">
        <div class="row mt-3">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="name">Nombre</label>
                    <input type="text" name="name" class="form-control" placeholder="Nombre del grupo" value="{{ $group->name }}" required>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="description">Descripción</label>
                    <textarea name="description" class="form-control" placeholder="Descripción">{{ $group->description }}</textarea>
                </div>
            </div>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Todos <input type="checkbox" class="select_all_areas" id="areas_check"></th>
                </tr>
            </thead>
            <tbody>
          <?php $prev_area = $next_area = ''; ?>
                @foreach ($areas as $key => $area)
                  @if (in_array($area->id, $areas_without_group) || in_array($area->id, $groupAreas))
                    @if (!empty($areas[$key+1]))
                <?php $next_area = $areas[$key+1]->name; ?>
                    @else
                <?php $next_area = ''; ?>
                    @endif
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                    @if ($prev_area == $area->name || $next_area == $area->name)
                        <td>{{ $area->name }} ( {{(!empty($area->department) ? $area->department->name : '')}} )</td>
                    @else
                        <td>{{ $area->name }}</td>
                    @endif
              <?php $prev_area = $area->name; ?>
                        <td>{{ $area->description }}</td>
                        <td>
                            <input type="checkbox" class="area" value="{{ $area->id }}" {{ in_array($area->id, $groupAreas)?'checked':'' }}>
                        </td>
                    </tr>
                  @endif
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
            <input type="submit" value="Guardar" class="btn btn-primary" {{ $areas->isEmpty()?'disabled':'' }}>
        </div>
    </form>
  </div>
</div>
@endsection

@section('scripts')
<script>

  var table = 0;
  var checked = false;

  $(document).ready(function(){
    table = $('.table').DataTable({
      language: {
        "sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
            }
    });

    // Handle form submission event
   $('form.areas').on('submit', function(e){
      var form = this;
 
      //var rows_selected = table.column(3).checkboxes.selected();
      //console.log(table.column(3));

      //checkboxes should have a general class to traverse
      var rowcollection = table.$(".area:checked", {"page": "all"});

      var checkbox_value = 0;

      //Now loop through all the selected checkboxes to perform desired actions
      rowcollection.each(function(index,elem){
      //You have access to the current iterating row
        checkbox_value = $(elem).val();
        $(form).append(
             $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'areas[]')
                .val(checkbox_value)
         );
    //Do something with 'checkbox_value'
      });
 
      // Iterate over all selected checkboxes
      /*$.each(rows_selected, function(index, rowId){
         // Create a hidden element
         $(form).append(
             $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'areas[]')
                .val(rowId)
         );
      });*/

      //return false;
   });

   $('body').on('click', '.select_all_areas', function(){
            
    var allPstarted_ats = table.rows({ search: 'applied' }).nodes();
    checked = !checked;
           
    if (checked){

      $('input[type="checkbox"]', allPstarted_ats).prop('checked', true);
    }

    else{

      $('input[type="checkbox"]', allPstarted_ats).prop('checked', false);
    }
  });
  });
</script>
@endsection