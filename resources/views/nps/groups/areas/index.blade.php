@extends('layouts.app')

@section('content')
<div class="row mt-3">
  <div class="col-md-2 text-right">
    @include('clima-organizacional/partials/sub-menu')
  </div>
  <div class="col-md-10">
    <img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">


    <h3 class="titulos-evaluaciones mt-4 font-weight-bold">GRUPO ÁREAS</h3>
    <hr>

    @if (count($periods) > 0)

    <div class="col text-center mb-4">
      <div>Periodo: <select class="periods">

<?php foreach ($periods as $key => $period){ ?>
                                              
        <option value="<?php echo $period->id?>" <?php if ($period_id == $period->id){ ?>selected="selected"<?php } ?>><?php echo $period->name?></option>
<?php } ?>
                                                 
                    </select>
      </div>
    </div>

<a href="{{ url('clima-organizacional/grupos_areas/create') }}" class="btn btn-primary">Crear grupo</a>


   
<div class="my-3">

    <table class="table">
      <thead class="bg-purple text-white">
        <tr>
          <th>#</th>
          <th>Nombre</th>
          <th>Descripción</th>
          <th>Áreas</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tbody>
        <tr>
        <td>1</td>
        <td>Sin Grupo</td>
        <td>Áreas en Ningún Grupo</td>
        <td>
    @if (!empty($areas_without_group))

      @foreach ($areas_without_group as $key => $value)
      
          <div>{{$value->name}}</div>
      @endforeach
    @endif

        </td>
        <td></td>
      </tr>
    @foreach ($groups as $group)
        <tr>
          <td>{{ $loop->iteration + 1 }}</td>
          <td>{{ $group->name }}</td>
          <td>{{ $group->description }}</td>
          <td>
      @if (!empty($group->areas))

        @foreach ($group->areas as $key => $value)
      
          <div>{{$value->name}}</div>
        @endforeach
      @endif

          </td>
          <td>
            <a href="{{ url('clima-organizacional/grupos_areas/' . $group->id . '/edit') }}" class="btn btn-primary"> Editar </a>
            <form action="{{ url('clima-organizacional/grupos_areas/' . $group->id) }}" method="POST">
              @csrf
              <input type="hidden" name="_method" value="DELETE">
              <input type="submit" value="Eliminar" class="btn btn-danger">
            </form>
          </td>
        </tr>
    @endforeach
      </tbody>
    </table>
  </div>

    @else

  <h3>No hay periodos disponibles</h3>
    @endif

</div>
</div>
@endsection

@section('scripts')
<script>

  $(document).ready(function(){
    $('.table').DataTable({
      language: {
        "sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      }
    });
  });
</script>
@endsection