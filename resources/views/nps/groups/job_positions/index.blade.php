@extends('layouts.app')

@section('content')

<div class="row mt-3">
  <div class="col-md-2 text-right">
    @include('clima-organizacional/partials/sub-menu')
  </div>
  <div class="col-md-10">
    <img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">


    <h3 class="titulos-evaluaciones mt-4 font-weight-bold">GRUPO PUESTOS</h3>
<hr>
<a href="{{ url('clima-organizacional/grupos_puestos/create') }}" class="btn btn-primary">Crear grupo</a>



<div class="my-3">
<table class="table">
    <thead class="bg-purple text-white">
        <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Puestos</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>Sin Grupo</td>
        <td>Puestos en Ningún Grupo</td>
        <td>
    @if (!empty($job_positions_without_group))

<?php $job_positions = array(); ?>

      @foreach ($job_positions_without_group as $key => $value)

        @if (!in_array($value->name, $job_positions))
      
          <div>{{$value->name}}</div>

    <?php $job_positions[] = $value->name; ?>
        @endif
      @endforeach
    @endif

        </td>
        <td></td>
      </tr>
  @foreach ($groups as $group)
      <tr>
        <td>{{ $loop->iteration + 1 }}</td>
        <td>{{ $group->name }}</td>
        <td>{{ $group->description }}</td>
        <td>
    @if (!empty($group->jobPositions))

<?php $job_positions = array(); ?>

      @foreach ($group->jobPositions as $key => $value)

        @if (!in_array($value->name, $job_positions))
      
          <div>{{$value->name}}</div>

    <?php $job_positions[] = $value->name; ?>
        @endif
      @endforeach
    @endif

        </td>
                <td>
                    <!--<a href="{{ url('clima-organizacional/grupos_puestos/' . $group->id) }}" class="btn btn-primary"> Ver </a>-->
                    <a href="{{ url('clima-organizacional/grupos_puestos/' . $group->id . '/edit') }}" class="btn btn-success"> Editar</a>
                    <form action="{{ url('clima-organizacional/grupos_puestos/' . $group->id) }}" method="POST">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="submit" value="Eliminar" class="btn btn-danger">
                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
</div>
</div>
</div>
@endsection

@section('scripts')
<script>
  $(document).ready(function(){
    $('.table').DataTable({
      language: {
        "sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
            }
    });
  });
</script>
@endsection