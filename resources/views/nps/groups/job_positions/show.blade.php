@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label for="">Nombre del grupo</label>
            <input type="text" class="form-control" value="{{ $group->name }}" disabled>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label for="">Descripción</label>
            <textarea type="text" class="form-control" disabled>{{ $group->description }}</textarea>
        </div>
    </div>
</div>
<table class="table">
    <thead>
        <tr>
            <th>#</th>
            <th>Nombre</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($group->jobPositions as $job)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $job->name }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
@endsection