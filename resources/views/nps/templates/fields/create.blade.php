@extends('layouts.app')
@section('content')

<div class="container-fluid">
	
	
	<div class="card mt-3">
		<h5 class="card-header bg-info text-white font-weight-bolder">Campos Dinámicos - Crear
	<a href="{{url('/home')}}" class="btn btn-sm float-right btn-success">Regresar</a>
		  
  </h5>

  <form action="{{ url('templates-fields') }}" method="POST">
	@csrf
		<div class="card-body">


		<div class="row">
			
			<div class="col-12">
				<h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid rgb(137, 135, 136);">Datos Generales</h3>
			</div>

			<form action="{{ url('templates-fields') }}" method="POST">
				@csrf
				
			<div class="col-12 mt-4">
					<div class="form-row">
						<div class="form-group col-md-4">
							<label for="label">Etiqueta</label>
							<input type="text" class="form-control" name="label" id="label" placeholder="" required="">
						</div>
						<div class="form-group col-md-4">
							<label for="description">Descripción</label>
							<input type="text" class="form-control" name="description" id="description" placeholder="">
						</div> 
						<div class="col-md-4">
							<label for="status">Estado:</label>
							<select name="status" id="status" class="form-control" required="">
								<option disabled value="">Seleccione una opción...</option>
								<option value="1" selected>Disponible</option>
								<option value="0">No Disponible</option>
							</select>
						</div> 
						<div class="form-group col-md-4">
							<label for="">Tipo de Campo:</label><br>
							<div class="form form-check-inline ">
								<input class="form-check-input" type="radio" name="type" id="campo" value="campo">
								<label class="form-check-label" for="campo">Campo Directo</label>
							</div>
							<div class="form  form-check-inline">
								<input class="form-check-input" type="radio" name="type" id="attribute" value="attribute">
								<label class="form-check-label" for="attribute">Atributo</label>
							</div>
						</div>
						<div class="form-group col-md-4">
							<label for="attribute">Campo/Atributo</label>
							<input type="text" class="form-control" name="attribute" id="attribute" placeholder="" required="">
						</div>
					</div>
	
			</div>
			
		</div>
	</div>
	<div class="card-footer">

		<a href="{{url('/templates-fields')}}" class="btn btn-primary mr-3">Regresar</a>

		<button type="submit" class="btn btn-success mr-3">Guardar</button>
	</div>
  </form>

</div>
@endsection

@section('scripts')
<style type="text/css">
	.clase_error {
		display: none;
		color: red;
	}
</style>



@endsection