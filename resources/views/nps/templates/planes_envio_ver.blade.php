@extends('nps.app')

@section('title', 'Lista de Colaboradores')

@section('content')

	<div class="row">

		<div class="col-md-2 text-right">

			@include('nps/partials/sub-menu')

		</div>

		<div class="col-md-10">
			
			<img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">

			<form action="{{ url('actualizar_plan_correos') }}" method="post" id="period_form">
				{!! csrf_field() !!}
				<div class="row py-5">
					
					<div class="col-md-4">
						<div class="row info-box bg-info rounded py-2 text-white mx-1">
							<div class="col-2 d-flex align-items-center justify-content-center">
								<span class="info-box-icon">
									<h4 class="m-0"><i class="fas fa-users"></i></h4>
								</span>
							</div>
							<div class="col-10">
								<div class="info-box-content">
									<span class="info-box-text h5">Colaboradores</span><br>
									<span class="info-box-number h5">{{count($plan->colaboradores)}} en Total</span>
									
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="row info-box bg-success rounded py-2 text-white mx-1">
							<div class="col-2 d-flex align-items-center justify-content-center">
								<span class="info-box-icon">
									<h4 class="m-0"><i class="fas fa-arrow-alt-circle-up"></i></h4>
								</span>
							</div>
							<div class="col-10">
								<div class="info-box-content">
									<span class="info-box-text h5">Enviados</span><br>
									<span class="info-box-number h5">{{count($plan->enviados)}}</span>
									<div class="progress">
										<div class="progress-bar" style="width: {{ number_format(count($plan->enviados) * 100 / count($plan->colaboradores), 1, '.', ',') . '%' }}"></div>
									</div>
									<span class="progress-description h5">
										{{ number_format(count($plan->enviados) * 100 / count($plan->colaboradores), 1, '.', ',') . '%' }}
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="row info-box bg-danger rounded py-2 text-white mx-1">
							<div class="col-2 d-flex align-items-center justify-content-center">
								<span class="info-box-icon">
									<h4 class="m-0"><i class="fas fa-times"></i></h4>
								</span>
							</div>
							<div class="col-10">
								<div class="info-box-content">
									<span class="info-box-text h5">Con Errores</span><br>
									<span class="info-box-number h5">{{count($plan->errores)}}</span>
									<div class="progress">
										<div class="progress-bar" style="width: {{ number_format(count($plan->errores) * 100 / count($plan->colaboradores), 1, '.', ',') . '%' }}"></div>
									</div>
									<span class="progress-description h5">
										{{ number_format(count($plan->errores) * 100 / count($plan->colaboradores), 1, '.', ',') . '%' }}
									</span>
								</div>
							</div>
						</div>
					</div>
						<div class="col-md-12 text-center">
							<a href="{{ url('/enviar_plan_correos/' . $plan->id ) }}" class="btn btn-danger btn-static" style="cursor: default;">Enviar Plan</a>
									
						</div>
					</div>

					<div class="row">
					<div class="col-md-12">
									
						<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Editar Plan de Correos</h3>
						
						<hr class="mb-5">

					
						@if (count($periodos) > 0) 
							<div class="table-responsive">
								<table id="table-evaluados" class="table table-striped table-bordered listado w-100">
									<thead style="background-color: #222B64; color:white;">
										<tr>
											<th>ID</th>
											<th>Rfc</th>
											<th>Evaluado</th>
											<th>Correo</th>
											<th>Departamento</th>
											<th>Puesto</th> 
											<th>Estado</th> 
											<th class="mx-auto">
												Todos
												<input type="checkbox" class="select_all_evaluados" id="users_check">
											</th>
										</tr>
									</thead>
									<tbody>
										@if (count($evaluados) > 0)							
											@foreach ($evaluados as $eval)
										
										<tr>
					                        <td class="text-center">
					                            {{ (!empty($eval->employee) ? $eval->employee->idempleado : '') }}
					                        </td>
					                        <td class="text-center">
					                            {{ (!empty($eval->employee) ? $eval->employee->rfc : '')}}
					                        </td>
					                        <td class="text-center">
					                            {{ $eval->FullName }}
					                        </td>
					                        <td class="text-center">
					                            {{ $eval->email }}
					                        </td>
					                        <td class="text-center">
					                            {{ (isset($eval->employee->jobPosition->area->department) ? $eval->employee->jobPosition->area->department->name : '') }}
					                        </td>
					                        <td class="text-center">
					                            {{ (!empty($eval->employee->jobPosition) ? $eval->employee->jobPosition->name : '') }}
					                        </td> 
											<td>
												{{$eval->estado_envio	}}
											</td> 
											<td>
												<input type="checkbox" name="users[]" value="{{$eval->id}}" class="evaluados" <?php if (in_array($eval->id, $colaboradores_select)){ ?>checked="checked"<?php } ?>>
											</td>
										</tr>
										@endforeach
										@endif
									</tbody>
								</table>
							</div>
						@endif
					</div>
					
					
					<div class="col-md-12 pt-5">
 
					<h3 class="titulos-evaluaciones font-weight-bold">Información de Plan de Correos</h3>
						
					</div>

					<div class="col-md-4 mb-4">
						<label for="id_template">Plantillas</label>
						<select id="id_template" name="id_template" class="form-control" required>
							<option disabled value="" selected>Seleccione una opción...</option>
							@foreach($plantillas as $plantilla)
								<option value="{{$plantilla->id}}" <?php if (!empty($plan->id_template) && $plan->id_template == $plantilla->id){ ?>selected="selected"<?php } ?>>{{$plantilla->name}}</option>
							@endforeach
						</select>
					</div>
					
					<div class="col-md-4 mb-4">
						<label for="name">Nombre</label>						 
						<input type="text" class="form-control" name="name" value="{{$plan->name}}" required>						
						<input type="hidden" class="form-control" name="id" value="{{$plan->id}}" required>						
					</div>

					<div class="col-md-4 mb-4">
						<label for="date">Fecha</label>						 
						<input type="date" class="form-control" name="date" value="{{$plan->date}}">						
					</div>
 
					<div class="col-md-4 mb-4">
						<label for="date">Maximo de envio por lote</label>						 
						<input type="number" class="form-control" name="max_sends" value="{{$plan->max_sends}}">						
					</div>
 
					<div class="col-12">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="template_modules_id" value="clima">
						  
						<div class="float-right">
							<button class="btn btn-success mr-3" type="submit" style="color: white"><span class="fas fa-check-circle"></span> Actualizar Plan</button> 
							<a class="btn btn-danger" href="{{ url('/nps/plan-correos') }}"><span class="fas fa-times-circle"></span> Volver</a>
						</div>
					</div> 
				</div> 
			</form>
			
		</div>
		
	</div>
 
@endsection

@section('scripts')


<script>

    var checked = false;
    var total_users = 0;
    var usersTable = 0;
 
$(document).ready(function(){

    usersTable = $('#table-evaluados').DataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
    });

		
        $('body').on('click', '.select_all_evaluados, .evaluados', function(){
            
            if ($(this).hasClass('select_all_evaluados')){
            
              var allPstarted_ats = usersTable.rows({ search: 'applied' }).nodes();
              checked = !checked;			  
              if (checked){
                $('input[type="checkbox"]', allPstarted_ats).prop('checked', true);
              }else{
                $('input[type="checkbox"]', allPstarted_ats).prop('checked', false);
              }
            }

            var users_array = usersTable.$('input').serializeArray()
            total_users = users_array.length;
            $('.btn-question').removeClass('active');
            var users_message = '';
            if (total_users > 0){              
              users_message = 'Se enviaran correos a : ' + total_users +' colaboradores';
            }
            // $('.mensaje_empleados').text(users_message);
            $('#num_c').val(total_users);
        });
 
        // $('#table-evaluados tbody tr').click(function (e) {
		// 	if(!$(e.target).is('#table-evaluados td input:checkbox'))
		// 	$(this).find('input:checkbox').trigger('click');
		// });

        $('#period_form').on('submit', function(e){
            var users_array = usersTable.$('input').serializeArray();
			var users = [];
			$.each(users_array, function(i, field){
				users.push(this.value);
			});
			if(users.length > 0){
				$(this).append(
					$('<input>')
						.attr('type', 'hidden')
						.attr('name', 'users')
						.val(users)
				);
			} 
        }); 
	$('select#periodos').change(function(){

	$('form.form_estadisticas .period_id').val($(this).val());
	$('form.form_estadisticas').submit();
	});
	$('select#status').change(function(){

	$('form.form_estadisticas .status').val($(this).val());
	$('form.form_estadisticas').submit();
	});
 
	
    }); 
</script>
@endsection