{{-- @extends('layouts.app')
@section('content') --}}
@extends('bienes.app')
@section('content')

<div class="container-fluid mt-5">
		<div class="row mb-3">
				<div class="col-12 blue_header rounded py-3">
					<h2 class="font-italic font-weight-bold m-0"> Tipos de Recurso - Consulta</h2>
				</div>
				
		<div class="col-12 mt-4">
				<h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid rgb(137, 135, 136);">Datos Generales</h3>
			</div>
			</div>



	<div class="form-row">
		<div class="form-group col-md-3">
			<label for="id_codigo">Código</label>
			<input type="text" class="form-control" name="codigo" id="id_codigo" placeholder="" readonly="" value="{{ $TipoBienes->codigo }}" >
		</div>
		<div class="form-group col-md-3">
			<label for="id_nombre">Nombre</label>
			<input type="text" class="form-control" name="nombre" id="id_nombre" placeholder="" readonly="" value="{{ $TipoBienes->nombre }}">
		</div>
		<div class="form-group col-md-3">
			<label for="id_descripcion">Descripción</label>
			<input type="text" class="form-control" name="descripcion" id="id_descripcion" readonly="" value="{{ $TipoBienes->descripcion }}">
		</div>
		<div class="col-md-3">
			<label for="id_estatus">Estado:</label>
			<select name="estatus" id="id_estatus" class="form-control" readonly="">
				<option disabled value="" >Seleccione una opción...</option>
				<option value="1" selected>Disponible</option>
				<option value="0">No Disponible</option>
			</select>
		</div>
	</div>
	<br>

	<div class="row mt-4">
		<div class="col-12">
				<h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid rgb(137, 135, 136);">Especificaciones</h3>
		</div>
	</div>
	<div class="form-row">

	@if( count($TipoBienes->tipobiendetalles) == 0 )


        <div class="form-group col-md-12 text-center">

           <p> <strong> No tiene Especificaciones registradas </strong> </p>

        </div>

	@endif


    @foreach ($TipoBienes->tipobiendetalles as $tipobiendetalles)

        <div class="form-group dataBienes col-md-3" >

            <label class="an-title font-weight-bold"> {{ $tipobiendetalles->variable->etiqueta }}</label>

            <input type="text" class="form-control" readonly="">

        </div>

    @endforeach


	</div>
				
	<br>

	<hr style="border: 2px solid #000000;">
	<div class="form-row">

		<div class="form-group col col-md-2">
		</div>

		<div class="form-group col col-md-2 ">
		</div>

		<div class="form-group col col-md-8 d-flex justify-content-end">
				<a class="btn btn-small btn-primary mr-3" href="{{ url('Tipobienes') }}">Regresar</a>
			<a href="{{ url('Tipobienes/' . $TipoBienes->id . '/edit') }}" class="btn btn-success  mr-3">Editar</a>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$(function() {

			$("#id_estatus option[value='{{ $TipoBienes->estatus }}']").prop('selected', true);

		});
	</script>
@endsection