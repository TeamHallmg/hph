@extends('layouts.app')
@section('content')

<div class="container-fluid">
	
	
	<div class="card mt-3">
		<h5 class="card-header bg-info text-white font-weight-bolder">Previsualizar Plantilla
	  
  </h5>
  

		<div class="card-body">


		<div class="row">

			<div class="col-md-12">
				{!! $plantilla_final!!} 
			</div>
			
		</div>
	</div>
	<div class="card-footer">

		<a href="{{url('/templates')}}" class="btn btn-primary mr-3">Regresar</a>

	</div>
	

</div>
@endsection

@section('scripts')


@endsection