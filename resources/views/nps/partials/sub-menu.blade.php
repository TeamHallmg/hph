<h4 class="font-weight-bold  color-personalizado-azul" style="margin-top: 15px;">eNPS</h4>
<hr>
<ul class="nav nav-pills flex-column">
	<li class="nav-item"><a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('nps/que-es') }}">Información General</a></li>
	<li class="nav-item">
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_encuestas']) || auth()->user()->hasNpsPermissions(1))
		<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('nps/encuestas') }}">Preguntas Abiertas</a>
	    @endif
		@if (auth()->user()->role == 'admin' || auth()->user()->role == 'supervisor' || isset($userPermissions['see_progress'])  || auth()->user()->hasRolePermission('show_only_region_nps') || auth()->user()->hasNpsPermissions(2))
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('nps/avances') }}">Avances</a></li>
		@endif
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_questions']) || auth()->user()->hasNpsPermissions(3))
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('nps/preguntas') }}">Preguntas</a></li>
			@endif
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_questions']) || auth()->user()->hasNpsPermissions(4))
		<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('nps/etiquetas') }}">Etiquetas</a></li>
		@endif
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_periods']) || auth()->user()->hasNpsPermissions(5))
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('nps/periodos') }}">Periodos</a></li>
		@endif
		@if (auth()->user()->role == 'admin')
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('nps/permissions') }}">Permisos</a></li>
		@endif
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_periods']) || auth()->user()->hasNpsPermissions(6))
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('nps/reporte/fidelizacion') }}">Reporte Fidelización</a></li>
		@endif
		
		@if (auth()->user()->role == 'admin' || auth()->user()->hasNpsPermissions(7))
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('nps/invitaciones-masivas') }}">Invitaciones Masivas</a></li>
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('nps/plan-correos') }}">Plan de Correos</a></li>
		@endif
</ul>