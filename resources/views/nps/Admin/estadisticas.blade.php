@extends('layouts.app')

@section('title', 'Estadisticas')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>

	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }} " alt="">
		<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Estado del Avance</h3>
		<hr class="mb-5">
	

	
			
		@if (count($periodos) > 0)
			
			<div class="text-center font-weight-bold mb-5">
					<ul style="padding: 0; margin: 0; list-style-type: none;">
						<li class="text-center mr-3" style="display: inline-block">
							<!-- $terminado, $iniciado, $noIniciado -->
								<div>
									{{ $noIniciado }} / {{ $totalEvaluaciones }}
								</div>
								<div>
								{{ number_format($porcentajeNoIniciado, 1, '.', ',') }}%
								</div>
							<div>
								<a class="btn btn-danger font-weight-bold" style="cursor: default;color: white;">No Iniciada</a>
							</div>
						</li>
						<li class="text-center  mr-3" style="display: inline-block">
								<div>
									{{ $iniciado }} / {{ $totalEvaluaciones }}
								</div>
								<div>
									{{ number_format($porcentajeIniciado, 1, '.', ',') }}%
								</div>
							<div>
								<a class="btn btn-warning font-weight-bold" style="cursor: default;color: white;">Inconclusa</a>
							</div>
						</li>
						<li class="text-center  mr-3" style="display: inline-block">
								<div>
									{{ $terminado }} / {{ $totalEvaluaciones }}
								</div>
								<div>
									{{ number_format($porcentajeTerminado, 1, '.', ',') }}%
								</div>
							<div>
								<a class="btn btn-success font-weight-bold" style="cursor: default;color: white;">Terminada</a>
							</div>
						</li>
						<li class="text-center  mr-3" style="display: inline-block">
							<!-- $terminado, $iniciado, $noIniciado -->
								<div>
									{{ $total_retros }} / {{ $totalEvaluados }}
								</div>
								<div>
								{{ number_format($porcentajeRetro, 1, '.', ',') }}%
								</div>
							<div>
								<a class="btn btn-primary font-weight-bold" style="cursor: default; width: 100px;color: white;">Retro</a>
							</div>
						</li>
					</ul>
			</div>
		@endif

		

		@if (count($periodos) > 0)
		<div class="row">
			<div class="col-md-4 mx-auto mb-4">
				<select id="periodos" class="form-control">

			@foreach($periodos as $periodo)

	        	<option value="{{$periodo->id}}" <?php if ($id_periodo == $periodo->id){ ?>selected="selected"<?php } ?>>{{$periodo->descripcion}}</option>
	      	@endforeach

				</select>
			</div>

			<div class="col-md-4 mx-auto">
				<select id="field" class="form-control">
	        <option value="-1">-- Columna en cual buscar --</option>
	        <option value="0">ID Evaluador</option>
	        <option value="1">Evaluador</option>
	        <option value="4">ID Evaluado</option>
	        <option value="5">Evaluado</option>
				</select>
			</div>
		</div>

		@endif
			
			<table class="table table-striped table-bordered stats dt-responsive">
				<thead style="background-color: #222B64; color:white;">
					<tr>
						<th>ID Evaluador</th>
						<th>Evaluador</th>
						<th>Departamento</th>
						<th>Puesto</th>
						<th>ID Evaluado</th>
						<th>Evaluado</th>
						<th>Departamento</th>
						<th>Puesto</th>
						<th>Estado</th>
					</tr>
				</thead>
				<tbody>
					@if($periodos->count() > 0)
						@foreach($estadisticas as $estadistica)

							<tr>
								<td class="text-center">
									{{ $estadistica->id_evaluador }}
								</td>
								<td class="text-center">
									{{ (!empty($estadistica->nameCompleteUserEvaluador->fullname) ? str_replace('Ã‘','Ñ',$estadistica->nameCompleteUserEvaluador->fullname) : '') }}
								</td>
								<td class="text-center" style="display: none">
									{{ (!empty($estadistica->nameCompleteUserEvaluador->department) ? str_replace('Ã‘','Ñ',$estadistica->nameCompleteUserEvaluador->department) : '') }}
								</td>
								<td class="text-center" style="display: none">
									{{ (!empty($estadistica->nameCompleteUserEvaluador->puesto) ? str_replace('Ã‘','Ñ',$estadistica->nameCompleteUserEvaluador->puesto) : '') }}
								</td>
								<td class="text-center">
									{{ $estadistica->id_evaluado }}
								</td>
								<td class="text-center">
									{{ (!empty($estadistica->nameCompleteUserEvaluado->fullname) ? str_replace('Ã‘','Ñ',$estadistica->nameCompleteUserEvaluado->fullname) : '') }}
								</td>
								<td class="text-center" style="display: none">
									{{ (!empty($estadistica->nameCompleteUserEvaluado->department) ? str_replace('Ã‘','Ñ',$estadistica->nameCompleteUserEvaluado->department) : '') }}
								</td>
								<td class="text-center" style="display: none">
									{{ (!empty($estadistica->nameCompleteUserEvaluado->puesto) ? str_replace('Ã‘','Ñ',$estadistica->nameCompleteUserEvaluado->puesto) : '') }}
								</td>
								<td class="text-center">
									@if (!empty($estadistica->retro))
										<span class="btn btn-primary" style="width: 100%">Retro</span>
									@else
										@if($estadistica->status == 'No Iniciada')
										<span class="btn btn-danger">No Iniciada</span>
										@else
											@if($estadistica->status == 'Iniciada')
										<span class="btn btn-warning">Inconclusa</span>
											@else
										<span class="btn btn-success">Terminada</span>
											@endif
										@endif
									@endif
								</td>
							</tr>
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>

<form action="/estadisticas" method="post" class="form_estadisticas">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="hidden" name="id_periodo" value="{{$id_periodo}}" class="id_periodo">
	<input type="submit" style="display: none">
</form>
@endsection

@section('scripts')
<script>

	var table = '';

	$(document).ready(function(){


		table = $('.stats').DataTable({
      	dom: 'Blftip',
		language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  },
      		  
	  buttons: [
        {
          			extend: 'excel',
					text: 'Exportar a Excel',
					titleAttr: 'Exportar a Excel',
					title: 'Evaluaciones de Desempeño',
					
				}
        ]

  	});

		$('body').on('keyup', '#DataTables_Table_0_wrapper #DataTables_Table_0_filter label input', function(){

    	var busqueda = $(this).val();
    	var numero_columna = $('select#field').val() * 1;

    	if (numero_columna != -1){

    		table.columns().search('').draw();
      	table.columns([numero_columna]).search(busqueda).draw();
    	}
  	});
		
		$('select#field').change(function(){

    	var busqueda = $('#DataTables_Table_0_wrapper #DataTables_Table_0_filter label input').val();
    	var numero_columna = $(this).val() * 1;

    	if (numero_columna != -1){

      	table.columns().search('').draw();
      	table.columns([numero_columna]).search(busqueda).draw();
    	}

    	else{

    		table.columns().search('').draw();
    	}
  	});

  	$('select#periodos').change(function(){

  		$('form.form_estadisticas .id_periodo').val($(this).val());
  		$('form.form_estadisticas').submit();
  	});

  });


</script>
@endsection