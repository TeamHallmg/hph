@extends('layouts.app')

@section('title', 'Borrar Periodo')

@section('content')
<div class="row mt-3">
	<div class="col-md-2 text-right">
		@include('clima-organizacional/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">


<h3 class="titulos-evaluaciones mt-4 font-weight-bold">Borrar Periodo</h3>
<hr>

 <div class="row mt-3">
	<div class="col-md-3"></div>
	<div class="col-md-6 text-center">
		<form action="/borrar-periodo" method="post">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id" value="{{$periodo[0]->id}}">
			<div class="form-group">
        <label for="nombre">Se borrarán todas las evaluaciones, resultados, etc, relacionados con este periodo. ¿De verdad quieres eliminar el periodo {{$periodo[0]->descripcion}}?</label>
      </div>
			<div class="form-group">
        <button class="btn btn-danger" type="submit">Borrar</button> <a href="/periodos" class="btn btn-primary">Cancelar</a>
      </div>
    </form>
	</div>
	<div class="col-md-3"></div>
</div>
</div>
</div>
@endsection
