@extends('clima-organizacional.app')

@section('title', 'Editar Periodo')

@section('content')

<div class="row mt-3">
  <div class="col-md-2 text-right">
    @include('nps/partials/sub-menu')
  </div>
  <div class="col-md-10">
    <img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
<h3 class="titulos-evaluaciones mt-4 font-weight-bold">Periodos</h3>
<hr>
<form action="{{ url('nps/periodos/' . $periodo->id) }}" method="post" class="periodos">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="_method" value="PUT">
    <div class="card">
        <div class="card-header">
            Editar Periodo
        </div>
        <div class="card-header">
            <div class="row">
                <div class="col-md-6">
                    <label for="nombre" class="font-weight-bold requerido">Nombre</label>
                    <input type="text" class="form-control" name="name" required value="{{$periodo->name}}">
                </div>
                <div class="col-md-6">
                    <label for="status" class="requerido">Estado</label>
                    <select class="form-control estado" name="status">
                        @if ($periodo->status == 'Preparatorio')
                            <option value="Preparatorio">Preparatorio</option>
                        @endif
                        @if ($periodo->status != 'Cancelado' && (!$periodo_abierto || $periodo->status == 'Abierto'))
                            <option value="Abierto" {{ $periodo->status=='Abierto'?'selected':'' }}>Abierto</option>
                        @endif
                        @if ($periodo->status == 'Abierto' || $periodo->status == 'Cerrado')
                            <option value="Cerrado" {{ $periodo->status=='Cerrado'?'selected':'' }}>Cerrado</option>
                        @endif
                        <option value="Cancelado" {{ $periodo->status=='Cancelado'?'selected':'' }}>Cancelado</option>
                    </select>
                </div>
            </div>
            <div class="row mt-4">
              <div class="col-md-6">
                <label for="no_aplica" class="font-weight-bold">No aplica (N/A) aparece en la encuesta</label>
                <select class="form-control" name="no aplica">
                  <option value="0">No</option>
                  <option value="1" <?php if ($periodo->no_aplica == 1){ ?>selected="selected"<?php } ?>>Si</option>
                </select>
              </div>

            @if (count($enterprises) > 0)

                <div class="col-md-6">
                    <label for="enterprise_id" class="font-weight-bold">Empresa</label>
                    <select class="form-control enterprises" name="enterprise_id">

              @if ($periodo->status != 'Preparatorio')

                @if (empty($periodo->enterprise_id))

                      <option value="0">-- Todas --</option>
                @else

                  @foreach ($enterprises as $key => $enterprise)

                    @if ($periodo->enterprise_id == $enterprise->id)
                    
                      <option value="{{$enterprise->id}}" >{{$enterprise->name}}</option>
                    @endif
                  @endforeach
                @endif
              @else

                      <option value="0">-- Todas --</option>
                      
                @foreach ($enterprises as $key => $enterprise)
                    
                      <option value="{{$enterprise->id}}" <?php if ($periodo->enterprise_id == $enterprise->id){ ?>selected="selected"<?php } ?>>{{$enterprise->name}}</option>
                @endforeach
              @endif

                    </select>
                </div>
            @endif

            </div>
        </div>
    </div>

    <h3 class="titulos-evaluaciones mt-4 font-weight-bold">Preguntas</h3> 
<hr>
    <table class="table table-striped" id="table-preguntas">
        <thead>
            <tr>
                <th>ID</th>
                <th>Pregunta</th>
                <th>Dimensión</th>
                <th>Todas <input type="checkbox" class="select_all"></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($preguntas as $key => $pregunta)
                <tr>
                    <td>{{ $pregunta->id }}</td>
                    <td>{{ $pregunta->question }}</td>
                    <td>{{ (!empty($pregunta->factor) ? $pregunta->factor->name : '') }}</td>
                    <td>
                        <input type="checkbox" class="preguntas" value="{{ $pregunta->id }}" {{ (in_array($pregunta->id, $preguntas_periodo))?'checked':'' }}>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    
    <h3 class="titulos-evaluaciones mt-4 font-weight-bold">Evaluadores</h3>
<hr>
    <table class="table table-striped" id="table-evaluados">
        <thead>
            <tr>
                <th>ID</th>
                <th>Evaluador</th>
                <th>Empresa</th>
                <th>Departamento</th>
                <th>Área</th>
                <th>Puesto</th>
                <th>Jefe</th>
                <th>Todos <input type="checkbox" class="select_all_evaluados"></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($evaluados as $key => $evaluado)
                <tr>
                    <td>{{ $evaluado->employee_wt->idempleado }}</td>
                    <td>{{ $evaluado->Fullname }}</td>
                    <td>{{ (isset($evaluado->employee_wt->enterprise) ? $evaluado->employee_wt->enterprise->name : '') }}</td>
                    <td>{{ (isset($evaluado->employee_wt->jobPosition->area->department) ? $evaluado->employee_wt->jobPosition->area->department->name : '') }}</td>
                    <td>{{ (isset($evaluado->employee_wt->jobPosition->area->department->direction) ? $evaluado->employee_wt->jobPosition->area->department->direction->name : '') }}</td>
                    <td>{{ (isset($evaluado->employee_wt->jobPosition) ? $evaluado->employee_wt->jobPosition->name : '') }}</td>
                    <td>{{ (isset($evaluado->employee_wt->boss->user) ? $evaluado->employee_wt->boss->user->fullname : '') }}</td>
                    <td>
                        <input type="checkbox" class="evaluados" value="{{ $evaluado->id }}" {{ (in_array($evaluado->id,$evaluados_periodo))?'checked':'' }}>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    
    <div class="row">
        <div class="col-12">
            <div class="float-right">
                <button class="btn btn-success mr-3" type="submit" style="color: white"><span class="fas fa-check-circle"></span> Guardar</button> 
                <a class="btn btn-danger" href="/nps/periodos"><span class="fas fa-times-circle"></span> Regresar</a>
            </div>
        </div>
    </div>
</form>
</div>
</div>

@endsection

@section('scripts')
  <script>
	  
	  var questions_table = '';
    var users_table = '';
    
    $(document).ready(function(){

      questions_table = $('#table-preguntas').DataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
    });

  users_table = $('#table-evaluados').DataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
    });

        $('.select_all').click(function(){
          //var cells = questions_table.cells().nodes();
          var cells = questions_table.rows({ search: 'applied' }).nodes();
            if ($(this).prop('checked')){
                //$('.preguntas').prop('checked', true);
                $(cells).find(':checkbox').prop('checked', true);
            }else{
                //$('.preguntas').prop('checked', false);
                $(cells).find(':checkbox').prop('checked', false);
            }
        });

        $('.select_all_evaluados').click(function(){
          //var cells = users_table.cells().nodes();
          var cells = users_table.rows({ search: 'applied' }).nodes();
            if ($(this).prop('checked')){
                //$('.evaluados').prop('checked', true);
                $(cells).find(':checkbox').prop('checked', true);
            }else{
                //$('.evaluados').prop('checked', false);
                $(cells).find(':checkbox').prop('checked', false);
            }
        });

        $('select.estado').change(function(){
            if ($(this).val() == 'Cancelado'){
                alert("Si se cancela el periodo ya no podrá volver a abrirse y toda la información de este periodo se perderá")
            }
        });

        // Handle form submission event
   $('form.periodos').on('submit', function(e){
      var form = this;

      //checkboxes should have a general class to traverse
      var rowcollection = questions_table.$(".preguntas:checked", {"page": "all"});

      var checkbox_value = 0;

      //Now loop through all the selected checkboxes to perform desired actions
      rowcollection.each(function(index,elem){
      //You have access to the current iterating row
        checkbox_value = $(elem).val();
        $(form).append(
             $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'questions[]')
                .val(checkbox_value)
         );
      });

      rowcollection = users_table.$(".evaluados:checked", {"page": "all"});

      //Now loop through all the selected checkboxes to perform desired actions
      rowcollection.each(function(index,elem){
      //You have access to the current iterating row
        checkbox_value = $(elem).val();
        $(form).append(
             $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'users[]')
                .val(checkbox_value)
         );
      });
   });

   $('select.enterprises').change(function(){

      var enterprise_id = $(this).val();
      var enterprise_name = $(this).find('option[value="' + enterprise_id + '"]').text();

      if (enterprise_name == '-- Todas --'){

        users_table.column(2).search('').draw();
      }

      else{

        users_table.column(2).search("^" + enterprise_name + "$", true, false, true).draw();
      }
    });

    $('select.enterprises').change();
  });
  </script>
@endsection