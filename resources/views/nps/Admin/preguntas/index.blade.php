@extends('clima-organizacional.app')

@section('title', 'Preguntas')

@section('content')

<div class="row mt-3">
	<div class="col-md-2 text-right">
		@include('nps/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
<h3 class="titulos-evaluaciones mt-4 font-weight-bold">Preguntas</h3>
<hr>
<a href="{{ url('nps/preguntas/create') }}" id='agregarPregunta' class="btn btn-success"><span class="fas fa-plus-circle"></span> Agregar Pregunta</a>

<div class="my-3">
	<table class="table table-striped" id="preguntas">
		<thead class="bg-purple text-white">
			<tr>
				<th>ID</th>
				<th>Pregunta</th>
				<th>Sentido</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($preguntas as $pregunta)
				<tr>
					<td>{{$pregunta->id}}</td>
					<td>{{$pregunta->question}}</td>
					<td>{{($pregunta->positive == 1 ? 'Positivo' : 'Negativo')}}</td>
					<td>
						<a href="{{ url('nps/preguntas/' . $pregunta->id . '/edit') }}" class="btn btn-primary"><i class="fas fa-edit"></i> Editar</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>
</div>
</div>

@endsection

@section('scripts')
<script>

	$(document).ready(function(){

		$('#preguntas').DataTable({
			language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
     	}
		});
	});

</script>
@endsection