@extends('layouts.app')

@section('title', 'Areas')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-12">
		<img class="img-fluid" src="{{ asset('img/clima-organizacional/banner.png') }} " alt="">
		<h3 class="titulos-evaluaciones titulos-evaluaciones mt-3 font-weight-bold" style="margin-bottom: 20px">Grupos de áreas</h3>
		<hr class="mb-3">
		<center>
			<a class="btn btn-primary" href="{{ url('areas/create') }}">Nuevo grupo de áreas</a>
		</center>
		<br>
		<table class="table table-striped table-bordered">
			<thead style="background-color: #002C49; color:white;">
				<tr>
					<th>ID</th>
					<th>Grupo De Areas</th>
					<th>Areas</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				@if($gruposAreas->isEmpty())
					<tr>
						<td colspan="4" align="center">No hay registros</td>
					</tr>
				@endif
      	@foreach($gruposAreas as $grupoArea)
      		<tr>
						<td>{{ $grupoArea->id }}</td>
						<td>{{ $grupoArea->Name }}</td>
						<td>
							@foreach($areas_gruposAreas as $areasGrupo)
								@foreach($areasGrupo as $areaGrupo)
									@if($grupoArea->id == $areaGrupo->id_grupos_areas)
										{{ $areaGrupo->Areas->nombre }}
										<br>
									@endif
								@endforeach
							@endforeach
						</td>
						<td>
							@if($grupoArea->id == 1)
							@else
								<a class="btn btn-primary" href="{{ url('areas/' . $grupoArea->id . '/edit') }}">Editar</a>
							@endif
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection