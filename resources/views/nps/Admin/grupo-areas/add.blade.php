@extends('layouts.app')

@section('title', 'Crear grupo de areas')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-12">
		<img class="img-fluid" src="{{ asset('img/clima-organizacional/banner.png') }} " alt="">
		<h3 class="titulos-evaluaciones mt-3 font-weight-bold">Crear grupo de áreas</h3>
		<hr class="mb-3">
		<form method="post" action="/crear-grupo-area">
			{{ csrf_field() }}
			@if(!$otrasAreas->isEmpty())
				<div class="form-group">
					<label for="nombreGrupoArea">Nombre del grupo de áreas:</label>
					<input type="text" class="form-control" required name="nombreGrupoArea">
				</div>
			@endif
			<div class="form-group">
				<div class="table-responsive">
					<table class="table table-striped table-bordered" id="table-areas">
						<thead style="background-color: #002C49; color:white;">
							<tr>
								<th>
									ID
								</th>
								<th>
									Nombre
								</th>
								<th>
									Acción
								</th>
							</tr>
						</thead>
						<tbody>
							
							@if($otrasAreas->isEmpty())
								<tr>
									<td colspan="3" align="center">No hay áreas para seleccionar.</td>
								</tr>
							@else
								@foreach($otrasAreas as $otrasArea)
								<tr>
									<td>{{ $otrasArea->id_areas }}</td>
									<td>{{ $otrasArea->Areas->Name }}</td>
									<td align="center">
										<input type="checkbox" name="id_areas[]" value="{{$otrasArea->id_areas}}" class="field">
									</td>
								</tr>
								@endforeach
							@endif
						</tbody>
					</table>
				</div>
			</div>
			<div class="form-group">
				@if(!$otrasAreas->isEmpty())
					<button type="submit" class="btn btn-success">Guardar</button>
				@endif
				<a class="btn btn-primary" href="{{ URL::previous() }}">Regresar</a>
			</div>
		</form>

	</div>
</div>
@endsection

@section('scripts')
<script>
	$('#table-areas tbody tr').click(function (e) {
    if(!$(e.target).is('#table-areas td input:checkbox'))
    $(this).find('input:checkbox').trigger('click');
});
</script>
@endsection