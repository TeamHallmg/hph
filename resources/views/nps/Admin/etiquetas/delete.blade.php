@extends('layouts.app')

@section('title', 'Borrar Etiqueta')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right" style="padding: 0">
		@include('clima-organizacional/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
		<h4 class="margin-top-20 titulos-evaluaciones">Borrar Etiqueta</h4>
	</div>
</div>
<div class="row margin-top-20">
	<div class="col-md-3"></div>
	<div class="col-md-6 text-center">
		<form action="/clima-organizacional/borrar-etiqueta" method="post">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id" value="{{$etiqueta[0]->id}}">
			<div class="form-group">
        <label for="nombre">¿De verdad quieres eliminar la etiqueta {{$etiqueta[0]->name}}?</label>
      </div>
			<div class="form-group">
        <button class="btn btn-danger" type="submit">Borrar</button> <a href="/clima_organizacional/etiquetas" class="btn btn-primary">Cancelar</a>
      </div>
    </form>
	</div>
	<div class="col-md-3"></div>
</div>
@endsection
