@extends('clima-organizacional.app')

@section('title', 'Encuestas')
@section('content')
<style>
	table.dataTable thead {background-color:#406BB2; color: white;}
</style>
<div class="row mt-3">
	<div class="col-md-2 text-right">
		@include('nps/partials/sub-menu')
	</div>
	<div class="col-md-10">
		{{-- @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['banner'], 'section' => 'clima'])  --}}
		<img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
	<h3 class="titulos-evaluaciones mt-4 font-weight-bold">Preguntas índice de fidelización eNps</h3>
	<div class="card">
		<div class="row justify-content-center pb-5">
    
			<div class="col-3">
				<div class="form-group text-center">
					<br>
					<label for="name" class="requerido">Periodo:</label>
					<select class="respuesta form-control" >
					  <option value="{{$periodoStatus->id}}">{{$periodoStatus->name}}</option>
					</select>   
				</div>
			</div>
		</div>
	</div>

	<div class="row container-fluid my-3">
		@if (auth()->user()->email == 'soporte@hallmg.com')
		<div class="col-12">
			<div class="exportar_resultados mt-4">
				<form action="/nps/exportar-resultados-nps" method="post">
				  <input type="hidden" name="_token" value="{{ csrf_token() }}">
				  <input type="hidden" name="period_id" value="{{$periodoStatus->id}}">
				  <input type="submit" value="Exportar Respuestas" class="btn btn-primary">
				</form>
			</div>
		</div>
		@endif
		<div class="col-12">
			<table class="table table-bordered table-striped encuesta">
				<thead class="card-header">
					<tr>
						<th> # </th>
						<th>Centro de Trabajo</th>
						<th>Departamento</th>
						<th>Pregunta</th>
						<th>Calificación</th>
						<th>Comentario</th>
					</tr>
				</thead>
				<tbody>
					@php
         				$count = 0;
		        	@endphp
					@foreach ($answers as $answer)
					<tr>
						<td>{{ ++$count }}</td>
						<td>{{ $answer->region }}</td>
						<td>{{ $answer->department }}</td>
						<td>{{ str_replace($answer->number.". ", "", $answer->question) }}</td>
						<td>{{ $answer->answer }}</td>
						<td>{{ $answer->porque }}</td>
					</tr>	
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

  </div>
</div>
@endsection
@section('scripts')
<script>
$(document).ready(function() {
	var groupColumn = 3;
	let encuesta = $('.encuesta').DataTable({
      language: {
        "sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
            },
            "aaSorting": [],
			"columnDefs": [
				{"visible": false, "targets": groupColumn}
			],
			"drawCallback": function( settings ) {
				var api = this.api();
				var rows = api.rows({page:'current'}).nodes();
				var last = null;
				api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
					if ( last !== group ) {
						$(rows).eq( i ).before(
							'<tr class="group"><td colspan="5">'+group+'</td></tr>'
						);
                    	last = group;
                	}
            	} );
			}
		
    });
});
</script>
@endsection