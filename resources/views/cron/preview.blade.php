@extends('layouts.app')

@section('title', 'Administrar Usuarios')

@section('content')

<div class="row mb-3">
    <div class="col-12 bg-company-primary text-white rounded py-3">
        <h2 class="font-italic font-weight-bold m-0"><i class="far fa-calendar-alt"></i> Administrar Usuarios | Importación</h2>
    </div>
</div>
<div class="row mb-2">
	@if(session()->has('errors'))
		<div class="alert alert-danger alert-dismissible fade show card-1" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				<span class="sr-only">Close</span>
			</button>
			<strong>{{$errors->first()}}</strong>
		</div>
	@endif
	
	<div class="col-12 mt-1 mb-3">
		<h4 class="text-white font-weight-bold m-0 rounded-pill bg-company-secondary px-3 py-1 d-flex justify-content-between">
			Datos a Importar
		</h4>
	</div>
</div>

<div class="table-responsive">
	<div class="col-md-12 tableWrapper d-none">
        <table class="table table-bordered">
            <thead class="bg-company-primary text-white">
                <tr>
                    @foreach($data['summary'] as $key => $row)
                        <th>{{ $row['title'] }}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody id="tbody">
                <tr>
		    	    @foreach($data['summary'] as $row)
                        <td class="text-center">
                            {{ $row['count'] }}
                        </td>    
				    @endforeach
                </tr>
        	</tbody>
    	</table>
	</div>
</div>

<hr>

<div class="table-responsive">
	<div class="col-md-12 tableWrapper d-none">
        <table class="table table-bordered" id="userImport">
            <thead class="bg-company-primary text-white">
                <tr>
                    @foreach($data['headers'] as $key => $header)
                        <th class="{{ $key < 4?'bg-company-secondary':'' }}">{{ strtoupper($header) }}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody id="tbody">
		    	@foreach($data['layout'] as $row)
                    <tr>
                        @foreach($row as $value)
                            <td class="text-center">
                                {{ $value }}
                            </td>
                        @endforeach
                    </tr>
				@endforeach
        	</tbody>
    	</table>
	</div>
</div>

<div class="row">
    <div class="col-12 text-right">
        <a href="{{ url('admin-de-usuarios') }}" class="btn btn-secondary">
            Regresar
        </a>
        <a href="{{ url('cron/process_import/'.$data['filename']) }}" class="btn btn-success">
            Confirmar importación
        </a>
    </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('#userImport').DataTable({
			"order": [[ 0, "asc" ]],
			"paging": true,
			"pagingType": "numbers",
			"buttons": [
				{
					"extend": 'excelHtml5',
					"text": 'Exportar a Excel',
					"titleAttr": 'Exportar a Excel',
					"title": 'Administración de Usuarios',
					"exportOptions": {
						"columns": [1, ':visible']
					}
				}
			],
			"scrollX": true,
			"fixedColumns":{
				"leftColumns": 4,
			},
			"language": {
		 		"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     ">",
					"sPrevious": "<"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
      		  }
	  	});

		setTimeout(() => {
			$('.tableWrapper').removeClass('d-none');
			$($.fn.dataTable.tables(true)).DataTable()
			.columns.adjust()
			.fixedColumns().relayout();
		}, 500);
	});
	
		
</script>
@endsection