  {{-- @include('partials.google-analytics') --}}

	<!-- HEADER -->
		<header>
      <div class="row">
        @if (Auth::check())
        <div id="content" class="container">
          <div class="row clearfix">
            <div class="col-md-1"></div>

    @if (Session::has('primer_login'))
            <div class="col-md-3">
              <a href="{{ url('/') }}">
                <img class="img-responsive" src="{{ asset('/img/header/logo maver.png') }}">
              </a>
            </div>
            <div class="col-md-5">
              <h3 class="titulos-evaluaciones text-center" style="margin-top: 0">Cambia tu Contraseña</h3>
              <form class="form-horizontal cambiar_contrasena" role="form" method="POST" action="/cambiar-contrasena/{{auth()->user()->id}}" onsubmit="if ($('.nueva_contrasena').val() != $('.confirm_password').val()){ alert('Las contraseñas no coinciden'); return false; }">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="col-md-6 text-center" style="padding-right: 0">
                  <label style="font-size: 15px">Nueva Contraseña</label>
                  <div>
                    <input type="password" class="form-control nueva_contrasena" name="nueva_contrasena" required style="width: 100%">
                  </div>
                </div>
                <div class="text-center col-md-6" style="padding-left: 0">
                  <label style="font-size: 15px">Confirmar Contraseña</label>
                  <div>
                    <input type="password" class="form-control confirm_password" name="confirm_password" required style="width: 100%">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="text-center">
                    <button type="submit" class="btn btn-primary">Cambiar</button>
                  </div>
                </div>
              </form>
            </div>
    @else
            <div class="col-md-8">
              <a href="{{ url('/') }}">
                <img class="img-responsive" src="{{ asset('/img/header/logo maver.png') }}">
              </a>
            </div>
    @endif
            <div class="col-md-3 text-center">
              <p class="user-chenson">Bienvenido {{ str_replace('Ã‘','Ñ',auth()->user()->first_name) }}</p>
              <a class="btn btn-chenson-red" href="{{ url('logout') }}">Cerrar Sesión</a>
            </div>
          </div>
          <!-- <div class="col-md-1"></div> -->
        </div>
        @endif
      </div>
			<!--div class="col-md-4 column">
        <a href="{{ url('/') }}">
          <img class="img-responsive center" width="300" src="{{ asset('/img/header/logo-chenson.png') }}">
        </a>
			</div>
			<div class="col-md-4 col-md-offset-3 column">
				<img style="margin-top: 40px" width="350" class="img-responsive center" width="" src="{{ asset('/img/header/movimiento.png') }}">
			</div>-->
		</header>
		<div id="menu-container">
      @if (Auth::check())
			 @include('partials/menu')
      @endif
		</div>
	<!-- /HEADER -->

  <!-- CONTENT -->
  <div id="content" class="container">
    <div class="row clearfix">
      <!-- MAIN CONTENT -->
      <div class="col-md-1 column">
      </div>
      <div class="col-md-10 column">

        <div class="margin-top-20">
          @include('flash::message')
        </div>

        @yield('content')
      </div>
      <div class="col-md-1 column">
      </div>
      <!-- /MAIN CONTENT -->
    </div>
  </div>
  <!-- CONTENT -->

  <!-- FOOTER -->
    <footer class="margin-top-20" style="position: relative">
		<div style="position: absolute; right: 0; bottom: 70px; z-index: -1">
        <img src="/img/footer/Marca de agua_caps-01.png" style="max-width: 350px">
      </div>
		<div style="text-align:center;">
			<i>Grupo Maver</i><br>
			<i>Desarrollar, fabricar y comercializar productos que contribuyan a mejorar la calidad de vida de las personas.</i>
		</div>
    </footer>
  <!-- /FOOTER -->

  <!-- Scripts -->
  {{-- <script src="{{ asset('/js/jquery.min.js') }}"></script>
  <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('/js/bootstrap-switch.min.js') }}"></script>
  <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/js/dataTables.fixedColumns.min.js') }}"></script>
  <script src="{{ asset('/js/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('/js/jszip.min.js') }}"></script>
  <script src="{{ asset('/js/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('/js/html2canvas.js') }}"></script>
  <script src="{{ asset('/js/highcharts.js') }}"></script> --}}
	
  <!--Start of Tawk.to Script-->
    <!--<script type="text/javascript">
      var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
      (function(){
      var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
      s1.async=true;
      s1.src='https://embed.tawk.to/593edfcbb3d02e11ecc6983e/default';
      s1.charset='UTF-8';
      s1.setAttribute('crossorigin','*');
      s0.parentNode.insertBefore(s1,s0);
      })();
    </script>-->
  <!--End of Tawk.to Script-->
  @yield('scripts')
</body>
</html>
