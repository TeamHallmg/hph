<li class="nav-item has-treeview">
    <a href="#" class="nav-link">
            <i class="nav-icon fas fa-temperature-low"></i>
        <p>
        Clima Organizacional
        <i class="right fa fa-angle-left"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a class="nav-link" href="{{ url('clima-organizacional/que-es') }}">
                <i class="fa fa-plus nav-icon"></i>
                <p><span>¿Qué es?</span></p>
            </a>
        </li>
        @if(Auth::user()->role != 'admin')
            <li class="nav-item">
                <a class="nav-link" href="{{ url('clima-organizacional/evaluaciones') }}">
                    <i class="fa fa-plus nav-icon"></i>
                    <p><span>Encuesta</span></p>
                </a>
            </li>
        @endif
        @if (auth()->user()->role == 'admin' || isset($userPermissions['see_progress']))
            <li class="nav-item">
                <a class="nav-link" href="{{ url('clima-organizacional/avances') }}">
                    <i class="fa fa-plus nav-icon"></i>
                    <p><span>Avances</span></p>
                </a>
            </li>
        @endif
        @if (auth()->user()->role == 'admin' || isset($userPermissions['see_reports']))
            <li class="nav-item">
                <a class="nav-link" href="{{ url('clima-organizacional/reporte-grafico') }}">
                    <i class="fa fa-plus nav-icon"></i>
                    <p><span>Reportes</span></p>
                </a>
            </li>
        @endif
        @if (auth()->user()->role == 'admin' || isset($userPermissions['see_factors']))
            <li class="nav-item">
                <a class="nav-link" href="{{ url('clima-organizacional/factores') }}">
                    <i class="fa fa-plus nav-icon"></i>
                    <p><span>Factores</span></p>
                </a>
            </li>
        @endif
        @if (auth()->user()->role == 'admin' || isset($userPermissions['see_questions']))
            <li class="nav-item">
                <a class="nav-link" href="{{ url('clima-organizacional/preguntas') }}">
                    <i class="fa fa-plus nav-icon"></i>
                    <p><span>Preguntas</span></p>
                </a>
            </li>
        @endif
        @if (auth()->user()->role == 'admin' || isset($userPermissions['see_periods']))
            <li class="nav-item">
                <a class="nav-link" href="{{ url('clima-organizacional/periodos') }}">
                    <i class="fa fa-plus nav-icon"></i>
                    <p><span>Periodos</span></p>
                </a>
            </li>
        @endif
    </ul>
</li>