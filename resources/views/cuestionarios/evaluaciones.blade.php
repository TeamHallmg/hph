@extends('clima-organizacional.app')

@section('title', 'Encuesta')

@section('content')
<style type="text/css">
	.card {border: 0;}
	.card .nav-item {width: 23%; margin: 0 1%;}
</style>
<?php $sociograma_questions_ids = array(); ?>
<div class="row mt-3">
	<div class="col-md-2 text-right">
		@include('cuestionarios/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
		<div class="card cuestionarios">
			<ul class="nav nav-pills mt-3" id="pills-tab" role="tablist">
				<li class="nav-item guia-tab text-center">
					<a class="btn-tab active fondo_diferente" id="guia-tab" data-toggle="pill" href="#guia" role="tab" aria-controls="guia" aria-selected="true">Guía General</a>
				</li>

			@if (!empty($evaluado_nps))

				<li class="nav-item cuestionarios-tab text-center">
					<a class="btn-tab {{($evaluado_nps->status == 1 ? 'fondo_rojo' : ($evaluado_nps->status == 2 ? 'fondo_amarillo' : 'fondo_verde'))}}" id="seccion1-tab" data-toggle="pill" href="#seccion1" role="tab" aria-controls="seccion1" aria-selected="false">eNPS</a>
				</li>
			@endif

			@if (!empty($evaluado_clima))

				<li class="nav-item cuestionarios-tab text-center">
					<a class="btn-tab {{($evaluado_clima->status == 1 ? 'fondo_rojo' : ($evaluado_clima->status == 2 ? 'fondo_amarillo' : 'fondo_verde'))}}" id="seccion2-tab" data-toggle="pill" href="#seccion2" role="tab" aria-controls="seccion2" aria-selected="false">CLIMA LABORAL</a>
				</li>
			@endif

			@if (!empty($evaluado_sociograma))

				<li class="nav-item cuestionarios-tab text-center">
					<a class="btn-tab {{($evaluado_sociograma->status == 1 ? 'fondo_rojo' : ($evaluado_sociograma->status == 2 ? 'fondo_amarillo' : 'fondo_verde'))}}" id="seccion3-tab" data-toggle="pill" href="#seccion3" role="tab" aria-controls="seccion3" aria-selected="false">SOCIOMETRÍA</a>
				</li>
			@endif

			</ul>
		</div>
		<div class="card-body cuestionarios">
			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="guia" role="tabpanel" aria-labelledby="guia-tab">
					<div class="row">
						<div class="col-12 col-md-12">
							<div class="guia_title">GUÍA GENERAL</div>

			@if ((!empty($evaluado_nps) && !empty($evaluado_sociograma)) || (!empty($evaluado_nps) && !empty($evaluado_clima)) || (!empty($evaluado_clima) && !empty($evaluado_sociograma)))

							<p class="mt-3">Recibe una cordial bienvenida a este ejercicio de opinión, elaborado para convertir a <b>Hospitales Puerta de Hierro</b>, en el espacio donde logremos disfrutar nuestro trabajo al máximo y demos los mejores resultados.</p>
							<p>Para ello, te invitamos a emitir tu opinión sobre diversos temas que ponemos a tu disposición a través de estos cuestionarios. Con este ejercicio se busca identificar los aspectos que debemos mejorar y reconocer aquellos que debemos fortalecer.</p>
							<p>Agradecemos de antemano tu sinceridad al responder, ya que tu aportación será clave para realizar acciones de mejora en nuestro ambiente laboral.</p>
							<p>Sus opiniones sobre estos temas guiarán los esfuerzos de la direccion general para hacer de nuestra organización un excelente lugar para trabajar.</p>
							<p>Sus respuestas serán tratadas con la más estricta confidencialidad y su identidad no será revelada de ninguna manera. </p>
							<p>La forma para responder es muy sencilla, solamente debes ir a la pestaña de cada sección y seguir las instrucciones que contiene cada una de ellas.</p>
			@else

							<p>Te agradecemos por formar parte de esta encuesta {{(!empty($evaluado_nps) ? 'eNPS' : (!empty($evaluado_sociograma) ? 'SOCIOMETRÍA' : 'CLIMA LABORAL'))}}, queremos saber si actualmente si nos recomendarías con tus conocidos y amigos, ya que estamos realizando este ejercicio para conocer cómo pudiéramos mejorar <b>Hospitales Puerta de Hierro</b> para que disfrutes más tu trabajo.</p>
							<p>Para ello, te invitamos a emitir tu opinión con toda sinceridad, asegurándote que las respuestas serán tratadas con la más estricta confidencialidad y tu identidad no será revelada de ninguna manera.</p>
							<p>La forma de responder es muy sencilla, selecciona la pestaña "{{(!empty($evaluado_nps) ? 'eNPS' : (!empty($evaluado_sociograma) ? 'SOCIOMETRÍA' : 'CLIMA LABORAL'))}}" y sigue las instrucciones que contiene la página.</p>
			@endif

							<p class="text-center">Al iniciar todas las pestañas estarán en <b>color rojo</b> debido a que aún no han sido contestadas.</p>
							<p>

			@if (empty($evaluado_nps) || !empty($evaluado_clima) || !empty($evaluado_sociograma))

								<img class="img-fluid" src="/img/cuestionarios/guia_general/imagen 1.png">
			@else

								<img class="img-fluid" src="/img/cuestionarios/nps/imagen 1.png">
			@endif
							</p>
							<p class="text-center mt-3">Posteriormente, cuando comiences a responder el color de la pestaña cambiará a <b>color amarillo</b></p>
							<p>

			@if (empty($evaluado_nps) || !empty($evaluado_clima) || !empty($evaluado_sociograma))

								<img class="img-fluid" src="/img/cuestionarios/guia_general/imagen 2.png">
			@else

								<img class="img-fluid" src="/img/cuestionarios/nps/imagen 2.png">
			@endif

							</p>
							<p class="text-center mt-3">y cuando termines con la sección, el <b>color cambiará a verde</b>.</p>
							<p>

			@if (empty($evaluado_nps) || !empty($evaluado_clima) || !empty($evaluado_sociograma))

								<img class="img-fluid" src="/img/cuestionarios/guia_general/imagen 3.png">
			@else

								<img class="img-fluid" src="/img/cuestionarios/nps/imagen 3.png">
			@endif

							</p>
							<p class="text-center mt-3">Una vez que finalices cada sección deberás presionar el botón <b>enviar</b> para que tus respuestas sean procesadas.</p>
							<p>

			@if (empty($evaluado_nps) || !empty($evaluado_clima) || !empty($evaluado_sociograma))

								<img class="img-fluid" src="/img/cuestionarios/guia_general/imagen 4.png">
			@else

								<img class="img-fluid" src="/img/cuestionarios/nps/imagen 4.png">
			@endif

							</p>
						</div>
					</div>
					<div class="text-center">
						<h1 class="sociograma_table">
							<b>¡Iniciamos!</b>
						</h1>
					</div>
					<hr>
					<div class="text-center">

			@if (!empty($evaluado_nps))

						<input type="button" value="Ir a los Cuestionarios" class="btn fondo_turqueza cl-white guia_title" data-to="1">
			@else

				@if (!empty($evaluado_clima))

						<input type="button" value="Ir a los Cuestionarios" class="btn fondo_turqueza cl-white guia_title" data-to="2">
				@else

						<input type="button" value="Ir a los Cuestionarios" class="btn fondo_turqueza cl-white guia_title" data-to="3">
				@endif
			@endif

					</div>
				</div>

			@if (!empty($evaluado_nps))

				<div class="tab-pane fade" id="seccion1" role="tabpanel" aria-labelledby="seccion1-tab">
					<div class="row">
						<div class="col-12 col-md-12 color-personalizado-marron">
							<div class="guia_title">eNPS</div>
							<h5 class="titulos-evaluaciones mt-4 font-weight-bold">Instrucciones</h5>
							{{-- <p>Marca la casilla que mejor describa tu respuesta siendo 0 la calificación más baja y 10 la calificación más alta para describir que tan dispuesto estás para recomendar a tus conocidos amigos y/o familiares a trabajar en Puerta de Hierro.</p> --}}
						</div>
					</div>
					<form action="{{ url('/nps/guardar-respuestas') }}" method="post" target="my_frame">
							@csrf
						<div class="row">
							<div class="col-12 col-md-12 fondo_blanco">

    @if (count($nps_questions) > 0)

			@foreach ($nps_questions as $key => $question)

								<p>{{$key + 1}}. {{$question->question}}</p>
								<ul class="nps_buttons text-center">

				@for ($i = 0;$i < 11;$i++)

									<li>
										<div>
											<span class="questions {{(isset($nps_answers[$question->question_id]) && $nps_answers[$question->question_id] == $i ? 'selected' : '')}}" id="{{$question->question_id}}">{{$i}}</span>
										</div>
									</li>
				@endfor

								</ul>
								<input type="hidden" name="question[{{$question->question_id}}]" value="{{(isset($nps_answers[$question->question_id]) ? $nps_answers[$question->question_id] : '')}}">
								<div class="mt-3">
									¿Porque?<br>
									<textarea name="why[{{$question->question_id}}]" class="nps_porque">{{(isset($nps_why[$question->question_id]) ? $nps_why[$question->question_id] : '')}}</textarea>
								</div>
			@endforeach
		@endif

							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-4">
							</div>
							<div class="col-md-4 text-center">
								<input type="submit" value="Enviar" class="btn fondo_verde send_answers" data-card="1">
							</div>
							<div class="col-md-4 text-right">

				@if (!empty($evaluado_clima))

								<input type="button" value="Siguiente encuesta" class="btn fondo_turqueza cl-white text-center right" data-to="2">
				@else

					@if (!empty($evaluado_sociograma))

								<input type="button" value="Siguiente encuesta" class="btn fondo_turqueza cl-white text-center right" data-to="3">
					@endif
				@endif

							</div>
						</div>
					</form>
				</div>
			@endif

			@if (!empty($evaluado_clima))

				<div class="tab-pane fade" id="seccion2" role="tabpanel" aria-labelledby="seccion2-tab">
					<div class="row">
						<div class="col-12 col-md-12 color-personalizado-marron">
							<div class="guia_title">CLIMA LABORAL</div>
							<form action="{{ url('/clima-organizacional/guardar-respuestas') }}" method="post" target="my_frame">
	@csrf
	<h5 class="titulos-evaluaciones mt-4 font-weight-bold">Instrucciones</h5>
	<p>Lee con cuidado cada pregunta y responde qué tan de acuerdo o desacuerdo estás con lo que describe. De tu lado derecho se encuentran unos "emojis" para que indiques tu opinión. Presiona debajo de aquel que mejor represente tu opinión</p>

	@foreach ($closed_questions as $factor)
		
		<table class="table">
			<thead class="titulos-evaluaciones font-weight-bold bg-blue p-2 rounded cl-white">
				<tr>
					<th style="min-width: 50%">
						<h3 style="margin: 0;">Pregunta</h3>
					</th>

				@if ($no_aplica)

					<th class="text-center" style="font-size: 10px">No aplica</th>
				@endif

				@foreach ($etiquetas as $etiqueta)

					<th class="text-center" style="font-size: 10px">{{$etiqueta->name}}</th>
				@endforeach

				</tr>
			</thead>
			<tbody>
				@foreach ($factor->questions as $question)
					<tr <?php if ($status->status > 1 && is_null($question->answer)){ ?>class="btn-ambar"<?php } ?>>
						<td class="w-50">{{ $question->question }}</td>

					@if ($no_aplica)

						<td class="text-center pregunta">
							<div style="width: 40px; height: 40px; border-radius: 50%; background-color: gray"></div><br>
							<input type="radio" name="question[{{ $question->id }}]" class="{{ $question->id }}" value="N/A" {{ (!is_null($question->answer) && ($question->answer->answer == 'N/A'))?'checked':'' }}>
						</td>
					@endif

					@foreach ($etiquetas as $key => $etiqueta)

						@if ($question->positive == 1)

				<?php $tag = $etiqueta; ?>
						@else

				<?php $tag = $etiquetas[count($etiquetas) - 1 - $key]; ?>
						@endif

						<td class="text-center pregunta"><div style="min-height: 40px">@if (!empty($tag->file))<img src="/img/clima/etiquetas/{{$tag->id}}/{{$tag->file}}" style="max-width: 100%">@endif</div><br><input type="radio" name="question[{{ $question->id }}]" class="{{ $question->id }}" value="{{$etiqueta->valor}}" {{ (!is_null($question->answer) && ($question->answer->answer == $etiqueta->valor))?'checked':'' }}></td>
					@endforeach

					</tr>
				@endforeach

			</tbody>
		</table>
	@endforeach
	<!--<h3 class="titulos-evaluaciones mt-4 font-weight-bold bg-purple bg-purple-sepanka p-2 rounded cl-white">Factor</h3>
	<div style="background-color: #EDEDF6; padding: 10px">
		<h3 class="text-center titulos-evaluaciones" style="margin-top: 0">Preguntas Abiertas</h3>
	</div>
	<h3 class="titulos-evaluaciones mt-4 font-weight-bold bg-purple bg-purple-sepanka p-2 rounded cl-white">Pregunta</h3>-->
	<table class="table">
		<thead class="titulos-evaluaciones font-weight-bold bg-blue p-2 rounded cl-white">
			<tr>
				<td class="w-50">
					<h3 style="margin: 0">Pregunta</h3>
				</td>
				<td></td>
			</tr>
		</thead>
		<tbody>
			@foreach ($opened_questions as $question)
				<tr <?php if ($status->status > 1 && is_null($question->answer)){ ?>class="btn-ambar"<?php } ?>>
					<td class="w-50">{{ $question->question }}</td>
					<td><textarea class="respuesta_abierta form-control" id="{{$question->id}}" name="question[{{ $question->id }}]">{{ (!is_null($question->answer))?$question->answer->answer:'' }}</textarea></td>
				</tr>
			@endforeach
		</tbody>
	</table>

	<div class="row">
		<div class="col-md-4">

		@if (!empty($evaluado_nps))

			<input type="button" value="Encuesta anterior" class="btn bg-blue cl-white text-center left" data-to="1">
		@endif

		</div>
		<div class="col-md-4 text-center">
			<input type="submit" value="Enviar" class="btn fondo_verde send_answers" data-card="2">
		</div>
		<div class="col-md-4 text-right">

		@if (!empty($evaluado_sociograma))

			<input type="button" value="Siguiente encuesta" class="btn fondo_turqueza cl-white text-center right" data-to="3">
		@endif

		</div>
	</div>
</form>
						</div>
					</div>
				</div>
			@endif

			@if (!empty($evaluado_sociograma))

				<div class="tab-pane fade" id="seccion3" role="tabpanel" aria-labelledby="seccion3-tab">
					<div class="row">
						<div class="col-12 col-md-12 color-personalizado-marron">
							<div class="guia_title">SOCIOMETRÍA</div>
							<h5 class="titulos-evaluaciones mt-4 font-weight-bold">Instrucciones</h5>
							<p>Llene los espacios con los nombres de las personas que usted crea que cumplen con las características mencionadas en cada pregunta</p>
						</div>
					</div>
					<form action="{{ url('/sociograma/guardar-respuestas') }}" method="post" target="my_frame">
							@csrf
						<div class="row">
							<div class="col-12 col-md-12 fondo_blanco">
								<table class="sociograma_table">
									<tbody>
							
		@if (count($sociograma_questions) > 0)

			@foreach ($sociograma_questions as $key => $question)

	<?php $sociograma_questions_ids[] = $question->question_id; ?>

										<tr>
											<td width="50%" class="users {{($key > 0 ? 'separate' : '')}}">{{$key + 1}}. {{$question->question}}</td>
											<td class="users {{($key > 0 ? 'separate' : '')}}">
												<div class="row">
													<div class="col-md-3 text-right">
														<p><b>Persona 1</b></p>
													</div>
													<div class="col-md-8">
														<input type="text" class="users_autocomplete users_autocomplete{{$key+1}}" placeholder="Escribe el nombre" data-question="{{$key+1}}" data-index="1" data-question_id="{{$question->question_id}}" value="{{(isset($sociograma_answers[$question->question_id][0]) ? $sociograma_users[$sociograma_answers[$question->question_id][0]] : '')}}"><input type="hidden" class="user_id" name="user_id[{{$question->question_id}}][]" value="{{(isset($sociograma_answers[$question->question_id][0]) ? $sociograma_answers[$question->question_id][0] : '')}}">
													</div>
				@if (isset($sociograma_answers[$question->question_id][0]))

													<div class="col-md-1">
														<button class="btn btn-danger delete_sociograma" type="button">
															<i class="fas fa-trash-alt"></i>
														</button>
													</div>
				@endif

												</div>
												<div class="row users">
													<div class="col-md-3 text-right">
														<p><b>Persona 2</b></p>
													</div>
													<div class="col-md-8">
														<input type="text" class="users_autocomplete users_autocomplete{{$key+1}}" placeholder="Escribe el nombre" data-question="{{$key+1}}" data-index="2" data-question_id="{{$question->question_id}}" value="{{(isset($sociograma_answers[$question->question_id][1]) ? $sociograma_users[$sociograma_answers[$question->question_id][1]] : '')}}"><input type="hidden" class="user_id" name="user_id[{{$question->question_id}}][]" value="{{(isset($sociograma_answers[$question->question_id][1]) ? $sociograma_answers[$question->question_id][1] : '')}}">
													</div>

				@if (isset($sociograma_answers[$question->question_id][1]))

													<div class="col-md-1">
														<button class="btn btn-danger delete_sociograma" type="button">
															<i class="fas fa-trash-alt"></i>
														</button>
													</div>
				@endif

												</div>
												<div class="row users">
													<div class="col-md-3 text-right">
														<p><b>Persona 3</b></p>
													</div>
													<div class="col-md-8">
														<input type="text" class="users_autocomplete users_autocomplete{{$key+1}}" placeholder="Escribe el nombre" data-question="{{$key+1}}" data-index="3" data-question_id="{{$question->question_id}}" value="{{(isset($sociograma_answers[$question->question_id][2]) ? $sociograma_users[$sociograma_answers[$question->question_id][2]] : '')}}"><input type="hidden" class="user_id" name="user_id[{{$question->question_id}}][]" value="{{(isset($sociograma_answers[$question->question_id][2]) ? $sociograma_answers[$question->question_id][2] : '')}}">
													</div>

				@if (isset($sociograma_answers[$question->question_id][2]))

													<div class="col-md-1">
														<button class="btn btn-danger delete_sociograma" type="button">
															<i class="fas fa-trash-alt"></i>
														</button>
													</div>
				@endif

												</div>
											</td>
										</tr>
			@endforeach
		@endif

									</tbody>
								</table>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-4">

				@if (!empty($evaluado_clima))

								<input type="button" value="Encuesta anterior" class="btn bg-blue cl-white text-center left" data-to="2">
				@else

					@if (!empty($evaluado_nps))

								<input type="button" value="Encuesta anterior" class="btn bg-blue cl-white text-center left" data-to="1">
					@endif
				@endif

							</div>
							<div class="col-md-4 text-center">
								<input type="submit" value="Enviar" class="btn fondo_verde send_answers" data-card="3">
							</div>
						</div>
					</form>
				</div>
			@endif

			</div>
		</div>
	</div>
</div>
<iframe name="my_frame" width="0" height="0" class="my_frame" onload="checkFrame()"></iframe>
@endsection
@section('scripts')
  <script>

    var users = <?php echo json_encode($users)?>;
    var total_sociograma_questions = <?php echo count($sociograma_questions)?>;
    var sociograma_answers = <?php echo json_encode($sociograma_answers)?>;
    var sociograma_questions_ids = <?php echo json_encode($sociograma_questions_ids)?>;
    var current_index = 0;
    var current_question = '';
    var current_value = 0;
    var selected_values = [];
    var current_card = 0;
    var i = 0, j = 0, k = 0, id_pregunta = 0, respuesta = 0;
    var next_prev = false;

    $(document).ready(function(){

    	$.ajaxSetup({
    		headers: {
        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    		}
			});

    	selected_values[0] = [];

    	for (i = 0;i < sociograma_questions_ids.length;i++){

    		j = i + 1;
    		selected_values[j] = [];

    		if (sociograma_answers[sociograma_questions_ids[i]] !== undefined){

    			current_index = 0;

    			do{

    				selected_values[j][current_index] = sociograma_answers[sociograma_questions_ids[i]][current_index];
    				current_index++;

    			}while(sociograma_answers[sociograma_questions_ids[i]][current_index] !== undefined);
    		}
    	}

    	for (i = 1;i <= total_sociograma_questions;i++){
        
      	$('.users_autocomplete' + i).autocomplete({
      
        	lookup: users[i],
        	lookupFilter: function(suggestion, originalQuery, queryLowerCase){

        		for (k = 0;k < selected_values[suggestion.index].length;k++){

        			if (selected_values[suggestion.index][k] == suggestion.data){

        				return false;
        			}		
        		}

						if (suggestion.value.toLowerCase().indexOf(queryLowerCase) >= 0){

							return suggestion;
						}        	
        	},
        	minChars: 1,
        	onSelect: function (suggestion){

          	var id_pregunta = $(this).attr('data-question_id');
          	var button = $(this);
						var respuesta = suggestion.data;

						$.ajax({
        			type:'POST',    
        			url: '/sociograma/guardar-respuesta',
        			data:{
          			id_pregunta : id_pregunta,
          			respuesta : respuesta
        			},
        			success: function(){

								button.parent().parent().append('<div class="col-md-1"><button class="btn btn-danger delete_sociograma" type="button"><i class="fas fa-trash-alt"></i></button></div>');
								selected_values[suggestion.index].push(suggestion.data);
								button.next().val(suggestion.data);
        			}
      			});
        	}
      	});
     	}

     	$('span.questions').click(function(){

				var id_pregunta = $(this).attr('id');
				var respuesta = $(this).text() * 1;
				//$(this).next().val(respuesta);
				$(this).parent().parent().parent().next().val(respuesta);
				$('span.questions').removeClass('selected');
				$(this).addClass('selected');

			$.ajax({
        		type:'POST',    
        		url: '/nps/guardar-respuesta',
        		data:{
          			id_pregunta : id_pregunta,
          			respuesta : respuesta
        			}
      		});
			});

			$('input[type="radio"]').click(function(){
		//$('select.respuesta').change(function(){

				var id_pregunta = $(this).attr('class');
				var respuesta = $(this).val();

				$.ajax({
        	type:'POST',    
        	url: '/clima-organizacional/guardar-respuesta',
        	data:{
          	id_pregunta : id_pregunta,
          	respuesta : respuesta
        	}
      	});
			});

			$('body').on('click', 'button.delete_sociograma', function(){

				var id_pregunta = $(this).parent().prev().find('input[type="text"]').attr('data-question_id');
				var respuesta = $(this).parent().prev().find('input[type="hidden"]').val();
				var button = $(this);
							
				$.ajax({
        	type:'POST',    
        	url: '/sociograma/borrar-respuesta',
        	data:{
          	id_pregunta : id_pregunta,
          	respuesta : respuesta
        	},
        	success: function(data){

        		var temp = [];
						var counter = 0;
						button.parent().prev().find('input[type="hidden"]').val('');
						button.parent().prev().find('input[type="text"]').val('');
						button.parent().remove();

						for (i = 0;i < sociograma_questions_ids.length;i++){

    					if (sociograma_questions_ids[i] == id_pregunta){

    						j = i + 1;

    						for (k = 0;k < selected_values[j].length;k++){

    							if (selected_values[j][k] != respuesta){

    								temp[counter] = selected_values[j][k];
    								counter++;
    							}
    						}

    						selected_values[j] = temp;
    					}
    				}
        	}
      	});
			});

			$('.send_answers').click(function(){

				current_card = $(this).attr('data-card');
			});

			$('input.cl-white').click(function(){

				next_prev = true;
				$('li.cuestionarios-tab a[href="#seccion' + $(this).attr('data-to') + '"]').click();
				$('div.card.cuestionarios').get(0).scrollIntoView();

				if ($(this).hasClass('right')){

					$(this).parent().prev().find('.send_answers').click();
				}

				else{

					$(this).parent().next().find('.send_answers').click();
				}
			});

			$('.users_autocomplete').blur(function(){

				if ($(this).next().val() == ''){

					$(this).val('');
				}

				else{

					var selected = $(this).next().val();
					var names = $(this).val();

					if (users[1] !== undefined){

						for (i = 0;i < users[1].length;i++){

							if (users[1][i].data == selected){

								if (names != users[1][i].value){

									$(this).val(users[1][i].value);
								}
							}
						}
					}
				}
			});

			$('.users_autocomplete').keyup(function (e){

				if ($(this).parent().parent().find('button.delete_sociograma').length == 1){

					var code = e.keyCode || e.which;

					if (code != 9){

						$(this).parent().parent().find('button.delete_sociograma').click();
					}
				}
			});
    });

    function checkFrame(){

    	var result = $('iframe.my_frame').contents().find('body').html();

    	if (result == 'Success' || result == 'Danger'){

    		if (result == 'Success'){

    			$('li.cuestionarios-tab a[href="#seccion' + current_card + '"]').removeClass('fondo_amarillo');
    			$('li.cuestionarios-tab a[href="#seccion' + current_card + '"]').removeClass('fondo_verde');
    			$('li.cuestionarios-tab a[href="#seccion' + current_card + '"]').addClass('fondo_verde');
    		}

    		else{

    			$('li.cuestionarios-tab a[href="#seccion' + current_card + '"]').removeClass('fondo_rojo');
    			$('li.cuestionarios-tab a[href="#seccion' + current_card + '"]').removeClass('fondo_amarillo');
    			$('li.cuestionarios-tab a[href="#seccion' + current_card + '"]').addClass('fondo_amarillo');
    		}

    		if (!next_prev){

    			alert('Respuestas guardadas');
    		}

    		else{

    			next_prev = false;
    		}
    	}

    	else{

    		alert('Error inesperado... Envía tus respuestas de nuevo por favor');
    	}
    }
  </script>
@endsection