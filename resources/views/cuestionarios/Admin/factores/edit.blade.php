@extends('clima-organizacional.app')

@section('title', 'Dimensiones')

@section('content')
<div class="row mt-3">
	<div class="col-md-2 text-right">
		@include('clima-organizacional/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
<h3 class="titulos-evaluaciones mt-4 font-weight-bold">Dimensiones</h3>
<hr>
<div class="row">
	<div class="col-md-12 col-sm-9">
        <form method="post" action="{{ url('clima-organizacional/factores/' . $factor->id) }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="PUT">
			<div class="card">
				<div class="card-header">
					Editar Dimensión
				</div>
				<div class="card-body">	
					<div class="form-group">
						<label for="name" class="requerido">Nombre</label>
						<input type="text" class="form-control" placeholder="Nombre" name="name" required value="{{$factor->name}}">
					</div>
					<div class="form-group">
						<label for="description" class="requerido">Descripción</label>
						<textarea name="description" class="form-control" placeholder="Descripción" style="resize: none" cols="2" rows="2" required>{{$factor->description}}</textarea>
					</div>
				</div>

				<div class="card-footer">
					<div class="float-right">
						<button type="submit" class="btn btn-success">
					<i class="fas fa-check-circle"></i> Guardar Dimensión
				</button>
					</div>
				</div>

			</div> 
		</form>
	</div>
</div>
</div>
</div>

@endsection