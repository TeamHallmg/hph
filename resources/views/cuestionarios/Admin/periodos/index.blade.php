@extends('clima-organizacional.app')

@section('title', 'Periodos')

@section('content')

<div class="row mt-3">
	<div class="col-md-2 text-right">
		@include('clima-organizacional/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
<h3 class="titulos-evaluaciones mt-4 font-weight-bold">Periodos</h3>
<hr>
<a href="/clima-organizacional/periodos/create" class="btn btn-success"><i class="fas fa-plus-circle"></i> Agregar Periodo</a>

<div class="my-3">
	<table class="table table-striped" id="periodos">
		<thead class="bg-purple text-white">
			<tr>
				<th>Estado</th>
				<th>Nombre</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($periodos as $periodo)
				<tr>
					<td class="text-center">
						<h4>
						<span class="badge" style="background-color: {{ $periodo->status != 'Abierto' ? '#4B4B4D' : '#0090DA' }}; color: white">
							@if($periodo->status == 'Cerrado')
								<i class="fas fa-door-closed"></i>
							@elseif($periodo->status == 'Abierto')
								<i class="fas fa-door-open"></i>								
							@else
								<i class="fas fa-tools"></i>
							@endif
							{{$periodo->status}}
						</span>
						</h4>
					</td>
					<td>{{$periodo->name}}</td>
					<td class="text-center">
						<a href="{{ url('clima-organizacional/periodos/' . $periodo->id . '/edit') }}" class="btn btn-primary"><i class="fas fa-edit"></i> Editar </a> 
						@if ($periodo->estado == 'Cancelado')
							<a href="{{ url('clima-organizacional/periodos/' . $periodo->id) }}" class="btn btn-danger">Eliminar</a>	
						@endif	
					</td>
				</tr>	
			@endforeach
		</tbody>
	</table>
</div>
</div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">

	$(document).ready(function(){

		$('#periodos').DataTable({
			language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      }
   	});
  });
</script>

@endsection