@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
	@include('clima-organizacional/partials/sub-menu')
	</div>
	<div class="col-md-10">
    <img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">

    <h3 class="titulos-evaluaciones mt-4 font-weight-bold">REPORTES</h3>
    <hr>

    <div class="row justify-content-center">
        
        <div class="col-3">

            <div class="form-group text-center">
              <form id="changePlanForm" action="/clima-organizacional/reporte-grafico" method="post">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <label for="name" class="requerido">Periodo:</label>
                  <select name="period_id" class="period_id form-control" >
                           @for ($i = 0;$i < count($periodos);$i++)

                          <option value="{{$periodos[$i]->id}}" <?php if ($id_periodo == $periodos[$i]->id){ ?>selected="selected"<?php } ?>>{{$periodos[$i]->name}}</option>
                        @endfor

                  </select>   

                <input type="submit" style="display: none">
              </form>

            </div>
        </div>

    </div>
 
    <div class="row">
      <div class="col-md-12">
        <hr>
        <div class="font-weight-bold">
          Orden: <input type="checkbox" name="sort" class="sort" value="1" style="margin-left: 20px"> Progresivo
        </div>
        <div class="font-weight-bold">
          Mostrar: <input type="radio" name="filter" class="filter" value="1" style="margin-left: 20px"> Filtros <input type="radio" name="filter" class="filter" value="2" checked="checked" style="margin-left: 20px"> Dimensiones <input type="radio" name="filter" class="filter" value="3" style="margin-left: 20px"> Antigüedad <input type="radio" name="filter" class="filter" value="4" style="margin-left: 20px"> Edad <input type="radio" name="filter" class="filter" value="5" style="margin-left: 20px"> Sexo <input type="radio" name="filter" class="filter" value="6" style="margin-left: 20px"> Generaciones <input type="radio" name="filter" class="filter" value="7" style="margin-left: 20px"> Turnos <input type="radio" name="filter" class="filter" value="8" style="margin-left: 20px"> Categoría de Puesto <input type="radio" name="filter" class="filter" value="9" style="margin-left: 20px"> Unidad de Negocio <input type="radio" name="filter" class="filter" value="10" style="margin-left: 20px"> Clasificación del Colaborador
        </div>

        <div class="row mt-3">

        @if (!empty($factors))
          <div class="factores col-md-6">
            <div class="form-group">
                <label for="factores" class="font-weight-bold">Dimensiones:</label>
                <select class="factores form-control" id="factores">
                        <option value="0">-- Seleccione --</option>
                         <?php foreach ($factors as $key => $value){ ?>
                            <option value="{{$value->id}}">{{$value->name}}</option>
                        <?php } ?>   
                </select>   
            </div>
          </div>
        @endif

        @if (!empty($group_areas))
    
          <div class="grupos_areas col-md-6">
            <div class="form-group">
                <label for="grupos_areas" class="font-weight-bold">Grupo de Áreas:</label>
                <select class="grupos_areas form-control" id="grupos_areas">
                        <option value="0">-- Seleccione --</option>
                          <?php foreach ($group_areas as $key => $value){ ?>
                          <option value="{{$value->id}}">{{$value->name}}</option>
                           <?php } ?> 
                </select>   
            </div>
          </div>

        @endif
    
        @if (!empty($group_departments))
    
          <div class="grupos_departamentos col-md-6">
            <div class="form-group">
                <label for="grupos_departamentos" class="font-weight-bold">Grupos de Departamentos:</label>
                <select class="grupos_departamentos form-control" id="grupos_departamentos">
                        <option value="0">-- Seleccione --</option>
                          <?php foreach ($group_departments as $key => $value){ ?>
                          <option value="{{$value->id}}">{{$value->name}}</option>
                           <?php } ?> 
                </select>   
            </div>
          </div>

        @endif
    
        @if (!empty($group_job_positions))
    
          <div class="grupos_puestos col-md-6">
            <div class="form-group">
                <label for="grupos_puestos" class="font-weight-bold">Grupos de Puestos:</label>
                <select class="grupos_puestos form-control" id="grupos_puestos">
                        <option value="0">-- Seleccione --</option>
                          <?php foreach ($group_job_positions as $key => $value){ ?>
                          <option value="{{$value->id}}">{{$value->name}}</option>
                           <?php } ?> 
                </select>   
            </div>
          </div>

        @endif
    
        @if (!empty($directions))
    
          <div class="direcciones col-md-6">
            <div class="form-group">
                <label for="direcciones" class="font-weight-bold">Direcciones:</label>
                <select class="direcciones form-control" id="direcciones">
                        <option value="0">-- Seleccione --</option>
                          <?php foreach ($directions as $key => $value){ ?>
                          <option value="{{$value->id}}">{{$value->name}}</option>
                           <?php } ?> 
                </select>   
            </div>
          </div>

        @endif
    
        @if (!empty($departments))
    
          <div class="departamentos col-md-6">
            <div class="form-group">
                <label for="departamentos" class="font-weight-bold">Departamentos:</label>
                <select class="departamentos form-control" id="departamentos">
                        <option value="0">-- Seleccione --</option>
                          <?php foreach ($departments as $key => $value){ ?>
                          <option value="{{$value->id}}" data-direction="{{(!empty($value->direction_id) ? $value->direction_id : '')}}">{{$value->name}}</option>
                           <?php } ?> 
                </select>   
            </div>
          </div>  

        @endif
    
        </div>
        <h2 class="text-center">Gráfica del promedio de Clima</h2>
        <div class="row mt-3 mb-5">
          <div class="col-md-5"></div>
          <div class="col-md-2 text-center rounded-pill">
            <h1 class="average" style="margin-bottom: 0; color: white"></h1>
          </div>
          <div class="col-md-5"></div>
        </div>

        <div id="container" style="width: 1000px; height: 700px; margin: 0 auto"></div>
        <div class="row mt-4">
          <div class="col-md-6">
            <h4>Distribución del Personal</h4>
            <table class="table table-bordered table-striped sexo">
              <thead class="card-header">
                <tr>
                  <th class="anchura">Sexo</th>
                  <th>Totales</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Mujeres</td>
                  <td class="mujeres"></td>
                </tr>
                <tr>
                  <td>Hombres</td>
                  <td class="hombres"></td>
                </tr>
                <tr>
                  <td>Total</td>
                  <td class="total_sexo"></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="col-md-6">
            <h4>Tabla de Antigüedad</h4>
            <table class="table table-bordered table-striped sexo">
              <thead class="card-header">
                <tr>
                  <th class="anchura">Antigüedad</th>
                  <th>Personas</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Un año o menos</td>
                  <td class="duracion1"></td>
                </tr>
                <tr>
                  <td>Un año un día a cinco años</td>
                  <td class="duracion2"></td>
                </tr>
                <tr>
                  <td>Cinco años un día a diez años</td>
                  <td class="duracion3"></td>
                </tr>
                <tr>
                  <td>Diez años un día y más</td>
                  <td class="duracion4"></td>
                </tr>
                <tr>
                  <td class="text-right">Total</td>
                  <td class="total_duracion"></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="row mt-4">
          <div class="col-md-6">
            <h4>Personal por Generación</h4>
            <table class="table table-bordered table-striped sexo">
              <thead class="card-header">
                <tr>
                  <th class="anchura">Nacimiento</th>
                  <th>Generación</th>
                  <th>Totales</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Antes de 1965</td>
                  <td>Baby Boomers</td>
                  <td class="baby"></td>
                </tr>
                <tr>
                  <td>1965 a 1979</td>
                  <td>Generación X</td>
                  <td class="gx"></td>
                </tr>
                <tr>
                  <td>1980 a 1999</td>
                  <td>Millenials</td>
                  <td class="millenials"></td>
                </tr>
                <tr>
                  <td>De 2000 en adelante</td>
                  <td>Generación Z</td>
                  <td class="gz"></td>
                </tr>
                <tr>
                  <td colspan="2" class="text-right">Totales</td>
                  <td class="total_generacion"></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="col-md-6">
            <h4>Turnos</h4>
            <table class="table table-bordered table-striped turnos">
              <thead class="card-header">
                <tr>
                  <th class="anchura">Turno</th>
                  <th>Totales</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
        {{-- <div class="exportar_resultados">
          <a href="javascript:;" class="btn btn-primary">Exportar Preguntas Abiertas</a>
        </div>
 --}}

        <div class="exportar_resultados mt-4">
          <form action="/clima-organizacional/exportar-resultados-clima" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="period_id" value="{{$id_periodo}}">
            <input type="submit" value="Exportar Respuestas" class="btn btn-primary">
          </form>
        </div>
        <div class="factores_table mt-4" style="overflow: auto">

        <table class="table table-striped table-bordered preguntas">
            <thead class="bg-purple text-white">
              <tr>
                <th>Pregunta</th>

        @foreach ($etiquetas as $etiqueta)

                <th>{{$etiqueta->valor}}</th>
        @endforeach

                <th>Promedio</th>
              </tr>
            </thead>
            <tbody>
    
            </tbody>
          </table>
        </div>
        <!--<div id="sliders">
          <table>
             <tr>
               <td>Girar en el Eje X</td>
               <td>
                 <input id="R0" type="range" min="0" max="45" value="0"/> <span id="R0-value" class="value"></span>
               </td>
             </tr>
             <tr>
               <td>Girar en el Eje Y</td>
               <td>
                 <input id="R1" type="range" min="0" max="45" value="0"/> <span id="R1-value" class="value"></span>
               </td>
             </tr>
          </table>
        </div>-->
      </div>
    </div>
  </div>
</div>

@endsection
@section('scripts')

<script type="text/javascript">

	var answers = <?php echo json_encode($answers)?>;
  var factors = <?php echo json_encode($factors)?>;
  var factors_names = <?php echo json_encode($factores)?>;
  var area_pivots = <?php echo json_encode($area_pivots)?>;
  var department_pivots = <?php echo json_encode($department_pivots)?>;
  var job_position_pivots = <?php echo json_encode($job_position_pivots)?>;
  var directions = <?php echo json_encode($directions)?>;
  var areas = <?php echo json_encode($areas)?>;
  var departments = <?php echo json_encode($departments)?>;
  var job_positions = <?php echo json_encode($job_positions)?>;
  var etiquetas = <?php echo json_encode($etiquetas)?>;
  var id_periodo = <?php echo $id_periodo?>;
	/*var factores = <?php //echo json_encode($factores)?>;
	var respuestas = <?php //echo json_encode($valores)?>;
	var factores_ordenados = <?php //echo json_encode($factores_ordenados)?>;
	var respuestas_ordenadas = <?php //echo json_encode($valores_ordenados)?>;*/
	var highchart = 0;
	var series = 0;
	var xAxis = 0;
  var yAxis = 0;
  var title = 0;
  var plotOptions = 0;
	var actual_orden = 2;
  var actual_filtro = 2;
  var direccion = 0;
  var departamento = 0;
  var area = 0;
  var puesto = 0;
  var grupo_departamento = 0;
  var grupo_area = 0;
  var grupo_puesto = 0;
  var chart = 0;
  var table = 0;
  var factor = 0;
  var fecha_actual = new Date();
  var month1 = 0, month2 = fecha_actual.getMonth() + 1, day1 = 0, day2 = fecha_actual.getDate(), year1 = 0, year2 = fecha_actual.getFullYear(), diff = 0;

	$(document).ready(function(){

   	chart = {
      renderTo: 'container',
      backgroundColor: 'rgb(248, 250, 252)',
      type: 'column',
      margin: 170,
      marginTop: 20,
      marginLeft: 50,
      marginRight: 50,
      options3d: {
        enabled: true,
        alpha: 0,
        beta: 0,
        depth: 50,
        viewDistance: 25
      }
   	};

   	title = {
      text: ''   
   	};

    plotOptions = {
        column: {
          depth: 25
        }
    };

    plotOptions.column.zones = [];

    if (etiquetas.length > 0){

      for (i = etiquetas.length - 1;i >= 0;i--){

        plotOptions.column.zones.push({value: etiquetas[i].valor * 1 + 1, color: etiquetas[i].color});
      }
    }

    else{

      plotOptions.column.zones.push({value: 1, color: '#C00000'});
      plotOptions.column.zones.push({value: 2, color: '#FF4343'});
      plotOptions.column.zones.push({value: 3, color: '#FFFF00'});
      plotOptions.column.zones.push({value: 4, color: '#73CB77'});
    }
   
   	/*plotOptions = {
      column: {
        depth: 25,
        zones: [
        {
          value: 3,
          color: 'rgb(255,116,116)'
        },
        {
          value: 4,
          color: 'rgb(253,236,109)'
        },
        {
          color: 'rgb(144,237,125)'
        }
       	]
      },
      showInLegend: true
   	};*/

    yAxis = [{
    labels: {
                style: {
                    fontSize:'20px'
                }
            }
  }];

    mostrar_grafica();

   	// Activate the sliders
   	$('#R0').on('change', function(){

      highchart.options.chart.options3d.alpha = this.value;
      showValues();
      highchart.redraw(false);
   	});

   	$('#R1').on('change', function(){

      highchart.options.chart.options3d.beta = this.value;
      showValues();
      highchart.redraw(false);
   	});
		
	  $('input[type="checkbox"].sort').click(function(){
		
		  if ($(this).prop('checked') == true){

        actual_orden = $(this).val();
      }

      else{

        actual_orden = 2;
      }

      mostrar_grafica();
    });
		
	  $('input[type="radio"].filter').click(function(){
    
      actual_filtro = $(this).val();
      mostrar_grafica();
    });

    $('select.factores').change(function(){

      factor = $(this).val();
      mostrar_grafica();
    });

    $('select.grupos_departamentos').change(function(){

      $('select.puestos, select.departamentos, select.areas, select.direcciones, select.grupos_areas, select.grupos_puestos').val(0);
      puesto = departamento = area = direccion = grupo_area = grupo_puesto = 0;
      grupo_departamento = $(this).val();
      var department_id = 0;
      var i = 0;
      var j = 0;
      var k = 0;
      $('select.puestos option, select.departamentos option, select.areas option, select.direcciones option').hide();
      $('select.departamentos option:first, select.puestos option:first, select.areas option:first, select.direcciones option:first').show();

      if (grupo_departamento != 0){

        for (i = 0;i < department_pivots.length;i++){

          if (department_pivots[i].group_department_id == grupo_departamento){

            department_id = department_pivots[i].department_id;
            $('select.departamentos option[value="' + department_id + '"]').show();

            for (j = 0;j < departments.length;j++){

              if (departments[j].id == department_id){

                $('select.direcciones option[value="' + departments[j].direction_id + '"]').show();

                for (k = 0;k < directions.length;k++){

                  if (directions[k].id == departments[j].direction_id){

                    $('select.direcciones option[value="' + departments[k].direction_id + '"]').show();
                  }
                }
              }
            }

            //$('select.areas option[data-department="' + department_id + '"]').show();
          }
        }
      }

      else{

        $('select.departamentos option, select.puestos option, select.areas option, select.direcciones option').show();
      }

      mostrar_grafica();
    });

    $('select.grupos_areas').change(function(){

      $('select.puestos, select.departamentos, select.areas, select.direcciones, select.grupos_departamentos, select.grupos_puestos').val(0);
      puesto = departamento = area = direccion = grupo_departamento = grupo_puesto = 0;
      grupo_area = $(this).val();
      var area_id = 0;
      var i = 0;
      var j = 0;
      var puestos = [];
      var puesto_actual = '';
      $('select.puestos option, select.departamentos option, select.areas option, select.direcciones option').hide();
      $('select.departamentos option:first, select.puestos option:first, select.areas option:first, select.direcciones option:first').show();

      if (grupo_area != 0){

        for (i = 0;i < area_pivots.length;i++){

          if (area_pivots[i].group_area_id == grupo_area){

            area_id = area_pivots[i].area_id;
            $('select.departamentos option[data-direction="' + area_id + '"]').show();
            $('select.direcciones option[value="' + area_id + '"]').show();

            /*for (j = 0;j < areas.length;j++){

              if (areas[j].id == area_id){

                $('select.departamentos option[value="' + areas[j].department_id + '"]').show();
              }
            }*/

            /*$('select.puestos option[data-area="' + area_id + '"]').each(function(){

              puesto_actual = $(this).text();

              if (!puestos.includes(puesto_actual)){

                $(this).show();
                puestos.push(puesto_actual);
              }
            });*/

            /*for (j = 0;j < job_positions.length;j++){

              if (job_positions[j].area_id == area_id){

                $('select.puestos option[data-area="' + areas[j].id + '"]').show();
              }
            }*/
          }
        }
      }

      else{

        $('select.departamentos option, select.puestos option, select.areas option, select.direcciones option').show();
      }

      mostrar_grafica();
    });

    $('select.grupos_puestos').change(function(){

      $('select.puestos, select.departamentos, select.areas, select.direcciones, select.grupos_areas, select.grupos_departamentos').val(0);
      puesto = departamento = area = direccion = grupo_area = grupo_departamento = 0;
      grupo_puesto = $(this).val();
      var job_position_id = 0;
      var i = 0;
      var j = 0;
      var k = 0;
      var l = 0;
      var puestos = [];
      var puesto_actual = '';
      $('select.puestos option, select.departamentos option, select.areas option, select.direcciones option').hide();
      $('select.departamentos option:first, select.puestos option:first, select.areas option:first, select.direcciones option:first').show();

      if (grupo_puesto != 0){

        for (i = 0;i < job_position_pivots.length;i++){

          if (job_position_pivots[i].group_job_position_id == grupo_puesto){

            job_position_id = job_position_pivots[i].job_position_id;
            puesto_actual = $('select.puestos option[value="' + job_position_id + '"]').text();

            if (!puestos.includes(puesto_actual)){

              $('select.puestos option[value="' + job_position_id + '"]').show();
              puestos.push(puesto_actual);
            }

            for (j = 0;j < job_positions.length;j++){

              if (job_positions[j].id == job_position_id){

                //$('select.areas option[value="' + job_positions[j].area_id + '"]').show();

                for (k = 0;k < areas.length;k++){

                  if (areas[k].id == job_positions[j].area_id){

                    $('select.departamentos option[value="' + areas[k].department_id + '"]').show();

                    for (l = 0;l < departments.length;l++){

                      if (departments[l].id == areas[k].department_id){

                        $('select.direcciones option[value="' + departments[l].direction_id + '"]').show();
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }

      else{

        $('select.departamentos option, select.puestos option, select.areas option, select.direcciones option').show();
      }

      mostrar_grafica();
    });

    $('select.direcciones').change(function(){

      $('select.puestos, select.departamentos, select.areas').val(0);
      puesto = departamento = area = 0;
      direccion = $(this).val();
      var i = 0;
      var j = 0;
      var puestos = [];
      var puesto_actual = '';
      $('select.puestos option, select.departamentos option, select.areas option').hide();
      $('select.departamentos option:first, select.puestos option:first, select.areas option:first').show();

      if (direccion != 0){

        $('select.departamentos option[data-direction="' + direccion + '"]').show();

        for (i = 0;i < departments.length;i++){

          if (departments[i].direction_id == direccion){

            //$('select.areas option[data-department="' + departments[i].id + '"]').show();

            for (j = 0;j < areas.length;j++){

              if (areas[j].department_id == departments[i].id){

                $('select.puestos option[data-area="' + areas[j].id + '"]').each(function(){

                  puesto_actual = $(this).text();

                  if (!puestos.includes(puesto_actual)){

                    $(this).show();
                    puestos.push(puesto_actual);
                  }
                });
              }
            }
          }
        }
      }

      else{

        $('select.departamentos option, select.puestos option, select.areas option').show();
      }

      mostrar_grafica();
    });

    $('select.areas').change(function(){

      $('select.puestos').val(0);
      puesto = 0;
      area = $(this).val();
      var i = 0;
      $('select.puestos option').hide();
      $('select.puestos option:first').show();

      if (area != 0){

        $('select.puestos option[data-area="' + area + '"]').show();
      }

      else{

        $('select.puestos option').show();
      }

      mostrar_grafica();
    });

    $('select.departamentos').change(function(){

      $('select.puestos, select.areas').val(0);
      puesto = area =  0;
      departamento = $(this).val();
      var i = 0;
      var puestos = [];
      var puesto_actual = '';
      $('select.puestos option, select.areas option').hide();
      $('select.puestos option:first, select.areas option:first').show();

      if (departamento != 0){

        //$('select.areas option[data-department="' + departamento + '"]').show();

        for (i = 0;i < areas.length;i++){

          if (areas[i].department_id == departamento){

            $('select.puestos option[data-area="' + areas[i].id + '"]').each(function(){

              puesto_actual = $(this).text();

              if (!puestos.includes(puesto_actual)){

                $(this).show();
                puestos.push(puesto_actual);
              }
            });
          }
        } 
      }

      else{

        $('select.puestos option, select.areas option').show();
      }

      mostrar_grafica();
    });

    $('select.puestos').change(function(){

      puesto = $(this).val();
      //departamento = 0;
      mostrar_grafica();
    });

    $('select.period_id').change(function(){

      $(this).parent().submit();
    });

    table = $('.preguntas').DataTable({
      language: {
        "sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
            },
             "aaSorting": []
    });
  });

  function mostrar_grafica(){

    var factores = [];
    var valores = [];
    var factores_ordenados = [];
    var valores_ordenados = [];
    var valor_actual = 0;
    var categorias = [];
    var sumatoria = [];

    //if (actual_filtro == 'factores'){

      var factor_actual = 0;
      var pregunta_actual = 0;
      var pregunta = '';
      var contador_preguntas = 0;
      var total_10 = 0;
      var total_9 = 0;
      var total_8 = 0;
      var total_7 = 0;
      var total_6 = 0;
      var total_5 = 0;
      var total_4 = 0;
      var total_3 = 0;
      var total_2 = 0;
      var total_1 = 0;
      var total_0 = 0;
      var promedio_pregunta = 0;
      var valor = 0;
      var total = 0;
      var contador_respuestas = 0;
      var temp_factores = [];
      var temp_valores = [];
      var sexo = [];
      var edad = [];
      var generaciones = [];
      var turnos = [];
      var contador_turnos = [];
      var tipos_puestos = [];
      var unidades_negocio = [];
      var clasificacion = [];
      var antiguedad = [];
      var filters = [];
      var year = 0;
      var current_year = new Date();
      var years = 0;
      current_year = current_year.getFullYear() * 1;
      //sexo["F"] = {nombre: 'Mujeres', suma: 0, contador: 0};
      //sexo['M'] = {nombre: 'Hombres', suma: 0, contador: 0};
      var band = false;
      var counter = 0;
      var sex_title = 0;
      var title = 0;
      var k = 0, i = 0, diff = 0, diffdays = 0, diffmonths = 0;
      var babygoomer = [];
      var generacionX = [];
      var milennials = [];
      var generacionZ = [];
      var duracion = [], duracion2 = [], duracion3 = [], duracion4 = [];
      var mujeres = [], hombres = [], participantes_revisados = [];
      var answers_table = '';

      if (table != 0){

        table.destroy();
      }

      $('div.factores_table table tbody').html('');

      for (i = 0;i < answers.length;i++){

        if (pregunta_actual != answers[i].question_id){

          if (pregunta_actual != 0 && (factor == 0 || factor_actual == factor)){

            promedio_pregunta = (total_1 + total_2 * 2 + total_3 * 3 + total_4 * 4 + total_5 * 5) / contador_preguntas;
            total_10 = (total_10 * 100 / contador_preguntas).toFixed(1) + '%';
            total_9 = (total_9 * 100 / contador_preguntas).toFixed(1) + '%';
            total_8 = (total_8 * 100 / contador_preguntas).toFixed(1) + '%';
            total_7 = (total_7 * 100 / contador_preguntas).toFixed(1) + '%';
            total_6 = (total_6 * 100 / contador_preguntas).toFixed(1) + '%';
            total_5 = (total_5 * 100 / contador_preguntas).toFixed(1) + '%';
            total_4 = (total_4 * 100 / contador_preguntas).toFixed(1) + '%';
            total_3 = (total_3 * 100 / contador_preguntas).toFixed(1) + '%';
            total_2 = (total_2 * 100 / contador_preguntas).toFixed(1) + '%';
            total_1 = (total_1 * 100 / contador_preguntas).toFixed(1) + '%';
            total_0 = (total_0 * 100 / contador_preguntas).toFixed(1) + '%';
            answers_table = '<tr><td>' + pregunta + '</td>';
            if (total_10 != '0.0%'){
              answers_table += '<td>' + total_10 + '</td>';
            }
            if (total_9 != '0.0%'){
              answers_table += '<td>' + total_9 + '</td>';
            }
            if (total_8 != '0.0%'){
              answers_table += '<td>' + total_8 + '</td>';
            }
            if (total_7 != '0.0%'){
              answers_table += '<td>' + total_7 + '</td>';
            }
            if (total_6 != '0.0%'){
              answers_table += '<td>' + total_6 + '</td>';
            }
            answers_table += '<td>' + total_5 + '</td>';
            answers_table += '<td>' + total_4 + '</td>';
            answers_table += '<td>' + total_3 + '</td>';
            answers_table += '<td>' + total_2 + '</td>';
            answers_table += '<td>' + total_1 + '</td>';
            answers_table += '<td>' + promedio_pregunta.toFixed(1) + '</td>';
            if (total_0 != '0.0%'){
              answers_table += '<td>' + total_0 + '</td>';
            }
            answers_table += '</tr>';
            $('div.factores_table table tbody').append(answers_table);
          }

          pregunta_actual = answers[i].question_id;
          total_10 = total_9 = total_8 = total_7 = total_6 = total_5 = total_4 = total_3 = total_2 = total_1 = total_0 = 0;
          contador_preguntas = 0;
          pregunta = answers[i].question;
        }

        if (factor_actual != answers[i].factor_id){

          if (factor_actual != 0){

            if (contador_respuestas > 0){

              valor = (total / contador_respuestas).toFixed(1) * 1;
            }

            else{

              valor = 0;
            }

            band = false;
            temp_factores = [];
            temp_valores = [];

            for (var j = 0;j < valores_ordenados.length;j++){
                
              if (valores_ordenados[j] < valor && !band){

                temp_valores.push(valor);
                temp_factores.push(factors_names[factor_actual]);
                band = true;
              }

              temp_valores.push(valores_ordenados[j]);
              temp_factores.push(factores_ordenados[j]);
            }

            if (!band){

              temp_valores.push(valor);
              temp_factores.push(factors_names[factor_actual]);
            }

            factores_ordenados = temp_factores;
            valores_ordenados = temp_valores;
            //factores.push(factor_actual);
            factores.push(factors[counter].name);
            valores.push(valor);
            counter++;
          }

          total = 0;
          contador_respuestas = 0;
          factor_actual = answers[i].factor_id;
          valores_tabla = '';
        }

        if ((factor == 0 || (answers[i].factor_id != 0 && answers[i].factor_id == factor))
          && (grupo_area == 0 || (answers[i].group_area_id !== undefined && answers[i].group_area_id == grupo_area))
          && (grupo_departamento == 0 || (answers[i].group_department_id !== undefined && answers[i].group_department_id == grupo_departamento))
          && (grupo_puesto == 0 || (answers[i].group_job_position_id !== undefined && answers[i].group_job_position_id == grupo_puesto))
          && (direccion == 0 || (answers[i].direction_id !== undefined && answers[i].direction_id == direccion)) 
          && (departamento == 0 || (answers[i].department_id !== undefined && answers[i].department_id == departamento))
          && (area == 0 || (answers[i].area_id !== undefined && answers[i].area_id == area)) 
          && (puesto == 0 || (answers[i].job_position_id !== undefined && answers[i].job_position_id == puesto))){

          if (!participantes_revisados.includes(answers[i].id)){

            if (answers[i].sexo == 'M'){

              hombres.push(answers[i].id);
            }

            else{

              mujeres.push(answers[i].id);
            }

            day1 = answers[i].ingreso.substr(8, 2) * 1;
            month1 = answers[i].ingreso.substr(5, 2) * 1;
            year1 = answers[i].ingreso.substr(0, 4) * 1;

            diff = diffmonths = diffdays = 0;
            diff = year2 - year1;

            if (month1 > month2){

              diff--;
            }

            else{

              diffmonths = month2 - month1;

              if (month1 == month2){

                if (day1 > day2){

                  diff--;
                }

                else{

                  diffdays = day2 - day1;
                }
              }
            }

            if (diff > 10 || (diff == 10 && (diffmonths > 0 || diffdays > 0))){

              duracion4.push(answers[i].id);
            }

            else{

              if ((diff > 5 && diff < 10) || (diff == 5 && (diffdays > 0 || diffmonths > 0)) || (diff == 10 && diffdays == 0 && diffmonths == 0)){

                duracion3.push(answers[i].id);
              }

              else{

                if ((diff > 1 && diff < 5) || (diff == 1 && (diffdays > 0 || diffmonths > 0)) || (diff == 5 && diffdays == 0 && diffmonths == 0)){

                  duracion2.push(answers[i].id);
                }

                else{

                  duracion.push(answers[i].id);
                } 
              }
            }

            year1 = answers[i].nacimiento.substr(0, 4) * 1;

            if (year1 < 1965){

              babygoomer.push(answers[i].id);
            }

            else{

              if (year1 >= 1965 && year1 <= 1979){

                generacionX.push(answers[i].id);
              }

              else{

                if (year1 >= 1980 && year1 <= 1999){

                  milennials.push(answers[i].id);
                }

                else{

                  generacionZ.push(answers[i].id);
                }
              }              
            }

            title = answers[i].turno;

            if (contador_turnos[title] === undefined){

              contador_turnos[title] = {title: title, contador: 0};
            }

            contador_turnos[title].contador++;
            participantes_revisados.push(answers[i].id);
          }

          if (actual_filtro == 1){

            if (puesto != 0){

              if (answers[i].job_position_id !== undefined && answers[i].job_position_id == puesto){

                for (k = 0;k < job_positions.length;k++){

                  title = puesto;

                  if (job_positions[k].id == puesto){

                    if (filters[puesto] === undefined){

                      filters[puesto] = {title: job_positions[k].name, suma: 0, contador: 0};
                    }

                    break;
                  }
                }
              }
            }

            else{

              if (departamento != 0){

                if (answers[i].job_position_id !== undefined){

                  for (k = 0;k < job_positions.length;k++){

                    if (job_positions[k].id == answers[i].job_position_id){

                      title = answers[i].job_position_id;

                      if (filters[title] === undefined){

                        filters[title] = {title: job_positions[k].name, suma: 0, contador: 0};
                      }

                      break;
                    }
                  }
                }
              }

              else{

                if (departamento != 0){

                  if (answers[i].area_id !== undefined){

                    for (k = 0;k < areas.length;k++){

                      if (areas[k].id == answers[i].area_id){

                        title = answers[i].area_id;

                        if (filters[title] === undefined){

                          filters[title] = {title: areas[k].name, suma: 0, contador: 0};
                        }

                        break;
                      }
                    }
                  }
                }

                else{

                  if (direccion != 0){

                    if (answers[i].department_id !== undefined){

                      for (k = 0;k < departments.length;k++){

                        if (departments[k].id == answers[i].department_id){

                          title = answers[i].department_id;

                          if (filters[title] === undefined){

                            filters[title] = {title: departments[k].name, suma: 0, contador: 0};
                          }

                          break;
                        }
                      }
                    }
                  }

                  else{

                    if (answers[i].direction_id !== undefined){

                      for (k = 0;k < directions.length;k++){

                        if (directions[k].id == answers[i].direction_id){

                          title = answers[i].direction_id;

                          if (filters[title] === undefined){

                            filters[title] = {title: directions[k].name, suma: 0, contador: 0};
                          }

                          break;
                        }
                      }
                    }
                  }
                }
              }
            }
          }

          else{

            if (actual_filtro == 5){

              if (answers[i].sexo == 'F'){

                sex_title = 1;
                title = 'Mujeres';
              }

              else{

                sex_title = 2;
                title = 'Hombres';
              }

              if (sexo[sex_title] === undefined){

                sexo[sex_title] = {title: title, suma: 0, contador: 0}
              }
            }

            else{

              if (actual_filtro == 4){

                year = answers[i].nacimiento;
                year = year.substr(0, 4) * 1;
                years = current_year - year;

                if (edad[years] === undefined){

                  edad[years] = {anios: years, suma: 0, contador: 0};
                }
              }

              else{

                if (actual_filtro == 6){

                  year = answers[i].nacimiento.substr(0, 4) * 1;

                  if (year < 1965){

                    title = 'Baby Boomers';
                  }

                  else{

                    if (year >= 1965 && year <= 1979){

                      title = 'Generación X';
                    }

                    else{

                      if (year >= 1980 && year <= 1999){

                        title = 'Millenials';
                      }

                      else{

                        title = 'Generación Z';
                      }
                    }              
                  }

                  if (generaciones[title] === undefined){

                    generaciones[title] = {title: title, suma: 0, contador: 0}
                  }
                }

                else{

                  if (actual_filtro == 7){

                    title = answers[i].turno;

                    if (turnos[title] === undefined){

                      turnos[title] = {title: title, suma: 0, contador: 0};
                    }
                  }

                  else{

                    if (actual_filtro == 8){

                      title = answers[i].grado;

                      if (tipos_puestos[title] === undefined){

                        tipos_puestos[title] = {title: title, suma: 0, contador: 0};
                      }
                    }

                    else{

                      if (actual_filtro == 9){

                        title = answers[i].division;

                        if (unidades_negocio[title] === undefined){

                          unidades_negocio[title] = {title: title, suma: 0, contador: 0};
                        }
                      }

                      else{

                        if (actual_filtro == 10){

                          title = answers[i].relacion;

                          if (clasificacion[title] === undefined){

                            clasificacion[title] = {title: title, suma: 0, contador: 0};
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }

          total += answers[i].answer * 1;
          valor_actual = answers[i].answer * 1;

          switch (answers[i].answer){

            case '10':
          
              total_10++;
              break;

            case '9':
          
              total_9++;
              break;

            case '8':
          
              total_8++;
              break;

            case '7':
          
              total_7++;
              break;

            case '6':
          
              total_6++;
              break;

            case '5':
          
              total_5++;
              break;

            case '4':
          
              total_4++;
              break;

            case '3':
          
              total_3++;
              break;

            case '2':
          
              total_2++;
              break;

            case '1':
          
              total_1++;
              break;

            case '0':
          
              total_0++;
          }

          contador_respuestas++;
          contador_preguntas++;

          if (actual_filtro == 5){

            sexo[sex_title].contador = sexo[sex_title].contador + 1;
            sexo[sex_title].suma = sexo[sex_title].suma + valor_actual;
          }

          else{

            if (actual_filtro == 4){
          
              edad[years].contador = edad[years].contador + 1;
              edad[years].suma = edad[years].suma + valor_actual;
            }

            else{

              if (actual_filtro == 3){
            
                year = answers[i].ingreso;
                year = year.substr(0, 4) * 1;
                years = current_year - year;

                if (antiguedad[years] === undefined){

                  antiguedad[years] = {anios: years, suma: 0, contador: 0};
                }

                antiguedad[years].contador = antiguedad[years].contador + 1;
                antiguedad[years].suma = antiguedad[years].suma + valor_actual;
              }

              else{

                if (actual_filtro == 1){

                  filters[title].contador = filters[title].contador + 1;
                  filters[title].suma = filters[title].suma + valor_actual;
                }

                else{

                  if (actual_filtro == 6){

                    generaciones[title].contador = generaciones[title].contador + 1;
                    generaciones[title].suma = generaciones[title].suma + valor_actual;
                  }

                  else{

                    if (actual_filtro == 7){

                      turnos[title].contador = turnos[title].contador + 1;
                      turnos[title].suma = turnos[title].suma + valor_actual;
                    }

                    else{

                      if (actual_filtro == 8){

                        tipos_puestos[title].contador = tipos_puestos[title].contador + 1;
                        tipos_puestos[title].suma = tipos_puestos[title].suma + valor_actual;
                      }

                      else{

                        if (actual_filtro == 9){

                          unidades_negocio[title].contador = unidades_negocio[title].contador + 1;
                          unidades_negocio[title].suma = unidades_negocio[title].suma + valor_actual;
                        }

                        else{

                          if (actual_filtro == 10){

                            clasificacion[title].contador = clasificacion[title].contador + 1;
                            clasificacion[title].suma = clasificacion[title].suma + valor_actual;
                          } 
                        } 
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }

      $('td.mujeres').text(mujeres.length);
      $('td.hombres').text(hombres.length);
      $('td.total_sexo').text(mujeres.length + hombres.length);
      $('td.duracion1').text(duracion.length);
      $('td.duracion2').text(duracion2.length);
      $('td.duracion3').text(duracion3.length);
      $('td.duracion4').text(duracion4.length);
      $('td.total_duracion').text(duracion.length + duracion2.length + duracion3.length + duracion4.length);
      $('td.baby').text(babygoomer.length);
      $('td.gx').text(generacionX.length);
      $('td.millenials').text(milennials.length);
      $('td.gz').text(generacionZ.length);
      $('td.total_generacion').text(babygoomer.length + generacionZ.length + milennials.length + generacionX.length);
      $('table.turnos tbody').html('');
      var turns_counter = 0;

      Object.values(contador_turnos).forEach(function(value){

        $('table.turnos tbody').append('<tr><td>' + value.title + '</td><td>' + value.contador + '</td></tr>');
        turns_counter += value.contador;
      });

      $('table.turnos tbody').append('<tr><td>Total</td><td>' + turns_counter + '</td></tr>');

      if (pregunta_actual != 0 && (factor == 0 || factor_actual == factor)){

        promedio_pregunta = (total_1 + total_2 * 2 + total_3 * 3 + total_4 * 4 + total_5 * 5) / contador_preguntas;
        total_10 = (total_10 * 100 / contador_preguntas).toFixed(1) + '%';
        total_9 = (total_9 * 100 / contador_preguntas).toFixed(1) + '%';
        total_8 = (total_8 * 100 / contador_preguntas).toFixed(1) + '%';
        total_7 = (total_7 * 100 / contador_preguntas).toFixed(1) + '%';
        total_6 = (total_6 * 100 / contador_preguntas).toFixed(1) + '%';
        total_5 = (total_5 * 100 / contador_preguntas).toFixed(1) + '%';
        total_4 = (total_4 * 100 / contador_preguntas).toFixed(1) + '%';
        total_3 = (total_3 * 100 / contador_preguntas).toFixed(1) + '%';
        total_2 = (total_2 * 100 / contador_preguntas).toFixed(1) + '%';
        total_1 = (total_1 * 100 / contador_preguntas).toFixed(1) + '%';
        total_0 = (total_0 * 100 / contador_preguntas).toFixed(1) + '%';
        answers_table = '<tr><td>' + pregunta + '</td>';
        if (total_10 != '0.0%'){
          answers_table += '<td>' + total_10 + '</td>';
        }
        if (total_9 != '0.0%'){
          answers_table += '<td>' + total_9 + '</td>';
        }
        if (total_8 != '0.0%'){
          answers_table += '<td>' + total_8 + '</td>';
        }
        if (total_7 != '0.0%'){
          answers_table += '<td>' + total_7 + '</td>';
        }
        if (total_6 != '0.0%'){
          answers_table += '<td>' + total_6 + '</td>';
        }
        answers_table += '<td>' + total_5 + '</td>';
        answers_table += '<td>' + total_4 + '</td>';
        answers_table += '<td>' + total_3 + '</td>';
        answers_table += '<td>' + total_2 + '</td>';
        answers_table += '<td>' + total_1 + '</td>';
        answers_table += '<td>' + promedio_pregunta.toFixed(1) + '</td>';
        if (total_0 != '0.0%'){
          answers_table += '<td>' + total_0 + '</td>';
        }
        answers_table += '</tr>';
        $('div.factores_table table tbody').append(answers_table);
      }

      if (factor_actual != 0){

        if (contador_respuestas > 0){

          valor = (total / contador_respuestas).toFixed(1) * 1;
        }

        else{

          valor = 0;
        }

        band = false;
        temp_factores = [];
        temp_valores = [];

        for (var j = 0;j < valores_ordenados.length;j++){
                
          if (valores_ordenados[j] < valor && !band){

            temp_valores.push(valor);
            temp_factores.push(factors_names[factor_actual]);
            band = true;
          }

          temp_valores.push(valores_ordenados[j]);
          temp_factores.push(factores_ordenados[j]);
        }

        if (!band){

          temp_valores.push(valor);
          temp_factores.push(factors_names[factor_actual]);
        }

        factores_ordenados = temp_factores;
        valores_ordenados = temp_valores;
        //factores.push(factor_actual);
        factores.push(factors[counter].name);
        valores.push(valor);
        counter++;
      }

      $('div.factores_table, div.exportar_resultados').show();

      $('div.exportar_resultados a').attr('href', '/clima-organizacional/exportar-resultados/' + id_periodo + '/' + grupo_departamento + '/' + grupo_area + '/' + grupo_puesto + '/' + direccion + '/' + departamento + '/' + area + '/' + puesto);

      /*if (departamento == 0 && puesto == 0){

        $('div.exportar_resultados a').attr('href', '/clima-organizacional/exportar-resultados/' + id_periodo + '/0/0');
      }

      else{

        if (departamento != 0 && puesto != 0){

          $('div.exportar_resultados a').attr('href', '/clima-organizacional/exportar-resultados/' + id_periodo + '/' + departamento + '/' + puesto);
        }

        else{

          if (departamento != 0){

            $('div.exportar_resultados a').attr('href', '/clima-organizacional/exportar-resultados/' + id_periodo + '/' + departamento + '/0');
          }

          else{

            $('div.exportar_resultados a').attr('href', '/clima-organizacional/exportar-resultados/' + id_periodo + '/0/' + puesto);
          }
        }
      }*/
    /*}

    else{

      factores_ordenados = resultados_antiguedad.filtros_ordenados;
      factores = resultados_antiguedad.filtros;
      valores_ordenados = resultados_antiguedad.valores_ordenados;
      valores = resultados_antiguedad.valores;
      $('div.factores_table, div.exportar_resultados').hide();
    }*/

    var datos = 0;
    var datos2 = 0;
    var temp1 = [];
    var temp2 = [];

    if (actual_filtro == 2){

      if (actual_orden == 2){

        datos = valores;
        datos2 = factores;
      }

      else{

        datos = valores_ordenados;
        datos2 = factores_ordenados;
      }
    }

    else{

      if (actual_filtro == 3){

        antiguedad.forEach(function(value, key){

          temp1.push((value.suma / value.contador).toFixed(1) * 1);
          temp2.push(value.anios);
        });
      }

      else{

        if (actual_filtro == 4){

          edad.forEach(function(value, key){

            temp1.push((value.suma / value.contador).toFixed(1) * 1);
            temp2.push(value.anios);
          });
        }

        else{

          if (actual_filtro == 5){

            sexo.forEach(function(value, key){

              temp1.push((value.suma / value.contador).toFixed(1) * 1);
              temp2.push(value.title);
            });
          }

          else{

            if (actual_filtro == 6){

              temp1.push((generaciones['Baby Boomers'] !== undefined ? (generaciones['Baby Boomers'].suma / generaciones['Baby Boomers'].contador).toFixed(1) * 1 : 0));
              temp1.push((generaciones['Generación X'] !== undefined ? (generaciones['Generación X'].suma / generaciones['Generación X'].contador).toFixed(1) * 1 : 0));
              temp1.push((generaciones['Generación Z'] !== undefined ? (generaciones['Generación Z'].suma / generaciones['Generación Z'].contador).toFixed(1) * 1 : 0));
              temp1.push((generaciones['Millenials'] !== undefined ? (generaciones['Millenials'].suma / generaciones['Millenials'].contador).toFixed(1) * 1 : 0));
              temp2.push('Baby Boomers');
              temp2.push('Generación X');
              temp2.push('Generación Z');
              temp2.push('Millenials');
            }

            else{

              if (actual_filtro == 7){

                Object.values(turnos).forEach(function(value){

                  temp1.push((value.suma / value.contador).toFixed(1) * 1);
                  temp2.push(value.title);
                });
              }

              else{

                if (actual_filtro == 8){

                  Object.values(tipos_puestos).forEach(function(value){

                    temp1.push((value.suma / value.contador).toFixed(1) * 1);
                    temp2.push(value.title);
                  });
                }

                else{

                  if (actual_filtro == 9){

                    Object.values(unidades_negocio).forEach(function(value){

                      temp1.push((value.suma / value.contador).toFixed(1) * 1);
                      temp2.push(value.title);
                    });
                  }

                  else{

                    if (actual_filtro == 10){

                      Object.values(clasificacion).forEach(function(value){

                        temp1.push((value.suma / value.contador).toFixed(1) * 1);
                        temp2.push(value.title);
                      });
                    }

                    else{

                      filters.forEach(function(value, key){

                        temp1.push((value.suma / value.contador).toFixed(1) * 1);
                        temp2.push(value.title);
                      });
                    }
                  }
                }
              }
            }
          }
        }
      }

      datos = temp1;
      datos2 = temp2;

      if (actual_orden == 1){

        for (total = 0;total < datos.length;total++){

          for (i = 0;i < (datos.length - 1);i++){

            k = i + 1;

            if (datos[k] > datos[i]){

              valor = datos[i];
              datos[i] = datos[k];
              datos[k] = valor;
              valor = datos2[i];
              datos2[i] = datos2[k];
              datos2[k] = valor;
            }
          }
        }
      }
    }

    var promedio = 0;
    var contador = 0;

    for (i = 0;i < datos.length;i++){

      promedio = promedio + datos[i];
      contador++;
    }

    if (actual_filtro != 2 || (actual_filtro == 2 && factor == 0)){

      promedio = promedio / contador;
    }

    $('h1.average').text(promedio.toFixed(2));

    if (etiquetas.length > 0){

      for (i = 0;i < etiquetas.length;i++){

        if (promedio >= etiquetas[i].valor){

          $('h1.average').parent().css('background-color', etiquetas[i].color);
          break;
        }
      }
    }

    else{

      if (promedio >= 4){

        $('h1.average').parent().css('background-color', 'rgb(144,237,125)');
      }

      else{

        if (promedio >= 3){

          $('h1.average').parent().css('background-color', 'rgb(253,236,109)');
        }

        else{

          $('h1.average').parent().css('background-color', 'rgb(255,116,116)');
        }
      }
    }

    series = [{
      showInLegend: false,
      data: datos,
      colorByPoint: true,
      dataLabels: {
        enabled: true,
        verticalAlign: 'top',
        align: 'center',
        y: -20,
        formatter: function(){
          return this.y;
        }
      }
    }];

    xAxis = [{
      categories: datos2,
      labels: {
            style: {
              fontSize:'12px'
            }
      }
    }];

    var credits = {
      enabled: false
    };

    var json = {};
    json.chart = chart;
    json.title = title;
    json.series = series;
    json.credits = credits;
    json.plotOptions = plotOptions;
    json.xAxis = xAxis;
    json.yAxis = yAxis;
    highchart = new Highcharts.Chart(json);

    if (table != 0){

      /*setTimeout(function(){
      
        $('ul.pagination li.paginate_button.active').next().click();
        $('ul.pagination li.paginate_button:first').next().click();
      }, 1000);*/
      table = $('.preguntas').DataTable({
      language: {
        "sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
            },
             "aaSorting": []
    });
    }
  }
  
  function showValues(){

    $('#R0-value').html(highchart.options.chart.options3d.alpha);
    $('#R1-value').html(highchart.options.chart.options3d.beta);
  }
</script>
@endsection