@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
	@include('clima-organizacional/partials/sub-menu')
	</div>
	<div class="col-md-10">
    <img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">

    <h3 class="titulos-evaluaciones mt-4 font-weight-bold">REPORTE DE MAPA DE CALOR POR GRUPOS DE ÁREAS</h3>
    <hr>

    <div class="row justify-content-center">
        
        <div class="col-3">

            <div class="form-group text-center">
              <form id="changePlanForm" action="/clima-organizacional/heat-report" method="post">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <label for="name" class="requerido">Periodo:</label>
                  <select name="period_id" class="period_id form-control" >
                           @for ($i = 0;$i < count($periods);$i++)

                          <option value="{{$periods[$i]->id}}" <?php if ($period_id == $periods[$i]->id){ ?>selected="selected"<?php } ?>>{{$periods[$i]->name}}</option>
                        @endfor

                  </select>   

                <input type="submit" style="display: none">
              </form>

            </div>
        </div>

    </div>
 
    <div class="row">
      <div class="col-md-12">
        <hr>
        <table class="table table-responsive">
          <thead>
            <th style="color: #7C75B7"><b>GRUPOS DE ÁREAS</b></th>

          @foreach ($clima_factors as $key => $factor)
        
            <th class="text-center" style="background-color: #7C75B7; color: white"><b>{{$factor}}</b></th>
          @endforeach

            <th class="text-center" style="background-color: #7C75B7; color: white"><b>Total general</b></th>

          </thead>
          <tbody>

    <?php $current_group_area = 0;
          $current_factor = 0;
          $group_area_total = 0;
          $group_area_factors = 0;
          $totales_factores = $contadores_factores = array(); ?>

          @foreach ($results as $key => $result)

            @if ($current_group_area != $result->id)

              @if ($current_group_area != 0)

                @while ($current_factor < count($clima_factors))

              <td></td>

            <?php $current_factor++; ?>
                @endwhile

          <?php $total_by_group_area = $group_area_total / $group_area_factors;
                $color = 0;

                if (count($etiquetas) > 0){

                  for ($i = 0;$i < count($etiquetas);$i++){

                    if ($total_by_group_area >= $etiquetas[$i]->valor){

                      if (!empty($etiquetas[$i]->color)){

                        $color = $etiquetas[$i]->color;
                      }

                      break;
                    }
                  }
                }

                if (empty($color)){ ?>

              <td class="text-center" style="background-color: {{($total_by_group_area > 8.333333333333333 ? '#67C080' : ($total_by_group_area > 6.666666666666667 ? '#D0DE87' : ($total_by_group_area > 5 ? '#FAE886' : ($total_by_group_area > 3.333333333333333 ? '#FFCE7D' : ($total_by_group_area > 1.666666666666667 ? '#F69779' : '#F9726C')))))}}; border-color: {{($total_by_group_area > 8.333333333333333 ? '#67C080' : ($total_by_group_area > 6.666666666666667 ? '#D0DE87' : ($total_by_group_area > 5 ? '#FAE886' : ($total_by_group_area > 3.333333333333333 ? '#FFCE7D' : ($total_by_group_area > 1.666666666666667 ? '#F69779' : '#F9726C')))))}}">{{number_format($total_by_group_area, 2)}}</td>
            </tr>
          <?php }

            else{ ?>

              <td class="text-center" style="background-color: {{$color}}; border-color: {{$color}}">{{number_format($total_by_group_area, 2)}}</td>
      <?php } ?>
              @endif

            <tr>

        <?php $current_group_area = $result->id;
              $current_factor = 0;
              $group_area_total = 0;
              $group_area_factors = 0; ?>

              <td>{{$result->name}}</td>
            @endif

            @while (!empty($clima_factors[$current_factor]) && $clima_factors[$current_factor] != $result->factor_name)

        <?php $current_factor++; ?>

              <td></td>
            @endwhile

            @if (!isset($totales_factores[$current_factor]))

        <?php $totales_factores[$current_factor] = 0;
              $contadores_factores[$current_factor] = 0; ?>
            @endif

      <?php $totales_factores[$current_factor] += $result->average;
            $contadores_factores[$current_factor]++; 
            $current_factor++;
            $group_area_total += $result->average;
            $group_area_factors++;
            $color = 0;

            if (count($etiquetas) > 0){

              for ($i = 0;$i < count($etiquetas);$i++){

                if ($result->average >= $etiquetas[$i]->valor){

                  if (!empty($etiquetas[$i]->color)){

                    $color = $etiquetas[$i]->color;
                  }

                  break;
                }
              }
            }

            if (empty($color)){ ?>

              <td class="text-center" style="background-color: {{($result->average > 8.333333333333333 ? '#67C080' : ($result->average > 6.666666666666667 ? '#D0DE87' : ($result->average > 5 ? '#FAE886' : ($result->average > 3.333333333333333 ? '#FFCE7D' : ($result->average > 1.666666666666667 ? '#F69779' : '#F9726C')))))}}; border-color: {{($result->average > 8.333333333333333 ? '#67C080' : ($result->average > 6.666666666666667 ? '#D0DE87' : ($result->average > 5 ? '#FAE886' : ($result->average > 3.333333333333333 ? '#FFCE7D' : ($result->average > 1.666666666666667 ? '#F69779' : '#F9726C')))))}}">{{number_format($result->average, 2)}}</td>
      <?php }

            else{ ?>

              <td class="text-center" style="background-color: {{$color}}; border-color: {{$color}}">{{number_format($result->average, 2)}}</td>
      <?php } ?>
          @endforeach
          
          @if ($current_group_area != 0)

            @while ($current_factor < count($clima_factors))

              <td></td>

        <?php $current_factor++; ?>
            @endwhile

      <?php $total_by_group_area = $group_area_total / $group_area_factors;
            $color = 0;

            if (count($etiquetas) > 0){

              for ($i = 0;$i < count($etiquetas);$i++){

                if ($total_by_group_area >= $etiquetas[$i]->valor){

                  if (!empty($etiquetas[$i]->color)){

                    $color = $etiquetas[$i]->color;
                  }

                  break;
                }
              }
            }

            if (empty($color)){ ?>

              <td class="text-center" style="background-color: {{($total_by_group_area > 8.333333333333333 ? '#67C080' : ($total_by_group_area > 6.666666666666667 ? '#D0DE87' : ($total_by_group_area > 5 ? '#FAE886' : ($total_by_group_area > 3.333333333333333 ? '#FFCE7D' : ($total_by_group_area > 1.666666666666667 ? '#F69779' : '#F9726C')))))}}; border-color: {{($total_by_group_area > 8.333333333333333 ? '#67C080' : ($total_by_group_area > 6.666666666666667 ? '#D0DE87' : ($total_by_group_area > 5 ? '#FAE886' : ($total_by_group_area > 3.333333333333333 ? '#FFCE7D' : ($total_by_group_area > 1.666666666666667 ? '#F69779' : '#F9726C')))))}}">{{number_format($total_by_group_area, 2)}}</td>
      <?php }

            else{ ?>

              <td class="text-center" style="background-color: {{$color}}; border-color: {{$color}}">{{number_format($total_by_group_area, 2)}}</td>
      <?php } ?>

            </tr>
          @endif

            <tr>
              <td class="text-center" style="background-color: #7C75B7; color: white"><b>Total general</b></td>

      <?php $total_de_totales = 0;
            $contador_de_totales = 0; ?>

          @foreach ($clima_factors AS $key => $value)

            @if (isset($totales_factores[$key]))

        <?php $total_de_total = $totales_factores[$key] / $contadores_factores[$key];
              $total_de_totales += $total_de_total;
              $contador_de_totales++;
              $color = 0;

              if (count($etiquetas) > 0){

                for ($i = 0;$i < count($etiquetas);$i++){

                  if ($total_de_total >= $etiquetas[$i]->valor){

                    if (!empty($etiquetas[$i]->color)){

                      $color = $etiquetas[$i]->color;
                    }

                    break;
                  }
                }
              }

              if (empty($color)){ ?>

              <td class="text-center" style="background-color: {{($total_de_total > 8.333333333333333 ? '#67C080' : ($total_de_total > 6.666666666666667 ? '#D0DE87' : ($total_de_total > 5 ? '#FAE886' : ($total_de_total > 3.333333333333333 ? '#FFCE7D' : ($total_de_total > 1.666666666666667 ? '#F69779' : '#F9726C')))))}}; border-color: {{($total_de_total > 8.333333333333333 ? '#67C080' : ($total_de_total > 6.666666666666667 ? '#D0DE87' : ($total_de_total > 5 ? '#FAE886' : ($total_de_total > 3.333333333333333 ? '#FFCE7D' : ($total_de_total > 1.666666666666667 ? '#F69779' : '#F9726C')))))}}">{{number_format($total_de_total, 2)}}</td>
        <?php }

              else{ ?>

              <td class="text-center" style="background-color: {{$color}}; border-color: {{$color}}">{{number_format($total_de_total, 2)}}</td>
        <?php } ?>
            @else

              <td></td>
            @endif
          @endforeach

          @if ($contador_de_totales > 0)

      <?php $total_de_totales = $total_de_totales / $contador_de_totales;
            $color = 0;

            if (count($etiquetas) > 0){

              for ($i = 0;$i < count($etiquetas);$i++){

                if ($total_de_totales >= $etiquetas[$i]->valor){

                  if (!empty($etiquetas[$i]->color)){

                    $color = $etiquetas[$i]->color;
                  }

                  break;
                }
              }
            }

            if (empty($color)){ ?>

              <td class="text-center" style="background-color: {{($total_de_totales > 8.333333333333333 ? '#67C080' : ($total_de_totales > 6.666666666666667 ? '#D0DE87' : ($total_de_totales > 5 ? '#FAE886' : ($total_de_totales > 3.333333333333333 ? '#FFCE7D' : ($total_de_totales > 1.666666666666667 ? '#F69779' : '#F9726C')))))}}; border-color: {{($total_de_totales > 8.333333333333333 ? '#67C080' : ($total_de_totales > 6.666666666666667 ? '#D0DE87' : ($total_de_totales > 5 ? '#FAE886' : ($total_de_totales > 3.333333333333333 ? '#FFCE7D' : ($total_de_totales > 1.666666666666667 ? '#F69779' : '#F9726C')))))}}">{{number_format($total_de_totales, 2)}}</td>
      <?php }

            else{ ?>

              <td class="text-center" style="background-color: {{$color}}; border-color: {{$color}}">{{number_format($total_de_totales, 2)}}</td>
      <?php } ?>
          @else

              <td></td>
          @endif

            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@endsection
@section('scripts')

<script type="text/javascript">

  $(document).ready(function(){

    $('select.period_id').change(function(){

      $('form#changePlanForm').submit();
    });
  });
    
</script>
@endsection