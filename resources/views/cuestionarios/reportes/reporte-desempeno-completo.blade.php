@extends('layouts.app')

@section('title', 'Reporte Evaluación de Desempeño')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/evaluacion_desempeno.png" alt="">
<div class="row margin-top-20">
	<div class="col-md-12">
		<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Reporte Evaluación de Desempeño</h3>
		<hr class="mb-5">
	</div>
</div>

@if (!empty($periodos))

<div class="row margin-top-20">
	<div class="col-md-12">
		<div>Periodo: <select class="periodos form-control" style="display: inline-block; width: auto">

			      <?php foreach ($periodos as $key => $periodo){ ?>
			                                      	
			            	<option value="<?php echo $periodo->id?>" <?php if (!empty($id_periodo) && $id_periodo == $periodo->id){ ?>selected="selected"<?php } ?>><?php echo $periodo->descripcion?></option>
			      <?php } ?>
								 																 
								 	</select>
		</div>
	</div>
</div>

@if (!empty($resultados))

<div class="row margin-top-20">
	<div class="col-md-12">
		<table width="100%" class="data-table table table-striped">
			<thead class="cabeceras-tablas-evaluaciones">
				<tr>
					<th>ID</th>
					<th>Evaluado</th>
					<th>Departamento</th>
					<th>Nombre jefe Directo</th>
					<th>Promedio del Jefe</th>
					<th>Promedio Par</th>
					<th>Promedio Colaborador</th>
					<th>Promedio autoevaluación</th>
					<th>Retro</th>

	<?php foreach ($factores as $key => $factor){ ?>
		
					<th>{{$factor->nombre}}</th>
	<?php } ?>
					
					<th>Promedio</th>
					<th>Promedio Ponderado</th>
				</tr>
			</thead>
			<tbody>

 <?php 	$actual_usuario = 0;
 				$total = 0;
 				$promedio = 0;
 				$contador_factores = 0;
 				$actual_columna = 0;
 				$i = 0;
 				$j = 0;
 				$autoevaluacion = $jefe = $colaborador = $par = false;
 				$promedio_ponderado = 0;

 				foreach ($resultados as $key => $resultado){

 					if ($actual_usuario != $resultado->id_evaluado){

 						if ($actual_usuario != 0){

 							for ($i = $actual_columna;$i < count($factores);$i++){
						
								if ($factores[$i]->id == $resultado->id_factor){

									break;
								} ?>

					<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
				<?php }

 							$total = $promedio / $contador_factores; ?>

 					<td class="text-center" style="vertical-align: middle; font-weight: bold; font-size: 20px; border: 1px solid #DDD; background-color: {{($total >= 3.5 ? '#008000' : ($total >= 3 ? '#73CB77' : ($total >= 2.5 ? '#FFFF00' : ($total >= 2 ? '#FF4343' : '#C00000'))))}}">{{number_format($total, 1, '.', ',')}}</td>

 				<?php if ($promedio_ponderado != 0){ ?>

 					<td class="text-center" style="vertical-align: middle; font-weight: bold; font-size: 20px; border: 1px solid #DDD; background-color: {{($promedio_ponderado >= 3.5 ? '#008000' : ($promedio_ponderado >= 3 ? '#73CB77' : ($promedio_ponderado >= 2.5 ? '#FFFF00' : ($promedio_ponderado >= 2 ? '#FF4343' : '#C00000'))))}}">{{number_format($promedio_ponderado, 1, '.', ',')}}</td>
 				<?php }

 							else{ ?>

 					<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
 				<?php } ?>

 				</tr>
 			<?php }

 						$actual_usuario = $resultado->id_evaluado;
 						$contador_factores = 0;
 						$total = 0;
 						$actual_columna = 0;
 						$promedio = 0;
 						$autoevaluacion = $jefe = $colaborador = $par = false;
 						$promedio_ponderado = 0; ?>

				<tr>
					<td class="text-left" style="border: 1px solid #DDD">{{$actual_usuario}}</td>
					<td class="text-left" style="border: 1px solid #DDD">{{str_replace('Ã‘','Ñ',$promedios_tipo_evaluacion[$j]->first_name)}} {{str_replace('Ã‘','Ñ',$promedios_tipo_evaluacion[$j]->last_name)}}</td>
					<td class="text-left" style="border: 1px solid #DDD">{{str_replace('Ã‘','Ñ',$promedios_tipo_evaluacion[$j]->division)}}</td>
					<td class="text-left" style="border: 1px solid #DDD">{{str_replace('Ã‘','Ñ',$promedios_tipo_evaluacion[$j]->jefe)}}</td>

			<?php if (!empty($promedios_tipo_evaluacion[$j]->promedio_jefe)){

							$jefe = true; ?>

					<td class="text-center" style="vertical-align: middle; font-weight: bold; font-size: 20px; border: 1px solid #DDD; background-color: {{($promedios_tipo_evaluacion[$j]->promedio_jefe >= 3.5 ? '#008000' : ($promedios_tipo_evaluacion[$j]->promedio_jefe >= 3 ? '#73CB77' : ($promedios_tipo_evaluacion[$j]->promedio_jefe >= 2.5 ? '#FFFF00' : ($promedios_tipo_evaluacion[$j]->promedio_jefe >= 2 ? '#FF4343' : '#C00000'))))}}">{{number_format($promedios_tipo_evaluacion[$j]->promedio_jefe, 1, '.', ',')}}</td>
			<?php }

						else{ ?>

					<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
			<?php }

						if (!empty($promedios_tipo_evaluacion[$j]->promedio_par)){

							$par = true; ?>

					<td class="text-center" style="vertical-align: middle; font-weight: bold; font-size: 20px; border: 1px solid #DDD; background-color: {{($promedios_tipo_evaluacion[$j]->promedio_par >= 3.5 ? '#008000' : ($promedios_tipo_evaluacion[$j]->promedio_par >= 3 ? '#73CB77' : ($promedios_tipo_evaluacion[$j]->promedio_par >= 2.5 ? '#FFFF00' : ($promedios_tipo_evaluacion[$j]->promedio_par >= 2 ? '#FF4343' : '#C00000'))))}}">{{number_format($promedios_tipo_evaluacion[$j]->promedio_par, 1, '.', ',')}}</td>
			<?php }

						else{ ?>

					<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
			<?php }

						if (!empty($promedios_tipo_evaluacion[$j]->promedio_colaborador)){

							$colaborador = true; ?>

					<td class="text-center" style="vertical-align: middle; font-weight: bold; font-size: 20px; border: 1px solid #DDD; background-color: {{($promedios_tipo_evaluacion[$j]->promedio_colaborador >= 3.5 ? '#008000' : ($promedios_tipo_evaluacion[$j]->promedio_colaborador >= 3 ? '#73CB77' : ($promedios_tipo_evaluacion[$j]->promedio_colaborador >= 2.5 ? '#FFFF00' : ($promedios_tipo_evaluacion[$j]->promedio_colaborador >= 2 ? '#FF4343' : '#C00000'))))}}">{{number_format($promedios_tipo_evaluacion[$j]->promedio_colaborador, 1, '.', ',')}}</td>
			<?php }

						else{ ?>

					<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
			<?php }

						if (!empty($promedios_tipo_evaluacion[$j]->promedio_autoevaluacion)){

							$autoevaluacion = true; ?>

					<td class="text-center" style="vertical-align: middle; font-weight: bold; font-size: 20px; border: 1px solid #DDD; background-color: {{($promedios_tipo_evaluacion[$j]->promedio_autoevaluacion >= 3.5 ? '#008000' : ($promedios_tipo_evaluacion[$j]->promedio_autoevaluacion >= 3 ? '#73CB77' : ($promedios_tipo_evaluacion[$j]->promedio_autoevaluacion >= 2.5 ? '#FFFF00' : ($promedios_tipo_evaluacion[$j]->promedio_autoevaluacion >= 2 ? '#FF4343' : '#C00000'))))}}">{{number_format($promedios_tipo_evaluacion[$j]->promedio_autoevaluacion, 1, '.', ',')}}</td>
			<?php }

						else{ ?>

					<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
			<?php } ?>

					<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">{{(!empty($resultado->comentario) && !empty($resultado->objetivo) ? 'SI' : 'NO')}}
		
			<?php if (!empty($ponderaciones) && in_array($resultado->id_nivel_puesto, $niveles_puestos)){

							$promedio_ponderado = $promedios_tipo_evaluacion[$j]->promedio_jefe * $ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->jefe / 100 + $promedios_tipo_evaluacion[$j]->promedio_par * $ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->par / 100 + $promedios_tipo_evaluacion[$j]->promedio_colaborador * $ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->colaborador / 100 + $promedios_tipo_evaluacion[$j]->promedio_autoevaluacion * $ponderaciones[$posiciones[$resultado->id_nivel_puesto]]->autoevaluacion / 100;
						}

						$j++;
					}

					for ($i = $actual_columna;$i < count($factores);$i++){
						
						if ($factores[$i]->id == $resultado->id_factor){

							break;
						} ?>

					<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
		<?php }

					$actual_columna = $i;
					$contador_factores++;
					$total = $resultado->total_evaluacion / $resultado->numero_evaluaciones;
					$promedio += $total;
					$actual_columna++; ?>

					<td class="text-center" style="vertical-align: middle; font-weight: bold; font-size: 20px; border: 1px solid #DDD; background-color: {{($total >= 3.5 ? '#008000' : ($total >= 3 ? '#73CB77' : ($total >= 2.5 ? '#FFFF00' : ($total >= 2 ? '#FF4343' : '#C00000'))))}}">{{number_format($resultado->total_evaluacion / $resultado->numero_evaluaciones, 1, '.', ',')}}</td>
	<?php }

				if ($actual_usuario != 0){

					for ($i = $actual_columna;$i < count($factores);$i++){
						
						if ($factores[$i]->id == $resultado->id_factor){

							break;
						} ?>

					<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
		<?php }

 					$total = $promedio / $contador_factores; ?>

 					<td class="text-center" style="vertical-align: middle; font-weight: bold; font-size: 20px; border: 1px solid #DDD; background-color: {{($total >= 3.5 ? '#008000' : ($total >= 3 ? '#73CB77' : ($total >= 2.5 ? '#FFFF00' : ($total >= 2 ? '#FF4343' : '#C00000'))))}}">{{number_format($total, 1, '.', ',')}}</td>

 		<?php if ($promedio_ponderado != 0){ ?>

 					<td class="text-center" style="vertical-align: middle; font-weight: bold; font-size: 20px; border: 1px solid #DDD; background-color: {{($promedio_ponderado >= 3.5 ? '#008000' : ($promedio_ponderado >= 3 ? '#73CB77' : ($promedio_ponderado >= 2.5 ? '#FFFF00' : ($promedio_ponderado >= 2 ? '#FF4343' : '#C00000'))))}}">{{number_format($promedio_ponderado, 1, '.', ',')}}</td>
 		<?php }

 					else{ ?>

 					<td class="text-center" style="vertical-align: middle; border: 1px solid #DDD">N/A</td>
 		<?php } ?>
 							
 				</tr>
 	<?php } ?>

			</tbody>
		</table>
	</div>
</div>

@else

<div class="row">
	<div class="col-md-12 text-center">
		<h4 class="margin-top-20 no_subordinados">No cuenta con colaboradores o aún no hay resultados en la evaluación del periodo</h4>
	</div>
</div>
@endif

@else

<div class="row">
	<div class="col-md-12 text-center">
		<h4 class="margin-top-20">No Hay Periodos Abiertos o Cerrados</h4>
	</div>
</div>
@endif
</div>
</div>
@endsection

@section('scripts')
	<script>
		
		$(document).ready(function(){

			$('.data-table').DataTable({
				dom: 'Blfrtip',
   			'order': [[0,'asc']],
   			'scrollX': true,
   			language: {
		 		'url': '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
      		  },
      	buttons: [
        {
          extend: 'excel',
					text: 'Exportar a Excel',
					titleAttr: 'Exportar a Excel',
					title: 'Reporte Desempeño Completo'
				}
        ]
   		});

   		$('select.periodos').change(function(){

   			window.location = '/reporte-detalle/' + $(this).val();
   		});
		});
	</script>
@endsection
