@extends('clima-organizacional.app')

@section('title', 'Evaluación')

@section('content')
<?php $tag = 0; ?>
<style type="text/css">
	.responses {display: inline-block; float: left;}
</style>
<div class="row mt-3">
	<div class="col-md-2 text-right">
		@include('cuestionarios/partials/sub-menu')
	</div>
	<div class="col-md-10">
	<img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
<form action="{{ url('/clima-organizacional/guardar-respuestas') }}" method="post" >
	@csrf
	<h3 class="titulos-evaluaciones mt-4 font-weight-bold">Encuesta</h3>
	<hr>
	<h5 class="titulos-evaluaciones mt-4 font-weight-bold">Instrucciones</h5>
	<p>Para contestar cada una de las secciones relativas a una dimensión o aspecto del Clima Laboral, lee con cuidado cada pregunta y decide qué tan de acuerdo o qué tan desacuerdo estás con lo que describe. De tu lado derecho se encuentran unos "emojis" para que indiques tu opinión. Presiona debajo de aquel que mejor represente tu opinión</p>

	@foreach ($closed_questions as $factor)
		<!--<h3 class="titulos-evaluaciones mt-4 font-weight-bold bg-purple bg-purple-sepanka p-2 rounded cl-white">Factor</h3>
		<div style="background-color: #EDEDF6; padding: 10px">
			<h3 class="text-center titulos-evaluaciones" style="margin-top: 0">{{ $factor->name }}</h3>
			<p class="text-center" style="margin: 0">{{ $factor->description }}</p>
		</div>
		<h3 class="titulos-evaluaciones mt-4 font-weight-bold bg-purple bg-purple-sepanka p-2 rounded cl-white">Pregunta</h3>-->
		<table class="table">
			<thead class="titulos-evaluaciones font-weight-bold bg-purple bg-purple-sepanka p-2 rounded cl-white">
				<tr>
					<th style="min-width: 50%">
						<h3 style="margin: 0;">Pregunta</h3>
					</th>

				@if ($no_aplica)

					<th class="text-center" style="font-size: 10px">No aplica</th>
				@endif

				@foreach ($etiquetas as $etiqueta)

					<th class="text-center" style="font-size: 10px">{{$etiqueta->name}}</th>
				@endforeach

				</tr>
			</thead>
			<tbody>
				@foreach ($factor->questions as $question)
					<tr <?php if ($status->status > 1 && is_null($question->answer)){ ?>class="btn-danger"<?php } ?>>
						<td class="w-50">{{ $question->question }}</td>

					@if ($no_aplica)

						<td class="text-center pregunta">
							<div style="width: 40px; height: 40px; border-radius: 50%; background-color: gray"></div><br>
							<input type="radio" name="question[{{ $question->id }}]" class="{{ $question->id }}" value="N/A" {{ (!is_null($question->answer) && ($question->answer->answer == 'N/A'))?'checked':'' }}>
						</td>
					@endif

					@foreach ($etiquetas as $key => $etiqueta)

						@if ($question->positive == 1)

				<?php $tag = $etiqueta; ?>
						@else

				<?php $tag = $etiquetas[count($etiquetas) - 1 - $key]; ?>
						@endif

						<td class="text-center pregunta"><div style="min-height: 40px">@if (!empty($tag->file))<img src="/img/clima/etiquetas/{{$tag->id}}/{{$tag->file}}" style="max-width: 100%">@endif</div><br><input type="radio" name="question[{{ $question->id }}]" class="{{ $question->id }}" value="{{$etiqueta->valor}}" {{ (!is_null($question->answer) && ($question->answer->answer == $etiqueta->valor))?'checked':'' }}></td>
					@endforeach

					</tr>
				@endforeach

			</tbody>
		</table>
	@endforeach
	<!--<h3 class="titulos-evaluaciones mt-4 font-weight-bold bg-purple bg-purple-sepanka p-2 rounded cl-white">Factor</h3>
	<div style="background-color: #EDEDF6; padding: 10px">
		<h3 class="text-center titulos-evaluaciones" style="margin-top: 0">Preguntas Abiertas</h3>
	</div>
	<h3 class="titulos-evaluaciones mt-4 font-weight-bold bg-purple bg-purple-sepanka p-2 rounded cl-white">Pregunta</h3>-->
	<table class="table">
		<thead class="titulos-evaluaciones font-weight-bold bg-purple bg-purple-sepanka p-2 rounded cl-white">
			<tr>
				<td class="w-50">
					<h3 style="margin: 0">Pregunta</h3>
				</td>
				<td></td>
			</tr>
		</thead>
		<tbody>
			@foreach ($opened_questions as $question)
				<tr <?php if ($status->status > 1 && is_null($question->answer)){ ?>class="btn-danger"<?php } ?>>
					<td class="w-50">{{ $question->question }}</td>
					<td><textarea class="respuesta_abierta form-control" id="{{$question->id}}" name="question[{{ $question->id }}]">{{ (!is_null($question->answer))?$question->answer->answer:'' }}</textarea></td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<input type="submit" value="Guardar" class="btn btn-primary text-center">
</form>
</div>
</div>
@endsection

@section('scripts')
	<script>

	var total_preguntas;
	var guardar = false;
	var alertar = false;
	var preguntas_sin_contestar = false;
		
	$(document).ready(function(){

		$.ajaxSetup({
    		headers: {
        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    		}
		});

		$('input[type="radio"]').click(function(){
		//$('select.respuesta').change(function(){

			var id_pregunta = $(this).attr('class');
			var respuesta = $(this).val();

			$.ajax({
        		type:'POST',    
        		url: '/clima-organizacional/guardar-respuesta',
        		data:{
          			id_pregunta : id_pregunta,
          			respuesta : respuesta
        			}
      		});
		});

		$('button.guardar_abiertas').click(function(){

			preguntas_sin_contestar = false;

			//$('select.respuesta').each(function(){
			$('td.pregunta').each(function(){

				//if ($(this).val() == '0'){
				if ($(this).find('input[type="radio"]:checked').length == 0){

					preguntas_sin_contestar = true;
					$(this).parent().css('background-color', 'red')
				}
			});

			/*if (preguntas_sin_contestar){

				alert('La encuesta no ha sido contestada completamente, favor de terminarla.');
				return false;
			}*/

			$('textarea.respuesta_abierta').each(function(){

				if ($(this).val() == ''){

					preguntas_sin_contestar = true;
					$(this).parent().parent().parent().parent().css('background-color', 'red')
				}
			});
			
			if (preguntas_sin_contestar){

				alert('La encuesta no ha sido contestada completamente, favor de terminarla.');
				return false;
			}

			$('textarea.respuesta_abierta').each(function(){

				$('form.guardar_respuestas').append('<input type="hidden" name="respuestas[]" value="' + $(this).val() + '"><input type="hidden" name="preguntas[]" value="' + $(this).attr('id') + '">');
			});

			$('form.guardar_respuestas').submit();
		});

		/*$('form.guardar_respuestas').submit(function(){

			guardar = true;
		});*/
	});

	setInterval(function(){

		if (preguntas_sin_contestar){

			alertar = false;
			//alert('La encuesta no ha sido concluida correctamente, favor de terminarla');
			//window.reload();
		}
	}, 100)

	window.onbeforeunload = function(e){ 
  
  if (!guardar){
    
  	alertar = true;
  	$(window).unbind();
    return undefined;
  }
}
	</script>
@endsection