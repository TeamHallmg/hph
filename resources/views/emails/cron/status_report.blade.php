<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reporte Importación</title>
</head>
<body>
    <h2>Resumen de Importación de Usuarios del Día {{ date('d-m-Y') }}</h2>
    <h3>Usuarios Nuevos</h3>
    <ul>
        @foreach ($data['created_users'] as $line)
            <li>{{ $line }}</li>
        @endforeach
    </ul>
    <h4>Errores al crear Usuarios</h4>
    <ul>
        @foreach ($data['create_error'] as $line)
            <li>{{ $line }}</li>
        @endforeach
    </ul>
    <hr>
    <h3>Usuarios actualizados</h3>
    <ul>
        @foreach ($data['updated_users'] as $line)
            <li>{{ $line }}</li>
        @endforeach
    </ul>
    <h4>Errores al actualizar Usuarios</h4>
    <ul>
        @foreach ($data['update_errors'] as $line)
            <li>{{ $line }}</li>
        @endforeach
    </ul>
    <hr>
    <h3>Usuarios Suspendidos</h3>
    <ul>
        @foreach ($data['deleted_users'] as $line)
            <li>{{ $line }}</li>
        @endforeach
    </ul>
    <h4>Errores al eliminar Usuarios</h4>
    <ul>
        @foreach ($data['delete_errors'] as $line)
            <li>{{ $line }}</li>
        @endforeach
    </ul>
    <hr>
    <h3>Errores en el Archivo</h3>
    <ul>
        @foreach ($data['data_error'] as $line)
            <li>{{ $line }}</li>
        @endforeach
    </ul>
</body>
</html>