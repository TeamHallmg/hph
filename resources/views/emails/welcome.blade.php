<!DOCTYPE html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Correo de Bienvenida</title>
    <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
    <style>
    @import url('https://fonts.googleapis.com/css?family=Nunito&display=swap');
    
    body{
        font-family: 'Nunito', sans-serif;
    }
    .container {
        width: 100%;
        padding-right: 7.5px;
        padding-left: 7.5px;
        margin-right: auto;
        margin-left: auto;
    }

    @media (min-width: 576px) {
        .container {
            max-width: 540px;
        }
    }

    @media (min-width: 768px) {
        .container {
            max-width: 720px;
        }
    }

    @media (min-width: 992px) {
        .container {
            max-width: 960px;
        }
    }

    @media (min-width: 1200px) {
        .container {
            max-width: 1140px;
        }
    }

    .container-fluid {
        width: 100%;
        padding-right: 7.5px;
        padding-left: 7.5px;
        margin-right: auto;
        margin-left: auto;
    }

    .row {
        display: flex;
        flex-wrap: wrap;
        margin-right: -7.5px;
        margin-left: -7.5px;
    }

    .img-fluid {
        max-width: 100%;
        height: auto;
    }

    .mr-3,
    .mx-3 {
        margin-right: 3rem !important;
    }

    .ml-3,
    .mx-3 {
        margin-left: 3rem !important;
    }

    .text-justify {
        text-align: justify !important;
    }
/*---------------------------------------------------------------------*/
    .font-weight-bold{
        font-weight: 900;
    }

    .font-x2{
        font-size: 1.5em;
    }
    .font-x1{
        font-size: 1.2em;
    }

    </style>
</head>
<body class="container-fluid">
    <a href="https://ahkimpech.soysepanka.com"><img class="img-fluid" src="{{asset('/img/email_header.png')}}" alt="email_header"></a>
    <div class="container-fluid">
        {{-- <div class="row mx-3">
            <p class="font-x1 text-justify">
                Recibe una cordial bienvenida a Sepanka Suite la plataforma para gestión y desarrollo del talento humano. 
                Sepanka Suite está integrada por varios módulos como el que vas a utilizar y que está diseñado para 
                medir el <i><b>Clima Laboral</b></i> que prevalece en donde laboras actualmente. 
                Contamos con tu buena disposición para responder un sencillo cuestionario en línea. 
                Para comenzar debes dar clic sobre la liga que se muestra a continuación; además, para acceder a la 
                Plataforma deberás utilizar el usuario y contraseña que aparece debajo.
            </p>
        </div>

        <div class="row mx-3">
            <p>
                <span class="font-weight-bold font-x2"><strong>Para entrar dirígete a la siguiente liga</strong></span>
                <br>
                <span class="font-x1">ahkimpech.soysepanka.com</span>
            </p>
        </div>
        <div class="row mx-3">
            <p><span class="font-weight-bold font-x2"><strong>Introduce tu usuario y contraseña</strong></span></p>
        </div> --}}
        
    
        <div class="row mx-3">
            <table style="width:100%;">
                <tr>
                    <td class="font-x1" style="text-align:center;">
                        <span class="font-weight-bold" style="color:#7A75B5;">Usuario:</span> {{ $tmp_email }}
                    </td>
                </tr>
                <tr>
                    <td class="font-x1" style="text-align:center;">
                        <span class="font-weight-bold" style="color:#7A75B5;">Contraseña:</span> {{ $password }}
                    </td>
                </tr>
            </table>
            {{-- <p class="font-x1"> {{ $tmp_email }}</p> --}}
        </div>
        {{-- <div class="row mx-3">
            <p class="font-x1"><span class="font-weight-bold">Contraseña:</span>  </p>
        </div> --}}

        {{-- <div class="row mx-3">
            <p class="font-x1"><span class="font-weight-bold"><strong>Nota:</strong></span>  es importante que al ingresar a la plataforma cambies tu contraseña y edites tu perfil, sube tu foto y ayúdanos a que todos sepamos un poco más de ti.</p>
        </div> --}}
    </div>
    <img class="img-fluid" src="{{asset('/img/email_footer.png')}}" alt="email_footer">
</body>
</html>