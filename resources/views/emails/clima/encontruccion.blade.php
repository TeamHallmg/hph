<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
    <meta name="viewport" content="width=600,initial-scale = 2.3,user-scalable=no">
    <!-- <![endif]-->

    <title>Email</title>

</head>


<body >
   
    <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff" style="box-shadow: 0 10px 20px rgb(0 0 0 / 19%), 0 6px 6px rgb(0 0 0 / 23%);margin-bottom: 30px;">

        <tbody>
            <tr>
            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
        </tr>

        <tr>
            <td align="center">

                <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590" >

                    <tbody><tr>
                        <td>
                            <table border="0" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
                                <tbody><tr>
                                    <td align="center" style="color: #aaaaaa; font-size: 20px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                                        <div style="line-height: 24px;">

                                        <img src="{{asset('img/logo_clima.png')}}" style="padding: 10px; display: block;" width="130" border="0" alt="">

                                        </div>
                                    </td>
                                </tr>
                            </tbody></table>

                        </td>
                    </tr>

                </tbody></table>
            </td>
        </tr>  
    </tbody></table>
     <!-- big image section -->
     <table border="0" width="80%" cellpadding="0" cellspacing="0"  class="bg_color" align="center">

        <tbody>
            
            <tr>
                <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
            </tr>
            <tr>
            <td align="center">
                <table border="0" align="center"  cellpadding="0" cellspacing="0" class="container590">
                    <tbody>  

                    <tr>
                        <td align="center" style="color: #000000; font-size: 35px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                            <div style="line-height: 35px">
                                <strong>
                                    Plantilla no disponible
                                </strong>
                            </div>
                        </td>
                    </tr> 
                    </tbody></table></td></tr>
    </tbody></table>
    <!-- end section -->

    <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="f4f4f4">

        <tbody><tr>
            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
        </tr>

        <tr>
            <td align="center">

                <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">

                    <tbody><tr>
                        <td>
                            <table border="0" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
                                <tbody><tr>
                                    <td align="center" style="color: #aaaaaa; font-size: 20px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                                        <div style="line-height: 24px;">

                                        <img src="{{asset('img/sepanka.png')}}" style="display: block;" width="300" border="0" alt="">

                                        </div>
                                    </td>
                                </tr>
                            </tbody></table>

                        </td>
                    </tr>

                </tbody></table>
            </td>
        </tr>

        <tr>
            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
        </tr>

    </tbody></table>
    <!-- end footer ====== -->




</body></html>
