<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
    <meta name="viewport" content="width=600,initial-scale = 2.3,user-scalable=no">
    <!-- <![endif]-->

    <title>Email</title>

    <style type="text/css">
        body {
            width: 100%;
            background-color: #ffffff;
            margin: 0;
            padding: 0;
            -webkit-font-smoothing: antialiased;
            mso-margin-top-alt: 0px;
            mso-margin-bottom-alt: 0px;
            mso-padding-alt: 0px 0px 0px 0px;
        }

        p,
        h1,
        h2,
        h3,
        h4 {
            margin-top: 0;
            margin-bottom: 0;
            padding-top: 0;
            padding-bottom: 0;
        }

        span.preheader {
            display: none;
            font-size: 1px;
        }

        html {
            width: 100%;
        }

        table {
            font-size: 14px;
            border: 0;
        }
        /* ----------- responsivity ----------- */

        @media only screen and (max-width: 640px) {
            /*------ top header ------ */
            .main-header {
                font-size: 20px !important;
            }
            .main-section-header {
                font-size: 28px !important;
            }
            .show {
                display: block !important;
            }
            .hide {
                display: none !important;
            }
            .align-center {
                text-align: center !important;
            }
            .no-bg {
                background: none !important;
            }
            /*----- main image -------*/
            .main-image img {
                width: 440px !important;
                height: auto !important;
            }
            /* ====== divider ====== */
            .divider img {
                width: 440px !important;
            }
            /*-------- container --------*/
            .container590 {
                width: 440px !important;
            }
            .container580 {
                width: 400px !important;
            }
            .main-button {
                width: 220px !important;
            }
            /*-------- secions ----------*/
            .section-img img {
                width: 320px !important;
                height: auto !important;
            }
            .team-img img {
                width: 100% !important;
                height: auto !important;
            }
        }

        @media only screen and (max-width: 479px) {
            /*------ top header ------ */
            .main-header {
                font-size: 18px !important;
            }
            .main-section-header {
                font-size: 26px !important;
            }
            /* ====== divider ====== */
            .divider img {
                width: 280px !important;
            }
            /*-------- container --------*/
            .container590 {
                width: 280px !important;
            }
            .container590 {
                width: 280px !important;
            }
            .container580 {
                width: 260px !important;
            }
            /*-------- secions ----------*/
            .section-img img {
                width: 280px !important;
                height: auto !important;
            }
        }
    </style>
    <!-- [if gte mso 9]><style type=”text/css”>
        body {
        font-family: arial, sans-serif!important;
        }
        </style>
    <![endif]-->
</head>


<body class="respond" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background: url({{asset('img/bg_GWTC.png')}}); background-size: 100% 100%; ">
    <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff" style="box-shadow: 0 10px 20px rgb(0 0 0 / 19%), 0 6px 6px rgb(0 0 0 / 23%);margin-bottom: 30px;">

        <tbody>
            <tr>
            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
        </tr>

        <tr>
            <td align="center">

                <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">

                    <tbody><tr>
                        <td>
                            <table border="0" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
                                <tbody><tr>
                                    <td align="center" style="color: #aaaaaa; font-size: 20px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                                        <div style="line-height: 24px;">

                                            <img src="{{asset('img/logo_clima.png')}}" style="padding: 10px; display: block;" width="130" border="0" alt="">

                                        </div>
                                    </td>
                                </tr>
                            </tbody></table>

                        </td>
                    </tr>

                </tbody></table>
            </td>
        </tr>  
    </tbody></table>
    <!-- big image section -->
    <table border="0" width="80%" cellpadding="0" cellspacing="0" class="bg_color" align="center">

        <tbody>
            <tr>
                <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
            </tr><tr>
            <td align="center">
                <table border="0" align="center"  cellpadding="0" cellspacing="0" class="container590">
                    <tbody>  

                    <tr>
                        <td align="center" style="color: #000000; font-size: 35px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                            <div style="line-height: 35px">
                                <strong>
                                    En búsqueda de un entorno laboral favorable
                                </strong>
                            </div>
                        </td>
                    </tr> 
                    
                    <tr>
                        <td height="10" style="font-size: 10px; line-height: 25px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td align="center" style="color: #000000; font-size: 21px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">


                            <div style="line-height: 30px">
                                Te invitamos a participar en el 
                            </div>
                        </td>
                    </tr> 
 
                    <tr>
                        <td height="10" style="font-size: 10px; line-height: 25px;">&nbsp;</td>
                    </tr>
 
                    <tr>
                        <td align="center" style="color: #094496; font-size: 40px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">

                            <img src="{{asset('img/clima.png')}}" style="display: block;" width="350" border="0" alt="">
                        </td>
                    </tr> 
                
                    <tr>
                        <td height="20" style="font-size: 20px; line-height: 25px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td align="center" style="color: #000000; font-size: 21px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">


                            <div style="line-height: 30px">
                                Tu contribución es muy importante
                            </div>
                        </td>
                    </tr> 
 
                    <tr>
                        <td height="20" style="font-size: 20px; line-height: 25px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td align="center" style="color: #000000; font-size: 21px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                            <div style="line-height: 30px;  width: 650px; text-align: justify;">
                                    Con tus sugerencias y recomendaciones podremos aplicar mejoras para crear un ambiente óptimo de trabajo que nos permita continuar siendo uno de los mejores lugares para trabajar
                            </div>
                        </td>
                    </tr> 

                    <tr>
                        <td height="20" style="font-size: 20px; line-height: 25px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td align="center" style="color: #000000; font-size: 21px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                            <div style="line-height: 30px">
                                Podrás contestar la encuesta desde cualquier dispositivo electrónico como
                            </div>
                        </td>
                    </tr> 

                    <tr>
                        <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td align="center">
                            <table border="0" width="85%" align="center" cellpadding="0" cellspacing="0" style="background: #8080803b">
                                <tbody><tr>
                                    <td height="2" style="font-size: 2px; line-height: 2px;">&nbsp;</td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>

                    <tr>
                        <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                    </tr>
                    
					<tr>
                        <td align="center" class="section-img">
                            <a href="" style=" border-style: none !important; display: block; border: 0 !important;">
                                <img src="{{asset('img/correo_GWTC.png')}}" style="display: block;" width="80%" border="0" alt="">
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td align="center">
                            <table border="0" width="85%" align="center" cellpadding="0" cellspacing="0" style="background: #8080803b">
                                <tbody><tr>
                                    <td height="2" style="font-size: 2px; line-height: 2px;">&nbsp;</td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>

                    <tr>
                        <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td align="center" style="color: #094496; font-size: 30px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">


                            <div style="line-height: 24px; color:red">
                                <strong>
                                    11 al 22 de Octubre 
                                </strong>
                            </div>
                        </td>
                    </tr> 

                    <tr>
                        <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                    </tr>
 
                    <tr>
                        <td align="center">
                            <table border="0" width="85%" align="center" cellpadding="0" cellspacing="0" style="background: #8080803b">
                                <tbody><tr>
                                    <td height="2" style="font-size: 2px; line-height: 2px;">&nbsp;</td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>

                    <tr>
                        <td height="20" style="font-size: 20px; line-height: 25px;">&nbsp;</td>
                    </tr>


                    <tr>
                        <td align="center" style="color: #000000; font-size: 21px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                            <div style="line-height: 30px">
                                    El manejo de la información que brindes en la encuesta será tratada de manera confidencial.
                            </div>
                        </td>
                    </tr> 


                    <tr>
                        <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                    </tr>
 
                    <tr>
                        <td align="center">
                            <table border="0" width="85%" align="center" cellpadding="0" cellspacing="0" style="background: #8080803b">
                                <tbody><tr>
                                    <td height="2" style="font-size: 2px; line-height: 2px;">&nbsp;</td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                   
                    <tr>
                        <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                    </tr>
 
                   
                    <tr>
                        <td align="center" style="color: #094496; font-size: 35px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">


                            <div style="line-height: 24px">
                                <strong>
                                    Tu aportación es muy importante  
                                </strong>
                            </div>
                        </td>
                    </tr>  

                    <tr>
                        <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td align="center" style="color: #000; font-size: 30px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">

                            <div style="line-height: 24px">
                                <strong>
                                   Sabemos que podemos contar contigo
                                </strong>
                            </div>
                        </td>
                    </tr> 
                </tbody></table>

            </td>
        </tr>
  
        <tr>
            <td height="40" style="font-size: 40px; line-height: 40px;">&nbsp;</td>
        </tr>

    </tbody></table>
    <!-- end section -->


    <!-- footer ====== -->
    <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="f4f4f4">

        <tbody><tr>
            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
        </tr>

        <tr>
            <td align="center">

                <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">

                    <tbody><tr>
                        <td>
                            <table border="0" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
                                <tbody><tr>
                                    <td align="center" style="color: #aaaaaa; font-size: 20px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                                        <div style="line-height: 24px;">

                                        <img src="{{asset('img/sepanka.png')}}" style="display: block;" width="300" border="0" alt="">

                                        </div>
                                    </td>
                                </tr>
                            </tbody></table>

                        </td>
                    </tr>

                </tbody></table>
            </td>
        </tr>

        <tr>
            <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
        </tr>

    </tbody></table>
    <!-- end footer ====== -->




</body></html>
