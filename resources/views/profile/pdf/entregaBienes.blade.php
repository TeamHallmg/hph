
<html>
<head>

    <link href="{{ asset('css/pdf.css')}}" rel="stylesheet">
<style>
body {
font-family: "Century Gothic", serif !important;
font-size: 1rem !important;
margin: 8mm;
}

            /** Define the footer rules **/
            footer {
                position: fixed; 
                bottom: 2cm; 
                right: 0cm;
                float: right;
                height: 2cm;
            }
</style>
</head>
<body>

 <main class="py-4">
            <div class="row justify-content-center">
                <div class="col-md-8 text-center">
                    <strong>ACCIÓN  DE PERSONAL / ENTREGA DE CELULAR</strong>
                </div>
            </div>
            <br><br>
            <div class="row justify-content-center  mt-3">
                <div class="col-md-8 text-justify">
                    
                    Se hace constar la entrega de un bien(activo) a: 

                    <strong>
                    
                    	{!! $profile->name!!}
                    	{!! $profile->surname_father!!}
                    	{!! $profile->surname_mother!!}

                    </strong> perteneciente a la Empresa LOGISTICA Y SERVICIOS DE DISTRIBUCIÓN, S.A. Quien ejecuta actividades de <strong>{!! $profile->user->employee->jobPosition->name!!}</strong> en el área de <strong>{!! $profile->user->employee->jobPosition->area->name !!}</strong>.

                </div>
            </div>

            <div class="row justify-content-center  mt-3">
                <div class="col-md-8 text-center">
                    
                    <strong> Detalles: </strong> 



				    <table class="table table-striped table-sm table-bordered nowrap mt-2" id="tableBienes">
				        <thead class="bg-red">
				            <tr> 
				                <th class="bg-red">Detalle</th>
				                <th class="bg-red">Descripción</th>
				            </tr>
				        </thead>
				        <tbody>


	                    @foreach($profile->bienes as $bien)

							<tr>
				        		<td>Cantidad / Bien</td>
				        		<td>(1) {!! $bien->tipobien->nombre !!}</td>
				        	</tr>
				        	<tr>
				        		<td>Código</td>
				        		<td>{!! $bien->codigo !!}</td>
				        	</tr>
				        	<tr>
				        		<td>Fecha Entrega</td>
				        		<td>{!! Carbon\Carbon::parse($bien->created_at)->format('d-m-Y') !!}</td>
				        	</tr>

	                            @foreach ($bien->tipobien->tipobiendetalles as $tipobiendetalles)

			                        <tr>

			                                <td>
			                                   {!! $tipobiendetalles->variable->etiqueta !!}
			                               	</td>
			                                
			                                <td>    
                                          @if(isset($tipobiendetalles->variable->valores))
			                                    {!! $tipobiendetalles->variable->valores->valor !!}
                                    @endif
											</td>	
			                        </tr>
	
	                            @endforeach

	                    @endforeach

				        </tbody>
				    </table>


                </div>
            </div>


            <div class="row justify-content-center  mt-3">

                <div class="col-md-8 text-justify">
                    
                   El colaborador se compromete en darle el uso correcto y administrarlo de la mejor manera, siendo así el responsable de estas:

                  		<br>
                  		<br>

                  	<ul>
                  		<li>
                   			Usar y manejar con cuidado las herramientas que se pongan a su disposición para ejercer su trabajo, así como devolver aquellos que se les haya proporcionado en el estado en que recibieron, tomando en cuenta el desgaste que sufrieron por el uso normal de los mismos en la forma acostumbrada.
                  		</li>
                  		<br>

                  		<li>
                   			NO utilizar los teléfonos de la entidad LOGISTICA Y SERVICIOS DE DISTRIBUCIÓN, S.A.  para realizar llamadas privadas o personales, salvo que cuente con  la autorización del jefe inmediato.
                  		</li>
                  	</ul>

                </div>
            </div>

            <div class="row justify-content-center  mt-3">
                <div class="col-md-8 text-justify"> 
                
					En caso de robo o extravío se le hará el cobro correspondiente del valor del activo por su reposición.

                </div>
            </div>


</body>
</html>
