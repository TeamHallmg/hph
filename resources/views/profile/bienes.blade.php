<div class="container-fluid">
    <h5>BIENES</h5>

    <div class="form-row">
        <div class="form-group col-3">
            <div class="row">
                <div class="form-group col text-center">
                    @if (isset($profile) && !is_null($profile->image))
                        <img src="{{ asset('uploads/profile/'.$profile->image) }}" class="mb-3" style="max-width: 270px; max-height: 183px">
                    @else
                        <img src="{{ asset('img/vacantes/sinimagen.png') }}" class="mb-3" style="max-width: 300px; max-height: 183px">
                    @endif
                </div>
            </div>
        </div>
        <div class="col-9">
            <div class="row">
                <div class="form-group col"></div>
                
                <div class="form-group col"></div>

                <div class="form-group col">
                    <label for="" class="requerido">Número de empleado</label>
                    <input type="text" name="" id="" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->user) ? !is_null($profile->user->employee) ? $profile->user->employee->idempleado : null : null : null }}" disabled="{{ is_null($user) ? false : true }}">
                </div>
            </div>
            <div class="row">
                <div class="form-group col">
                    <label for="name" class="requerido">Nombre</label>
                    <input type="text" name="name" id="name" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->user) ? !is_null($profile->user->employee) ? $profile->user->employee->nombre : null : $user->first_name : null }}" disabled={{ is_null($user) ? false : true }}>
                </div>
                
                <div class="form-group col">
                    <label for="surname_father" class="requerido">Apellido Paterno</label>
                    <input type="text" name="surname_father" id="surname_father" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->user) ? !is_null($profile->user->employee) ? $profile->user->employee->paterno : null : $user->last_name : null }}" disabled={{ is_null($user) ? false : true }}>
                </div>

                <div class="form-group col">
                    <label for="surname_mother" class="requerido">Apellido Materno</label>
                    <input type="text" name="surname_mother" id="surname_mother" class="form-control" value="{{ !is_null($profile) ? !is_null($profile->user) ? !is_null($profile->user->employee) ? $profile->user->employee->materno : null : null : null }}" disabled={{ is_null($user) ? false : true }}>
                </div>
            </div>

            <div class="row">
                <div class="form-group col"></div>
                
                <div class="form-group col"></div>

                <div class="form-group col">
                    <label for="">Puesto</label><br>
{{-- 
                    @if(!is_null($profile))

                        <a href="{{ url('jobPosition/' .  $profile->user->employee->jobPosition->id . '/edit') }}"> {{ $profile->user->employee->jobPosition->name }} </a>
                     @else
                        <a href="{{ url('jobPosition/' .  $user->employee->jobPosition->id . '/edit') }}"> {{ $user->employee->jobPosition->name }} </a>
                     @endif --}}

                 
                </div>
            </div>
            <div class="row col"></div>
        </div>
    </div>
    <br>
   
    <div class="row">
        <div class="col-md-8">
            <h3 class="font-weight-bold">Bienes Asignados</h3>
        </div>
        <div class="col-md-4" style="text-align: right;">
            

            <a class="btn morado" data-toggle="modal" data-target="#nuevo-bien-modal">
                Asignar Bien
            </a>
         

        </div>
    </div>
    <hr style="border: 2px solid #000000;">

    <table class="table table-striped table-bordered nowrap" id="tableBienes">
        <thead class="bg-red text-white">
            <tr>
                <th class="bg-red">Código</th>
                <th class="bg-red">Bien</th>
                <th class="bg-red">Entrega</th>
                <th class="bg-red">Estatus</th>
                <th class="bg-red">Opciones</th>
                {{-- <th>ruta</th> --}}
            </tr>
        </thead>
        <tbody>
            @if(!is_null($profile))

                    @if (count($profile->bienes) == 0)
                    <tr>
                        <td colspan="5" align="center">
                            No tienes bienes Asignados
                        </td>
                    </tr>
                @endif


                @foreach($profile->bienes as $bien)
                    <tr>
                        <td> 
                            <a href="#" data-toggle="modal" data-target="#ver-bien-modal" class="verDetalles" data-id="{{ $bien->id }}" data-codigo="{{ $bien->codigo }}" data-bien="{{ $bien->tipobien->nombre }}" data-entrega="{{ Carbon\Carbon::parse($bien->created_at)->format('d-m-Y') }}"> {{ $bien->codigo }} </a>
                        </td>
                        <td>{{ $bien->tipobien->nombre }}</td>
                        <td>{{ Carbon\Carbon::parse($bien->created_at)->format('d-m-Y') }}</td>
                        <td>
                            @if($bien->estatus == 1)
                                <span style="color: green;">Entregado</span>
                            @endif
                        </td>
                        <td>
                            <a href="{{url('NotaEntregaBien/'.$profile->id.'/'.$bien->id)}}" target="_blank" class="btn btn-sm btn-primary">Pdf</a>

                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>

    <div class="row">
        <div class="col text-right">
            <button type="submit" class="btn morado">{{ is_null($profileRegistros) ? 'Guardar' : 'Actualizar' }}</button>
        </div>
    </div>
</div>

@if(!is_null($profile) && isset($profile->user->employee))
{{-- modal para agregar solicitud de empleo --}}
<div class="modal fade" id="nuevo-bien-modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Asignar Bien</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ url('updateBienes') }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
                <input type="hidden" name="profile_id" id="profile_id" value="{{ isset($profile) ? $profile->id : null }}">
                {{-- <input type="hidden" name="user_id" id="user_id" value="{{ !is_null($user) ? $user->id : null }}"> --}}
                
                <div class="modal-body">
                   <div class="row">
                        <div class="col-12 col-md-12">
                            <div class="form-group col-12 col-md-12">
                                <label class="an-title font-weight-bold" for="type">Tipo de Bienes Disponibles</label>
                                <select id="userB" class="form-control" name="tipoBien" required>
                                        <option value="">Elije un tipo Bien...</option>
                                        @foreach ($profile->user->employee->jobPosition->Jobtipobienes as $Jobtipobienes)
                                            <option value="{{$Jobtipobienes->tipobien->id}}">{{ $Jobtipobienes->tipobien->nombre }}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                    </div>


                   <div class="row" id="code" style="display: none">
                        <div class="col-12 col-md-12">
                            <div class="form-group col-12 col-md-12">
                                <label class="an-title font-weight-bold" for="codigo">Código Del Bien</label>
                                <input type="text" name="codigo" id="codigo" class="form-control" >
                            </div>
                        </div>
                    </div>

                    
                    @foreach ($profile->user->employee->jobPosition->Jobtipobienes as $Jobtipobienes)

                        <div class="row">

                            @foreach ($Jobtipobienes->tipobien->tipobiendetalles as $tipobiendetalles)

                                <div class="form-group dataBienes col-md-6 {{ $Jobtipobienes->tipobien->id }}" style="display: none">

                                    <label for="{{ $Jobtipobienes->tipobien->id }}{{ $tipobiendetalles->variable->id }}" class="an-title font-weight-bold"> {{ $tipobiendetalles->variable->etiqueta }}</label>

                                    <input type="text" name="bienesDatos[{{ $Jobtipobienes->tipobien->id }}][{{ $tipobiendetalles->variable->id }}][]" id="{{ $Jobtipobienes->tipobien->id }}{{ $tipobiendetalles->variable->id }}" class="form-control">

                                </div>

                            @endforeach

                        </div>

                    @endforeach

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-success">Aceptar</button>
                </div>

            </form>
        </div>
    </div>
</div>

{{-- modal para agregar solicitud de empleo --}}
<div class="modal fade" id="ver-bien-modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Detalles Bien</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body">

                   <div class="row">
                        <div class="col-12 col-md-12">
                            <div class="form-group col-12 col-md-12">
                                <label class="an-title font-weight-bold" for="codigo">Nombre Del Bien</label>
                                <input type="text" name="codigo" id="ver-bien" class="form-control InputDetallesBienes">
                            </div>
                        </div>
                        <div class="col-6 col-md-6">
                            <div class="form-group col-12 col-md-12">
                                <label class="an-title font-weight-bold" for="codigo">Código Del Bien</label>
                                <input type="text" name="codigo" id="ver-codigo" class="form-control InputDetallesBienes" >
                            </div>
                        </div>
                        <div class="col-6 col-md-6">
                            <div class="form-group col-12 col-md-12">
                                <label class="an-title font-weight-bold" for="codigo">Fecha Entrega Del Bien</label>
                                <input type="text" name="codigo" id="ver-entrega" class="form-control InputDetallesBienes" >
                            </div>
                        </div>
                    </div>

                    @foreach($profile->bienes as $bien)

                        <div class="row">

                            @foreach ($bien->tipobien->tipobiendetalles as $tipobiendetalles)

                                <div class="form-group dataBienes col-md-6 dataDetallesBienes {{ $bien->id }}" style="display: none">

                                    <label class="an-title font-weight-bold"> {{ $tipobiendetalles->variable->etiqueta }}</label>
                                    @if(isset($tipobiendetalles->variable->valores))
                                    <input type="text" name="" id="" value="{{ $tipobiendetalles->variable->valores->valor }}" class="form-control InputDetallesBienes">
                                    @endif
                                </div>

                            @endforeach

                        </div>

                    @endforeach

                </div>

                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-success">Aceptar</button>
                </div>

        </div>
    </div>
</div>
@endif