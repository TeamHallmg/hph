@extends('vacantes.app')
@section('content')

<div class="container-fluid">
	<div class="flash-message" id="mensaje">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-'.$msg))
                <p class="alert alert-{{ $msg }}">{!! Session::get('alert-'.$msg) !!} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <br>
	<div class="form-row">
		<div class="col-md-12">
			<h4><strong style="color: #ED3237">CURRICULUM VITAE PARA EXTERNO</strong><hr class="mt-1" style="border-bottom: 4px solid #373435;"></h4>
		</div>
	</div>

	<div class="form-row my-4">
		<div class="col-md-12">
			<h4><strong style="color: #ED3237;">Datos Generales</strong><hr class="mt-1" style="border-bottom: 2px solid #373435;"></h4>
		</div>
	</div>
	<form action="{{ url('createComplementaria') }}" method="POST" enctype="multipart/form-data">
		@csrf

		<input type="hidden" name="external" value="true">
		<input type="hidden" name="idVi" value="{{ $idVi }}">
		<input type="hidden" name="user_id" value="0">
		<div class="form-row">
			<div class="col-12 col-md-3 text-center">
				<div class="photo-profile-container" 
					style="
					background-image:url({{ Auth::user()->getProfileImage() }});
					background-size:cover;
					background-position:center;
					">
				</div>
				{{--@if (isset($profile) && !is_null($profile->image))
					<img src="{{ asset('uploads/profile/'.$profile->image) }}" class="mb-3" style="max-width: 270px; max-height: 183px">
				@else
					<img src="{{ asset('img/vacantes/sinimagen.png') }}" class="mb-3" style="max-width: 300px; max-height: 183px">
				@endif--}}
				<div class="custom-file form-group col">                
					<input type="file" name="file[image]" class="form-control custom-file-input" id="image" aria-describedby="inputGroupFileAddon01">
					<label class="custom-file-label" for="image" data-browse="Subir Fotografía"></label>
					<small><strong>2MB max *</strong></small>
				</div>
			</div>
			<div class="col-12 col-md-3 d-flex flex-column align-content-start justify-content-start">
				<label for="name" class="requerido">Nombre(s)</label>
				<input class="form-control mb-3" type="text" name="name" id="name" required>

				<label for="cellphone" class="requerido">Número de Celular</label>
				<input class="form-control mb-3" type="text" name="cellphone" id="cellphone" required>

				<label for="gender">Sexo:</label>
				{{-- <input class="form-control mb-3" type="text" name="gender" id="gender"> --}}
				<select class="custom-select" name="gender">
					<option value="F">F</option>
					<option value="M">M</option>
				</select>
			</div>
			<div class="col-12 col-md-3 d-flex flex-column align-content-start justify-content-start">
				<label for="surname_father" class="requerido">Apellido Paterno</label>
				<input class="form-control mb-3" type="text" name="surname_father" id="surname_father" required>
				
				<label for="phone">Número de Teléfono Fijo</label>
				<input class="form-control mb-3" type="text" name="phone" id="phone">

				<label for="date_birth">Fecha de Nacimiento</label>
				<input class="form-control mb-3" type="date" name="date_birth" id="date_birth">
			</div>
			<div class="col-12 col-md-3  d-flex flex-column align-items-start justify-content-start">

				<label for="surname_mother" class="requerido">Apellido Materno</label>
				<input class="form-control mb-3" type="text" name="surname_mother" id="surname_mother" required>

				<label for="lname" class="requerido">Correo Eléctronico</label>
				<input class="form-control mb-3" type="text" name="email" id="email" required>

				<label for="rfc">DPI</label>
				<input class="form-control mb-3" type="text" name="rfc" id="rfc">

			</div>

			<div class="col-12 col-md-3 offset-md-3  d-flex flex-column align-items-start justify-content-start">
				<label for="age">Edad</label>
				<input type="text" id="age" class="form-control mb-3" readonly>
			</div>
			<div class="col-12 col-md-3  d-flex flex-column align-items-start justify-content-start">
				<label for="igss">IGSS</label>
				<input type="text" name="igss" id="igss" class="form-control mb-3">
			</div>
			<div class="col-12 col-md-3  d-flex flex-column align-items-start justify-content-start">
				<label for="irtra">IRTRA</label>
				<input type="text" name="irtra" id="irtra" class="form-control mb-3">
			</div>
		</div>

		<div class="form-row mb-4 my-4">
            <div class="col-md-12">
                <h4><strong style="color: #ED3237;">Documento CV</strong><hr class="mt-1" style="border-bottom: 2px solid #373435;"></h4>
            </div>
		</div>
		
		<div class="form-row">
            <div class="col-12 mb-3 col-md-6 pull-right">
                <label for="files_cv">Adjuntar CV</label>
                <div class="custom-file col">
                    <input class="form-control-file custom-file-input" type="file" name="file[file_cv]" id="files_cv">
                    <label class="custom-file-label" for="files_cv" data-browse="Subir documento"></label>
                </div>
            </div>
        </div>
		
		<div class="form-row my-4">
			<div class="col-md-12">
				<h4><strong style="color: #ED3237;">Domicilio Actual</strong><hr class="mt-1" style="border-bottom: 2px solid #373435;"></h4>
			</div>
		</div>

		<div class="form-row">
			<div class="col-12 mb-3 col-md-6">
				<label for="address">Calle</label>
				<input class="form-control mb-3" type="text" name="address" id="address_c">
			</div>
			<div class="col-12 mb-3 col-md-2">
				<label for="num_outside">No. Casa</label>
				<input class="form-control mb-3" type="text" name="num_outside" id="num_outside_c">
			</div>
			<div class="col-12 mb-3 col-md-2">
				<label for="num_inside">Zona</label>
				<input class="form-control mb-3" type="text" name="num_inside" id="num_inside_c">
			</div>
			<div class="col-12 mb-3 col-md-2">
				<label for="colony">Colonia</label>
				<input class="form-control mb-3" type="text" name="colony" id="colony_c">
			</div>
		</div>
			
		<div class="form-row">
			<div class="col-12 mb-3 col-md-3">
				<label for="zip_code">Código Postal</label>
				<input class="form-control mb-3" type="text" name="zip_code" id="zip_code_c">
			</div>

			<div class="col-12 mb-3 col-md-3">
				<label for="state">Departamento</label>
				<select name="state" id="state_c" class="selectpicker form-control mb-3" data-live-search="true">
					@foreach ($estados as $id => $estado)
						<option disabled value="" selected hidden>Debe seleccionar un Departamento...</option>
						<option value="{{ $id }}"> {{ $estado }}</option>
					@endforeach
				</select>
			</div>
			<div class="col-12 mb-3 col-md-3">
				<label for="city">Municipio</label>
				<select class="selectpicker form-control mb-3" name="city" id="city_c" data-live-search="true">
					<option disabled value="" selected hidden>Debe seleccionar un Municipio...</option>
				</select>
			</div>
				
			<div class="col-12 mb-3 col-md-3">
				<label for=""><small>Comprobante de Domicilio</small></label>
				<div class="custom-file col">
					<input class="form-control-file custom-file-input" type="file" name="file_address" id="file_address">
					<label class="custom-file-label" for="file_address" data-browse="Subir documento"></label>
				</div>
			</div>
		</div>

		{{-- Comento esto porque no tiene lógica --}}

		{{-- <div class="form-row my-4">
			<div class="col-md-12">
				<h4><strong style="color: #ED3237;">Estudios</strong><hr class="mt-1" style="border-bottom: 2px solid #373435;"></h4>
			</div>
		</div>

		<div class="float-right">
			<button type="button" class="btn btn-danger mt-4" id="agregar_escolar_c">
				<span class="fa fa-plus"></span>
			</button>
		</div>

			<div id="escolar_c">
				<hr class="separador d-none">
				<div class="form-row">
					<div class="col-12 mb-3 col-md-2">
						<input type="hidden" name="type[]" value="basica">
						<label for="studio">Nivel Alcanzado</label>
						<select name="studio[]" id="studio_c" class="form-control mb-3">
							<option disabled value="" selected hidden>Seleccione una opción...</option>
							<option value="Primaria" class="s-Primaria-c">Primaria</option>
							<option value="Secundaria" class="s-Secundaria-c">Secundaria</option>
							<option value="Preparatoria" class="s-Preparatoria-c">Diversificado</option>
							<option value="Técnico" class="s-Técnico-c">Técnico</option>
						</select>
					</div>
					<div class="col-12 mb-3 col-md-5">
						<label for="school">Institución que Expidió Constancia</label>
						<input type="text" name="school[]" id="school_c" class="form-control mb-3">
					</div>
					<div class="col-12 mb-3 col-md-3">
						<label for="date_end">Fecha Terminación</label>
						<input type="date" name="date_end[]" id="date_end_c" class="form-control mb-3">
					</div> 
					<div class="col-12 mb-3 col-md-6">
						<label for="">Agregar Comprobante del Último Grado de Estudios</label>
						<div class="custom-file col">
							<input class="form-control-file custom-file-input" type="file" name="file_studio[]" id="file_studio_c">
							<label class="custom-file-label" for="file_studio" data-browse="Subir documento"></label>
						</div>
					</div>

					<div class="col-12 mb-3 col-md-6 text-right d-none">
						<button type="button" class="btn btn-danger borrar_escolar_c mt-4">
							<span class="fa fa-minus"></span>
						</button>
					</div>

				</div>
			</div>
		<div id="destino_escolar_c"></div>

		<div class="form-row my-4">
			<div class="col-md-12">
				<h4><strong style="color: #ED3237;">Universitarios</strong><hr class="mt-1" style="border-bottom: 2px solid #373435;"></h4>
			</div>
		</div>

		<div class="float-right">
			<button type="button" class="btn btn-danger mt-4" id="agregar_escolar_uni_c">
				<span class="fa fa-plus"></span>
			</button>
		</div>

			<div id="escolar_uni_c">
				<hr class="separador d-none">
				<div class="form-row">
					<div class="col-12 mb-3 col-md-3">
						<input type="hidden" name="type_uni[]" value="superior">
						<label for="studio_uni_c">Tipo de carrera:</label>
						<select name="studio_uni[]" id="studio_uni_c" class="form-control">
							<option disabled value="" selected hidden>Seleccione una opción...</option>
							<option value="Licenciatura" class="s-Licenciatura-c">Licenciatura</option>
							<option value="Maestria" class="s-Maestria-c">Maestria</option>
							<option value="Doctorado" class="s-Doctorado-c">Doctorado/Posgrado</option>
						</select>
					</div>
					<div class="col-12 mb-3 col-md-6">
						<label for="career">Carrera:</label>
						<input class="form-control mb-3" type="text" name="career_uni[]" id="career_uni_c">
					</div>
					<div class="col-12 mb-3 col-md-1 mt-4 text-right d-none">
						<button type="button" class="btn btn-danger borrar_escolar_uni_c">
							<span class="fa fa-minus"></span>
						</button>
					</div>
					<div class="col-12 mb-3 col-md-6">
						<label for="">Agregar comprobante de estudios:</label>
						<div class="custom-file col">
							<input class="form-control-file custom-file-input" type="file" name="file_studio_uni[]" id="file_studio_uni">
							<label class="custom-file-label" for="file_studio_uni" data-browse="Subir documento"></label>
						</div>
					</div>
				</div>
			</div>

		<div id="destino_escolar_uni_c"></div>

		<div class="form-row my-4">
			<div class="col-md-12">
				<h4><strong style="color: #ED3237;">Otros Conocimientos</strong><hr class="mt-1" style="border-bottom: 2px solid #373435;"></h4>
			</div>
		</div>

		<div class="float-right">
			<button type="button" class="btn btn-danger mt-4" id="agregar_language_c">
				<span class="fa fa-plus"></span>
			</button>
		</div>

			<div id="language_c">
				<hr class="separador d-none">
				<div class="form-row">
					<div class="col-2 col-md-12">
						Idioma
					</div>

					<div class="col-12 col-md">
						<div class="form-group">
							<label for="reading" class="mr-2 font-weight-bold">A</label>
							<input type="text" name="language[]" id="language_c" class="form-control">
						</div>
					</div>
					
					<div class="col-12 col-md">
						<div class="form-group">
							<label for="reading" class="mr-2 font-weight-bold">B</label>
							<div class="input-group col-md-12 border-0 input-group-text bg-check mr-0">
								<span>Lectura</span>
								<input type="number" min="1" max="100" name="reading[]" id="reading_c" class="form-control form-control-sm">
							</div>
						</div>
					</div>

					<div class="col-12 col-md">
						<div class="form-group">
							<label for="writing_c" class="mr-2 font-weight-bold">C</label>
							<div class="input-group col-md-12 border-0 input-group-text bg-check">
								<span>Escritura</span>
								<input type="number" min="1" max="100" name="writing[]" id="writing_c" class="form-control form-control-sm">
							</div>
						</div>
					</div>

					<div class="col-12 col-md">
						<div class="form-group">
							<label for="writing_c" class="mr-2 font-weight-bold">D</label>
							<div class="input-group col-md-12 border-0 input-group-text bg-check">
								<span>Conversación</span>
								<input type="number" min="1" max="100" name="spoken[]" id="spoken_c" class="form-control form-control-sm">
							</div>
						</div>
					</div>

					<div class="col-12 col-md-1 mt-4 text-right d-none">
						<button type="button" class="btn btn-danger borrar_language_c">
							<span class="fa fa-minus"></span>
						</button>
					</div>

				</div>
			</div>

		<div id="destino_language_c"></div>


		<div class="float-right">
			<button type="button" class="btn btn-danger mt-4" id="agregar_knowledge_c">
				<span class="fa fa-plus"></span>
			</button>
		</div>

			<div id="escolar_knowledge_c">
				<hr class="separador d-none">
				<div class="form-row">
					<div class="col-12 col-md-11">
						<label for="knowledge_type">Competencias Desarrolladas</label>
						<select name="knowledge_type[]" id="knowledge_type_c" class="form-control">
							<option disabled value="" selected hidden>Selecciona una opción...</option>
							<option value="Maquinaria">Maquinaria y equipo</option>
							<option value="Programas">Programas y sistemas</option>
							<option value="Funciones">Funciones de oficina</option>
						</select>
					</div>
					
					<div class="col-12 mb-3 col-md-8">
						<label for="knowledge_name">Detalle:</label>     
						<input type="text" name="knowledge_name[]" id="knowledge_name_c" class="form-control">
					</div>
		
					<div class="col-12 mb-3 col-md-1 mt-4 text-right d-none">
						<button type="button" class="btn btn-danger borrar_knowledge_c">
							<span class="fa fa-minus"></span>
						</button>
					</div>
				</div>
			</div>

		<div id="destino_knowledge_c"></div>

		<div class="form-row my-4">
			<div class="col-md-12">
				<h4><strong style="color: #ED3237;">Experiencia Laboral</strong><hr class="mt-1" style="border-bottom: 2px solid #373435;"></h4>
			</div>
		</div>

		<div class="float-right">
			<button type="button" class="btn btn-danger mt-4" id="agregar_experience_c">
				<span class="fa fa-plus"></span>
			</button>
		</div>

			<div id="escolar_experience_c">
				<hr class="separador d-none">
				<div class="form-row">
					<div class="col-12 mb-3 col-md-5">
						<label for="job">Puesto Ocupado</label>
						<input class="form-control mb-3" type="text" name="job[]" id="job_c">
					</div>
					<div class="col-12 mb-3 col-md-6">
						<label for="company">Empresa/Domicilio</label>
						<input class="form-control mb-3" type="text" name="company[]" id="company_c">
					</div>
					<div class="col-12 mb-3 col-md-1 mt-4 text-right d-none">
						<button type="button" class="btn btn-danger borrar_experience_c">
							<span class="fa fa-minus"></span>
						</button>
					</div>
				</div>
				<div class="form-row">
					<div class="col-12 mb-3 col-md-3">
						<label for="date_begin_experience">Fecha Inicio</label>
						<input class="form-control mb-3" type="date" name="date_begin_experience[]" id="date_begin_experience_c">
					</div>
					<div class="col-12 mb-3 col-md-3">
						<label for="date_end_experience">Fecha Fin</label>
						<input class="form-control mb-3" type="date" name="date_end_experience[]" id="date_end_experience_c">
					</div>
					<div class="col-12 mb-3 col-md-3">
						<label for="salary">Sueldo</label>
						<input class="form-control mb-3" type="text" name="salary[]" id="salary_c">
					</div>
					<div class="col-12 mb-3 col-md-3">
						<label for="reason_separation">Motivo de Separación</label>
						<input class="form-control mb-3" type="text" name="reason_separation[]" id="reason_separation_c">
					</div>
				</div>
				<div class="form-row">
					<div class="col-12 mb-3 col-md-12">
						<label for="activity">Principales actividades y Responsabilidades del Puesto</label>
						<textarea class="form-control mb-3" name="activity[]" id="activity_c" rows="4"></textarea>
					</div>
				</div>
			</div>

		<div id="destino_experience_c"></div>

		<div class="form-row">
			<div class="col-12 mb-3 col-md-12">
				<p class="text-white p-3 bg-gray">Anota al menos dos últimos empleos</p>
			</div>
		</div>

		<div class="form-row mb-4">
			<div class="col-md-12">
				<h4><strong style="color: #ED3237;">Referencias</strong><hr class="mt-1" style="border-bottom: 2px solid #373435;"></h4>
			</div>
		</div>

		<div class="float-right">
			<button type="button" class="btn btn-danger mt-4" id="agregar_reference_c">
				<span class="fa fa-plus"></span>
			</button>
		</div>

			<div id="escolar_reference_c">
				<hr class="separador d-none">
				<div class="form-row">

                    <div class="col-12 mb-3 col-md-3">

                            <label for="type">Referecia</label>
                            <select name="type_ref[]" id="type_ref" class="form-control mb-3">
                                <option value="Personal">Personal</option>
                              <option value="Laboral">Laboral</option>
                             
                            </select>

                        </div>

                        <div class="col-12 mb-3 col-md-9 mt-4 text-right pull-right">
                            <button type="button" class="btn btn-danger borrar_reference_c">
                                <span class="fa fa-minus"></span>
                            </button>
                        </div>

                    </div>

                <div class="form-row">

					<div class="col-12 mb-3 col-md-3">
						<label for="reference_name">Nombre:</label>
						<input class="form-control mb-3" type="text" name="reference_name[]" id="reference_name_c">
					</div>
					<div class="col-12 mb-3 col-md-3">
						<label for="reference_phone">Teléfono:</label>
						<input class="form-control mb-3" type="text" name="reference_phone[]" id="reference_phone_c">
					</div>
					<div class="col-12 mb-3 col-md-3">
						<label for="reference_time_meet">Tiempo de conocerla:</label>
						<input class="form-control mb-3" type="text" name="reference_time_meet[]" id="reference_time_meet_c">
					</div>
					<div class="col-12 mb-3 col-md-3">
						<label for="reference_occupation">Ocupación:</label>
						<input class="form-control mb-3" type="text" name="reference_occupation[]" id="reference_occupation_c">
					</div>
				</div>
			</div>
		
		<div id="destino_reference_c"></div>

		<div class="form-row">
			<div class="col-12 mb-3 col-md-12">
				<p class="text-white p-3 bg-gray">Anota al menos dos referencias</p>
			</div>
		</div> --}}

		<div class="form-row">
			<div class="col-md-12">
				<h6>
					<strong style="color: #ED3237">Ratifico que todos los datos asentados son verídicos y que, en caso de probarse lo contrario, se cancelará el registro.</strong>
					<hr class="mt-1" style="border-bottom: 4px solid #373435;"></h6>
			</div>
		</div>
		
		<div class="row">
			<div class="col text-right">
				<button type="submit" class="btn btn-success">Guardar</button>
			</div>
		</div>
	</form>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(function() {
		$("#state_c").on('change', function(e) {
			e.preventDefault();
			var id = $(this).val();
			$.ajax ({
				type: 'GET',
				url: "{{ url('listStates') }}/"+id,
				dataType: 'json',
				success: function(response) {
					$('#city_c').empty();
					$.each(response, function(key, element) {
						$('#city_c').append("<option value='" + element.id + "'>" + element.nombre + "</option>").selectpicker('refresh');
					});
				}
			});
		});

		$('#date_birth').on('change', calcularEdad);
	});

	function calcularEdad() {
		fecha = $(this).val();
		var hoy = new Date();
		var cumpleanos = new Date(fecha);
		var edad = hoy.getFullYear() - cumpleanos.getFullYear();
		var m = hoy.getMonth() - cumpleanos.getMonth();

		if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
			edad--;
		}
		if(edad <= 0) {
			alert('Fecha no valida');
			$('#date_birth').val('');
		} else {
			$('#age').val(edad);
		}
	}
        
        
</script>
@endsection