@extends('layouts.app')

@section('sidebar')
    @include('profile.sidebar')
@endsection

@section('mainScripts')
    @yield('scripts')
@endsection