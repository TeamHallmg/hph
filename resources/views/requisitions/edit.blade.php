{{-- @extends('layouts.app')
@section('content') --}}
@extends('requisitions.app')
@section('content')

<div class="container-fluid">
	<div class="d-flex justify-content-center">
  		<h3>EDITAR REQUISICIÓN</h3>
	</div>
	<hr style="border: 1px solid #3E4095;">

	<form action="{{ route('requisitions.update', $requisition->id) }}" method="post">
		@csrf
		@method('PUT')

		<div class="row">
			<div class="col-md-3">
				<label for="fecha_requerida" class="requerido">Fecha requerida</label>
				<input type="date" value="{{ $requisition->fecha_requerida }}" name="fecha_requerida" class='form-control' min={{ date('Y-m-d') }} max={{ date('Y-m-d',strtotime('+5years')) }} required />
			</div>
		
			<div class="col-md-3">
				<label for="fecha_elaborada">Fecha elaboración</label>
				<input type="text" value="{{ $requisition->fecha_elaborada }}" name="fecha_elaborada" value={{ date('Y-m-d') }} class="form-control" readonly />
			</div>
		
			<div class="col-md-3">
				<label for="estatus_requi">Estatus requisición</label>
				<input type="text" value="{{ $requisition->estatus_requi }}" name="estatus_requi" {{--value="SOLICITADO"--}} class="form-control" maxlength="45" readonly />
			</div>
		

			<div class="col-md-3">
				<label for="disponibilidad">Disponibilidad:</label>
				<input type="text" name="disponibilidad" id="disponibilidad" value="{{ $requisition->disponibilidad }}" class="form-control">
			</div>

			{{-- <div class="col-md-3">
				<label for="confidencial">Confidencial</label><br>
				<input type="checkbox" name="confidencial" id="confidencial" value="1"/>
			</div>	 --}}
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label for="tipo_vacante" class="requerido">Tipo de vacante</label>
				<select name="tipo_vacante" id="tipo_vacante" class="form-control" required>
					<option disabled value="" selected hidden>Seleccione una opción...</option>
					<option value="SUSTITUIR">Sustituir</option>
					<option value="INCREMENTAR">Incrementar Puesto Existente</option>
					<option value="CREAR">Nuevo Puesto</option>
				</select>
			</div>
		
			<div class="col-md-3">
				<label for="cantidad">Número de plazas a autorizar:</label>
				<input type="number" value="{{ $requisition->cantidad }}" name="cantidad" class="form-control", min="1" max="30" />
			</div>
		
			<div class="col-md-3">
				<label for="area">Departamento:</label>
				<select name="area" id="area" class="form-control">
					<option value="" disabled hidden selected>Selecciona un Departamento</option>
					@foreach ($departments as $id => $department)
						<option value="{{ $id }}" {{ ($id==$requisition->area) ? 'selected' : '' }}> {{ $department }}</option>
					@endforeach
				</select>
			</div>


			<div class="col-md-3">
				<label for="puesto">Puesto:</label>
				{{-- <input type="text" value="{{ $requisition->puesto }}" name="puesto" id="puesto" class="form-control" maxlength="60" /> --}}
				<select name="puesto" id="puesto" class="form-control">
					@foreach ($jobPosition as $id => $job)
						<option value="{{ $id }}" {{ ($id==$requisition->puesto_id) ? 'selected' : '' }}> {{ $job }}</option>
					@endforeach
				</select>
			</div>
		</div>
		<br>

		<div class="row" id="nuevo_puesto">
			<div class="col-md-6">
				<label for="puesto">Puesto:</label>
				<input type="text" name="puesto_nuevo" id="puesto_nuevo" class="form-control" maxlength="60" />
			</div>
			<br>
		</div>

		<div class="row">
			<div class="col-md-12">
				<label for="puesto_descripcion">Descripción del puesto:</label>
				<textarea name="puesto_descripcion" id="puesto_descripcion" style="resize: none;" cols="30" rows="5" maxlength="1000" class="form-control">{{ $requisition->puesto_descripcion }}</textarea>
			</div>
		</div>
		<br>		

		<div class="row">
			<div class="col-md-3">
				<label for="horario">Lunes a Viernes</label><br>
				<input type="radio" name="horario" value="0" />
			</div>

			<div class="col-md-3">
				<label for="horario">Lunes a Sábado</label><br>
				<input type="radio" name="horario" value="1" />
			</div>

			<div class="col-md-6">
				<label for="horario_descripcion">Horario descripción:</label>
				<input type="text" value="{{ $requisition->horario_descripcion }}" name="horario_descripcion" class="form-control" maxlength="1000" />
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label for="edad_min">Edad mínima (años):</label>
				<input type="number" value="{{ $requisition->edad_min }}" name="edad_min" id="edad_min" class="form-control" min="18" max="99" />
			</div>

			<div class="col-md-3">
				<label for="edad_max">Edad máxima (años):</label>
				<input type="number" value="{{ $requisition->edad_max }}" name="edad_max" id="edad_max" class="form-control" min="18" max="99" />
				<span id="edad_max_error" class="clase_error"><strong>Mayor o igual a edad mínima</strong></span>
			</div>

			<div class="col-md-3">
				<label for="edo_civil">Estado civil:</label>
				<select name="edo_civil" id="edo_civil" class="form-control">
					<option disabled value="" selected hidden>Seleccione una opción...</option>
					<option value="SOLTERO">Soltero</option>
					<option value="CASADO">Casado</option>
					<option value="VIUDO">Viudo</option>
					<option value="DIVORCIADO">Divorciado</option>
					<option value="INDISTINTO">Indistinto</option>
				</select>
			</div>
			
			<div class="col-md-3">
				<label for="sexo">Sexo:</label>
				<select name="sexo" id="sexo" class="form-control">
					<option disabled value="" selected hidden>Seleccione una opción...</option>
					<option value="FEMENINO">Femenino</option>
					<option value="MASCULINO">Masculino</option>
					<option value="INDISTINTO">Indistinto</option>
				</select>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-12">
				<label for="objetivo">Objetivo:</label>
				<textarea name="objetivo" class="form-control" style="resize:none;" maxlength="1000" cols="30" rows="5">{{ $requisition->objetivo }}</textarea>
			</div>
		</div>
		<br>
{{-- 
		<div class="row">
			<div class="col-md-12">
				<label for="rolesyresponsabilidades">Roles y responsabilidades:</label>
				<textarea name="rolesyresponsabilidades" class="form-control" style="resize:none;" maxlength="1000" cols="30" rows="5">{{ $requisition->rolesyresponsabilidades }}</textarea>
			</div>
		</div> --}}
		<input type="hidden" name="rolesyresponsabilidades" value="">
		<br>

		<div class="row">
			<div class="col-md-2">
				<label for="experiencia">Requiere experiencia:</label>
			</div>

			<div class="col-md-1">
				<label for="experiencia">Si</label><br>
				<input type="radio" name="experiencia" id="experiencia_1" value="1" />
			</div>

			<div class="col-md-1">
				<label for="experiencia">No</label><br>
				<input type="radio" name="experiencia" id="experiencia_0" value="0" />
			</div>

			<div class="col-md-2">
				<label for="experiencia_anios">Tiempo de experiencia:</label>
				<input type="text" value="{{ $requisition->experiencia_anios }}" name="experiencia_anios" id="experiencia_anios" class="form-control" maxlength="15" />
			</div>

			<div class="col-md-3">
				<label for="experiencia_grado_escolar">Nivel académico:</label>
				<select name="experiencia_grado_escolar" id="experiencia_grado_escolar" class="form-control">
					<option disabled value="" selected hidden>Seleccione una opción...</option>
					<option value="DOCTORADO">Doctorado</option>
					<option value="MAESTRIA">Maestría</option>
					<option value="LICENCIATURA">Licenciatura</option>
					<option value="DIVERSIFICADO">Diversificado</option>
					<option value="TÉCNICO">Técnico</option>
					<option value="PASANTE">Pasante</option>
					<option value="ESTUDIANTE">Estudiante</option>
					<option value="INDISTINTO">Indistinto</option>
				</select>
			</div>

			<div class="col-md-3">
				<label for="experiencia_especializado">Especialidad:</label>
				<input type="text" value="{{ $requisition->experiencia_especializado }}" name="experiencia_especializado" class="form-control" maxlength="45" />
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-12">
				<label for="experiencia_conocimientos">Conocimientos requeridos</label>
				<textarea name="experiencia_conocimientos" class="form-control" style="resize:none;" cols="30" rows="5" maxlength="1000">{{ $requisition->experiencia_conocimientos }}</textarea>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-12">
				<label for="experiencia_habilidades">Habilidades requeridas:</label>
				<textarea name="experiencia_habilidades" class="form-control" style="resize:none;" cols="30" rows="5" maxlength="1000">{{ $requisition->experiencia_habilidades }}</textarea>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<input type="radio" name="lugar_trabajo" value="0" />
				<label for="lugar_trabajo">Oficina</label>
			</div>

			<div class="col-md-3">
				<input type="radio" name="lugar_trabajo" value="1" />
				<label for="lugar_trabajo">Laboratorio</label>
			</div>

			<div class="col-md-3">
				<input type="radio" name="lugar_trabajo" value="2" />
				<label for="lugar_trabajo">Mostrador (punto de venta)</label>
			</div>

			<div class="col-md-3">
				<input type="radio" name="lugar_trabajo" value="3" />
				<label for="lugar_trabajo">Otro</label>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label>¿Maneja información confidencial?</label>
			</div>

			<div class="col-md-2">
				<label for="informacion_confidencial">Si</label>
				<input type="radio" name="informacion_confidencial" value="1" />
			</div>

			<div class="col-md-2">
				<label for="informacion_confidencial">No</label>
				<input type="radio" name="informacion_confidencial" value="0" />
			</div>

			<div class="col-md-5">
				<label style="background-color: #EEE;">Equipo a manjear (Maquinaria, equipo de oficina, vehículo, etc.)</label>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label>¿Requiere equipo de cómputo?</label>
			</div>

			<div class="col-md-2">
				<label for="pc_requiere">Si</label>
				<input type="radio" name="pc_requiere" value="1" />
			</div>

			<div class="col-md-2">
				<label for="pc_requiere">No</label>
				<input type="radio" name="pc_requiere" value="0" />
			</div>

			<div class="col-md-5">
				<input type="text" name="pc_descripcion" class="form-control" maxlength="300" value="{{ $requisition->pc_descripcion }}">
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label>¿Requiere licencia sistema?</label>
			</div>

			<div class="col-md-2">
				<label for="sap_requiere">Si</label>
				<input type="radio" name="sap_requiere" value="1" />
			</div>

			<div class="col-md-2">
				<label for="sap_requiere">No</label>
				<input type="radio" name="sap_requiere" value="0" />
			</div>

			<div class="col-md-5">
				<input type="text" name="sap_descripcion" class="form-control" maxlength="300" value="{{ $requisition->sap_descripcion }}"/>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label>¿Requiere viajar (con frecuencia)?</label>
			</div>

			<div class="col-md-2">
				<label for="viajar_requiere">Si</label>
				<input type="radio" name="viajar_requiere" value="1" />
			</div>

			<div class="col-md-2">
				<label for="viajar_requiere">No</label>
				<input type="radio" name="viajar_requiere" value="0" />
			</div>

			<div class="col-md-5">
				<input type="text" name="viajar_descripcion" class="form-control" maxlength="300" value="{{ $requisition->viajar_descripcion }}"/>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label>¿Requiere automóvil?</label>
			</div>

			<div class="col-md-2">
				<label for="carro_requiere">Si</label>
				<input type="radio" name="carro_requiere" value="1" />
			</div>

			<div class="col-md-2">
				<label for="carro_requiere">No</label>
				<input type="radio" name="carro_requiere" value="0" />
			</div>

			<div class="col-md-5">
				<input type="text" name="carro_descripcion" class="form-control" maxlength="300" value="{{ $requisition->carro_descripcion }}" />
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label>¿Requiere teléfono?</label>
			</div>

			<div class="col-md-2">
				<label for="tel_requiere">Si</label>
				<input type="radio" name="tel_requiere" value="1" />
			</div>

			<div class="col-md-2">
				<label for="tel_requiere">No</label>
				<input type="radio" name="tel_requiere" value="0" />
			</div>

			<div class="col-md-5">
				<input type="text" name="tel_descripcion" class="form-control" maxlength="300" value="{{ $requisition->tel_descripcion }}"/>
			</div>
		</div>
		<br>
		
		<div class="row">
			<div class="col-md-12">
				<label for="comentarios">Comentarios</label>
				<textarea name="comentarios" class="form-control" cols="30" rows="5" style="resize:none;" maxlength="1000">{{ $requisition->comentarios }}</textarea>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-4">
				<label for="sueldo">Salario Base</label>
				<div class="input-group">
					<div class="input-group-prepend">
						<div class="input-group-text">Q</div>
					</div>
					<input type="number" value="{{ $requisition->sueldo }}" name="sueldo" placeholder="0.00" class="form-control" min="1" max="99999999.99" step="0.01"/>
				</div>
			</div>
			
			<div class="col-md-4">
				<label for="bonif_decret">Bonificación Decreto 78 - 89</label>
				<input type="text" value="{{ $requisition->bonif_decret }}" name="bonif_decret" class="form-control"/>
			</div>
		
			<div class="col-md-4">
				<label for="bonif_produc">Bonificación Productividad</label>
				<input type="text" value="{{ $requisition->bonif_produc }}" name="bonif_produc" class="form-control" />
			</div>
		
		</div>

		<br>
		<div class="row">
			
			<div class="col-md-12">
				<label for="observaciones">Observaciones</label>
				<textarea name="observaciones" class="form-control" cols="30" rows="5" style="resize:none;" maxlength="1000">{{ $requisition->observaciones }}</textarea>
			</div>

		</div>
		<br>
			{{-- <div class="col-md-4">
				<label for="ispt">ISPT</label>
				<input type="number" value="{{ $requisition->ispt }}" name="ispt" placeholder="0.00" class="form-control" min="1" max="99999999.99" step="0.01"/>
			</div>
		
			<div class="col-md-4">
				<label for="imss">IMSS</label>
				<input type="number" value="{{ $requisition->imss }}" name="imss" placeholder="0.00" class="form-control" min="1" max="99999999.99" step="0.01"/>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label for="sd">SD:</label>
				<input type="number" value="{{ $requisition->sd }}" name="sd" placeholder="0.00" class="form-control" min="1" max="99999999.99" step="0.01"/>
			</div>
		
			<div class="col-md-3">
				<label for="sdi">SDI:</label>
				<input type="number" value="{{ $requisition->sdi }}" name="sdi" placeholder="0.00" class="form-control" min="1" max="99999999.99" step="0.01"/>
			</div>
		
			<div class="col-md-3">
				<label for="sueldo_aut">Salario Base autorizado:</label>
				<input type="number" value="{{ $requisition->sueldo_aut }}" name="sueldo_aut" placeholder="0.00" class="form-control" min="1" max="99999999.99" step="0.01"/>
			</div> --}}
		
			{{-- <div class="col-md-8">
				<label for="prestaciones">Prestaciones:</label>
				<input type="text" value="{{ $requisition->prestaciones }}" name="prestaciones" class="form-control" />
			</div> --}}
		</div>
		<br>

		<div class="pull-right">
			<button type="submit" class="btn btn-success" id="btn_enviar">Actualizar</button>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="{{ url('requisitions') }}" class="btn btn-danger">Cancelar</a>
		</div>
	</form>

	<div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					{!! Session::get('flag') !!}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<style type="text/css">
	.clase_error {
		display: none;
		color: red;
	}
</style>
<script type="text/javascript">
	@if (Session::has('flag'))
		$(function() {
			$('#modalError').modal('show');
		});
	@endif

	$(function() {
		// {{-- PARA MOSTRAR UNA VENTANA DE ERROR SINO SE ENCUENTRA EL ARCHIVO --}}
		$("#nuevo_puesto").hide();

		//select según el valor que viene de la informacion capturada en la tabla
		$("#tipo_vacante option[value='{{ $requisition->tipo_vacante }}']").prop('selected', true);
		$("#edo_civil option[value='{{ $requisition->edo_civil }}']").prop('selected', true);
		$("#sexo option[value='{{ $requisition->sexo }}']").prop('selected', true);
		$("#experiencia_grado_escolar option[value='{{ $requisition->experiencia_grado_escolar }}']").prop('selected', true);

		// $('#confidencial').prop('checked', {{ $requisition->confidencial }});

		//radio button según el valor que viene de la informacion capturada en la tabla
		$("input[name=horario][value='{{ $requisition->horario }}']").prop('checked',true);
		$("input[name=experiencia][value='{{ $requisition->experiencia }}']").prop('checked',true);
		$("input[name=lugar_trabajo][value='{{ $requisition->lugar_trabajo }}']").prop('checked',true);
		$("input[name=informacion_confidencial][value='{{ $requisition->informacion_confidencial }}']").prop('checked',true);
		$("input[name=pc_requiere][value='{{ $requisition->pc_requiere }}']").prop('checked',true);
		$("input[name=sap_requiere][value='{{ $requisition->sap_requiere }}']").prop('checked',true);
		$("input[name=viajar_requiere][value='{{ $requisition->viajar_requiere }}']").prop('checked',true);
		$("input[name=carro_requiere][value='{{ $requisition->carro_requiere }}']").prop('checked',true);
		$("input[name=tel_requiere][value='{{ $requisition->tel_requiere }}']").prop('checked',true);
		
		//si tipo vacante es CREAR, Los campos se muestran desactivdos
		var tipoVacante = $("#tipo_vacante").val();
		if (tipoVacante == 'CREAR') {
			$( "#puesto" ).prop( "disabled", true );
			$("input[name=puesto_nuevo]").val('{{ $requisition->puesto_nuevo }}');
			$( "#nuevo_puesto" ).show();
		}

		$("#tipo_vacante").on('change', function(e) {
			e.preventDefault();
			var seleccion = $(this).val();
			if (seleccion == 'CREAR') {
				$( "#puesto_nuevo" ).val('');
				$( "#nuevo_puesto" ).slideDown( "slow", function() {
					$( "#puesto" ).val('');
					$( "#puesto_descripcion" ).val('');
					// $( "#area" ).val('');
					// $( "#puesto" ).prop( "disabled", true );
					// $( "#puesto_descripcion" ).prop( "disabled", true );
					// $( "#area" ).prop( "disabled", true );
				});
			} else {
				$( "#puesto_nuevo" ).val('');
				$( "#nuevo_puesto" ).slideUp( "slow", function() {
					$( "#puesto" ).prop( "disabled", false );
					// $( "#puesto_descripcion" ).prop( "disabled", false );
					// $( "#area" ).prop( "disabled", false );
				});
			}
		});

		$("#edad_max, #edad_min").on('change', function (e) {
			e.preventDefault();
			var edad_min = $("#edad_min").val();
			var edad_max = $("#edad_max").val();
    		if (edad_min > edad_max) {
				$("#edad_max_error").slideDown('slow');
			} else{
				$("#edad_max_error").slideUp('slow');
			}
		});

		//si el valor de experiencia es NO, el campo de texto experiencia_anios desactivado
		var seleccionado = $("#experiencia_0").is(':checked');
		if (seleccionado) {
			$( "#experiencia_anios" ).prop( "disabled", true );
		}

		$("#experiencia_0, #experiencia_1").on('change', function (e) {
			e.preventDefault();
			if ($("#experiencia_0").is(":checked")) {
				$("#experiencia_anios").val('');
				$( "#experiencia_anios" ).prop( "disabled", true );
			} else  {
				$( "#experiencia_anios" ).prop( "disabled", false );
			}
		});

		$('form').submit(function() {
			$("button[type='submit']").prop('disabled',true);
		});
	});
</script>
@endsection