@extends('requisitions.app')
@section('content')

<div class="container-fluid">
	<div class="row mb-5">
		<div class="col-12">
			<h3 class="font-weight-bold text-danger">NUEVA REQUISICIÓN DE PERSONAL</h3>
			<hr style="border: 2px solid #000000;">
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<h4 class="font-weight-bold text-danger">Datos Generales</h4>
			<hr style="border: 1px solid #000000;">
		</div>
		<div class="col-12">
			<form action="{{ url('requisitions') }}" method="POST">
				@csrf
				<div class="form-row">
					<div class="form-group col-md-4 mb-0">
						<label>Tipo de Vacante</label>
					</div>
				</div>
				<div class="form-row mb-md-3 pl-1">
					<div class="form-check col-md-3 form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mb-md-0">
						<label class="form-check-label" for="">Nueva</label>
						<input class="form-check-input" type="radio" name="inlineRadioOptions" id="" value="option1">
					</div>
					<div class="form-check col-md-3 form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mb-md-0">
						<label class="form-check-label" for="">Existente</label>
						<input class="form-check-input" type="radio" name="inlineRadioOptions" id="" value="option1">
					</div>
					<div class="form-group row d-flex align-items-center col mb-4 mb-md-0">
						<label class="m-0 col-2" for="">Motivo:</label>
						<div class="col-10">
							<input type="email" class="form-control" name="" placeholder="">
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="">Nombre de la Vacante</label>
						<input type="email" class="form-control" name="" placeholder="">
					</div>
					<div class="form-group col-md-3">
						<label for="">Número de Vacantes</label>
						<input type="password" class="form-control" name="" placeholder="">
					</div>
					<div class="form-group col-md-3">
						<label for="">Folio</label>
						<input type="text" class="form-control" name="" placeholder="" readonly>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-4">
						<label>Tipo de Contratación</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
					<div class="form-group col-md-4">
						<label>Relación</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
					<div class="form-group col-md-4">
						<label>Jornada de Trabajo</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-4 mb-0">
						<label>Publicación</label>
					</div>
				</div>
				<div class="form-row mb-md-3 pl-1">
					<div class="form-check col-md form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mb-md-0">
						<label class="form-check-label" for="">Internos</label>
						<input class="form-check-input" type="radio" name="" id="" value="option1">
					</div>
					<div class="form-check col-md form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mb-md-0">
						<label class="form-check-label" for="">Externos</label>
						<input class="form-check-input" type="radio" name="" id="" value="option1">
					</div>
					<div class="form-check col-md form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mb-md-0">
						<label class="form-check-label" for="">Ambos</label>
						<input class="form-check-input" type="radio" name="" id="" value="option1">
					</div>
					<div class="form-check col-md form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mb-md-0">
						<label class="form-check-label" for="">Confidencial</label>
						<input class="form-check-input" type="radio" name="" id="" value="option1">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<label>Lugar/Centro de Trabajo</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
				</div>
				<div class="row mt-4">
					<div class="col-12">
						<h4 class="font-weight-bold text-danger">Especificaciones</h4>
						<hr style="border: 1px solid #000000;">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-4">
						<label>Departamento</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
					<div class="form-group col-md-5">
						<label>Jefe Directo</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
					<div class="form-group col-md-3">
						<label>Centro de Costos</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<label for="">Objetivo General</label>
						<textarea class="form-control" id="" rows="4"></textarea>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<label for="">Principales Actividades y Responsabilidades del Puesto</label>
						<textarea class="form-control" id="" rows="4"></textarea>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-10 col-md-11">
						<label>Conocimientos Específicos</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
					<div class="form-group col col-md-1 d-flex justify-content-end align-items-end">
						<button class="btn btn-danger"><i class="fas fa-plus"></i></button>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-10 col-md-11">
						<label>Competencias Requeridas</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
					<div class="form-group col col-md-1 d-flex justify-content-end align-items-end">
						<button class="btn btn-danger"><i class="fas fa-plus"></i></button>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-10 col-md-11">
						<label>Manejo de Herramientas, Software, ERP's, otro</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
					<div class="form-group col col-md-1 d-flex justify-content-end align-items-end">
						<button class="btn btn-danger"><i class="fas fa-plus"></i></button>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-10 col-md-6">
						<label>Nivel de Estudios Requeridos en el Puesto</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
					<div class="form-group col-md-5">
						<label for="">Años de Experiencia</label>
						<input type="text" class="form-control" name="" placeholder="">
					</div>
					<div class="form-group col col-md-1 d-flex justify-content-end align-items-end">
						<button class="btn btn-danger"><i class="fas fa-plus"></i></button>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-10 col-md-5">
						<label>Idiomas</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
					<div class="form-group col-md-2">
						<label for="">Lectura</label>
						<input type="text" class="form-control" name="" placeholder="Porcentaje">
					</div>
					<div class="form-group col-md-2">
						<label for="">Escritura</label>
						<input type="text" class="form-control" name="" placeholder="Porcentaje">
					</div>
					<div class="form-group col-md-2">
						<label for="">Conversación</label>
						<input type="text" class="form-control" name="" placeholder="Porcentaje">
					</div>
					<div class="form-group col col-md-1 d-flex justify-content-end align-items-end">
						<button class="btn btn-danger"><i class="fas fa-plus"></i></button>
					</div>
				</div>
				<div class="row">
					<div class="col-12 mt-4">
						<h4 class="font-weight-bold text-danger">Equipo requerido para desempeñar el puesto</h4>
						<hr style="border: 1px solid #000000;">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-10 col-md-11">
						<label></label>
						<select class="form-control">
							<option>Default select</option>
							<option>Equipo de Cómputo</option>
							<option>Celular</option>
							<option>Licencia SAP</option>
							<option>Correo Empresa</option>
						</select>
					</div>
					<div class="form-group col col-md-1 d-flex justify-content-end align-items-end">
						<button class="btn btn-danger"><i class="fas fa-plus"></i></button>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-12">
						<label for="">Otros</label>
						<input type="email" class="form-control" name="" placeholder="">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-12 col-md-4">
						<label>Sexo</label>
						<select class="form-control">
							<option>Default select</option>
							<option>Hombre</option>
							<option>Mujer</option>
							<option>Indistinto</option>
						</select>
					</div>
					<div class="form-group col-10 col-md-4">
						<label>Estado Civil</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
					<div class="form-group col-10 col-md-4">
						<label>Disponibilidad</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-10 col-md-6">
						<label>Rango de Salario Base</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
					<div class="form-group col-10 col-md-6">
						<label>Prestaciones</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
				</div>
				<hr style="border: 2px solid #000000;">
				<div class="form-row">
					<div class="form-group col col-md-2">
						<a href="" class="btn btn-primary">Regresar</a>
					</div>
					<div class="form-group col col-md-2">
						<a href="" class="btn btn-danger">Cancelar</a>
					</div>
					<div class="form-group col col-md-8 d-flex justify-content-end">
						<a href="" class="btn btn-success">Guardar</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

	{{-- PARA MOSTRAR UNA VENTANA DE ERROR SINO SE ENCUENTRA EL ARCHIVO --}}
	@if (Session::has('flag'))
		<script type="text/javascript">
			$(function() {
				$('#modalError').modal('show');
			});
		</script>
	@endif

	<div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					{!! Session::get('flag') !!}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<style type="text/css">
	.clase_error {
		display: none;
		color: red;
	}
</style>
<script type="text/javascript">
	$(function() {
		$("#nuevo_puesto").hide();

		$("#tipo_vacante").on('change', function(e) {
			e.preventDefault();
			var seleccion = $(this).val();
			if (seleccion == 'CREAR') {
				$( "#puesto_nuevo" ).val('');
				$( "#nuevo_puesto" ).slideDown( "slow", function() {
					$( "#puesto" ).prop( "disabled", true );
					$( "#puesto_descripcion" ).prop( "disabled", true );
					$( "#area" ).prop( "disabled", true );
				});
			} else {
				$( "#puesto_nuevo" ).val('');
				$( "#nuevo_puesto" ).slideUp( "slow", function() {
					$( "#puesto" ).prop( "disabled", false );
					$( "#puesto_descripcion" ).prop( "disabled", false );
					$( "#area" ).prop( "disabled", false );
				});
			}
		});

		$("#edad_max, #edad_min").on('change', function (e) {
			e.preventDefault();
			var edad_min = $("#edad_min").val();
			var edad_max = $("#edad_max").val();
    		if (edad_min > edad_max) {
				$("#edad_max_error").slideDown('slow');
			} else{
				$("#edad_max_error").slideUp('slow');
			}
		});

		$("#experiencia_0, #experiencia_1").on('change', function (e) {
			e.preventDefault();
			if ($("#experiencia_0").is(":checked")) {
				$( "#experiencia_anios" ).prop( "disabled", true );
			} else  {
				$( "#experiencia_anios" ).prop( "disabled", false );
			}
		});

		$('form').submit(function() {
			$("button[type='submit']").prop('disabled',true);
		});
	});
</script>
@endsection