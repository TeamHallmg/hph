@extends('layouts.app')

@section('title', 'Evaluación de Desempeño')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-responsive" src="{{ asset('img/evaluacion-desempeno/que-es/ban.png') }}" alt="">
		<br>
		<br>
		<p class="text-center">Reporte</p>
		<br>
		@foreach($usuario as $user)
		<p class="text-center"> Nombre: 
			<b>
				{{ $user->fullname }}
			</b>
		</p>
		<br>
		<p class="text-center"> Puesto: 
			<b>
				{{ $user->namePuesto->puesto }}
			</b>
		</p>
		<br>
		<p class="text-center"> Area: 
			<b>
				{{ $user->nameArea->nombre }}
			</b>
		</p>
		@endforeach
		<div class="col-md-12 margin-top-20 table-responsive">
			
			<table class="table table-striped">
				<caption align="center">Reportes</caption>
				<thead>
					<tr>
						<th rowspan="2" style="vertical-align: middle;">ID Factor</th>
						<th rowspan="2" style="vertical-align: middle;">Nombre del Factor</th>
						{{--  <th>Descripción del Factor</th>
						<th>Comportamientos del Factor</th>--}}
						<th rowspan="2" style="vertical-align: middle;">Valor esperado</th>
						{{--<th rowspan="2" style="vertical-align: middle;">Valor Máximo por Valor</th>
						 <th colspan="{{ $nivelesPuestos->count() }}">Valor Esperado</th>--}}
						<th rowspan="2" style="vertical-align: middle;">Auto Evaluación</th>
						<th rowspan="2" style="vertical-align: middle;">Jefe</th>
						
					</tr>
					{{--  <tr>
						@foreach($nivelesPuestos as $np)
						<th style="padding: 5px; margin: 0;">{{ $np->nombre }}</th>
						@endforeach
					</tr>--}}
				</thead>
				<tbody>
					{{--*/ $guardarCalifiJefe = "" /*--}}
					{{--*/ $guardarCalifiAuto = "" /*--}}

					{{--*/ $sumAuto = "" /*--}}
					{{--*/ $sumJefe = "" /*--}}
					{{--*/ $sumValorEsperado = "" /*--}}
					{{--*/ $sumTotalFactor = "" /*--}}
					
					{{--*/ $cambioFactor = 0 /*--}}
					{{--*/ $cambioFamilia = 0 /*--}}
					{{--*/ $cambioFamiliaSuma = 0 /*--}}
					@foreach($resultados as $resultado)
						@if($cambioFactor != $resultado->id_factor)
							@if($cambioFactor != 0)
									<td>
										{{ $guardarCalifiAuto }}
									</td>
									<td>
										{{ $guardarCalifiJefe }}
									</td>
								</tr>
								@if($cambioFamiliaSuma != $resultado->factores->factoresFamilia->id)
									<tr>
										<td colspan="2">Suma de Valores</td>
										<td>{{ $sumValorEsperado }}</td>
										<td>{{ $sumAuto }}</td>
										<td>{{ $sumJefe }}</td>
									</tr>
									{{--*/ $porcentajeEsperado = ($sumJefe/$sumValorEsperado)*100 /*--}}

									{{--*/ $porcentajeMaximo = ($sumJefe/$sumTotalFactor)*100 /*--}}
									<tr>
										<td colspan="2">Puntos logrados de los esperados:</td>
										<td colspan="3">{{ number_format($porcentajeEsperado, 0, '.', '') . '%' }}</td>
									</tr>
									<tr>
										<td colspan="2">Puntos logrados máximos:</td>
										<td colspan="3">{{ number_format($porcentajeMaximo, 0, '.', '') . '%' }}</td>
									</tr>
									
								@endif
								{{--*/$guardarCalifiJefe = ""/*--}}
								{{--*/$guardarCalifiAuto = ""/*--}}
							@endif

								{{--*/$cambioFactor = $resultado->id_factor/*--}}
								{{--*/$cambioFamiliaSuma = $resultado->factores->factoresFamilia->id/*--}}

							@if($cambioFamilia != $resultado->factores->factoresFamilia->id)
								<tr>
									<td colspan="5" align="center">
										{{ $resultado->factores->factoresFamilia->nombre }}
									</td>
								</tr>
								
								{{--*/ $sumAuto = "" /*--}}
								{{--*/ $sumJefe = "" /*--}}
								{{--*/ $sumValorEsperado = "" /*--}}
								{{--*/ $sumTotalFactor = "" /*--}}

								{{--*/$cambioFamilia = $resultado->factores->factoresFamilia->id/*--}}
							@endif
							<tr>
								<td>{{ $resultado->id_factor }}</td>
								<td>{{ $resultado->factores->nombre }}</td>
								{{--  <td>{{ $resultado->factores->descripcion }}</td>
								<td>
									<table  class="table table-striped">
										@foreach($resultado->factores->nivelDeDominio as $nivelDominio)
											<tr>
												<td>{{ $nivelDominio->nivel_dominio }}</td>
											</tr>
										@endforeach
									</table>
								</td>
									En esta celda se esta obteniendo el valor esperado conforme al nivel de puesto
									 
								--}}
								<td>
									{{ $resultado->nameUser->idPuestoNivelPuestoReporte->deseadoReporte->nivel_esperado }}
									{{--*/ $sumValorEsperado += $resultado->nameUser->idPuestoNivelPuestoReporte->deseadoReporte->nivel_esperado /*--}}
											
								</td>
								{{--  <td>
									{{ $resultado->factoresNivelDom->max('calificacion') }}--}}
									{{--*/ $sumTotalFactor += $resultado->factoresNivelDom->max('calificacion') /*--}}
								{{--</td>
								  <td>
									@foreach($nivelesPuestos as $nivelP)
										@foreach($resultado->nivelPuesto as $nivP)
											@if($nivelP->id == $nivP->id_nivel_puesto)
												<td>
													{{ $nivP->nivel_esperado }}
												</td>
											@endif
										@endforeach
									@endforeach
									</td>--}}
						@endif
						@if($resultado->id_evaluador == $resultado->id_evaluado)
							{{--*/ $guardarCalifiAuto = $resultado->nivel_desempeno; /*--}}
							{{--*/ $sumAuto += $resultado->nivel_desempeno; /*--}}
						@else
							{{--*/ $guardarCalifiJefe = $resultado->nivel_desempeno; /*--}}
							{{--*/ $sumJefe += $resultado->nivel_desempeno; /*--}}
						@endif
					@endforeach
					<td>
						{{ $guardarCalifiAuto }}
					</td>
					<td>
						{{ $guardarCalifiJefe }}
					</td>
				</tr>
				
					<tr>
						<td colspan="2">Suma de Valores:</td>
						<td>{{ $sumValorEsperado }}</td>
						<td>{{ $sumAuto }}</td>
						<td>{{ $sumJefe }}</td>
					</tr>
					{{--*/ $porcentajeEsperado = ($sumJefe/$sumValorEsperado)*100 /*--}}
					{{--*/ $porcentajeMaximo = ($sumJefe/$sumTotalFactor)*100 /*--}}
					<tr>
						<td colspan="2">Puntos logrados de los esperados:</td>
						<td colspan="3">{{ number_format($porcentajeEsperado, 0, '.', '') . '%' }}</td>
					</tr>
					<tr>
						<td colspan="2">Puntos logrados máximos:</td>
						<td colspan="3">{{ number_format($porcentajeMaximo, 0, '.', '') . '%' }}</td>
					</tr>
				</tbody>
			</table>
		</div>
		
		<div class="col-md-12 margin-top-20">
			<div class="col-md-6">
				<div style="max-height: 200px; overflow-y: auto">
					<h3 style="margin-top: 0">Cronología de Notas</h3>
					<table width="100%" style="font-size: 13px" border="1" class="notas">
						<thead>
							<tr>
								<th class="text-center cabeceras-tablas-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Fecha</th>
								<th class="text-center cabeceras-tablas-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Persona</th>
								<th class="text-center cabeceras-tablas-evaluaciones" style="padding: 5px 10px; border: 1px solid white;">Nota</th>
							</tr>
						</thead>
						<tbody>

      			{{--*/ 
      				$meses = array('0', 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');
           		$meses_completos = array('0', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');


      			/*--}}

      				@foreach ($notas as $nota) 
								<tr>
									<td class="text-center" style="padding: 5px; border: 0">{{ $nota->created_at->format('d/M/Y') }}</td>
									<td class="text-center" style="padding: 5px; border: 0">{{ $nota->nameUser->fullname }}</td>
									<td class="text-center" style="padding: 5px; border: 0">{{ $nota->nota }}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<h4 style="color: #B6BD00">Nota</h4>
				{!! Form::open(['id' => 'form-nota-reporte-jefe', 'action' => 'EvaluacionDesempeno\ReportesJefesController@store', 'method' => 'POST']) !!}
				<div>
					<textarea style="width: 100%; height: 100px; border-radius: 7px" class="nota" name="notaJefe"></textarea>
				</div>
				<div class="text-right margin-top-10">
					<button class="btn btn-primary btn-globalgas-yellow agregar_nota" value="agregarNotaJefe" name="btnNotaJefe">Agregar nota</button>
				</div>
				{{-- Form::close() --}}
			</div>
		</div>
	</div>
</div>

<div id="modalContainer" class="modal fade">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          
          <h4 class="modal-title text-center text-warning"><span class="glyphicon glyphicon-warning-sign"></span>&nbsp;&nbsp;&nbsp;ADVERTENCIA</h4>
        </div>
        <div class="modal-body">
          <p id="modalMessage"></p>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
@endsection

@section('scripts')
	<script>
		var id_empleado = {{ $id }};

 		$(document).ready(function(){
			/*$.ajaxSetup({
    		headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    		}
			});*/

      $('button.agregar_nota').on('click', function(e) {
				e.preventDefault();

				var nota = $('textarea.nota').val();

				var btn = $(this);
				var form = $('#form-nota-reporte-jefe');
				var url = form.attr('action');
				var data = form.serialize() +  
					'&id_empleado=' + id_empleado;

				$.post(url, data, function (reply){
					if(reply.success){
						
						$('table.notas').prepend('<tr><td class="text-center" style="padding: 5px; border: 0">' + reply.fecha +  '</td><td class="text-center" style="padding: 5px; border: 0">' + reply.usuario + '</td><td class="text-center" style="padding: 5px; border: 0">' + reply.nota + '</td></tr>');
            $('textarea.nota').val('');
					}else{
						$('#modalMessage').html(reply.msn);
						$('#modalContainer').modal('show');
					}
				});
				
				/*$.ajax({
        	type:'POST',    
        	url: 'reporte-jefe',
          data:{
          	mensaje: nota,
          	id_empleado: id_empleado
          },
          success: function(data){
            alert(data);
            $('table.notas').prepend('<tr><td class="text-center" style="padding: 5px; border: 0">' + '</td><td class="text-center" style="padding: 5px; border: 0">' + '</td><td class="text-center" style="padding: 5px; border: 0">' + '</td></tr>');
            $('textarea.agregar_nota_reporte_desempeno').val('');
          }
        });*/
			});
		});
	</script>
@endsection

 {{--*/$cambioFamiliaFactor = $resultado->factores->factoresFamilia->id/*--}}
								{{--@if($cambioFamiliaFactor != $resultado->factores->factoresFamilia->id)
									@if($cambioFamiliaFactor != 0)
										<tr>
											<td colspan="5" align="center">{{ $resultado->factores->factoresFamilia->nombre }}</td>
										</tr>
									@endif
								@endif --}}