@extends('layouts.app')

@section('title', 'Reporte Evaluación de Desempeño')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('/img/evaluacion_desempeno.png') }}" alt="">
	</div>
</div>
<div class="row">
	<div class="col">
		<h1 class="text-center titulos-evaluaciones font-weight-bold">Reporte Valoración del Desempeño</h1>
		<hr>
	</div>
</div>
<div class="row my-5">
	<div class="col-md-12 text-center">
		<div id="myImageContainer">
			<img id="myImage">
		</div>
	@if ($evaluaciones_terminadas == true)
		<button class="btn btn-primary print_report" <?php if (auth()->user()->role != 'admin' && (count($comentario) == 0 || count($objetivos_entregables_desempeno) == 0)){ ?>style="display: none"<?php } ?>><i class="fas fa-print"></i> Imprimir Reporte Resumen</button>
	@endif
	@if ($user[0]->boss_id == auth()->user()->id || auth()->user()->role == 'admin')
		<button class="btn btn-primary print_report completo" <?php if (auth()->user()->role != 'admin' && (count($comentario) == 0 || count($objetivos_entregables_desempeno) == 0)){ ?>style="display: none"<?php } ?>><i class="fas fa-print"></i> Imprimir Reporte Completo</button>
	@endif
	</div>
</div>
<div class="row" id="areaImprimir">
	<div class="text-center">
		<h1 class="text-center mostrar font-weight-bold" style="display: none; margin-top: 0px">Reporte Valoración del Desempeño</h1>
	</div>
	<div class="col-md-8 datos_evaluado mt-5">
		<table class="table-fluid tabla_evaluado" border="1" width="100%" style="font-size: 14px">
			<tbody>
				<tr>
					<td>EVALUADO:</td>
					<td class="text-center">
						<b>{{str_replace('Ã‘','Ñ',$user[0]->first_name)}} {{str_replace('Ã‘','Ñ',$user[0]->last_name)}}</b>
					</td>
				</tr>
				<tr>
					<td>NO. EMPLEADO:</td>
					<td class="text-center">
						<b>{{$user[0]->id}}</b>
					</td>
				</tr>
				<tr>
					<td>PUESTO:</td>
					<td class="text-center">
						<b>{{(!empty($user[0]->subdivision) ? str_replace('Ã‘','Ñ',$user[0]->subdivision) : '')}}</b>
					</td>
				</tr>
				<tr>
					<td>FECHA:</td>
					<td class="text-center">
						<b>{{$meses[date('m') * 1] . '-' . date('y')}}</b>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-4 text-center datos_logo mt-5">
		<img src="{{ asset('img/logo.png') }}" class="img-fluid" style="position: relative; top: -10px">
	</div>

<div class="mt-5">

@if (count($periodos) > 0)

	<div class="col-md-12 margin-top-20 oculto">
@endif

		<form action="/reporte-desempeno/{{$user[0]->id}}" method="post" class="form_periodo_a_comparar">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id_periodo" value="{{$id_periodo}}">

@if (count($periodos) > 0)

		Periodo a comparar 	<select name="periodo_a_comparar" class="periodo_a_comparar">
											   	<option value="0">--Selecciona periodo--</option>

								<?php foreach ($periodos as $key => $value){ ?>
										
												 	<option value="{{$value->id}}" <?php if (!empty($_POST['periodo_a_comparar']) && $_POST['periodo_a_comparar'] == $value->id){ ?>selected="selected"<?php } ?>>{{$value->descripcion}}</option>
								<?php } ?>

												</select>
@else
	
			<select name="periodo_a_comparar" class="periodo_a_comparar" style="display: none">
											   	<option value="0">--Selecciona periodo--</option>
			</select>
@endif
		</form>

@if (count($periodos) > 0)
	
	</div>
@endif

@if (count($resultados_a_comparar))
<div class="row my-5">
	<div class="col-md-5 margin-top-20 oculto">
		<table class="table-responsive" border="1" width="100%">
			<thead>
				<tr>
					<th>Competencias</th>
					<th class="text-center">Promedio</th>
				</tr>
			</thead>
			<tbody>

<?php $actual_factor = 0;
			$contador = 0;
			$contador_factores = 0;
			$total = 0;
			$total_factor = 0;
			$promedio = 0;
			$comentarios = array();
			$results_a_comparar = array();
			$factors_a_comparar = array();

			foreach ($resultados_a_comparar as $key => $value){
			
				if ($actual_factor != $value->id_factor){
					
					$factors_a_comparar[] = $value->nombre;

					if ($actual_factor != 0){

						$contador_factores++;

						if ($contador == 0){

							$contador = 1;
						}

						$promedio = $total_factor / $contador;
						$total += $promedio;
						$results_a_comparar[] = number_format($promedio, 2, '.', ',') * 1;
						?>

					<td class="text-center" style="color: white; background-color: {{($promedio >= 3.5 ? '#008000' : ($promedio >= 3 ? '#73CB77' : ($promedio >= 2.5 ? '#FFFF00; color: black' : ($promedio >= 2 ? '#FF4343' : '#C00000'))))}}"><b>{{number_format($promedio, 2, '.', ',')}}</b></td>
				</tr>
		<?php }

					$contador = 0;
					$actual_factor = $value->id_factor;
					$total_factor = 0; ?>

				<tr>
					<td>{{$value->nombre}}</td>
	<?php }

				$total_factor += $value->nivel_desempeno;
				$contador++;
			}

			if ($actual_factor != 0){

				$contador_factores++;

				if ($contador == 0){

					$contador = 1;
				}

				$promedio = $total_factor / $contador;
				$results_a_comparar[] = number_format($promedio, 2, '.', ',') * 1;
				$total += $promedio; ?>

					<td class="text-center" style="color: white; background-color: {{($promedio >= 3.5 ? '#008000' : ($promedio >= 3 ? '#73CB77' : ($promedio >= 2.5 ? '#FFFF00; color: black' : ($promedio >= 2 ? '#FF4343' : '#C00000'))))}}"><b>{{number_format($promedio, 2, '.', ',')}}</b></td>
				</tr>
<?php }

			if ($contador_factores == 0){

				$contador_factores = 1;
			} ?>

				<tr>
					<td>RESULTADO GLOBAL</td>
					<td class="text-center" style="color: white; background-color: {{($promedio >= 3.5 ? '#008000' : ($promedio >= 3 ? '#73CB77' : ($promedio >= 2.5 ? '#FFFF00; color: black' : ($promedio >= 2 ? '#FF4343' : '#C00000'))))}}"><b>{{number_format($total / $contador_factores, 2, '.', ',')}}</b></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-7 margin-top-20 oculto">
		<h4 class="text-center titulos-evaluaciones font-weight-bold" style="margin-top: 0">Resultados Valoración del desempeño</h4>
		<div id="container_a_comparar" style="width: 100%; margin: 0 auto"></div>
	</div>
</div>
@endif

<div class="row mb-5">
	<div class="col-md-5 datos_competencias">
		<table class="table-fluid tabla_competencias" border="1" width="100%">
			<thead>
				<tr>
					<th>Competencias</th>
					<th class="text-center">Promedio</th>
				</tr>
			</thead>
			<tbody>

<?php $actual_factor = 0;
			$contador = 0;
			$contador_factores = 0;
			$total = 0;
			$total_factor = 0;
			$promedio = 0;
			$comentarios = array();
			$results = array();
			$factors = array();

			foreach ($resultados as $key => $value){
			
				if ($actual_factor != $value->id_factor){
					
					$factors[] = $value->nombre;

					if ($actual_factor != 0){

						$contador_factores++;

						if ($contador == 0){

							$contador = 1;
						}

						$promedio = $total_factor / $contador;
						$total += $promedio;
						$results[] = number_format($promedio, 2, '.', ',') * 1;
						?>

					<td class="text-center" style="color: white; background-color: {{($promedio >= 3.5 ? '#008000' : ($promedio >= 3 ? '#73CB77' : ($promedio >= 2.5 ? '#FFFF00; color: black' : ($promedio >= 2 ? '#FF4343' : '#C00000'))))}}"><b>{{number_format($promedio, 2, '.', ',')}}</b></td>
				</tr>
		<?php }

					$contador = 0;
					$actual_factor = $value->id_factor;
					$total_factor = 0; ?>

				<tr>
					<td>{{$value->nombre}}</td>
	<?php }

				$total_factor += $value->nivel_desempeno;
				$contador++;

	      if (!empty($value->comentario)){

	      	if (empty($comentarios[$value->nombre])){

	      		$comentarios[$value->nombre] = $value->comentario;
	      	}

	      	else{

	      		$comentarios[$value->nombre] .= '*' . $value->comentario;
	      	}
	      }
			}

			if ($actual_factor != 0){

				$contador_factores++;

				if ($contador == 0){

					$contador = 1;
				}

				$promedio = $total_factor / $contador;
				$results[] = number_format($promedio, 2, '.', ',') * 1;
				$total += $promedio; ?>

					<td class="text-center" style="color: white; background-color: {{($promedio >= 3.5 ? '#008000' : ($promedio >= 3 ? '#73CB77' : ($promedio >= 2.5 ? '#FFFF00; color: black' : ($promedio >= 2 ? '#FF4343' : '#C00000'))))}}"><b>{{number_format($promedio, 2, '.', ',')}}</b></td>
				</tr>
<?php }

			if ($contador_factores == 0){

				$contador_factores = 1;
			}

			$alto_grafica = 40 * (1 + $contador_factores);	?>

				<tr>
					<td>RESULTADO GLOBAL</td>
					<td class="text-center" style="color: white; background-color: {{($promedio >= 3.5 ? '#008000' : ($promedio >= 3 ? '#73CB77' : ($promedio >= 2.5 ? '#FFFF00; color: black' : ($promedio >= 2 ? '#FF4343' : '#C00000'))))}}"><b>{{number_format($total / $contador_factores, 2, '.', ',')}}</b></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-7 margin-top-20 datos_grafica">
		<h4 class="text-center titulos-evaluaciones font-weight-bold" style="margin-top: 0">Resultados Valoración del desempeño</h4>
		<div id="container" style="width: 100%; margin: 0 auto"></div>
	</div>
</div>

@if (!empty($comentarios))

	<div class="col-md-12 oculto2 mb-5">
		<div class="text-center titulos-evaluaciones font-weight-bold" style="font-size: 20px">Comentarios u Observaciones</div>
		<table class="table-fluid comentarios_reporte" border="1" width="100%">
			<tbody>

<?php $contador = 0;

			foreach ($comentarios as $key => $value){ ?>

				<tr>

				@if ($contador == 0)

					<td rowspan="{{count($comentarios)}}" class="text-center">
						<h4>COMPETENCIAS GENERALES</h4>
					</td>
				@endif

	<?php $contador++?>

					<td class="text-center">{{$contador}}</td>
					<td class="text-center">{{$key}}</td>
					<td>

	<?php $comments = explode('*', $value)?>

				@for ($i = 0;$i < count($comments);$i++)	

					<p>{{$comments[$i]}}</p>
				@endfor

					</td>
				</tr>
<?php	} ?>

			</tbody>
		</table>
	</div>
@endif

<form action="/guardar-comentarios-reporte" method="post" target="response" class="comentarios_reporte">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="hidden" name="id_evaluado" value="{{$user[0]->id}}">
	<input type="hidden" name="id_periodo" value="{{$id_periodo}}">
	<div class="col-md-12 my-4 oculto2">
		<div class="text-center titulos-evaluaciones font-weight-bold" style="font-size: 20px">Retroalimentación</div>
		<textarea style="width: 100%; height: 100px; resize: none" name="comentarios_adicionales" <?php if ($user[0]->boss_id != auth()->user()->id || !$evaluaciones_terminadas){ ?>readonly="readonly"<?php } ?> class="retro">{{(count($comentario) > 0 ? $comentario[0]->comentario : '')}}</textarea>
	</div>
	<div class="col-md-12 my-5">
		<div class="text-center titulos-evaluaciones objetivos_entregables font-weight-bold" style="font-size: 20px">Objetivos y acciones a realizar</div>
	<p class="text-center">¿Qué objetivos laborales (relacionados con los objetivos estratégicos del departamento) necesito y quiero alcanzar? ¿Qué acciones específicas puedo realizar para lograrlo?</p>
		<table class="table-fluid objetivos_entregables" border="1" width="100%">
			<thead>
				<tr>
					<th class="text-center">Objetivos</th>
					<th class="text-center">Acciones</th>
					<th class="text-center">Entregables</th>
				</tr>
			</thead>
			<tbody>

<?php foreach ($objetivos_entregables_desempeno as $key => $value){ ?>

				<tr>
					<td>
						<textarea style="width: 100%;height: 100%;resize: none;border: none;overflow: auto;outline: none;" name="objetivo{{$key + 1}}" <?php if ($user[0]->boss_id != auth()->user()->id || !$evaluaciones_terminadas){ ?>readonly="readonly"<?php } ?> class="retro">{{$value->objetivo}}</textarea>
					</td>
					<td>
						<textarea style="width: 100%;height: 100%;resize: none;border: none;overflow: auto;outline: none;" name="accion{{$key + 1}}" <?php if ($user[0]->boss_id != auth()->user()->id || !$evaluaciones_terminadas){ ?>readonly="readonly"<?php } ?> class="retro">{{$value->accion}}</textarea>
					</td>
					<td>
						<textarea style="width: 100%;height: 100%;resize: none;border: none;overflow: auto;outline: none;" name="entregable{{$key + 1}}" <?php if ($user[0]->boss_id != auth()->user()->id || !$evaluaciones_terminadas){ ?>readonly="readonly"<?php } ?> class="retro">{{$value->entregable}}</textarea>
					</td>
				</tr>
<?php }

			for ($i = count($objetivos_entregables_desempeno) + 1; $i < 6;$i++){ ?>

				<tr>
					<td>
						<textarea style="width: 100%;height: 100%;resize: none;border: none;overflow: auto;outline: none;" name="objetivo{{$i}}" <?php if ($user[0]->boss_id != auth()->user()->id || !$evaluaciones_terminadas){ ?>readonly="readonly"<?php } ?> class="retro"></textarea>
					</td>
					<td>
						<textarea style="width: 100%;height: 100%;resize: none;border: none;overflow: auto;outline: none;" name="accion{{$i}}" <?php if ($user[0]->boss_id != auth()->user()->id || !$evaluaciones_terminadas){ ?>readonly="readonly"<?php } ?> class="retro"></textarea>
					</td>
					<td>
						<textarea style="width: 100%;height: 100%;resize: none;border: none;overflow: auto;outline: none;" name="entregable{{$i}}" <?php if ($user[0]->boss_id != auth()->user()->id || !$evaluaciones_terminadas){ ?>readonly="readonly"<?php } ?> class="retro"></textarea>
					</td>
				</tr>
<?php } ?>

			</tbody>
		</table>
	</div>

<?php if ($user[0]->boss_id == auth()->user()->id  && $evaluaciones_terminadas){ ?>

	<div class="col-md-12 margin-top-20 text-center oculto">
		<input type="submit" class="btn btn-primary" value="Guardar Comentario y Objetivos">
	</div>
<?php } ?>

</form>
<div class="row">
	<div class="col-md-6 col-sm-6 text-center datos_firmas">
		<div style="width: 300px; margin: 70px auto 0 auto; border-top: 2px solid black; font-size: 12px">
			<div>{{str_replace('Ã‘','Ñ',$user[0]->first_name)}} {{str_replace('Ã‘','Ñ',$user[0]->last_name)}}</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-6 text-center datos_firmas">
		<div style="width: 300px; margin: 70px auto 0 auto; border-top: 2px solid black; font-size: 12px">
			<div>{{(!empty($jefe) ? str_replace('Ã‘','Ñ',$jefe[0]->first_name) . ' ' . str_replace('Ã‘','Ñ',$jefe[0]->last_name) : '')}}</div>
		</div>
	</div>
</div>
</div>
<iframe frameborder="0" width="0" height="0" name="response"></iframe>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bluebird/3.3.5/bluebird.min.js"></script>
@endsection

@section('scripts')
	<script type="text/javascript">

		var resultados = <?php echo json_encode($resultados)?>;
		var factors = <?php echo json_encode($factors)?>;
		var results = <?php echo json_encode($results)?>;
		var resultados_a_comparar = <?php echo json_encode($resultados_a_comparar)?>;
		var factors_a_comparar = <?php echo (!empty($factors_a_comparar) ? json_encode($factors_a_comparar) : '0')?>;
		var results_a_comparar = <?php echo (!empty($results_a_comparar) ? json_encode($results_a_comparar) : '0')?>;
		var user = <?php echo json_encode($user[0])?>;
		var alertar = false;
		var abandonar_pagina = false;
		var canEdit = <?php echo ($user[0]->boss_id == auth()->user()->id && $evaluaciones_terminadas ? '1' : '0')?>;

		$(document).ready(function(){

			var autoevaluacion = [], jefe = [], colaborador = [], par = [];
			var contador_autoevaluacion = 0, contador_jefe = 0, contador_colaborador = 0, contador_par = 0;
			var factor_colaborador = '', factor_par = '';
			var evaluciones_colaborador = 0, evaluciones_par = 0, total_colaborador = 0, total_par = 0;

			if (window.navigator.userAgent.indexOf("Edge") > -1){

				$('table.objetivos_entregables th, table.objetivos_entregables textarea').css('border', '1px solid grey');
				$('table.objetivos_entregables').removeAttr('border');
			}

			for(var i = 0;i < resultados.length;i++){

				if (user.id == resultados[i].id_evaluador){

					while (resultados[i].nombre != factors[contador_autoevaluacion]){

						autoevaluacion[contador_autoevaluacion] = 0;
						contador_autoevaluacion++;
					}

					autoevaluacion[contador_autoevaluacion] = resultados[i].nivel_desempeno * 1;
					contador_autoevaluacion++;
				}

				else{

					if (user.boss_id == resultados[i].id_evaluador){

						while (resultados[i].nombre != factors[contador_jefe]){

							jefe[contador_jefe] = 0;
							contador_jefe++;
						}

						jefe[contador_jefe] = resultados[i].nivel_desempeno * 1;
						contador_jefe++;
					}

					else{

						if (user.id == resultados[i].boss_id){
							
							if (factor_colaborador == resultados[i].nombre){

								contador_colaborador--;
								evaluciones_colaborador++;
								total_colaborador += resultados[i].nivel_desempeno;
								colaborador[contador_colaborador] = (total_colaborador / evaluciones_colaborador).toFixed(1) * 1;
								contador_colaborador++;
							}

							else{

								while (resultados[i].nombre != factors[contador_colaborador]){

									colaborador[contador_colaborador] = 0;
									contador_colaborador++;
								}
								
								total_colaborador = resultados[i].nivel_desempeno * 1;
								colaborador[contador_colaborador] = total_colaborador;
								factor_colaborador = resultados[i].nombre;
								contador_colaborador++;
								evaluciones_colaborador = 1;

							}
						}

						else{

							//if (user.boss_id == resultados[i].boss_id){
								
								if (factor_par == resultados[i].nombre){

									contador_par--;
									evaluciones_par++;
									total_par += resultados[i].nivel_desempeno;
									par[contador_par] = (total_par / evaluciones_par).toFixed(1) * 1;
									contador_par++;
								}

								else{

									while (resultados[i].nombre != factors[contador_par]){

										par[contador_par] = 0;
										contador_par++;
									}
									
									total_par = resultados[i].nivel_desempeno * 1;
									par[contador_par] = total_par;
									factor_par = resultados[i].nombre;
									contador_par++;
									evaluciones_par = 1;
								}
							//}
						}
					}		
				}
			}

			for (var i = contador_autoevaluacion;i < factors.length;i++){

				autoevaluacion[i] = 0;
			}

			for (var i = contador_jefe;i < factors.length;i++){

				jefe[i] = 0;
			}

			for (var i = contador_colaborador;i < factors.length;i++){

				colaborador[i] = 0;
			}

			for (var i = contador_par;i < factors.length;i++){

				par[i] = 0;
			}
   
   		var chart = {
      	renderTo: 'container',
      	options3d: {
        	enabled: true,
        	alpha: 0,
        	beta: 0,
        	depth: 50,
        	viewDistance: 25
      	}
   		};

   		var title = {
      	text: ''   
   		};

   		var legend = {
        align: 'center',
        verticalAlign: 'bottom',
        layout: 'horizontal',
				floating: true,
				y: 20
      };
   
   		var plotOptions = {
      	column: {
        	depth: 25
      	}
   		};

    	var series = [{
      	name: 'PROMEDIO',
            data: results,
            color: '#004A91',
			type: 'column',
        },{
            name: 'AUTOEVALUACION',
            data: autoevaluacion,
            color: '#C00000',
			type: 'line',
        },{
        	name: 'JEFE',
            data: jefe,
            color: '#008000',
			type: 'line',
        },{
        	name: 'COLABORADOR',
            data: colaborador,
            color: '#FFA500',
			type: 'line',
        },{
        	name: 'PAR',
          data: par,
          color: '#A020F0',
		  type: 'line',
        }];
		
			var yAxis = [{
				labels: {
        	style: {
          	fontSize:'12px',
          	fontWeight:'bold'
        	}
      	},
      	max: 4,
      	tickInterval: 0.5
			}];

   		var xAxis = [{
      	categories: factors,
	  		labels: {
          style: {
            fontSize:'8px',
            fontWeight:'bold'
          },
          step: 1
        },
    	}];     
      
   		var json = {};
   		json.chart = chart;
   		json.title = title;
   		json.series = series;
   		json.plotOptions = plotOptions;
   		json.xAxis = xAxis;
			json.yAxis = yAxis;
			json.legend = legend;
   		highchart = new Highcharts.Chart(json);
   		//var alto = (alto_grafica > 210 ? alto_grafica : 210);

   		// Se seleccionó un periodo a comparar... se hacen los cálculos para mostrar la segunda gráfica
			if (factors_a_comparar != '0'){

   			var autoevaluacion = [], jefe = [], colaborador = [], par = [];
				var contador_autoevaluacion = 0, contador_jefe = 0, contador_colaborador = 0, contador_par = 0;
				var factor_colaborador = '', factor_par = '';
				var evaluciones_colaborador = 0, evaluciones_par = 0, total_colaborador = 0, total_par = 0;

				for(var i = 0;i < resultados_a_comparar.length;i++){

					if (user.id == resultados_a_comparar[i].id_evaluador){

						while (resultados_a_comparar[i].nombre != factors_a_comparar[contador_autoevaluacion]){

							autoevaluacion[contador_autoevaluacion] = 0;
							contador_autoevaluacion++;
						}

						autoevaluacion[contador_autoevaluacion] = resultados_a_comparar[i].nivel_desempeno * 1;
						contador_autoevaluacion++;
					}

					else{

						if (user.boss_id == resultados_a_comparar[i].id_evaluador){

							while (resultados_a_comparar[i].nombre != factors_a_comparar[contador_jefe]){

								jefe[contador_jefe] = 0;
								contador_jefe++;
							}

							jefe[contador_jefe] = resultados_a_comparar[i].nivel_desempeno * 1;
							contador_jefe++;
						}

						else{

							if (user.id == resultados_a_comparar[i].boss_id){
							
								if (factor_colaborador == resultados_a_comparar[i].nombre){

									contador_colaborador--;
									evaluciones_colaborador++;
									total_colaborador += resultados_a_comparar[i].nivel_desempeno;
									colaborador[contador_colaborador] = (total_colaborador / evaluciones_colaborador).toFixed(1) * 1;
									contador_colaborador++;
								}

								else{

									while (resultados_a_comparar[i].nombre != factors_a_comparar[contador_colaborador]){

										colaborador[contador_colaborador] = 0;
										contador_colaborador++;
									}
								
									total_colaborador = resultados_a_comparar[i].nivel_desempeno * 1;
									colaborador[contador_colaborador] = total_colaborador;
									factor_colaborador = resultados_a_comparar[i].nombre;
									contador_colaborador++;
									evaluciones_colaborador = 1;
								}
							}

							else{

								//if (user.boss_id == resultados[i].boss_id){
								if (factor_par == resultados_a_comparar[i].nombre){

									contador_par--;
									evaluciones_par++;
									total_par += resultados_a_comparar[i].nivel_desempeno;
									par[contador_par] = (total_par / evaluciones_par).toFixed(1) * 1;
									contador_par++;
								}

								else{

									while (resultados_a_comparar[i].nombre != factors_a_comparar[contador_par]){

										par[contador_par] = 0;
										contador_par++;
									}
									
									total_par = resultados_a_comparar[i].nivel_desempeno * 1;
									par[contador_par] = total_par;
									factor_par = resultados_a_comparar[i].nombre;
									contador_par++;
									evaluciones_par = 1;
								}
							//}
							}
						}		
					}
				}

				for (var i = contador_autoevaluacion;i < factors_a_comparar.length;i++){

					autoevaluacion[i] = 0;
				}

				for (var i = contador_jefe;i < factors_a_comparar.length;i++){

					jefe[i] = 0;
				}

				for (var i = contador_colaborador;i < factors_a_comparar.length;i++){

					colaborador[i] = 0;
				}

				for (var i = contador_par;i < factors_a_comparar.length;i++){

					par[i] = 0;
				}
   
   			var chart = {
      		renderTo: 'container_a_comparar',
      		options3d: {
        		enabled: true,
        		alpha: 0,
        		beta: 0,
        		depth: 50,
        		viewDistance: 25
      		}
   			};

   			var title = {
      		text: ''   
   			};

   			var legend = {
        	align: 'center',
        	verticalAlign: 'bottom',
        	layout: 'horizontal',
					floating: true,
					y: 20
      	};
   
   			var plotOptions = {
      		column: {
        		depth: 25
      		}
   			};

    		var series = [{
      		name: 'PROMEDIO',
          data: results_a_comparar,
          color: '#004A91',
					type: 'column',
        },{
            name: 'AUTOEVALUACION',
            data: autoevaluacion,
            color: '#C00000',
						type: 'line',
        },{
        	name: 'JEFE',
            data: jefe,
            color: '#008000',
						type: 'line',
        },{
        		name: 'COLABORADOR',
            data: colaborador,
            color: '#FFA500',
						type: 'line',
        },{
        		name: 'PAR',
          	data: par,
          	color: '#A020F0',
		 	 			type: 'line',
        }];
		
				var yAxis = [{
					labels: {
        		style: {
          		fontSize:'12px',
          		fontWeight:'bold'
        		}
      		},
      		max: 4,
      		tickInterval: 0.5
				}];

   			var xAxis = [{
      		categories: factors_a_comparar,
	  			labels: {
          	style: {
            	fontSize:'8px',
            	fontWeight:'bold'
          	},
          	step: 1
        	},
    		}];     
      
   			var json = {};
   			json.chart = chart;
   			json.title = title;
   			json.series = series;
   			json.plotOptions = plotOptions;
   			json.xAxis = xAxis;
				json.yAxis = yAxis;
				json.legend = legend;
   			highchart = new Highcharts.Chart(json);
   		}

   		$('form.comentarios_reporte').submit(function(){

   			// Temporizador para que se guarde la retroalimentación y recargar la página
   			setTimeout(function(){

   				alert('Comentarios y Objetivos Guardados');

   			/*if ($('textarea[name="comentarios_adicionales"]').val().trim().length > 0){

   				for (var i = 1;i < 6;i++){

   					if ($('textarea[name="objetivo' + i + '"]').val().trim().length > 0 && $('textarea[name="accion' + i + '"]').val().trim().length > 0 && $('textarea[name="entregable' + i + '"]').val().trim().length > 0){

   						$('button.print_report').show();
   					}
   				}
   			}*/

   				alertar = false;
   			
   				// Se envía el formulario para comparar periodos
					$('form.form_periodo_a_comparar').submit();
				}, 1000);
   		});

   		$('table.objetivos_entregables textarea').keydown(function(event){
				
				var altura = $(this).prop('scrollHeight') * 1;

   			if (altura > 82){

   				$(this).val($(this).val().substr(0, $(this).val().length - 2));
   			}
   		});

   		$('table.objetivos_entregables textarea').keyup(function(event){
				
				var altura = $(this).prop('scrollHeight') * 1;

   			if (altura > 82){

   				$(this).val($(this).val().substr(0, $(this).val().length - 1));
   			}
   		});

   		$('body').on('click', '.print_report', function(){

   			if ($(this).hasClass('completo')){

   				printDiv('areaImprimir', 2);
   			}

   			else{

   				printDiv('areaImprimir', 1);
   			}
   		});

   		// Write in some field for retroalimentation
   		$('.retro').keyup(function(){

   			if (canEdit == '1'){

   				alertar = true;
   			}
   		});

			// Se seleccionó un periodo para comparar
			$('body').on('change', 'select.periodo_a_comparar', function(){

				// Se envía el formulario para comparar periodos
				$('form.form_periodo_a_comparar').submit();
			});

			// Evento que muestra una alerta cuando se quiere abandonar la pagina
			window.onbeforeunload = function(e){

				// No hay cambios sin guardar
				if (!alertar){

					// No se muestra la alerta
					return null;
				}

  			// Se desea abandonar la página
  			abandonar_pagina = true;

  			// Se debe retornar algo diferente a null para que se muestre la alerta antes de abanadonar la página
  			return 'algo';
			};

			// El navegador no es Edge
			if (window.navigator.userAgent.indexOf("Edge") == -1){

				// Se crea un intérvalo que corre cada medio segundo
				setInterval(function(){

					// Se desea abandonar la pagina y debe mostrarse la alerta
					if (abandonar_pagina && alertar){

						abandonar_pagina = false;
					
						// Se muestra una segunda alerta con el mensaje correcto
						alert("¿Quieres salir de la página?\n\nSi sales de la página los cambios no se guardarán.");
					}
				}, 500);
			}
   	});

		function printDiv(nombreDiv, completo){
			
			// Se oculta todo lo que no debe imprimirse
			$('.oculto').hide();

			// Se va a imprimir el resumen del reporte
			if (completo == 1){

				// Se ocultan los comentarios y retroalimentación
				$('.oculto2').hide();
			}

			$('.mostrar').show();
			//$('html').css('margin', 0);
    	//$('body').css('margin', 0);
    	//$('.highcharts-container').attr('height', '350');
    	//$('.highcharts-container').css('height', '350px');
    	$('g.highcharts-yaxis text, text.highcharts-credits').remove();

    	if (navigator.userAgent.indexOf('Trident') > -1){

    		var anchura = $('.highcharts-container').attr('width');
				var altura = $('.highcharts-container').attr('height');
				$('.highcharts-container').attr('width', '546');
				$('.highcharts-container svg').attr('width', '546');
				$('.highcharts-container').attr('height', '388');
				$('.highcharts-container svg').attr('height', '388');
				$('.highcharts-container').css('overflow', 'visible');
    		$('.datos_evaluado').addClass('estilos_evaluado');
    		$('.datos_logo').addClass('estilos_logo');
    		$('.datos_competencias').addClass('estilos_competencias');
    		$('.datos_grafica').addClass('estilos_grafica');
    		$('.datos_firmas').addClass('estilos_firmas');
				$('#' + nombreDiv).print();
				$('.datos_evaluado').removeClass('estilos_evaluado');
    		$('.datos_logo').removeClass('estilos_logo');
    		$('.datos_competencias').removeClass('estilos_competencias');
    		$('.datos_grafica').removeClass('estilos_grafica');
    		$('.datos_firmas').removeClass('estilos_firmas');
    		$('.oculto').show();

    		// Se muestran los comentarios y retroalimentación
				$('.oculto2').show();

				$('.mostrar').hide();

				setTimeout(function(){
			
					$('.highcharts-container').attr('width', anchura);
					$('.highcharts-container svg').attr('width', anchura);
					$('.highcharts-container').attr('height', altura);
					$('.highcharts-container svg').attr('height', altura);
				}, 4000);
    	}

    	else{

    		// El navegador Edge
    		if (window.navigator.userAgent.indexOf("Edge") > -1){

    			// Agrega un borde a las celdas de la tabla de comentarios u observaciones, a la tabla de competencias y a la de los datos del evaluado
    			$('table.comentarios_reporte tbody tr td, table.tabla_competencias tbody tr td, table.tabla_evaluado tbody tr td').css('border', '1px solid grey');

    			// Quita el atributo border de la tabla de comentarios u observaciones, de la de competencias y de la de los datos del evaluado
    			$('table.comentarios_reporte, table.tabla_competencias, table.tabla_evaluado').removeAttr('border');
    		}

    		var areaImprimir = document.getElementById(nombreDiv);
				html2canvas(areaImprimir).then(function(canvas){
    			var myImage = canvas.toDataURL("image/png");
					$('#myImage').attr('src', myImage);
				
					setTimeout(function(){
					
						var contenido = document.getElementById('myImageContainer').innerHTML;
     				var contenidoOriginal= document.body.innerHTML;
						document.body.innerHTML = contenido;
						window.print();
						document.body.innerHTML = contenidoOriginal;
						$('#myImage').attr('src', '');
						$('.oculto').show();

						// Se muestran los comentarios y retroalimentación
						$('.oculto2').show();

						$('.mostrar').hide();

						// El navegador Edge
						if (window.navigator.userAgent.indexOf("Edge") > -1){

    					// Agrega el atributo border con valor de 1 a la tabla de comentarios u observaciones, a la de competencias y a la de los datos del evaluado
    					$('table.comentarios_reporte, table.tabla_competencias, table.tabla_evaluado').attr('border', 1);
    				}
					}, 500);
				});
			}
		}
	</script>
@endsection
