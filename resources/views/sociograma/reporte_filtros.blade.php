<div class="card-body">
     <div class="row">

         <div class="col-md-4">
            <div class="form-group">
                <label for="">Periodo</label>
                <select class="cambiando form-control _change" id="select_periodo" name="periodos" v-model="id_periodo" @change="getRecords()">
                    <option :value="item.id" v-for="(item, index) in filtros.periodos" :key="index"> @{{item.name}} </option> 
                </select>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="">Hospital</label>
                <select class="cambiando form-control _change" id="select_regiones" v-model="region" name="region" @change="getRecords()">
                    <option value="">--TODO--</option>
                    <option :value="item.id" v-for="(item, index) in filtros.regiones" :key="index"> @{{item.name}} </option> 
                </select>
            </div>
        </div>  

        <div class="col-md-4">
            <div class="form-group">
                <label for="">Centros de Trabajos</label>
                <select class="cambiando form-control" name="centro_trabajo" v-model="centro_trabajo" @change="getRecords()">
                    <option value="">--TODO--</option>
                    <option :value="item.id" v-for="(item, index) in filtros.centros_trabajo" :key="index"> @{{item.name}} </option> 
                </select>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="">Areas</label>
                <select class="cambiando form-control _change" id="select_direccion" v-model="area" name="area" @change="getRecords()">
                    <option value="">--TODO--</option>
                    <option :value="item.id" v-for="(item, index) in filtros.areas" :key="index"> @{{item.name}} </option> 
                </select>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="">Departamentos</label>
                <select class="cambiando form-control _change" id="select_sucursal" v-model="dpto" name="dpto" @change="getRecords()">
                    <option value="">--TODO--</option>
                    <option :value="item.id" v-for="(item, index) in filtros.dptos" :key="index"> @{{item.name}} </option> 
                </select>
            </div>
        </div>
        
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Antigüedad</label>
                <select class="cambiando form-control _change" id="select_inicio" v-model="antiguedad" name="antiguedad" @change="getRecords()">
                    <option value="">--TODO--</option>
                    <option :value="item.id" v-for="(item, index) in filtros.antiguedades" :key="index"> @{{item.name}} </option> 
                </select>
            </div>
        </div>

         <div class="col-md-4">
            <div class="form-group">
                <label for="">Puestos</label>
                <select class="cambiando form-control _change" id="select_job" v-model="puesto" name="puesto" @change="getRecords()">
                    <option value="">--TODO--</option>
                    <option :value="item.name" v-for="(item, index) in filtros.puestos" :key="index"> @{{item.name}} </option> 
                </select>
            </div>
        </div> 
            
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Turnos</label>
                <select class="cambiando form-control _change" id="selects_centro_trabajo" v-model="turno" name="turno" @change="getRecords()">
                    <option value="">--TODO--</option>                
                    <option :value="item.id" v-for="(item, index) in filtros.turnos" :key="index"> @{{item.name}} </option> 
                     
                </select>
            </div>
        </div>
    </div>
</div>

<div class="card-footer">
  <a href="#" class="btn btn-info _reset btn-sm float-right" @click="resetFiltros()"> Reiniciar Filtros</a>
</div>
