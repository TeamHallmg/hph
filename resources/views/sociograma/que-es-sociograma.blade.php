@extends('clima-organizacional.app')

@section('title', '¿Que es?')

@section('content')
<div class="row mt-3">
	<div class="col-md-2 text-right">
		@include('sociograma/partials/sub-menu')
	</div>
	<div class="col-md-10">
		@include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['banner'], 'section' => 'sociometria']) 
		{{-- <img class="img-fluid" src="/img/banner_clima_laboral.png" alt=""> --}}
	<h3 class="titulos-evaluaciones mt-4 font-weight-bold">¿Qué es?</h3>
	<div class="card">
		<div class="mx-3">
			<ul class="nav nav-pills mt-3" id="pills-tab" role="tablist">
				<li class="nav-item">
					<a class="btn-tab active" id="objetivo-tab" data-toggle="pill" href="#objetivo" role="tab" aria-controls="objetivo" aria-selected="true">Objetivo</a>
				</li>
				<li class="nav-item">
					<a class="btn-tab" id="informacion-tab" data-toggle="pill" href="#informacion" role="tab" aria-controls="informacion" aria-selected="false">Información General</a>
				</li>
			</ul>
		</div>
		<hr>
		<div class="card-body">
			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="objetivo" role="tabpanel" aria-labelledby="objetivo-tab">
					<div class="row">
						<div class="col-12 col-md-2">
							<img class="img-fluid" src="{{asset('img/objetivo_que_es.png')}}" alt="objetivo">
						</div>
						<div class="col-12 col-md-10">
							@include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['evaluacion'], 'section' => 'sociometria']) 
							{{-- <p class="color-personalizado-marron"><b>¡Bienvenidos!</b>Esta encuesta está diseñada para recopilar información sobre las areas que podrían estar afectando nuestro alto desempeño Y grado de alineamiento organizacional.</p>						</div> --}}
					</div>
				</div>
				<div class="tab-pane fade" id="informacion" role="tabpanel" aria-labelledby="informacion-tab">
					<div class="row">
						<div class="col-12 col-md-2">
							<img class="img-fluid" src="{{asset('img/informacion_que_es.png')}}" alt="información">
						</div>
						<div class="col-12 col-md-10 color-personalizado-marron">
							<p>Sus opiniones sobre estos temas guiarán los esfuerzos de la administración para hacer de nuestra organización un excelente lugar para trabajar, altamente competitiva.</p>
							<p><strong>Sus respuestas serán tratadas con la más estricta confidencialidad y su identidad no podrá ser revelada de ninguna manera.</strong></p>
							{{-- <p>
								<strong>El solicitar su nombre , es solo con fines de control de la aplicación de la encuesta y saber quiénes han respondido dicha encuesta en la que  solo AdGentis tiene acceso a ello</strong>
							</p> --}}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
  </div>
</div>
@endsection