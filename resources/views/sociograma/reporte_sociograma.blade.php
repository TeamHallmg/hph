@extends('clima-organizacional.app')

@section('title', 'Encuesta')

@section('content')
<div class="app">
<div class="row mt-3">
    <div class="col-md-2 text-right">
        @include('sociograma/partials/sub-menu')
    </div>
    <div class="col-md-10">
        <img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
        <h3 class="titulos-evaluaciones mt-4 font-weight-bold">SOCIOMETRÍA REPORTE GENERAL</h3>
        <hr>



        <form id="changePlanForm" action="/sociograma/reporte/exportar_sociograma" method="post">

        <div class="row">
            <div class="col-md-12"> 
                @include('sociograma.reporte_filtros')
            </div>
        </div> 

<div class="row">
    <div class="col-md-12"> 
        <div class="card mt-3">
            <h5 class="card-header bg-info text-white font-weight-bolder">
                COMPAÑEROS ELEGIDOS
                @csrf
                <button type="submit" class="float-right btn btn-success">
                    <i class="fas fa-file-excel"></i> Generar Reporte
                </button> 
            </h5>
            <div class="card-body text-center" v-cloak v-if="loading">
                <img :src="'/img/ajax-loader.gif'" alt="">
            </div>
            <div class="card-body" v-cloak v-else>

                <div class="row  pb-5"  v-for="(item, index) in preguntas" :key="index">

                    <div class="col-md-12">
                        <div class="row font-weight-bolder pb-1">
                            <div class="col-md-12" >
                                <div class="form-group">

                                    <label for="">Motivo de Elección</label>
                                    <input type="text" class="form-control" v-model="item.name" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row font-weight-bolder pb-2">
                            <div class="col-md-5" >
                                Nombre de Compañeros
                            </div>
                            <div class="col-md-3" >
                                Departamento
                            </div>
                            <div class="col-md-3" >
                                Puesto
                            </div>
                            <div class="col-md-1" style="display: flex;">
                                Totales
                            </div>
                        </div>
                        <div class="row pb-2"  v-for="(votacion, index_votacion) in item.mas_votados" :key="index_votacion">
                            <div class="col-md-5 d-flex align-items-center" >
                                @{{index_votacion+1}} <input type="text" class="form-control ml-2" v-model="votacion.name" readonly>
                            </div>
                            <div class="col-md-3" >
                                <input type="text" class="form-control" v-model="votacion.dpto" readonly>
                            </div>
                            <div class="col-md-3" >
                                <input type="text" class="form-control" v-model="votacion.puesto" readonly>
                            </div>
                            <div class="col-md-1" >
                                <input type="text" class="form-control" v-model="votacion.total" readonly>
                            </div>
                        </div>

                    </div>

                </div>
        </div>
    </div>
    
   
    
</div>



</div>
</form>
</div>
</div>
</div>
@endsection
@section('scripts_vuejs')

<script type="text/javascript">
 
 
var app = new Vue({
  el: '#app',
    created: function(){
        this.getRecords();
    }, 
  data: {
    loading:true,
    id_periodo:2,
    preguntas:[],
     
    filtros: {   
        periodos : [],
        dptos : [],
        areas : [],
        turnos : [],
        puestos : [],
        centros_trabajo : [],
        regiones : [],
        antiguedades : [],
        }, 
        dpto : '',
        area : '',
        turno : '',
        centro_trabajo : '',
        puesto : '',
        region : '',
        antiguedad : '',
    
  },
    computed: {
        
    },
    methods:{
 
        resetFiltros: function(){
            this.centro_trabajo = '';
            this.dpto = '';
            this.area = '';
            this.puesto = '';
            this.region = '';
            this.antiguedad = '';
            this.turno = '';
            this.getRecords();
        },
        getRecords: function(){
            var seriesas = [];
            var dpto = '';
            this.loading =true;
            var url = "/sociograma/reporte/sociograma";

 
            if (this.area!='') {
                dpto = this.dpto;
            }else{ 
                var dpto_id = _.find(this.filtros.dptos, ['id', this.dpto]);
                if (dpto_id!=undefined) {
                    dpto = dpto_id['name'];
                } 
            }
            console.log(dpto);

            axios.post(url,{
                id_periodo:this.id_periodo,
                dpto:dpto,
                area:this.area,
                centro_trabajo:this.centro_trabajo,
                puesto:this.puesto,
                region:this.region,
                antiguedad:this.antiguedad,
                turno:this.turno
            }).then(response=>{
                


                console.log(response.data);


                this.loading = false;
                this.preguntas = response.data.preguntas;
                
                

                this.filtros.periodos =response.data.periodos;                
                if(this.centro_trabajo==''){
                    this.filtros.centros_trabajo = response.data.centros_trabajo;
                }
                if(this.dpto==''){
                    this.filtros.dptos = response.data.dptos;
                }
                if(this.turno==''){
                    this.filtros.turnos = response.data.turnos;
                }
                if(this.area==''){
                    this.filtros.areas = response.data.areas;
                }
                if(this.puesto==''){
                    this.filtros.puestos = response.data.puestos;
                } 
                if(this.region==''){
                    this.filtros.regiones = response.data.regiones;
                }
                if(this.antiguedad==''){
                    this.filtros.antiguedades = response.data.antiguedades;
                }
 
                
            })
        },

    },
})

</script>
@endsection