@extends('clima-organizacional.app')

@section('title', 'Encuesta')

@section('content')
<div class="app">
<div class="row mt-3">
    <div class="col-md-2 text-right">
        @include('sociograma/partials/sub-menu')
    </div>
    <div class="col-md-10" v-cloak>
        <img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
        <h3 class="titulos-evaluaciones mt-4 font-weight-bold">SOCIOMETRÍA REPORTE INDIVIDUAL</h3>
        <hr>
 
        <form id="changePlanForm" action="/sociograma/reporte/exportar_sociograma_individual" method="post">

        <div class="row">
            <div class="col-md-12"> 
                <div class="card mt-3">
                    <h5 class="card-header bg-info text-white font-weight-bolder">
                        Datos
                        {{-- <button @click="getRecords()"></button> --}}
                    </h5>
                    <div class="card-body">
                        @include('sociograma.reporte_filtros_detallado')
                    </div>
                </div>
            </div>
        </div> 

<div class="row">
    <div class="col-md-12"> 
        <div class="card mt-3">
            <h5 class="card-header bg-info text-white font-weight-bolder">
                Resultado 
                
                @csrf
                <button type="submit" class="float-right btn btn-success">
                    <i class="fas fa-file-excel"></i> Generar Reporte
                </button> 

            </h5>
            {{-- <div class="card-body text-center" v-cloak v-if="loading">
                <img :src="'/img/ajax-loader.gif'" alt="">
            </div> --}}
            {{-- <div class="card-body" v-cloak v-else> --}}
            <div class="card-body">

                <div class="row">
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="" class="font-weight-bolder">Factor Evaluado</label>
                            <select class="cambiando form-control _change" id="select_periodo" name="periodos" v-model="question">
                                <option value="" disabled="">Seleccionar...</option>
                                <option :value="item.short_name" v-for="(item, index) in questions" :key="index"> @{{item.short_name}} </option> 
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4 offset-4">
                        <div class="form-group">
                            <label for="" class="font-weight-bolder">Tablero de Código de color</label>
                            <table class="table table-sm table-bordered">
                                <tr v-for="(code, index_code) in code_color" :key="index_code" :class="code.color">
                                    <td>
                                        @{{code.name}}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </div>      

                <div class="row">
                    
                    <div class="col-md-6">

                            <table class="table table-sm table-bordered">                               
                               <thead style="background-color: #7C75B7; color: white">
                                   <tr class="text-center">
                                       <th style="width: 10%">
                                           Número de personas que les siguen
                                       </th>
                                       <th style="width: 10%">
                                           Personas que me eligieron
                                       </th>
                                       <th style="width: 20%">
                                           Nombre
                                       </th>
                                   </tr>
                               </thead>
                               <tbody>
                                   <tr :class="su_respuesta.color" v-for="(su_respuesta, index_su_respuesta) in resultado.sus_respuestas" :key="index_su_respuesta" >
                                       <td class="text-right">@{{su_respuesta.votaron_por_el}}</td>
                                       <td class="text-right">@{{su_respuesta.user.employee_wt.idempleado}}</td>
                                       <td>@{{su_respuesta.user.first_name}} @{{su_respuesta.user.last_name}}</td>
                                   </tr> 
                                   <tr style="background-color: #7C75B7; color: white">
                                       <td class="text-right">@{{ resultado.sus_respuestas | sumaTotales('votaron_por_el') }}</td> 
                                       <td class="text-right">@{{(resultado)?resultado.sus_respuestas.length:0}}</td>
                                       <td></td>
                                       
                                   </tr> 

                               </tbody>

                            </table>
                    </div>
                    <div class="col-md-6">

                            <table class="table table-sm table-bordered">                               
                               <thead style="background-color: #7C75B7; color: white">
                                   <tr class="text-center">
                                       <th style="width: 10%">
                                           Personas a quienes yo elegí
                                       </th>
                                       <th style="width: 20%">
                                           Nombre
                                       </th>
                                       <th style="width: 10%">
                                           Número de personas que les siguen
                                       </th>
                                   </tr>
                               </thead>
                               <tbody>
                                   <tr :class="mi_respuesta.color" v-for="(mi_respuesta, index_mis_respuesta) in resultado.mis_respuestas" :key="index_mis_respuesta" >
                                       <td class="text-right">@{{mi_respuesta.respuesta_empleado.employee_wt.idempleado}}</td>
                                       <td>@{{mi_respuesta.respuesta_empleado.first_name}} @{{mi_respuesta.respuesta_empleado.last_name}}</td>
                                       <td class="text-right">@{{mi_respuesta.votaron_por_el}}</td>
                                   </tr>
                                   <tr style="background-color: #7C75B7; color: white">
                                       <td class="text-right">@{{(resultado)?resultado.mis_respuestas.length:0}}</td>
                                       <td></td>
                                       <td class="text-right">@{{ resultado.mis_respuestas | sumaTotales('votaron_por_el') }}</td>
                                       
                                   </tr> 
                               </tbody>

                            </table> 
                    </div>
                </div>

        </div>
    </div>
    
   
    
</div>



</div>
</div>
</form>
</div>
</div>
@endsection
@section('scripts_vuejs')

<script type="text/javascript">
Vue.component('v-select', VueSelect.VueSelect);
var app = new Vue({
  el: '#app',
    created: function(){
        this.loadPeridos();
    }, 
  data: {
    code_color:[
    {
        name:'Coincidencia de Medición',
        color:'bg-success'
    },{
        name:'Coincidencia de Medición',
        color:'bg-warning'
    },{
        name:'Coincidencia de Medición',
        color:'bg-info'
    },{
        name:'Coincidencia de Medición',
        color:''
    }
    ],

    existe:true,
    loading:false,
    loadingData:false,
    question:'',
    periodos : [],
    id_periodo:'',
    id_empleado:'',
    empleado:'',
    mis_respuestas:[],
    questions:[],
    evaluados:[],
       
    
  },
    methods:{
 
        loadPeridos: function(){ 
          axios.get('/sociograma/getPeriodos').then(response=>{
              this.periodos =response.data.periodos;        
            })
        },
        loadEmployees: function(){ 
          this.loadingData =true;
          axios.get('/sociograma/cargarEmpleadosPeriodo/'+this.id_periodo).then(response=>{
              console.log(response.data.evaluados);  
              this.evaluados = response.data.evaluados;
              this.loadingData =false;
            })
        },
        getRecords: function(){ 
            this.loading =true;
            var url = "/sociograma/reporte/sociograma-individual";
            axios.post(url,{
                id_empleado:this.id_empleado, 
                id_periodo:this.id_periodo
            }).then(response=>{
                this.loading =false;
                this.existe = response.data.result;
                this.empleado = response.data.empleado;
                this.mis_respuestas = response.data.resultados;
                this.questions = response.data.questions;
                console.log(response.data);
                
            })
        },

    },
    computed: {
        resultado: function () {     

            var data = this.mis_respuestas;

            var question = this.question;

            result =  _.filter(data, ['name', question]);

            if(result.length>0){
                return result[0];   
            }else{
                return false;                
            }
        },
        empleados: function () {     
 
        }
    },
    filters:{
      sumaTotales: function (array, key) {    
            return _.sumBy(array, key); 
        }
    }
})

</script>
@endsection