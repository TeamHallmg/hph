<div class="card-body">
     <div class="row">
        <div class="col-md-6 pb-4"> 
            <div class="form-group">
                <label for="" class="font-weight-bolder">Buscar en Periodo</label>
                <select class="cambiando form-control _change" id="select_periodo" name="periodos" v-model="id_periodo" @change="loadEmployees()">
                    <option value="" disabled="">Seleccionar...</option>
                    <option :value="item.id" v-for="(item, index) in periodos" :key="index"> @{{item.name}} </option> 
                </select>
            </div>
        </div>
        <div class="col-md-6 pb-4">
            <label for="" class="font-weight-bolder">Buscar Empleado</label>
            <div class="input-group">    
                <v-select
                class="w-100"
                    v-model="id_empleado"
                    placeholder="Seleccione un empleado"
                    :reduce="nombre => nombre.id" 
                    label="nombre"
                    :options="evaluados"
                    @input="getRecords()"
                >
                    <template #spinner="{ loading }">
                      <div
                        v-if="loadingData"
                      >
                          <span class="spinner-grow spinner-grow-sm bg-primary" role="status" aria-hidden="true"></span>
                        <span class="sr-only">Loading...</span>
                      </div>
                    </template>
                </v-select>
            </div> 

            <span v-if="loading">
              <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
              <span class="sr-only">Loading...</span>
            </span>

        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="">Id del Empleado</label>
                <input type="hidden" class="form-control" v-model="id_empleado" name="id_empleado" readonly="">
                <input type="text" class="form-control" v-model="empleado.id" readonly="">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Nombre</label>
                <input type="text" class="form-control" v-model="empleado.nombre" readonly="">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Puesto</label>
                <input type="text" class="form-control" v-model="empleado.puesto" readonly="">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Departamento</label>
                <input type="text" class="form-control" v-model="empleado.departmento" readonly="">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Hospital</label>
                <input type="text" class="form-control" v-model="empleado.hospital" readonly="">
            </div>
        </div>
        
        
    </div>
</div>

