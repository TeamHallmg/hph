<h4 class="font-weight-bold  color-personalizado-azul" style="margin-top: 15px;">SOCIOMETRÍA</h4>
<hr>
<ul class="nav nav-pills flex-column">
	<li class="nav-item"><a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('sociograma/que-es') }}">Información General</a></li>
	<li class="nav-item">

		@if (auth()->user()->role == 'admin' || auth()->user()->role == 'supervisor' || isset($userPermissions['see_progress']) || auth()->user()->hasRolePermission('show_only_region_sociograma') || auth()->user()->hasSociogramaPermissions(1))
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('sociograma/avances') }}">Avances</a></li>
		@endif
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_questions']) || auth()->user()->hasSociogramaPermissions(2))
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('sociograma/preguntas') }}">Preguntas</a></li>
			@endif
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_questions']) || auth()->user()->hasSociogramaPermissions(3))
		<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('sociograma/etiquetas') }}">Etiquetas</a></li>
		@endif
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_periods']) || auth()->user()->hasSociogramaPermissions(4))
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('sociograma/periodos') }}">Periodos</a></li>
		@endif
		@if (auth()->user()->role == 'admin')
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('sociograma/permissions') }}">Permisos</a></li>
		@endif
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_periods']) || auth()->user()->hasSociogramaPermissions(5))
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('sociograma/reporte/sociograma') }}">Reporte Sociometría</a></li>
		@endif
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_periods']) || auth()->user()->hasSociogramaPermissions(6))
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('sociograma/reporte/sociograma-individual') }}">Reporte Sociometría Individual</a></li>
		@endif
		@if (auth()->user()->role == 'admin')
			<a class="nav-link color-personalizado-azul font-weight-bold" href="{{ url('sociograma/plan-correos') }}">Plan de Correos</a></li>
		@endif
</ul>