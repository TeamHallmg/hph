@extends('layouts.app')

@section('title', 'Editar niveles de puesto')

@section('content')
<div class="row">
	<div class="col-md-12">
		<img class="img-fluid" src="{{ asset('img/clima-organizacional/banner.png') }} " alt="">
      	<h3 class="titulos-evaluaciones mt-3 font-weight-bold">Editar nivel de puesto</h3>
      	<hr class="mb-3">
      	<form method="post" action="/editar-nivel-puesto/">
      		{{ csrf_field() }}
					<input type="hidden" value="{{$id}}" name="id"> 
			<div class="row">
				<div class="col-md-9">
					<div class="form-group">
						<label for="nombreNivelPuesto">Nombre del nivel de puesto:</label>
						<input type="text" class="form-control" required name="nombreNivelPuesto" value="{{$nombreNP}}">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label for="mandoNivelPuesto">Mando:</label>
						<select class="form-control" required name="mandoNivelPuesto">
							<option value="0">No</option>
							<option value="1" <?php if ($mando == 1){ ?>selected="selected"<?php } ?>>Si</option>
						</select>
					</div>
				</div>
			</div>


				<div class="form-group">
						<table class="table table-striped table-bordered td-responsive" id="table-puestos">
							<thead style="background-color: #222B64; color:white;">
								<tr>
									<th>
										ID
									</th>
									<th >
										Nombre
									</th>
									<th>
										Acción
									</th>
								</tr>
							</thead>
							<tbody>
								@if($puestos->isEmpty())
									<tr>
										<td colspan="3">No hay puestos para seleccionar.</td>
									</tr>
								@endif
								@foreach($puestos as $key => $puesto)
										<tr>
											<td>{{ $puesto->id }}</td>
											<td>{{ str_replace('Ã‘','Ñ',$puesto->puesto) }}</td>
											<td>
												<input type="checkbox" name="id_puestos[]" value="{{$puesto->id}}" class="field" <?php if (in_array($puesto->id, $selected)){ ?>checked="checked"<?php } ?>>
											</td>
										</tr>
								@endforeach
							</tbody>
						</table>
				</div>

				<div class="form-group">
					<button type="submit" class="btn btn-success">
						<i class="fas fa-check-circle"></i> Guardar
					</button>
					<a class="btn btn-primary" href="{{ URL::previous() }}"><span class="fas fa-chevron-circle-left"></span> Regresar</a>
				</div>
		</div>
		
	</div>
</div>
@endsection

@section('scripts')
<script>
	
	/*$('#table-puestos').DataTable({
    language: {
        "sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
            }
    });*/
	
	$('#table-puestos tbody tr').click(function (e) {
    if(!$(e.target).is('#table-puestos td input:checkbox'))
    $(this).find('input:checkbox').trigger('click');
});
</script>
@endsection