@extends('layouts.app')

@section('title', 'Puestos')

@section('content')
<div class="row">
	<div class="col-md-12">
		<img class="img-fluid" src="{{ asset('img/clima-organizacional/banner.png') }} " alt="">
			<h3 class="titulos-evaluaciones mt-3 font-weight-bold">Niveles de puesto</h3>
			<hr class="mb-3">
		<center>
			<a class="btn btn-primary" href="{{ url('puestos/create') }}"><i class="fas fa-plus-circle"></i> Nuevo nivel de puesto</a>
		</center>
		<br>
		<table class="table table-striped table-bordered puestos dt-responsive">
			<thead style="background-color: #222B64; color:white;">
				<tr>
					<th>ID</th>
					<th>Nivel de puesto</th>
					<th>Puestos</th>
					<th>Mando</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>

			@if (!$nivelesPuestos->isEmpty())

      	@foreach($nivelesPuestos as $nPuestos)
      		<tr>
						<td>{{ $nPuestos->id }}</td>
						<td>{{ str_replace('Ã‘','Ñ',$nPuestos->Name) }}</td>
						<td>
							@foreach($puestos_NivelesPuestos as $nivelesPuestos)
								@foreach($nivelesPuestos as $nivelPuesto)
									@if($nPuestos->id == $nivelPuesto->id_nivel_puesto)
										{{ str_replace('Ã‘','Ñ',$nivelPuesto->Puestos->puesto) }}
										<br>
									@endif
								@endforeach
							@endforeach
						</td>
						<td>
							@if( $nPuestos->mando == 0 )
								No
							@else
								Si
							@endif
						</td>
						<td>

							@if ($nPuestos->id != 1)

								<a class="btn btn-primary" href="{{ url('puestos/' . $nPuestos->id . '/edit') }}"><i class="fas fa-edit"></i> Editar</a>
							@endif
						</td>
					</tr>
				@endforeach
			@endif
			</tbody>
		</table>
	</div>
</div>
@endsection
@section('scripts')
<script>
	$('.puestos').DataTable({
		language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
   	});
</script>
@endsection