@extends('clima-organizacional.app')

@section('title', 'Crear Permiso')

@section('content')
<div class="row mt-3">
    <div class="col-md-2 text-right">
        @include('sociograma/partials/sub-menu')
    </div>
    <div class="col-md-10">
     <img src="/img/banner_clima_laboral.png" alt="" class="img-fluid">
<h3 class="titulos-evaluaciones mt-4 font-weight-bold text-client-primary">Crear Permiso</h3>

<form action="{{ url('sociograma/permissions') }}" method="post" id="period_form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="user_id" class="font-weight-bold requerido">Usuario</label>
                        <input type="text" class="form-control users_autocomplete" required>
                        <input type="hidden" name="user_id" class="user_id">
                    </div>
                    <div class="form-group">
                        <label for="region_id" class="font-weight-bold">Hospital</label>
                        <select class="form-control" name="region_id">
                          <option value="0">Todos los Hospitales</option>

                      @foreach ($regions as $key => $value)
                        
                          <option value="{{$value->id}}">{{$value->name}}</option>
                      @endforeach

                        </select>
                    </div>
                    <div class="form-group">
                        <label for="section_id" class="font-weight-bold">Sección</label>
                        <select class="form-control" name="section_id[]" multiple>

                      @foreach ($sections as $key => $value)
                        
                          <option value="{{$value->id}}">{{$value->section}}</option>
                      @endforeach

                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row p-4">
        <div class="col-12">
            <div class="float-right">
                <button class="btn btn-success mr-3" type="submit" style="color: white"><span class="fas fa-check-circle"></span> Crear</button> 
                <a class="btn btn-danger" href="{{ url('sociograma/permissions') }}"><span class="fas fa-times-circle"></span> Regresar</a>
            </div>
        </div>
    </div>
</form>
</div>
</div>
@endsection

@section('scripts')
  <script>

    var id_user = 0;
    var users = <?php echo json_encode($users)?>;

    $(document).ready(function(){
        
      $('.users_autocomplete').autocomplete({
      
        lookup: users,
        minChars: 0,
        onSelect: function (suggestion){

          id_user = suggestion.data;
          $('input.user_id').val(id_user);
        }
      });
    });
  </script>
@endsection
