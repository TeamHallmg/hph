@extends('layouts.app')

@section('title', 'Crear Etiqueta')

@section('content')
<div class="row">
	<div class="col-md-2 text-right" style="padding: 0">
		@include('sociograma/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
      <h3 class="titulos-evaluaciones mt-4 font-weight-bold">Crear Etiqueta</h3>
      <hr class="mb-4">


		<form action="/sociograma/crear-etiqueta" method="post" enctype="multipart/form-data">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="row">
        <div class="col-md-6">
          
          <div class="form-group">
            <label for="name" class="font-weight-bold">Nombre</label>
            <input type="text" class="form-control" name="name" required>
          </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="valor" class="font-weight-bold">Valor</label>
          <input type="text" class="form-control" name="valor" required>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="color" class="font-weight-bold">Color</label>
          <input type="color" class="form-control size" name="color">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="file" class="font-weight-bold">Archivo (Imagen)</label>
          <input type="file" class="form-control" name="file">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <button class="btn btn-success mr-3" type="submit" style="color: white"><span class="fas fa-check-circle"></span> Crear</button> 
          <a class="btn btn-danger" href="/sociograma/etiquetas"><span class="fas fa-times-circle"></span> Regresar</a>
        </div>
      </div>
      </div>
    </form>

</div>
</div>
@endsection