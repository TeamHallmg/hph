@extends('layouts.app')

@section('title', 'Editar Etiqueta')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-2 text-right" style="padding: 0">
		@include('sociograma/partials/sub-menu')
	</div>
	<div class="col-md-10">
		  <img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
      <h3 class="titulos-evaluaciones mt-4 font-weight-bold">Editar Etiqueta</h3>
      <hr class="mb-4">

	<div class="col mx-auto">
		<form action="/sociograma/editar-etiqueta" method="post" enctype="multipart/form-data">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id" value="{{$etiqueta[0]->id}}">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="name">Nombre</label>
            <input type="text" class="form-control" name="name" required value="{{$etiqueta[0]->name}}">
          </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="valor">Valor</label>
          <input type="text" class="form-control" name="valor" required value="{{$etiqueta[0]->valor}}">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="color">Color</label>
          <input type="color" class="form-control size" name="color" <?php if (!empty($etiqueta[0]->color)){ ?>value="{{$etiqueta[0]->color}}"<?php } ?>>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="file">Archivo (imagen)</label>
          <input type="file" class="form-control" name="file"> 
        </div>
        <div class="form-group text-center">
          @if (!empty($etiqueta[0]->file))<img src="/img/sociograma/etiquetas/{{$etiqueta[0]->id}}/{{$etiqueta[0]->file}}" style="max-width: 150px;">@endif
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <button class="btn btn-success mr-3" type="submit" style="color: white"><span class="fas fa-check-circle"></span> Guardar</button> 
          <a class="btn btn-danger" href="/sociograma/etiquetas"><span class="fas fa-times-circle"></span> Regresar</a>
        </div>
      </div>
      </div>
    </form>
	</div>
	<div class="col-md-2"></div>

  </div>
</div>
@endsection