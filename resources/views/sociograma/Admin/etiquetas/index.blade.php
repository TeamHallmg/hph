@extends('layouts.app')

@section('title', 'Etiquetas')

@section('content')
<div class="row">
	<div class="col-md-2 text-right" style="padding: 0">
		@include('sociograma/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
		<h3 class="titulos-evaluaciones mt-4 font-weight-bold">Etiquetas</h3>
		<hr class="mb-4">

		@if (auth()->user()->role === 'admin')

		<a href="/sociograma/etiquetas/create" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Agregar Etiqueta</a>
		@endif

	<div class="row mt-4">
		<div class="col-md-12">
		<table class="table table-striped table-bordered etiquetas dt-responsive">
			<thead style="background-color: #222B64; color:white;">
				<tr>
					<th>Nombre</th>
					<th>Valor</th>
					<th>Color</th>
					<th>Archivo (Imagen)</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>

			@for ($i = 0;$i < count($etiquetas);$i++)

				<tr>
					<td>{{$etiquetas[$i]->name}}</td>
					<td>{{$etiquetas[$i]->valor}}</td>
					<td>@if (!empty($etiquetas[$i]->color))<input type="color" class="form-control size" value="{{$etiquetas[$i]->color}}" style="padding: 0">@endif</td>
					<td>@if (!empty($etiquetas[$i]->file))<img src="/img/sociograma/etiquetas/{{$etiquetas[$i]->id}}/{{$etiquetas[$i]->file}}" style="max-width: 150px;">@endif</td>
					<td class="text-center">
						<a href="/sociograma/etiquetas/{{$etiquetas[$i]->id}}/edit" class="btn btn-primary"><i class="fas fa-edit"></i> Editar</a>
					</td>
				</tr>
			@endfor
			</tbody>
		</table>
	</div>
	</div>
	</div>
</div>


@endsection
@section('scripts')

<script type="text/javascript">
	$('.etiquetas').DataTable({
		'order': [[1,'asc']],
		language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
   	});
</script>

@endsection