@extends('clima-organizacional.app')

@section('title', 'Preguntas')

@section('content')

<div class="row mt-3">
	<div class="col-md-2 text-right">
		@include('sociograma/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
<h3 class="titulos-evaluaciones mt-4 font-weight-bold">Preguntas</h3>
<hr>
<div class="row">
	<div class="col-md-12 col-sm-9">
        <form method="post" action="{{ url('sociograma/preguntas/' . $pregunta->id) }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="PUT">
			<div class="card">
				<div class="card-header">
					Crear Pregunta
				</div>
				<div class="card-body">	
					<div class="form-group">
						<label for="question" class="requerido">Pregunta</label>
                        <textarea name="question" class="form-control" placeholder="Escribe la pregunta" style="resize: none" cols="2" rows="2" required>{{$pregunta->question}}</textarea>
					</div>
					<div class="form-group">
						<label for="positive" class="requerido">Sentido</label>
						<select class="form-control" name="positive">
							<option value="1">Sentido Positivo</option>
                            <option value="0" {{ ($pregunta->positive == 0)?"selected":''}} >Sentido Negativo</option>
						</select>
					</div>
				</div>

				<div class="card-footer">
					<div class="float-right">
						<button type="submit" class="btn btn-success">
							<i class="fas fa-check-circle"></i> Guardar Pregunta
						</button>
					</div>
				</div>
			</div> 
		</form>
	</div>
</div>
</div>
</div>

@endsection