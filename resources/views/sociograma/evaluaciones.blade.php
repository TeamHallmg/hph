@extends('clima-organizacional.app')

@section('title', 'Encuesta')

@section('content')

<div class="row mt-3">
	<div class="col-md-2 text-right">
		@include('clima-organizacional/partials/sub-menu')
	</div>
	<div class="col-md-10">
	<img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
<h3 class="titulos-evaluaciones mt-4 font-weight-bold">Encuesta</h3>
<div class="card">
	<div class="mx-3">
		<ul class="nav nav-pills mt-3" id="pills-tab" role="tablist">
			<li class="nav-item">
				<a class="btn-tab active" id="intrucciones-tab" data-toggle="pill" href="#intrucciones" role="tab" aria-controls="intrucciones" aria-selected="true">Intrucciones</a>
			</li>
		</ul>
	</div>
	<hr>
	<div class="card-body">
		@include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['evaluacion'], 'section' => 'sociograma']) 
		<div class="tab-content" id="myTabContent">
			<div class="tab-pane fade show active" id="intrucciones" role="tabpanel" aria-labelledby="intrucciones-tab">
				<p>¡El trabajo de tus sueños, tú lo creas!</p>
				<p>Sigamos haciendo de HPDH el espacio en donde logremos disfrutar de nuestro trabajo al máximo y dar los mejores resultados. Ayúdanos a seguir mejorando, te invitamos a emitir tu opinión y propuestas para cada uno de los temas que hemos puesto a tu disposición.</p>
				<p>A través de esta encuesta buscamos identificar en nuestra cultura lo que nos hace fuertes y los temas en los que debemos trabajar para mejorarlos. Es un espacio en dónde puedes sentirte en total confianza al responder cada una de las preguntas, pues tus datos y respuestas se mantendrán en total anonimato y confidencialidad.</p>
				<p>Sepanka Suite es un proveedor externo que nos ayuda a garantizar el anonimato, al ser ellos los únicos que manejan la información comprometiéndose a trabajar con honestidad y respeto.</p>
				<p>Los resultados finales nos ayudarán a establecer planes encaminados a establecer planes de mejora.</p>
				<p>Agradecemos de antemano tu compromiso, sinceridad y confianza ya que tu aportación será clave para el desarrollo de HPDH.</p>
				<p>¡Gracias por participar!</p>
				<div class="row mt-3">
					<div class="col-md-12 text-center">
						<video src="/videos/clima/Video Tutorial Clima Laboral.mp4" width="100%" controls></video>
					</div>
				</div>
				<p class="text-center">
				@if ($status->status < 3)
					@if ($status->status == 1)
					<a class="btn btn-primary" href="{{ url('clima-organizacional/evaluacion') }}">Entrar a la Encuesta</a>
					@else
					<a class="btn btn-warning" href="{{ url('clima-organizacional/evaluacion') }}">Completa la Encuesta</a>
					@endif
				@else
					<!--<button class="btn btn-success" style="cursor: default">Encuesta Terminada</button>-->
					<a class="btn btn-success" href="{{ url('clima-organizacional/evaluacion') }}">Encuesta Terminada</a>
				@endif
				</p>
			</div>
		</div>
	</div>
</div>
</div>
</div>
@endsection
