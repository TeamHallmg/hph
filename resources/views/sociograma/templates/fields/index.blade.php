@extends('layouts.app')
@section('content')

	<div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
	</div>
	<br>
	
	
	<div class="card mt-3">
		<h5 class="card-header bg-info text-white font-weight-bolder">Administrar Campos Dinámicos
	<a href="{{url('/home')}}" class="btn btn-sm float-right btn-success">Regresar</a>
		  
  </h5>
		<div class="card-body">

<div class="row justify-content-center mb-3">
    <div class="col-md-4">
        <a  href="{{ url('templates-fields/create') }}" class="btn btn-primary btn-block"> Nuevo Campo</a>
    </div>
</div>

<div class="table-responsive">
		<div class="col-md-12 tableWrapper">
		<table class="table table-hover table-bordered" id="tableBienes">
			<thead  class="bg-purple text-white">
				<tr>
					<th>#</th>
					<th>Etiqueta</th>
					<th>Descripción</th>
					<th>Atributo</th>
					<th>Estatus</th>
					<th>Opciones</th>
					{{-- <th>ruta</th> --}}
				</tr>
			</thead>
		<tbody>
			
			@foreach($records as $record)
				<tr>
					<td>{{ $record->id }}</td>
					<td>{{ $record->label }}</td>
					<td>{{ $record->description }}</td> 
					<td>{{ $record->attribute }}</td> 
					<td>
	 					@if($record->status==1)
	                        Disponible
	                    @else
	                    	No Disponible
	                    @endif
					</td>
					<td nowrap>
						<a class="btn btn-sm btn-warning" href="{{ url('templates-fields/' . $record->id.'/edit') }}" title="Editar datos"> <i class="fa fa-edit"></i> </a>
					</td>
				</tr>

			@endforeach

		</tbody>
	</table>
		</div>
	</div>
	</div>
	</div>
@endsection

@section('scripts')

<script type="text/javascript">
	$('#tableBienes').DataTable({
		language: {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
   	});
</script>
@endsection