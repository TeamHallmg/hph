@extends('clima-organizacional.app')

@section('title', 'Lista de Colaboradores')

@section('content')

	<div class="row">

		<div class="col-md-2 text-right">
		@include('clima-organizacional/partials/sub-menu')
		</div>

		<div class="col-md-10">
			
			<img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">

			<form action="{{ url('send_correos_masivos') }}" method="post" id="period_form">
				<div class="row">
					
					<div class="col-md-12">
						
						<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Enviar Invitaciones Masiva a Colaboradores</h3>
						
						<hr class="mb-5">

					
						@if (count($periodos) > 0)
							<div class="row">
								<div class="col-md-4 mx-auto mb-4">
									<label for="periodos">Periodos</label>
									<select id="periodos" class="form-control">

										@foreach ($periodos as $key => $value)

										<option value="{{$value->id}}" <?php if ($value->id == $period_id){ ?>selected="selected"<?php } ?>>{{$value->name}}</option>
							@endforeach

									 
									</select>
								</div>
								<div class="col-md-4 mx-auto mb-4">
									<label for="status">Estatus</label>
									<select id="status" class="form-control">
										<option value="todos" {{($status == 'todos'?'selected':'')}}>Todos...</option>
										<option value="1" {{($status == '1'?'selected':'')}}>No Iniciada</option>
										<option value="2" {{($status == '2'?'selected':'')}}>Inconclusa</option>
									</select>
								</div>
								<div class="col-md-4 mx-auto mb-4">
									<label for="template_id">Plantillas</label>
									<select id="template_id" name="template_id" class="form-control" required>
										<option disabled value="" selected>Seleccione una opción...</option>
										@foreach($plantillas as $plantilla)
											<option value="{{$plantilla->id}}" >{{$plantilla->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
						
							<div class="table-responsive">
								<table id="table-evaluados" class="table table-striped table-bordered listado w-100">
									<thead style="background-color: #222B64; color:white;">
										<tr>
											<th>ID</th>
											<th>Rfc</th>
											<th>Evaluado</th>
											<th>Correo</th>
											<th>Empresa</th>
											<th>Departamento</th>
											<th>Área</th>
											<th>Puesto</th>
											<th>Empresa</th>
											<th>Estado</th>
											<th class="mx-auto">
												Todos
												<input type="checkbox" class="select_all_evaluados" id="users_check">
											</th>
										</tr>
									</thead>
									<tbody>
										@if (count($evaluados) > 0)							
											@foreach ($evaluados as $eval)
										
										<tr>
					                        <td class="text-center">
					                            {{ (!empty($eval->employee) ? $eval->employee->idempleado : '') }}
					                        </td>
					                        <td class="text-center">
					                            {{ (!empty($eval->employee) ? $eval->employee->rfc : '')}}
					                        </td>
					                        <td class="text-center">
					                            {{ $eval->FullName }}
					                        </td>
					                        <td class="text-center">
					                            {{ $eval->email }}
					                        </td>
					                        <td class="text-center">
					                            {{ (isset($eval->employee->enterprise) ? $eval->employee->enterprise->name : '') }}
					                        </td>
					                        <td class="text-center">
					                            {{ (isset($eval->employee->jobPosition->area->department) ? $eval->employee->jobPosition->area->department->name : '') }}
					                        </td>
					                        <td class="text-center">
					                            {{ $eval->employee->direccion }}
					                        </td>
					                        <td class="text-center">
					                            {{ (!empty($eval->employee->jobPosition) ? $eval->employee->jobPosition->name : '') }}
					                        </td>
					                        <td class="text-center">
												{{ (!empty($eval->employee->enterprise) ? $eval->employee->enterprise->name : '')}}
					                        </td>
					                        <td class="text-center">
					                            @if($eval->pivot->status == 1)
					                                <button class="btn btn-danger btn-block btn-static" style="cursor: default;">No Iniciada</button>
					                            @elseif($eval->pivot->status == 2)
					                                <button class="btn btn-warning btn-block btn-static" style="cursor: default;">En Proceso</button>
					                            @else
					                                <button class="btn btn-success btn-block btn-static" style="cursor: default;">Terminada</button>
					                            @endif
					                        </td>
											
											<td>
												<input type="checkbox" name="M" value="{{$eval->id}}" class="evaluados">
											</td>
										</tr>
										@endforeach
										@endif
									</tbody>
								</table>
							</div>
						@endif
					</div>

					
					<div class="col-12">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						
						<h4 class="mensaje_empleados py-5"></h4>
				
						<div class="float-right">
							<button class="btn btn-success mr-3" type="submit" style="color: white"><span class="fas fa-check-circle"></span> Enviar Invitaciones</button> 
							<a class="btn btn-danger" href="{{ url('/') }}"><span class="fas fa-times-circle"></span> Regresar</a>
						</div>
					</div> 
				</div> 
			</form>

			
			<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Enviar Correo de Prueba</h3>
						
			<hr class="mb-5">

			<form action="{{ url('send_correos_masivos_test') }}" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="row mt-4">
					<div class="col-md-4 mx-auto mb-4">
						<label for="template_test_id">Plantillas</label>
						<select id="template_test_id" name="template_test_id" class="form-control" required>
							<option disabled value="" selected>Seleccione una opción...</option>
							@foreach($plantillas as $plantilla)
								<option value="{{$plantilla->id}}" >{{$plantilla->name}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-4 mx-auto mb-4">
						<label for="template_test_id">Email</label>
						<input type="email" name="email_test" class="form-control" placeholder="Email">
					</div>
					<div class="col-md-4 mx-auto mb-4">
						<label class="text-white d-block">enviar</label>
						<button type="submit" class="btn btn-primary">Enviar Prueba</button>
					</div>
				</div>
			</form>

		</div>
		
	</div>

	<form action="/clima-organizacional/invitaciones-masivas" method="post" class="form_estadisticas">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="period_id" value="{{$period_id}}" class="period_id">
		<input type="hidden" name="status" value="{{$status}}" class="status">
		<input type="submit" style="display: none">
	</form> 
@endsection

@section('scripts')


<script>

    var checked = false;
    var total_users = 0;
    var usersTable = 0;
 
$(document).ready(function(){

    usersTable = $('#table-evaluados').DataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
    });

		
        $('body').on('click', '.select_all_evaluados, .evaluados', function(){
            
            if ($(this).hasClass('select_all_evaluados')){
            
              var allPstarted_ats = usersTable.rows({ search: 'applied' }).nodes();
              checked = !checked;			  
              if (checked){
                $('input[type="checkbox"]', allPstarted_ats).prop('checked', true);
              }else{
                $('input[type="checkbox"]', allPstarted_ats).prop('checked', false);
              }
            }

            var users_array = usersTable.$('input').serializeArray()
            total_users = users_array.length;
            $('.btn-question').removeClass('active');
            var users_message = '';
            if (total_users > 0){              
              users_message = 'Se enviaran correos a : ' + total_users +' colaboradores';
            }
            $('.mensaje_empleados').text(users_message);
        });
 
        $('#table-evaluados tbody tr').click(function (e) {
			if(!$(e.target).is('#table-evaluados td input:checkbox'))
			$(this).find('input:checkbox').trigger('click');
		});

        $('#period_form').on('submit', function(e){
            var users_array = usersTable.$('input').serializeArray();
			var users = [];
			$.each(users_array, function(i, field){
				users.push(this.value);
			});
			if(users.length > 0){
				$(this).append(
					$('<input>')
						.attr('type', 'hidden')
						.attr('name', 'users')
						.val(users)
				);
			} 
        }); 
	$('select#periodos').change(function(){

	$('form.form_estadisticas .period_id').val($(this).val());
	$('form.form_estadisticas').submit();
	});
	$('select#status').change(function(){

	$('form.form_estadisticas .status').val($(this).val());
	$('form.form_estadisticas').submit();
	});
 
	
    }); 
</script>
@endsection