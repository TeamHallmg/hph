@extends('sociograma.app')

@section('title', 'Lista de Colaboradores')

@section('content')

	<div class="row">

		<div class="col-md-2 text-right">
		@include('sociograma/partials/sub-menu')
		</div>

		<div class="col-md-10">
			
			<div class="flash-message" id="mensaje">
				@foreach (['danger', 'warning', 'success', 'info'] as $msg)
					@if(Session::has('alert-'.$msg))
						<p class="alert alert-{{ $msg }}">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
					@endif
				@endforeach
			</div>
			<br>

			<img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
 
		
			<div class="row">
					
					<div class="col-md-12">
						
						<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Planes de Correos</h3>
						
						<hr class="mb-5">

						<div class="row mb-3"> 
							<div class="col-12 offset-md-4 col-md-3 text-center">
								
								<a href="/sociograma/plan-correos/crear" class="btn btn-primary btn-block btn-static">Crear Plan</a>
								
							</div> 
						</div>

						<div class="table-responsive">
							<table id="table" class="table table-striped table-bordered listado w-100">
								<thead style="background-color: #222B64; color:white;">
									<tr>
										<th>ID</th>
										<th>Nombre del Plan</th>
										<th>Plantilla</th>
										<th>Programado</th>
										<th># Colaboradores</th>
										<th>Exitoso / Errores</th>
										<th>Estado</th>
										<th>Acciones</th>
									</tr>
								</thead>
								<tbody>
															
									@foreach ($planes as $plan)
									
									<tr>
										<td class="text-center">
											{{ $plan->id }}
											{{-- {{ (!empty($plan->id) ? $eval->employee->idempleado : '') }} --}}
										</td>
										<td class="text-center">
											{{ $plan->name }}
											{{-- {{ (!empty($plan->id) ? $eval->employee->idempleado : '') }} --}}
										</td>
										<td class="text-center"> 
											{{ (!empty($plan->template) ? $plan->template->name : '') }}
										</td>
										<td class="text-center"> 
											{{ (!empty($plan->date) ? $plan->date : 'N/A') }}
										</td>
										<td class="text-center"> 
											{{ (!empty($plan->colaboradores) ? count($plan->colaboradores) : 0) }}
										</td>
										<td class="text-center"> 
											{{ (!empty($plan->enviados) ? count($plan->enviados) : 0) }} / {{ (!empty($plan->errores) ? count($plan->errores) : 0) }}
										</td>
										
										<td class="text-center h5">
											@if($plan->status == 1)
												<div class="badge badge-pill badge-primary p-2">Pendiente</span>
											@elseif($plan->status == 2)
												<span class="badge badge-pill badge-success p-2">Con Exito</span>
											@else
												@if( (!empty($plan->errores) && count($plan->errores)>0))
													<span class="badge badge-pill badge-danger p-2">Con Errores</span>
												@else	
													<span class="badge badge-pill badge-info p-2">En Proceso</span>
												@endif	
											@endif
										</td>

										<td class="text-center">
										 
											<a href="{{ url('/enviar_plan_correos/' . $plan->id ) }}" class="btn btn-danger btn-block btn-static {{(count($plan->colaboradores)==0)?'disabled':null}}" style="cursor: default;" {{(count($plan->colaboradores)==0)?'disabled':null}}>Enviar</a>
											<a href="{{ url('/sociograma/ver-plan-correo/'. $plan->id ) }}" class="btn btn-success btn-block btn-static" style="cursor: default;">Ver</a>
											
										</td>	
									</tr>
									@endforeach
									
								</tbody>
							</table>
						</div>
						
					</div>

					
					
				</div> 			
		</div>
		
	</div>

	
@endsection

@section('scripts')


<script>
	
 
$(document).ready(function(){

    $('#table').DataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
    });
    });

		
       
	
</script>
@endsection