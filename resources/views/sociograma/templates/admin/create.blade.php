@extends('layouts.app')
@section('content')

<div class="container-fluid">
	
	
	<div class="card mt-3">
		<h5 class="card-header bg-info text-white font-weight-bolder">Plantillas Dinámicas - Crear
	<a href="{{url('/home')}}" class="btn btn-sm float-right btn-success">Regresar</a>
		  
  </h5>

  <form action="{{ url('templates') }}" method="POST">
	@csrf
		<div class="card-body">


		<div class="row">
			
			<div class="col-12">
				<h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid rgb(137, 135, 136);">Datos Generales</h3>
			</div>

			<form action="{{ url('templates') }}" method="POST">
				@csrf
				
			<div class="col-12 mt-4">
					<div class="form-row">
						<div class="form-group col-md-3">
							<label for="name">Nombre</label>
							<input type="text" class="form-control" name="name" id="name" placeholder="" required="">
						</div>
						<div class="form-group col-md-3">
							<label for="description">Descripción</label>
							<input type="text" class="form-control" name="description" id="description" placeholder="">
						</div>
						<div class="col-md-3">
							<label for="out">Formato de Salida:</label>
							<select name="out" id="out" class="form-control" required="">
								<option disabled value="" selected>Seleccione una opción...</option>
								<option value="PDF">PDF</option>
								<option value="EMAIL">EMAIL</option>
								<option value="AMBOS">AMBAS OPCIONES</option>
							</select>
						</div>
						<div class="col-md-3">
							<label for="status">Estado:</label>
							<select name="status" id="status" class="form-control" required="">
								<option disabled value="">Seleccione una opción...</option>
								<option value="1" selected>Disponible</option>
								<option value="0">No Disponible</option>
							</select>
						</div>
						<div class="col-md-6">
							<label for="template_modules_id">Módulo:</label>
							<select name="template_modules_id" id="template_modules_id" class="form-control" required="">
								<option disabled value="" selected>Seleccione una opción...</option>
								@foreach ($modulos as $modulo)
									<option value="{{$modulo->id}}">{{$modulo->name}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group col-md-6">
							<label for="template_static">URL plantilla Estatica</label>
							<input type="text" class="form-control" name="template_static" id="template_static" placeholder="">
						</div>
					</div>
	
			</div>
			
			<div class="col-12 mt-5">
				<h3 class="text-blue font-italic font-weight-bold m-0" style="border-bottom: 4px solid rgb(137, 135, 136);">Configuración para el envio de Correos</h3>
			</div>

			<div class="col-12 mt-4">
				<div class="form-row">
					<div class="form-group col-md-12">
						<label for="subject">Asunto</label>
						<input type="text" class="form-control" name="subject" id="subject" placeholder="" required="">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-12">
						<label for="cc_email">Enviar Correo Con Copia al Email</label>
						<input type="text" class="form-control" name="cc_email" id="cc_email" placeholder="">
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="card-footer">

		<a href="{{url('/templates')}}" class="btn btn-primary mr-3">Regresar</a>

		<button type="submit" class="btn btn-success mr-3">Guardar</button>
	</div>
  </form>

</div>
@endsection

@section('scripts')
<style type="text/css">
	.clase_error {
		display: none;
		color: red;
	}
</style>



@endsection