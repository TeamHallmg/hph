@extends('clima-organizacional.app')

@section('title', 'Encuesta')

@section('content')
<div class="row mt-3">
    <div class="col-md-2 text-right">
        @include('sociograma/partials/sub-menu')
    </div>
    <div class="col-md-10">
      <img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
<h3 class="titulos-evaluaciones mt-4 font-weight-bold">Avances</h3>
<hr>

@if (count($periodoStatus) > 0)

<div class="row justify-content-center pb-5">
    
    <div class="col-3">
        <div class="form-group text-center">
            <form id="changePlanForm" action="/sociograma/avances" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <label for="name">Periodo:</label>
            <select name="period_id" class="period_id respuesta form-control">

  @for ($i = 0;$i < count($periodoStatus);$i++)

              <option value="{{$periodoStatus[$i]->id}}" <?php if ($period_id == $periodoStatus[$i]->id){ ?>selected="selected"<?php } ?>>{{$periodoStatus[$i]->name}}</option>
  @endfor            

            </select>   
            <input type="submit" style="display: none">
          </form>
        </div>
    </div>

</div>
@endif

<div class="row">
    {{--<div class="col-md-3">
        <div class="card collapsed-card">
            <div class="card-header bg-dark no-border ui-sortable-handle">
                <h3 class="card-title">
                    Gráfica
                </h3>
                <div class="card-tools">
                    <button type="button" class="btn bg-dark btn-sm" data-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn bg-dark btn-sm" data-widget="remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div id="container" style="margin: 0 auto; width: 100%"></div>
            </div>
        </div>
    </div>--}}
    <div class="col-md-4">
        <div class="row info-box bg-danger rounded py-2 text-white mx-1" id="noiniciadas" style="cursor: pointer;">
            <div class="col-2 d-flex align-items-center justify-content-center">
                <span class="info-box-icon">
                    <h4 class="m-0"><i class="fas fa-arrow-alt-circle-down"></i></h4>
                </span>
            </div>
            <div class="col-10">
                <div class="info-box-content">
                    <span class="info-box-text">No Iniciadas</span><br>
                    <span class="info-box-number">{{count($no_iniciados) . '/' . $total}}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ number_format(count($no_iniciados) * 100 / $total, 1, '.', ',') . '%' }}"></div>
                    </div>
                    <span class="progress-description">
                        {{ number_format(count($no_iniciados) * 100 / $total, 1, '.', ',') . '%' }}
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="row info-box bg-warning rounded py-2 mx-1" id="enproceso" style="cursor: pointer;">
            <div class="col-2 d-flex align-items-center justify-content-center">
                <span class="info-box-icon">
                    <h4 class="m-0"><i class="fas fa-arrow-alt-circle-up"></i></h4>
                </span>
            </div>
            <div class="col-10">
                <div class="info-box-content">
                    <span class="info-box-text">En Proceso</span><br>
                    <span class="info-box-number">{{count($iniciados) . '/' . $total}}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ number_format(count($iniciados) * 100 / $total, 1, '.', ',') . '%' }}"></div>
                    </div>
                    <span class="progress-description">
                        {{ number_format(count($iniciados) * 100 / $total, 1, '.', ',') . '%' }}
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="row info-box bg-success rounded py-2 text-white mx-1" id="terminadas" style="cursor: pointer;">
            <div class="col-2 d-flex align-items-center justify-content-center">
                <span class="info-box-icon">
                    <h4 class="m-0"><i class="fas fa-check-circle"></i></h4>
                </span>
            </div>
            <div class="col-10">
                <div class="info-box-content">
                    <span class="info-box-text">Terminadas</span><br>
                    <span class="info-box-number">{{count($terminados) . '/' . $total}}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ number_format(count($terminados) * 100 / $total, 1, '.', ',') . '%' }}"></div>
                    </div>
                    <span class="progress-description">
                        {{ number_format(count($terminados) * 100 / $total, 1, '.', ',') . '%' }}
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row d-none" id="mostrarAll">   

    <div class="col-md-12 text-center">
        <h4>    
        <span class="badge badge-pill badge-success" style="cursor: pointer;">Mostrar Todos</span>
    </h4>
    </div>
    </div>

<hr>
    <table class="table table-striped table-bordered dt-responsive" id="evaluados">
        <thead class="bg-purple text-white">
            <tr>
                <th>ID</th>
                <th>Rfc</th>
                <th>Evaluado</th>
                <th>Región</th>
                <th>Correo</th>
                <th>Departamento</th>
                <th>Área</th>
                <th>Puesto</th>
                <th>Estado</th>
                <th>Preguntas Sin Contestar</th>
            </tr>
        </thead>
        <tbody>
            @if (count($evaluados) > 0)							
                @foreach ($evaluados as $eval)
                  @if ((auth()->user()->id != 1 && auth()->user()->id != 3505 && !in_array(0, $permissions) && !in_array($eval->employee_wt->region_id, $permissions)) || (in_array($eval->employee_wt->region_id, array(7,8)) && $eval->pivot->status != 3))
              <?php continue; ?>
                  @endif
                    <tr>
                        <td class="text-center">
                            {{ (!empty($eval->employee_wt) ? $eval->employee_wt->idempleado : '') }}
                        </td>
                        <td class="text-center">
                            {{ (!empty($eval->employee_wt) ? $eval->employee_wt->rfc : '')}}
                        </td>
                        <td class="text-center">
                            {{ $eval->FullName }}
                        </td>
                        <td class="text-center">
                            {{ (isset($eval->employee_wt->region) ? $eval->employee_wt->region->name : '') }}
                        </td>
                        <td class="text-center">
                            {{ $eval->email }}
                        </td>
                        <td class="text-center">
                            {{ (isset($eval->employee_wt->jobPosition->area->department) ? $eval->employee_wt->jobPosition->area->department->name : '') }}
                        </td>
                        <td class="text-center">
                            {{ (isset($eval->employee_wt->jobPosition->area->department->direction) ? $eval->employee_wt->jobPosition->area->department->direction->name : '') }}
                        </td>
                        <td class="text-center">
                            {{ (!empty($eval->employee_wt->jobPosition) ? $eval->employee_wt->jobPosition->name : '') }}
                        </td>
                        <td class="text-center">
                            @if($eval->pivot->status == 1)
                                <button class="btn btn-danger btn-block btn-static" style="cursor: default;">No Iniciada</button>
                            @elseif($eval->pivot->status == 2)
                                <button class="btn btn-warning btn-block btn-static" style="cursor: default;">En Proceso</button>
                            @else
                                <button class="btn btn-success btn-block btn-static" style="cursor: default;">Terminada</button>
                            @endif
                        </td>
                        <td class="text-center">
                          @if ($eval->pivot->status == 2)
                            {{$total_questions - $total_answers_by_user[$eval->id]}}
                          @endif
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="5" class="text-center">No hay evaluados.</td>
                </tr>
            @endif						
        </tbody>
    </table>
</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">

var no_iniciados = {{ count(isset($no_iniciados)?$no_iniciados:0) }};
var iniciados = {{ count(isset($iniciados)?$iniciados:0) }};
var terminados = {{ count(isset($terminados)?$terminados:0) }};

$(document).ready(function(){
	var table = $('#evaluados').DataTable({
    dom: 'Blftip',
		language: {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		},
		buttons: [
			{
				extend: 'excel',
				text: '<i class="fa fa-file-pdf"></i> Exportar a Excel',
				titleAttr: 'Exportar a Excel',
				title: '',
			}
		]
    });

    $( '#noiniciadas' ).on( 'click', function () { 
            table.columns().search('').draw();
            table
                .column(8)
                .search('No Iniciada')
                .draw(); 
        $( '#mostrarAll' ).removeClass('d-none');          
    } );

    $( '#enproceso' ).on( 'click', function () {
            table.columns().search('').draw();
            table
                .column(8)
                .search('Inconclusa')
                .draw(); 
        $( '#mostrarAll' ).removeClass('d-none');
    } );

    $( '#terminadas' ).on( 'click', function () { 
            table.columns().search('').draw();
            table
                .column(8)
                .search('Terminada')
                .draw(); 
        $( '#mostrarAll').removeClass('d-none');
    } );

    $('#mostrarAll').on( 'click', function () {
            table.columns().search('').draw();
        $( '#mostrarAll' ).addClass('d-none');
    } );

    /*Highcharts.chart('container', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: null,
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        states: {
      hover: {
        enabled: false
      }
    },
    enableMouseTracking: false,
        data: [
            {
                y: no_iniciados,
                color: '#e3342f'
            }, {
                y: iniciados,
                color: '#ffed4a'
            }, {
                y: terminados,
                color: '#38c172'
            }
        ],
    }],
});*/

    $('select.period_id').change(function(){

    $(this).parent().submit();
  });
});
</script>
@endsection