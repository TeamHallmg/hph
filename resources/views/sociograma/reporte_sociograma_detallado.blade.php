@extends('clima-organizacional.app')

@section('title', 'Encuesta')

@section('content')
<div class="app">
<div class="row mt-3">
    <div class="col-md-2 text-right">
        @include('sociograma/partials/sub-menu')
    </div>
    <div class="col-md-10">
        <img class="img-fluid" src="/img/banner_clima_laboral.png" alt="">
        <h3 class="titulos-evaluaciones mt-4 font-weight-bold">SOCIOGRAMA REPORTE INDIVIDUAL</h3>
        <hr>



        <div class="row">
            <div class="col-md-12"> 
                @include('sociograma.reporte_filtros_detallado')
            </div>
        </div> 

<div class="row">
    <div class="col-md-12"> 
        <div class="card mt-3">
            <h5 class="card-header bg-info text-white font-weight-bolder">
                COMPAÑEROS ELEGIDOS
                {{-- <button @click="getRecords()"></button> --}}
            </h5>
            <div class="card-body text-center" v-cloak v-if="loading">
                <img :src="'/img/ajax-loader.gif'" alt="">
            </div>
            <div class="card-body" v-cloak v-else>

                <div class="row  pb-5"  v-for="(item, index) in preguntas" :key="index">

                    <div class="col-md-12">
                        <div class="row font-weight-bolder pb-1">
                            <div class="col-md-12" >
                                <div class="form-group">

                                    <label for="">Motivo de Elección</label>
                                    <input type="text" class="form-control" v-model="item.name" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row font-weight-bolder pb-2">
                            <div class="col-md-6" >
                                Nombre de Compañeros
                            </div>
                            <div class="col-md-4" >
                                Departamento
                            </div>
                            <div class="col-md-2" >
                                Totales
                            </div>
                        </div>
                        <div class="row pb-2"  v-for="(votacion, index_votacion) in item.mas_votados" :key="index_votacion">
                            <div class="col-md-6 d-flex align-items-center" >
                                @{{index_votacion+1}} <input type="text" class="form-control ml-2" v-model="votacion.name" readonly>
                            </div>
                            <div class="col-md-4" >
                                <input type="text" class="form-control" v-model="votacion.dpto" readonly>
                            </div>
                            <div class="col-md-2" >
                                <input type="text" class="form-control" v-model="votacion.total" readonly>
                            </div>
                        </div>

                    </div>

                </div>
        </div>
    </div>
    
   
    
</div>



</div>
</div>
</div>
</div>
@endsection
@section('scripts_vuejs')

<script type="text/javascript">
 
 
var app = new Vue({
  el: '#app',
    created: function(){
        this.getRecords();
    }, 
  data: {
    loading:true,
    id_periodo:2,
    preguntas:[],
       
    
  },
    computed: {
        
    },
    methods:{
 
        resetFiltros: function(){ 
            this.getRecords();
        },
        getRecords: function(){ 
            this.loading =true;
            var url = "/sociograma/reporte/sociograma-detallado";
            axios.post(url,{
                id_periodo:this.id_periodo, 
            }).then(response=>{
                 

                console.log(response.data);


                this.loading = false;
                this.preguntas = response.data.preguntas;
                
                  
                
            })
        },

    },
})

</script>
@endsection