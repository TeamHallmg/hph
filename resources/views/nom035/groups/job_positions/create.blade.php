@extends('nom035.app')

@section('content')

<form action="{{ route('nom035.grupos_puestos.store') }}" method="POST">
    @csrf
    <div class="card">
        <div class="card-header">
            Crear grupo
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Nombre del grupo" required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="description">Descripción</label>
                        <textarea name="description" class="form-control" placeholder="Descripción">{{ old('description') }}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="table-responsive my-3">
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Área</th>
                    <th>Departamento</th>
                    <th>Dirección</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($jobPositions as $job)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $job->name }}</td>
                        <td>{{ $job->area->name }}</td>
                        <td>{{ $job->area->department->name }}</td>
                        <td>{{ $job->area->department->direction->name }}</td>
                        <td>
                            <input type="checkbox" name="jobPositions[]" value="{{ $job->id }}">
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="text-right">
        <input type="submit" value="Crear grupo" class="btn btn-primary" {{ $jobPositions->isEmpty()?'disabled':'' }}>
        <a href="{{ route('nom035.grupos_puestos.index') }}" class="btn btn-danger">Regresar</a>
    </div>
</form>
@endsection

@section('scripts')
<script>
    var jobsTable = $('.table').DataTable();
</script>
@endsection