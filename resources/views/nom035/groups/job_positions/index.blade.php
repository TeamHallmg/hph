@extends('nom035.app')

@section('content')

<h3>Grupos de Áreas</h3>
<a href="{{ route('nom035.grupos_puestos.create') }}" class="btn btn-primary"> Crear grupo </a>

<div class="table-responsive my-4">
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Descripción</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($groups as $group)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $group->name }}</td>
                    <td>{{ $group->description }}</td>
                    <td>
                        <a href="{{ route('nom035.grupos_puestos.show', $group->id) }}" class="btn btn-primary"> Ver </a>
                        <a href="{{ route('nom035.grupos_puestos.edit', $group->id) }}" class="btn btn-success"> Editar</a>
                        <form action="{{ route('nom035.grupos_puestos.destroy', $group->id) }}" method="POST">
                            @csrf
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="submit" value="Eliminar" class="btn btn-danger">
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

@section('scripts')
<script>
    $('.table').DataTable();
</script>
@endsection