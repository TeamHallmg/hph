@extends('nom035.app')

@section('content')

<div class="card">
    <div class="card-header">
        Ver grupo
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="">Nombre del grupo</label>
                    <input type="text" class="form-control" value="{{ $group->name }}" disabled>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="">Descripción</label>
                    <textarea type="text" class="form-control" disabled>{{ $group->description }}</textarea>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="table-responsive my-3">
    <table class="table" id="jobsTable">
        <thead>
            <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Área</th>
                <th>Departamento</th>
                <th>Dirección</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($group->jobPositions as $job)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $job->name }}</td>
                <td>{{ $job->area->name }}</td>
                <td>{{ $job->area->department->name }}</td>
                <td>{{ $job->area->department->direction->name }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="text-right">
    <a href="{{ route('nom035.grupos_puestos.index') }}" class="btn btn-danger">Regresar</a>
</div>
@endsection


@section('scripts')
<script>
    $('#jobsTable').DataTable();
</script>
@endsection