@extends('nom035.app')

@section('content')
<form action="{{ route('nom035.grupos_puestos.update', $group->id ) }}" method="POST">
    @csrf
    <input type="hidden" name="_method" value="PUT">
    <div class="card">
        <div class="card-header">
            Editar grupos
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input type="text" name="name" class="form-control" placeholder="Nombre del grupo" value="{{ old('name', $group->name) }}" required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="description">Descripción</label>
                        <textarea name="description" class="form-control" placeholder="Descripción">{{ old('description', $group->description) }}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive my-2">
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Área</th>
                    <th>Departamento</th>
                    <th>Dirección</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($jobPositions as $job)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $job->name }}</td>
                        <td>{{ $job->area->name }}</td>
                        <td>{{ $job->area->department->name }}</td>
                        <td>{{ $job->area->department->direction->name }}</td>
                        <td>
                            <input type="checkbox" name="jobPositions[]" value="{{ $job->id }}" {{ isset($groupJobPositions[$job->id])?'checked':'' }}>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="text-right">
        <input type="submit" value="Guardar" class="btn btn-primary" {{ $jobPositions->isEmpty()?'disabled':'' }}>
        <a href="{{ route('nom035.grupos_puestos.index') }}" class="btn btn-danger">Regresar</a>
    </div>
</form>
@endsection

@section('scripts')
<script>
    $('.table').DataTable();
</script>
@endsection