@extends('nom035.app')

@section('content')
    <form action="{{ url('nom035/grupos_departamentos') }}" method="POST">
        @csrf
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="name">Nombre</label>
                    <input type="text" name="name" class="form-control" placeholder="Nombre del grupo" required>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="description">Descripción</label>
                    <textarea name="description" class="form-control" placeholder="Descripción"></textarea>
                </div>
            </div>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($departments as $department)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $department->name }}</td>
                        <td>{{ $department->description }}</td>
                        <td>
                            <input type="checkbox" name="departments[]" value="{{ $department->id }}">
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
            <input type="submit" value="Crear grupo" class="btn btn-primary" {{ $departments->isEmpty()?'disabled':'' }}>
        </div>
    </form>
@endsection

@section('scripts')
<script>
    $('.table').DataTable();
</script>
@endsection