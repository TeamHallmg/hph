@extends('nom035.app')

@section('content')

<div class="row justify-content-center">
    <div class="col-sm-4 text-center">
        <a href="{{ url('nom035/grupos_departamentos/create') }}" class="btn btn-primary"> Crear grupo </a>
    </div>
</div>
<table class="table">
    <thead>
        <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($groups as $group)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $group->name }}</td>
                <td>{{ $group->description }}</td>
                <td>
                    <a href="{{ url('nom035/grupos_departamentos/' . $group->id) }}" class="btn btn-primary"> Ver </a>
                    <a href="{{ url('nom035/grupos_departamentos/' . $group->id . '/edit') }}" class="btn btn-success"> Editar</a>
                    <form action="{{ url('nom035/grupos_departamentos/' . $group->id) }}" method="POST">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="submit" value="Eliminar" class="btn btn-danger">
                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
@endsection

@section('scripts')
<script>
    $('.table').DataTable();
</script>
@endsection