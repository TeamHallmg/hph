@extends('nom035.app')

@section('title', 'Evaluación')

@section('content')

<div id="questions">
<div class="text-center">
  <img src="{{ asset('img/banner_nom035.png') }}" alt="" class="img-fluid">
</div>
<form action="{{ url('/nom035/guardar-respuestas') }}" method="post" >
	@csrf
	<input type="hidden" name="period_id" value="{{ $periodo->id }}">
	<input type="hidden" name="cuestionario" value="{{ $cuestionario }}">
	<h3 class="titulos-evaluaciones mt-4 font-weight-bold oculto text-center" style="display: none;">{{$user->first_name}} {{$user->last_name}}</h3>
	<h3 class="titulos-evaluaciones mt-4 font-weight-bold text-client-primary">Encuesta</h3>

	@if ($status->status == 3 || $status->status == 33)

	<div class="text-center mb-4 mostrar">
  	<button class="btn btn-primary print_questions"><i class="fas fa-print"></i> Imprimir Cuestionario</button>
	</div>
	@else

		@if (count($cuestionarios) == 2)

			@if (!empty($factor1))

				@if ($status->status == 31 || $status->status == 32)

	<div class="text-center mb-4 mostrar">
  	<button class="btn btn-primary print_questions"><i class="fas fa-print"></i> Imprimir Cuestionario</button>
	</div>
				@endif
			@else

				@if ($status->status == 13 || $status->status == 23)

	<div class="text-center mb-4 mostrar">
  	<button class="btn btn-primary print_questions"><i class="fas fa-print"></i> Imprimir Cuestionario</button>
	</div>
				@endif
			@endif
		@endif
	@endif

<?php $grouper = ''; ?>
	@if (!empty($factor1))
		@foreach ($groupers as $key => $value)
			@if ($value->grouper_id == 1)
	<?php $grouper = $value;
				break; ?>
			@endif
		@endforeach
	<div>
		<h3 class="titulos-evaluaciones mt-4 font-weight-bold bg-client-secondary p-2 rounded cl-white">Sección</h3>
		<div style="background-color: #EDEDF6; padding: 10px">
			<h3 class="text-center titulos-evaluaciones" style="margin-top: 0">{{ $factor1->name }}</h3>
			<p class="text-center" style="margin: 0">{{ $factor1->description }}</p>
		</div>
		<h3 class="titulos-evaluaciones mt-4 font-weight-bold  bg-client-secondary p-2 rounded cl-white">Pregunta</h3>
		<table class="table">
			<tbody>
		@if(!empty($factor1->question))
				<td class="w-50">{!! $factor1->question !!}</td>
				<td class="w-50 text-right">
					<select class="respuesta form-control parent" data-id="{{ $factor1->id }}" data-show="Si" name="groupers[{{ 1 }}]" id="{{ $factor1->id }}">
			@if (empty($grouper))
						<option value="">--Selecciona una opción--</option>
			@endif
						<option value="Si" {{ (!empty($grouper) && $grouper->answer == 'Si'?'selected':'') }}>Sí</option>
						<option value="No" {{ (!empty($grouper) && $grouper->answer == 'No'?'selected':'') }}>No</option>
					</select>
				</td>
		@endif
			</tbody>
		</table>
	</div>
	@endif

<?php $current_group_id = 0; ?>
	@foreach ($closed_questions as $factor)
		@if ($factor->cuestionario != $cuestionario)
<?php continue; ?>
		@endif
		<div class="{{ $factor->allQuestionBelongsToASameParent()?'son-'.$factor->getParentQuestionBelongsToASameParent():'' }}" {{ ($factor->allQuestionBelongsToASameParent() && (empty($grouper) || (!empty($grouper) && $grouper->answer == 'No'))?'style=display:none':'') }}>
			<h3 class="titulos-evaluaciones mt-4 font-weight-bold bg-client-secondary p-2 rounded cl-white">Sección</h3>
			<div style="background-color: #EDEDF6; padding: 10px">
				<h3 class="text-center titulos-evaluaciones" style="margin-top: 0">{{ $factor->name }}</h3>
				<p class="text-center" style="margin: 0">{{ $factor->description }}</p>
			</div>
			<h3 class="titulos-evaluaciones mt-4 font-weight-bold  bg-client-secondary p-2 rounded cl-white">Pregunta</h3>
			<table class="table">
				<tbody>
					@if(!empty($factor->question))
						<td class="w-50">{!! $factor->question !!}</td>
						<td class="w-50 text-right">
							<select class="respuesta form-control parent" data-id="{{ $factor->id }}" data-show="{{ $factor->show_on }}" name="factors[{{ $factor->id }}]" id="{{ $factor->id }}">
								<option value="">--Selecciona una opción--</option>
								<option value="Si">Sí</option>
								<option value="No">No</option>
							</select>
						</td>
					@endif
					@foreach ($factor->questions as $question)
						@if (is_null($question->parent_factor_id) && !empty($question->group) && $question->group->id != $current_group_id)
				<?php $current_group_id = $question->group->id;
							$grouper = ''; ?>
							@foreach ($groupers as $key => $value)
							 	@if ($value->grouper_id == $question->group->id)
						<?php $grouper = $value;
							 		break; ?>
							 	@endif
							@endforeach
					<tr class="{{ (empty($grouper) && ((count($cuestionarios) == 1 && $status->status == 2) || (count($cuestionarios) == 2 && ($status->status == 12 || $status->status == 22 || $status->status == 32))) ? 'btn-ambar' : '') }}">
						<td class="w-50">{{ $question->group->question }}</td>
						<td class="w-50 text-right">
							<select class="form-control parent" data-id="{{ $question->group->id }}" data-show="{{ $question->group->show_on }}" id="{{ $question->group->id }}" name="groupers[{{$question->group->id}}]">
							@if (empty($grouper))
								<option value="">--Selecciona una opción--</option>
							@endif
								<option value="Si" {{ (!empty($grouper) && $grouper->answer == 'Si'?'selected':'') }}>Sí</option>
								<option value="No" {{ (!empty($grouper) && $grouper->answer == 'No'?'selected':'') }}>No</option>
							</select>
						</td>
					</tr>
						@endif

						@if (count($cuestionarios) == 1)
						<tr class="{{ (!is_null($question->group)?'son-'.$question->group->id:'') }} {{ (empty($question->answer) && $status->status == 2 ? 'btn-ambar' : '') }}" {{ (!empty($question->group) && $question->group->id == $current_group_id && (empty($grouper) || (!empty($grouper) && (($question->group->show_on == 'Si' && $grouper->answer == 'No') || ($question->group->show_on == 'No' && $grouper->answer == 'Si'))))?'style=display:none':'') }}>
						@else

							@if ($cuestionario == 'Cuestionario 1')
						<tr class="{{ (!is_null($question->group)?'son-'.$question->group->id:'') }} {{ (empty($question->answer) && ($status->status == 21 || $status->status == 22 || $status->status == 23) ? 'btn-ambar' : '') }}" {{ (!empty($question->group) && $question->group->id == $current_group_id && (empty($grouper) || (!empty($grouper) && (($question->group->show_on == 'Si' && $grouper->answer == 'No') || ($question->group->show_on == 'No' && $grouper->answer == 'Si'))))?'style=display:none':'') }}>
							@else
						
						<tr class="{{ (!is_null($question->group)?'son-'.$question->group->id:'') }} {{ (empty($question->answer) && ($status->status == 12 || $status->status == 22 || $status->status == 32) ? 'btn-ambar' : '') }}" {{ (!empty($question->group) && $question->group->id == $current_group_id && (empty($grouper) || (!empty($grouper) && (($question->group->show_on == 'Si' && $grouper->answer == 'No') || ($question->group->show_on == 'No' && $grouper->answer == 'Si'))))?'style=display:none':'') }}>
							@endif
						@endif
							
							<td class="w-50">{{ $question->question }}</td>
							<td class="w-50 text-right">
								<select class="respuesta respuesta_a_guardar form-control" name="question[{{ $question->id }}]" id="{{$question->id}}">
									@if (is_null($question->answer))
										<option value="" class="default">--Selecciona una opción--</option>
									@endif
									@if($question->type == "multi") 
										<option value="Siempre" {{ (!is_null($question->answer) && ($question->answer->answer == "Siempre"))?'selected':'' }}>Siempre</option>
										<option value="Casi siempre" {{ (!is_null($question->answer) && ($question->answer->answer == "Casi siempre"))?'selected':'' }}>Casi siempre</option>
										<option value="Algunas veces" {{ (!is_null($question->answer) && ($question->answer->answer == "Algunas veces"))?'selected':'' }}>Algunas veces</option>
										<option value="Casi nunca" {{ (!is_null($question->answer) && ($question->answer->answer == "Casi nunca"))?'selected':'' }}>Casi nunca</option>
										<option value="Nunca" {{ (!is_null($question->answer) && ($question->answer->answer == "Nunca"))?'selected':'' }}>Nunca</option>
									@elseif($question->type == "yes_no")
										<option value="Si" {{ (!is_null($question->answer) && ($question->answer->answer == "Si"))?'selected':'' }}>Sí</option>
										<option value="No" {{ (!is_null($question->answer) && ($question->answer->answer == "No"))?'selected':'' }}>No</option>
									@endif
								</select>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	@endforeach
	@if(count($opened_questions) > 0)
		<h3 class="titulos-evaluaciones mt-4 font-weight-bold bg-client-secondary p-2 rounded cl-white">Factor</h3>
		<div style="background-color: #EDEDF6; padding: 10px">
			<h3 class="text-center titulos-evaluaciones" style="margin-top: 0">Preguntas Abiertas</h3>
		</div>
		<h3 class="titulos-evaluaciones mt-4 font-weight-bold bg-client-secondary p-2 rounded cl-white">Pregunta</h3>
		<table class="table">
			<tbody>
				@foreach ($opened_questions as $question)
					<tr>
						<td class="w-50">{{ $question->question }}</td>
						<td class="w-50"><textarea class="respuesta_abierta form-control" id="{{$question->id}}" name="question[{{ $question->id }}]">{{ (!is_null($question->answer))?$question->answer->answer:'' }}</textarea></td>
					</tr>
				@endforeach
			</tbody>
		</table>
	@endif
	<input type="submit" value="Guardar" class="btn btn-success text-center mostrar">
</form>
</div>

@endsection

@section('scripts')
	<script>

	var total_preguntas;
	var guardar = false;
	var alertar = false;
	var preguntas_sin_contestar = false;
	var id_periodo = <?php echo $periodo->id?>;
		
	$(document).ready(function(){

		$('.parent').on('change', function(){
			let id = $(this).data('id');
			let show_on = $(this).data('show');
			let val = $(this).val();
			if( (show_on === 'Si' && val === 'Si') || (show_on === 'No' && val === 'No') ){
				if (id == 1){
					$('tr').removeClass('btn-ambar');
				}
				else{
					if ($('tr.son-' + id + '.btn-ambar').length > 0){
						$('tr.son-' + id).addClass('btn-ambar');
					}
				}
				$('.son-' + id).fadeIn();
				$('.son-' + id).show();
			}else{
				$('.son-' + id).each(function(){
					if ($(this).find('td select option.default').length == 0){
						$(this).find('td select').prepend('<option value="" class="default">--Selecciona una opción--</option>');
					}
					$(this).find('td select').val('');
					$(this).find('td select').change();
				});
				$('.son-' + id).fadeOut();

				//})
				/*if ($('.son-' + id).find('td select option.default').length == 0){
					$('.son-' + id).find('td select').prepend('<option value="" class="default">--Selecciona una opción--</option>');
				}
				$('.son-' + id).find('td select').val('');
				$('.son-' + id).find('td select').change();
				$('.son-' + id).fadeOut();*/
			}

			var grouper = $(this).attr('id');
			var respuesta = $(this).val();
			$.ajax({
        		type:'POST',    
        		url: '/nom035/guardar-respuesta',
        		data:{
          			grouper : grouper,
          			respuesta : respuesta,
          			id_periodo: id_periodo
        		}
      		});
		});


		$.ajaxSetup({
    		headers: {
        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    		}
		});

		//$('input[type="radio"]').click(function(){
		$('select.respuesta_a_guardar').change(function(){
			var id_pregunta = $(this).attr('id');
			var respuesta = $(this).val();
			$.ajax({
        		type:'POST',    
        		url: '/nom035/guardar-respuesta',
        		data:{
          			id_pregunta : id_pregunta,
          			respuesta : respuesta,
          			id_periodo: id_periodo
        		}
      		});
		});

		$('button.guardar_abiertas').click(function(){
			preguntas_sin_contestar = false;
			$('select.respuesta').each(function(){
				if ($(this).val() == '0'){
					preguntas_sin_contestar = true;
					$(this).parent().parent().parent().parent().css('background-color', 'red')
				}
			});

			$('textarea.respuesta_abierta').each(function(){
				if ($(this).val() == ''){
					preguntas_sin_contestar = true;
					$(this).parent().parent().parent().parent().css('background-color', 'red')
				}
			});
			
			if (preguntas_sin_contestar){
				alert('La encuesta no ha sido contestada completamente, favor de terminarla.');
				return false;
			}
			$('textarea.respuesta_abierta').each(function(){
				$('form.guardar_respuestas').append('<input type="hidden" name="respuestas[]" value="' + $(this).val() + '"><input type="hidden" name="preguntas[]" value="' + $(this).attr('id') + '">');
			});
			$('form.guardar_respuestas').submit();
		});

		$('body').on('click', 'button.print_questions', function(){

      $('.oculto').show();
      $('.mostrar').hide();
      var contenido = document.getElementById('questions').innerHTML;
      var contenidoOriginal= document.body.innerHTML;
      document.body.innerHTML = contenido;
      window.print();
      document.body.innerHTML = contenidoOriginal;
      $('.oculto').hide();
      $('.mostrar').show();
    });
	});

	setInterval(function(){

		if (preguntas_sin_contestar){

			alertar = false;
			//alert('La encuesta no ha sido concluida correctamente, favor de terminarla');
			//window.reload();
		}
	}, 100)

	window.onbeforeunload = function(e){ 
  
  if (!guardar){
    
  	alertar = true;
  	$(window).unbind();
    return undefined;
  }
}
	</script>
@endsection