@extends('layouts.app')

@section('moduleSidebar')
    @include('nom035.sidebar')
@endsection

@section('mainContent')
    <div class="text-center">
        <img src="{{ asset('img/banner_nom035.png') }}" alt="">
    </div>
    @if ($errors->any())
        <div class="alert alert-danger my-2">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="mx-4 my-2">
        @yield('content')
    </div>
@endsection

{{-- @section('mainScripts')
    @yield('scripts')
@endsection --}}