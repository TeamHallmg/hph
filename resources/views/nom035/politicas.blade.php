@extends('nom035.app')

@section('title', 'Políticas')

@section('content')
{{-- <h4 class="font-weight-bold my-4"  style="color: #002C49;">POLÍTICAS NOM035</h4> --}}
<div class="text-center mb-4 mt-4" style="display: none;">
  <button class="btn btn-primary print_politics"><i class="fas fa-print"></i> Imprimir Políticas</button>
</div>
<div class="card card-2">
    {{-- <div class="card-header bg-white text-white p-3">
    </div> --}}
    <div class="card-body">
        <div class="container-fluid">

            <div id="politics">
                @if (auth()->user()->role == 'admin' || in_array(0, $permissions))
  <div>
    <div class="margin-top-20 text-center pull-right" style="margin-top: 20px; margin-bottom: 20px">
      <a class="btn btn-primary" href="{{ url('nom035/politicas-index') }}"  role="button">Editar Políticas</a>
    </div>
  </div>
  @endif
  
  @if (!empty($politica))
    {!! $politica['description'] !!}
  @endif

  
<div class="row">
    <div class="col-md-12 d-inline-block justify-content-end text-right">

      @if (!empty($politicas))

      <a class="btn btn-success" href="/nom035/evaluaciones"><i class="fas fa-check"></i> Confirmada</a>
      @else

        @if (!empty($politica))

        <form action="/nom035/announcement_policy_agreement" method="post" class="text-right">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="enterprise_id" value="{{$politica->enterprise_id}}">
          <input type="hidden" name="sucursal_id" value="{{$politica->sucursal_id}}">
          <button class="btn btn-warning" type="sumbit"><i class="fas fa-check"></i> Confirmar de Enterado</button>
        </form>
        @endif
      @endif

    </div>
</div>

<div class="modal fade" id="modalAsistir" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm border-0 shadow-3" role="document">
    <div class="modal-content border-0">
    <form method="post" action="https://mbge.soysepanka.com/announcement_register_assistance" enctype="multipart/form-data" autocomplete="off">
      <input type="hidden" name="_token" value="8JO5oWki3exzvSOnEyFWzGdvhXC5sis6oAqgvC7Z">      <input type="hidden" name="announcement_id" id="announcement_to_register">
      <div class="modal-header text-white bg-purple">
        <h5 class="modal-title" id="exampleModalLabel">Aviso!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            Confirmar de enterado.
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cerrar</button>
        <button class="btn btn-sm text-white bg-purple" type="sumbit"><i class="fas fa-check-circle"></i> Confirmar</button>
      </div>
    </form>
    </div>
  </div>
</div>

                <div class="col-md-12 text-center oculto" style="display: none; padding: 0">
                  <div style="width: 330px; margin: 70px auto; border-top: 2px solid black; font-size: 15px">
                    <div>{{ auth()->user()->fullname }}</div>
                    <div>{{ (!empty(auth()->user()->employee) ? auth()->user()->employee->sucursal : '') }}</div>
                    <div>{{ (!empty(auth()->user()->employee) && !empty(auth()->user()->employee->enterprise) ? auth()->user()->employee->enterprise->name : '') }}</div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
          $("#modalAsistir").on('show.bs.modal', function (e) {
                var triggerLink = $(e.relatedTarget);
                var id = triggerLink.data("id");
                $('#announcement_to_register').val(id);
            });

            $('body').on('click', 'button.print_politics', function(){

              $('.oculto').show();
              $('.mostrar').hide();
              $('i.fa-cog').parent().parent().hide();
              var contenido = document.getElementById('politics').innerHTML;
              var contenidoOriginal= document.body.innerHTML;
              document.body.innerHTML = contenido;
              window.print();
              document.body.innerHTML = contenidoOriginal;
              $('.oculto').hide();
              $('.mostrar').show();
              $('i.fa-cog').parent().parent().show();
            });

            if ($('button.btn-success').length > 0){

              $('button.print_politics').parent().show();
              $('button.btn-success').addClass('mostrar');
              $('button.btn-success').prev().addClass('mostrar');
            }      
        });
    </script>
@endsection
