<h4 class="font-weight-bold" style="margin-top: 15px">Módulo <br> NOM 035</h4>
<hr>
<ul class="nav nav-pills flex-column">
	<li class="nav-item"><a class="nav-link" href="{{ url('nom035/que-es') }}">¿Qué es?</a></li>
	<li class="nav-item"><a class="nav-link" href="{{ url('nom035/que-es') }}">Política</a></li>
	<li class="nav-item"><a class="nav-link" href="{{ url('nom035/que-es') }}">Plan Individual</a></li>
	<li class="nav-item"><a class="nav-link" href="{{ url('nom035/reporte_nom') }}">Reporte General</a></li>
	<li class="nav-item"><a class="nav-link" href="{{ url('nom035/tablero/calificacion') }}">Tablero de Calificaciones</a></li>
	<li class="nav-item"><a class="nav-link" href="{{ url('nom035/tablero/dominio') }}">Tablero de Dominio</a></li>
	
	
	@if (auth()->user()->role == 'admin' || 
	isset($userPermissions['see_progress']) || 
	isset($userPermissions['see_reports']) || 
	isset($userPermissions['see_factors']) || 
	isset($userPermissions['see_questions']) || 
	isset($userPermissions['see_periods'])
	)		
	<li class="nav-item"><h4 class="font-weight-bold mt-4">Administrador 	<a data-toggle="collapse" href="#collapseMenu" role="button" aria-expanded="false" aria-controls="collapseMenu"><i class="fas fa-cog"></i></a>
	</h4><hr></li>
	
	<div class="collapse" id="collapseMenu">
		<li class="nav-item">
 
		@if (auth()->user()->role != 'admin')
		{{-- <a class="nav-link" href="{{ url('nom035/evaluaciones') }}">Encuesta</a></li> --}}
		@endif
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_progress']))
			{{-- <a class="nav-link" href="{{ url('nom035/avances') }}">Avances</a></li> --}}
		@endif
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_reports']))
			{{-- <a class="nav-link" href="{{ url('nom035/reporte-grafico') }}">Reportes</a></li> --}}
		@endif
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_factors']))
			{{-- <a class="nav-link" href="{{ url('nom035/factores') }}">Factores</a></li> --}}
		@endif
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_questions']))
			{{-- <a class="nav-link" href="{{ url('nom035/preguntas') }}">Preguntas</a></li> --}}
		@endif		
		@if (auth()->user()->role == 'admin' || isset($userPermissions['see_periods']))
			{{-- <a class="nav-link" href="{{ url('nom035/periodos') }}">Periodos</a></li> --}}
		@endif
		@if (auth()->user()->role == 'admin')
			{{-- <a class="nav-link" href="{{ url('nom035/permissions') }}">Permisos</a></li> --}}
		@endif
		{{-- <hr>
		<a class="nav-link" href="{{ url('nom035/grupos_areas') }}">Centros de Trabajo</a></li>
		<a class="nav-link" href="{{ url('nom035/grupos_departamentos') }}">Grupos Departamentos</a></li>
		<a class="nav-link" href="{{ url('nom035/grupos_puestos') }}">Grupos Puestos</a></li>
  --}}
</li>
	</div>
	@endif
</ul>