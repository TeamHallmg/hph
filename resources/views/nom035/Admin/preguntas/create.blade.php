@extends('nom035.app')

@section('title', 'Preguntas')

@section('content')


<h3 class="titulos-evaluaciones mt-4 font-weight-bold">Preguntas</h3>
<form method="post" action="{{ url('nom035/preguntas') }}">
	<div class="row">
		<div class="col-md-6 col-sm-9">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="card">
				<div class="card-header">
					Crear Pregunta
				</div>
				<div class="card-body">	
					<div class="form-group">
						<label for="question" class="requerido">Pregunta</label>
						<textarea name="question" class="form-control" placeholder="Escribe la pregunta" style="resize: none" cols="2" rows="2" required></textarea>
					</div>
					<div class="form-group">
						<label for="factor_id" class="requerido">Sección</label>
						<select class="form-control" name="factor_id">
							<option value="">Pregunta Abierta</option>
							@foreach ($factores as $key => $factor)
								<option value="{{$factor->id}}">{{$factor->name}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="positive" class="requerido">Sentido</label>
						<select class="form-control" name="positive">
							<option value="1">Sentido Positivo</option>
							<option value="0">Sentido Negativo</option>
						</select>
					</div>
				</div>
			</div>
			<div class="float-right">
				<button type="submit" class="btn btn-success">
					<i class="fas fa-check-circle"></i> Guardar Factor
				</button>
			</div>
		</div>
		<div class="col-md-6 col-sm-9">
			<div class="card">
				<div class="card-body">
					<div class="form-group">
						<label for="">Categoría</label>
						<select name="category_id" class="form-control">
							<option value="">...Seleccione una opción</option>
							@foreach ($categories as $key => $name)
								<option value="{{ $key }}">{{ $name }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="">Dominio</label>
						<select name="domain_id" class="form-control">
							<option value="">...Seleccione una opción</option>
							@foreach ($domains as $key => $name)
								<option value="{{ $key }}">{{ $name }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="">Dimensión</label>
						<select name="dimension_id" class="form-control">
							<option value="">...Seleccione una opción</option>
							@foreach ($dimensions as $key => $name)
								<option value="{{ $key }}">{{ $name }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

@endsection