@extends('nom035.app')

@section('title', 'Factores')

@section('content')

<h3 class="titulos-evaluaciones mt-4 font-weight-bold">Preguntas</h3>
<a href="{{ url('nom035/preguntas/create') }}" id='agregarPregunta' class="btn btn-success"><span class="fas fa-plus-circle"></span> Agregar Pregunta</a>

<div class="my-3">
	<table class="table table-striped" id="preguntas">
		<thead>
			<tr>
				<th>ID</th>
				<th>Pregunta</th>
				<th>Factor</th>
				<th>Sentido</th>
				<th>Categoría</th>
				<th>Dominio</th>
				<th>Dimensión</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($preguntas as $pregunta)
				<tr>
					<td>{{$pregunta->id}}</td>
					<td>{{$pregunta->question}}</td>
					<td>{{(!is_null($pregunta->factor) ? $pregunta->factor->name : 'Pregunta Abierta')}}</td>
					<td>{{($pregunta->positive == 1 ? 'Positivo' : 'Negativo')}}</td>
					<td>
						{{ $pregunta->category?$pregunta->category->name:'' }}
					</td>
					<td>
						{{ $pregunta->domain?$pregunta->domain->name:'' }}
					</td>
					<td>
						{{ $pregunta->dimension?$pregunta->dimension->name:'' }}
					</td>
					<td>
						<a href="{{ url('nom035/preguntas/' . $pregunta->id . '/edit') }}" class="btn btn-primary"><i class="fas fa-edit"></i> Editar</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>

@endsection

@section('scripts')
<script>
	$('#preguntas').DataTable({
		language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
	});

</script>
@endsection