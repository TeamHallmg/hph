@extends('nom035.app')

@section('title', 'Factores')

@section('content')

<h3 class="titulos-evaluaciones mt-4 font-weight-bold">Factores</h3>
<a href="{{ url('nom035/factores/create') }}" id='agregarFactor' class="btn btn-success"><span class="fas fa-plus-circle"></span> Agregar Factor</a>

<div class="my-3">
	<table class="table table-striped" id="factores">
		<thead>
			<tr>
				<th>ID</th>
				<th>Nombre</th>
				<th>Descripción</th>
				<th>Tipo</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>

			@foreach ($factores as $factor)
				
				<tr>
					<td>{{$factor->id}}</td>
					<td>{{$factor->name}}</td>
					<td>{{$factor->description}}</td>
					<td>
						@if($factor->type == "yes_no")
							Si - No
						@elseif($factor->type == "multi")
							5 respuestas
						@endif
					</td>
					<td>
						<a href="{{ url('nom035/factores/' . $factor->id . '/edit') }}" class="btn btn-primary"><i class="fas fa-edit"></i> Editar</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>

@endsection
@section('scripts')
<script>

	$('#factores').DataTable({
		language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
	});

</script>
@endsection