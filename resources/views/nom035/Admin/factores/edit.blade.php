@extends('nom035.app')

@section('title', 'Factores')

@section('content')

<h3 class="titulos-evaluaciones mt-4 font-weight-bold">Factores</h3>
<div class="row">
	<div class="col-md-6 col-sm-9">
        <form method="post" action="{{ url('nom035/factores/' . $factor->id) }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="PUT">
			<div class="card">
				<div class="card-header">
					Editar Factor
				</div>
				<div class="card-body">	
					<div class="form-group">
						<label for="name" class="requerido">Nombre</label>
						<input type="text" class="form-control" placeholder="Nombre" name="name" required value="{{$factor->name}}">
					</div>
					<div class="form-group">
						<label for="description" class="requerido">Descripción</label>
						<textarea name="description" class="form-control" placeholder="Descripción" style="resize: none" cols="2" rows="2" required>{{$factor->description}}</textarea>
					</div>
				</div>
			</div>
			<div class="float-right">
				<button type="submit" class="btn btn-success">
					<i class="fas fa-check-circle"></i> Guardar Factor
				</button>
			</div>
		</form>
	</div>
</div>

@endsection