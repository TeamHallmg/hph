@extends('clima-organizacional.app')

@section('title', 'Permisos')

@section('content')

<div class="text-center">
  <img src="{{ asset('img/banner_nom035.png') }}" alt="" class="img-fluid">
</div>
<h3 class="titulos-evaluaciones mt-4 font-weight-bold text-client-primary">Permisos</h3>
<div class="text-center mt-4">
	<a href="/nom035/permissions/create" class="btn btn-success"><i class="fas fa-plus-circle"></i> Agregar Permiso</a>
</div>
<div class="my-3 mt-4">
	<table class="table table-striped" id="permissions">
		<thead class="bg-client-primary text-white">
			<tr>
				<th>Usuario</th>
				<th>Hospital</th>
				<th>Sección</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>

			@foreach ($permissions as $permission)
				<tr>
					<td>{{$permission->user->fullname}}</td>
					<td>{{(!empty($permission->region) ? $permission->region->name : 'Todos los Hospitales')}}</td>
					<td>{{(!empty($permission->section) ? $permission->section->section : '')}}</td>
					<td>
						<form action="{{ url('nom035/permissions/' . $permission->id) }}" method="POST">
              @csrf
              <input type="hidden" name="_method" value="DELETE">
              <input type="submit" value="Eliminar" class="btn btn-danger">
            </form>
					</td>
				</tr>	
			@endforeach
		</tbody>
	</table>
</div>
@endsection

@section('scripts')

<script type="text/javascript">
	$('#permissions').DataTable({
		language: {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
   	});
</script>

@endsection