@extends('nom035.app')

@section('title', 'Periodo')

@section('content')

<div class="text-center">
  <img src="{{ asset('img/banner_nom035.png') }}" alt="" class="img-fluid">
</div>
<h3 class="titulos-evaluaciones mt-4 font-weight-bold text-client-primary">Periodo</h3>
<a href="{{ url('nom035/evaluations/create') }}" id='agregarFactor' class="btn btn-success"><span class="fas fa-plus-circle"></span> Crear Periodo</a>

<div class="my-3">
	<table class="table table-striped" id="factores">
		<thead class="bg-client-primary text-white">
			<tr>
				<th>ID</th>
				<th>Nombre</th>
				<th>Descripción</th>
				<th>Estatus</th>
				<th>Inicio Aplicación</th>
                <th>Fin Aplicación</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>

			@foreach ($evaluations as $evaluation)
				<tr>
					<td>{{ $evaluation->id }}</td>
					<td>{{ $evaluation->name }}</td>
					<td>{{ $evaluation->description }}</td>
					<td>{{ $evaluation->status }}</td>
					<td>{{ $evaluation->start_date }}</td>
                    <td>{{ $evaluation->end_date }}</td>
					<td>
						<a href="{{ url('nom035/evaluations/' . $evaluation->id . '/edit') }}" class="btn btn-primary"><i class="fas fa-edit"></i> Editar </a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>

@endsection
@section('scripts')
<script>

	$('#factores').DataTable({
		language: {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
	});

</script>
@endsection