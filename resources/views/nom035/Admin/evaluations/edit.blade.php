@extends('nom035.app')

@section('title', 'Periodo')

@section('content')

<div class="text-center">
  <img src="{{ asset('img/banner_nom035.png') }}" alt="" class="img-fluid">
</div>
<h3 class="titulos-evaluaciones mt-4 font-weight-bold text-client-primary">Periodo</h3>
<div class="row">
	<div class="col-md-12 col-sm-12">
        <form method="post" action="{{ url('nom035/evaluations/' . $evaluation->id) }}">
            @csrf
            @method('PUT')
			<div class="card">
				<div class="card-header bg-client-primary text-white">
					Editar Periodo
				</div>
				<div class="card-body">	
					<div class="form-group">
						<label for="name" class="requerido">Nombre</label>
						<input type="text" class="form-control" placeholder="Nombre" name="name" value="{{ old('name', $evaluation->name) }}" required>
					</div>
					<div class="form-group">
						<label for="description" class="requerido">Descripción</label>
						<textarea name="description" class="form-control" placeholder="Descripción" style="resize: none" cols="2" rows="2" required>{{ old('description', $evaluation->description) }}</textarea>
					</div>
					<div class="form-group">
						<label for="status" class="requerido">Estatus</label>
						<select name="status" class="form-control" required>
							<option value="">...</option>
							<option {{ old('status', $evaluation->status)=='activo'?'selected':'' }} value="activo">Activo</option>
							<option {{ old('status', $evaluation->status)=='inactivo'?'selected':'' }} value="inactivo">Inactivo</option>
						</select>
                    </div>
                    <div class="form-group">
                        <label for="start_date" class="requerido">Fecha de Inicio</label>
                        <input type="date" value="{{ old('start_date', $evaluation->start_date) }}" name="start_date" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="end_date" class="requerido">Fecha de Fin</label>
                        <input type="date" value="{{ old('end_date', $evaluation->end_date) }}" name="end_date" class="form-control" required>
                    </div>
				</div>
			</div>
			<div class="float-right pt-4">
				<button type="submit" class="btn btn-success">
					<i class="fas fa-check-circle"></i> Guardar
				</button>
			</div>
		</form>
	</div>
</div>

@endsection