@extends('nom035.app')

@section('title', 'Encuesta')

@section('content')

<div class="row">

  <div class="col-md-2 text-right">
      @include('nom035/partials/sub-menu')
  </div>
      
  <div class="col-md-10">
      <div class="text-center">
          <img src="{{ asset('img/banner_nom035.png') }}" class="img-fluid" alt="">
      </div>
  </div>
</div>
 

<div class="row">

<div class="col-md-12 sub_menu">
  <div class="card mt-3">
    <h5 class="card-header bg-info text-white font-weight-bolder">REPORTE GENERAL
      {{-- <a href="/url" class="btn btn-success float-right">Regresar</a> --}}
    </h5>
    <div class="card-body">
 
 
 
 
    <div class="row">

        <div class="col-md-12 sub_menu">
            <div class="card mt-3">
                <h5 class="card-header bg-info text-white font-weight-bolder">Listado de Personal 
                </h5>
                <div class="card-body">
            
                <div class="table-responsive ">
                    <table class="table table-striped" id="evaluados">
                        <thead>
                            <tr>
                                <th>nombre</th>
                                <th>totales</th>   
                            </tr>
                        </thead>
                        <tbody>
                        
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
            </div>
 
    </div>

    </div>
    </div>
    </div>
</div>
 

@endsection
@section('scripts')
  
@endsection