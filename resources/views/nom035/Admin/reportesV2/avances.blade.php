@extends('nom035.app')

@section('title', 'Encuesta')

@section('content')
{{-- <style type="text/css">
  div#evaluados_wrapper {position: relative;padding-top: 40px}
  div.dt-buttons {top: 0;position: absolute;}
  div.dataTables_length, div.dataTables_info {float: left;}
  div#evaluados_filter, div.dataTables_paginate {float: right;}  
  .buttons-excel{background: green!important; margin-bottom: 25px!important;}
</style> --}}
<style type="text/css">
  div.bg-danger, div.bg-success, div.bg-warning, div#mostrarAll {cursor: pointer;}
  i.red-background {color: red;}
</style>

<div class="row">

  <div class="col-md-2 text-right">
      @include('nom035/partials/sub-menu')
  </div>
      
  <div class="col-md-10">
      <div class="text-center">
          <img src="{{ asset('img/banner_nom035.png') }}" class="img-fluid" alt="">
      </div>
  </div>
</div>
 

<div class="row">

<div class="col-md-12 sub_menu">
  <div class="card mt-3">
    <h5 class="card-header bg-info text-white font-weight-bolder">Avances
      {{-- <a href="/url" class="btn btn-success float-right">Regresar</a> --}}
</h5>
    <div class="card-body">

    <div class="row pb-5">
    <div class="col-5">
      <div class="form-group" style="display: flex;align-items: flex-end;">
        <label for=""> Periodos</label> 
        <select class="ml-3 form-control period_id">
          @foreach ($periodoStatus as $key => $value)
        
          <option value="{{$value->id}}" <?php if ($period_id == $value->id){ ?>selected="selected"<?php } ?>>{{$value->name}}</option>
            @endforeach
        </select>
      </div>      
    </div> 
    </div> 

@if ($canEdit)

<?php $index = 9; ?>

@foreach ($cuestionarios as $key => $value)
{{-- 

<div class="col-md-3">
  <div class="card collapsed-card">
      <div class="card-header bg-dark no-border ui-sortable-handle">
          <h3 class="card-title" style="color: white">
              Gráfica
          </h3>
          <div class="card-tools">
              <button type="button" class="btn bg-dark btn-sm" data-widget="collapse">
                  <i class="fas fa-minus" style="color: white"></i>
              </button>
              <button type="button" class="btn bg-dark btn-sm" data-widget="remove">
                  <i class="fas fa-times" style="color: white"></i>
              </button>
          </div>
      </div>
      <div class="card-body" style="display: none">
          <div id="container{{$key}}" style="margin: 0 auto; width: 100%"></div>
      </div>
  </div>
</div> --}}




<div class="row">
    <div class="col-md-12">
      <h3>{{$value}}</h3>
      <hr>
    </div>
</div>
<div class="row pb-5">
    <div class="col-md-4">
        <div class="info-box bg-danger" data-index="{{$index}}">
            <span class="info-box-icon">
                <i class="fas fa-arrow-alt-circle-down"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">No iniciadas</span>
                <span class="info-box-number">{{ $no_iniciados[$key] . '/' . $total_evals }}</span>
                <div class="progress">
                    <div class="progress-bar" style="width: {{ number_format($no_iniciados[$key] * 100 / $total_evals, 1, '.', ',') . '%' }}"></div>
                </div>
                <span class="progress-description">
                    {{ number_format($no_iniciados[$key] * 100 / $total_evals, 1, '.', ',') . '%' }}
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="info-box bg-warning" data-index="{{$index}}">
            <span class="info-box-icon">
                <i class="fas fa-arrow-alt-circle-up"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Inconclusas</span>
                <span class="info-box-number">{{ $iniciados[$key] . '/' . $total_evals }}</span>
                <div class="progress">
                    <div class="progress-bar" style="width: {{ number_format($iniciados[$key] * 100 / $total_evals, 1, '.', ',') . '%' }}"></div>
                </div>
                <span class="progress-description">
                    {{ number_format($iniciados[$key] * 100 / $total_evals, 1, '.', ',') . '%' }}
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="info-box bg-success" data-index="{{$index}}">
            <span class="info-box-icon">
                <i class="fas fa-check-circle"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Terminadas</span>
                <span class="info-box-number">{{ $terminados[$key] . '/' . $total_evals }}</span>
                <div class="progress">
                    <div class="progress-bar" style="width: {{ number_format($terminados[$key] * 100 / $total_evals, 1, '.', ',') . '%' }}"></div>
                </div>
                <span class="progress-description">
                    {{ number_format($terminados[$key] * 100 / $total_evals, 1, '.', ',') . '%' }}
                </span>
            </div>
        </div>
    </div>
</div>
<?php $index++; ?>
@endforeach

<div class="row" id="mostrarAll" style="display: none">   
  <div class="col-md-12 text-center">
    <h4>    
      <span class="badge badge-pill badge-success">Mostrar Todos</span>
    </h4>
  </div>
</div>
@endif

@if ($canEdit)
 
<div class="row">

<div class="col-md-12 sub_menu">
  <div class="card mt-3">
    <h5 class="card-header bg-info text-white font-weight-bolder">Listado de Personal 
</h5>
    <div class="card-body">

      <button class="btn btn-success text-left mb-5 btn-excel" title="Exportar a Excel">
        <i class="fa fa-file-excel"></i> Exportar a Excel
      </button>

      <div class="table-responsive ">
          <table class="table table-striped" id="evaluados">
              <thead>
                  <tr>
                      <th>ID</th>
                      <th>Rfc</th>
                      <th>Nombre</th>
                      <th>Correo</th>
                      <th>Puesto</th>
                      <th>Departamento</th>
                      <th>Área</th>
                      <th>Centro de trabajo</th>
                      <th>Empresa</th>

              @foreach ($cuestionarios as $key => $value)

                      <th nowrap>{{$value}}</th>
              @endforeach

                  </tr>
              </thead>
              <tbody>
                  @if (!empty($evaluados))							
                      @foreach ($evaluados as $eval)
                          <tr>
                              <td class="text-center">
                                  {{ $eval->employee_wt->idempleado }}
                              </td>
                              <td>
                                  {{ $eval->employee_wt->rfc }}
                              </td>
                              <td>
                                  {{ $eval->FullName }}
                              </td>
                              <td>
                                  {{ $eval->email }}
                              </td>
                              <td>
                                  {{ optional($eval->employee_wt->jobPosition)->name }}
                              </td>
                              <td>
                                  {{-- {{ optional($eval->employee_wt->department())->name }} --}}
                              </td>
                              <td>
                                  {{-- {{ optional($eval->employee_wt->area())->name }} --}}
                              </td>
                              <td>
                                  {{ $eval->employee_wt->sucursal }}
                              </td>
                              <td>
                                  {{ (!empty($eval->employee_wt->enterprise) ? $eval->employee_wt->enterprise->name : '') }}
                              </td>

                            @if (count($cuestionarios) == 1)

                              <td class="text-center">

                              @if($eval->pivot->status == 1)
                                <button class="btn btn-danger btn-block btn-static">No iniciada</button>
                              @elseif($eval->pivot->status == 2)
                                <button class="btn btn-warning btn-block btn-static">Inconclusa</button>
                              @else
                                <button class="btn btn-success btn-block btn-static">Terminada</button>
                              @endif
                              </td>
                            @else
                              @foreach ($cuestionarios as $key => $value)
                                      
                              <td class="text-center">

                                @if ($key == 0)
                                  @if($eval->pivot->status < 21)
                                <button class="btn btn-danger btn-block btn-static">No iniciada</button>
                                  @elseif($eval->pivot->status > 20 && $eval->pivot->status < 31)
                                <button class="btn btn-warning btn-block btn-static">Inconclusa</button>
                                  @else
                                <button class="btn btn-success btn-block btn-static">Terminada</button>
                                  @endif
                                @else
                                  @if($eval->pivot->status % 10 == 1)
                                <button class="btn btn-danger btn-block btn-static">No iniciada</button>
                                  @elseif($eval->pivot->status % 10 == 2)
                                <button class="btn btn-warning btn-block btn-static">Inconclusa</button>
                                  @else
                                <button class="btn btn-success btn-block btn-static">Terminada</button>
                                  @endif
                                @endif
                              </td>
                              @endforeach
                            @endif
                          
                          </tr>
                      @endforeach
                  @else
                      <tr>
                          <td colspan="5" class="text-center">No hay evaluados.</td>
                      </tr>
                  @endif						
              </tbody>
          </table>
      </div>
    </div>
  </div>
</div>
@else

  <div class="text-center">
    <h2>Ya no cuentas con los permisos para ver esta información</h2>
  </div>
@endif

    </div>
    </div>
    </div>
    </div>

<form action="/nom035/avances" method="post" class="change_period_id">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="period_id" class="period_id">
  <input type="submit" style="display: none">
</form>

@endsection
@section('scripts')

<style>div.dt-buttons {
	float: right;
	margin-left:10px;
	display: none;
	}</style>
   
<script type="text/javascript">




$('.btn-excel').click(function(){
			$('.buttons-excel').trigger('click');
		});

var no_iniciados = {{ json_encode($no_iniciados) }};
var iniciados = {{ json_encode($iniciados) }};
var terminados = {{ json_encode($terminados) }};
var cuestionarios = <?php echo json_encode($cuestionarios)?>;
var table = 0;

$(document).ready(function(){
	table = $('#evaluados').DataTable({
    // dom: 'Blfrtip',
				dom:"<'row'<'col-md-6'l><'col-md-6'Bf>>" +
"<'row'<'col-md-6'><'col-md-6'>>" +
"<'row'<'col-md-12't>><'row'<'col-md-12'ip>>",
		language: {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		},
		buttons: [
			{
				extend: 'excel',
				text: 'Exportar a Excel',
				titleAttr: 'Exportar a Excel',
				title: 'Avance Encuesta',
			}
		]
    });

    // Cambia el periodo
    $('select.period_id').change(function(){

      // Recarga la pagina con el nuevo periodo
      $('form.change_period_id .period_id').val($(this).val());
      $('form.change_period_id').submit();
    });

    $('div.bg-danger').click(function(){

      var index = $(this).attr('data-index');
      table.columns().search('').draw();
      table.columns([index]).search('No iniciada').draw();
      $('div#mostrarAll').show();
      $('div.info-box i').removeClass('red-background');
      $(this).find('i').addClass('red-background');
    });

    $('div.bg-warning').click(function(){

      var index = $(this).attr('data-index');
      table.columns().search('').draw();
      table.columns([index]).search('Inconclusa').draw();
      $('div#mostrarAll').show();
      $('div.info-box i').removeClass('red-background');
      $(this).find('i').addClass('red-background');
    });

    $('div.bg-success').click(function(){

      var index = $(this).attr('data-index');
      table.columns().search('').draw();
      table.columns([index]).search('Terminada').draw();
      $('div#mostrarAll').show();
      $('div.info-box i').removeClass('red-background');
      $(this).find('i').addClass('red-background');
    });

    $('div#mostrarAll').click(function(){

      table.columns().search('').draw();
      $(this).hide();
      $('div.info-box i').removeClass('red-background');
    });
});

for (var i = 0;i < cuestionarios.length;i++){

Highcharts.chart('container' + i, {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: null,
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'white'
                }
            }
        }
    },
    series: [{
        states: {
			hover: {
				enabled: false
			}
		},
		enableMouseTracking: false,
        data: [
            {
                y: no_iniciados[i],
                color: '#e3342f'
            }, {
                y: iniciados[i],
                color: '#ffed4a'
            }, {
                y: terminados[i],
                color: '#38c172'
            }
        ],
    }],
});
}
</script>
@endsection