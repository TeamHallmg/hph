@extends('nom035.app')

@section('title', 'Crear Periodo')

@section('content')
<div class="text-center">
  <img src="{{ asset('img/banner_nom035.png') }}" alt="" class="img-fluid">
</div>
<h3 class="titulos-evaluaciones mt-4 font-weight-bold text-client-primary">Periodo</h3>

<form action="{{ url('nom035/periodos') }}" method="post" id="period_form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="card">
        <div class="card-header bg-client-primary text-white">
            Periodo
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="nombre" class="font-weight-bold requerido">Nombre</label>
                        <input type="text" class="form-control" name="name" required>
                    </div>
                    <div class="form-group">
                        <label for="estado" class="font-weight-bold">Estado</label>
                        <select class="form-control" name="status">
                            <option value="Preparatorio">Preparatorio</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">

                  @if (count($enterprises) > 0)

                    <div class="form-group">
                        <label for="enterprise_id" class="font-weight-bold">Empresa</label>
                        <select class="form-control enterprises" name="enterprise_id">

                      @if ($all_enterprises)

                          <option value="0">-- Todas --</option>
                      @endif

                      @foreach ($enterprises as $key => $enterprise)
                    
                          <option value="{{$enterprise->id}}">{{$enterprise->name}}</option>
                      @endforeach

                        </select>
                    </div>
                  @endif

                  @if (count($sucursaless) > 0)

                    <div class="form-group" style="display: none;">
                        <label for="sucursal_id" class="font-weight-bold">Centro de trabajo</label>
                        <select class="form-control sucursals" name="sucursal_id">
                          <option value="0">-- Todos --</option>
                        </select>
                    </div>
                  @endif

                </div>
            </div>
        </div>
    </div>

    <h3 class="titulos-evaluaciones mt-4 font-weight-bold text-client-primary">Evaluadores</h3>

    <div class="card">
        <div class="card-header bg-client-primary">
            <ul class="nav nav-pills">
                <li class="nav-item">
                    <a href="#pills-users" class="nav-link active" id="pills-users-tab" data-toggle="pill" role="tab" aria-controls="pills-users" aria-selected="true">Usuarios</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
        <div class="tab-content" id="pills-tabContent">
            <div class="p-3 tab-pane fade show active" id="pills-users" role="tabpanel" aria-labelledby="pills-users-tab">
                <div class="table-responsive">
                    <table class="table table-striped" id="table-evaluados">
                        <thead class="bg-client-primary text-white">
                            <tr>
                                <th>ID</th>
                                <th>Evaluado</th>
                                <th>Departamento</th>
                                <th>Área</th>
                                <th>Puesto</th>
                                <th>Centro de trabajo</th>
                                <th>Empresa</th>
                                <th>Sexo</th>
                                <th class="mx-auto">
                                    Todos
                                    <input type="checkbox" class="select_all_evaluados" id="users_check">
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($evaluados as $key => $evaluado)
                            <tr>
                                <td>{{ $evaluado->employee->idempleado }}</td>
                                <td>{{ $evaluado->fullname }}</td>
                                <td>{{ (isset($evaluado->employee->jobPosition->area->department) ? $evaluado->employee->jobPosition->area->department->name : '') }}</td>
                                <td>{{ (isset($evaluado->employee->jobPosition->area) ? $evaluado->employee->jobPosition->area->name : '') }}</td>
                                <td>{{ (!empty($evaluado->employee->jobPosition) ? $evaluado->employee->jobPosition->name : '') }}</td>
                                <td>{{ $evaluado->employee->sucursal }}</td>
                                <td>{{ (!empty($evaluado->employee->enterprise) ? $evaluado->employee->enterprise->name : '') }}</td>
                                <td>{{ $evaluado->employee->sexo }}</td>
                                <td>
                                    <input type="checkbox" name="{{ $evaluado->employee->sexo }}" value="{{ $evaluado->id }}" class="evaluados">
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="card-body row">
        <div class="card col-md-2 border-0 text-center">
            <h4 class="text-client-primary font-weight-bold pt-5">Seleccionados</h4>
        </div>
        <div class="col-md-3">
            <div class="card p-2">
                <div class="row no-gutters">
                    <div class="col-md-4 bg-success text-white rounded p-2 text-center">
                        <span><i class="fas fa-users fa-5x"></i></span>
                    </div>
                    <div class="col-md-8 p-3">
                        <h3 class="text-muted">Empleados</h3>
                        <h4 class="text-muted font-weight-bold" id="count_total">0</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card p-2">
                <div class="row no-gutters">
                    <div class="col-md-4 bg-info text-white rounded p-2 text-center">
                        <span><i class="fas fa-male fa-5x"></i></span>
                    </div>
                    <div class="col-md-8 p-3">
                        <h3 class="text-muted">Hombres</h3>
                        <h4 class="text-muted font-weight-bold" id="count_men">0</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card p-2">
                <div class="row no-gutters">
                    <div class="col-md-4 bg-pink text-white rounded p-2 text-center">
                        <span><i class="fas fa-female fa-5x"></i></span>
                    </div>
                    <div class="col-md-8 p-3">
                        <h3 class="text-muted">Mujeres</h3>
                        <h4 class="text-muted font-weight-bold" id="count_women">0</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
          

    <h3 class="titulos-evaluaciones mt-4 font-weight-bold text-client-primary px-3">Elegir Cuestionario(s)</h3>
    <div class="card border-0">
        <div class="card-header bg-transparent border-0">
            <ul class="nav nav-pills">
                <li class="nav-item">
                    <a href="#pills-uno" class="nav-link btn-question" id="pills-uno-tab" data-class="h_question_1" aria-controls="pills-uno" aria-selected="true">Cuestionario 1</a>
                </li>
                <li class="nav-item">
                    <a href="#pills-dos" class="nav-link btn-question" id="pills-dos-tab" data-class="h_question_2" aria-controls="pills-dos" aria-selected="true">Cuestionario 2</a>
                </li>
                <li class="nav-item">
                    <a href="#pills-tres" class="nav-link btn-question" id="pills-tres-tab" data-class="h_question_3" aria-controls="pills-tres" aria-selected="true">Cuestionario 3</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="row mt-4">
      <div class="col-md-6">
        <input type="text" class="form-control contrasena" placeholder="Contraseña para el acceso a los usuarios involucrados (no obligatorio)">
      </div>
      <div class="col-md-6">
        <button type="button" class="btn btn-primary cambiar_contrasena">Cambiar Contraseña</button>
      </div>
    </div>
    <div class="row p-4">
        <div class="col-12">
            <div class="float-right">
                <button class="btn btn-success mr-3" type="submit" style="color: white"><span class="fas fa-check-circle"></span> Crear</button> 
                <a class="btn btn-danger" href="{{ url('nom035/periodos') }}"><span class="fas fa-times-circle"></span> Regresar</a>
            </div>
        </div>
    </div>
</form>

@endsection

@section('scripts')
  <script>

    var checked = false;
    var total_users = 0;
    var cuestionario1 = false;
    var cuestionario2 = false;
    var cuestionario3 = false;
    var usersTable = 0;
    var contrasena = '';
    var sucursals = <?php echo json_encode($sucursaless)?>;

    var clonedthead = $('#table-evaluados thead tr').clone(true).appendTo( '#table-evaluados thead' );
    clonedthead.removeClass('bg-client-primary');
    clonedthead.addClass('bg-client-secondary');
    $('#table-evaluados thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();

        if (title != 'Todos '){

          $(this).html( '<input type="text" placeholder="'+title+'" style="color: black; width: 100px"/>' );
        }

        else{

          $(this).html('');
        }
 
        $( 'input[type="text"]', this ).on( 'keyup change', function () {
            if ( usersTable.column(i).search() !== this.value ) {
                usersTable
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    var questionsTable = $('.table-preguntas').DataTable({
        "paging": false,
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
    });

    var factorsTable = $('#table-factores').DataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
    });

    usersTable = $('#table-evaluados').DataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
    });
	  
    $(document).ready(function(){
        $('.select_all').click(function(){
            var allPstarted_ats = questionsTable.rows({ search: 'applied' }).nodes();
            if ($(this).prop('checked')){
                $('input[type="checkbox"]', allPstarted_ats).prop('checked', true);
            }else{
                $('input[type="checkbox"]', allPstarted_ats).prop('checked', false);
            }
        });

        questionsTable.on('search.dt', function(){
            if($('#questions_check').is(':checked')){
                $('#questions_check').prop('checked',false);
            }
            var allPstarted_atsItemsRemoved = questionsTable.rows({ search: 'removed' }).nodes();
            $('input[type="checkbox"]', allPstarted_atsItemsRemoved).prop('checked', false);
        });

        $('body').on('click', '.select_all_evaluados, .evaluados', function(){
            
            if ($(this).hasClass('select_all_evaluados')){
            
              var allPstarted_ats = usersTable.rows({ search: 'applied' }).nodes();
              checked = !checked;
              //if ($(this).prop('checked')){
              if (checked){

                $('input[type="checkbox"]', allPstarted_ats).prop('checked', true);
              }else{

                $('input[type="checkbox"]', allPstarted_ats).prop('checked', false);
              }
            }

            var users_array = usersTable.$('input').serializeArray()
            total_users = users_array.length;
            $('.btn-question').removeClass('active');
            cuestionario1 = cuestionario2 = cuestionario3 = false;
            var users_message = '';
            var men = 0, women = 0;

            if (total_users > 0){
              
              $.each(users_array, function(i, field){
                if (this.name == 'M'){
                  men++;
                }
                else{
                  women++;
                }
              });

              users_message = 'Número de Empleados Seleccionados: ' + total_users + '. Hombres: ' + men + ', Mujeres: ' + women;
            //   users_message = 
            }

            if (total_users < 15){

              $('a#pills-uno-tab').addClass('active');
              cuestionario1 = true;
            }

            else{

              if (total_users < 50){

                $('a#pills-uno-tab, a#pills-dos-tab').addClass('active');
                cuestionario1 = cuestionario2 = true;
              }

              else{

                $('a#pills-uno-tab, a#pills-tres-tab').addClass('active');
                cuestionario1 = cuestionario3 = true;
              }
            }

            // $('.mensaje_empleados').text(users_message);
            $("#count_total").text(total_users);
            $("#count_men").text(men);
            $("#count_women").text(women);
        });

        /*usersTable.on('search.dt', function(){
            if($('#users_check').is(':checked')){
                $('#users_check').prop('checked',false);
            }
            var allPstarted_atsItemsRemoved = usersTable.rows({ search: 'removed' }).nodes();
            $('input[type="checkbox"]', allPstarted_atsItemsRemoved).prop('checked', false);
        });*/

        $('#table-evaluados tbody tr').click(function (e) {
    
          if(!$(e.target).is('#table-evaluados td input:checkbox'))
    
            $(this).find('input:checkbox').trigger('click');
        });

        $('.btn-question').on('click', function(event){

          var id = $(this).attr('id');

          if (id == 'pills-uno-tab'){

            cuestionario1 = !cuestionario1;

            if (!cuestionario1){

              if (total_users < 50){

                $('a#pills-uno-tab').removeClass('active');
              }

              else{

                cuestionario1 = true;
              }
            }

            else{

              $('a#pills-uno-tab').addClass('active');
            }
          }

          else{

            if (id == 'pills-dos-tab'){

              cuestionario2 = !cuestionario2;

              if (!cuestionario2){

                $('a#pills-dos-tab').removeClass('active');
              }

              else{

                $('a#pills-dos-tab').addClass('active');

                if (cuestionario3){

                  $('a#pills-tres-tab').removeClass('active');
                  cuestionario3 = false;
                }
              }
            }

            else{

              cuestionario3 = !cuestionario3;

              if (!cuestionario3){

                $('a#pills-tres-tab').removeClass('active');
              }

              else{

                $('a#pills-tres-tab').addClass('active');

                if (cuestionario2){

                  $('a#pills-dos-tab').removeClass('active');
                  cuestionario2 = false;
                }
              } 
            } 
          }
            /*var d_class = $(this).data('class');
            $('.h_questions').prop("disabled", true);
            $('.' + d_class).prop('disabled', false);*/
        });

        $('#period_form').on('submit', function(e){
            var users_array = usersTable.$('input').serializeArray();
			      var users = [];
			     $.each(users_array, function(i, field){
				    users.push(this.value);
			     });
			     if(users.length > 0){
				$(this).append(
					$('<input>')
						.attr('type', 'hidden')
						.attr('name', 'users')
						.val(users)
				);
			}

          else{

            alert('Deben seleccionarse usuarios');
            return false;
          }

          if (!cuestionario1 && !cuestionario2 && !cuestionario3){

            alert('Debe seleccionarse por lo menos un cuestionario');
            return false;
          }

          else{

            var questions = [];

            if (cuestionario1){

              questions.push(1);
            }

            if (cuestionario2){

              questions.push(2);
            }

            if (cuestionario3){

              questions.push(3);
            }

            $(this).append(
              $('<input>')
              .attr('type', 'hidden')
              .attr('name', 'questions')
              .val(questions)
            );
          }
        });

        $('.cambiar_contrasena').click(function(){

          if (contrasena.trim().length > 0){

            var users_array = usersTable.$('input').serializeArray();
            var users = '';
            
            $.each(users_array, function(i, field){
              users += ',' + this.value;
            });
            
            if(users.length > 0){

              $.ajax({
                type:'POST',    
                url: '/nom035/cambiar-contrasena',
                data:{
                  contrasena: contrasena,
                  users: users.substr(1),
                '_token': '{{ csrf_token() }}'
                },
                success: function(data){
        
                  alert('Contrasena cambiada');  
                }
              });
            }

            else{

              alert('Deben seleccionarse usuarios');
            }
          }

          else{

            alert('La contraseña esta vacía');
            $('.contrasena').focus();
          }          
        });

        $('.contrasena').keyup(function(){

          var temp = '';
          var contra = $(this).val();
          var mask = '';

          if (contra.trim().length > 0){

            for (var i = 0;i < contra.length;i++){

              if (contra.charAt(i) == '*'){

                temp += contrasena.charAt(i);
              }

              else{

                temp += contra.charAt(i);
              }

              mask += '*';
            }

            contrasena = temp;
          }

          else{

            contrasena = '';
          }

          $('.contrasena').val(mask);
        });

        $('select.enterprises').change(function(){

          var enterprise_id = $(this).val();
          var content = '';
          var counter = 0;
          var enterprise_name = $(this).find('option[value="' + enterprise_id + '"]').text();

          for (var i = 0;i < sucursals.length; i++){
            
            if (sucursals[i].enterprise_id == enterprise_id){

              content += '<option value="' + sucursals[i].id + '">' + sucursals[i].name + '</option>';
              counter++;
            }
          }

          $('select.sucursals').html('<option value="0">-- Todos --</option>');

          if (counter == 0){

            $('select.sucursals').parent().hide();
          }

          else{

            $('select.sucursals').append(content);
            $('select.sucursals').parent().show();
          }

          if (enterprise_name == '-- Todas --'){

            usersTable.column(6).search('').column(5).search('').draw();
          }

          else{

            usersTable.column(6).search("^" + enterprise_name + "$", true, false, true).column(5).search('').draw();
          }
        });

        $('select.sucursals').change(function(){

          var sucursal_id = $(this).val();

          if (sucursal_id == 0){

            $('select.enterprises').change();
          }

          else{

            var enterprise_name = $('select.enterprises').find('option[value="' + $('select.enterprises').val() + '"]').text();
            var sucursal_name = $(this).find('option[value="' + sucursal_id + '"]').text();
            usersTable.column(6).search("^" + enterprise_name + "$", true, false, true).column(5).search("^" + sucursal_name + "$", true, false, true).draw();
          }
        });

        $('select.enterprises').change();
    });
  </script>
@endsection
