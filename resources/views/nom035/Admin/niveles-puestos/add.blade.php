@extends('layouts.app')

@section('title', 'Crear nivel de puestos')

@section('content')
<div class="row">
	<div class="col-md-12">
		<img class="img-fluid" src="{{ asset('img/clima-organizacional/banner.png') }} " alt="">
      <h3 class="titulos-evaluaciones mt-3 font-weight-bold">Crear nivel de puesto</h3>
      <hr class="mb-3">
      <form method="post" action="/crear-nivel-puesto/">
      	{{ csrf_field() }}
		@if(!$puestos->isEmpty())
			<div class="row">
				<div class="col-md-9">
					<div class="form-group">
						<label for="nombreNivelPuesto">Nombre del nivel de puesto:</label>
						<input type="text" class="form-control" required name="nombreNivelPuesto">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label for="mandoNivelPuesto">Mando:</label>
						<select class="form-control" required name="mandoNivelPuesto">
							<option value="0">No</option>
							<option value="1">Si</option>
						</select>
					</div>
				</div>
			</div>
		@endif

			<div class="form-group">
					<table class="table table-striped table-bordered" id="table-puestos">
						<thead style="background-color: #222B64; color:white;">
							<tr>
								<th>
									ID
								</th>
								<th>
									Nombre
								</th>
								<th>
									Acción
								</th>
							</tr>
						</thead>
						<tbody>
							@if($puestos->isEmpty())
									<tr>
										<td colspan="3">No hay puestos para seleccionar.</td>
									</tr>
								@endif
							@foreach($puestos as $puesto)
								<tr>
									<td align="center">{{ $puesto->puestos->id }}</td>
									<td>{{ str_replace('Ã‘','Ñ',$puesto->puestos->puesto) }}</td>
									<td align="center">
										<input type="checkbox" name="id_puestos[]" value="{{$puesto->puestos->id}}" class="field">
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
			</div>
			<div class="form-group">
				@if(!$puestos->isEmpty())
					<button type="submit" class="btn btn-success">
						<i class="fas fa-check-circle"></i> Guardar
					</button>
				@endif
				<a class="btn btn-primary" href="{{ URL::previous() }}"><span class="fas fa-chevron-circle-left"></span> Regresar</a>
			</div>
			</form>
		
	</div>
</div>
@endsection

@section('scripts')
<script>
/*$('#table-puestos').DataTable({
    language: {
        "sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
            }
    });*/


	$('#table-puestos tbody tr').click(function (e) {
    if(!$(e.target).is('#table-puestos td input:checkbox'))
    $(this).find('input:checkbox').trigger('click');
});
</script>
@endsection