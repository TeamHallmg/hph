@extends('nom035.app')

@section('title', 'Informe Personal')

@section('content')

<div class="text-center">
    @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['banner'], 'section' => 'Nom035']) 
    {{-- <img src="{{ asset('img/banner_nom035.png') }}" alt="" class="img-fluid"> --}}
  </div>
<div class="mx-5 px-5 mt-4">
  <h2 class="text-client-primary">Informe Personal</h2>

  @if (!empty($id_periodo_cerrado))

<div class="card my-3">
    <div class="card-header bg-client-secondary text-white">
        DATOS
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-3 text-center">

    @if (Auth::user()->profile != null && !is_null(Auth::user()->profile->image))

              <img src="{{ asset('uploads/' . Auth::user()->profile->image) }}" alt="" class="img-fluid img-thumbnail">
    @else

              <img src="{{ asset('img/default_foto.jpeg') }}" alt="" class="img-fluid img-thumbnail">                
    @endif

            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Nombre</label>
                            <input class="form-control" type="text" value="{{ $user->employee->nombre }}" disabled>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Apellido Paterno</label>
                            <input class="form-control" type="text" value="{{ $user->employee->paterno }}" disabled>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Apellido Materno</label>
                            <input class="form-control" type="text" value="{{ $user->employee->materno }}" disabled>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Departamento</label>
                            <input class="form-control" type="text" value="{{ (isset($user->employee->jobPosition->area->department) ? $user->employee->jobPosition->area->department->name : '') }}" disabled>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Área</label>
                            <input class="form-control" type="text" value="{{ (isset($user->employee->jobPosition->area) ? $user->employee->jobPosition->area->name : '') }}" disabled>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Puesto</label>
                            <input class="form-control" type="text" value="{{ (!empty($user->employee->jobPosition) ? $user->employee->jobPosition->name : '') }}" disabled>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Centro de trabajo</label>
                            <input class="form-control" type="text" value="{{ $user->employee->sucursal }}" disabled>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Empresa</label>
                            <input class="form-control" type="text" value="{{ (isset($user->employee->enterprise) ? $user->employee->enterprise->name : '') }}" disabled>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Número de Celular</label>
                            <input class="form-control" type="text" value="{{ $user->employee->celular }}" disabled>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Número de Teléfono Fijo</label>
                            <input class="form-control" type="text" value="{{ $user->employee->telefono }}" disabled>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Correo Electrónico</label>
                            <input class="form-control" type="text" value="{{ $user->email }}" disabled>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Sexo</label>
                            <input class="form-control" type="text" value="{{ $user->employee->sexo }}" disabled>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Fecha de Nacimiento</label>
                            <input class="form-control" type="text" value="{{ $user->employee->nacimiento }}" disabled>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card my-5">
    <div class="card-header bg-client-secondary text-white">
        PERSONAL
    </div>
    <div class="card-body">
        <div class="row justify-content-center text-center">
            <div class="col-md-4 my-3 py-4 {{ $global_result?'bg-danger':'bg-success' }}" style="border-radius: .5rem">
                <h3>{{ $global_result?'Requiere valoración clínica':'No Requiere valoración clínica' }}</h3>
            </div>
        </div>
        <div class="row mt-3">
        @foreach ($factor as $key => $value)
            <div class="col-md-6">
                <table class="table">
                    <tbody>
                        <tr>
                            <td style="width: 80%">{{ $key }}</td>
                            <td style="width: 10%" class="{{ !$value?'bg-success':'bg-secondary' }}">Verde</td>
                            <td style="width: 10%" class="{{ $value?'bg-danger':'bg-secondary' }}">Rojo</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        @endforeach
        </div>
    </div>
</div>

<div class="card my-5">
    <div class="card-header bg-client-secondary text-white">
        ACCIONES A TRABAJAR
    </div>
    <div class="card-body">
        @if(count($avances) > 0)
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <table class="table">
                        <thead class="bg-client-primary text-white">
                            <tr>
                                <th>Registro</th>
                                <th>Avance</th>
                            </tr>
                        </thead>
                        <tbody>

          @for ($i = 0;$i < count($avances); $i++)

                            <tr>
                                <td>{{ $avances[$i]->created_at }}</td>
                                <td>{{ $avances[$i]->avances }}</td>
                            </tr>
          @endfor
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    </div>
</div>

<div class="card my-5">
    <div class="card-header bg-client-secondary text-white">
        PROGRAMACIÓN PARA LA ATENCIÓN DE RIESGOS PSICOSOCIALES
    </div>
    <div class="card-body">
        <table class="table">
            <thead class="bg-client-primary text-white">
                <tr>
                    <th>Categoría</th>
                    <th>Medida o Acción</th>
                    <th>Fecha Implementar</th>
                    <th>Medio de Verificación</th>
                </tr>
            </thead>
            <tbody>
                @foreach($planes as $plan)
                    <tr>
                        <td>{{ $plan->category->name }}</td>
                        <td>{{ $plan->accion }}</td>
                        <td>{{ $plan->fecha_implementar }}</td>
                        <td>{{ $plan->medio_verificacion }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@else

  <h4 class="text-center">Disponible hasta que se cierre la evaluación</h4>
@endif

</div>

@endsection