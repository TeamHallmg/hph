<div class="card-body">
     <div class="row">
         <div class="col-md-3">
            <div class="form-group">
                <label for="">Periodo</label>
                <select class="cambiando form-control _change" id="select_periodo" name="periodo">
                    @foreach($periodos as $periodo)
                        <option value="{{ $periodo->id }}" {{ $periodo->id==$period_id?'selected':'' }} >{{ $periodo->name }} ({{ $periodo->status }})</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Departamento</label>
                <select class="cambiando form-control _change" id="select_sucursal" name="sucursal">
                    <option value="">--TODO--</option>
                    @foreach($sucursales as $sucursal)
                        <option value="{{ $sucursal['id'] }}">{{ $sucursal['nombre'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Areas</label>
                <select class="cambiando form-control _change" id="select_direccion" name="area">
                    <option value="">--TODO--</option>
                    @foreach($directions as $direction)
                        <option value="{{ $direction['id'] }}">{{ $direction['nombre'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Categoría de Puestos</label>
                <select class="cambiando form-control _change" id="select_job" name="puesto">
                    <option value="">--TODO--</option>
                    @foreach($jobs as $job)
                        <option value="{{ $job->id }}">{{ $job->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Edad</label>
                <select class="cambiando form-control _change" id="select_edad" name="edad">
                    <option value="">--TODO--</option>
                    @foreach($ages as $age)
                        <option value="{{ $age['id'] }}">{{ $age['nombre'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Antigüedad</label>
                <select class="cambiando form-control _change" id="select_inicio" name="inicio">
                    <option value="">--TODO--</option>
                    @foreach($starts as $start)
                        <option value="{{ $start['id'] }}">{{ $start['nombre'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Sexo</label>
                <select class="cambiando form-control _change" id="select_sexo" name="sexo">
                    <option value="">--TODO--</option>
                    @foreach($sexs as $sex)
                        <option value="{{ $sex['id'] }}">{{ $sex['nombre'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Centro de Trabajo</label>
                <select class="cambiando form-control _change" id="select_centro_trabajo" name="centro_trabajo">
                    <option value="">--TODO--</option>
                    @foreach($centros_trabajo as $centro_trabajo)
                        <option value="{{ $centro_trabajo['id'] }}">{{ $centro_trabajo['nombre'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>

<div class="card-footer">
  <button class="btn btn-info _reset btn-sm float-right" > Reiniciar Filtros</button>
</div>
