
<div class="row">

    {{-- CLASIFICACION POR CATEGORIAS --}}
    <div class="col-md-12">

        <div class="card">

            <div class="card-body">

                <h4 class="text-client-primary">Categorias</h4>
                    
                <div id="table-categorias">
                </div>

            </div>

        </div>

    </div>
 
</div>
