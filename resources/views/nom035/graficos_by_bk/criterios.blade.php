 <div class="col p-1 d-flex">

  	<div class="card">
  		
  		<div class="card-header rojo">

  			<h5>Muy alto</h5>

  		</div>

  		<div class="card-body text-justify" style="font-size: smaller;">
  			Se requiere realizar el análisis de cada categoría y dominio para establecer las acciones de intervención apropiadas, mediante un Programa de intervención que deberá incluir evaluaciones específicas, y contemplar campañas de sensibilización, revisar la política de prevención de riesgos psicosociales y programas para la prevención de los factores de riesgo psicosocial, la promoción de un entorno organizacional favorable y la prevención de la violencia laboral, así como reforzar su aplicación y difusión.
  		</div>

  	</div>

</div> 

<div class="col p-1 d-flex">

  	<div class="card">
  		
  		<div class="card-header naranja">

  			<h5>Alto</h5>

  		</div>

  		<div class="card-body text-justify" style="font-size: smaller;">
  			Se requiere realizar un análisis de cada categoría y dominio, de manera que se puedan determinar las acciones de intervención apropiadas a través de un Programa de intervención, que podrá incluir una evaluación específica y deberá incluir una campaña de sensibilización, revisar la política de prevención de riesgos psicosociales y programas para la prevención de los factores de riesgo psicosocial, la promoción de un entorno organizacional favorable y la prevención de la violencia laboral, así como reforzar su aplicación y difusión.
  		</div>

  	</div>
  	
</div>

<div class="col p-1 d-flex">

  	<div class="card">
  		
  		<div class="card-header amarillo">

  			<h5>Medio</h5>

  		</div>

  		<div class="card-body text-justify" style="font-size: smaller;">
  			Se requiere revisar la política de prevención de riesgos psicosociales y programas para la prevención de los factores de riesgo psicosocial, la promoción de un entorno organizacional favorable y la prevención de la violencia laboral, así como reforzar su aplicación y difusión, mediante un Programa de intervención.
  		</div>

  	</div>
  	
</div>

<div class="col p-1 d-flex">

  	<div class="card">
  		
  		<div class="card-header verde">

  			<h5>Bajo</h5>

  		</div>

  		<div class="card-body text-justify" style="font-size: smaller;">
  			Es necesario una mayor difusión de la política de prevención de riesgos psicosociales y programas para: la prevención de los factores de riesgo psicosocial, la promoción de un entorno organizacional favorable y la prevención de la violencia laboral.
  		</div>

  	</div>
  	
</div>

<div class="col p-1 d-flex">

  	<div class="card">
  		
  		<div class="card-header turqueza">

  			<h5>Nulo</h5>

  		</div>

  		<div class="card-body text-justify" style="font-size: smaller;">
  			El riesgo resulta despreciable por lo que no se requiere medidas adicionales.
  		</div>

  	</div>
  	
</div>