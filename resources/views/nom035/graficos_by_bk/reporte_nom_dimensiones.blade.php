
<div class="row">

    {{-- CLASIFICACION POR CATEGORIAS --}}
    <div class="col-md-7">

        <div class="card">

            <div class="card-body">
                <div id="table-dimensiones">
               <table class="table table-striped" id="Dimensiones">
                    <thead class="bg-client-primary text-white">
                        <tr>
                            <th>Categoría</th>
                            <th>Dominio</th>
                            <th>Dimension</th>
                            <th>Para la calificación final:</th>
                        </tr>
                    </thead>
            
                    <tbody id="DataResultreporte_nom_dimensiones">

                        @foreach ($clasificacion_dimensiones as $key => $clasificacion_categoria)
                            
                            <tr>
                                <td>{!! $clasificacion_categoria['category'] !!}</td>
                                <td>{!! $clasificacion_categoria['domain'] !!}</td>
                                <td>{!! $clasificacion_categoria['name'] !!}</td>

                                <td class="text-right font-weight-bold {!! $colores_dominios[$clasificacion_categoria['domain_id']] !!}">
                                  {{ is_int($clasificacion_categoria['data']['0']) ? $clasificacion_categoria['data']['0'] : number_format($clasificacion_categoria['data']['0'], 2) }} 
                                </td>

                            </tr>

                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>

        </div>

    </div>

</div>

<hr>

<div class="row">

    <div class="col-md-12">

        <div class="card">

            <div class="card-body">

                <div id="containerdimensiones" class="my-3">

                </div>

            </div>

        </div>

    </div>

</div>
