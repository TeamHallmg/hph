<div class="row">

    {{-- CLASIFICACION POR CATEGORIAS --}}
    <div class="col-md-6">

        <div class="card">

            <div class="card-body">

                <h4 class="text-client-primary">Dominios</h4>
                <div id="table-dominios">
               <table class="table table-striped" id="dominios">
                    <thead class="bg-client-primary text-white">
                        <tr>
                            <th>Categoría</th>
                            <th>Dominio</th>
                            <th>Para la calificación final:</th>
                        </tr>
                    </thead>

                    <tbody>

                        @foreach ($clasificacion_dominios as $key => $clasificacion_categoria)
                            
                            <tr>
                                <td>{!! $clasificacion_categoria['category'] !!}</td>
                                <td>{!! $clasificacion_categoria['name'] !!}</td>

                                <td class="text-right font-weight-bold {!! $clasificacion_categoria['colors'] !!}">
                                    {{ is_int($clasificacion_categoria['critico']['0']) ? $clasificacion_categoria['critico']['0'] : number_format($clasificacion_categoria['critico']['0'], 2) }}
                                </td>

                            </tr>

                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>

        </div>

    </div>


    {{-- CLASIFICACION POR CATEGORIAS --}}
    <div class="col-md-6">

        <div class="card">

            <div class="card-body">

                <div id="containerdominios" class="my-3">

                </div>

            </div>

         </div>

    </div>

</div>

<hr>

<div class="row">

    {{-- CLASIFICACION POR CATEGORIAS --}}
    <div class="col-md-12">

        <div class="card">

            <div class="card-body">

                <h4 class="text-client-primary">Detalle por Empleados</h4>
                <div class="table-responsive">
                    <div id="table-empleados-dominios">
                    <table class="table table-striped" id="table_usuarios_dominios">
                        <thead class="bg-client-primary text-white">
                            <tr>
                                <th>Empleado</th>
                                <th>Departamento</th>
                                <th>Área</th>
                                <th>Puesto</th>
                                <th>Centro de trabajo</th>
                                <th>Empresa</th>
                                @foreach ($name_dominios as $categories)
                                <th>{!! $categories['name'] !!}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($clasificacion_dominios_by_usuarios as $key => $usuarios)
                                <?php $count=0; 
                                $count = count($usuarios);


                                ?>
                                <tr>
                          <?php $personal = explode('*', $usuarios['0']);
                                $department = explode('#', $personal[1]);
                                $area = explode('=', $department[1]);
                                $jobPosition = explode('$', $area[1]);
                                $sucursal = explode('[', $jobPosition[1]);
                                $empresa = $sucursal[1]; ?>
                                    <td>{{ $personal[0] }}</td>
                                    <td>{{ $department[0] }}</td>
                                    <td>{{ $area[0] }}</td>
                                    <td>{{ $jobPosition[0] }}</td>
                                    <td>{{ $sucursal[0] }}</td>
                                    <td>{{ $empresa }}</td>
                                    @for($i=1; $i<$count; $i++)
                                    <td class="text-right font-weight-bold {{ $usuarios[$i]['color'] }}">
                                        {{ $usuarios[$i]['valor'] }}
                                    </td>
                                    @endfor
                                   
                                </tr>


                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>

