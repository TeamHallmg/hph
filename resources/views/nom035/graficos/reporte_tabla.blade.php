@extends('nom035.app')

@section('content')

<div class="card mt-3">
    <div class="card-header p-3">
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Factor</label>
                    <select class="form-control _change" id="select_factor">
                        <option value="">--TODO--</option>
                        @foreach($factors as $factor)
                            <option value="{{ $factor->id }}">{{ $factor->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Dirección</label>
                    <select class="form-control _change" id="select_direction">
                        <option value="">--TODO--</option>
                        @foreach($directions as $direction)
                            <option value="{{ $direction->name }}">{{ $direction->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Departamento</label>
                    <select class="form-control _change" id="select_department">
                        <option value="">--TODO--</option>
                        @foreach($departments as $department)
                            <option value="{{ $department->name }}">{{ $department->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Área</label>
                    <select class="form-control _change" id="select_area">
                        <option value="">--TODO--</option>
                        @foreach($areas as $area)
                            <option value="{{ $area->name }}">{{ $area->name }}</option>
                        @endforeach
                    </select> 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Puestos</label>
                    <select class="form-control _change" id="select_job">
                        <option value="">--TODO--</option>
                        @foreach($jobs as $job)
                            <option value="{{ $job->name }}">{{ $job->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Nivel</label>
                    <select class="form-control _change" id="select_job_level">
                        <option value="">--TODO--</option>
                        @foreach ($jobLevels as $level)
                            <option value="{{ $level->id }}">{{ $level->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Grupo de Áreas</label>
                    <select class="form-control _change" id="select_group_areas">
                        <option value="">--TODO--</option>
                        @foreach($groupAreas as $group)
                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Grupo de Puestos</label>
                    <select class="form-control _change" id="select_group_jobs">
                        <option value="">--TODO--</option>
                        @foreach($groupJobs as $group)
                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="table-responsive">
    <table class="table" id="reportTable">
        <thead>
            <tr>
                <th>#</th>
                <th>Preguntas</th>
                <th>Totalmente desacuerdo</th>
                <th>Desacuerdo</th>
                <th>Mas o menos</th>
                <th>De acuerdo</th>
                <th>Totalmente de acuerdo</th>
                <th>Hombre</th>
                <th>Mujeres</th>
                <th>Otros</th>
                <th>Num. Personas</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($questions as $question)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $question->question }}</td>
                    @foreach ($question->cont as $answer)
                        <td>{{ $answer }}</td>
                    @endforeach
                    <td>{{ $question->hombres }}</td>
                    <td>{{ $question->mujeres }}</td>
                    <td>{{ $question->otros }}</td>
                    <td>{{ $question->total }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="table-responsive">
    <table class="table" id="reportTable">
        <thead>
            <tr>
                <th style="width: 5%">#</th>
                <th style="width: 30%">Preguntas</th>
                <th style="width: 35%"></th>
                <td style="width: 25%">Resultados</td>
                <th style="width: 5%">Num. Personas</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($questions as $question)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $question->question }}</td>
                    <td>
                        <div class="progress my-1">
                            <div class="progress-bar bg-success" role="progressbar" style="width: {{ round($question->t_value * 100 / $question->max) }}%" aria-valuenow="{{ round($question->t_value * 100 / $question->max) }}" aria-valuemin="0" aria-valuemax="{{ $question->max }}">
                                {{ round($question->t_value * 100 / $question->max) }}%
                            </div>
                        </div>
                    </td>
                    <td>
                        @foreach ($question->cont as $answer)
                            <div class="progress my-1">
                                <div class="progress-bar" role="progressbar" style="width: {{ round($answer * 100 / $question->total) }}%" aria-valuenow="{{ round($answer * 100 / $question->total) }}" aria-valuemin="0" aria-valuemax="{{ $question->total }}">
                                    {{ round($answer * 100 / $question->total) }}%
                                </div>
                            </div>
                        @endforeach
                    </td>
                    <td>{{ $question->total }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection


@section('scripts')
<script>

$(function (){
    $('._change').on('change', function(){
        axios.post('{{ route("nom035.reports.post_tabla") }}', {
            factor: $('#select_factor').val(),
            direction: $('#select_direction').val(),
            department: $('#select_department').val(),
            area: $('#select_area').val(),
            job: $('#select_job').val(),
            job_level: $('#select_job_level').val(),
            groupJob: $('#select_group_jobs').val(),
            groupArea: $('#select_group_areas').val(),
        })
        .then(function (response){
            reportTable.rows().remove().draw();
            reportTable.rows.add(response.data).draw();
        })
        .catch(function (error){

        });
    });

    var reportTable = $('#reportTable').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        columns: [
            { data: 'pos' },
            { data: 'question' },
            { data: 'cont1' },
            { data: 'cont2' },
            { data: 'cont3' },
            { data: 'cont4' },
            { data: 'cont5' },
            { data: 'hombres' },
            { data: 'mujeres' },
            { data: 'otros' },
            { data: 'total' }
        ]
    });
});
</script>
@endsection