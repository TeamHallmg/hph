@extends('nom035.app')

@section('content')

<div class="text-center">
    <img src="{{ asset('img/banner_nom035.png') }}" alt="" class="img-fluid">
  </div>
  <div class="text-center mt-3">
    <img src="{{ asset('img/NOM035_Tabla_de_criterios.png') }}" class="img-fluid" alt="">
</div>

<div class="card mt-3">
    <div class="card-header p-3" style="background-color: rgb(122, 117, 181); color: white;">
        <h4>Filtros</h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Sección</label>
                    <select class="cambiando form-control _change" id="select_factor">
                        <option value="">--TODO--</option>
                        @foreach($factors as $factor)
                            <option value="{{ $factor->id }}">{{ $factor->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Dirección</label>
                    <select class="cambiando form-control _change" id="select_direction">
                        <option value="">--TODO--</option>
                        @foreach($directions as $direction)
                            <option value="{{ $direction->name }}">{{ $direction->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Departamento</label>
                    <select class="cambiando form-control _change" id="select_department">
                        <option value="">--TODO--</option>
                        @foreach($departments as $department)
                            <option value="{{ $department->name }}">{{ $department->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Área</label>
                    <select class="cambiando form-control _change" id="select_area">
                        <option value="">--TODO--</option>
                        @foreach($areas as $area)
                            <option value="{{ $area->name }}">{{ $area->name }}</option>
                        @endforeach
                    </select> 
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Puestos</label>
                    <select class="cambiando form-control _change" id="select_job">
                        <option value="">--TODO--</option>
                        @foreach($jobs as $job)
                            <option value="{{ $job->name }}">{{ $job->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Nivel</label>
                    <select class="cambiando form-control _change" id="select_job_level">
                        <option value="">--TODO--</option>
                        @foreach ($jobLevels as $level)
                            <option value="{{ $level->id }}">{{ $level->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Grupo de Áreas</label>
                    <select class="cambiando form-control _change" id="select_group_areas">
                        <option value="">--TODO--</option>
                        @foreach($groupAreas as $group)
                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Grupo de Puestos</label>
                    <select class="cambiando form-control _change" id="select_group_jobs">
                        <option value="">--TODO--</option>
                        @foreach($groupJobs as $group)
                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
					<label for="">Categoría</label>
					<select name="" class="form-control cambiando">
						<option value="">...Seleccione una opción</option>
						@foreach ($categories as $key => $category)
							<option value="{{ $category->id }}">{{ $category->name }}</option>
						@endforeach
					</select>
				</div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
					<label for="">Dominio</label>
					<select name="" class="form-control cambiando">
						<option value="">...Seleccione una opción</option>
						@foreach ($domains as $key => $domain)
							<option value="{{ $domain->id }}">{{ $domain->name }}</option>
						@endforeach
					</select>
				</div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Dimensión</label>
                    <select name="" class="form-control cambiando">
                        <option value="">...Seleccione una opción</option>
                        @foreach ($dimensions as $key => $dimension)
                            <option value="{{ $dimension->id }}">{{ $dimension->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="container1" class="my-3">

</div>

<div id="container2" class="my-3">

</div>

<div id="container3" class="my-3">

</div>

@endsection


@section('scripts')
<script>

$(function (){
    
    var graf1 = Highcharts.chart('container1', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Calificación de la Categoría'
        },
        subtitle: {
            text: 'General'
        },
        xAxis: {
            categories: {!! json_encode($name_categories) !!},
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0,
                zones: [{
                    value:20,
                    color: '#0098DA'
                },{
                    value:45,
                    color: '#A8CF45',
                },{
                    value:70,
                    color: '#FFCC29',
                },{
                    value:90,
                    color: '#F58634',
                },{
                    color: '#Ed3237',
                }]
            }
        },
        series: {!! json_encode($cuestionario_categorias) !!}
    });

    var graf2 = Highcharts.chart('container2', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Calificación del Dominio'
        },
        subtitle: {
            text: 'General'
        },
        xAxis: {
            categories: {!! json_encode($name_domains) !!},
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0,
                zones: [{
                    value:20,
                    color: '#0098DA'
                },{
                    value:45,
                    color: '#A8CF45',
                },{
                    value:70,
                    color: '#FFCC29',
                },{
                    value:90,
                    color: '#F58634',
                },{
                    color: '#Ed3237',
                }]
            }
        },
        series: {!! json_encode($cuestionario_dominions) !!}
    });

    var graf3 = Highcharts.chart('container3', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Calificación de la Dimensión'
        },
        subtitle: {
            text: 'General'
        },
        xAxis: {
            categories: {!! json_encode($name_dimensions) !!},
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0,
                zones: [{
                    value:20,
                    color: '#0098DA'
                },{
                    value:45,
                    color: '#A8CF45',
                },{
                    value:70,
                    color: '#FFCC29',
                },{
                    value:90,
                    color: '#F58634',
                },{
                    color: '#Ed3237',
                }]
            }
        },
        series: {!! json_encode($cuestionario_dimensions) !!}
    });

    $('.cambiando').on('change', function(){
        axios.post('{{ url("nom035/reporte_nom_graf") }}')
        .then(function (response){
            var aux_chart = {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Calificación de la Categoría'
                },
                subtitle: {
                    text: 'General'
                },
                xAxis: {
                    categories: response.data.categories,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    }
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0,
                        zones: [{
                            value:20,
                            color: '#0098DA'
                        },{
                            value:45,
                            color: '#A8CF45',
                        },{
                            value:70,
                            color: '#FFCC29',
                        },{
                            value:90,
                            color: '#F58634',
                        },{
                            color: '#Ed3237',
                        }]
                    }
                },
                series: response.data.cuestionario_categorias
            };
            var chart = new Highcharts.chart('container1', aux_chart); 

            var aux_chart = {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Calificación del Dominio'
                },
                subtitle: {
                    text: 'General'
                },
                xAxis: {
                    categories: response.data.domains,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    }
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0,
                        zones: [{
                            value:20,
                            color: '#0098DA'
                        },{
                            value:45,
                            color: '#A8CF45',
                        },{
                            value:70,
                            color: '#FFCC29',
                        },{
                            value:90,
                            color: '#F58634',
                        },{
                            color: '#Ed3237',
                        }]
                    }
                },
                series: response.data.cuestionario_dominions
            };
            var chart = new Highcharts.chart('container2', aux_chart); 

            var aux_chart = {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Calificación de la Dimensión'
                },
                subtitle: {
                    text: 'General'
                },
                xAxis: {
                    categories: response.data.dimensions,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    }
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0,
                        zones: [{
                            value:20,
                            color: '#0098DA'
                        },{
                            value:45,
                            color: '#A8CF45',
                        },{
                            value:70,
                            color: '#FFCC29',
                        },{
                            value:90,
                            color: '#F58634',
                        },{
                            color: '#Ed3237',
                        }]
                    }
                },
                series: response.data.cuestionario_dimensions
            };
            var chart = new Highcharts.chart('container3', aux_chart); 
        });
    });
});
</script>
@endsection