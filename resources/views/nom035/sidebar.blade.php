<li class="nav-item has-treeview">
    <a href="#" class="nav-link">
            <i class="nav-icon far fa-file-alt"></i>
        <p>
        NOM-035
        <i class="right fa fa-angle-left"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a class="nav-link" href="{{ url('nom035/que-es') }}">
                <i class="fa fa-plus nav-icon"></i>
                <p><span>¿Qué es?</span></p>
            </a>
        </li>
        @if(Auth::user()->role != 'admin')
            <li class="nav-item">
                <a class="nav-link" href="{{ url('nom035/evaluaciones') }}">
                    <i class="fa fa-plus nav-icon"></i>
                    <p><span>Encuesta</span></p>
                </a>
            </li>
        @endif
        @if (auth()->user()->role != 'admin')
            <li class="nav-item">
                <a class="nav-link" href="{{ url('nom035/informe') }}">
                    <i class="fa fa-plus nav-icon"></i>
                    <p><span>Informe Personal</span></p>
                </a>
            </li>
        @endif
        @if (auth()->user()->role == 'admin' || isset($userPermissions['see_progress']))
            <li class="nav-item">
                <a class="nav-link" href="{{ url('nom035/avances') }}">
                    <i class="fa fa-plus nav-icon"></i>
                    <p><span>Avances</span></p>
                </a>
            </li>
        @endif
        @if (auth()->user()->role == 'admin' || isset($userPermissions['see_reports']))
            <li class="nav-item">
                {{-- <a class="nav-link" href="{{ url('nom035/reportes') }}"> --}}
                <a class="nav-link" href="{{ url('nom035/reporte_nom') }}">
                    <i class="fa fa-plus nav-icon"></i>
                    <p><span>Reportes</span></p>
                </a>
            </li>
            <li class="nav-item">
                {{-- <a class="nav-link" href="{{ url('nom035/reportes') }}"> --}}
                <a class="nav-link" href="{{ url('nom035/plan_individual') }}">
                    <i class="fa fa-plus nav-icon"></i>
                    <p><span>Plan Individual</span></p>
                </a>
            </li>
            <li class="nav-item">
                {{-- <a class="nav-link" href="{{ url('nom035/reportes') }}"> --}}
                <a class="nav-link" href="{{ url('nom035/plan_accion') }}">
                    <i class="fa fa-plus nav-icon"></i>
                    <p><span>Plan de Acción</span></p>
                </a>
            </li>
        @endif
        @if (auth()->user()->role == 'admin' || isset($userPermissions['see_reports']))
            {{-- <li class="nav-item">
                <a class="nav-link" href="{{ url('nom035/reporte_tabla') }}">
                    <i class="fa fa-plus nav-icon"></i>
                    <p><span>Reporte Tabla</span></p>
                </a>
            </li> --}}
        @endif
        @if (auth()->user()->role == 'admin' || isset($userPermissions['see_factors']))
            {{-- <li class="nav-item">
                <a class="nav-link" href="{{ url('nom035/factores') }}">
                    <i class="fa fa-plus nav-icon"></i>
                    <p><span>Factores</span></p>
                </a>
            </li> --}}
        @endif
        @if (auth()->user()->role == 'admin' || isset($userPermissions['see_questions']))
            {{-- <li class="nav-item">
                <a class="nav-link" href="{{ url('nom035/preguntas') }}">
                    <i class="fa fa-plus nav-icon"></i>
                    <p><span>Preguntas</span></p>
                </a>
            </li> --}}
        @endif
        @if (auth()->user()->role == 'admin' || isset($userPermissions['see_periods']))
            <li class="nav-item">
                <a class="nav-link" href="{{ url('nom035/periodos') }}">
                    <i class="fa fa-plus nav-icon"></i>
                    <p><span>Periodos</span></p>
                </a>
            </li>
        @endif
        @if (auth()->user()->role == 'admin' || isset($userPermissions['see_periods']))
            <li class="nav-item">
                <a class="nav-link" href="{{ url('nom035/evaluations') }}">
                    <i class="fa fa-plus nav-icon"></i>
                    <p><span>Evaluaciones</span></p>
                </a>
            </li>
        @endif
        @if (auth()->user()->role == 'admin' || isset($userPermissions['import_questions']))
            {{-- <li class="nav-item">
                <a class="nav-link" href="{{ url('nom035/import') }}">
                    <i class="fa fa-cogs nav-icon"></i>
                    <p><span>Importar</span></p>
                </a>
            </li> --}}
        @endif
    </ul>
</li>