@extends('nom035.app')

@section('content')

<div class="card">
    <div class="card-header">
        <h3>PlAN DE TRABAJO</h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label>Nombre</label>
                    <input type="text" class="form-control" value="{{ $plan->user->FullName }}" disabled>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Puesto</label>
                    <input type="text" class="form-control" value="" disabled>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Área</label>
                    <input type="text" class="form-control" value="" disabled>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Fecha</label>
                    <input type="text" class="form-control" value="" disabled>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label>Resultado</label>
                    <input type="text" class="form-control" value="Requiere valoración clínica" disabled>
                </div>
            </div>
        </div>
        <form action="{{ url('nom035/plan_individual/' . $plan->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Control de los Avances</label>
                        <textarea name="avances" class="form-control" rows="3">{{ $plan->avances }}</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Evaluación Posterior Fecha y Avances</label>
                        <textarea name="avances_posterior" class="form-control" rows="3">{{ $plan->avances_posterior }}</textarea>
                    </div>
                </div>
            </div>
            <div class="text-right">
                <input type="submit" value="Guardar" class="btn btn-success">
            </div>
        </form>
    </div>
</div>

@endsection