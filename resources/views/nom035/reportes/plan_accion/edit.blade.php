@extends('nom035.app')

@section('title', '¿Que es?')

@section('content')

<div class="text-center">
  <img src="{{ asset('img/banner_nom035.png') }}" alt="" class="img-fluid">
</div>
<div class="card mt-4">
    <div class="card-header bg-client-primary text-white">
        <h3>CREAR MEDIDA O ACCIÓN</h3>
    </div>
    <div class="card-body">
        <form action="{{ url('nom035/plan_accion/' . $plan->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <input type="hidden" name="period_id" value="{{ $period->id }}">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Categoría</label>
                        <select name="category_id" class="form-control categories" required>
                            @foreach ($categories as $key => $category)
                                <option value="{{ $key }}" {{ ($key == $plan->category->id ? 'selected' : '' )}} >{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Dominio</label>
                        <select name="domains_ids[]" class="form-control domains" multiple>
                            @foreach ($domains as $key => $domain)
                                <option value="{{ $domain->id }}" data-category="{{$domain->category_id}}" {{($domain->category_id != $plan->category->id ? 'style=display:none' : (in_array($domain->id, $plan->domains_ids) ? 'selected="selected"' : ''))}}>{{ $domain->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Dimension</label>
                        <select name="dimensions_ids[]" class="form-control dimensions" multiple>
                            @foreach ($dimensions as $key => $dimension)
                                <option value="{{ $dimension->id }}" data-domain="{{$dimension->domain_id}}" {{(!in_array($dimension->domain_id, $plan->domains_ids) ? 'style=display:none' : (in_array($dimension->id, $plan->dimensions_ids) ? 'selected="selected"' : ''))}}>{{ $dimension->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Hospital</label>
                        <select name="region_id[]" class="form-control" multiple required>
                      <?php $permissions = Auth::user()->getNom035Regions(5); ?>
                            @if (in_array(0, $permissions) || auth()->user()->role == 'admin')
                              @foreach ($regions as $key => $region)
                                <option value="{{ $key }}" {{(in_array($key, $plan->region_id) ? 'selected="selected"' : '')}}>{{ $region }}</option>
                              @endforeach
                            @else
                              @if (!empty($permissions))
                                @foreach ($permissions as $key => $permission)
                                <option value="{{ $permission }}" {{(in_array($permission, $plan->region_id) ? 'selected="selected"' : '')}}>{{ $regions[$permission] }}</option>
                                @endforeach
                              @endif
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Medida o Acción</label>
                        <textarea name="accion" class="form-control" rows="5" required>{{ $plan->accion }}</textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Dirigido a Áreas/Trabajadores</label>
                        <input type="text" name="dirigido" value="{{ $plan->dirigido }}" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Identificador de Resultados</label>
                        <input type="text" name="resultados" value="{{ $plan->resultados }}" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Fecha Implmentación</label>
                        <input type="date" name="fecha_implementar" value="{{ $plan->fecha_implementar }}" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Medio de Verificación</label>
                        <input type="text" name="medio_verificacion" value="{{ $plan->medio_verificacion }}" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Fecha Verificación</label>
                        <input type="date" name="fecha_verificacion" value="{{ $plan->fecha_verificacion }}" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Área Responsable</label>
                        <input type="text" name="area_responsable" value="{{ $plan->area_responsable }}" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Control de los Avances</label>
                        <input type="text" name="control_avances" value="{{ $plan->control_avances }}" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Evaluación Posterior Fecha y Nivel de Riesgo</label>
                        <input type="text" name="evaluacion_posterior" value="{{ $plan->evaluacion_posterior }}" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <h2>Bitácora</h2>
                  <div style="max-height: 200px; overflow-y: auto">
                    <table width="100%" border="1" class="table table-bordered table-hover table-striped notas">
                      <thead style="background-color: rgb(122, 117, 181); color: white;">
                        <tr>
                          <th>Registro</th>
                          <th>Avance</th>
                          <th>Documento</th>
                          <th>Creado Por</th>
                        </tr>
                      </thead>
                      <tbody>

                  @if (!empty($plan->bitacora))

                    @for ($i = 0;$i < count($plan->bitacora);$i++) 

                        <tr>
                          <td>{{$plan->bitacora[$i]->created_at}}</td>
                          <td>{{$plan->bitacora[$i]->avances}}</td>
                    
                      @if (!empty($plan->bitacora[$i]->evidencia))
                            
                          <td>
                            <a class="btn btn-custom-guia" href="/documents/nom035/plan_accion/{{$plan->bitacora[$i]->id_plan}}/{{$plan->bitacora[$i]->id}}/{{$plan->bitacora[$i]->evidencia}}" style="background-color: #ebc200" download>
                              <span class="icon-span-filestyle fas fa-file-pdf"></span>
                            </a>
                          </td>
                      @else
                              
                          <td></td>
                      @endif
                            
                          <td>{{$plan->bitacora[$i]->created_by_user->fullname}}</td>
                        </tr>
                    @endfor
                  @endif

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>  
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Nuevo Avance</label>
                  <textarea name="avances" class="form-control" rows="3"></textarea>
                </div>
              </div>
            </div>
            <div class="text-right">
              <div class="row">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                  <input class="filestyle" data-buttontext="Explorar" data-badge="false" style="display: none;" data-buttonname="btn-custom-" name="file" type="file" id="file" accept="application/pdf">
                  <div class="bootstrap-filestyle input-group">
                    <input type="text" class="form-control " placeholder="Documento PDF" disabled="">
                    <span class="group-span-filestyle input-group-btn" tabindex="0" style="background-color: rgb(122, 117, 181); font-weight: bold; color: white; height: 37px">
                      <label for="file" class="btn btn-custom-guia" style="font-weight: bold; color: white; height: 27px">
                        <span class="icon-span-filestyle fas fa-folder-open"></span> <span class="buttonText">Explorar</span>
                      </label>
                    </span>
                  </div>
                </div>
              </div>
              <input type="submit" value="Guardar" class="btn btn-primary"> <a href="/nom035/plan_accion" class="btn btn-success">Regresar</a>
            </div>
        </form>
    </div>
</div>

@endsection

@section('scripts')
<script>

  $(document).ready(function(){

    $('.notas').DataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "No hay avances",
            "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
    });

    $('.filestyle').change(function(){
      
      $('.bootstrap-filestyle input[type="text"]').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });

    $('select.categories').change(function(){

      var category_id = $(this).val();
      $('select.domains, select.dimensions').val([]);
      $('select.domains option, select.dimensions option').hide();
      
      $('select.domains option').each(function(){

        if ($(this).attr('data-category') == category_id){

          $(this).show();
          $('select.domains option[data-domain="' + $(this).attr('value') + '"]').show();
        }
      });
    });

    $('select.domains').change(function(){

      var selected = [];

      $('select.domains :selected').each(function(){

        selected.push(this.value);
      });
      
      $('select.dimensions option').each(function(){

        if (selected.includes($(this).attr('data-domain'))){

          $(this).show();
        }

        else{

          $(this).removeAttr('selected');
          $(this).hide();
        }
      });
    });
  });

</script>
@endsection