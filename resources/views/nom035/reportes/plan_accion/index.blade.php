@extends('nom035.app')

@section('title', '¿Que es?')

@section('content')

<div class="text-center">
  <img src="{{ asset('img/banner_nom035.png') }}" alt="" class="img-fluid">
</div>
<div class="card mt-4">
    <div class="card-header bg-client-primary text-white">
        <h3>PROGRAMA PARA LA ATENCIÓN DE RIESGOS PSICOSOCIALES</h3>
    </div>

    <div class="mt-4 text-center">
      Periodo
      <select class="form-group period_id">

      @foreach ($periods as $key => $period)
    
      <option value="{{$period->id}}" <?php if ($period->id == $period_id){ ?>selected="selected"<?php } ?>>{{$period->name}}</option>
      @endforeach

      </select>
    </div>

    <div class="card-body">
        <div class="text-left my-3">
            <a href="/nom035/plan_accion/create/{{$period_id}}" class="btn btn-success">Nueva medida o acción</a>
        </div>
        <div class="table-responsive">
            <table class="table" id="planAccion">
                <thead class="bg-client-primary text-white">
                    <tr>
                        <th>Categoria</th>
                        <th>Nivel de Riesgo</th>
                        <th>Medida o Acción</th>
                        <th>Dirigido a Áreas/Trabajadores</th>
                        <th>Identificador de resultados</th>
                        <th>Fecha Implementar</th>
                        <th>Medio de Verificación</th>
                        <th>Fecha Verificación</th>
                        <th>Área Responsable</th>
                        <th>Control de los Avances</th>
                        <th>Evaluación Posterior Fecha y Nivel de Riesgo</th>
                        <th>Acción</th>
                    </tr>
                </thead>
                <tbody>

                  @if (count($planes) > 0)

                    @foreach ($planes as $plan)
                        <tr>
                            <td>{{ $plan->category->name }}</td>
                      @foreach ($categories as $key => $category)
                        @if ($plan->category->id == $key)
                            <td class="{{($category->status=='Muy alto'?'rojo':($category->status=='Alto'?'naranja':($category->status=='Medio'?'amarillo':($category->status=='Bajo'?'verde':'turqueza'))))}}">{{$category->status}}</td>
                    <?php break; ?>
                        @endif
                      @endforeach
                            <td>{{ $plan->accion }}</td>
                            <td>{{ $plan->dirigido }}</td>
                            <td>{{ $plan->resultados }}</td>
                            <td>{{ $plan->fecha_implementar }}</td>
                            <td>{{ $plan->medio_verificacion }}</td>
                            <td>{{ $plan->fecha_verificacion }}</td>
                            <td>{{ $plan->area_responsable }}</td>
                            <td>{{ $plan->control_avances }}</td>
                            <td>{{ $plan->evaluacion_posterior }}</td>
                            <td>
                                <a href="{{ url('nom035/plan_accion/' . $plan->id) }}" class="btn btn-success">Editar</a>
                            </td>
                        </tr>
                    @endforeach
                  @endif
                </tbody>
            </table>
        </div>
    </div>

</div>
<form action="/nom035/plan_accion" method="post" class="change_period_id">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="period_id" class="period_id">
  <input type="submit" style="display: none">
</form>

@endsection

@section('scripts')
<script>
    $('#planAccion').DataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
    });

    // Cambia el periodo
    $('select.period_id').change(function(){

      // Recarga la pagina con el nuevo periodo
      $('form.change_period_id .period_id').val($(this).val());
      $('form.change_period_id').submit();
    });
</script>
@endsection