@extends('nom035.app')

@section('title', '¿Que es?')

@section('content')

<div class="text-center">
  <img src="{{ asset('img/banner_nom035.png') }}" alt="" class="img-fluid">
</div>
<div class="card mt-4">
    <div class="card-header bg-client-primary text-white">
        <h3>CREAR MEDIDA O ACCIÓN</h3>
    </div>
    <div class="card-body">
        @if(count($categories) > 0 && !empty($period))
            <form action="{{ url('nom035/plan_accion/create') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <input type="hidden" name="period_id" value="{{ $period->id }}">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Categoría</label>
                            <select name="category_id" class="form-control categories" required>
                                @foreach ($categories as $key => $category)
                                    <option value="{{ $key }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Dominio</label>
                            <select name="domains_ids[]" class="form-control domains" multiple>
                                @foreach ($domains as $key => $domain)
                                    <option value="{{ $domain->id }}" data-category="{{$domain->category_id}}" {{($domain->category_id != 1 ? 'style=display:none' : '')}}>{{ $domain->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Dimension</label>
                            <select name="dimensions_ids[]" class="form-control dimensions" multiple>
                                @foreach ($dimensions as $key => $dimension)
                                    <option value="{{ $dimension->id }}" data-domain="{{$dimension->domain_id}}" style="display: none;">{{ $dimension->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Hospital</label>
                            <select name="region_id[]" class="form-control" multiple required>
                          <?php $permissions = Auth::user()->getNom035Regions(5); ?>
                                @if (in_array(0, $permissions) || auth()->user()->role == 'admin')
                                  @foreach ($regions as $key => $region)
                                    <option value="{{ $key }}">{{ $region }}</option>
                                  @endforeach
                                @else
                                  @foreach ($permissions as $key => $permission)
                                    <option value="{{ $permission }}">{{ $regions[$permission] }}</option>
                                  @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Medida o Acción</label>
                            <textarea name="accion" class="form-control" rows="5" required></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Dirigido a Áreas/Trabajadores</label>
                            <input type="text" name="dirigido" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Identificador de Resultados</label>
                            <input type="text" name="resultados" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Fecha Implmentación</label>
                            <input type="date" name="fecha_implementar" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Medio de Verificación</label>
                            <input type="text" name="medio_verificacion" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Fercha Verificación</label>
                            <input type="date" name="fecha_verificacion" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Área Responsable</label>
                            <input type="text" name="area_responsable" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Control de los Avances</label>
                            <input type="text" name="control_avances" class="form-control" required>
                        </div>
                    </div>
                    <!--<div class="col-md-12">
                        <div class="form-group">
                            <label>Evaluación Posterior Fecha y Nivel de Riesgo</label>
                            <input type="text" class="form-control" disabled>
                        </div>
                    </div>-->
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <h2>Bitácora</h2>

                    </div>
                    <div class="form-group">
                      <label>Avance</label>
                      <textarea name="avances" class="form-control" rows="3"></textarea>
                    </div>
                  </div>
                </div>
                <div class="text-right">
                  <div class="row">
                    <div class="col-md-8"></div>
                    <div class="col-md-4">
                      <input class="filestyle" data-buttontext="Explorar" data-badge="false" style="display: none;" data-buttonname="btn-custom-" name="file" type="file" id="file" accept="application/pdf">
                      <div class="bootstrap-filestyle input-group">
                        <input type="text" class="form-control " placeholder="Documento PDF" disabled="">
                        <span class="group-span-filestyle input-group-btn" tabindex="0" style="background-color: rgb(122, 117, 181); font-weight: bold; color: white; height: 37px">
                          <label for="file" class="btn btn-custom-guia" style="font-weight: bold; color: white; height: 27px">
                            <span class="icon-span-filestyle fas fa-folder-open"></span> <span class="buttonText">Explorar</span>
                          </label>
                        </span>
                      </div>
                    </div>
                  </div>
                  <input type="submit" value="Guardar" class="btn btn-primary"> <a href="/nom035/plan_accion" class="btn btn-success">Regresar</a>
                </div>
            </form>
        @else

          @if (empty($period))

            <div class="alert alert-warning" role="alert">
              <h4 class="alert-heading">No hay un periodo abierto</h4>
              <p></p>
              <p class="mb-0">
                  Para poder crear Planes de Acción es necesario que exista un periodo abierto
              </p>
            </div>
          @else

            <div class="alert alert-warning" role="alert">
              <h4 class="alert-heading">Upss! Parece que falta información</h4>
              <p></p>
              <p class="mb-0">
                  Para poder crear Planes de Acción es necesario que el personal empiece a contestar la Evaluación para el Cuestionario 2 y/o Cuestionario 3
              </p>
            </div>
          @endif
        @endif
    </div>
</div>
@endsection

@section('scripts')
<script>

  $(document).ready(function(){

    $('.filestyle').change(function(){
      
      $('.bootstrap-filestyle input[type="text"]').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });

    $('select.categories').change(function(){

      var category_id = $(this).val();
      $('select.domains, select.dimensions').val([]);
      $('select.domains option, select.dimensions option').hide();
      
      $('select.domains option').each(function(){

        if ($(this).attr('data-category') == category_id){

          $(this).show();
          $('select.domains option[data-domain="' + $(this).attr('value') + '"]').show();
        }
      });
    });

    $('select.domains').change(function(){

      var selected = [];

      $('select.domains :selected').each(function(){

        selected.push(this.value);
      });
      
      $('select.dimensions option').each(function(){

        if (selected.includes($(this).attr('data-domain'))){

          $(this).show();
        }

        else{

          $(this).removeAttr('selected');
          $(this).hide();
        }
      });
    });
  });

</script>
@endsection