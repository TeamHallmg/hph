@extends('nom035.app')

@section('content')

<div class="text-center">
  <img src="{{ asset('img/banner_nom035.png') }}" alt="" class="img-fluid">
</div>
<div class="card mt-4">
    <div class="card-header bg-transparent">
      <div class="row">
        <div class="col-md-6">
          <h3 class="text-client-primary">PLAN DE TRABAJO</h3>
        </div>

  @if (count($avances) > 0 && empty($closed_plan))

        <div class="col-md-6">
          <h3 class="text-client-primary">
            <form action="{{ url('nom035/plan-individual-close') }}" method="POST">
              @csrf
              <input type="hidden" name="period_id" value="{{$period->id}}">
              <input type="hidden" name="user_id" value="{{$user->id}}">
              <label>Fecha de Cierre</label>
              <input type="date" class="form-control" name="fecha_cierre" style="width: auto; display: inline-block;" value="{{ date('Y-m-d') }}">
              <button type="submit" class="btn btn-success">Cerrar Plan</button>
            </form>
          </h3>
        </div>
  @else
    @if (!empty($closed_plan))

        <div class="col-md-6 text-right">
          <h3 class="text-client-primary">
            <label>Fecha de Cierre: {{$closed_plan->fecha_cierre}}</label>
            
          </h3>
        </div>
    @endif
  @endif

      </div>  
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Nombre</label>
                    <input type="text" class="form-control" value="{{ $user->FullName }}" disabled>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Departamento</label>
                    <input type="text" class="form-control" value="{{ (isset($user->employee_wt->jobPosition->area->department) ? $user->employee_wt->jobPosition->area->department->name : '') }}" disabled>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Región</label>
                    <input type="text" class="form-control" value="{{ (isset($user->employee_wt->region) ? $user->employee_wt->region->name : '') }}" disabled>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Área</label>
                    <input type="text" class="form-control" value="{{ (isset($user->employee_wt->jobPosition->area) ? $user->employee_wt->jobPosition->area->name : '') }}" disabled>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Puesto</label>
                    <input type="text" class="form-control" value="{{ (!empty($user->employee_wt->jobPosition) ? $user->employee_wt->jobPosition->name : '') }}" disabled>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Centro de trabajo</label>
                    <input type="text" class="form-control" value="{{ $user->employee_wt->sucursal }}" disabled>
                </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Empresa</label>
                    <input type="text" class="form-control" value="{{ (isset($user->employee_wt->enterprise) ? $user->employee_wt->enterprise->name : '') }}" disabled>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Fecha</label>
                    <input type="text" class="form-control" value="{{ $user->employee_wt->fecha }}" disabled>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label>Resultado</label>
                    <input type="text" class="form-control" value="Requiere valoración clínica" disabled>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                  <label>Control de los Avances</label>
                  <div style="max-height: 200px; overflow-y: auto">
                    <table width="100%" border="1" class="table table-bordered table-hover table-striped notas">
                        <thead class="bg-client-primary text-white">
                            <tr>
                              <th>Registro</th>
                              <th>Avance</th>
                              <th>Documento</th>
                              <th>Creado Por</th>
                            </tr>
                        </thead>
                        <tbody>

  @for ($i = 0;$i < count($avances); $i++) 

                          <tr>
                            <td>{{$avances[$i]->created_at}}</td>
                            <td>{{$avances[$i]->avances}}</td>
                            @if (!empty($avances[$i]->avances_posterior))
                            <td>
                              <a class="btn btn-custom-guia" href="/documents/nom035/{{$avances[$i]->id}}/{{$avances[$i]->avances_posterior}}" style="background-color: #ebc200" download>
                                <span class="icon-span-filestyle fas fa-file-pdf"></span>
                              </a>
                            </td>
                            @else
                              <td></td>
                            @endif
                            <td>{{$avances[$i]->created_by_user->fullname}}</td>
                          </tr>
  @endfor

                        </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>  
        <form action="{{ url('nom035/plan_individual/' . $period->id . '/' . $user->id ) }}" method="POST" enctype="multipart/form-data">
            @csrf
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Nuevo Avance</label>
                  <textarea name="avances" class="form-control" rows="3"></textarea>
              </div>
            </div>
          </div>
            <!--<div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Evaluación Posterior Fecha y Avances</label>
                        <textarea name="" class="form-control" rows="3" disabled></textarea>
                    </div>
                </div>
            </div>-->
            <div class="text-right">
              <div class="row">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                  <input class="filestyle" data-buttontext="Explorar" data-badge="false" style="display: none;" data-buttonname="btn-custom-" name="file" type="file" id="file" accept="application/pdf">
                  <div class="bootstrap-filestyle input-group">
                    <input type="text" class="form-control " placeholder="Documento PDF" disabled="">
                    <span class="group-span-filestyle input-group-btn" tabindex="0" style="background-color: rgb(122, 117, 181); font-weight: bold; color: white; height: 37px">
                      <label for="file" class="btn btn-custom-guia" style="font-weight: bold; color: white; height: 27px">
                        <span class="icon-span-filestyle fas fa-folder-open"></span> <span class="buttonText">Explorar</span>
                      </label>
                    </span>
                  </div>
                </div>
              </div>
              <input type="submit" value="Guardar" class="btn btn-success"> <a href="/nom035/plan_individual" class="btn btn-success">Regresar</a>
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')

<script type="text/javascript">

    $(document).ready(function(){
    
    $('.filestyle').change(function(){
      
      $('.bootstrap-filestyle input[type="text"]').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });
  });
</script>
@endsection