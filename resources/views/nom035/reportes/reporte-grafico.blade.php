@extends('nom035.app')

@section('title', 'Reportes')

@section('content')
<div class="row">
	<div class="col-md-2 text-right">
		@include('nom035/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/clima-organizacional/banner.png') }} " alt="">
		<h3 class="titulos-evaluaciones mt-4 font-weight-bold">Reportes</h3>
		<hr>
		<div class="text-center">
			Orden: <input type="radio" name="sort" class="sort" value="1" style="margin-left: 20px"> Evaluación <input type="radio" name="sort" class="sort" value="2" style="margin-left: 20px" checked="checked"> Filtro
		</div>
		<div class="text-center">
  		Filtro: <input type="radio" name="filter" class="filter" value="factores" checked="checked" style="margin-left: 20px"> Factores <!--<input type="radio" name="filter" class="filter" value="departamento" style="margin-left: 20px"> Departamento <input type="radio" name="filter" class="filter" value="puesto" style="margin-left: 20px"> Puesto--> <!--<input type="radio" name="filter" class="filter" value="edad" style="margin-left: 20px"> Edad--> <input type="radio" name="filter" class="filter" value="antiguedad" style="margin-left: 20px"> Antigüedad
		</div>
    <div class="row">
      <div class="text-center grupos_areas col-md-6" style="display: inline">
      Grupos de Áreas: <select class="grupos_areas">
                      <option value="0">-- Seleccione Grupo de Área --</option>

              <?php foreach ($grupo_areas as $key => $value){ ?>
              
                      <option value="{{$value->id}}">{{$value->nombre}}</option>
              <?php } ?>                  

                     </select>
      </div>
      <div class="text-center niveles_puestos col-md-6" style="display: inline">
      Niveles de Puesto: <select class="niveles_puestos">
                      <option value="0" class="first_option">-- Seleccione Nivel de Puesto --</option>

              <?php foreach ($niveles_puestos as $key => $value){ ?>
              
                      <option value="{{$value->id}}">{{$value->nombre}}</option>
              <?php } ?>                  

                     </select>
      </div>
    </div>
    <div class="row">
      <div class="text-center departamentos col-md-6" style="display: inline">
      Departamentos: <select class="departamentos">
                      <option value="0">-- Seleccione Departamento --</option>

              <?php foreach ($departamentos as $key => $value){ ?>
              
                      <option value="{{$value->departamento}}" data-grupo="{{(!empty($value->id) ? $value->id : '')}}">{{$value->departamento}}</option>
              <?php } ?>                  

                     </select>
      </div>
      <div class="text-center puestos col-md-6" style="display: inline">
      Puestos: <select class="puestos">
                      <option value="0" class="first_option">-- Seleccione Puesto --</option>

              <?php $actual_puesto = '';

                    foreach ($puestos as $key => $value){

                      if ($value->puesto != $actual_puesto){

                        $actual_puesto = $value->puesto; ?>
              
                      <option value="{{$value->puesto}}" data-depa="{{$value->departamento}}" data-nivel="{{(!empty($value->id) ? $value->id : '')}}">{{$value->puesto}}</option>
                <?php }

                      else{ ?>

                      <option value="{{$value->puesto}}" data-depa="{{$value->departamento}}" data-nivel="{{(!empty($value->id) ? $value->id : '')}}" style="display: none">{{$value->puesto}}</option>
                <?php }
                    } ?>                  

                </select>
      </div>
    </div>
		<div id="container" style="width: 1000px; height: 750px; margin: 0 auto"></div>
    <div class="exportar_resultados">
      <a href="javascript:;" class="btn btn-primary">Exportar Preguntas Abiertas</a>
    </div>
    <div class="factores_table" style="overflow: auto">
      <table class="table table table-striped table-bordered preguntas">
        <thead>
          <tr>
            <th>Pregunta</th>
            <th>5</th>
            <th>4</th>
            <th>3</th>
            <th>2</th>
            <th>1</th>
          </tr>
        </thead>
        <tbody>

        </tbody>
      </table>
    </div>
		<!--<div id="sliders">
			<table>
   			<tr>
   				<td>Girar en el Eje X</td>
   				<td>
   					<input id="R0" type="range" min="0" max="45" value="0"/> <span id="R0-value" class="value"></span>
   				</td>
   			</tr>
   			<tr>
   				<td>Girar en el Eje Y</td>
   				<td>
   					<input id="R1" type="range" min="0" max="45" value="0"/> <span id="R1-value" class="value"></span>
   				</td>
   			</tr>
			</table>
		</div>-->
	</div>
</div>
@endsection
@section('scripts')

<script type="text/javascript">

	var respuestas = <?php echo json_encode($resultados)?>;
  var resultados_antiguedad = <?php echo json_encode($resultados_antiguedad)?>;
  var puestos = <?php echo json_encode($puestos)?>;
  var departamentos = <?php echo json_encode($departamentos)?>;
  var id_periodo = <?php echo $id_periodo?>;
	/*var factores = <?php //echo json_encode($factores)?>;
	var respuestas = <?php //echo json_encode($valores)?>;
	var factores_ordenados = <?php //echo json_encode($factores_ordenados)?>;
	var respuestas_ordenadas = <?php //echo json_encode($valores_ordenados)?>;*/
	var highchart = 0;
	var series = 0;
	var xAxis = 0;
  var yAxis = 0;
  var title = 0;
  var plotOptions = 0;
	var actual_orden = 2;
  var actual_filtro = 'factores';
  var departamento = 0;
  var puesto = 0;
  var grupo_area = 0;
  var nivel_puesto = 0;
  var chart = 0;
  var table = 0;

	$(document).ready(function(){
   
   	chart = {
      renderTo: 'container',
      backgroundColor: 'rgb(248, 250, 252)',
      type: 'column',
      margin: 170,
      marginTop: 20,
      marginLeft: 50,
      marginRight: 50,
      options3d: {
        enabled: true,
        alpha: 0,
        beta: 0,
        depth: 50,
        viewDistance: 25
      }
   	};

   	title = {
      text: ''   
   	};
   
   	plotOptions = {
      column: {
        depth: 25,
        zones: [
        {
          value: 3,
          color: 'rgb(255,116,116)'
        },
        {
          value: 4,
          color: 'rgb(253,236,109)'
        },
        {
          color: 'rgb(144,237,125)'
        }
       	]
      },
      showInLegend: true
   	};

    yAxis = [{
    labels: {
                style: {
                    fontSize:'20px'
                }
            }
  }];

    mostrar_grafica();

   	// Activate the sliders
   	$('#R0').on('change', function(){

      highchart.options.chart.options3d.alpha = this.value;
      showValues();
      highchart.redraw(false);
   	});

   	$('#R1').on('change', function(){

      highchart.options.chart.options3d.beta = this.value;
      showValues();
      highchart.redraw(false);
   	});
		
	$('input[type="radio"].sort').click(function(){
		
		actual_orden = $(this).val();
    mostrar_grafica();
  });
		
	$('input[type="radio"].filter').click(function(){
    
      actual_filtro = $(this).val();

      if (actual_filtro == 'factores'){

        $('div.departamentos, div.puestos, div.grupos_areas, div.niveles_puestos').show();
        $('select.departamentos, select.puestos, select.grupos_areas, select.niveles_puestos').val(0);
        //$('select.puestos option').hide();

        var actual_puesto = '';

        $('select.puestos option').each(function(){

          if ($(this).val() != actual_puesto){

            $(this).show();
            actual_puesto = $(this).val();
          }
        });

        departamento = puesto = 0;
      }

      else{

        $('div.departamentos, div.puestos, div.grupos_areas, div.niveles_puestos').hide();
      }

      mostrar_grafica();
      
    });

    $('select.grupos_areas').change(function(){

      $('select.puestos, select.departamentos, select.niveles_puestos').val(0);
      puesto = departamento = nivel_puesto = 0;
      grupo_area = $(this).val();
      $('select.puestos option, select.departamentos option').hide();
      $('select.departamentos option:first').show();

      if (grupo_area != 0){

        $('select.departamentos option[data-grupo="' + grupo_area + '"]').show();
      }

      else{

        $('select.departamentos option').show();

        var actual_puesto = '';

        $('select.puestos option').each(function(){

          if ($(this).val() != actual_puesto){

            $(this).show();
            actual_puesto = $(this).val();
          }
        });
      }

      mostrar_grafica();
    });

    $('select.niveles_puestos').change(function(){

      $('select.puestos, select.departamentos, select.grupos_areas').val(0);
      puesto = departamento = grupo_area = 0;
      nivel_puesto = $(this).val();
      $('select.puestos option, select.departamentos option').hide();
      $('select.puestos option:first, select.departamentos option:first').show();

      if (nivel_puesto != 0){

        //$('select.puestos option[data-nivel="' + nivel_puesto + '"]').show();

        var actual_puesto = '';

        $('select.puestos option[data-nivel="' + nivel_puesto + '"]').each(function(){

          if ($(this).val() != actual_puesto){

            $(this).show();
            actual_puesto = $(this).val();
            $('select.departamentos option[value="' + $(this).attr('data-depa') + '"]').show();
          }
        });
      }

      else{

        $('select.departamentos option').show();

        var actual_puesto = '';

        $('select.puestos option').each(function(){

          if ($(this).val() != actual_puesto){

            $(this).show();
            actual_puesto = $(this).val();
          }
        });
      }

      mostrar_grafica();
    });

    $('select.departamentos').change(function(){

      $('select.puestos').val(0);
      $('select.puestos option').hide();
      puesto = 0;
      departamento = $(this).val();
      $('select.puestos option').hide();
      $('select.puestos option:first').show();

      if (departamento != 0){

        $('select.puestos option[data-depa="' + departamento + '"]').show();
      }

      else{

        var actual_puesto = '';

        $('select.puestos option').each(function(){

          if ($(this).val() != actual_puesto){

            $(this).show();
            actual_puesto = $(this).val();
          }
        });
      }

      mostrar_grafica();
    });

    $('select.puestos').change(function(){

      puesto = $(this).val();
      //departamento = 0;
      mostrar_grafica();
    });

    table = $('.preguntas').DataTable({
      language: {
        "sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
            },
             "aaSorting": []
    });
  });

  function mostrar_grafica(){

    var factores = [];
    var valores = [];
    var factores_ordenados = [];
    var valores_ordenados = [];

    if (actual_filtro == 'factores'){

      var factor_actual = '';
      var pregunta_actual = 0;
      var pregunta = '';
      var contador_preguntas = 0;
      var total_5 = 0;
      var total_4 = 0;
      var total_3 = 0;
      var total_2 = 0;
      var total_1 = 0;
      var valor = 0;
      var total = 0;
      var contador_respuestas = 0;
      var temp_factores = [];
      var temp_valores = [];
      var band = false;
      $('div.factores_table table tbody').html('');

      for (var i = 0;i < respuestas.length;i++){

        if (pregunta_actual != respuestas[i].id_pregunta){

          if (pregunta_actual != 0){

            total_5 = (total_5 * 100 / contador_preguntas).toFixed(1) + '%';
            total_4 = (total_4 * 100 / contador_preguntas).toFixed(1) + '%';
            total_3 = (total_3 * 100 / contador_preguntas).toFixed(1) + '%';
            total_2 = (total_2 * 100 / contador_preguntas).toFixed(1) + '%';
            total_1 = (total_1 * 100 / contador_preguntas).toFixed(1) + '%';
            $('div.factores_table table tbody').append('<tr><td>' + pregunta + '</td><td>' + total_5 + '</td><td>' + total_4 + '</td><td>' + total_3 + '</td><td>' + total_2 + '</td><td>' + total_1 + '</td></tr>');
          }

          pregunta_actual = respuestas[i].id_pregunta;
          total_5 = total_4 = total_3 = total_2 = total_1 = 0;
          contador_preguntas = 0;
          pregunta = respuestas[i].pregunta;
        }

        if (factor_actual != respuestas[i].nombre){

          if (factor_actual != ''){

            if (contador_respuestas > 0){

              valor = (total / contador_respuestas).toFixed(1) * 1;
            }

            else{

              valor = 0;
            }

            band = false;
            temp_factores = [];
            temp_valores = [];

            for (var j = 0;j < valores_ordenados.length;j++){
                
              if (valores_ordenados[j] < valor && !band){

                temp_valores.push(valor);
                temp_factores.push(factor_actual);
                band = true;
              }

              temp_valores.push(valores_ordenados[j]);
              temp_factores.push(factores_ordenados[j]);
            }

            if (!band){

              temp_valores.push(valor);
              temp_factores.push(factor_actual);
            }

            factores_ordenados = temp_factores;
            valores_ordenados = temp_valores;
            factores.push(factor_actual);
            valores.push(valor);
          }

          total = 0;
          contador_respuestas = 0;
          factor_actual = respuestas[i].nombre;
          valores_tabla = '';
        }

        if ((grupo_area == 0 || respuestas[i].grupo_area == grupo_area) && (nivel_puesto == 0 || respuestas[i].nivel_puesto == nivel_puesto) && (departamento == 0 || respuestas[i].departamento == departamento) && (puesto == 0 || respuestas[i].puesto == puesto)){

          switch (respuestas[i].respuesta){

            case 'Totalmente de acuerdo':

              if (respuestas[i].positiva == 1){
            
                total += 5;
                total_5++;
              }

              else{

                total += 1;
                total_1++;
              }

              break;

            case 'De acuerdo':

              if (respuestas[i].positiva == 1){
            
                total += 4;
                total_4++;
              }

              else{

                total += 2;
                total_2++;
              }

              break;

            case 'Mas o menos':
              total += 3;
              total_3++;
              break;

            case 'Desacuerdo':

              if (respuestas[i].positiva == 1){
            
                total += 2;
                total_2++;
              }

              else{

                total += 4;
                total_4++;
              }

              break;

            case 'Totalmente desacuerdo':
            
              if (respuestas[i].positiva == 1){
            
                total += 1;
                total_1++;
              }

              else{

                total += 5;
                total_5++;
              }
          }

          contador_respuestas++;
          contador_preguntas++;
        }
      }

      if (pregunta_actual != 0){

        total_5 = (total_5 * 100 / contador_preguntas).toFixed(1) + '%';
        total_4 = (total_4 * 100 / contador_preguntas).toFixed(1) + '%';
        total_3 = (total_3 * 100 / contador_preguntas).toFixed(1) + '%';
        total_2 = (total_2 * 100 / contador_preguntas).toFixed(1) + '%';
        total_1 = (total_1 * 100 / contador_preguntas).toFixed(1) + '%';
        $('div.factores_table table tbody').append('<tr><td>' + pregunta + '</td><td>' + total_5 + '</td><td>' + total_4 + '</td><td>' + total_3 + '</td><td>' + total_2 + '</td><td>' + total_1 + '</td></tr>');
      }

      if (factor_actual != ''){

        if (contador_respuestas > 0){

          valor = (total / contador_respuestas).toFixed(1) * 1;
        }

        else{

          valor = 0;
        }

        band = false;
        temp_factores = [];
        temp_valores = [];

        for (var j = 0;j < valores_ordenados.length;j++){
                
          if (valores_ordenados[j] < valor && !band){

            temp_valores.push(valor);
            temp_factores.push(factor_actual);
            band = true;
          }

          temp_valores.push(valores_ordenados[j]);
          temp_factores.push(factores_ordenados[j]);
        }

        if (!band){

          temp_valores.push(valor);
          temp_factores.push(factor_actual);
        }

        factores_ordenados = temp_factores;
        valores_ordenados = temp_valores;
        factores.push(factor_actual);
        valores.push(valor);
      }

      $('div.factores_table, div.exportar_resultados').show();

      $('div.exportar_resultados a').attr('href', '/clima-organizacional/exportar-resultados/' + id_periodo + '/' + grupo_area + '/' + nivel_puesto + '/' + departamento + '/' + puesto);

      /*if (departamento == 0 && puesto == 0){

        $('div.exportar_resultados a').attr('href', '/clima-organizacional/exportar-resultados/' + id_periodo + '/0/0');
      }

      else{

        if (departamento != 0 && puesto != 0){

          $('div.exportar_resultados a').attr('href', '/clima-organizacional/exportar-resultados/' + id_periodo + '/' + departamento + '/' + puesto);
        }

        else{

          if (departamento != 0){

            $('div.exportar_resultados a').attr('href', '/clima-organizacional/exportar-resultados/' + id_periodo + '/' + departamento + '/0');
          }

          else{

            $('div.exportar_resultados a').attr('href', '/clima-organizacional/exportar-resultados/' + id_periodo + '/0/' + puesto);
          }
        }
      }*/
    }

    else{

      factores_ordenados = resultados_antiguedad.filtros_ordenados;
      factores = resultados_antiguedad.filtros;
      valores_ordenados = resultados_antiguedad.valores_ordenados;
      valores = resultados_antiguedad.valores;
      $('div.factores_table, div.exportar_resultados').hide();
    }

      var datos = 0;
      var datos2 = 0;

      if (actual_orden == 2){

        datos = valores;
        datos2 = factores;
      }

    else{

      datos = valores_ordenados;
      datos2 = factores_ordenados;
    }

    series = [{
      data: datos,
      colorByPoint: true,
    }];

    xAxis = [{
      categories: datos2,
      labels: {
            style: {
              fontSize:'12px'
            }
      }
    }];

    var json = {};
    json.chart = chart;
    json.title = title;
    json.series = series;
    json.plotOptions = plotOptions;
    json.xAxis = xAxis;
    json.yAxis = yAxis;
    highchart = new Highcharts.Chart(json);

    if (table != 0){

      //table.reload();

      setTimeout(function(){
      
        $('ul.pagination li.paginate_button.active').next().click();
        $('ul.pagination li.paginate_button:first').next().click();
      }, 1000);
    }
  }
  
  function showValues(){

    $('#R0-value').html(highchart.options.chart.options3d.alpha);
    $('#R1-value').html(highchart.options.chart.options3d.beta);
  }
</script>
@endsection