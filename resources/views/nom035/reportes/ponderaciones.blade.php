@extends('layouts.app')

@section('title', 'Ponderaciones')

@section('content')
@if (!empty($message_flash))
<div class="margin-top-20">
  <div class="alert alert-info">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		Las ponderaciones fueron guardadas
  </div>
</div>
@endif
<div class="row">
	<div class="col-md-2 text-right">
		@include('evaluacion-desempeno/partials/sub-menu')
	</div>
	<div class="col-md-10">
		<img class="img-fluid" src="{{ asset('img/evaluacion_desempeno.png') }} " alt="">
		<h3 class="titulos-evaluaciones mt-5 font-weight-bold">Ponderaciones</h3>
		<hr class="mb-5">


@if (!empty($periodos))

<div class="row mb-4">
	<div class="col-md-12">
		<div>Periodo: <select class="periodos">

			    <?php foreach ($periodos as $key => $periodo){ ?>
			                                      	
			              <option value="<?php echo $periodo->id?>" <?php if (!empty($_POST['id_periodo']) && $_POST['id_periodo'] == $periodo->id){ ?>selected="selected"<?php } ?>><?php echo $periodo->descripcion?></option>
			    <?php } ?>
								 																 
								 	</select>
		</div>
	</div>
</div>

<div class="row margin-top-20">
	<form method="post" action="/ponderaciones">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="id_periodo" value="{{(!empty($_POST['id_periodo']) ? $_POST['id_periodo'] : $periodos[0]->id)}}">
		<div class="col-md-12">
			<table width="100%" border="1" class="table tabla_ponderaciones">
				<thead style="background-color: #222B64; color:white;">
					<tr>
						<td></td>

		<?php foreach ($niveles_puestos as $key => $nivel_puesto){ ?>

						<td style="text-align: center">
							<strong class="text-center">{{$nivel_puesto->nombre}}</strong>
						</td>
		<?php } ?>
					</tr>
				</thead>
				<tbody>

	<?php if (!empty($ponderaciones)){ ?>
						
					<tr>
	  				<td>
	  					<strong>Jefe</strong>
	  				</td>

	  <?php for ($i = 0;$i < count($niveles_puestos);$i++){ ?>

	  				<td class="text-center">
	  					<input type="text" value="{{$ponderaciones[$i]->jefe}}" required name="ponderacion[jefe][]">
	  				</td>
	  <?php } ?>

	  			</tr>
					<tr>
  					<td>
  						<strong>Par</strong>
  					</td>

  	<?php for ($i = 0;$i < count($niveles_puestos);$i++){ ?>

	  				<td class="text-center">
	  					<input type="text" value="{{$ponderaciones[$i]->par}}" required name="ponderacion[par][]">
	  				</td>
	  <?php } ?>

	  			</tr>
	  			<tr>
  					<td>
  						<strong>Colaborador</strong>
  					</td>

  	<?php for ($i = 0;$i < count($niveles_puestos);$i++){ ?>

	  				<td class="text-center">
	  					<input type="text" value="{{$ponderaciones[$i]->colaborador}}" required name="ponderacion[colaborador][]">
	  				</td>
	  <?php } ?>

	  			</tr>
	  			<tr>
  					<td>
  						<strong>Autoevaluación</strong>
  					</td>

  	<?php for ($i = 0;$i < count($niveles_puestos);$i++){ ?>

	  				<td class="text-center">
	  					<input type="text" value="{{$ponderaciones[$i]->autoevaluacion}}" required name="ponderacion[autoevaluacion][]">
	  				</td>
	  <?php } ?>

	  			</tr>
	<?php }

	  		else{ ?>

	  			<tr>
	  				<td>
	  					<strong>Jefe</strong>
	  				</td>

	  <?php for ($i = 0;$i < count($niveles_puestos);$i++){ ?>

	  				<td class="text-center">
	  					<input type="text" value="100" required name="ponderacion[jefe][]">
	  				</td>
	  <?php } ?>

	  			</tr>
					<tr>
  					<td>
  						<strong>Par</strong>
  					</td>

  	<?php for ($i = 0;$i < count($niveles_puestos);$i++){ ?>

	  				<td class="text-center">
	  					<input type="text" value="0" required name="ponderacion[par][]">
	  				</td>
	  <?php } ?>

	  			</tr>
	  			<tr>
  					<td>
  						<strong>Colaborador</strong>
  					</td>

  	<?php for ($i = 0;$i < count($niveles_puestos);$i++){ ?>

	  				<td class="text-center">
	  					<input type="text" value="0" required name="ponderacion[colaborador][]">
	  				</td>
	  <?php } ?>

	  			</tr>
	  			<tr>
  					<td>
  						<strong>Autoevaluación</strong>
  					</td>

  	<?php for ($i = 0;$i < count($niveles_puestos);$i++){ ?>

	  				<td class="text-center">
	  					<input type="text" value="0" required name="ponderacion[autoevaluacion][]">
	  				</td>
	  <?php } ?>

	  			</tr>
	 <?php } ?>

	  			<tr>
  					<td></td>

  	<?php for ($i = 0;$i < count($niveles_puestos);$i++){ ?>

	  				<td class="text-center">
	  					<input type="hidden" value="{{$niveles_puestos[$i]->id}}" name="ponderacion[id_nivel_puesto][]">
	  				</td>
	  <?php } ?>

	  			</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-12 text-center">
			<button class="btn btn-primary" type="submit"><span class="fas fa-check-circle"></span> Guardar</button>
		</div>
	</form>
</div>
<form action="/ponderaciones" method="post" class="ponderaciones">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="hidden" name="id_periodo" class="id_periodo_ponderaciones">
	<input type="submit" style="display: none">
</form>

@else

<div class="row">
	<div class="col-md-12 text-center">
		<h4 class="margin-top-20">No hay periodos.</h4>
	</div>
</div>

@endif
	</div>
</div>
@endsection

@section('scripts')
	<script>
		
		$(document).ready(function(){

			// El dropdown con los periodos cambia
			$('select.periodos').change(function(){

				// Se agrega el id del periodo seleccionado al formulario oculto
				$('.id_periodo_ponderaciones').val($(this).val());

				// Se envia el formulario oculto
				$('form.ponderaciones').submit();
			});
		});
	</script>
@endsection