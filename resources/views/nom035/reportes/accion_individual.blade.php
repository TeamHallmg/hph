@extends('nom035.app')

@section('content')

<div class="text-center">
    <img src="{{ asset('img/banner_nom035.png') }}" alt="" class="img-fluid">
  </div>
<div class="card mt-4">
    <div class="card-header bg-transparent">
        <h3 class="text-client-primary">TRABAJADORES EXPUESTOS A ACONTECIMIENTOS TRAUMÁTICOS</h3>
    </div>

  @if (count($periods) > 1)

    <div class="mt-4 text-center">
      Periodo
      <select class="form-group period_id">

    @foreach ($periods as $key => $period)
    
      <option value="{{$period->id}}" <?php if ($period->id == $period_id){ ?>selected="selected"<?php } ?>>{{$period->name}}</option>
    @endforeach

      </select>
    </div>
  @endif

    <div class="card-body">
        <div class="table-responsive">
            <table class="table" id="planIndividual">
                <thead class="bg-client-primary text-white">
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Departamento</th>
                        <th>Área</th>
                        <th>Puesto</th>
                        <th>Centro de trabajo</th>
                        <th>Región</th>
                        <th>Empresa</th>
                        <th>Fecha</th>
                        <th>Resultado</th>
                        <th>Último Avance</th>
                        <th>Fecha de Cierre</th>
                        <th>Otros Periodos</th>
                        <!--<th>Evaluación Posterior Fecha y Avances</th>-->
                        <th>Avances</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td>{{ $user->employee_wt->id }}</td>
                            <td>{{ $user->employee_wt->FullName }}</td>
                            <td>{{ (isset($user->employee_wt->jobPosition->area->department) ? $user->employee_wt->jobPosition->area->department->name : '') }}</td>
                            <td>{{ (isset($user->employee_wt->jobPosition->area) ? $user->employee_wt->jobPosition->area->name : '') }}</td>
                            <td>{{ (!empty($user->employee_wt->jobPosition) ? $user->employee_wt->jobPosition->name : '') }}</td>
                            <td>{{ $user->employee_wt->sucursal }}</td>
                            <td>{{ (isset($user->employee_wt->region) ? $user->employee_wt->region->name : '') }}</td>
                            <td>{{ (isset($user->employee_wt->enterprise) ? $user->employee_wt->enterprise->name : '') }}</td>
                            <td>{{ $user->employee_wt->fecha_respuesta }}</td>
                      @if (!empty($user->employee_wt->fecha_respuesta))
                            <td class="bg-danger">Requiere valoración clínica</td>
                            <td>
                                @if(!empty($user->planIndividual))
                                    {{ $user->planIndividual->avances }}
                                @endif
                            </td>
                      @else
                            <td></td>
                            <td></td>
                      @endif
                            
                            <td>{{ (!empty($user->fecha_cierre) ? $user->fecha_cierre : '') }}</td>
                            <td>
                      @if (!empty($user->other_periods))
                        @foreach ($user->other_periods as $other_period)
                          @if ($other_period != $period_id)
                            @foreach ($periods as $period)
                              @if ($period->id == $other_period)
                                <a href="javascript:;" class="other_periods" data-period="{{$other_period}}">{{$period->name}}</a><br>
                              @endif
                            @endforeach
                          @endif
                        @endforeach
                      @endif
                            </td>
                            <td>
                      @if (!empty($user->employee_wt->fecha_respuesta))
                                    <a href="{{ url('nom035/plan_individual/'. $period_id .'/'. $user->id.'/create') }}" class="btn btn-success">Avances</a>
                      @endif
                                
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
  
</div>
<form action="/nom035/plan_individual" method="post" class="change_period_id">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="period_id" class="period_id">
  <input type="submit" style="display: none">
</form>

@endsection

@section('scripts')
<script>

    var period_id = <?php echo $period_id?>;

    $('#planIndividual').DataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "No hay personal que requiera valoración",
            "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
    });

    // Cambia el periodo
    $('select.period_id').change(function(){

      // Recarga la pagina con el nuevo periodo
      $('form.change_period_id .period_id').val($(this).val());
      $('form.change_period_id').submit();
    });

    // Click en otro periodo del usuario
    $('a.other_periods').click(function(){

      // Recarga la pagina con el nuevo periodo
      $('form.change_period_id .period_id').val($(this).attr('data-period'));
      $('form.change_period_id').submit();
    });

    if ($('table#planIndividual tbody td.bg-danger').length > 0){

      $('#planIndividual_wrapper > div:nth-child(1) > div:nth-child(1)').removeClass('col-md-6');
      $('#planIndividual_wrapper > div:nth-child(1) > div:nth-child(1)').addClass('col-md-4');
      $('#planIndividual_wrapper > div:nth-child(1) > div:nth-child(1)').next().removeClass('col-md-6');
      $('#planIndividual_wrapper > div:nth-child(1) > div:nth-child(1)').next().addClass('col-md-4');
      $('#planIndividual_wrapper > div:nth-child(1) > div:nth-child(1)').after('<div class="col-md-4"><a href="/nom035/exportar-trabajadores-con-traumas/' + period_id + '" class="btn btn-success">Exportar a Excel</a></div>');
    }

</script>
@endsection