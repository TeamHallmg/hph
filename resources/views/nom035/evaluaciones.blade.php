@extends('nom035.app')

@section('title', 'Cuestionario')

@section('content')

<div class="text-center">
	@include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['banner'], 'section' => 'Nom035']) 
	{{-- <img src="{{ asset('img/banner_nom035.png') }}" alt="" class="img-fluid"> --}}
</div>
<h3 class="titulos-evaluaciones mt-4 font-weight-bold text-client-primary">Cuestionario</h3>

	@if (count($periodosAbiertos) > 1)

<div class="mt-4 text-center">
  Periodo
  <select class="form-group period_id">

    @foreach ($periodosAbiertos as $key => $value)
    
    <option value="{{$value->id}}" <?php if ($id_periodo == $value->id){ ?>selected="selected"<?php } ?>>{{$value->name}}</option>
     @endforeach

  </select>
</div>
  @endif
<div class="card">
	{{-- <div class="mx-3">
		<ul class="nav nav-pills mt-3" id="pills-tab" role="tablist">
			<li class="nav-item">
				<a class="btn-tab active" id="intrucciones-tab" data-toggle="pill" href="#intrucciones" role="tab" aria-controls="intrucciones" aria-selected="true">Instrucciones</a>
			</li>
		</ul>
	</div>
	<hr> --}}
	<div class="card-body">
		{{-- @include('announcement.announcement_types.displays.display_announcement', ['announcements' => $display_announcements['objetivo'], 'section' => 'nom035'])  --}}
		<div class="tab-content" id="myTabContent">
			<div class="tab-pane fade show active" id="intrucciones" role="tabpanel" aria-labelledby="intrucciones-tab">
				<h3>Objetivo:</h3>
				<p>Obtener información, acerca de la forma en que se percibe la operación de la organización, para con base en ella implementar planes de acción, que ayuden incrementar la calidad de vida en el trabajo, la efectividad en la búsqueda de la calidad, la productividad y el logro de objetivos.</p>
				<div>Nuestra organización busca mantener la cercanía con sus colaboradores, por lo que,</div>
				<div>Queremos conocer tu opinión respecto a las condiciones en que desempeñas tus labores en el día a día, por esta razón agradecemos sinceramente inviertas unos minutos para responder este cuestionario.</div>
				<h3>Instrucciones:</h3>
				<p>
					<div>El cuestionario está organizado en varias secciones que podrás valorar a través de cinco escalas que se muestran en cada pregunta y son:</div>
				</p>
				<ul style="list-style-type: circle">
					<li>
						<strong>Siempre</strong>
					</li>
					<li>
						<strong>Casi siempre</strong>
					</li>
					<li>
						<strong>Algunas veces</strong>
					</li>
					<li>
						<strong>Casi nunca</strong>
					</li>
					<li>
						<strong>Nunca</strong>
					</li>
				</ul>
				<p>Al finalizar el cuestionario deberás presionar el botón enviar para que tu opinión sea tomada en cuenta.</p>
				<p>
					<strong>Gracias por tu confianza y el tiempo dedicado.</strong>
				</p>
	@if ($evaluados->isEmpty())
				<h4 class="text-center">No cuenta con cuestionarios</h4>
	@else
		@if (empty($politicas))
				<h4 class="text-center">No ha aceptado las Políticas. Favor de ir al <a href="politicas">Enlace</a> y aceptar</h4>
		@else
				<!--<p class="text-center">-->
				<div class="row">
<?php $counter = 0; ?>
			
					@if(count($periodoAbierto->users) > 0)
						@foreach($periodoAbierto->users as $user)
							@if ($user->id == auth()->user()->id)
					<?php $status = $user->pivot->status . ''; ?>
								@foreach($periodoAbierto->getTypeOfQuestions() as $key => $typeOfQuestion)

								  @if (count($periodoAbierto->getTypeOfQuestions()) == 1)

								<div class="col-md-12 text-center">
									@else

										@if ($key == 0)

								<div class="col-md-6 text-right">
										@else

								<div class="col-md-6">
										@endif
									@endif

								<!--if($period->users[0]->pivot->status == 1)-->
					<form action="/nom035/evaluacion/{{$typeOfQuestion}}" method="post">
  					<input type="hidden" name="_token" value="{{ csrf_token() }}">
  					<input type="hidden" name="period_id" value="{{$id_periodo}}">
									@if($status[$counter] == 1)
						<button type="submit" class="btn btn-danger">Entrar al {{$typeOfQuestion}} (Sin Iniciar)</button>
									@elseif($status[$counter] == 2)
						<button type="submit" class="btn btn-warning">Entrar al {{$typeOfQuestion}} (Iniciado)</button>
									@elseif($status[$counter] == 3)
						<button type="submit" class="btn btn-success">Entrar al {{$typeOfQuestion}} (Terminado)</button>
									@endif
						<?php $counter++; ?>
					</form>
								</div>
								@endforeach
							@endif
						@endforeach
					@endif
		</div>
				<!--</p>-->
		@endif
	@endif
			</div>
		</div>
	</div>
</div>
<form action="/nom035/evaluaciones" method="post" class="change_period_id">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="id_periodo" class="period_id">
  <input type="submit" style="display: none">
</form>
@endsection
@section('scripts')
<script type="text/javascript">

$(document).ready(function(){
	
	// Cambia el periodo
  $('select.period_id').change(function(){

    // Recarga la pagina con el nuevo periodo
    $('form.change_period_id .period_id').val($(this).val());
    $('form.change_period_id').submit();
  });
});

</script>
@endsection