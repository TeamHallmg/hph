@extends('nom035.app')

@section('title', 'Encuesta')

@section('content')
<style type="text/css">
  div#evaluados_wrapper {position: relative;padding-top: 40px}
  div.dt-buttons {top: 0;position: absolute;}
  div.dataTables_length, div.dataTables_info {float: left;}
  div#evaluados_filter, div.dataTables_paginate {float: right;}  
</style>
<div class="text-center">
    <img src="{{ asset('img/banner_nom035.png') }}" alt="" class="img-fluid">
</div>
<h3 class="titulos-evaluaciones mt-4 font-weight-bold">Avances</h3>

  @if (count($periodoStatus) > 1)

<div class="mt-4 text-center">
  Periodo
  <select class="form-group period_id">

    @foreach ($periodoStatus as $key => $value)
    
    <option value="{{$value->id}}" <?php if ($period_id == $value->id){ ?>selected="selected"<?php } ?>>{{$value->name}}</option>
      @endforeach

  </select>
</div>
  @endif

@if ($canEdit)

@foreach ($cuestionarios as $key => $value)

<div class="row">
    <div class="col-md-12 text-center">
      <h3>{{$value}}</h3>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="info-box bg-danger">
            <span class="info-box-icon">
                <i class="fas fa-arrow-alt-circle-down"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">No iniciadas</span>
                <span class="info-box-number">{{ $no_iniciados[$key] . '/' . $total_evals }}</span>
                <div class="progress">
                    <div class="progress-bar" style="width: {{ number_format($no_iniciados[$key] * 100 / $total_evals, 1, '.', ',') . '%' }}"></div>
                </div>
                <span class="progress-description">
                    {{ number_format($no_iniciados[$key] * 100 / $total_evals, 1, '.', ',') . '%' }}
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="info-box bg-warning">
            <span class="info-box-icon">
                <i class="fas fa-arrow-alt-circle-up"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Inconclusas</span>
                <span class="info-box-number">{{ $iniciados[$key] . '/' . $total_evals }}</span>
                <div class="progress">
                    <div class="progress-bar" style="width: {{ number_format($iniciados[$key] * 100 / $total_evals, 1, '.', ',') . '%' }}"></div>
                </div>
                <span class="progress-description">
                    {{ number_format($iniciados[$key] * 100 / $total_evals, 1, '.', ',') . '%' }}
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="info-box bg-success">
            <span class="info-box-icon">
                <i class="fas fa-check-circle"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Terminadas</span>
                <span class="info-box-number">{{ $terminados[$key] . '/' . $total_evals }}</span>
                <div class="progress">
                    <div class="progress-bar" style="width: {{ number_format($terminados[$key] * 100 / $total_evals, 1, '.', ',') . '%' }}"></div>
                </div>
                <span class="progress-description">
                    {{ number_format($terminados[$key] * 100 / $total_evals, 1, '.', ',') . '%' }}
                </span>
            </div>
        </div>
    </div>
</div>
@endforeach
@endif

@if ($canEdit)
<div class="table-responsive my-4">
    <table class="table table-striped" id="evaluados">
        <thead>
            <tr>
                <th>ID</th>
                <th>Rfc</th>
                <th>Evaluado</th>
                <th>Correo</th>
                <th>Departamento</th>
                <th>Área</th>
                <th>Puesto</th>
                <th>Centro de trabajo</th>
                <th>Empresa</th>

        @foreach ($cuestionarios as $key => $value)

                <th>{{$value}}</th>
        @endforeach

            </tr>
        </thead>
        <tbody>
            @if (!empty($evaluados))							
                @foreach ($evaluados as $eval)
                    <tr>
                        <td class="text-center">
                            {{ $eval->employee_wt->idempleado }}
                        </td>
                        <td>
                            {{ $eval->employee_wt->rfc }}
                        </td>
                        <td>
                            {{ $eval->FullName }}
                        </td>
                        <td>
                            {{ $eval->email }}
                        </td>
                        <td>
                            {{ optional($eval->employee_wt->jobPosition->area->department)->name }}
                        </td>
                        <td>
                            {{ optional($eval->employee_wt->jobPosition->area)->name }}
                        </td>
                        <td>
                            {{ optional($eval->employee_wt->jobPosition)->name }}
                        </td>
                        <td>
                            {{ $eval->employee_wt->sucursal }}
                        </td>
                        <td>
                            {{ (!empty($eval->employee_wt->enterprise) ? $eval->employee_wt->enterprise->name : '') }}
                        </td>

                      @if (count($cuestionarios) == 1)

                        <td class="text-center">

                        @if($eval->pivot->status == 1)
                          <button class="btn btn-danger btn-block btn-static">No iniciada</button>
                        @elseif($eval->pivot->status == 2)
                          <button class="btn btn-warning btn-block btn-static">Inconclusa</button>
                        @else
                          <button class="btn btn-success btn-block btn-static">Terminada</button>
                        @endif
                        </td>
                      @else
                        @foreach ($cuestionarios as $key => $value)
                                
                        <td class="text-center">

                          @if ($key == 0)
                            @if($eval->pivot->status < 21)
                          <button class="btn btn-danger btn-block btn-static">No iniciada</button>
                            @elseif($eval->pivot->status > 20 && $eval->pivot->status < 31)
                          <button class="btn btn-warning btn-block btn-static">Inconclusa</button>
                            @else
                          <button class="btn btn-success btn-block btn-static">Terminada</button>
                            @endif
                          @else
                            @if($eval->pivot->status % 10 == 1)
                          <button class="btn btn-danger btn-block btn-static">No iniciada</button>
                            @elseif($eval->pivot->status % 10 == 2)
                          <button class="btn btn-warning btn-block btn-static">Inconclusa</button>
                            @else
                          <button class="btn btn-success btn-block btn-static">Terminada</button>
                            @endif
                          @endif
                        </td>
                        @endforeach
                      @endif
                    
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="5" class="text-center">No hay evaluados.</td>
                </tr>
            @endif						
        </tbody>
    </table>
</div>
@else

  <div class="text-center">
    <h2>Ya no cuentas con los permisos para ver esta información</h2>
  </div>
@endif

<form action="/nom035/avances" method="post" class="change_period_id">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="period_id" class="period_id">
  <input type="submit" style="display: none">
</form>

@endsection
@section('scripts')
<script type="text/javascript">

var no_iniciados = {{ json_encode($no_iniciados) }};
var iniciados = {{ json_encode($iniciados) }};
var terminados = {{ json_encode($terminados) }};
var cuestionarios = <?php echo json_encode($cuestionarios)?>;

$(document).ready(function(){
	$('#evaluados').DataTable({
    dom: 'Blfrtip',
		language: {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		},
		buttons: [
			{
				extend: 'excel',
				text: 'Exportar a Excel',
				titleAttr: 'Exportar a Excel',
				title: 'Avance Encuesta Grupo Guia',
			}
		]
    });

    // Cambia el periodo
    $('select.period_id').change(function(){

      // Recarga la pagina con el nuevo periodo
      $('form.change_period_id .period_id').val($(this).val());
      $('form.change_period_id').submit();
    });
});
</script>
@endsection