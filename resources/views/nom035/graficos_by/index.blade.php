@extends('nom035.app')

@section('content')
<div class="row">

    <div class="col-md-2 text-right">
        @include('nom035/partials/sub-menu')
    </div>
        
    
    <div class="col-md-10">
        <div class="text-center">
            <img src="{{ asset('img/banner_nom035.png') }}" class="img-fluid" alt="">
        </div>
    </div>
</div>


@if(isset($sin_periodos))

  @if (count($periodos) > 0)

    <div>
      <div class="col-md-6">
        <form action="/nom035/reporte_nom" method="post" class="change_period_id">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label for="period_id" style="margin-bottom: 0" class="mt-3">Periodo</label>
                <select class="form-control" id="period_id" name="period_id">
                    @foreach($periodos as $periodo)
                        <option value="{{ $periodo->id }}" {{ $periodo->id==$period_id?'selected':'' }} >{{ $periodo->name }} ({{ $periodo->status }})</option>
                    @endforeach
                </select>
            </div>
            <input type="submit" style="display: none">
        </form>
      </div>
    </div>
  @endif

<div class="card mt-3">
    <div class="card-header bg-client-primary text-white p-3">
        <h4>Reporte General</h4>
    </div>
    
    @if (count($periodos) == 0)      
        <div class="card-body">
        <div>
            <h4>No hay Periodos disponibles para graficar</h4>
            </div>
        </div>
    @else
        
        <div class="card-body">
            <div>
                <h4>No hay Resultados disponibles para graficar</h4>
            </div>
        </div>
        
    @endif
  
</div>


@else


{{-- <div class="card mt-3">
  
    <div class="card-header bg-client-secondary text-white p-3">
        <h4>Filtros</h4>
    </div>
    
    @include('nom035.graficos_by.filtros')
         
</div> --}}


<div id="sin_resultados">
  
</div>  

@if (auth()->user()->role == 'admin' || auth()->user()->hasNom035Permissions(3))
       
<div class="card mt-3">

    <div class="card-header bg-client-primary text-white p-3">
        <h4>Filtros</h4>
    </div>
    
    @include('nom035.graficos_by.filtros-admin')
        
</div>

@endif


<div class="card mt-3">
  
    <div class="card-header bg-client-primary text-white p-3">
        <h4>REPORTE GENERAL</h4>
    </div>
      


      <div class="card-body">      
         <div class="row py-3">
             
            <div class="col-6 h3">
                <div id="nivel_critico"></div>
           </div>   
        
        </div>


         <div class="row text-center py-3">
              
            @if (auth()->user()->role != 'admin' && !auth()->user()->hasNom035Permissions(3))
                <div class="col-4">
            
                    <div class="form-group" style="display: flex;align-items: flex-end;">
                        <label for="" class="font-weight-bolder"> Periodos</label> 
                        <select class="ml-3 form-control period_id" id="select_periodo" name="periodo">
                            @foreach($periodos as $periodo)
                            <option value="{{ $periodo->id }}" {{ $periodo->id==$period_id?'selected':'' }} >{{ $periodo->name }} ({{ $periodo->status }})</option>
                        @endforeach
                        </select>
                    </div>    
                    
                    <button class="btn btn-warning _change btn-sm float-right mr-3    " > Ejecutar Consulta </button>
 
                </div>
            @endif
         </div>
<nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    {{-- <a class="nav-item nav-link active" id="nav-general-tab" data-toggle="tab" href="#nav-general" role="tab" aria-controls="nav-general" aria-selected="true">Reporte General</a> --}}
    <a class="nav-item nav-link active" id="nav-categorias-tab" data-toggle="tab" href="#nav-categorias" role="tab" aria-controls="nav-categorias" aria-selected="false">Por Categoría</a>
    <a class="nav-item nav-link" id="nav-dominios-tab" data-toggle="tab" href="#nav-dominios" role="tab" aria-controls="nav-dominios" aria-selected="false">Por Dominio</a>
    <a class="nav-item nav-link" id="nav-dimensiones-tab" data-toggle="tab" href="#nav-dimensiones" role="tab" aria-controls="nav-dimensiones" aria-selected="false">Por Dimensiones</a>
  </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active p-3" id="nav-categorias" role="tabpanel" aria-labelledby="nav-categorias-tab">
     
        <div class="row text-center">
            <div class="content_loading col text-center">
                @include('nom035.graficos_by.loading')
            </div> 
          </div>
        @include('nom035.graficos_by.reporte_nom_categorias')
    </div>
    <div class="tab-pane fade p-3" id="nav-dominios" role="tabpanel" aria-labelledby="nav-dominios-tab">
       
        <div class="row text-center">
            <div class="content_loading col text-center">
                @include('nom035.graficos_by.loading')
            </div> 
          </div>
        @include('nom035.graficos_by.reporte_nom_dominios')
    </div>
    <div class="tab-pane fade p-3" id="nav-dimensiones" role="tabpanel" aria-labelledby="nav-dimensiones-tab">
       
        <div class="row text-center">
            <div class="content_loading col text-center">
                @include('nom035.graficos_by.loading')
            </div> 
          </div>
        @include('nom035.graficos_by.reporte_nom_dimensiones')
    </div>
</div>
</div>
</div>


{{--   <div class="row">
     <div class="col-12">
         <a href="{{ url('/nom35/reporte_nom/resultado_general') }}" class="btn btn-info ">
             Reporte General
         </a>
         <a href="{{ url('/nom35/reporte_nom/categorias') }}" class="btn btn-primary">
             Reporte Por Categorias
         </a>
         <a href="{{ url('/nom35/reporte_nom/dominios') }}" class="btn btn-primary">
             Reporte por Dominios
         </a>
         <a href="{{ url('/nom35/reporte_nom/dimensiones') }}" class="btn btn-primary">
             Reporte por Dimensiones
         </a>
     </div>
 </div>
 --}}


@endif



<div class="card mt-3">
    <div class="card-header bg-client-primary text-white p-3">
        <h4>Criterios para la toma de acciones</h4>
    </div>
    
        
    <div class="card-body">
       <div class="row">
          @include('nom035.graficos_by.criterios')
        </div>
    </div>
</div>

@endsection


@section('scripts')

<style>

    .content_loading{
        display: none;
    }
    
    .col-1-5{
    width: 20%;
    float: left;
    position: relative;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
  }
    .nav-item.nav-link{
        color: #fff;
        font-weight: bolder;
        background: #545b5f !important;
    }

    .bg-client-secondary{
        color: #fff;
        font-weight: bolder;
        background: #545b5f !important;
    }
    .bg-client-primary{
        color: #fff;
        font-weight: bolder;
        background: #406bb2 !important;
    }

    .nav-item.nav-link.active{
        color: #fff;
        font-weight: bolder;
        background: #406bb2 !important;
    }

    .text-client-primary{
        color: #406bb2;
        font-weight: bolder;
        /* background: #406bb2 !important; */
    }
    .highcharts-credits{
        display: none !important;
    }

    @keyframes spinner-border {
            to { transform: rotate(360deg); }
          } 
          .spinner-border{
              display: inline-block;
              width: 2rem;
              height: 2rem;
              vertical-align: text-bottom;
              border: .25em solid currentColor;
              border-right-color: transparent;
              border-radius: 50%;
              -webkit-animation: spinner-border .75s linear infinite;
              animation: spinner-border .75s linear infinite;width: 1rem;
          }
          .spinner-border-sm{
              height: 1rem;
              border-width: .2em;
          }
          .content-wrapper, .right-side {
    min-height: 100%;
    
    z-index: 800;
}
</style>

<script>



$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

    $('.report_general').DataTable({
      'order': [[1,'desc']]
   });

   $('#report_categorias').DataTable();
   $('#report_general').DataTable();
   $('#table_empleados_categorias').DataTable();

   $('#dominios').DataTable();
   $('#table_usuarios_dominios').DataTable();


$('#Dimensiones').DataTable();

    var change_sucursal = function(sucursales) {

            var selOpts = "";
            var sucursales = sucursales;
            $('#select_sucursal').empty();
            selOpts += "<option value=''>--TODO--</option>";
            for (i=0;i<sucursales.length;i++){
              var id = sucursales[i]['id'];
              var val = sucursales[i]['nombre'];
              selOpts += "<option value='"+id+"'>"+val+"</option>";
            }
            $('#select_sucursal').append(selOpts);

    };

    var change_areas = function(directions) {

             var selOpts = "";
            var directions = directions;
            $('#select_direccion').empty();
            selOpts += "<option value=''>--TODO--</option>";
            for (i=0;i<directions.length;i++){
              var id = directions[i]['id'];
              var val = directions[i]['nombre'];
              selOpts += "<option value='"+id+"'>"+val+"</option>";
            }
            $('#select_direccion').append(selOpts);

    };

    var change_puestos = function(jobs) {
          
            var selOpts = "";
            var puestos = jobs;
            $('#select_job').empty();
            selOpts += "<option value=''>--TODO--</option>";
            for (i=0;i<puestos.length;i++){
              var id = puestos[i]['id'];
              var val = puestos[i]['name'];
              selOpts += "<option value='"+id+"'>"+val+"</option>";
            }
            $('#select_job').append(selOpts);



    };

    var change_edades = function(ages) {
          
            var selOpts = "";
            var edades = ages;
            $('#select_edad').empty();
            selOpts += "<option value=''>--TODO--</option>";
            for (i=0;i<edades.length;i++){
              var id = edades[i]['id'];
              var val = edades[i]['nombre'];
              selOpts += "<option value='"+id+"'>"+val+"</option>";
            }
            $('#select_edad').append(selOpts);
    };

    var change_inicios = function(starts) {
          
            var selOpts = "";
            var inicios = starts;
            $('#select_inicio').empty();
            selOpts += "<option value=''>--TODO--</option>";
            for (i=0;i<inicios.length;i++){
              var id = inicios[i]['id'];
              var val = inicios[i]['nombre'];
              selOpts += "<option value='"+id+"'>"+val+"</option>";
            }
            $('#select_inicio').append(selOpts);
    };

    var change_sexos = function(sexs) {
          
            var selOpts = "";
            var sexos = sexs;
            $('#select_sexo').empty();
            selOpts += "<option value=''>--TODO--</option>";
            for (i=0;i<sexos.length;i++){
              var id = sexos[i]['id'];
              var val = sexos[i]['nombre'];
              selOpts += "<option value='"+id+"'>"+val+"</option>";
            }
            $('#select_sexo').append(selOpts);
    };

    var change_centros_trabajo = function(sucursals) {
          
            var selOpts = "";
            var centros_trabajo = sucursals;
            $('#select_centro_trabajo').empty();
            selOpts += "<option value=''>--TODO--</option>";
            for (i=0;i<centros_trabajo.length;i++){
              var id = centros_trabajo[i]['id'];
              var val = centros_trabajo[i]['nombre'];
              selOpts += "<option value='"+id+"'>"+val+"</option>";
            }
            $('#select_centro_trabajo').append(selOpts);
    };

    var change_regiones = function(datas) {
          
            var selOpts = "";
            var datas = datas;
            $('#select_regiones').empty();
            selOpts += "<option value=''>--TODO--</option>";
            for (i=0;i<datas.length;i++){
              var id = datas[i]['id'];
              var val = datas[i]['nombre'];
              selOpts += "<option value='"+id+"'>"+val+"</option>";
            }
            $('#select_regiones').append(selOpts);
    };

    var change_turnos = function(datas) {
          
            var selOpts = "";
            var datas = datas;
            $('#selects_turno').empty();
            selOpts += "<option value=''>--TODO--</option>";
            for (i=0;i<datas.length;i++){
              var id = datas[i]['id'];
              var val = datas[i]['nombre'];
              selOpts += "<option value='"+id+"'>"+val+"</option>";
            }
            $('#selects_turno').append(selOpts);
    };

    // var change_empleados = function(datas) {
          
    //         var selOpts = "";
    //         var datas = datas;
    //         $('#selects_empleados').empty();
    //         selOpts += "<option value=''>--TODO--</option>";
    //         for (i=0;i<datas.length;i++){
    //           var id = datas[i]['id'];
    //           var val = datas[i]['nombre'];
    //           selOpts += "<option value='"+id+"'>"+val+"</option>";
    //         }
    //         $('#selects_empleados').append(selOpts);
    // };


    var change_filtro = function(reporte, cambio) {

    var sel_sucursal = $('#select_sucursal').val();
    var sel_direccion = $('#select_direccion').val();
    var sel_job = $('#select_job').val();
 

axios.post('{{ url("nom035/reporte_norma_grafica") }}', {
    empleado_id: $('#select_empleado').val(),
    periodo_id: $('#select_periodo').val(),
    sucursal: $('#select_sucursal').val(),
    direccion: $('#select_direccion').val(),
    turno: $('#selects_turno').val(),
    job: $('#select_job').val(),
    periodo: $('#select_periodo').val(),
    sexo: $('#select_sexo').val(),
    edad: $('#select_edad').val(),
    ingreso: $('#select_inicio').val(),
    centro_trabajo: $('#select_centro_trabajo').val(),
    regiones: $('#select_regiones').val(),
    cambio: cambio,
})
.then(function (response){           
  
    console.log(response);
 
    if($('#select_regiones').val()==''){
      change_regiones(response.data.regiones,response.data.id_centros_trabajo);
    }

    if($('#select_sucursal').val()==''){
      change_sucursal(response.data.sucursales,response.data.id_sucursal);
    }

    if($('#select_direccion').val()==''){
      change_areas(response.data.directions,response.data.id_direction);
    }

    if($('#select_job').val()==''){
      change_puestos(response.data.jobs,response.data.id_job);
    }

    if($('#select_sexo').val()==''){
      change_sexos(response.data.sexs,response.data.id_sex);
    }

    if($('#select_edad').val()==''){
      change_edades(response.data.ages,response.data.id_age);
    }

    if($('#select_inicio').val()==''){
      change_inicios(response.data.starts,response.data.id_start);
    }

    if($('#select_centro_trabajo').val()==''){
      change_centros_trabajo(response.data.centros_trabajo,response.data.id_centros_trabajo);
    }

    if($('#selects_turno').val()==''){
      change_turnos(response.data.turnos,response.data.id_centros_trabajo);
    }

    // if($('#select_empleado').val()==''){
    //   change_empleado(response.data.empleados,response.data.empleado_id);
    // }


   if(response.data.sin_data){

    html =  '<div class="margin-top-20">'+
            '<div class="alert alert-info">'+
            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
            'El filtro aplicado no contiene tiene resultados'+
            '</div>'+
            '</div>';

      $('#sin_resultados').html(html);


    html =  '<div class="info-box mb-3">'+
            '<span class="info-box-icon"><i class="fa fa-bar-chart"></i></span>'+
            '<div class="info-box-content">'+
            '<span class="info-box-text">Para la calificación final:</span>'+
            '<span class="info-box-number">0</span>'+
            '</div>'+
            '</div>';
      $('#DataResult').html(html);

    }else{

      $('#sin_resultados').html('');

    }


    $('.content_loading').hide();
var nivel = '';
    if(response.data.promedio_general==0){
        nivel = 'Nulo';        
        }else if(response.data.promedio_general==1){
            nivel ='Bajo';}
            else if(response.data.promedio_general==2){
                nivel ='Medio';}else if(response.data.promedio_general==3){
                    nivel ='Alto';}
                    else{
                        nivel = 'Muy Alto';}

    html =  '<div id="nivel_critico"><div class="info-box-content">'+
        '<span class="info-box-text">Nivel General de Riesgo:</span>'+
        '<span class="info-box-number"  style="padding: 10px;border-radius: 10px;background:'+response.data.color_general+'">'+ nivel+'</span>'+
        '</div>'+
        '</div>';

      $('#nivel_critico').html(html);

    var html = '';
            var i;
            var filas = Object.keys(response.data.tables_dim.antiquity.rows).length;
            var data = response.data.tables_dim.antiquity.rows;
            var users_count = response.data.users_count;
    
            console.log('asd',filas,data);
 
            $.each( data, function( key, value ) {

                html += '<tr>' +
                        '<td>' + value.name + '</td>' +
                        '<td class="text-right">' + value.total + '</td>' +          
                        '</tr>';

            }); 
 
            html += '<tr><td colspan="2" class="text-right font-weight-bolder"><span class="mr-5"> Total </span> ' + users_count + ' </td></tr>';

            $('.Antigüedad').html(html);


            var html = '';
            var i;
            var filas = Object.keys(response.data.tables_dim.sex.rows).length;
            var data = response.data.tables_dim.sex.rows;
    
            console.log('asd',filas,data);
 
            $.each( data, function( key, value ) {

                html += '<tr>' +
                        '<td>' + value.name + '</td>' +
                        '<td class="text-right">' + value.total + '</td>' +          
                        '</tr>';

            }); 
 
            html += '<tr><td colspan="2" class="text-right font-weight-bolder"><span class="mr-5"> Total </span> ' + users_count + ' </td></tr>';

            $('.Sexo').html(html);


            var html = '';
            var i;
            var filas = Object.keys(response.data.tables_dim.generations.rows).length;
            var data = response.data.tables_dim.generations.rows;            
 
            $.each( data, function( key, value ) {

                html += '<tr>' +
                        '<td>' + value.nac + '</td>' +
                        '<td>' + value.name + '</td>' +          
                        '<td class="text-right">' + value.total + '</td>' +          
                        '</tr>';

            }); 
 
            html += '<tr><td colspan="3" class="text-right font-weight-bolder"><span class="mr-5"> Total </span> ' + users_count + ' </td></tr>';

            $('.Generación').html(html);

            var html = '';
            var i;
            var filas = Object.keys(response.data.tables_dim.turnos.rows).length;
            var data = response.data.tables_dim.turnos.rows;            
 
            $.each( data, function( key, value ) {

                html += '<tr>' +
                        '<td>' + value.name + '</td>' +          
                        '<td class="text-right">' + value.total + '</td>' +          
                        '</tr>';

            }); 
 
            html += '<tr><td colspan="2" class="text-right font-weight-bolder"><span class="mr-5"> Total </span> ' + users_count + ' </td></tr>';

            $('.Turnos').html(html);

    var boxgraf0 = {}; // my object
    var boxesgraf0 =  [];

    
    $.each(response.data.clasificacion_categorias, function(key, value) {
        // console.log(key, value);
        boxgraf0 = { 
                stacking: 'normal', 
                name: "Nivel Crítico",
                colorByPoint: true,        
                data: [                    
                    {
                        name: value.category_name_short,
                        color:  value.color,
                        y: value.data[0]
                    }
                ]
            }
    boxesgraf0.push(boxgraf0); 

    });
    var graf0 =  {
            chart: {
                type: 'column'
            },
            title: false,
            xAxis: {
                type: 'category'
            },
            yAxis: { 
                title: false,
                softMax: 5,
                // tickInterval: 1,
                // height:300
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: false,
                        format: '{point.y:.1f}'
                    }
                }
            },  
          series:  boxesgraf0
      }

    var chart = new Highcharts.chart('containercategoria', graf0);
    

    <?php foreach($clasificacion_categorias as $key => $cat) {	?>  

    var box = {}; // my object
    var boxes =  [];

    
    $.each(response.data.clasificacion_dominios.filter( element => element.category_id ==<?php echo $cat["id"]; ?>), function(key, value) {
        // console.log(key, value);
        box = { 
                stacking: 'normal', 
                name: "Nivel Crítico",
                colorByPoint: true,        
                data: [                    
                    {
                        name: value.name_short,
                        color:  value.color,
                        y: value.data[0]
                    }
                ]
            }
    boxes.push(box); 

    });
   
    var graf1 =  {
            chart: {
                type: 'column'
            },
            title: false,
            xAxis: {
                type: 'category'
            },
            yAxis: { 
                title: false,
                softMax: 5,
                tickInterval: 1,
                height:300
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: false,
                        format: '{point.y:.1f}'
                    }
                }
            },  
          series:  boxes
      }


      

    var chart = new Highcharts.chart('containerdominios<?php echo $cat["name"]; ?>', graf1);
       
    <?php }   ?>



    var boxgraf2 = {}; // my object
    var boxesgraf2 =  [];

    
    $.each(response.data.clasificacion_dimensiones, function(key, value) {
        // console.log(key, value);
        boxgraf2 = { 
                stacking: 'normal', 
                name: "Nivel Crítico",
                colorByPoint: true,        
                data: [                    
                    {
                        name: value.category_name_short,
                        color:  value.color,
                        y: value.data[0]
                    }
                ]
            }
    boxesgraf2.push(boxgraf2); 

    });
    var graf2 =  {
            chart: {
                type: 'column'
            },
            title: false,
            xAxis: {
                type: 'category'
            },
            yAxis: { 
                title: false,
                softMax: 5,
                // tickInterval: 1,
                // height:300
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: false,
                        format: '{point.y:.1f}'
                    }
                }
            },  
          series:  boxesgraf2
      }  

    var chart = new Highcharts.chart('containerdimensiones', graf2);
    
    $(document).ready( function () {
        $('.report_general2').DataTable();
    } );



})
.catch(function (error){

});




};

























    var change_filtro_dominio = function(id) {

        $('.content_loading').show();

        axios.post('{{ url("nom035/reporte_norma_grafica_dominio") }}', {
            change_filtro_dominio: id ,
            sucursal: $('#select_sucursal').val(),
            direccion: $('#select_direccion').val(),
            job: $('#select_job').val(),
            periodo: $('#select_periodo').val(),
            sexo: $('#select_sexo').val(),
            edad: $('#select_edad').val(),
            ingreso: $('#select_inicio').val(),
            centro_trabajo: $('#select_centro_trabajo').val(),
            regiones: $('#select_regiones').val(),
        })
        .then(function (response){           
          console.log(response)

            $('.content_loading').hide();
            
            html = '<span class="text-client-primary sel_dom h4">'+ response.data.dominio_seleccionado + '  </span> ';

            $('.sel_dom').html(html);


          
            var box = {}; // my object
            var boxes =  [];

            
            $.each( response.data.clasificacion_dimensiones, function( key, value ) {

                box = { 
                        stacking: 'normal', 
                        name: "Nivel Crítico",
                        colorByPoint: true,        
                        data: [                    
                            {
                                name: value.name,
                                color:  value.color,
                                y: value.data[0]
                            }
                        ]
                    }
            boxes.push(box); 

            }); 

         

            var reporte_nom_dimensiones = {

                chart: {
                type: 'column'
            },
            title: false,
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: false

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: false,
                        format: '{point.y:.1f}'
                    }
                }
            },

                series: boxes

            }

            var chart = new Highcharts.chart('containerdimensiones', reporte_nom_dimensiones);

            var html = '';
            var i;
            var filas = Object.keys(response.data.tables_dim.antiquity.rows).length;
            var data = response.data.tables_dim.antiquity.rows;
            var users_count = response.data.users_count;
    
            console.log('asd',filas,data);
 
            $.each( data, function( key, value ) {

                html += '<tr>' +
                        '<td>' + value.name + '</td>' +
                        '<td class="text-right">' + value.total + '</td>' +          
                        '</tr>';

            }); 
 
            html += '<tr><td colspan="2" class="text-right font-weight-bolder"><span class="mr-5"> Total </span> ' + users_count + ' </td></tr>';

            $('#Antigüedad').html(html);


            var html = '';
            var i;
            var filas = Object.keys(response.data.tables_dim.sex.rows).length;
            var data = response.data.tables_dim.sex.rows;
    
            console.log('asd',filas,data);
 
            $.each( data, function( key, value ) {

                html += '<tr>' +
                        '<td>' + value.name + '</td>' +
                        '<td class="text-right">' + value.total + '</td>' +          
                        '</tr>';

            }); 
 
            html += '<tr><td colspan="2" class="text-right font-weight-bolder"><span class="mr-5"> Total </span> ' + users_count + ' </td></tr>';

            $('#Sexo').html(html);


            var html = '';
            var i;
            var filas = Object.keys(response.data.tables_dim.generations.rows).length;
            var data = response.data.tables_dim.generations.rows;            
 
            $.each( data, function( key, value ) {
                
                html += '<tr>' +
                        '<td>' + value.nac + '</td>' +
                        '<td>' + value.name + '</td>' +          
                        '<td class="text-right">' + value.total + '</td>' +          
                        '</tr>';

            }); 
 
            html += '<tr><td colspan="3" class="text-right font-weight-bolder"><span class="mr-5"> Total </span> ' + users_count + ' </td></tr>';

            $('#Generación').html(html);

            var html = '';
            var i;
            var filas = Object.keys(response.data.tables_dim.turnos.rows).length;
            var data = response.data.tables_dim.turnos.rows;            
 
            $.each( data, function( key, value ) {

                html += '<tr>' +
                        '<td>' + value.name + '</td>' +          
                        '<td class="text-right">' + value.total + '</td>' +          
                        '</tr>';

            }); 
 
            html += '<tr><td colspan="2" class="text-right font-weight-bolder"><span class="mr-5"> Total </span> ' + users_count + ' </td></tr>';

            $('#Turnos').html(html);

 
        })
        .catch(function (error){

        });

 

    };

    $('._change').on('click', function(){

    $('.content_loading').show();

// $('#content').html('<div class="loading"><img src="{{ asset('img/ajax-loader.gif') }}" alt="loading" /><br/>Generando Gráficas...</div>');

var reporte = this.value;

var cambio = this.name;

change_filtro(reporte, cambio);

});


  $('._reset').on('click', function(){

        $('.content_loading').show();
        var reporte = null;
        
        var cambio = 'periodo';

        change_filtro(reporte, cambio);

    });

  // Cambia el periodo
    $('select#period_id').change(function(){

      // Recarga la pagina con el nuevo periodo
      $('form.change_period_id').submit();
    });
  
</script>






<script>


    $(function (){
    
        var graf1 = Highcharts.chart('containercategoria', {
            
            chart: {
                type: 'column'
            },
            title: false,
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: false,

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    
                    borderWidth: 0,
                    dataLabels: {
                        enabled: false,
                        format: '{point.y:.1f}'
                    }
                }
            },
            series: [
                <?php foreach($clasificacion_categorias as $key => $value) {	?>          
                    {              
                    stacking: 'normal', 
                    name: "Nivel Crítico",
                    colorByPoint: true,

                        // name: '<?php echo $value["name"]; ?>',                
                        data: [                    
                            {
                                name: '<?php echo $value["category_name_short"]; ?>',
                                color:  '<?php echo $value["color"]; ?>',
                                y: <?php echo $value["data_y"]; ?>,
                            }
                        ]
                    },            
                <?php } ?>
            ]   
        });

    });



    $(function (){
        <?php foreach($clasificacion_categorias as $key => $cat) {	?>   
        var graf1 = Highcharts.chart('containerdominios<?php echo $cat["name"]; ?>', {
            
            chart: {
                type: 'column'
            },
            title: false,
            xAxis: {
                type: 'category'
            },
            yAxis: { 
                title: false,
                softMax: 5,
                tickInterval: 1,
                height:300
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: false,
                        format: '{point.y:.1f}'
                    }
                }
            },
            series: [
                <?php foreach($clasificacion_dominios as $key => $value) { if($value["category_id"] == $cat["id"]) {	?>          
                    {             
                    stacking: 'normal',    
                    name: "Nivel Crítico",
                    colorByPoint: true,

                        // name: '<?php echo $value["name"]; ?>',                
                        data: [                    
                            {
                                name: '<?php echo $value["name_short"]; ?>',
                                color:  '<?php echo $value["color"]; ?>',
                                y: <?php echo $value["data_y"]; ?>,
                            }
                        ]
                    },            
                <?php } } ?>
            ]   
        });
    
        <?php } ?>
    });
 
    $('._dominios').on('change', function(){
        
        change_filtro_dominio( this.value );
   
    });

 
    $(function (){
    var graf1 = Highcharts.chart('containerdimensiones', {
        
        chart: {
                type: 'column'
            },
            title: false,
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: false

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: false,
                        format: '{point.y:.1f}'
                    }
                }
            },
            series: [
                <?php foreach($clasificacion_dimensiones as $key => $value) {	?>          
                    {         
                    stacking: 'normal',        
                    name: "Nivel Crítico",
                    colorByPoint: true,

                        // name: '<?php echo $value["name"]; ?>',                
                        data: [                    
                            {
                                name: '<?php echo $value["category_name_short"]; ?>', 
                                color: '<?php echo $value["color"]; ?>', 
                                y: <?php echo $value["data_y"]; ?>,
                            }
                        ]
                    },            
                <?php } ?>
            ]   
        });

    });


</script>

@endsection|