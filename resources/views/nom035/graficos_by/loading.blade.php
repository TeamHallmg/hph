<svg class="loadingSpinner" viewBox="0 0 80 80" style="visibility: visible; opacity: 1;">
    <defs>
        <path id="a" d="M0 0h40v40H0z"></path>
    </defs>
    <g fill="none" fill-rule="evenodd">
        <circle cx="40" cy="40" r="38" stroke="#DEECF9" stroke-width="2.817"></circle>
        <g transform="translate(40)">
            <mask id="b" fill="#fff">
                <use xlink:href="#a"></use>
            </mask>
            <circle cy="40" r="38" stroke="#0078D7" stroke-width="2.817" mask="url(#b)"></circle>
        </g>
    </g>
</svg>
    <span style="display: block"> Cargando Información...</span>
<br>
<br>