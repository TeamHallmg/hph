<div class="row">

    <div class="col-md-6">

      <div class="card">

        <div class="card-body">

          <div id="DataResult">

            {{-- {{ $color_general }} --}}
            <div class="info-box mb-3 text-client-primary">
              <span class="info-box-icon"><i class="fa fa-chart-area"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Para la calificación final:</span>
                <span class="info-box-number"> {{ is_int($resultado_general) ? $resultado_general : number_format($resultado_general, 2) }} </span>
              </div>
            </div>
          </div>

        </div>

      </div>

    </div>

    {{-- CLASIFICACION POR CATEGORIAS --}}
    <div class="col-md-6">

      <div class="card">

        <div class="card-body">

          <h4 class="text-client-primary">Por Persona</h4>
          <div id="report">
          <table class="table table-striped report_general" id="report_general">
              <thead class="bg-client-primary text-white">
                  <tr>
                      <th>Nombres</th>
                      <th>Calificacion:</th>
                  </tr>
              </thead>
              <tbody>
                  @foreach ($clasificacion_general as $key => $clasificacion_categoria)
                      
                      <tr>
                          <td>{{ $clasificacion_categoria[0] }}</td>

                          <td class="text-right font-weight-bold {{ $clasificacion_categoria['color'] }}">
                               {{ is_int($clasificacion_categoria['valor']) ? $clasificacion_categoria['valor'] : number_format($clasificacion_categoria['valor'], 2) }}
                          </td>

                      </tr>

                  @endforeach
              </tbody>
            </table>

          </div>

        </div>

      </div>

    </div>

</div>

  