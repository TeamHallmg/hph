<div class="card-body">
     <div class="row">
         <div class="col-md-3">
            <div class="form-group">
                <label for="">Periodo</label>
                <select class="cambiando form-control" id="select_periodo" name="periodo">
                    @foreach($periodos as $periodo)
                        <option value="{{ $periodo->id }}" {{ $periodo->id==$period_id?'selected':'' }} >{{ $periodo->name }} ({{ $periodo->status }})</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Hospital</label>
                <select class="cambiando form-control " id="select_regiones" name="regiones">
                    <option value="">--TODO--</option>
                    @foreach($regiones as $region)
                        <option value="{{ $region['id'] }}"  {{ isset($regiones_id)?$region['id']==$regiones_id?'selected':'':'' }}>{{ $region['nombre'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Centros de Trabajos</label>
                <select class="cambiando form-control" id="select_centro_trabajo" name="centro_trabajo">
                    <option value="">--TODO--</option>
                    @foreach($centros_trabajo as $centro_trabajo)
                        <option value="{{ $centro_trabajo['id'] }}"  {{ isset($centro_trabajo_id)?$centro_trabajo['id']==$centro_trabajo_id?'selected':'':'' }}>{{ $centro_trabajo['nombre'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Areas</label>
                <select class="cambiando form-control" id="select_direccion" name="area">
                    <option value="">--TODO--</option>
                    @foreach($directions as $direction)
                        <option value="{{ $direction['id'] }}"  {{ isset($area_id)?$direction['id']==$area_id?'selected':'':'' }}>{{ $direction['nombre'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Departamento</label> 
                <select class="cambiando form-control" id="select_sucursal" name="sucursal">
                    <option value="">--TODO--</option> 
                    @foreach($sucursales as $sucursal)
                        <option value="{{ $sucursal['id'] }}"  {{ isset($sucursal_id)?$sucursal['id']==$sucursal_id?'selected':'':'' }}>{{ $sucursal['nombre'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Categoría de Puestos</label>
                <select class="cambiando form-control" id="select_job" name="puesto">
                    <option value="">--TODO--</option>
                    @foreach($jobs as $job)
                        <option value="{{ $job->id }}"  {{ isset($puesto_id)?$job->id==$puesto_id?'selected':'':'' }}>{{ $job->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Edad</label>
                <select class="cambiando form-control" id="select_edad" name="edad">
                    <option value="">--TODO--</option>
                    @foreach($ages as $age)
                        <option value="{{ $age['id'] }}"  {{ isset($edad_id)?$age['id']==$edad_id?'selected':'':'' }}>{{ $age['nombre'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Antigüedad</label>
                <select class="cambiando form-control" id="select_inicio" name="inicio">
                    <option value="">--TODO--</option>
                    @foreach($starts as $start)
                        <option value="{{ $start['id'] }}"  {{ isset($inicio_id)?$start['id']==$inicio_id?'selected':'':'' }}>{{ $start['nombre'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Sexo</label>
                <select class="cambiando form-control" id="select_sexo" name="sexo">
                    <option value="">--TODO--</option>
                    @foreach($sexs as $sex)
                        <option value="{{ $sex['id'] }}"  {{ isset($sexo_id)?$sex['id']==$sexo_id?'selected':'':'' }}>{{ $sex['nombre'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">Turnos</label>
                <select class="cambiando form-control" id="selects_turno" name="turnos">
                    <option value="">--TODO--</option>  
                    @foreach($turnos as $turno)
                        <option value="{{ $turno['id'] }}"  {{ isset($turnos_id)?$turno['id']==$turnos_id?'selected':'':'' }}>{{ $turno['name'] }}</option>
                    @endforeach                 
                </select>
            </div>
        </div>
        {{-- @if (auth()->user()->role == 'admin')
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">Empleados</label>
                    <select class="cambiando form-control" id="select_empleado" name="empleados">
                        <option value="">--TODO--</option>  
                        @foreach($empleados as $empleado)
                            <option value="{{ $empleado['id'] }}"  {{ isset($empleado_id)?$empleado['id']==$empleado_id?'selected':'':'' }}>{{ $empleado['name'] }}</option>
                        @endforeach                 
                    </select>
                </div>
            </div>
        @endif --}}
    </div>
</div>

<div class="card-footer">
    {{-- <button class="btn btn-info _reset btn-sm float-right" > Reiniciar Filtros</button> --}}
  <button class="btn btn-warning _change btn-sm float-right mr-3    " > Ejecutar Consulta </button>
</div>
