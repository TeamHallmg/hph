@extends('nom035.app')

@section('content')
<div class="row">

    <div class="col-md-2 text-right">
        @include('nom035/partials/sub-menu')
    </div>
        
    
    <div class="col-md-10">
        <div class="text-center">
            <img src="{{ asset('img/banner_nom035.png') }}" class="img-fluid" alt="">
        </div>
    </div>
</div>


@if(isset($sin_periodos))

<div class="card mt-3">
    <div class="card-header bg-client-primary text-white p-3">
        <h4>Reporte General</h4>
    </div>
    
    @if (count($periodos) == 0)      
        <div class="card-body">
        <div>
            <h4>No hay Periodos disponibles para graficar</h4>
            </div>
        </div>
    @else
        
        <div class="card-body">
            <div>
                <h4>No hay Resultados disponibles para graficar</h4>
            </div>
        </div>
        
    @endif
  
</div>


@else



@if (auth()->user()->role == 'admin' || !auth()->user()->hasNom035Permissions(8))
       
    <div class="card mt-3">

        <div class="card-header bg-client-primary text-white p-3">
            <h4>Filtros</h4>
        </div>
        
        @include('nom035.graficos_by.filtros')
            
    </div>

@endif

<div id="sin_resultados">
  
</div>  
 


<div class="card mt-3">
  
    <div class="card-header bg-client-primary text-white p-3">
        <h4>TABLERO DE CALIFICACIONES GENERALES</h4>
    </div> 
<div class="card card-body">
    
    <div class="table-responsive">
    <table class="table table-striped table-bordered">
        <thead class="bg-client-primary text-white">
            <tr>
                <th>Categoria</th>
                <th>Clasif.</th>
                <th>Riesgo</th>
                <th style="
    width: 41%;
">Dominio</th>
                <th style="
    width: 0%;
">Clasif.</th>
                <th style="
    width: 21%;
">Riesgo</th>
                <th style="
    width: 40%;
">Dimensiones</th>
                <th style="
    width: 4%;
">Clasif.</th>
                <th style="
    width: 30%;
">Riesgo</th>
            </tr>

            {{-- <tr>
                <th>Categoria</th>
                <th>Calificación</th>
                <th>Riesgo</th>
                <th>Dominio</th>
                <th>Clasif.</th>
                <th>Riesgo</th>
                <th>Dimensiones</th>
                <th>Clasif.</th>
                <th>Riesgo</th>
            </tr> --}}
        </thead>
        <tbody >

            @if ($metodo_post)
            @foreach ($clasificacion_categorias as $key => $clasificacion_categoria)
                
                <tr>
                    <td style="max-width: 180px;text-align: center; vertical-align: middle;">{!! $clasificacion_categoria['name'] !!}</td>

                    <td style="text-align: center; vertical-align: middle;" nowrap class="text-right font-weight-bold">
                       {{ is_int($clasificacion_categoria['critico']['0']) ? $clasificacion_categoria['critico']['0'] : number_format($clasificacion_categoria['critico']['0'], 2) }} 
                    </td>
                    <td style="text-align: center; vertical-align: middle;min-width: 100px; " class="h4" nowrap class="text-right font-weight-bold">
                        <span class="badge w-100 p-1" style="background: {!! $clasificacion_categoria['color'] !!}; {{($clasificacion_categoria['color']=='#FF0000')?'color:#fff':null}}" >
                        <?php $promedio_general= is_int($clasificacion_categoria['data']['0']) ? $clasificacion_categoria['data']['0'] : number_format($clasificacion_categoria['data']['0'], 2);
                            if($promedio_general==0){echo 'Nulo';}else if($promedio_general==1){echo 'Bajo';}else if($promedio_general==2){echo 'Medio';}else if($promedio_general==3){echo 'Alto';}else{echo 'Muy Alto';}
                            ?>
                        </span>

                    </td>
                    <td style="text-align: center; vertical-align: middle;" colspan="6">                                      
                        <table class="table table-striped table-bordered">                        
                            <tbody>                        
                                @foreach ($clasificacion_dominios as $key => $clasificacion_dominio)
                                    @if(isset($clasificacion_dominio['category_id']) && $clasificacion_dominio['category_id'] == $clasificacion_categoria['id'])
                                    <tr> 

                                        <td style="max-width: 180px;text-align: center; vertical-align: middle;">{!! $clasificacion_dominio['name'] !!}</td>  
                                        <td style="text-align: center; vertical-align: middle;    min-width: 60px;" nowrap class="text-right font-weight-bold">
                                           {{ is_int($clasificacion_dominio['critico']['0']) ? $clasificacion_dominio['critico']['0'] : number_format($clasificacion_dominio['critico']['0'], 2) }} 
                                        </td> 
                                        <td style="text-align: center; vertical-align: middle;    min-width: 100px;" class="h4" nowrap>
                                            <span class="badge w-100 p-1" style="background: {!! $clasificacion_dominio['color'] !!}; {{($clasificacion_dominio['color']=='#FF0000')?'color:#fff':null}}" >
                                                <?php $promedio_general= is_int($clasificacion_dominio['data']['0']) ? $clasificacion_dominio['data']['0'] : number_format($clasificacion_dominio['data']['0'], 2);
                                                if($promedio_general==0){echo 'Nulo';}else if($promedio_general==1){echo 'Bajo';}else if($promedio_general==2){echo 'Medio';}else if($promedio_general==3){echo 'Alto';}else{echo 'Muy Alto';}
                                                ?>
                                            </span>
                                        </td>

                                        <td style="text-align: center; vertical-align: middle;">
                                                              
                                            <table class="table table-striped table-bordered">                        
                                                <tbody>          
                                                        @foreach ($clasificacion_dimensiones as $key => $clasificacion_dimension)
                                                    
                                                        @if($clasificacion_dominio['id'] == $clasificacion_dimension['domain_id'])
                                                    
                                                            <tr> 
                                                                 <td style="max-width: 180px; text-align: center; vertical-align: middle;">{!! $clasificacion_dimension['name'] !!}</td>
                                                                 
                                                                <td style="text-align: center; vertical-align: middle;    min-width: 60px;" nowrap class="text-right font-weight-bold">
                                                                    {{ is_int($clasificacion_dimension['data']['0']) ? $clasificacion_dimension['data']['0'] : number_format($clasificacion_dimension['data']['0'], 2) }} 
                                                                </td>
                                                                <td style="text-align: center; vertical-align: middle;    min-width: 100px;" class="h4" nowrap>
                                                                    <span class="badge w-100 p-1" style="background: {!! $clasificacion_dominio['color'] !!}; {{($clasificacion_dominio['color']=='#FF0000')?'color:#fff':null}}" >
                                                                        <?php $promedio_general= is_int($clasificacion_dominio['data']['0']) ? $clasificacion_dominio['data']['0'] : number_format($clasificacion_dominio['data']['0'], 2);
                                                                        if($promedio_general==0){echo 'Nulo';}else if($promedio_general==1){echo 'Bajo';}else if($promedio_general==2){echo 'Medio';}else if($promedio_general==3){echo 'Alto';}else{echo 'Muy Alto';}
                                                                        ?>
                                                                    </span>
                                                                </td>

                                                            </tr>

                                                        @endif

                                                    @endforeach

                                                </tbody>
                                            </table> 
                                        </td>
                                    </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table> 

                    </td>
                    

                </tr>

            @endforeach
            @endif
        </tbody>
    </table>
</div>
</div>
</div>


<div class="card mt-3">
    <div class="card-header bg-client-secondary text-white p-3">
        <h4>Niveles de Riesgos</h4>
    </div>
    
        
    <div class="card-body">
       <div class="row">
          @include('nom035.graficos_by.criterios')
        </div>
    </div>
</div>

@endif


<form action="/nom035/tablero/calificacion" method="post" class="change_period_id">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="periodo_id" class="periodo">
  <input type="hidden" name="sucursal" class="sucursal">
  <input type="hidden" name="direccion" class="direccion">
  <input type="hidden" name="job" class="job">
  <input type="hidden" name="edad" class="edad">
  <input type="hidden" name="ingreso" class="ingreso">
  <input type="hidden" name="sexo" class="sexo">
  <input type="hidden" name="centro_trabajo" class="centro_trabajo">
  <input type="hidden" name="turno" class="turno">
  <input type="hidden" name="regiones" class="regiones">
  <input type="submit" style="display: none">
</form>


@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){  
            // Cambia el periodo
            $('._change').click(function(){
              // Recarga la pagina con el nuevo periodo
              $('form.change_period_id .periodo').val($('#select_periodo').val());
              $('form.change_period_id .sucursal').val($('#select_sucursal').val());
              $('form.change_period_id .direccion').val($('#select_direccion').val());
              $('form.change_period_id .job').val($('#select_job').val());
              $('form.change_period_id .edad').val($('#select_edad').val());
              $('form.change_period_id .ingreso').val($('#select_inicio').val());
              $('form.change_period_id .sexo').val($('#select_sexo').val());
              $('form.change_period_id .centro_trabajo').val($('#select_centro_trabajo').val());
              $('form.change_period_id .turno').val($('#selects_turno').val());
              $('form.change_period_id .regiones').val($('#select_regiones').val());
              $('form.change_period_id').submit();
            });
        });
    </script>
@endsection
