@extends('nom035.app')

@section('content')
<div class="row">

    <div class="col-md-2 text-right">
        @include('nom035/partials/sub-menu')
    </div>
        
    
    <div class="col-md-10">
        <div class="text-center">
            <img src="{{ asset('img/banner_nom035.png') }}" class="img-fluid" alt="">
        </div>
    </div>
</div>


@if(isset($sin_periodos))

  
<div class="card mt-3">
    <div class="card-header bg-client-primary text-white p-3">
        <h4>Reporte General</h4>
    </div>
    
    @if (count($periodos) == 0)      
        <div class="card-body">
        <div>
            <h4>No hay Periodos disponibles para graficar</h4>
            </div>
        </div>
    @else
        
        <div class="card-body">
            <div>
                <h4>No hay Resultados disponibles para graficar</h4>
            </div>
        </div>
        
    @endif
  
</div>


@else



@if (auth()->user()->role == 'admin' || !auth()->user()->hasNom035Permissions(9))
       
<div class="card mt-3">

    <div class="card-header bg-client-primary text-white p-3">
        <h4>Filtros</h4>
    </div>
    
    @include('nom035.graficos_by.filtros')
        
</div>

@endif


<div id="sin_resultados">
  
</div>  
 

{{-- @if (auth()->user()->role == 'admin')
       
    <div class="card mt-3">

        <div class="card-header bg-client-primary text-white p-3">
            <h4>Filtros</h4>
        </div>
        
        @include('nom035.graficos_by.filtros')
            
    </div>

@endif --}}

<div class="card mt-3">
  
    <div class="card-header bg-client-primary text-white p-3">
        <h4>Dominios</h4>
    </div> 

    <div class="card card-body">
     

        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
              <a class="nav-item nav-link active" id="nav-centro-tab" data-toggle="tab" href="#nav-centro" role="tab" aria-controls="nav-centro" aria-selected="true">Centros de Trabajos</a>
              <a class="nav-item nav-link" id="nav-hospitales-tab" data-toggle="tab" href="#nav-hospitales" role="tab" aria-controls="nav-hospitales" aria-selected="false">Hospitales</a>
            </div>
          </nav>
          <div class="tab-content" id="nav-tabContent">
              <div class="tab-pane fade show active p-3" id="nav-centro" role="tabpanel" aria-labelledby="nav-centro-tab">
               
                <div class="row pb-3">

            @if ($metodo_post)
                    @foreach ($clasificacion_dominiosxyz as $key => $values)
                    
                        <div class="col-md-4" >

                            <div class="card border-0">
                    
                                <div class="card-header bg-client-primary">
                                {!!$values['name']!!}
                                </div> 

                                <div class="card-body p-1"> 

                                    <table class="table table-striped table-bordered table-sm">
                                        <thead class="bg-client-secondary text-white    ">
                                            <tr>
                                                <th>Centro de Trabajo</th>
                                                <th>Calificaciones</th>
                                                <th>Nivel</th>
                                            </tr>
                                        </thead>
                                        <tbody> 
                                            @foreach ($values['centros'] as $keyrows => $centro)
                                            <tr>
                                                <td>
                                                    {!!$centro['name']!!} 
                                                </td> 
                                                <td class="text-right">
                                                    {{ number_format($centro['critico']['0'], 2) }} 
                                                </td> 
                                                <td>
                                                    <span class="badge w-100 p-1" style="background: {!! $centro['color'] !!}; {{($centro['color']=='#FF0000')?'color:#fff':null}}" >
                                                        <?php $promedio_general= is_int($centro['data']['0']) ? $centro['data']['0'] : number_format($centro['data']['0'], 2);
                                                        if($promedio_general==0){echo 'Nulo';}else if($promedio_general==1){echo 'Bajo';}else if($promedio_general==2){echo 'Medio';}else if($promedio_general==3){echo 'Alto';}else{echo 'Muy Alto';}
                                                        ?>
                                                    </span>
                                                </td> 
                                            </tr>
                                                
                                            @endforeach 
                                        </tbody>
                                    </table>



                                </div>

                            </div>

                        </div>
                        @endforeach
                        @endif
                </div>        

            </div>

              <div class="tab-pane fade p-3" id="nav-hospitales" role="tabpanel" aria-labelledby="nav-hospitales-tab">
                <div class="row pb-3">

            @if ($metodo_post)
                    @foreach ($clasificacion_dominiosxy as $key => $values)
                    
                        <div class="col-md-4" >

                            <div class="card border-0">
                    
                                <div class="card-header bg-client-primary">
                                {!!$values['name']!!}
                                </div> 

                                <div class="card-body p-1"> 

                                    <table class="table table-striped table-bordered table-sm">
                                        <thead class="bg-client-secondary text-white    ">
                                            <tr>
                                                <th>Hospitales</th>
                                                <th>Calificaciones</th>
                                                <th>Nivel</th>
                                            </tr>
                                        </thead>
                                        <tbody> 
                                            @foreach ($values['hospitales'] as $keyrows => $hospital)
                                            <tr>
                                                <td>
                                                    {!!$hospital['name']!!} 
                                                </td> 
                                                <td>
                                                    {{ number_format($hospital['critico']['0'], 2) }} 
                                                </td> 
                                                <td>
                                                    <span class="badge w-100 p-1" style="background: {!! $hospital['color'] !!}; {{($hospital['color']=='#FF0000')?'color:#fff':null}}" >
                                                        <?php $promedio_general= is_int($hospital['data']['0']) ? $hospital['data']['0'] : number_format($hospital['data']['0'], 2);
                                                        if($promedio_general==0){echo 'Nulo';}else if($promedio_general==1){echo 'Bajo';}else if($promedio_general==2){echo 'Medio';}else if($promedio_general==3){echo 'Alto';}else{echo 'Muy Alto';}
                                                        ?>
                                                    </span>
                                                </td> 
                                            </tr>
                                                
                                            @endforeach 
                                        </tbody>
                                    </table>



                                </div>

                            </div>

                        </div>
                        @endforeach
                        @endif
                </div>            
            </div>
          </div>


         
    </div>
</div>


<div class="row mt-5">

    <div class="col-md-12 font-weight-bolder text-client-primary h4">
        DEMOGRÁFICOS
        <HR style="background: #545b5f !important;">
    </div>
    <div class="col-md-12 p-0">

        <div class="card shadow-lg border-0">

            <div class="card-body"> 

                <div class="row pb-3">

                    @foreach ($tables_cat as $key => $values)
                    
                        <div class="col-md-6" >

                            <div class="card border-0">
                    
                                <div class="card-header bg-client-primary">
                                    Distribución por {{$values['name']}}
                                </div> 

                                <div class="card-body p-1"> 
        
                                    <div class="table-responsive">
                                    <div id="table-empleados-categorias">
                    
                                    <table class="table table-striped table-sm">
                                        <thead id="result_categoria_empleados_head" class="bg-client-secondary">
                                            <tr>
                                                @foreach ($values['headers'] as $keyheaders => $header)
                                                <th>{{$header}}</th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody id="result_categoria_empleados_body" class="{{$values['name']}}">
                                            @foreach ($values['rows'] as $keyrows => $row)
                                            <tr>
                                                <?php $colspan = 0; $colspan = count($row); ?>
                                                @foreach ($row as $keyrow => $items)
                                                    @if($keyrow!='id')
                                                        <td {{($keyrow=='total')?"class=text-right":null}}>
                                                            {{$items}} 
                                                        </td>
                                                    @endif
                                                @endforeach
                                            </tr>
                                             
                                            @endforeach
                                            <tr>
                                                <td class="text-right font-weight-bolder" colspan="{{$colspan}}"><span class="mr-5"> Total </span> {{$users_count}} </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    </div>
                                </div>

                                </div>

                            </div>

                        </div>
                        
                    @endforeach
                    
                </div>
                
            </div>

        </div>

    </div>

</div>



@endif

<form action="/nom035/tablero/dominio" method="post" class="change_period_id">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="periodo_id" class="periodo">
  <input type="hidden" name="sucursal" class="sucursal">
  <input type="hidden" name="direccion" class="direccion">
  <input type="hidden" name="job" class="job">
  <input type="hidden" name="edad" class="edad">
  <input type="hidden" name="ingreso" class="ingreso">
  <input type="hidden" name="sexo" class="sexo">
  <input type="hidden" name="centro_trabajo" class="centro_trabajo">
  <input type="hidden" name="turno" class="turno">
  <input type="hidden" name="regiones" class="regiones">
  <input type="submit" style="display: none">
</form>

@endsection

    
@section('scripts')

<style>
    
    .nav-item.nav-link{
        color: #fff;
        font-weight: bolder;
        background: #545b5f !important;
    }

    .bg-client-secondary{
        color: #fff;
        font-weight: bolder;
        background: #545b5f !important;
    }
    .bg-client-primary{
        color: #fff;
        font-weight: bolder;
        background: #406bb2 !important;
    }

    .nav-item.nav-link.active{
        color: #fff;
        font-weight: bolder;
        background: #406bb2 !important;
    }

    .text-client-primary{
        color: #406bb2;
        font-weight: bolder;
        /* background: #406bb2 !important; */
    }

</style>

<script>
    
        $(document).ready(function(){  
            // Cambia el periodo
            $('._change').click(function(){
              // Recarga la pagina con el nuevo periodo
              $('form.change_period_id .periodo').val($('#select_periodo').val());
              $('form.change_period_id .sucursal').val($('#select_sucursal').val());
              $('form.change_period_id .direccion').val($('#select_direccion').val());
              $('form.change_period_id .job').val($('#select_job').val());
              $('form.change_period_id .edad').val($('#select_edad').val());
              $('form.change_period_id .ingreso').val($('#select_inicio').val());
              $('form.change_period_id .sexo').val($('#select_sexo').val());
              $('form.change_period_id .centro_trabajo').val($('#select_centro_trabajo').val());
              $('form.change_period_id .turno').val($('#selects_turno').val());
              $('form.change_period_id .regiones').val($('#select_regiones').val());
              $('form.change_period_id').submit();
            });
        });



$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

    $('.report_general').DataTable({
      'order': [[1,'desc']]
   });

   $('#report_categorias').DataTable();
   $('#report_general').DataTable();
   $('#table_empleados_categorias').DataTable();

   $('#dominios').DataTable();
   $('#table_usuarios_dominios').DataTable();

   $('._change').on('change', function(){

$('#content').html('<div class="loading"><img src="{{ asset('img/ajax-loader.gif') }}" alt="loading" /><br/>Generando Gráficas...</div>');

var reporte = this.value;

var cambio = this.name;

change_filtro(reporte, cambio);

});



$('#Dimensiones').DataTable();

    var change_sucursal = function(sucursales) {

            var selOpts = "";
            var sucursales = sucursales;
            $('#select_sucursal').empty();
            selOpts += "<option value=''>--TODO--</option>";
            for (i=0;i<sucursales.length;i++){
              var id = sucursales[i]['id'];
              var val = sucursales[i]['nombre'];
              selOpts += "<option value='"+id+"'>"+val+"</option>";
            }
            $('#select_sucursal').append(selOpts);

    };

    var change_areas = function(directions) {

             var selOpts = "";
            var directions = directions;
            $('#select_direccion').empty();
            selOpts += "<option value=''>--TODO--</option>";
            for (i=0;i<directions.length;i++){
              var id = directions[i]['id'];
              var val = directions[i]['nombre'];
              selOpts += "<option value='"+id+"'>"+val+"</option>";
            }
            $('#select_direccion').append(selOpts);

    };

    var change_puestos = function(jobs) {
          
            var selOpts = "";
            var puestos = jobs;
            $('#select_job').empty();
            selOpts += "<option value=''>--TODO--</option>";
            for (i=0;i<puestos.length;i++){
              var id = puestos[i]['id'];
              var val = puestos[i]['name'];
              selOpts += "<option value='"+id+"'>"+val+"</option>";
            }
            $('#select_job').append(selOpts);



    };

    var change_edades = function(ages) {
          
            var selOpts = "";
            var edades = ages;
            $('#select_edad').empty();
            selOpts += "<option value=''>--TODO--</option>";
            for (i=0;i<edades.length;i++){
              var id = edades[i]['id'];
              var val = edades[i]['nombre'];
              selOpts += "<option value='"+id+"'>"+val+"</option>";
            }
            $('#select_edad').append(selOpts);
    };

    var change_inicios = function(starts) {
          
            var selOpts = "";
            var inicios = starts;
            $('#select_inicio').empty();
            selOpts += "<option value=''>--TODO--</option>";
            for (i=0;i<inicios.length;i++){
              var id = inicios[i]['id'];
              var val = inicios[i]['nombre'];
              selOpts += "<option value='"+id+"'>"+val+"</option>";
            }
            $('#select_inicio').append(selOpts);
    };

    var change_sexos = function(sexs) {
          
            var selOpts = "";
            var sexos = sexs;
            $('#select_sexo').empty();
            selOpts += "<option value=''>--TODO--</option>";
            for (i=0;i<sexos.length;i++){
              var id = sexos[i]['id'];
              var val = sexos[i]['nombre'];
              selOpts += "<option value='"+id+"'>"+val+"</option>";
            }
            $('#select_sexo').append(selOpts);
    };

    var change_centros_trabajo = function(sucursals) {
          
            var selOpts = "";
            var centros_trabajo = sucursals;
            $('#select_centro_trabajo').empty();
            selOpts += "<option value=''>--TODO--</option>";
            for (i=0;i<centros_trabajo.length;i++){
              var id = centros_trabajo[i]['id'];
              var val = centros_trabajo[i]['nombre'];
              selOpts += "<option value='"+id+"'>"+val+"</option>";
            }
            $('#select_centro_trabajo').append(selOpts);
    };

    var change_turnos = function(datas) {
          
            var selOpts = "";
            var datas = datas;
            $('#selects_turno').empty();
            selOpts += "<option value=''>--TODO--</option>";
            for (i=0;i<datas.length;i++){
              var id = datas[i]['id'];
              var val = datas[i]['nombre'];
              selOpts += "<option value='"+id+"'>"+val+"</option>";
            }
            $('#selects_turno').append(selOpts);
    };


    var change_filtro = function(reporte, cambio) {

var sel_sucursal = $('#select_sucursal').val();
var sel_direccion = $('#select_direccion').val();
var sel_job = $('#select_job').val();



axios.post('{{ url("/nom035/tablero/dominio") }}', {
    sucursal: $('#select_sucursal').val(),
    direccion: $('#select_direccion').val(),
    job: $('#select_job').val(),
    periodo: $('#select_periodo').val(),
    sexo: $('#select_sexo').val(),
    edad: $('#select_edad').val(),
    ingreso: $('#select_inicio').val(),
    centro_trabajo: $('#select_centro_trabajo').val(),
    regiones: $('#select_regiones').val(),
    cambio: cambio,
})
.then(function (response){           
  
    console.log(response);


    if($('#select_regiones').val()==''){
      change_regiones(response.data.regiones,response.data.id_centros_trabajo);
    }

    if($('#select_sucursal').val()==''){
      change_sucursal(response.data.sucursales,response.data.id_sucursal);
    }

    if($('#select_direccion').val()==''){
      change_areas(response.data.directions,response.data.id_direction);
    }

    if($('#select_job').val()==''){
      change_puestos(response.data.jobs,response.data.id_job);
    }

    if($('#select_sexo').val()==''){
      change_sexos(response.data.sexs,response.data.id_sex);
    }

    if($('#select_edad').val()==''){
      change_edades(response.data.ages,response.data.id_age);
    }

    if($('#select_inicio').val()==''){
      change_inicios(response.data.starts,response.data.id_start);
    }

    if($('#select_centro_trabajo').val()==''){
      change_centros_trabajo(response.data.centros_trabajo,response.data.id_centros_trabajo);
    }

    if($('#selects_turno').val()==''){
      change_turnos(response.data.turnos,response.data.id_centros_trabajo);
    }
 

   if(response.data.sin_data){

    html =  '<div class="margin-top-20">'+
            '<div class="alert alert-info">'+
            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
            'El filtro aplicado no contiene tiene resultados'+
            '</div>'+
            '</div>';
      $('#sin_resultados').html(html);


    html =  '<div class="info-box mb-3">'+
            '<span class="info-box-icon"><i class="fa fa-bar-chart"></i></span>'+
            '<div class="info-box-content">'+
            '<span class="info-box-text">Para la calificación final:</span>'+
            '<span class="info-box-number">0</span>'+
            '</div>'+
            '</div>';
      $('#DataResult').html(html);

    }else{

      $('#sin_resultados').html('');

    }

    var html = '';
            var i;
            var filas = Object.keys(response.data.tables_dim.antiquity.rows).length;
            var data = response.data.tables_dim.antiquity.rows;
            var users_count = response.data.users_count;
    
            console.log('asd',filas,data);
 
            $.each( data, function( key, value ) {

                html += '<tr>' +
                        '<td>' + value.name + '</td>' +
                        '<td class="text-right">' + value.total + '</td>' +          
                        '</tr>';

            }); 
 
            html += '<tr><td colspan="2" class="text-right font-weight-bolder"><span class="mr-5"> Total </span> ' + users_count + ' </td></tr>';

            $('.Antigüedad').html(html);


            var html = '';
            var i;
            var filas = Object.keys(response.data.tables_dim.sex.rows).length;
            var data = response.data.tables_dim.sex.rows;
    
            console.log('asd',filas,data);
 
            $.each( data, function( key, value ) {

                html += '<tr>' +
                        '<td>' + value.name + '</td>' +
                        '<td class="text-right">' + value.total + '</td>' +          
                        '</tr>';

            }); 
 
            html += '<tr><td colspan="2" class="text-right font-weight-bolder"><span class="mr-5"> Total </span> ' + users_count + ' </td></tr>';

            $('.Sexo').html(html);


            var html = '';
            var i;
            var filas = Object.keys(response.data.tables_dim.generations.rows).length;
            var data = response.data.tables_dim.generations.rows;            
 
            $.each( data, function( key, value ) {

                html += '<tr>' +
                        '<td>' + value.nac + '</td>' +
                        '<td>' + value.name + '</td>' +          
                        '<td class="text-right">' + value.total + '</td>' +          
                        '</tr>';

            }); 
 
            html += '<tr><td colspan="3" class="text-right font-weight-bolder"><span class="mr-5"> Total </span> ' + users_count + ' </td></tr>';

            $('.Generación').html(html);

            var html = '';
            var i;
            var filas = Object.keys(response.data.tables_dim.turnos.rows).length;
            var data = response.data.tables_dim.turnos.rows;            
 
            $.each( data, function( key, value ) {

                html += '<tr>' +
                        '<td>' + value.name + '</td>' +          
                        '<td class="text-right">' + value.total + '</td>' +          
                        '</tr>';

            }); 
 
            html += '<tr><td colspan="2" class="text-right font-weight-bolder"><span class="mr-5"> Total </span> ' + users_count + ' </td></tr>';

            $('.Turnos').html(html);


    // var html = '';
    // var i;
    // var filas = response.data.clasificacion_general.length;
    // var data = response.data.clasificacion_general;

    // html += ' <table class="table table-striped report_general2">'+
    //         '<thead>'+
    //         '<tr>'+
    //         '<th>Nombres</th>'+
    //         '<th>Calificacion:</th>'+
    //         '</tr>'+
    //         '</thead><tbody>';

    //   for (i = 0; i < filas; i++) {
    //     html += '<tr>' +
    //       '<td>' + data[i]['0'] + '</td>' +
    //       '<td class="text-right font-weight-bold ' + data[i]['color'] + '">' + data[i]['valor'] + '</td>' +                
    //       '</tr>';
    //   }
    //   html += '</tbody> <table>'+
    //   $('#report').html(html);


    //   $(document).ready( function () {
    //       $('.report_general2').DataTable();
    //   } );



    // html ='<div class="info-box mb-3 ' +response.data.color_general+'">'+
    //       '<span class="info-box-icon"><i class="fa fa-chart-area"></i></span>'+
    //       '<div class="info-box-content">'+
    //       '<span class="info-box-text">Para la calificación final:</span>'+
    //       '<span class="info-box-number">'+response.data.resultado_general.toFixed(2)+'</span>'+
    //       '</div>'+
    //       '</div>';
    //   $('#DataResult').html(html);




    var reporte_nom_categorias = {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Calificación de la Categoría'
        },
        subtitle: {
            text: 'General'
        },
        xAxis: {
            categories: response.data.name_categories,
            crosshair: true
        },
        yAxis: {
        title: {
            text: 'Calificación final'
        }

        },
        legend: {
            enabled: true
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
        },
        series: response.data.clasificacion_categorias 
    }
    var chart = new Highcharts.chart('containercategoria', reporte_nom_categorias);

   var graf1 =  {
          chart: {
              type: 'column'
          },
          title: {
              text: 'Calificación por Dominios'
          },
          subtitle: {
              text: 'General'
          },
          xAxis: {
              categories: response.data.name_categories,
              crosshair: true
          },
          yAxis: {
          title: {
              text: 'Calificación final'
          }

      },
      legend: {
          enabled: true
      },
      plotOptions: {
          series: {
              borderWidth: 0,
              dataLabels: {
                  enabled: true,
                  format: '{point.y:.1f}'
              }
          }
      },

      tooltip: {
          headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
          pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
      },

          series: response.data.clasificacion_dominios 
      }


    var chart = new Highcharts.chart('containerdominios', graf1);

    // var html = '';
    // var i;
    // var filas = response.data.clasificacion_categorias.length;
    // var data = response.data.clasificacion_categorias;

    // html +='<table class="table table-striped report_general2">'+
    //         '<thead>'+
    //         '<tr>'+
    //         '<th>Categoria</th>'+
    //         '<th>Para la calificación final::</th>'+
    //         '</tr>'+
    //         '</thead><tbody>';

    //   for (i = 0; i < filas; i++) {
    //     html += '<tr>' +
    //       '<td>' + data[i].name + '</td>' +
    //       '<td class="text-right font-weight-bold ' + data[i].colors + '">' + data[i].critico + '</td>' +                 
    //       '</tr>';
    //   }
    // html += ' </tbody> <table>'+


    // $('#table-categorias').html(html);

    // $(document).ready( function () {
    //     $('.report_general2').DataTable();
    // } );


    // html ='';
    // html += ' <table class="table table-striped report_general3"><thead>';
        
    // html += '<tr><th>Empleado</th>';
    // html += '<th>Departamento</th>';
    // html += '<th>Área</th>';
    // html += '<th>Puesto</th>';
    // html += '<th>Centro de trabajo</th>';
    // html += '<th>Empresa</th>';
    // var i;
    // var filas_2 = response.data.name_categories.length;
    // var name_categories = response.data.name_categories;
    // for (i = 0; i < filas_2; i++) {
    //     html += 
    //       '<th>' + name_categories[i]['name'] + '</th>';
          
    //   }
    // html += '</tr>';

    // html +='</thead><tbody>';

    // var i;
    // var filas = response.data.clasificacion_categorias_by_usuarios.length;
    // var data = response.data.clasificacion_categorias_by_usuarios;
    // var datos = '';

    // for (i = 0; i < filas; i++) {
    //     var filasx = response.data.clasificacion_categorias_by_usuarios[i].length;
    //     html += '<tr>' ;            
    //     datos = data[i]['0'].split('*');
    //     html += '<td>' + datos[0] + '</td>';
    //     datos = datos[1].split('#');
    //     html += '<td>' + datos[0] + '</td>';
    //     datos = datos[1].split('=');
    //     html += '<td>' + datos[0] + '</td>';
    //     datos = datos[1].split('$');
    //     html += '<td>' + datos[0] + '</td>';
    //     datos = datos[1].split('[');
    //     html += '<td>' + datos[0] + '</td>';
    //     html += '<td>' + datos[1] + '</td>';
    //     for (x = 1; x < filasx; x++) {
    //       html += '<td class="text-right font-weight-bold ' + data[i][x]['color'] + '">' + data[i][x]['valor'] + '</td>';     
    //     }
    //    '</tr>';

    // }
    // html += ' </tbody> <table>'+

    // $('#table-empleados-categorias').html(html);

    // $(document).ready( function () {
    //     $('.report_general3').DataTable();
    // } );



    // var html = '';
    // var i;
    // var filas = response.data.clasificacion_dominios.length;
    // var data = response.data.clasificacion_dominios;


    //   html += ' <table class="table table-striped report_general2">'+
    //   '<thead>'+
    //       '<tr>'+
    //           '<th>Categoría</th>'+
    //           '<th>Dominio</th>'+
    //          '<th>Para la calificación final::</th>'+
    //       '</tr>'+
    //   '</thead><tbody>';


    //   for (i = 0; i < filas; i++) {
    //     html += '<tr>' +
    //       '<td>' + data[i].category + '</td>' +
    //       '<td>' + data[i].name + '</td>' +
    //       '<td class="text-right font-weight-bold ' + data[i].colors + '">' + data[i].critico + '</td>' +
         
    //       '</tr>';
    //   }
      

    // html += ' </tbody> <table>'+


    // $('#table-dominios').html(html);

    // $(document).ready( function () {
    //     $('.report_general2').DataTable();
    // } );



    // html ='';
    // html += '<table class="table table-striped report_general3"><thead>';
    // html += '<tr><th>Empleado</th>';
    // html += '<th>Departamento</th>';
    // html += '<th>Área</th>';
    // html += '<th>Puesto</th>';
    // html += '<th>Centro de trabajo</th>';
    // html += '<th>Empresa</th>';
    // var i;
    // var filas_2 = response.data.name_dominios.length;
    // var name_categories = response.data.name_dominios;
    // for (i = 0; i < filas_2; i++) {
    //     html += 
    //       '<th>' + name_categories[i]['name'] + '</th>';
          
    //   }
     
    // html += '</tr>';

    // html +='</thead><tbody>';

    // var i;
    // var filas = response.data.clasificacion_dominios_by_usuarios.length;
    // var data = response.data.clasificacion_dominios_by_usuarios;
    // var datos = '';
        
    //   for (i = 0; i < filas; i++) {
      

    //     var filasx = response.data.clasificacion_dominios_by_usuarios[i].length;

    //     html += '<tr>' ;
    
    //     datos = data[i]['0'].split('*');
    //     html += '<td>' + datos[0] + '</td>';
    //     datos = datos[1].split('#');
    //     html += '<td>' + datos[0] + '</td>';
    //     datos = datos[1].split('=');
    //     html += '<td>' + datos[0] + '</td>';
    //     datos = datos[1].split('$');
    //     html += '<td>' + datos[0] + '</td>';
    //     datos = datos[1].split('[');
    //     html += '<td>' + datos[0] + '</td>';
    //     html += '<td>' + datos[1] + '</td>';

    //   for (x = 1; x < filasx; x++) {

    //     html += '<td class="text-right font-weight-bold ' + data[i][x]['color'] + '">' + data[i][x]['valor'] + '</td>';
         
         
    //   }

    //    '</tr>';

    //     }

    // html += ' </tbody> <table>'+


    // $('#table-empleados-dominios').html(html);

    // $(document).ready( function () {
    //     $('.report_general3').DataTable();
    // } );
    

    console.log('asd',response.data.clasificacion_dimensiones);
    // var tmpSeries;
    // for(var i=0;i<obj.length;i++)
    // {
    // tempSeries.push({name: obj[i].names, y: obj[i].salaries}) ;
    // } 

    var reporte_nom_dimensiones = {

        chart: {
            type: 'column'
        },
        title: {
            text: 'Calificación por Dimensiones'
        },
        subtitle: {
            text: 'General'
        },
        xAxis: {
            categories: response.data.name_categories,
            crosshair: true
        },
        yAxis: {
        title: {
            text: 'Calificación final'
        }

        },
        legend: {
            enabled: true
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
        },

        series: response.data.clasificacion_dimensiones 

    }

    var chart = new Highcharts.chart('containerdimensiones', reporte_nom_dimensiones);

    // var html = '';
    // var i;
    // var filas = response.data.clasificacion_dimensiones.length;
    // var data = response.data.clasificacion_dimensiones;
    // var colores_dominios = response.data.colores_dominios;


    //   html += ' <table class="table table-striped report_general2">'+
    //   '<thead>'+
    //       '<tr>'+
    //           '<th>Categoría</th>'+
    //           '<th>Dominio</th>'+
    //           '<th>Dimension</th>'+
    //          '<th>Para la calificación final::</th>'+
    //       '</tr>'+
    //   '</thead><tbody>';


    //   for (i = 0; i < filas; i++) {
    //     html += '<tr>' +
    //       '<td>' + data[i].category + '</td>' +
    //       '<td>' + data[i].domain + '</td>' +
    //       '<td>' + data[i].name + '</td>' +
    //       '<td class="text-right font-weight-bold ' + colores_dominios[data[i].domain_id] + '">' + data[i].data[0] + '</td>' +
         
    //       '</tr>';
    //   }

    // html += ' </tbody> <table>'+


    // $('#table-dimensiones').html(html);

    $(document).ready( function () {
        $('.report_general2').DataTable();
    } );


    $('#content').fadeIn(1000).html('');

})
.catch(function (error){

});




};



















    

</script>

@endsection
