
<div class="row">

    {{-- CLASIFICACION POR CATEGORIAS --}}
    {{-- <div class="col-md-6">

        <div class="card">

            <div class="card-body">

                <h4 class="text-client-primary">Categorias</h4>
                    
                <div id="table-categorias">
                    <table class="table table-striped" id="report_categorias">
                        <thead class="bg-client-primary text-white">
                            <tr>
                                <th>Categoria</th>
                                <th>Para la calificación final:</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($clasificacion_categorias as $key => $clasificacion_categoria)
                                
                                <tr>
                                    <td>{!! $clasificacion_categoria['name'] !!}</td>

                                    <td class="text-right font-weight-bold {!! $clasificacion_categoria['colors'] !!}">
                                       {{ is_int($clasificacion_categoria['critico']['0']) ? $clasificacion_categoria['critico']['0'] : number_format($clasificacion_categoria['critico']['0'], 2) }} 
                                    </td>

                                </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>

        </div>

    </div> --}}


    {{-- CLASIFICACION POR CATEGORIAS --}}
    <div class="col-md-12 p-0">

        <div class="card shadow-lg border-0">

            <div class="card-body">

                <h4 class="text-client-primary">GRÁFICA POR CATEGORÍA</h4>
                <hr>
                <div class="row d-flex">
                    <div class="col-2" style="align-self: center;">
                        <img class="img-responsive" src="{{ asset('/img/niveles.png') }}"><p class="py-2 text-white">as</p>
                    </div>
                    <div class="col-10">
                        
                        <div id="containercategoria" class="my-3">
        
                        </div>
                    </div>
                </div>

            </div>
            <div class="row px-3">
                <div class="col-12 text-center bg-primary" style="
                    border-radius: 0px 0px 10px 10px;
                ">.
                </div>
            </div>

        </div>

    </div>

</div>

<br><br>

<div class="row">

    <div class="col-md-12 p-0">

        <div class="card shadow-lg border-0">

            <div class="card-body"> 

                <div class="row pb-3"> 
                    @foreach ($tables_cat as $key => $values)
                    
                        <div class="col-md-6" >

                            <div class="card border-0">
                    
                                <div class="card-header bg-client-primary">
                                    Distribución por {{$values['name']}}
                                </div> 

                                <div class="card-body p-1"> 
        
                                    <div class="table-responsive">
                                    <div id="table-empleados-categorias">
                    
                                    <table class="table table-striped table-sm">
                                        <thead id="result_categoria_empleados_head" class="bg-client-secondary">
                                            <tr>
                                                @foreach ($values['headers'] as $keyheaders => $header)
                                                <th>{{$header}}</th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody id="result_categoria_empleados_body" class="{{$values['name']}}">
                                            @foreach ($values['rows'] as $keyrows => $row)
                                            <tr>
                                                <?php $colspan = 0; $colspan = count($row); ?>
                                                @foreach ($row as $keyrow => $items)
                                                    @if($keyrow!='id')
                                                        <td {{($keyrow=='total')?"class=text-right":null}}>
                                                            {{$items}} 
                                                        </td>
                                                    @endif
                                                @endforeach
                                            </tr>
                                             
                                            @endforeach
                                            <tr>
                                                <td class="text-right font-weight-bolder" colspan="{{$colspan}}"><span class="mr-5"> Total </span> {{$users_count}} </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    </div>
                                </div>

                                </div>

                            </div>

                        </div>
                        
                    @endforeach
                    
                </div>
                
            </div>

        </div>

    </div>

</div>

