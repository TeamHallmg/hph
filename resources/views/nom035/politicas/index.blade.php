@extends('nom035.app')

@section('title', 'Políticas')

@section('content')
	<div class="titulo" style="font-size:8vw; margin-bottom: 0px; color:#73160f">
    <h4>Políticas</h4>
    <hr>
	</div>
	<table id="politicas_table" class="table table-striped table-bordered dt-responsive" >
		<thead>
			<tr>
				<th>#</th>
				<th>Descripción</th>
				<th>Acciones</th>
			</tr>
		</thead>
	
	@if (count($politicas) > 0)
						
		@foreach($politicas as $politica)
		<tbody>					
			<tr>
				<td width="10%">{{ $politica->id }}</td>
				<td width="80%">Politica {{ $politica->id }}</td>
				<!--<td width="80%">{!! \Illuminate\Support\Str::words($politica->description, 20, '...') !!}</td>-->
				<td width="10%" align="center">
					<a class="btn btn-primary" href="{{ url('nom035/politicas-create/' . $politica->id) }}">
						<span class="glyphicon glyphicon-pencil"></span> Editar
					</a>
				</td>
			</tr>
		@endforeach
	@endif

		</tbody>
	</table>
	<div class="text-center">
    <a class="btn btn-primary" href="{{ url('nom035/politicas-create') }}">
    	<span class="glyphicon glyphicon-plus"></span> Crear
    </a>
	</div>
		
@endsection
@section('scripts')

<style>
	#politicas_table{
		width: 100% !important;
	}
</style>

<script type="text/javascript">
	$(document).ready(function() {
		var espanol = {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		};

	  var table = $('#noticias_table').DataTable( {
	    orderCellsTop: true,
	    //fixedHeader: true
	    language: espanol,
	    pageLength: 25,
	    order: [[ 0, "desc" ]], //ordenar descendete primer columna
	  });

	  $(function () {
			$('[data-toggle="tooltip"]').tooltip();
			$('[data-toggle="tooltip2"]').tooltip();
		});
	});
</script>
@endsection