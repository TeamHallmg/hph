@extends('nom035.app')

@section('title', 'Editar Política')

@section('content')

	<div class="titulo" style="font-size:8vw;">
		<h3>Editar Política</h3>
		<hr>
	</div>

	<div class="row" style="margin: 50px 0; display: block;">
		<form action="{{ url('nom035/politicas-store') }}" method="post">
			@csrf

			@if (!empty($politica))

			<input type="hidden" name="id" value="{{$politica->id}}">
			@endif
			
			<div class="row col-md-12" style="display: block;">
				<label for="description">Descripción</label>
				<textarea class="form-control" name="description" id="description" cols="30" rows="15">{!! $politica['description'] !!}</textarea>
			</div>

			<div class="row col-md-12" style="margin-top: 15px">
				<label for="enterprise_id">Empresa</label>
				<select name="enterprise_id" id="enterprise" class="form-control">
					<option value="0">Todas</option>

			@if (count($enterprises) > 0)

				@foreach ($enterprises as $key => $enterprise)
			
					<option value="{{ $enterprise->id }}" <?php if (!empty($politica) && $politica->enterprise_id == $enterprise->id){ ?>selected="selected"<?php } ?>>{{ $enterprise->name }}</option>
				@endforeach
			@endif
					
				</select>
			</div>

			<div class="row col-md-12 sucursals" style="margin-top: 15px; <?php if (empty($politica) || empty($politica->enterprise_id)){ ?>display: none<?php } ?>">
				<label for="sucursal_id">Centros de trabajo</label>
				<select name="sucursal_id" id="sucursal" class="form-control">
					<option value="0">Todos</option>

			@if (count($sucursals) > 0)

				@foreach ($sucursals as $key => $sucursal)

					@if (!empty($politica) && $politica->enterprise_id == $sucursal->enterprise_id)
					<option value="{{$sucursal->id}}" <?php if ($politica->sucursal_id == $sucursal->id){ ?>selected="selected"<?php } ?>>{{$sucursal->name}}</option>
					@endif
				@endforeach
			@endif
				</select>
			</div>

			<div class="row col-md-12" style="margin-top: 15px;">
				<button type="submit" class="btn btn-success">Guardar</button>
				<a href="{{ url('nom035/politicas') }}" type="button" class="btn btn-primary">Cancelar</a>
			</div>
		</form>
				
	</div>

@endsection
@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

	<script type="text/javascript">

		var sucursals = <?php echo json_encode($sucursals)?>;

		$(document).ready(function($) {
			$('#description').summernote({
				placeholder: '',
				tabsize: 2,
				height: 500,
				fontSizes: ['8', '9', '10', '11', '12', '14', '16', '18', '20', '24', '36'],
				toolbar: [
				['style', ['style']],
				['font', ['bold', 'underline', 'clear']],
				['fontsize', ['fontsize']],
				['color', ['color']],
				['para', ['ul', 'ol', 'paragraph']],
				//   ['table', ['table']],
				//   ['insert', ['link', 'picture', 'video']],
				//   ['view', ['fullscreen', 'codeview', 'help']]
				['view', ['help']]
				]
			});
	
    	// Cambia la empresa
    	$('select#enterprise').change(function(){

      	var enterprise_id = $(this).val();
      	var content = '<option value="0">Todos</option>';
      	var sucursales = 0;

      	for (var i = 0;i < sucursals.length;i++){

      		if (sucursals[i].enterprise_id == enterprise_id){

      			content += '<option value="' + sucursals[i].id + '">' + sucursals[i].name + '</option>';
      			sucursales++;
      		}
      	}

      	$('select#sucursal').html(content);

      	if (sucursales > 0){

      		$('div.sucursals').show();
      	}

      	else{

      		$('div.sucursals').hide();
      	}
			});
		});
	</script>
@endsection