@extends('nom035.app')

@section('title', '¿Que es?')

@section('content')
	{{-- <h3 class="titulos-evaluaciones mt-4 font-weight-bold">¿Qué es?</h3> --}}

	<div class="text-center">
    <img src="{{ asset('img/banner_nom035.png') }}" alt="" class="img-fluid">
  </div>
	<div class="text-center mt-3">
		<img class="img-fluid" src="{{ asset('img/nom035_que_es.png') }}" alt="">
	</div>
	{{-- <div class="card">
		<div class="mx-3">
			<ul class="nav nav-pills mt-3" id="pills-tab" role="tablist">
				<li class="nav-item">
					<a class="btn-tab active" id="objetivo-tab" data-toggle="pill" href="#objetivo" role="tab" aria-controls="objetivo" aria-selected="true">Objetivo</a>
				</li>
				<li class="nav-item">
					<a class="btn-tab" id="informacion-tab" data-toggle="pill" href="#informacion" role="tab" aria-controls="informacion" aria-selected="false">Información General</a>
				</li>
			</ul>
		</div>
		<hr>
		<div class="card-body">
			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="objetivo" role="tabpanel" aria-labelledby="objetivo-tab">
					<p>Obtener información, acerca de la forma en que se percibe la operación de la organización, para con base en ella implementar planes de acción, que ayuden a incrementar la efectividad en la búsqueda de la calidad, mejoramiento, productividad, calidad de vida en el trabajo y en el logro de objetivos.</p>
				</div>
				<div class="tab-pane fade" id="informacion" role="tabpanel" aria-labelledby="informacion-tab">
					<p>El Diagnóstico  pretende obtener información de <b>la organización</b> como un todo, a través de la percepción de sus colaboradores, pero no información sobre individuos, por lo que este cuestionario deberá contestarse en forma anónima.</p>
					<p>Este diagnóstico es personal y confidencial.</p>
					<p>La información proporcionada por la organización será la base del análisis.</p>
					<p>En la medida en que proporcione información válida y significativa los resultados a obtener también lo serán. <strong>Es muy importante su colaboración.</strong></p>
					<p>
						<strong>El solicitar su nombre , es solo con fines de control de la aplicación de la encuesta y saber quiénes han respondido dicha encuesta en la que  solo AdGentis tiene acceso a ello</strong>
					</p>
				</div>
			</div>
		</div>
	</div> --}}
@endsection