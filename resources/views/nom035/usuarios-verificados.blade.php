@extends('nom035.app')

@section('title', '¿Que es?')

@section('content')
<style type="text/css">
  div#verify_wrapper {position: relative;padding-top: 40px}
  div.dt-buttons {top: 0;left: 0;position: absolute;}
  div.dataTables_length, div.dataTables_info {float: left;}
  div#evaluados_filter, div.dataTables_paginate {float: right;}  
</style>
<?php $counter = 0; ?>
<div class="text-center">
  <img src="{{ asset('img/banner_nom035.png') }}" alt="" class="img-fluid">
</div>
<div class="container-fluid">
    <div class="row justify-content-center my-5">

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="row info-box bg-danger rounded py-2 text-white mx-1">
                        <div class="col-2 d-flex align-items-center justify-content-center">
                            <span class="info-box-icon">
                                <h4 class="m-0"><i class="fas fa-arrow-alt-circle-down fa-2x"></i></h4>
                            </span>
                        </div>
                        <div class="col-10">
                            <div class="info-box-content">
                                <span class="info-box-text h5">Sin Verificar</span><br>
                                <span class="info-box-number font-weight-bold">{{ $users->not_validated . '/' . ($users->count() - $total_inactive) }}</span>
                                <div class="progress" style="background-color: rgba(0,0,0,.125);">
                                    <div class="progress-bar bg-white" style="width: {{ number_format($users->not_validated * 100 / ($users->count() - $total_inactive), 1, '.', ',') . '%' }}"></div>
                                </div>
                                <span class="progress-description">
                                    {{ number_format($users->not_validated * 100 / ($users->count() - $total_inactive), 1, '.', ',') . '%' }}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-3">
                    <div class="info-box bg-danger">
                        <span class="info-box-icon">
                            <i class="fas fa-arrow-alt-circle-down"></i>
                        </span>
                        <div class="info-box-content">
                            <span class="info-box-text">Sin Verificar</span>
                            <span class="info-box-number">{{ $users->not_validated . '/' . $users->count() }}</span>
                            <div class="progress">
                                <div class="progress-bar" style="width: {{ number_format($users->not_validated * 100 / $users->count(), 1, '.', ',') . '%' }}"></div>
                            </div>
                            <span class="progress-description">
                                {{ number_format($users->not_validated * 100 / $users->count(), 1, '.', ',') . '%' }}
                            </span>
                        </div>
                    </div>
                </div> --}}
                <div class="col-md-6">
                    <div class="row info-box bg-success rounded py-2 text-white mx-1">
                        <div class="col-2 d-flex align-items-center justify-content-center">
                            <span class="info-box-icon">
                                <h4 class="m-0"><i class="fas fa-check-circle fa-2x"></i></h4>
                            </span>
                        </div>
                        <div class="col-10">
                            <div class="info-box-content">
                                <span class="info-box-text h5">Verificados</span><br>
                                <span class="info-box-number font-weight-bold">{{ $users->validated . '/' . ($users->count() - $total_inactive) }}</span>
                                <div class="progress" style="background-color: rgba(0,0,0,.125);">
                                    <div class="progress-bar bg-white" style="width: {{ number_format($users->validated * 100 / ($users->count() - $total_inactive), 1, '.', ',') . '%' }}"></div>
                                </div>
                                <span class="progress-description">
                                    {{ number_format($users->validated * 100 / ($users->count() - $total_inactive), 1, '.', ',') . '%' }}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-3">
                    <div class="info-box bg-success">
                        <span class="info-box-icon">
                            <i class="fas fa-check-circle"></i>
                        </span>
                        <div class="info-box-content">
                            <span class="info-box-text">Verificados</span>
                            <span class="info-box-number">{{ $users->validated . '/' . $users->count() }}</span>
                            <div class="progress">
                                <div class="progress-bar" style="width: {{ number_format($users->validated * 100 / $users->count(), 1, '.', ',') . '%' }}"></div>
                            </div>
                            <span class="progress-description">
                                {{ number_format($users->validated * 100 / $users->count(), 1, '.', ',') . '%' }}
                            </span>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>

        <div class="col-md-12 mt-3">
            <div class="card card-3">
                <div class="card-header bg-client-primary text-white">
                    <h2 class="my-auto font-weight-bold">Verificación de Política de Usuario</h2>
                </div>
                <div class="card-body">
                    <div class="col table-responsive my-4">
                        <table id="verify" class="table table-striped table-bordered">
                            <thead class="thead-inverse bg-client-primary text-white">
                                <tr>
                                    <th>ID Empleado</th>
                                    <th>Nombre Completo</th>
                                    <th>Email</th>
                                    <th>Departamento</th>
                                    <th>Área</th>
                                    <th>Puesto</th>
                                    <th>Centro de trabajo</th>
                                    <th>Empresa</th>
                                    <th>Jefe</th>
                                    <th>Fecha</th>
                                    <th>Último Periodo</th>
                                    <th>Verificación de Política</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                  @if ($user->active == 1 && empty($user->deleted_at))
                              <?php $counter++; ?>
                                    <tr>
                                        <td>{{$user->employee->idempleado ?? 'N/A'}}</td>
                                        <td>{{$user->first_name.' '.$user->last_name}}</td>
                                        <td>{{$user->email ?? 'N/A'}}</td>
                                        <td>{{$user->employee->departamento ?? 'N/A'}}</td>
                                        <td>{{$user->employee->areas ?? 'N/A'}}</td>
                                        <td>{{$user->employee->job ?? 'N/A'}}</td>
                                        <td>{{$user->employee->sucursal ?? 'N/A'}}</td>
                                        <td>{{$user->employee->empresa ?? 'N/A'}}</td>
                                        <td>{{$user->employee->jefe ?? 'N/A'}}</td>
                                        <td>{{$user->verification->updated_at ?? 'N/A'}}</td>
                                        <td>{{(!empty($user->last_period) ? $user->last_period->period->name : '')}}</td>
                                        <td class="{{!is_null($user->verification) ? 'bg-success' : 'bg-white'}}">{{!is_null($user->verification) ? 'Verificado' : 'Sin Verificar'}}</td>
                                    </tr>
                                  @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <hr>

                    @if ($counter > 0)
                    <div class="row justify-content-end mr-2">
                        <button type="button" class="btn btn-danger card-1 ml-2" data-toggle="modal" data-target="#modalResetPolicyAgreement"><i class="fas fa-envelope-open-text"></i> Resetear Política de Usuarios</button>
                    </div>
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalResetPolicyAgreement" tabindex="-1" role="dialog" aria-labelledby="modalResetPolicyAgreement" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-client-primary text-white">
                <h5 class="modal-title" id="exampleModalLabel">Atención!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form" action="{{ url('nom035/delete_policy_agreement') }}" method="POST" id="form_delete">
                {{ csrf_field() }}
				<div class="modal-body">
                    <div class="text-center" id="hasCalculations">
                        <h2 class="text-center"><span class="text-warning"><i class="fas fa-exclamation-triangle"></i></span> ¡Advertencia!</h2><br>
						<p class="text-danger">Se resetearan todas las aceptaciones de los usuarios a la política de la Norma 035</p>
					</div>
					<div class="form-group">
						<label id="cnt_motive" for="del_reason">Motivo (0/200)</label>
						<textarea name="del_reason" class="form-control noresize" maxlength="200" id="del_reason" onkeyup="countMotive(this.value)" rows="4" required></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-danger confirm-delete">Resetear de todos modos</button>
				</div>
			</form>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
    function countMotive(str) {
        var lng = str.length;
        $("#cnt_motive").html('Motivo (' + lng + '/200)');
    }

    $(document).ready(function(){
        $("#modalResetPolicyAgreement").on('hide.bs.modal', function (e) {
            $("#cnt_motive").html('Motivo (0/200)');
            $('#del_reason').val('');
        });

        $('#verify').DataTable({
            dom: 'Blfrtip',
            buttons: [
                {
                    extend: 'excel',
                    text: 'Guardar como Excel',
                    title: 'Empleadosquehanverificado',
                }
            ],
            "order": [[ 8, "desc" ]],
            language: {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },   
        });
    });

    var no_iniciados = {{ json_encode($users->not_validated) }};
    var iniciados = {{ json_encode($users->validated) }};

    /*Highcharts.chart('graphContainer', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: null,
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'white'
                    }
                }
            }
        },
        series: [{
            states: {
                hover: {
                    enabled: false
                }
            },
            enableMouseTracking: false,
            data: [
                {
                    y: no_iniciados,
                    color: '#e3342f'
                }, {
                    y: iniciados,
                    color: '#ffed4a'
                }
            ],
        }],
    });*/
</script>
@endsection