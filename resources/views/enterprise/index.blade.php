@extends('layouts.app')
@section('content')

<div class="container-fluid">
	<div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{!! Session::get('alert-'.$msg) !!} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
	</div>
	
	<h3>EMPRESAS</h3>
	<hr>
	<br>
	<table id="tablejob" class="table table-striped table-bottom-border">
		<thead>
			<tr>
				<td>ID</td>
				<td>Nombre</td>
				<td>Acciones</td>
			</tr>
		</thead>
		<tbody>
			@foreach($enterprises as $enterprise)
				<tr class="soft_blue_background">
					<td>{{ $enterprise->enterprise_code }}</td>
					<td>{{ $enterprise->name }}</td>
					<td>
						@if(!isset($enterprise->legal))
							<a class="btn btn-primary asignarRepre" data-id="{{  $enterprise->id }}" data-empresa="{{  $enterprise->name }}" data-toggle="modal" data-target="#asignar_representante" title="Asignar Representante" ><span class="fas fa-user-tie"></span></a>
						@else
							<a class="btn btn-success" data-toggle="modal" data-target="#edit-{{$enterprise->id}}" title="Editar Representante" ><span class="fas fa-user-tie"></span></a>
						@endif
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>


	{{-- modal para eliminar la postulación un nuevo usuario --}}
	@foreach($enterprises as $enterprise)
		@if(isset($enterprise->legal))
			@include('enterprise.editar_repre')
		@endif
	@endforeach

@include('enterprise.asignar_repre')

@endsection
	
@section('scripts')
	<script type="text/javascript">

		 $(".asignarRepre").click(function() {
  			var id = $(this).attr("data-id");
  			var empresa = $(this).attr("data-empresa");
  			$('#id_empresa').val(id);
  			$('#name_empresa').val(empresa);
			});



            $('#state_c').on('change', function(e) {
                e.preventDefault();
                var id = $(this).val();
                $.ajax ({
                    type: 'GET',
                    url: "{{ url('listStates') }}/"+id,
                    dataType: 'json',
                    success: function(response) {
                        $('#city_c').empty();
                        console.log(response);
                        $.each(response, function(key, element) {
                            $('#city_c').append("<option value='" + element.id + "'>" + element.nombre + "</option>").selectpicker('refresh');
                        });
                    }
                });
            });


	$(document).ready( function () {
		$('#tablejob').DataTable({
			"language": {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     '<span class="fa fa-arrow-right" aria-hidden="true"></span>',
					"sPrevious": '<span class="fa fa-arrow-left" aria-hidden="true"></span>',
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
		});
	});
	</script>
@endsection