  <div class="modal fade"  id="edit-{{ $enterprise->id }}" >
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Editar Datos del Representante</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" class="Exclude">&times;</span>
              </button>
            </div>

          <form action="{{ route('enterprise.update', $enterprise->id) }}" method="post">
            @csrf
            @method('PUT')
            
              <div class="modal-body">
                <div class="row">
                    <div class="col">
                      <h5>
                        Datos del representante legal
                      </h5>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-md-4">
                          
                      <div class="form-group">
                        <label for="name_empresa">Patrono </label>
                        <input id="name_empresa" type="text" class="form-control" title="Nombre" name="name_empresa" autocomplete="off" placeholder="Ingrese" required="" readonly=""  value="{{ $enterprise->name }}">
                      </div>
                    </div>

                    <div class="col-md-4">
                          
                      <div class="form-group">
                        <label for="dpi">DPI *</label>
                        <input id="dpi" type="text" class="form-control" title="Nombre" name="dpi" autocomplete="off" placeholder="Ingrese" required="" value="{{ $enterprise->legal->dpi }}">
                      </div>
                    </div>
                    <div class="col-md-4">
                          
                      <div class="form-group">
                        <label for="nombre">Nombre Completo *</label>
                        <input id="nombre" type="text" class="form-control" title="Nombre"  name="nombre" autocomplete="off" placeholder="Ingrese Nombre Completo" required="" value="{{ $enterprise->legal->nombre }}">
                      </div>
                    </div>

                    <div class="col-md-3">
                          
                      <div class="form-group">
                        <label for="nacionalidad">Nacionalidad *</label>
                        <input id="nacionalidad" type="text" class="form-control" title="Nombre"  name="nacionalidad" autocomplete="off" placeholder="Ingrese Nacionalidad" required="" value="{{ $enterprise->legal->nacionalidad }}">
                      </div>
                    </div>

                    <div class="col-md-3">
                          
                      <div class="form-group">
                        <label for="fecha_nac">Fecha de Nacimiento *</label>
                        <input id="fecha_nac" type="date" class="form-control" title="Nombre"  name="fecha_nac" autocomplete="off" placeholder="Ingrese" required="" value="{{ $enterprise->legal->fecha_nac }}">
                      </div>
                    </div>

                    <div class="col-md-3">
                          
                      <div class="form-group">
                        <label for="sexo">Sexo *</label>
                        <div class="form-group">
                            <input type="radio" name="sexo" id="Masculino" value="Masculino"  required="" {{ $enterprise->legal->sexo == 'Masculino' ? 'checked' : '' }}>
                            <label for="Masculino">Masculino</label>
                            <input type="radio" name="sexo" id="Femenino" value="Femenino" {{ $enterprise->legal->sexo == 'Femenino' ? 'checked' : '' }}>
                            <label for="Femenino">Femenino</label>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                          
                      <div class="form-group">
                        <label for="edo_civil">Estado Civil *</label>
                        <select id="edo_civil" name="edo_civil" class="form-control" required="">
                          <option disabled value="" selected hidden>Seleccione una opción...</option>
                          <option value="SOLTERO" {{ $enterprise->legal->edo_civil == 'SOLTERO' ? 'selected' : '' }}>Soltero</option>
                          <option value="CASADO"{{ $enterprise->legal->edo_civil == 'CASADO' ? 'selected' : '' }}>Casado</option>
                          <option value="VIUDO" {{ $enterprise->legal->edo_civil == 'VIUDO' ? 'selected' : '' }}>Viudo</option>
                          <option value="DIVORCIADO" {{ $enterprise->legal->edo_civil == 'DIVORCIADO' ? 'selected' : '' }}>Divorciado</option>
                          <option value="INDISTINTO" {{ $enterprise->legal->edo_civil == 'INDISTINTO' ? 'selected' : '' }}>Indistinto</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-md-3">
                          
                      <div class="form-group">
                        <label for="profesion">Profesión *</label>
                        <input id="profesion" type="text" class="form-control" title="Nombre"  name="profesion" autocomplete="off" placeholder="Ingrese Nacionalidad" required="" value="{{ $enterprise->legal->profesion }}">
                      </div>
                    </div>


                    <div class="col-md-3">
                          
                      <div class="form-group">
                        <label for="puesto">Puesto Actual *</label>
                        <input id="puesto" type="text" class="form-control" title="Nombre"  name="puesto" autocomplete="off" placeholder="Ingrese Nacionalidad" required="" value="{{ $enterprise->legal->puesto }}">
                      </div>
                    </div>


                </div> 
                <hr>
                <div class="row">
                    <div class="col">
                      <h5>
                        Datos Legales
                      </h5>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-md-3">
                          
                      <div class="form-group">
                        <label for="notario">Notario </label>
                        <input id="notario" type="text" class="form-control" title="Nombre" name="notario" autocomplete="off" placeholder="Ingrese" required="" value="{{ $enterprise->legal->notario }}">
                      </div>
                    </div>

                    <div class="col-md-3">
                          
                      <div class="form-group">
                        <label for="nombramiento">Fecha Nombramiento </label>
                        <input id="nombramiento" type="date" class="form-control" title="Nombre" name="nombramiento" autocomplete="off" placeholder="Ingrese" required="" value="{{ $enterprise->legal->nombramiento }}">
                      </div>
                    </div>

                    <div class="col-md-3">
                          
                      <div class="form-group">
                        <label for="numero_registro">Número </label>
                        <input id="numero_registro" type="text" class="form-control" title="Nombre" name="numero_registro" autocomplete="off" placeholder="Ingrese" required="" value="{{ $enterprise->legal->numero_registro }}">
                      </div>
                    </div>

                    <div class="col-md-3">
                          
                      <div class="form-group">
                        <label for="folio">Folio  </label>
                        <input id="folio" type="text" class="form-control" title="Nombre" name="folio" autocomplete="off" placeholder="Ingrese" required="" value="{{ $enterprise->legal->folio }}">
                      </div>
                    </div>

                    <div class="col-md-3">
                          
                      <div class="form-group">
                        <label for="libro">Libro</label>
                        <input id="libro" type="text" class="form-control" title="Nombre" name="libro" autocomplete="off" placeholder="Ingrese" required="" value="{{ $enterprise->legal->libro }}">
                      </div>
                    </div>

                  </div>
              </div>

              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default Exclude" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-warning">Actualizar</button>
              </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
