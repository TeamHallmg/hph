<div class="modal fade" id="modal-video" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Body-->
      <div class="modal-body mb-0 p-0">
        <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
          <iframe id="video-frame"  class="embed-responsive-item" src=""
            allowfullscreen></iframe>
        </div>
      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-right">
        <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
    <!--/.Content-->

  </div>
</div>