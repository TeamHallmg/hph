@extends('announcement.app')

@section('content')

@include('announcement.announcement_types.displays.display_announcement', ['announcements' => $banner])

<br>
<br>
<br>

@include('announcement.announcement_types.displays.display_announcement', ['announcements' => $carrousel])

<br>
<br>
<br>

@include('announcement.announcement_types.displays.display_announcement', ['announcements' => $mosaico])

<br>
<br>
<br>

@include('announcement.announcement_types.displays.display_announcement', ['announcements' => $tabla_anuncios])

<br>
<br>
<br>

@include('announcement.announcement_types.displays.display_announcement', ['announcements' => $cuatro_columnas])


<br>
<br>
<br>

@include('announcement.announcement_types.displays.display_announcement', ['announcements' => $por_categorias])

@endsection