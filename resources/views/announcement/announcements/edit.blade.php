@extends('layouts.app')

@section('title', 'Editar Anuncio')

@section('content')

<div class="card card-2">
	<div class="card-header">
		<h1>Editar Anuncio</h1>
	</div>
	<div class="card-body container">
			<form method="POST" action="{{ route('announcements.update',$announcement->id) }}" enctype="multipart/form-data">
				{!! method_field('PUT') !!}
				{!! csrf_field() !!}
				<div class="form-group col-12 col-md-12 d-none">
					<input class="form-control" type="text" name="view" value="{{$view}}">
					<input class="form-control" type="text" name="type" value="{{$announcement_type->id}}">
				</div>
				<div class="form-group col-12 col-md-12">
					@include('announcement.announcement_forms.form',['form' => $form])
				</div>
				@if($regions)
				<div class="form-group col-12 col-md-12">
					<label for="region">Regiones</label>
					<select name="region" id="region">
						@foreach($regions as $key => $region)
							<option value="{{$key}}">{{$region}}</option>
						@endforeach
					</select>
				</div>
				@endif
				@if($categories)
				<div class="form-group col-12 col-md-12">
					<label for="region">Categorías</label>
					<select name="region" id="region">
						@foreach($categories as $key => $category)
							<option value="{{$key}}">{{$category}}</option>
						@endforeach
					</select>
				</div>
				@endif
				<div class="form-group col-12 col-md-12">
					<div class="form-row ">
						<div class="col-12 col-md-3">
							<label for="starts">Inicia</label>
							<input class="form-control" type="date" name="starts" id="starts" placeholder="dd/mm/aaaa" value="{{$dates['starts']['date']}}">
						</div>
					</div>
					<div class="form-row my-3">
						<div class="col-12 col-md-1">
							<label for="starts_hour">Hora</label>
							<input class="form-control" type="number" name="starts_hour" id="starts_minute" min="00" max="23" value="{{$dates['starts']['hour']}}">
						</div>
						<div class="col-12 col-md-1">
							<label for="starts_minute">Minuto</label>
							<input class="form-control" type="number" name="starts_minute" id="starts_minute" min="00" max="59" value="{{$dates['starts']['minute']}}">
						</div>
					</div>
				</div>
				<div class="form-group col-12 col-md-12">
						<div class="form-row ">
							<div class="col-12 col-md-3">
								<label for="ends">Termina</label>
								<input class="form-control" type="date" name="ends" id="ends" placeholder="dd/mm/aaaa" value="{{$dates['ends']['date']}}">
							</div>
						</div>
						<div class="form-row my-3">
							<div class="col-12 col-md-1">
								<label for="ends_hour">Hora</label>
								<input class="form-control" type="number" name="ends_hour" id="ends_minute" min="00" max="23" value="{{$dates['ends']['hour']}}">
							</div>
							<div class="col-12 col-md-1">
								<label for="starts_minute">Minuto</label>
								<input class="form-control" type="number" name="ends_minute" id="ends_minute" min="00" max="59" value="{{$dates['ends']['minute']}}">
							</div>
						</div>
					</div>
				<div class="form-group col-12 col-md-12 mt-5">
					<button type="submit" class="btn btn-success card-1">Guardar Cambios</button>
				</div>
			</form>
			<form method="POST" action="{{ route('announcements.destroy',$announcement->id) }}">
				{{ csrf_field() }}
				{{ method_field('DELETE') }}
				<div class="form-group col-12 col-md-12 mt-2">
					<button type="submit" class="btn btn-danger card-1">Eliminar</button>
				</div>
			</form>
	</div>
	<div class="card-footer text-muted">
	</div>
</div>
@endsection