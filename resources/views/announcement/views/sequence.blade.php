@extends('layouts.app')

@section('content')

{!! Form::open(['url' => 'saveorder', 'method' => 'post'])!!}
{!!Form::hidden('view',$view)!!}
<table class="table table-striped">
	<thead>
		<tr>
			<th>Tipo de Anuncio</th>
			<th>Total de anuncios</th>
			<th>Orden</th>
		</tr>
	</thead>
	<tbody>
		@foreach($types as $type)
			<tr>
				{!!Form::hidden('types[]',$type['id'])!!}
				<td>{{$type['name']}}</td>
				<td></td>
				<td>{!! Form::number('order[]', $type['pos'],['class' => 'drop', 'id' => $type['pos'].$type['name'], 'min' => '1', 'max' => count($types)])!!}</td>
			</tr>
		@endforeach
		
	</tbody>
</table>
{!! Form::submit('Guardar',['class' => 'btn btn-primary'])!!}
{!! Form::close() !!}
<a href="{{url('announcements?view='.$view.'&type=all')}}">Regresar</a>

@endsection

@section('scripts')
<script>
	var ids = [];
	$(document).ready(function (){
		$('.drop').each(function (){
			var id = $(this).attr('id');
			ids.push(id);
		});
	});

	$('form').submit(function() {
		var cont = 0;
		var aux = [];
		$('.drop').each(function (){
			if(jQuery.inArray($(this).val(), aux) >= 0){
				var pos = jQuery.inArray($(this).val(), aux);
				$("#"+ids[cont]).css('border', "2px solid red");
				$("#"+ids[pos]).css('border', "2px solid red");
			}else{
				aux.push($(this).val());
				$(this).css('border', '2px solid green');
			}
			cont += 1;
		});
		console.log(ids.length,aux.length);
		if(ids.length == aux.length){
			return true;
		}else{
			return false;
		}
	});
</script>
@endsection