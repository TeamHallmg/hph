@extends('layouts.app')

@section('content')

<div class="card">
	<div class="card-header">
		<h1 class="font-weight-bold">Vistas</h1>
	</div>
	<div class="card-body">
		<table class="table table-bordered table-striped w-100" id="myTable">
			<thead>
				<tr>
					<td>#</td>
					<td>Nombre</td>
					<td>Url</td>
					<td>Acciones</td>
				</tr>
			</thead>
			<tbody>
				@if ($views->isEmpty())
					<tr class="text-center">
						<td colspan="4">No hay tipos de anuncio</td>
					</tr>
				@endif
				@foreach($views as $view)
					<tr>
						<td>{{ $loop->iteration }}</td>
						<td>{{ $view->name }}</td>
						<td><a href="{{$view->name}}">{{$view->name}}</a></td>
						<td align="center">
							<a href="{{url('views/'.$view->id)}}" class="btn btn-primary card-1">Editar</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="card-footer text-muted">
	</div>
</div>
@endsection
@section('mainScripts')
<script>
	$(document).ready( function () {
    	$('#myTable').DataTable();
	});
</script>
@endsection