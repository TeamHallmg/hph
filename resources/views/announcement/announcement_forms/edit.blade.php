@extends('announcement.app')

@section('content')

<div class="container mt-5">
	<div class="card">
		<div class="card-header">
			<h1 class="an-title">Editar: {{$announcement_type->show_name}}</h1>
		</div>
		<div class="card-body">
			<form method="POST" action="{{ route('announcement_forms.update',$announcement_type->id) }}">
				{!! method_field('PUT') !!}
				{!! csrf_field() !!}	
				
				<div class="form-group col-12 col-md-12 my-5">
					<table class="table table-bordered table-striped">
						<thead>
							<tr class="text-center">
								<th>Atributos</th>
								<th>Obligatorio</th>
							</tr>
						</thead>
						<tbody>
							@foreach($attributes as $attr)
							<tr>
								<td>
									<input class="mr-5" type="checkbox" name="attr_list[]" id="attr_list[]" value="{{$attr->id}}" @if($attr->active == '1')checked @endif autocomplete="off">
									<label class="">{{$attr->name}}</label>
								</td>
								<td align="center">
									<input type="checkbox" name="attr_list_req[]" id="attr_list_req[]" value="{{$attr->id}}" @if($attr->required == '1')checked @endif autocomplete="off">
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="form-group col-12 col-md-12">
					<button type="submit" class="btn btn-success card-1">Guardar Cambios</button>
				</div>
			</form>
		</div>
	</div>

		

</div>

@endsection
