@extends('announcement.app')

@section('content')

<div class="container mt-5">
	@foreach($form as $attr)
		@include('announcement.announcement_forms.components.'.$attr->announcementAttribute->attr_view, ['attr' => $attr->announcementAttribute, 'required' => ($attr->required)?'required':''])		
	@endforeach
</div>
@endsection