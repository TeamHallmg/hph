@foreach($form as $attr)
	@include('announcement.announcement_forms.components.'.$attr->announcementAttribute->attr_view, ['attr' => $attr->announcementAttribute, 'required' => ($attr->required)?'required':'', 'data' => isset($attr->data)? $attr->data:null])
@endforeach