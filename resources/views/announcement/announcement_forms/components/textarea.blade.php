<div class="form-group">
	<label for="{{$attr->form_name}}">{{$attr->name}}</label>
	<textarea class="form-control" id="{{$attr->form_name}}" name="{{$attr->form_name}}" value="{{$data}}" type="{{$attr->attr_view}}" {{$required}}>{{$data}}</textarea>
</div>