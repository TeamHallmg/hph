@extends('announcement.app')

@section('title', 'Tipos de Anuncios')

@section('content')
<div class="card">
	<div class="card-header">
		<h1>Tipos de Anuncios</h1>
	</div>
	<div class="card-body">
		<div class="row my-4">
			<div class="col text-center">
				<a href="{{url('announcement_types/create')}}" class="btn btn-primary card-1">Agregar</a>
			</div>
		</div>
			
		<table class="table table-striped table-bordered w-100" id="myTable">
			<thead>
				<tr>
					<th>#</th>
					<th>Nombre</th>
					<th>Nombre a Mostrar</th>
					<th>Máximo de Anuncios</th>
					<th>Acciones</th>
				</tr>
			</thead>
				@if ($types->isEmpty())
					<tr class="text-center">
						<td colspan="5">No hay tipos de anuncio</td>
					</tr>
				@endif
				@foreach ($types as $type)
					<tr>
						<td>{{ $loop->iteration }}</td>
						<td>{{ $type->name }}</td>
						<td>{{ $type->show_name }}</td>
						<td>{{ $type->max_quantity_stored }}</td>
						<td>
							<form class="form" action="{{ route('announcement_types.destroy' , $type->id)}}" method="POST">
								<div class="form-row justify-content-center">
									<a class="btn btn-primary mx-1 card-1" href="{{route('announcement_types.edit',$type->id)}}">Editar</a>
									<input name="_method" type="hidden" value="DELETE">
									{{ csrf_field() }}
									<button data-toggle="tooltip" title="Eliminar" type="submit" class="btn btn-danger mx-1 card-1">Eliminar</button>
								</div>
							</form>
						</td>
					</tr>
				@endforeach
			</table>
	</div>
	<div class="card-footer text-muted">

	</div>
</div>
@endsection
@section('mainScripts')
<script>
	$(document).ready( function () {
    	$('#myTable').DataTable();
	});
</script>
@endsection