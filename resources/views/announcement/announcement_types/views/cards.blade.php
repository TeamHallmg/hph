
<div style="margin: 2rem">
        <div class="row">
            @foreach($announcements as $announcement)
            <div class="col-md-3">
                <div class="card card-1">
                {{--<div class="card-header card-header-white-bg">
                        <h5 class="card-title text-white">-</h5>
        
                        <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        </div>
                    </div>--}}
                    <!-- /.card-header -->
                    <div class="card-body" style="min-height:550px">
                        <div class="row">
                            <div class="col-md-12 d-flex align-items-center justify-content-center">
                                <div class="col-md-12">
                                    <h2 class="title-blue">{{ isset($announcement->data['title'])?$announcement->data['title']:'' }}</h2>
                                </div>
                            </div>
                            <div class="col-md-12 d-flex align-items-center justify-content-center">
                                <img class="w-50" src="img/announcements/{{$announcement->data['image']}}" alt="{{ isset($announcement->data['title'])?$announcement->data['title']:'' }}">
                            </div>
                            <div class="col-md-12 d-flex align-items-center justify-content-center">
                                <div class="col-md-12">
                                    <p class="title-blue">{!! isset($announcement->data['description'])?$announcement->data['description']:'' !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
