@if(Auth::check() && Auth::user()->hasRolePermission('announcement_carrousel'))
    <div class="row">
        <div class="col-md-12 text-right">
            <a class="btn btn-sm btn-primary"
                href="{{ url('announcements?view=' . $display_announcements['evaluacion']['view'] . '&type=' . $display_announcements['evaluacion']['type']->id .'&section='. $section) }}">
                <i class="nav-icon fas fa-cog fa-lg"></i> Editar
            </a>
        </div>
    </div>
@endif
<div class="row d-flex justify-content-center">
    @foreach ($announcements as $announcement)
        @if ($announcement->data['section'] == $section)
            <div class="col-12 col-md-10">
                <h3>{{ isset($announcement->data['title'])?$announcement->data['title']:'' }}</h3>
				<p class="color-personalizado-marron">{!! isset($announcement->data['description'])?$announcement->data['description']:'' !!}</p>
            </div>
        @endif
    @endforeach
</div>
