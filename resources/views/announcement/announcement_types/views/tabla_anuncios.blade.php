<div class="container-fluid">
    <div class="titulo" style="font-size:8vw;color:#2B2D6C">
        {{-- <h3><strong>PRÓXIMOS EVENTOS</strong></h3> --}}
        <hr>
    </div>
    <div class="col-md-12 text-left margen-gen">
        <table class="table table-striped table-bordered eventos dt-responsive" style="margin: 0px, border: 0px; padding: 0px; width: 100%">
            <thead>
            <tr style="background-color: #ee3135; padding: 3px">
                <td style="text-align: center; color: #fff; font-size: 16px; font-weight: bold">Titulo</td>
                <td style="text-align: center; color: #fff; font-size: 16px; font-weight: bold">Información del Evento</td>
                <td style="text-align: center; color: #fff; font-size: 16px; font-weight: bold">Imagen</td>
                <td>Accion</td>
            </tr>
            </thead>
            <tbody>
			@foreach($announcements as $announcement)
            <tr id="{{$announcement->id}}">
                <td style="text-align: center; color: #ee3135; font-weight: bold">{{ $announcement->data['title'] }}</td>
                <td>{{ $announcement->data['description'] }}</td>
                <td>
                    @if(isset($announcement->data['link']))
                        <a href="http://{{$announcement->data['link']}}">
                    @endif
                        <img style="max-height: 150px" src="/img/announcements/{{ $announcement->data['image'] }}" class="img-responsive center-block">
                    @if(isset($announcement->data['link']))
                        </a>
                    @endif                    
                    <br>
                </td>
                <td><a class="btn" href="">Descargar</a></td>
            </tr>
            @endforeach
        </tbody>
        </table>
    </div>
</div>