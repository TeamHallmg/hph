@if(Auth::check() && Auth::user()->hasRolePermission('announcement_carrousel'))
<div style="position: absolute; right: 0; z-index: 5;">
	<a class="btn btn-sm text-body mt-2 mr-2" style="z-index:1; background-color:#295bff;"
		href="{{ url('announcements?view=' . $display_announcements['banner']['view'] . '&type=' . $display_announcements['banner']['type']->id.'&section='. $section)}}">
	  <i class="nav-icon fas fa-cog fa-lg"></i> Editar
	</a>
  </div>
@endif
  @foreach ($announcements as $key => $announcement)
  @if ($announcement->data['section'] == $section)
  <div class="col-12">
	<img class="w-100" src="{{ asset('img/announcements/'.$announcement->data['image']) }}" alt="{{ isset($announcement->data['title'])?$announcement->data['title']:'' }}">
	<a href="{{isset($announcement->data['link']) ? $announcement->data['link'] : '#' }}">
     </a>
  </div> 
  @endif
  @endforeach
  