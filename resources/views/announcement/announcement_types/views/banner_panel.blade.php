@foreach($announcements as $announcement)
<div style="margin: 2rem">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-2">
                    <div class="card-header card-header-white-bg">
                        <h5 class="card-title text-white">-</h5>
        
                        <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 d-flex align-items-center justify-content-center">
                                <img class="w-100" src="img/announcements/{{$announcement->data['image']}}" alt="{{ isset($announcement->data['title'])?$announcement->data['title']:'' }}">
                            </div>
                            <div class="col-md-6 d-flex align-items-center justify-content-center">
                                <div class="col-md-12">
                                    <h1 class="title-blue">{{ isset($announcement->data['title'])?$announcement->data['title']:'' }}</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach