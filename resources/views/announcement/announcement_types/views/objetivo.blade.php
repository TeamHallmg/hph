@if (Auth::check() && Auth::user()->hasRolePermission('announcement_objetivo'))
    <div class="row">
        <div class="col-md-12 text-right">
            <a class="btn btn-sm btn-primary"
                href="{{ url('announcements?view=' . $display_announcements['objetivo']['view'] . '&type=' . $display_announcements['objetivo']['type']->id .'&section='. $section) }}">
                <i class="nav-icon fas fa-cog fa-lg"></i> Editar
            </a>
        </div>
    </div>
@endif
<div class="row d-flex justify-content-center">
    @foreach ($announcements as $announcement)
        @if ($announcement->data['section'] == $section)
            <div class="col-6 col-md-4 my-2">
                    <div class="card-tile d-flex align-items-center justify-content-center h-25">
                    <h2 class="text-center text-orange h3">{{ isset($announcement->data['title'])?$announcement->data['title']:'' }}</h2>     
                    </div>
                    <div class="position-relative  overflow-hidden mx-auto">
                        <img class="card-img-top" width=" " src="{{ asset('/img/announcements/'.$announcement->data['image']) }}" alt="">
                    </div>
                    <div class="card-body text-center mt-4 h-100">
                        <p class="text-uppercase card-title-conference text-arioac" style="height: 215px;">{!! isset($announcement->data['description'])?$announcement->data['description']:'' !!}</p>
                        <hr class="hr-color">
                        <p class="card-text-conference">{!! isset($announcement->data['2description'])?$announcement->data['2description']:'' !!}</p>
                        <br>
                    </div>
            </div>
        @endif
    @endforeach
</div>
