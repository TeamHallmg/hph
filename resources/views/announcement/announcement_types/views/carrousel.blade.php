@if(Auth::check() && Auth::user()->hasRolePermission('announcement_carrousel'))
<div style="position: absolute; right: 0; z-index: 5;">
  <a href="{{ url('announcements?view=' . $display_announcements['carrousel']['view'] . '&type=' . $display_announcements['carrousel']['type']->id.'&section='. $section )}}" class="btn btn-sm text-body mt-2 mr-2" style="z-index:1; background-color:#295bff;">
    <i class="nav-icon fas fa-cog fa-lg"></i> Editar
  </a>
</div>
@endif
@foreach ($announcements as $key => $announcement)
@if ($announcement->data['section'] == $section)
<div class="col-12">
  @if ($section == 'nom035' || $section == 'cuestionario' || $section == 'Banner')
  <a href="{{isset($announcement->data['link']) ? $announcement->data['link'] : '#' }}">
    <img class="w-100" src="{{ asset('img/announcements/'.$announcement->data['image']) }}" alt="{{ isset($announcement->data['title']) ? $announcement->data['title']:'' }}">
  </a>
  @else
  <div style="cursor: pointer;" class="videoModal" data-video="{{ isset($announcement->data['link']) ? $announcement->data['link'] : '' }}">
    <img class="w-100" src="{{ asset('img/announcements/'.$announcement->data['image']) }}" alt="{{ isset($announcement->data['title']) ? $announcement->data['title']:'' }}">
  </div>
  @endif
</div> 
@endif
@endforeach