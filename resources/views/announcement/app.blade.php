@extends('layouts.app')

@section('moduleSidebar')
    @include('announcement.sidebar')
@endsection

@section('mainContent')
    @yield('content')
@endsection

@section('mainScripts')
    @yield('scripts')
@endsection