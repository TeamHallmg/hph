@extends('announcement.app')

@section('content')

<table class="table">
	<thead>
		<tr>
			<td>#</td>
			<td>Nombre</td>
			<td>Sub Categoria de</td>
			<td>Acciones</td>
		</tr>
	</thead>
	<tbody>
		@if ($categories->isEmpty())
			<tr class="text-center">
				<td colspan="4">No hay tipos de anuncio</td>
			</tr>
		@endif
		@foreach($categories as $category)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{ $category->name }}</td>
				<td>{{ optional($category->parentCategory)->name }}</td>
				<td>
					<a href="{{url('announcement_categories/'.$category->id)}}" class="btn btn-primary">Editar</a>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

@endsection