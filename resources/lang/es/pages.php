<?php

return [
    //Common
        'create'=>'Crear',
        //Home
        'hometitle'=>'Bienvenido',
        'homecontent'=>'Bienvenido, has ingresado exitosamente.',

        //Events create
        'eventcreatetitle'=>'Crear evento',
        'ecf_title'=>'Título',
        'ecf_incident'=>'Incidencia',
        'ecf_select'=>'Seleccione...',
        'ecf_block'=>'Bloqueo',
        'ecf_blockdays'=>'¿Bloquear días?',
        'ecf_users'=>'Usuarios (:number)',
        'ecf_region'=>'Horario',
        'ecf_message1' => 'Debe seleccionar al menos un usuario.',
        'ecf_message2' => 'Por favor seleccione un horiario.',
        'ecf_message3' => 'El registro fue agregado: :name.',
        'ecf_message4' => 'El registro no pudo ser agregado.',

        //Events cancel
        'eventscanceltitle'=>'Cancelar eventos',
        'eventscontent'=>'Información.',
        'ect_header'=>'Eventos activos.',
        'ect_title'=>'Título',
        'ect_start'=>'Inicio',
        'ect_end'=>'Fin',
        'ect_block'=>'Bloqueo',
        'ect_region'=>'Región',
        'ect_operations'=>'Operaciones',
        'ect_message1'=>"'¿Esta seguro de eliminar: '+title+'?'",
        'ect_message2'=>'Registro eliminado.',
        'ect_message3'=>'Error: Desconocido.',
        'ect_message4'=>'Error: No puede borrar un evento pasado.',

        //Approve incidents
        'eventscanceltitle'=>'Cancelar eventos',
        'eventscontent'=>'Información.',
        'ect_header'=>'Eventos activos.',
        'ect_title'=>'Título',
        'ect_start'=>'Inicio',
        'ect_end'=>'Fin',
        'ect_block'=>'Bloqueo',
        'ect_region'=>'Horarios',
        'ect_operations'=>'Operaciones',
        'ect_message1'=>"'¿Do you like to delete: '+title+'?'",
        'ect_message2'=>'The event was deleted.',
        'ect_message3'=>'Error: Unknown.',
        'ect_message4'=>'Error: You cannot delete a passed event.',
];
