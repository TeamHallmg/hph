<?php

return [
    'pending' => 'Pendiente',
    'processing' => 'Procesando',
    'accepted' => 'Aceptado',
    'processed' => 'Procesado',
    'rejected' => 'Rechazado',
    'complete' => 'Completado',
    'canceled' => 'Cancelado',
];