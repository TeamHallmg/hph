<?php
	
return [
	//Days of the week (short)
	'Mon' => 'Lu',
	'Tue' => 'Ma',
	'Wed' => 'Mi',
	'Thu' => 'Ju',
	'Fri' => 'Vi',
	'Sat' => 'Sa',
	'Sun' => 'Do',
	//Days of the week 
	//
	'complete' => 'completado',
	'accepted' => 'aceptado',
	'rejected' => 'rechazado',
	'pending' => 'pendiente', 
	'canceled' => 'cancelado',
	'global' => 'global',
	'time' => 'tiempo',
	'day' => 'dia',
];
?>