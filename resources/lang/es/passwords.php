<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Las contraseñas deben de ser de un mínimo de 6 carácteres y ser identicas.',
    'reset' => 'Tu contraseña ha sido restablecida correctamente!',
    'sent' => 'Hemos enviado enlace para restablecer tu contraseña a tu correo electrónico',
    'token' => 'El enlace para restablecer la contraseña es invalido.',
    'user' => "No hemos podido encontrar a un usuario vinculado esta dirección de correo electrónico.",

];
