<?php

return [
    'pending' => 'Pending',
    'processing' => 'Processing',
    'accepted' => 'Accepted',
    'processed' => 'Processed',
    'rejected' => 'Rejected',
    'complete' => 'Complete',
    'canceled' => 'Canceled',
];