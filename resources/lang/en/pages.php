<?php

return [
    //Common
        'create'=>'Create',
        //Home
        'hometitle'=>'Wellcome',
        'homecontent'=>'Wellcome, you have been logged in.',

        //Events create
        'eventcreatetitle'=>'Create an event',
        'ecf_title'=>'Title',
        'ecf_incident'=>'Incident',
        'ecf_select'=>'Select...',
        'ecf_block'=>'Block',
        'ecf_blockdays'=>'¿Do you like to block these days?',
        'ecf_users'=>'Users (:number)',
        'ecf_region'=>'Schedule',
        'ecf_message1' => 'Please select some users.',
        'ecf_message2' => 'Please select one Schedule.',
        'ecf_message3' => 'The event: :name was added.',
        'ecf_message4' => 'The event cannot be added.',

        //Events cancel
        'eventscanceltitle'=>'Cancel events',
        'eventscontent'=>'Information.',
        'ect_header'=>'Active events.',
        'ect_title'=>'Title',
        'ect_start'=>'Start',
        'ect_end'=>'End',
        'ect_block'=>'Blocked',
        'ect_region'=>'Schedule',
        'ect_operations'=>'Operations',
        'ect_message1'=>"'¿Do you like to delete: '+title+'?'",
        'ect_message2'=>'The event was deleted.',
        'ect_message3'=>'Error: Unknown.',
        'ect_message4'=>'Error: You cannot delete a passed event.',

        //Approve incidents
        'eventscanceltitle'=>'Cancel events',
        'eventscontent'=>'Information.',
        'ect_header'=>'Active events.',
        'ect_title'=>'Title',
        'ect_start'=>'Start',
        'ect_end'=>'End',
        'ect_block'=>'Blocked',
        'ect_region'=>'Schedule',
        'ect_operations'=>'Operations',
        'ect_message1'=>"'¿Do you like to delete: '+title+'?'",
        'ect_message2'=>'The event was deleted.',
        'ect_message3'=>'Error: Unknown.',
        'ect_message4'=>'Error: You cannot delete a passed event.',
];
