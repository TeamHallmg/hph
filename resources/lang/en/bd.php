<?php
	
return [
	//Days of the week (short)
	'Mon' => 'Mon',
	'Tue' => 'Tue',
	'Wed' => 'Wed',
	'Thu' => 'Thu',
	'Fri' => 'Fri',
	'Sat' => 'Sat',
	'Sun' => 'Sun',
	//Days of the week 
	//
	'complete' => 'complete',
	'accepted' => 'accepted',
	'rejected' => 'rejected',
	'pending' => 'pending', 
	'canceled' => 'canceled',
	'global' => 'global',
	'time' => 'time',
	'day' => 'day',
];
?>