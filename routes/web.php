<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('main');

/*
 *	Rutas de Importacion y de Moodle
 */
Route::group(['prefix' => 'cron'], function () {
  Route::get('import', 'CronAutomatico\CronController@mainFunction');

  Route::post('manual_import', 'CronAutomatico\CronController@manualImport');
  Route::get('process_import/{filename}', 'CronAutomatico\CronController@processImport');
});

Route::post('cambiar-contrasena/{id_user}', 'ClimaOrganizacional\UsersController@cambiar_contrasena');
Route::get('cambiar-contrasena/{id_user}', 'ClimaOrganizacional\UsersController@cambiar_contrasena');

/**
 *  ================ Announcement Routes ================ 
 */
Route::resource('announcements', 'Announcement\AnnouncementController');
Route::resource('announcement_types', 'Announcement\AnnouncementTypeController');
Route::resource('announcement_categories','Announcement\AnnouncementCategoryController');
Route::resource('views', 'Announcement\ViewController');
Route::resource('announcement_forms', 'Announcement\AnnouncementFormController');
Route::resource('announcement_types_per_view', 'Announcement\AnnouncementTypePerViewController');

Route::get('example', 'Announcement\AnnouncementController@example');
Route::post('announcements/{announcement}/activate', 'Announcement\AnnouncementController@activate');

Route::get('organigrama', 'HomeController@organigrama');
Route::get('organigrama-puestos', 'HomeController@organigramaPuestos');
Route::get('cumpleaños', 'BirthdayController@index')->name('cumpleaños');
Route::get('filosofia', 'HomeController@filosofia');

/**
 *  =============== /Announcement Routes ================
 */

 /**
 *   ================ Admin de Usuarios ================ 
 */
Route::resource('admin-de-usuarios','UserController');
Route::resource('admin-form','EmployeeFormController');
Route::get('user_analytics','UserController@analytics');
Route::post('get_user_analytics','UserController@getAnalyticsData');
Route::post('update_password', 'UserController@updatePassword');
Route::get('authMeWith/{user_email}', 'UserController@authMeWith');
Route::post('admin-de-usuarios/{id}/activate','UserController@activate')->name('activate-user');
Route::get('generar-contrato/{id}','UserController@generarContrato');
// Route::get('generate_users_random_passwords','UserController@generateUsersRandomPass');

Route::resource('direcciones','DirectionController');
Route::resource('departamentos','DepartmentController');
Route::resource('areas','AreaController');

Route::get('trashedDepartments/{id}','DepartmentController@getTrashed')->name('trashed-departments');
Route::get('trashedAreas/{id}','AreaController@trashedAreas')->name('trashed-areas');

Route::get('departments/{id}','PuestoController@departments')->name('departments');
Route::get('area/{id}','PuestoController@areas')->name('area');
Route::get('trashedJobs/{id}','PuestoController@trashedJobs')->name('trashed-jobs');
Route::get('jobpositions/{id}','PuestoController@jobpositions')->name('jobpositions');
Route::get('jobboss','PuestoController@jobboss')->name('jobboss');
Route::get('levels','PuestoController@levels')->name('levels');

Route::get('correo_de_bienvenida/{id?}', 'UserController@sendWelcomeMail')->middleware('permission:user_admin');
 /**
 *   =============== /Admin de Usuarios ================ 
 */


 
//*********************************************************************************
//rutas para la requisición de personal
//inicio
// Route::get('requisitions/{id}/autorizar', 'Requisitions\RequisitionController@autorizar');
// Route::post('rechazarRequi', 'Requisitions\RequisitionController@rechazar');

// Route::resource('requisitions', 'Requisitions\RequisitionController');
//fin
//*********************************************************************************

//*********************************************************************************
//rutas para vacntes de personal y todos los elemetos necesarios
//vacantes - reclutadores
//inicio
// Route::get('vacantes/showExt/{id}', 'Vacantes\VacantesController@showExt');
// Route::get('vacantes/administrar', 'Vacantes\VacantesController@administrar');
// Route::get('vacantes/reclutador', 'Vacantes\VacantesController@reclutador');
// Route::get('vacantes/recruiter/{id}', 'Vacantes\VacantesController@recruiter');
// Route::post('vacantes/addRecruiter', 'Vacantes\VacantesController@addRecruiter');
// Route::get('vacantes/{id}/closeVacancie', 'Vacantes\VacantesController@closeVacancie');
// Route::get('vacantes/closedVacancies', 'Vacantes\VacantesController@closedVacancies');

// Route::get('postulante/{id}/crear', 'Vacantes\PostulanteController@crear');
// Route::get('postulante_cerrada/{id}', 'Vacantes\PostulanteController@showClosed');
// Route::get('encuesta/{id}', 'Vacantes\PostulanteController@encuesta');

// Route::resources([
//   'vacantes' => 'Vacantes\VacantesController',
//   'reclutador' => 'Vacantes\ReclutadorController',
//   'postulante' => 'Vacantes\PostulanteController'
// ]);
 
//fin
//*********************************************************************************

//*********************************************************************************
//rutas para el perfil de personal
//inicio
Route::get('profile/{u}/{p}/{v}/consulta', 'Profile\ProfileController@consulta');
Route::get('profile/{p}/{v}/generar', 'Profile\ProfileController@generar');
Route::post('createBasica', 'Profile\ProfileController@createBasica');
Route::post('createComplementaria', 'Profile\ProfileController@createComplementaria');
Route::post('createRegistro', 'Profile\ProfileController@createRegistro');
Route::put('updateRegistro', 'Profile\ProfileController@updateRegistro');
Route::post('createSalud', 'Profile\ProfileController@createSalud');
Route::put('updateSalud', 'Profile\ProfileController@updateSalud');
Route::post('createBeneficiarios', 'Profile\ProfileController@createBeneficiarios');
Route::put('updateBeneficiarios', 'Profile\ProfileController@updateBeneficiarios');

Route::get('curriculum/{idVi?}', 'Profile\ProfileController@curriculum');
Route::get('profile/{id}/{idVi}/externo', 'Profile\ProfileController@externo');
Route::get('profile/{id}/showExternal', 'Profile\ProfileController@showExternal');
Route::get('listsprofile', 'Profile\ProfileController@listsprofile');

Route::resource('profile', 'Profile\ProfileController');
//fin
//*********************************************************************************

/********************************************************************************** */
//rutas para el listado dependiente de estados y municipios
//inicio ***************
Route::get('listStates/{id}', 'Entidades\EntidadesController@listadoMunicipios');
 
//fin

//rutas para el puestos de la empresa
//inicio ***************
Route::resource('jobPosition', 'JobPositionController');
Route::get('get_jobposition_data/{id?}', 'JobPositionController@getJobPositionData');
//Se agrega esta ruta, ya que es una consulta a la tabla puestos y obtener
//el beneficio del puesto de la persona logueada
Route::get('beneficios', 'JobPositionController@beneficios');
//fin



//*************************************************************************
//rutas para el modulo de Vacaciones
//inicio ***************
// Route::group(['prefix' => 'common', 'as' => 'common.'], function () {
//     Route::get('balances', 'EmployeeController@balances')->name('balances');
//     Route::get('inactive', 'Vacations\PleaController@getInactive')->name('plea.inactive');
//     Route::resource('plea', 'Vacations\PleaController');
// });

// Route::group(['prefix' => 'super', 'as' => 'super.'], function () {
//     Route::get('approve/{request}', 'Vacations\SuperController@approve')->name('approve');
//     Route::get('eventstable', 'Vacations\EventsController@table');
//     Route::post('requests', 'Vacations\SuperController@requests')->name('requests');
//     Route::post('incidents', 'Vacations\SuperController@incidents');
//     Route::post('incidents/{id}', 'Vacations\SuperController@update');
//     Route::resource('events', 'Vacations\EventsController');
//     Route::get('userstable', 'UsersController@table');
//     Route::get('users/{id}/add', 'UsersController@addBenefit');
//     Route::post('users/{id}/add', 'UsersController@setBenefit');
//     Route::resource('users', 'UsersController');
//     Route::get('excel/requests', 'Export\ExcelController@requests');
//     Route::get('excel/incidents', 'Export\ExcelController@incidents');
//     Route::get('view/incidents', 'Vacations\AdminController@incidents');
//     Route::get('view/requests', 'Vacations\AdminController@requests')->name('view.requests');
//     Route::get('tableincidents', 'Vacations\AdminController@tableIncidents');
//     Route::post('tableincidents', 'Vacations\AdminController@tableIncidents');
//     Route::get('tablerequests', 'Vacations\AdminController@tableRequests');
//     Route::post('tablerequests', 'Vacations\AdminController@tableRequests'); 
// });
 
//fin

/* Nom035 */
Route::group(['prefix' => 'nom035'], function () {
    Route::get('informe', 'Nom035\EvaluadosController@informe');
    Route::get('reporte-grafico', 'Nom035\EvaluadosController@reporte_grafico');
    Route::get('reportes', 'Nom035\ReporteGraficoController@index');
    Route::post('reportes', 'Nom035\ReporteGraficoController@post')->name('reports.post');
    Route::get('reporte_tabla', 'Nom035\ReporteGraficoController@reporte_tabla');
    Route::post('reporte_tabla', 'Nom035\ReporteGraficoController@post_tabla')->name('reports.post_tabla');
    Route::get('reporte_nom', 'Nom035\ResultadoNormaGraficaController@reporte_nom');
    Route::post('reporte_nom', 'Nom035\ResultadoNormaGraficaController@reporte');    

    Route::get('tablero/{calificacion}', 'Nom035\ResultadoNormaGraficaController@tablero_calificacion');
    Route::post('/tablero/{calificacion}', 'Nom035\ResultadoNormaGraficaController@tablero_calificacion_post');

    // Route::post('tablero/tablero_calificacion_post', 'Nom035\ResultadoNormaGraficaController@tablero_calificacion_post');

    Route::post('reporte_norma_grafica', 'Nom035\ResultadoNormaGraficaController@reporte');
    Route::post('reporte_norma_grafica_dominio', 'Nom035\ResultadoNormaGraficaController@reporte_norma_grafica_dominio');
    Route::post('reporte_nom_graf', 'Nom035\ReporteGraficoController@reporte_nom_graf')->name('reporte_nom_graf');
    Route::resource('evaluations', 'Nom035\Admin\EvaluationController');
    Route::resource('periodos', 'Nom035\Admin\PeriodosController');
    //Route::resource('permissions', 'Nom035\Admin\MultiPermissionsController');
    Route::resource('permissions', 'Nom035\Admin\PermissionsController');
    Route::resource('borrar-periodo', 'Nom035\Admin\PeriodosController@destroy');
    Route::resource('factores', 'Nom035\Admin\FactoresController');
    Route::resource('preguntas', 'Nom035\Admin\PreguntasController');
    Route::get('exportar-resultados/{id_periodo}/{grupo_area}/{nivel_puesto}/{departamento}/{puesto}', 'Nom035\EvaluadosController@exportar_resultados');

    Route::get('plan_individual', 'Nom035\Admin\PlanAccionController@indexPersonas');
    Route::post('plan_individual', 'Nom035\Admin\PlanAccionController@indexPersonas');
    Route::get('plan_individual/{periodo}/{user}/create', 'Nom035\Admin\PlanAccionController@plan_individual');
    Route::post('plan_individual/{periodo}/{user}', 'Nom035\Admin\PlanAccionController@plan_individual_store');
    Route::get('plan_individual/{id}/edit', 'Nom035\Admin\PlanAccionController@plan_individual_edit');
    Route::put('plan_individual/{id}', 'Nom035\Admin\PlanAccionController@plan_individual_update');
    Route::post('plan-individual-close', 'Nom035\Admin\PlanAccionController@plan_individual_close');

    Route::get('plan_accion', 'Nom035\Admin\PlanAccionController@indexAccion');
    Route::post('plan_accion', 'Nom035\Admin\PlanAccionController@indexAccion');
    Route::get('plan_accion/create/{period_id}', 'Nom035\Admin\PlanAccionController@createAccion');
    Route::post('plan_accion/create', 'Nom035\Admin\PlanAccionController@storeAccion');
    Route::get('plan_accion/{id}', 'Nom035\Admin\PlanAccionController@editAccion');
    Route::put('plan_accion/{id}', 'Nom035\Admin\PlanAccionController@updateAccion');

    Route::get('exportar-trabajadores-con-traumas/{period_id}', 'Nom035\Admin\PlanAccionController@exportPersonas');
    Route::get('dbseed', 'Nom035\Admin\Nom035DBSeederController@index');
    Route::post('cambiar-contrasena', 'Nom035\Admin\PeriodosController@cambiar_contrasena');
    //});
    /*
    * Fin de ruta para administrador
    */

    // Route::get('avances', 'Nom035\EvaluadosController@avances');
    // Route::post('avances', 'Nom035\EvaluadosController@avances');
    Route::get('avances', 'Nom035\Reportes\ReportesV2Controller@avances');
    Route::post('avances', 'Nom035\Reportes\ReportesV2Controller@avances');
    Route::get('reporte_general', 'Nom035\Reportes\ReportesV2Controller@reporte_general');



    Route::get('evaluaciones', 'Nom035\EvaluadosController@evaluaciones');
    Route::post('evaluaciones', 'Nom035\EvaluadosController@evaluaciones');
    Route::post('evaluacion/{cuestionario}', 'Nom035\EvaluadosController@formatoEvaluacion');
    Route::post('guardar-respuesta', 'Nom035\EvaluadosController@guardar_respuesta');
    Route::post('guardar-respuestas', 'Nom035\EvaluadosController@guardar_respuestas');
    Route::get('que-es', 'Nom035\EvaluadosController@que_es_clima_organizacional');
    Route::get('politicas', 'Nom035\EvaluadosController@politicas');
    Route::get('politicas-index', 'Nom035\EvaluadosController@politicas_index');
    Route::get('politicas-create/{id?}', 'Nom035\EvaluadosController@politicas_create');
    Route::post('politicas-store', 'Nom035\EvaluadosController@politicas_store');
    Route::get('usuarios-verificados', 'Nom035\EvaluadosController@indexVerification');

    Route::resource('grupos_areas', 'Nom035\Groups\GroupAreasController', ['parameters' => [
        'grupos_areas' => 'group'
    ]]);
    Route::resource('grupos_departamentos', 'Nom035\Groups\GroupDepartmentsController', ['parameters' => [
        'grupos_departamentos' => 'group'
    ]]);
    Route::resource('grupos_puestos', 'Nom035\Groups\GroupJobPositionsController', ['parameters' => [
        'grupos_puestos' => 'group'
    ]]);

    Route::get('import', 'Nom035\Admin\ImportController@index');
    // Route::post('import', 'Nom035\Admin\ImportController@index');
    Route::post('announcement_policy_agreement', 'Announcement\AnnouncementController@policyAgreement');
    Route::post('delete_policy_agreement', 'Announcement\AnnouncementController@deletePolicyAgreement');
});

// Clima Organizacional
Route::group(['prefix' => 'clima-organizacional'], function (){
    Route::get('reporte-grafico', 'ClimaOrganizacional\EvaluadosController@reporte_grafico');
    Route::post('reporte-grafico', 'ClimaOrganizacional\EvaluadosController@reporte_grafico');
    Route::get('heat-report', 'ClimaOrganizacional\ReporteGraficoClimaController@heat_report');
    Route::post('heat-report', 'ClimaOrganizacional\ReporteGraficoClimaController@heat_report');
    Route::get('heat-report-by-regions', 'ClimaOrganizacional\ReporteGraficoClimaController@heat_report_by_regions');
    Route::post('heat-report-by-regions', 'ClimaOrganizacional\ReporteGraficoClimaController@heat_report_by_regions');
    Route::get('heat-report-by-supervisor', 'ClimaOrganizacional\ReporteGraficoClimaController@heat_report_by_supervisor');
    Route::post('heat-report-by-supervisor', 'ClimaOrganizacional\ReporteGraficoClimaController@heat_report_by_supervisor');
    Route::get('heat-report-by-gerente', 'ClimaOrganizacional\ReporteGraficoClimaController@heat_report_by_gerente');
    Route::post('heat-report-by-gerente', 'ClimaOrganizacional\ReporteGraficoClimaController@heat_report_by_gerente');
    Route::get('heat-report-by-areas-groups', 'ClimaOrganizacional\ReporteGraficoClimaController@heat_report_by_areas_groups');
    Route::post('heat-report-by-areas-groups', 'ClimaOrganizacional\ReporteGraficoClimaController@heat_report_by_areas_groups');
    Route::get('reportes', 'ClimaOrganizacional\ReporteGraficoClimaController@index');
    Route::post('reportes', 'ClimaOrganizacional\ReporteGraficoClimaController@post')->name('clima.reports.post');
    Route::resource('periodos', 'ClimaOrganizacional\Admin\PeriodosController');
    Route::resource('permissions', 'ClimaOrganizacional\Admin\PermissionsController');
    Route::resource('borrar-periodo', 'ClimaOrganizacional\Admin\PeriodosController@destroy');
    Route::resource('periodos', 'ClimaOrganizacional\Admin\PeriodosController');
    Route::resource('factores', 'ClimaOrganizacional\Admin\FactoresController');
    Route::resource('preguntas', 'ClimaOrganizacional\Admin\PreguntasController');
    Route::get('exportar-resultados/{id_periodo}/{grupo_departamento}/{grupo_area}/{grupo_puesto}/{direccion}/{departamento}/{area}/{puesto}', 'ClimaOrganizacional\EvaluadosController@exportar_resultados');
    Route::post('exportar-resultados-clima', 'ClimaOrganizacional\ReportesController@exportar_resultados_clima');
    Route::post('exportar-resultados-clima-confidencialidad', 'ClimaOrganizacional\ReportesController@exportar_resultados_clima_confidencialidad');
    Route::resource('etiquetas', 'ClimaOrganizacional\Admin\EtiquetasController');
    Route::post('crear-etiqueta', 'ClimaOrganizacional\Admin\EtiquetasController@create');
    Route::post('editar-etiqueta', 'ClimaOrganizacional\Admin\EtiquetasController@edit');
    Route::get('borrar-etiqueta/{id}', 'ClimaOrganizacional\Admin\EtiquetasController@destroy');
    Route::post('borrar-etiqueta', 'ClimaOrganizacional\Admin\EtiquetasController@destroy');
    //});
    /*
    * Fin de ruta para administrador
    */
    Route::get('avances', 'ClimaOrganizacional\EvaluadosController@avances');
    Route::post('avances', 'ClimaOrganizacional\EvaluadosController@avances');
    Route::get('evaluaciones', 'ClimaOrganizacional\EvaluadosController@evaluaciones');
    Route::get('evaluacion', 'ClimaOrganizacional\EvaluadosController@formatoEvaluacion');
    Route::post('guardar-respuesta', 'ClimaOrganizacional\EvaluadosController@guardar_respuesta');
    Route::post('guardar-respuestas', 'ClimaOrganizacional\EvaluadosController@guardar_respuestas');
    Route::get('que-es', 'ClimaOrganizacional\EvaluadosController@que_es_clima_organizacional');  

    Route::resource('grupos_areas', 'ClimaOrganizacional\Groups\GroupAreasController');
    Route::resource('grupos_departamentos', 'ClimaOrganizacional\Groups\GroupDepartmentsController');
    Route::resource('grupos_puestos', 'ClimaOrganizacional\Groups\GroupJobPositionsController');
    Route::get('lista-empleados', 'ClimaOrganizacional\UsersController@lista_usuarios');
	
	   
    Route::get('invitaciones-masivas', 'ClimaOrganizacional\InvitacionesMasivasController@invitaciones_masivas');
    Route::post('invitaciones-masivas', 'ClimaOrganizacional\InvitacionesMasivasController@invitaciones_masivas');
    // Route::post('send-invitaciones-masivas', 'ClimaOrganizacional\InvitacionesMasivasController@send_invitaciones_masivas');
    // Route::post('send-invitaciones-test', 'ClimaOrganizacional\InvitacionesMasivasController@send_invitaciones_test');
	
    Route::get('plan-correos', 'ClimaOrganizacional\InvitacionesMasivasController@plan_correos');
    Route::get('encuestas','ClimaOrganizacional\EvaluadosController@encuestas');

    
    Route::get('plan-correos/crear', 'ClimaOrganizacional\InvitacionesMasivasController@planes_envio_crear');
    Route::post('plan-correos/crear', 'ClimaOrganizacional\InvitacionesMasivasController@planes_envio_crear');
    
    Route::get('ver-plan-correo/{id}', 'ClimaOrganizacional\InvitacionesMasivasController@planes_envio_ver');
    
       
    Route::post('send-invitaciones-masivas', 'ClimaOrganizacional\InvitacionesMasivasController@send_invitaciones_masivas');
    Route::post('send-invitaciones-test', 'ClimaOrganizacional\InvitacionesMasivasController@send_invitaciones_test');

});
// Fin Clima Organizacional

// Cuestionarios
Route::group(['prefix' => 'cuestionarios'], function (){
    Route::resource('periodos', 'Cuestionarios\Admin\PeriodosController');
    Route::resource('borrar-periodo', 'Cuestionarios\Admin\PeriodosController@destroy');
    Route::resource('periodos', 'Cuestionarios\Admin\PeriodosController');
    Route::resource('preguntas', 'Cuestionarios\Admin\PreguntasController');
    Route::resource('etiquetas', 'Cuestionarios\Admin\EtiquetasController');
    Route::post('crear-etiqueta', 'Cuestionarios\Admin\EtiquetasController@create');
    Route::post('editar-etiqueta', 'Cuestionarios\Admin\EtiquetasController@edit');
    Route::get('borrar-etiqueta/{id}', 'Cuestionarios\Admin\EtiquetasController@destroy');
    Route::post('borrar-etiqueta', 'Cuestionarios\Admin\EtiquetasController@destroy');
    //});
    /*
    * Fin de ruta para administrador
    */
    Route::get('avances', 'Cuestionarios\EvaluadosController@avances');
    Route::get('evaluaciones', 'Cuestionarios\EvaluadosController@evaluaciones');
    Route::get('evaluacion', 'Cuestionarios\EvaluadosController@formatoEvaluacion');
    Route::get('que-es', 'Cuestionarios\EvaluadosController@que_es_cuestionarios');  

    Route::get('lista-empleados', 'Cuestionarios\Admin\UsersController@lista_usuarios');
 
    Route::get('invitaciones-masivas', 'Cuestionarios\InvitacionesMasivasController@invitaciones_masivas');
    Route::post('invitaciones-masivas', 'Cuestionarios\InvitacionesMasivasController@invitaciones_masivas');
    Route::post('send-invitaciones-masivas', 'Cuestionarios\InvitacionesMasivasController@send_invitaciones_masivas');
    Route::post('send-invitaciones-test', 'Cuestionarios\InvitacionesMasivasController@send_invitaciones_test');
});
// Fin Cuestionarios

// Nps
Route::group(['prefix' => 'nps'], function (){
    Route::resource('periodos', 'Nps\Admin\PeriodosController');
    Route::resource('permissions', 'Nps\Admin\PermissionsController');
    Route::resource('borrar-periodo', 'Nps\Admin\PeriodosController@destroy');
    Route::resource('periodos', 'Nps\Admin\PeriodosController');
    Route::resource('preguntas', 'Nps\Admin\PreguntasController');
    Route::resource('etiquetas', 'Nps\Admin\EtiquetasController');
    Route::post('crear-etiqueta', 'Nps\Admin\EtiquetasController@create');
    Route::post('editar-etiqueta', 'Nps\Admin\EtiquetasController@edit');
    Route::get('borrar-etiqueta/{id}', 'Nps\Admin\EtiquetasController@destroy');
    Route::post('borrar-etiqueta', 'Nps\Admin\EtiquetasController@destroy');
    Route::get('encuestas','Nps\EvaluadosController@encuestas');  
    Route::post('exportar-resultados-nps', 'Nps\ReportesController@exportarEncuestaExcel');
    //});
    /*
    * Fin de ruta para administrador
    */
    Route::get('avances', 'Nps\EvaluadosController@avances');
    Route::post('avances', 'Nps\EvaluadosController@avances');
    Route::get('evaluaciones', 'Nps\EvaluadosController@evaluaciones');
    Route::get('evaluacion', 'Nps\EvaluadosController@formatoEvaluacion');
    Route::post('guardar-respuesta', 'Nps\EvaluadosController@guardar_respuesta');
    Route::post('guardar-respuestas', 'Nps\EvaluadosController@guardar_respuestas');
    Route::get('que-es', 'Nps\EvaluadosController@que_es_nps');
     

    Route::get('lista-empleados', 'Nps\Admin\UsersController@lista_usuarios');    
    
    Route::get('/reporte/fidelizacion', function () {
        return view('nps.reporte_fidelizacion');
    })->name('main');
    Route::post('/exportar_respuesta', 'Nps\ReportesFidelizacionController@exportar_respuesta');
  
    Route::post('/reporte/fidelizacion', 'Nps\ReportesFidelizacionController@fidelizacion');


    Route::get('plan-correos', 'Nps\InvitacionesMasivasController@plan_correos');  
    Route::get('plan-correos/crear', 'Nps\InvitacionesMasivasController@planes_envio_crear');
    Route::post('plan-correos/crear', 'Nps\InvitacionesMasivasController@planes_envio_crear');    
    Route::get('ver-plan-correo/{id}', 'Nps\InvitacionesMasivasController@planes_envio_ver');
    
    Route::get('invitaciones-masivas', 'Nps\InvitacionesMasivasController@invitaciones_masivas');
    Route::post('invitaciones-masivas', 'Nps\InvitacionesMasivasController@invitaciones_masivas');

    Route::post('send-invitaciones-masivas', 'Nps\InvitacionesMasivasController@send_invitaciones_masivas');
    Route::post('send-invitaciones-test', 'Nps\InvitacionesMasivasController@send_invitaciones_test');

});
// Fin Nps

// Sociograma
Route::group(['prefix' => 'sociograma'], function (){
    Route::resource('periodos', 'Sociograma\Admin\PeriodosController');
    Route::resource('permissions', 'Sociograma\Admin\PermissionsController');
    Route::resource('borrar-periodo', 'Sociograma\Admin\PeriodosController@destroy');
    Route::resource('periodos', 'Sociograma\Admin\PeriodosController');
    Route::resource('preguntas', 'Sociograma\Admin\PreguntasController');
    Route::resource('etiquetas', 'Sociograma\Admin\EtiquetasController');
    Route::post('crear-etiqueta', 'Sociograma\Admin\EtiquetasController@create');
    Route::post('editar-etiqueta', 'Sociograma\Admin\EtiquetasController@edit');
    Route::get('borrar-etiqueta/{id}', 'Sociograma\Admin\EtiquetasController@destroy');
    Route::post('borrar-etiqueta', 'Sociograma\Admin\EtiquetasController@destroy');
    //});
    /*
    * Fin de ruta para administrador
    */
    Route::get('avances', 'Sociograma\EvaluadosController@avances');
    Route::post('avances', 'Sociograma\EvaluadosController@avances');
    Route::get('evaluaciones', 'Sociograma\EvaluadosController@evaluaciones');
    Route::get('evaluacion', 'Sociograma\EvaluadosController@formatoEvaluacion');
    Route::post('guardar-respuesta', 'Sociograma\EvaluadosController@guardar_respuesta');
    Route::post('guardar-respuestas', 'Sociograma\EvaluadosController@guardar_respuestas');
    Route::post('borrar-respuesta', 'Sociograma\EvaluadosController@borrar_respuesta');
    Route::get('que-es', 'Sociograma\EvaluadosController@que_es_sociograma');  

    Route::get('lista-empleados', 'Sociograma\Admin\UsersController@lista_usuarios');

    Route::get('/reporte/sociograma', function () {
        return view('sociograma.reporte_sociograma');
    })->name('main');
    
    Route::post('/reporte/sociograma', 'Sociograma\ReportesSociogramaController@sociograma');
    Route::post('/reporte/exportar_sociograma', 'Sociograma\ReportesSociogramaController@exportar_sociograma');
    
    Route::get('/reporte/sociograma-individual', function () {
        return view('sociograma.reporte_sociograma_individual');
    })->name('main');
    Route::post('/reporte/sociograma-individual', 'Sociograma\ReportesSociogramaController@sociograma_individual');
    
    Route::post('/reporte/exportar_sociograma_individual', 'Sociograma\ReportesSociogramaController@exportar_sociograma_individual');


    Route::get('/getPeriodos', 'Sociograma\ReportesSociogramaController@get_periodos');
    Route::get('/cargarEmpleadosPeriodo/{id_periodo}', 'Sociograma\ReportesSociogramaController@cargar_empleados_periodo');

    Route::get('plan-correos', 'Sociograma\InvitacionesMasivasController@plan_correos');  
    Route::get('plan-correos/crear', 'Sociograma\InvitacionesMasivasController@planes_envio_crear');
    Route::post('plan-correos/crear', 'Sociograma\InvitacionesMasivasController@planes_envio_crear');    
    Route::get('ver-plan-correo/{id}', 'Sociograma\InvitacionesMasivasController@planes_envio_ver');
 
});
// Fin Sociograma

Route::auth();
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');






Route::resource('Tipobienes', 'Bienes\TipoBienesController');

Route::resource('TipoBienesDetalles', 'Bienes\VariablesController');



Route::post('createBienes', 'Profile\ProfileController@createBienes');
Route::put('updateBienes', 'Profile\ProfileController@updateBienes');

Route::post('createContrato', 'Profile\ProfileController@createContrato');
Route::put('updateContrato', 'Profile\ProfileController@updateContrato');


Route::get('NotaEntregaBien/{profile}/{id}', 'Profile\ProfileController@entregaBienes');


/********************************************************************************** */
//rutas para codigos que nos ayuden a actualizar información y/o parchar ciertos errores
//inicio ***************
// Route::get('turnRegionIntoForeign', 'Fixes\FixingCronDataController@turnRegionIntoForeign');
 


// Route::get('crons','CronController@index');

//fin
//*************************************************************************


Route::resource('templates', 'AdminTemplate\Admin\TemplatesController');
Route::get('template/edit/{id}', 'AdminTemplate\Admin\TemplatesController@edit_template');
Route::get('template/preview/{id}', 'AdminTemplate\Admin\TemplatesController@preview_template');
Route::get('template/send_email/{id}', 'AdminTemplate\Admin\TemplatesController@send_email');
Route::get('template/duplicated/{id}', 'AdminTemplate\Admin\TemplatesController@duplicated');
Route::post('template_saved', 'AdminTemplate\Admin\TemplatesController@template_saved');
Route::post('/update_file', 'AdminTemplate\Admin\TemplatesController@update_file');

Route::post('/send_correos_masivos', 'AdminTemplate\Admin\TemplatesController@send_correos_masivos');
Route::post('/send_correos_masivos_test', 'AdminTemplate\Admin\TemplatesController@send_correos_masivos_test');

Route::post('/guardar_plan_correos', 'AdminTemplate\Admin\TemplatesController@guardar_plan_correos');
Route::post('/actualizar_plan_correos', 'AdminTemplate\Admin\TemplatesController@actualizar_plan_correos');
Route::get('/enviar_plan_correos/{id}', 'AdminTemplate\Admin\TemplatesController@enviar_plan_correos');
Route::get('/ver-plan/{id}', 'AdminTemplate\Admin\TemplatesController@ver_plan_correos');
 

Route::resource('templates-fields', 'AdminTemplate\Admin\TemplatesFieldsController');
Route::get('labels', 'AdminTemplate\Admin\TemplatesFieldsController@labels');
/********************************************************************************** */



//rutas enterprise
Route::resource('enterprise', 'EnterpriseController');

// rutas para la entrevista de salida
// rutas para la entrevista de salida
Route::resource('cuestionario-salida', 'CuestionarioSalida\CuestionarioSalidaController');

Route::get('/cuestionario-graficos/reporte', function () { 
    return view('cuestionario-salida.graficos.reporte_general');
})->name('main');

Route::post('cuestionario-salida/reporte', 'CuestionarioSalida\CuestionarioGraficosController@reporte');

Route::get('cuestionario-salida/create', 'CuestionarioSalida\CuestionarioSalidaController@create')->name('cuestionario-salida.create');
Route::post('cuestionario-salida/updateEstatus', 'CuestionarioSalida\CuestionarioSalidaController@updateEstatus')->name('cuestionario-salida.updateEstatus');
Route::post('cuestionario-salida/updateDoc', 'CuestionarioSalida\CuestionarioSalidaController@updateDoc')->name('cuestionario-salida.updateDoc');
Route::get('cuestionario-salida/pdf/{id}', 'CuestionarioSalida\CuestionarioSalidaController@pdf')->name('cuestionario-salida.pdf');
Route::get('cuestionario-graficos/index', 'CuestionarioSalida\CuestionarioGraficosController@index')->name('cuestionario-salida.graficos.index');
Route::post('cuestionario-graficos/fechas', 'CuestionarioSalida\CuestionarioGraficosController@fechas');
Route::post('cuestionario-graficos/graficos', 'CuestionarioSalida\CuestionarioGraficosController@graficos');
