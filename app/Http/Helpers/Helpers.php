<?php

if (!function_exists('in_old_input')) {
 
    function in_old_input($old, $oldKey, $input = null, $arraKey = null, $returnValue = false){
        if(count($old) > 0){
            if($returnValue){
                return isset($old[$oldKey]) && isset($old[$oldKey][$arraKey])?$old[$oldKey][$arraKey]:$input;
            }else{
                return isset($old[$oldKey]) && isset($old[$oldKey][$arraKey]);
            }
        }else{
            if($returnValue){
                return $input;
            }else{
                return !!$input;
            }
        }
        return $returnValue?null:false;
    }
}

if (!function_exists('in_old_input_value')) {
 
    function in_old_input_value($old, $oldKey, $input = null, $valueForEmptyOld = null){
        if(count($old) > 0){
            if(isset($old[$oldKey])){
                return in_array($input, $old[$oldKey]);
            }
        }else{
            return $valueForEmptyOld;
        }
        return false;
    }
}

if (!function_exists('set_selected')) {
 
    function set_selected($key, $val, $property = null){
        if($val){
            if($property){
                return $val->$property == $key?'selected':'';
            }
            return $val == $key?'selected':'';
        }
        return '';
    }
}