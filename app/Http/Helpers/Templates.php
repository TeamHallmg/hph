<?php

namespace App\Http\Helpers;

use Exception;
use Carbon\Carbon;
use App\Models\Templates\Templates as Templat;
use App\Models\Templates\TemplatesPlansSends;

class Templates{

    protected $today;

    public function __construct(){
    }
 
    public function TiposPlantillasPorModulos($modulos){

        return Templat::
        whereHas('modulo', function($q) use($modulos){
            $q->whereCode($modulos);
        })
        ->whereStatus(1)
        ->whereHas('configemial')->get(['id','name']);
        
    }

    public function Plantilla($id){

        return Templat::find($id);
    }

    public function Planesorreos($modulos){

        return TemplatesPlansSends::whereHas('modulo', function($q) use($modulos){
            $q->whereCode($modulos);
        })->get();
        
    }

}