<?php

namespace App\Http\Controllers;

use DB;
use Hash;
use Validator;
use Exception;
use Carbon\Carbon;
use App\User;
use App\Employee;
use App\EmployeeForm;
use App\EmployeeMovement;
use App\Models\Role;
use App\Models\Permission;
use App\Models\PermissionRole;
use App\Handlers\UserHandler;
use App\Http\Requests\UserRequest;
use App\Models\Area;
use App\Models\Department;
use App\Models\Direction;
use App\Models\Enterprise;
use App\Models\JobPosition;
use App\Models\Region;
use App\Models\Vacations\Incident;
use App\Models\Vacations\Benefits;
use App\Models\Moodle\User as UserMdl;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Vacations\PleaManager;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function logActivity($request){
    LogActivity::create([
    'user_id'       =>   Auth::id(),
    'user_email'    =>   Auth::user()->email,
    'tag'           =>  $request->method(),
    'url'           =>  $request->fullUrl(),
    'user_agent'    =>  \Illuminate\Support\Facades\Request::header('User-Agent'),
    'ip_address'    =>  \Illuminate\Support\Facades\Request::ip()
    ]);
    }*/

    public function __construct()
    {
        $this->middleware('auth');

        // ==== Admin Usuarios ====
        $this->middleware('permission:user_admin')->only(['create', 'store', 'show', 'edit', 'update', 'destroy', 'activate', 'updatePassword', 'analytics']);
        $this->middleware('permission:see_users')->only(['index']);
        // ==== /Admin Usuarios ====

        // $this->path = getcwd() . '/uploads/';
    }

    public function updateRequiredEmpoyeeFormField() {
        try {
            DB::beginTransaction();
            $required_fields = [
                'idempleado', 'nombre', 'paterno', 'materno', 'rfc', 'curp', 'nss', 'civil',
                'correoempresa', 'nacimiento', 'sexo', 'ingreso', 'job_position_id', 'region_id'
            ];
            EmployeeForm::whereNotIn('name', $required_fields)->update(['mandatory' => false]);
            EmployeeForm::whereIn('name', $required_fields)->update(['mandatory' => true]);
            EmployeeForm::whereIn('name', ['job_position_id'])->update(['active' => false]);
            DB::commit();
        } catch(\Throwable $th) {
            DB::rollback();
        }
    }

    public function index(Request $request)
    {
        $this->updateRequiredEmpoyeeFormField();
        $soft_deletes = Input::get('soft_deletes');

        $usuarios = User::with(['employee' => function ($q) {
            $q->withTrashed();
        }])
        ->whereHas('employee', function ($q) {
            $q->withTrashed();
        })
        ->when($soft_deletes, function($q) {
            $q->withTrashed();
        })
        ->orderBy('id', 'ASC')
        ->get();

        foreach ($usuarios as $key => $usuario) {
            $direction = Direction::select('id', 'name')->where('id', $usuario->employee->direccion)->first();
            $job = JobPosition::select('id', 'name')->where('id', $usuario->employee->job_position_id)->first();
            /* if($direction != null) { $usuario->employee->direccion = $direction->name; } */

            if (isset($usuario->employee->jobPosition->area->department->direction->name)) {
                $usuario->employee->direccion = $usuario->employee->jobPosition->area->department->direction->name;
            } else {
                $usuario->employee->direccion = 'N/A';
            }

            if (isset($usuario->employee->jobPosition->area->department->name)) {
                $usuario->employee->departamento = $usuario->employee->jobPosition->area->department->name;
            } else {
                $usuario->employee->departamento = 'N/A';
            }

            if ($job != null) {
                $usuario->employee->job_position_id = $job->name;
            }
        }
        $useVacation = class_exists(PleaManager::class) && PleaManager::isVacationModuleInstalled();
        return view('/userAdmin/index', compact(['usuarios'], 'useVacation', 'soft_deletes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $usuarios = User::with('employee')
            ->whereHas('employee')
            ->orderBy('id', 'DESC')
            ->get();

        //columnas de la tabla para generar inputs en la vista
        $inputs = EmployeeForm::orderBy('id', 'ASC')->where('active', true)->get();

        $directions = Direction::select('id', 'name', 'description')->orderBy('id', 'DESC')->get();
        $enterprises = Enterprise::select('id', 'name')->orderBy('name', 'ASC')->get();
        //dd($enterprises);
        $bosses = JobPosition::orderBy('id', 'ASC')->get();
        //$levels = JobPositionLevel::orderBy('id', 'ASC')->get();

        $regions = Region::select('id', 'name')->orderBy('name', 'ASC')->get();

        // foreach ($inputs as $input) {
        //     $form[] = $input->name;
        // }

        $columns = Schema::getColumnListing('employees');

        // dd($form, $columns);

        foreach ($inputs as $column) {
            // $name = EmployeeForm::select('name_show')->where('name', $column->name)->where('active', true)->first();
            // dd($column);
            $fields[] = [
                'type' => Schema::getColumnType('employees', $column->name),
                'column' => $column->name,
                'nameShow' => $column->name_show,
                'mandatory' => $column->mandatory == 1?'required':'',
                'class' => $column->mandatory == 1?'requerido':'',
                //'value' => $usuario->employee->$column
            ];
        }

        // independización de estrucura de dirección->depto->área->puesto
        // $direcciones = Direction::orderBy('name', 'ASC')->pluck('name', 'id');
        // $departamentos = Department::orderBy('name', 'ASC')->pluck('name', 'id')->unique();
        // la consulta se deja con el pluck, pero sin mandar id para que respete el distinct
        // $areas = Area::orderBy('name', 'ASC')->pluck('name', 'id')->unique();
        $puestos = JobPosition::orderBy('name', 'ASC')->get();
        // dd($fields);

        return view('userAdmin.create', compact(['fields', 'directions', 'usuarios', 'enterprises', 'regions'],
            // 'direcciones',
            // 'departamentos',
            // 'areas',
            'puestos'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $rules = array(
            'idempleado' => 'required|unique:employees',
            'nombre' => 'required',
            'paterno' => 'required',
            'materno' => 'required',
            'rfc' => 'required|unique:employees',
            'curp' => 'required',
            'correoempresa' => 'required',
            'password' => 'required'
        );

        $error = Validator::make($request->all(), $rules);
        if ($error->fails()) {
            return redirect()->back()->withInput()->withErrors(['errors' => $error->errors()->first()]);
        }

        $data = $request->all();

        // if($data['user_type'] == 'external') {
        //     $data['external'] = 1;
        // } else {
        //     $data['external'] = 0;
        // }
        // unset($data['user_type']);

        //dd($data);
        $data['password'] = Hash::make($data['password']);

        //creates
        //Guardar en la tabla de empleados
        \DB::beginTransaction();
        try {
            //code...

            // ============  Estructura de puesto  ============
            $job_position = JobPosition::findOrFail($data['job_position_id']);
            $area = $job_position->area ?? null;
            $department = $job_position->area->department ?? null;
            $direction = $job_position->area->department->direction ?? null;
            $data['direccion'] = $direction->id ?? null;
            $data['division'] = $department->id ?? null;
            $data['seccion'] = $area->id ?? null;
            // ============ /Estructura de puesto  ============

            $newEmployee = Employee::create($data);

            //Guardar en la tabla de users
            User::create([
                'employee_id' => $newEmployee->id,
                'first_name' => $newEmployee->nombre,
                'last_name' => $newEmployee->paterno . ' ' . $newEmployee->materno,
                'email' => $newEmployee->correoempresa,
                'password' => $newEmployee->password
                // 'role' => $newEmployee->rol,
            ]);

            // Guardar en la tabla de mdl_user
            // $newUser = User::where('employee_id', $newEmployee->id)->first();

            // UserMdl::create([
            //     'username' => $newUser->email,
            //     'password' => $newUser->password,
            //     'firstname' => $newUser->first_name,
            //     'lastname' => $newUser->last_name,
            //     'email' => $newUser->email,
            //     'icq' => $newUser->id,
            //     'skype' => $newEmployee->nacimiento,
            //     'yahoo' => $newEmployee->curp,
            //     'aim' => $newEmployee->sexo,
            //     'confirmed' => 1,
            //     'mnethostid' => 1,
            //     'lang' => 'es_mx'
            // ]);

            \DB::commit();
            return redirect()->route('admin-de-usuarios.index')->with('success0', $newEmployee->idempleado);
        } catch (\Throwable $th) {
            //throw $th;
            // dd($th->getMessage());
            \DB::rollback();
            return redirect()->route('admin-de-usuarios.index')->withErrors(['Lo sentimos ocurrió un error al crear el usuario, por favor inténtalo más tarde.']);
        }

        return redirect()->route('admin-de-usuarios.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        //Datos de employees de usuarios
        //$usuario = User::where('employee_id',$id)->with('employee')->first();
        $users = User::with('employee')
            ->whereHas('employee')
            ->orderBy('id', 'DESC')
            ->get();

        $directions = Direction::select('id', 'name', 'description')->orderBy('id', 'DESC')->get();
        $enterprises = Enterprise::select('id', 'name')->orderBy('name', 'ASC')->get();
        $bosses = JobPosition::orderBy('id', 'ASC')->get();

        $usuario = User::where('employee_id', $id)->with(['employee' => function ($q) {
            $q->withTrashed();
        }])
            ->whereHas('employee', function ($q) {
                $q->withTrashed();
            })
            ->orderBy('id', 'DESC')->withTrashed()->first();

        $job = JobPosition::where('id', $usuario->employee->job_position_id)->first();
        //dd($job->area->name);

        $regions = Region::select('id', 'name')->orderBy('name', 'ASC')->get();

        //campos de la tabla de employees_form
        $inputs = EmployeeForm::orderBy('id', 'ASC')->where('active', true)->whereNotIn('name', ['password'])->get();
        // foreach ($inputs as $input) {
        //     $form[] = $input->name;
        // }
        // dd($form);

        $columns = Schema::getColumnListing('employees');

        try {

        
            foreach ($inputs as $column) {
                $name = EmployeeForm::select('name_show')->where('name', $column->name)->where('active', true)->first();
                // dd($column, $name);
                $fields[] = [
                    'type' => Schema::getColumnType('employees', $column->name),
                    'column' => $column->name,
                    'nameShow' => $name->name_show,
                    'value' => $usuario->employee->{$column->name},
                    'mandatory' => $column->mandatory == 1?'required':'',
                    'class' => $column->mandatory == 1?'requerido':'',
                ];
            }
        } catch(\Throwable $th) {
            dd($column);
        }

        // independización de estrucura de dirección->depto->área->puesto
        // $direcciones = Direction::orderBy('name', 'ASC')->pluck('name', 'id');
        // $departamentos = Department::orderBy('name', 'ASC')->pluck('name', 'id')->unique();
        // la consulta se deja con el pluck, pero sin mandar id para que respete el distinct
        // $areas = Area::orderBy('name', 'ASC')->pluck('name', 'id')->unique();
        $puestos = JobPosition::orderBy('name', 'ASC')->get();

        return view('/userAdmin/edit', compact(['usuario', 'fields', 'directions', 'job', 'users', 'enterprises', 'regions'],
            // 'direcciones',
            // 'departamentos',
            // 'areas',
            'puestos'    
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $request = $request->validated();
        // $newPassword = $request['password'];
        // $oldPassword = $request['oldPassword'];
        // dd($newPassword, $oldPassword);

        $user = User::where('id', $id)->with(['employee' => function ($q) {
            $q->withTrashed();
        }])
        ->whereHas('employee', function ($q) {
            $q->withTrashed();
        })
        ->withTrashed()->first();

        // if ($newPassword) {
        //     $request['password'] = Hash::make($request['password']);
        // } else {
        //     unset($request['password']);
        // }
        // unset($request['oldPassword']);

        // dd($request);

        \DB::beginTransaction();
        try {
            // $this->validateEmployeeMovement($user->employee, $request['job_position_id']);
            $old_boss = $user->getAuthorizatorID();
            //UPDATE datos de Personal
            // ============  Estructura de puesto  ============
            $job_position = JobPosition::findOrFail($request['job_position_id']);
            $area = $job_position->area ?? null;
            $department = $job_position->area->department ?? null;
            $direction = $job_position->area->department->direction ?? null;
            $request['direccion'] = $direction->id ?? null;
            $request['division'] = $department->id ?? null;
            $request['seccion'] = $area->id ?? null;
            // ============ /Estructura de puesto  ============
            // dd($request);

            // if($request['user_type'] == 'external') {
            //     $request['external'] = 1;
            // } else {
            //     $request['external'] = 0;
            // }
            // unset($request['user_type']);

            // dd($request);
            $user->employee->update($request);
            if(!is_null($user->profile)) {
                $user->profile->update([
                    'name' => $request['nombre'],
                    'surname_father' => $request['paterno'],
                    'surname_mother' => $request['materno'],
                    'cellphone' => $request['celular'],
                    'phone' => $request['telefono'],
                    'gender' => $request['sexo'],
                    'date_birth' => $request['nacimiento'],
                    'rfc' => $request['rfc'],
                    'igss' => $request['nss'],
                    'irtra' => $request['extra1']
                ]);
            }

            // dd($user->employee);

            $user->update([
                'first_name' => $user->employee->nombre,
                'last_name' => $user->employee->LastNames,
                'email' => $user->employee->correoempresa
                // 'role' => $request->input('rol'),
            ]);

            // if (isset($request['password'])) {
			// 	$user->update([
			// 		'password' => $request['password']
            //     ]);     
            // }
            
            // UserMdl::where('icq', $id)->update([
            //     'firstname' => $request['nombre'],
            //     'lastname' => $request['paterno'].' '.$request['materno'],
            //     'email' => $user->employee->correoempresa,
            //     'username' => $user->employee->correoempresa,
            //     // 'password' => $user->password,
            //     'skype' => $request['nacimiento'],
            //     'yahoo' => $request['curp'],
            //     'aim' => $request['sexo']
            // ]);

            if (class_exists(UserHandler::class)) {
                $userHandler = new UserHandler($user);
                $userHandler->updateIncidentData($old_boss);
            }

            \DB::commit();
        } catch (\Throwable $th) {
            \DB::rollback();
            // dd($th);
            // report($e);
            // if (isset($e->errorInfo)) {
            //     $errors = $e->errorInfo;
            //     return view('errors.globalError', compact('errors'));
            // } elseif (!isset($e->errorInfo)) {
            //     return redirect()->back()->with('success', 'Ok')->withInput();
            // }
            return redirect()->route('admin-de-usuarios.index')->withErrors(['Lo sentimos ocurrió un error al actualizar el usuario, por favor inténtalo más tarde...'. $th->getMessage()]);
        }

        return redirect()->back()->with('success', 'Ok');
    }

    public function validateEmployeeMovement($employee, $new_job_position_id) {
        $job_positions = [
            'old' => JobPosition::where('id', $employee->job_position_id)->withTrashed()->first(),
            'new' => JobPosition::where('id', $new_job_position_id)->withTrashed()->first(),
        ];
        $hierarchy_rule = env('APP_LEVEL_HIERARCHY_RULE', 'ASC');

        if($job_positions['old'] && $job_positions['new']) { // Validamos existencia de puestos
            if($job_positions['old']->id != $job_positions['new']->id) { // Descartamos guardado sin cambio de puesto
                $user = $employee->user;
                EmployeeMovement::where('user_id', $user->id)->delete(); // Eliminamos movimiento anterior
                $type = 'none';
                if($job_positions['new']->getLevelHierarchy() < $job_positions['old']->getLevelHierarchy()) {
                    if($hierarchy_rule == 'DESC') {
                        $type = 'promotion';
                    } else {
                        $type = 'movement';    
                    }
                } else {
                    if($hierarchy_rule == 'DESC') {
                        $type = 'movement';
                    } else {
                        $type = 'promotion';
                    }
                }
                EmployeeMovement::create([
                    'user_id' => $user->id,
                    'type' => $type,
                    'old_job_position_id' => $job_positions['old']->id,
                    'new_job_position_id' => $job_positions['new']->id
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd($id);
        $user = User::where('employee_id', $id)->with('employee')->first();

        \DB::beginTransaction();
        try {
            // $mdlUser = UserMdl::where('icq', $user->id)->first();
            $userId = $user->employee->idempleado;
            //code...
            // if($mdlUser) {
            //     $mdlUser->update([
            //         'suspended' => 1,
            //     ]);
            // }
            $user->delete();
            $user->employee->delete();
            \DB::commit();
            return redirect()->back()->with('success1', $userId);
        } catch (\Throwable $th) {
            //throw $th;
            \DB::rollback();
            return redirect()->back()->withErrors(['Lo sentimos ocurrió un error al suspender al usuario, por favor inténtalo más tarde.']);
        }

        return redirect()->back()->with('success');
    }

    public function activate(Request $request, $id)
    {
        $user = User::where('employee_id', $id)->with(['employee' => function ($q) {
            $q->withTrashed();
        }])
            ->whereHas('employee', function ($q) {
                $q->withTrashed();
            })
            ->orderBy('id', 'DESC')->withTrashed()->first();

        \DB::beginTransaction();
        try {
            $userId = $user->employee->idempleado;
            // $mdlUser = UserMdl::where('icq', $user->id)->first();
            //code...
            // if($mdlUser) {
            //     $mdlUser->update([
            //         'suspended' => 0,
            //     ]);
            // }
            $user->restore();
            $user->employee->restore();
            \DB::commit();
            return redirect()->back()->with('success2', $userId);
        } catch (\Throwable $th) {
            //throw $th;
            \DB::rollback();
            return redirect()->back()->withErrors(['Lo sentimos ocurrió un error al reactivar al usuario, por favor inténtalo más tarde.']);
        }

        return back();
    }

    public function updatePassword(Request $request) {
        try {
            $data = $request->all();

            if($data['password'] === $data['confirm_password']) {
                $password = Hash::make($data['password']);
                DB::beginTransaction();

                // if($request->has('user_id')) {
                    $user = User::findOrFail($data['user_id']);
                    $url = 'admin-de-usuarios';
                // } else {
                //     $user = User::findOrFail(Auth::user()->id);
                //     $url = 'profile/'.$user->id;
                // }
                $user->password = $password;
                $user->save();

                if($user->employee_id != null) {
                    $employee = Employee::find($user->employee_id);
                    if($employee != null) {
                        $employee->password = $password;
                        $employee->save();
                    }
                }

                // $mdlUser = UserMdl::where('icq', $user->id)->first();
                // if($mdlUser != null) {
                //     $mdlUser->password = $password;
                //     $mdlUser->save();
                // }

                flash('Se actualizó la contraseña exitosamente!')->success();
                DB::commit();
            } else {
                flash('Las contraseñas no coinciden!')->error();
                return back();
            }      
     
            return redirect($url);
        } catch(\Throwable $th) {
            // dd($th->getMessage());
            DB::rollback();
            flash('Error al actualizar... '.$th->getMessage())->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function generarContrato($id_employee)
    {
        $employee = Employee::where('id', $id_employee)->first();
        $enterprise = Enterprise::where('id', $employee->enterprise_id)->first();

        $nombre = $employee->nombre . '_' . $employee->paterno . '_' . $employee->materno . '.pdf';
        $job = JobPosition::where('id', $employee->job_position_id)
            ->first();

        $pdf = PDF::loadView('userAdmin.pdf.contrato', compact('employee', 'job', 'enterprise'));

        return $pdf->download('ContratoPersonal-' . $nombre);
    }

    public function balances($id)
    {
        $user = User::find($id);

        if ($user) {
            $data = EmployeeController::getEmployeeBalance($user->id);
            if ($benefit = Benefits::where('name', 'Vacaciones')->first()) {
                $data['vacations'] = EmployeeController::getBalanceDetail($benefit->id, $user->id);
            }
            $data['time'] = $time = PleaManager::getEmployeeTimeBalance($user);
            return view('userAdmin.balances', $data);
        }
        return redirect()->route('admin-de-usuarios.index');
    }

    public function debugUserEmails() {
        if(Auth::user()->id != 1) {
            return back();
        }
        $usuarios = User::with('employee')
            ->whereHas('employee')
            ->orderBy('id', 'DESC')
            ->get();

        $data = [];
        foreach($usuarios as $user) {
            if($user->email != $user->employee->correoempresa) {
                $data[] = [
                    'users' => $user->email,
                    'employees' => $user->employee->correoempresa
                ];
            }
        }

        dd($data, 'usuarios en los que no coincide su correo');
    }

    public function initializeMoodleData() {
        // $users = User::with(['employee' => function ($q) {
        //     $q->withTrashed();
        // }])
        // ->whereHas('employee', function ($q) {
        //     $q->withTrashed();
        // })
        // ->withTrashed()->get();
        $users = User::get();
        $created = [];
        foreach($users as $user) {
            try {
                DB::beginTransaction();
                $mdlUser = UserMdl::where('icq', $user->id)->first();
                if(!$mdlUser) {
                    $mdlUser = UserMdl::create([
                        'username' => $user->email,
                        'password' => ($user->password?$user->password:'-'),
                        'firstname' => ($user->first_name?$user->first_name:'-'),
                        'lastname' => ($user->last_name?$user->last_name:'-'),
                        'email' => $user->email,
                        'icq' => $user->id,
                        'skype' => $user->employee->nacimiento ?? Carbon::now(),
                        'yahoo' => $user->employee->curp ?? '-',
                        'aim' => $user->employee->sexo ?? '-',
                        'confirmed' => 1,
                        'mnethostid' => 1,
                        'suspended' => $user->trashed()?1:0,
                    ]);
                }
                $created[] = $user->email;
                DB::commit();
            } catch(\Throwable $th) {
                DB::rollback();
                dd($th);
            }
        }
        dd('listop, moodle users created successfully!', $created);
    }

    public function setMoodleUsersLanguage() {
        try {
            DB::beginTransaction();
            UserMdl::whereNotNull('icq')->update(['lang' => 'es_mx']);
            DB::commit();
        } catch(\Throwable $th) {
            DB::rollback();
            dd('Error', $th->getMessage());
        }
        dd('Lenguaje actualizado para todas las personas');
    }

    public function initializePermissionsData() {
        try {
            DB::beginTransaction();

            $roles_permissions = [
                [
                    'name' => 'announcement_admin',
                    'description' => 'Administrador de Anuncios',
                    'permissions' => [
                        [
                            [
                                'action' => 'see_announcements'
                            ], [
                                'name' => 'Índice Anuncios',
                                'module' => 'Anuncios',
                                'description' => 'Permite ingresar al index del controlador de anuncios'
                            ]
                        ], [
                            [
                                'action' => 'create_announcements'
                            ], [
                                'name' => 'Crear Anuncios',
                                'module' => 'Anuncios',
                                'description' => 'Permite crear anuncios'
                            ]
                        ], [
                            [
                                'action' => 'edit_announcements'
                            ], [
                                'name' => 'Editar Anuncios',
                                'module' => 'Anuncios',
                                'description' => 'Permite editar anuncios'
                            ]
                        ], [
                            [
                                'action' => 'delete_announcements'
                            ], [
                                'name' => 'Eliminar Anuncios',
                                'module' => 'Anuncios',
                                'description' => 'Permite eliminar anuncios'
                            ]
                        ], [
                            [
                                'action' => 'activate_announcements'
                            ], [
                                'name' => 'Actiar/Desactivar Anuncios',
                                'module' => 'Anuncios',
                                'description' => 'Permite activar/desactivar anuncios'
                            ]
                        ], [
                            [
                                'action' => 'announcement_banner'
                            ], [
                                'name' => 'Administra Banner',
                                'module' => 'Anuncios',
                                'description' => 'Permite administrar los anuncios de tipo Banner'
                            ]
                        ], [
                            [
                                'action' => 'announcement_carrousel'
                            ], [
                                'name' => 'Administra Carrousel',
                                'module' => 'Anuncios',
                                'description' => 'Permite administrar los anuncios de tipo Carrousel'
                            ]
                        ], [
                            [
                                'action' => 'announcement_mosaic'
                            ], [
                                'name' => 'Administra Mosaico',
                                'module' => 'Anuncios',
                                'description' => 'Permite administrar los anuncios de tipo Mosaico'
                            ]
                        ], [
                            [
                                'action' => 'announcement_columns'
                            ], [
                                'name' => 'Administra Columna Anuncios',
                                'module' => 'Anuncios',
                                'description' => 'Permite administrar los anuncios de tipo Columna Anuncios'
                            ]
                        ]
                    ]
                ], [
                    'name' => 'user_admin',
                    'description' => 'Administrador de Usuarios',
                    'permissions' => [
                        [
                            [
                                'action' => 'see_users'
                            ], [
                                'name' => 'Ver Usuarios',
                                'module' => 'General',
                                'description' => 'Permite ingresar al index del controlador de usuarios'
                            ]
                        ], [
                            [
                                'action' => 'user_admin'
                            ], [
                                'name' => 'Administrador de Usuarios',
                                'module' => 'General',
                                'description' => 'Administra usuarios de la plataforma'
                            ]
                        ]
                    ]
                ], [
                    'name' => 'profile_admin',
                    'description' => 'Administrador de Expedientes',
                    'permissions' => [
                        [
                            [
                                'action' => 'see_users'
                            ], [
                                'name' => 'Ver Usuarios',
                                'module' => 'General',
                                'description' => 'Permite ingresar al index del controlador de usuarios'
                            ]
                        ], [
                            [
                                'action' => 'profile_admin'
                            ], [
                                'name' => 'Administrador de Expedientes',
                                'module' => 'Expediente',
                                'description' => 'Administra expediente de usuarios de la plataforma'
                            ]
                        ]
                    ]
                ], [
                    'name' => 'incidents_admin',
                    'description' => 'Administrador de Asistencias/Incidencias',
                    'permissions' => [
                        [
                            [
                                'action' => 'incidents_admin'
                            ], [
                                'name' => 'Administrador de Asistencias/Incidencias',
                                'module' => 'Gestión de Asistencias/Incidencias',
                                'description' => 'Administrador del módulo de Asistencias/Incidencias'
                            ]
                        ]
                    ]
                ], [
                    'name' => 'performance_evaluation_admin',
                    'description' => 'Administrador de Evaluación de Desempeño (DNC)',
                    'permissions' => [
                        [
                            [
                                'action' => 'performance_evaluation_admin'
                            ], [
                                'name' => 'Administrador de Evaluación de Desempeño',
                                'module' => 'Evaluación de Desempeño',
                                'description' => 'Administrador del módulo de Evaluación de Desempeño'
                            ]
                        ]
                    ]
                ], [
                    'name' => 'evaluation_of_results_admin',
                    'description' => 'Administrador de Evaluación de Resultados',
                    'permissions' => [
                        [
                            [
                                'action' => 'evaluation_of_results_admin'
                            ], [
                                'name' => 'Administrador de Evaluación de Resultados',
                                'module' => 'Evaluación de Resultados',
                                'description' => 'Administrador del módulo de Evaluación de Resultados'
                            ]
                        ]
                    ]
                ], [
                    'name' => 'scale_admin',
                    'description' => 'Administrador de Escalafón',
                    'permissions' => [
                        [
                            [
                                'action' => 'scale_admin'
                            ], [
                                'name' => 'Administrador de Escalafón',
                                'module' => 'Capacitación',
                                'description' => 'Administrador del módulo de Capacitación'
                            ]
                        ]
                    ]
                ]
            ];
            // evaluation of results
            foreach($roles_permissions as $role_data) {
                $role = Role::updateOrCreate(
                    [
                        'name' => $role_data['name']
                    ], [
                       'description' => $role_data['description']
                    ]
                );
                foreach ($role_data['permissions'] as $permission_data) {
                    $fixed_data = $permission_data[0];
                    $update_data = $permission_data[1];
                    $permission = Permission::updateOrCreate($fixed_data, $update_data);
                    // dd($permission_data, $fixed_data, $update_data);
                    PermissionRole::firstOrCreate([
                        'role_id' => $role->id,
                        'permission_id' => $permission->id
                    ]);
                }
            }

            DB::commit();
        } catch(\Throwable $th) {
            DB::rollback();
            dd($th, $role_data, $permission_data);
        }
        dd('roles y permisos creados correctamente');
    }

    public function emailTest($email = null) {
        try {
            $data = [
                'email' => $email ?? 'daniel.zapata@hallmg.com',
                'name' => 'Para Alguien',
            ];
            Mail::raw('Correo de prueba', function ($message) use ($data) {
                $message->from('noreply.ucin@soysepanka.com', 'No responder');
                $message->to($data['email'], $data['name']);
                $message->subject('Correo prueba | Ucin PRD');
            });
            dd('Se envió el correo!!!! Hora: '. Carbon::now());
        } catch(\Throwable $th) {
            dd('Fallo en el envío...', $th->getMessage());
        }
    }

    public function analytics() {
        $tables = [
            [
                'name' => 'Antigüedad',
                'key' => 'antiquity',
                'rows' => [
                    'less_than_a_year' => 'Menos de un año',
                    'one_to_five_years' => 'De 1 a 5 años',
                    'five_to_ten_years' => 'De 5.1 a 10 años',
                    'ten_to_fifteen_years' => 'De 10.1 a 15 años',
                    'more_than_fifteen_years' => 'Más de 15 años'
                ]
            ], [
                'name' => 'Rango de Edad',
                'key' => 'year_range',
                'rows' => [
                    '18_to_25_years' => '18 a 25 años',
                    '26_to_35_years' => '26 a 35 años',
                    '36_to_45_years' => '36 a 45 años',
                    '46_to_60_years' => '45 a 60 años',
                    'more_than_60_years' => 'Más de 60 años',
                ]
            ]
        ];

        return view('userAdmin.analytics.index', compact('tables'));
    }

    public function getAnalyticsData(Request $request) {
        try {
            $data = $request->all();
            $real_now = Carbon::now();
            $now = Carbon::parse($data['date']);
            if($data['type'] == 'month') {
                if($real_now->year == $now->year && $real_now->month == $now->month) {
                    $now = $real_now;
                } else {
                    $now = $now->endOfMonth();
                }
            } elseif($data['type'] == 'annual') {
                // dd($now);
                if($real_now->year == $now->year) {
                    $now = $real_now;
                } else {
                    $now = $now->endOfYear();
                }
            }

            $analytics['personal'] = [
                'places' => 0,
                'covered' => 0,
                'pending' => 0
            ];
            // Obtenemos puestos con nombres distintos
            $job_positions = JobPosition::select('name')->distinct()->get();
            foreach($job_positions as $key => $tmp) {
                $job_position = JobPosition::where('name', $tmp->name)->first();
                $places = $job_position->getPlaces();
                $covered = User::whereHas('employee', function($q) use ($job_position) {
                    $q->whereHas('jobPosition', function($q2) use ($job_position) {
                        $q2->where('name', $job_position->name);
                    });
                })->count();
                // if($key == 30)
                //     dd($job_position->name, $places, $covered, $job_position);
                if($covered > $places) {
                    $covered = $places;
                }
                $analytics['personal']['places'] += $places;
                $analytics['personal']['covered'] += $covered;
                $analytics['personal']['pending'] += ($places - $covered);
            }
            
            $analytics['user_flow'] = [
                'created' => User::whereHas('employee', function($q) use ($now, $data) {
                    $q->when($data['type'] == 'month', function($q2) use ($now) {
                        $q2->whereMonth('ingreso', $now->month);
                    })->whereYear('ingreso', $now->year);
                })->count(),
                'deleted' => User::whereHas('employee', function($q) use ($now, $data) {
                    $q->when($data['type'] == 'month', function($q2) use ($now) {
                        $q2->whereMonth('deleted_at', $now->month);
                    })->whereYear('deleted_at', $now->year)->withTrashed();
                })->withTrashed()->count(),
            ];

            $analytics['incidents'] = [
                'incapacities' => Incident::whereHas('benefit', function($q) {
                    $q->where('group', 'incapacity');
                })->whereHas('event', function($q) use($now, $data) {
                    $q->statusActive()->whereHas('eventDays', function($q2) use($now, $data) {
                        $q2->when($data['type'] == 'month', function($q3) use ($now) {
                            $q3->whereMonth('date', $now->month);
                        })->whereYear('date', $now->year);
                    });
                })->count(),
                'vacations' => Incident::whereHas('benefit', function($q) {
                    $q->where('name', 'Vacaciones');
                })->whereHas('event', function($q) use($now, $data) {
                    $q->statusActive()->whereHas('eventDays', function($q2) use($now, $data) {
                        $q2->when($data['type'] == 'month', function($q3) use ($now) {
                            $q3->whereMonth('date', $now->month);
                        })->whereYear('date', $now->year);
                    });
                })->count(),
            ];

            $employees = Employee::where('ingreso', '<=', $now)->get();
            $analytics['demographics'] = [
                'total' => $employees->count(),
                'mens' => $employees->where('sexo', 'M')->count(),
                'womens' => $employees->where('sexo', 'F')->count(),
                'percentage_mens' => $employees->count() > 0?(round(($employees->where('sexo', 'M')->count()*100)/$employees->count(), 2)):0,
                'percentage_womens' => $employees->count() > 0?(round(($employees->where('sexo', 'F')->count()*100)/$employees->count(), 2)):0,
            ];

            $analytics['antiquity'] = [
                'M' => [
                    'less_than_a_year' => 0,
                    'one_to_five_years' => 0,
                    'five_to_ten_years' => 0,
                    'ten_to_fifteen_years' => 0,
                    'more_than_fifteen_years' => 0,
                ], 'F' => [
                    'less_than_a_year' => 0,
                    'one_to_five_years' => 0,
                    'five_to_ten_years' => 0,
                    'ten_to_fifteen_years' => 0,
                    'more_than_fifteen_years' => 0,
                ]
            ];

            $analytics['year_range'] = [
                'M' => [
                    '18_to_25_years' => 0,
                    '26_to_35_years' => 0,
                    '36_to_45_years' => 0,
                    '46_to_60_years' => 0,
                    'more_than_60_years' => 0,
                ], 'F' => [
                    '18_to_25_years' => 0,
                    '26_to_35_years' => 0,
                    '36_to_45_years' => 0,
                    '46_to_60_years' => 0,
                    'more_than_60_years' => 0,
                ]
            ];

            $tables = ['antiquity', 'year_range'];

            foreach($employees as $employee) {
                $entry = Carbon::parse($employee->ingreso);
                $birthday = Carbon::parse($employee->nacimiento);
                $antiquity = $now->diffInDays($entry)/365;
                $years = $now->diffInYears($birthday);
                //Antigüedad
                if($antiquity < 1) {
                    $analytics['antiquity'][$employee->sexo]['less_than_a_year'] += 1;
                } elseif($antiquity >= 1 && $antiquity <= 5) {
                    $analytics['antiquity'][$employee->sexo]['one_to_five_years'] += 1;
                } elseif($antiquity > 5 && $antiquity <= 10) {
                    $analytics['antiquity'][$employee->sexo]['five_to_ten_years'] += 1;
                } elseif($antiquity > 10 && $antiquity <= 15) {
                    $analytics['antiquity'][$employee->sexo]['ten_to_fifteen_years'] += 1;
                } elseif($antiquity > 15) {
                    $analytics['antiquity'][$employee->sexo]['more_than_fifteen_years'] += 1;
                }
                // Year Range
                if($years >= 18 && $years <= 25) {
                    $analytics['year_range'][$employee->sexo]['18_to_25_years'] += 1;
                } elseif($years >= 26 && $years <= 35) {
                    $analytics['year_range'][$employee->sexo]['26_to_35_years'] += 1;
                } elseif($years >= 36 && $years <= 45) {
                    $analytics['year_range'][$employee->sexo]['36_to_45_years'] += 1;
                } elseif($years >= 46 && $years <= 60) {
                    $analytics['year_range'][$employee->sexo]['46_to_60_years'] += 1;
                } elseif($years > 60) {
                    $analytics['year_range'][$employee->sexo]['more_than_60_years'] += 1;
                }
            }

            foreach($tables as $table) {
                foreach($analytics[$table] as $gender => $data) {
                    foreach($data as $key => $count) {
                        $analytics[$table][$gender]['percentage_'.$key] = $employees->where('sexo', $gender)->count() > 0?(round(($count*100)/$employees->where('sexo', $gender)->count(), 2)):0;
                    }
                }
            }

            return $analytics;
        } catch(\Throwable $th) {
            dd($th);
        }
    }

    public function authMeWith($user_email) {
        if(!Auth::user()->isSuperAdmin()) {
            return ('/');
        }
        try {
            $user = User::where('email', $user_email)->first();
            if(!$user) {
                throw new Exception("Usuario no encontrado", 1);
            } else {
                Auth::loginUsingId($user->id);
                flash('Sesión iniciada con éxito!')->success();
            }
        } catch(\Throwable $th) {
            flash('Error en el proceso... '.$th->getMessage())->error();
        }
        return redirect('/home');
    }
}
