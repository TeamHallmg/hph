<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\User\Region;
use App\Models\Schedule;
use App\Models\Workshift;
use App\User;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $regions = Region::orderBy('workshift')->get();
        $region5x2 = Region::where('workshift','5x2')->get();
        $region6x2 = Region::where('workshift','6x2')->get();
        $schedules6x2 = Workshift::select('schedule_id')->groupBy('schedule_id')->get()->pluck('schedule_id')->toArray();
        $schedules6x2 = Schedule::whereIn('id', $schedules6x2)->get();
        $colors = ['#76d33d', '#769ddb','#ff7e5e'];
        return view('admin.schedule.index', compact('regions','region5x2','region6x2','colors', 'schedules6x2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $days = [
            'mon' => 'Lunes',
            'tue' => 'Martes',
            'wed' => 'Miercoles',
            'thu' => 'Jueves',
            'fri' => 'Viernes',
            'sat' => 'Sabado',
            'dom' => 'Domingo',
        ];

        $schedules = Schedule::all();
        /*$data['region'] = new Region([
            'code' => '',
            'rules'=> '{"nightshift":false,"overtime":{"frecuency":60,"time":1,"incident":1,"before":true,"after":true},"delay":{"frecuency":60,"time":1,"incident":2,"before":true,"after":true}}',
        ]);
        $h = config('config.horarios')[1];
        foreach($h as $k => $v) {
            $data['schedule'][] = new Schedule([
                'day' => $k,
                'in' => $v[0],
                'out' => $v[1],
                'available' => 1
            ]);
        }
        $data['schedule'][] = new Schedule([
            'day' => 'Do',
            'in' => '00:00:00',
            'out' => '00:00:00',
            'available' => 0
        ]);*/
        return view('admin.schedule.create', compact('days', 'schedules'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $region = Region::where('code',$data['code'])->first();

        if($region) {
            flash('Error: El código ingresado ya existe.','danger');
            return redirect('admin/schedule/create');
        }

        $region = new Region();
        $region->code = $data['code'];
        $region->rules = json_encode($data['rules']);
        $region->save();

        if($data['repeat']) {
            $week = ['Lu','Ma','Mi','Ju','Vi'];
            foreach($week as $key) {
                $day = [
                    'day' => $key,
                    'in' => $data['week']['in'],
                    'out' => $data['week']['out'],
                    'available' => 1,
                ];
                $day = new Schedule($day);
                $region->schedule()->save($day);
            }
            $day = [
                'day' => 'Sa',
                'in' => $data['wSa']['in'],
                'out' => $data['wSa']['out'],
                'available' => $data['wSa']['available'],
            ];
            $day = new Schedule($day);
            $region->schedule()->save($day);

            $day = [
                'day' => 'Do',
                'in' => $data['wDo']['in'],
                'out' => $data['wDo']['out'],
                'available' => $data['wDo']['available'],
            ];
            $day = new Schedule($day);
            $region->schedule()->save($day);

        } else {
            foreach($data['d'] as $wd => $d) {
                $tmp = $d;
                $tmp['day'] = $wd;
                $tmp = new Schedule($tmp);
                $region->schedule()->save($tmp);
            }
        }
        flash('Los cambios se guardaron satisfactoriamente.','success');
        return redirect('admin/schedule');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $region = Region::where('id',$id)->firstOrFail();
        $currMonth = date('m');
        $totalDays = date('t');
        $date = mktime(0,0,0, $currMonth, 1, date('y'));
        $dayNumber = date('N', $date);
        // dd($currMonth, $totalDays, $dayNumber, $date);
        if($region->workshift == '5x2'){
            return view('admin.schedule.show5x2', compact('region', 'currMonth', 'totalDays', 'dayNumber'));
        }else{
            return view('admin.schedule.show6x2', compact('region'));
        }

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['region'] = Region::where('id',$id)->first();
        return view('admin.schedule.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $region = Region::where('id',$id)->first();

        $region->rules = json_encode($data['rules']);

        if($data['repeat']) {
            foreach($region->schedule as $day) {
                $day->in = $data['week']['in'];
                $day->out = $data['week']['out'];
                $day->available = 1;
                if($day->day == 'Sa') {
                    $day->in = $data['wSa']['in'];
                    $day->out = $data['wSa']['out'];
                    $day->available = $data['wSa']['available'];
                }
                if($day->day == 'Do') {
                    $day->in = $data['wDo']['in'];
                    $day->out = $data['wDo']['out'];
                    $day->available = $data['wDo']['available'];
                }
                $day->save();
            }
        } else {
            foreach($region->schedule as $day) {
                $day->in = $data['d'][$day->day]['in'];
                $day->out = $data['d'][$day->day]['out'];
                $day->available = $data['d'][$day->day]['available'];
                $day->save();
            }
        }
        $region->save();
        flash('Los cambios se guardaron satisfactoriamente.','success');
        return redirect('admin/schedule');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('region_id',$id)->first();
        if($user) {
            flash('Para borrar la región debe mover a la gente que pertenece a este horario a otro distinto.','warning');
            return redirect('admin/schedule');
        }
        flash('La región fue borrada correctamente','success');
        $region = Region::where('id',$id)->first();
        $region->delete();
        return redirect('admin/schedule');
    }

}
