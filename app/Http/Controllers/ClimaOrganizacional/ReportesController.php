<?php

namespace App\Http\Controllers\ClimaOrganizacional;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use DB;
use Session;

class ReportesController extends Controller
{
    
    /**
     * Create a instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Muestra la pagina de Reporte de Desempeño
     *
     * @return void
     */
    public function reporte_desempeno(){

      if (Session::has('primer_login')){

        flash('Debes cambiar tu contraseña antes de continuar a otra sección');
        return redirect('/');
      }

      $users = array();
      $id_jefe = auth()->user()->id;
      $id_periodo = 0;
      $periodos = DB::select("SELECT id, descripcion FROM periodos WHERE status = ? ORDER BY id DESC", ['Abierto']);
      
      if (count($periodos) > 0){

        $id_periodo = $periodos[0]->id;

        if (!empty($_POST['id_periodo'])){

          $id_periodo = $_POST['id_periodo'];
        }

        if (auth()->user()->role == 'admin'){

          // Obtiene los datos de todos los usuarios
          $users = DB::select('SELECT users.first_name, users.last_name, users.id, users.division, users.subdivision, users.boss_id, comentarios_desempeno_jefe.comentario, objetivos_entregables_desempeno.objetivo, evaluador_evaluado.status FROM users INNER JOIN evaluador_evaluado ON (evaluador_evaluado.id_evaluado = users.id) LEFT JOIN comentarios_desempeno_jefe ON (comentarios_desempeno_jefe.id_evaluado = users.id AND comentarios_desempeno_jefe.id_periodo = evaluador_evaluado.id_periodo) LEFT JOIN objetivos_entregables_desempeno ON (objetivos_entregables_desempeno.id_evaluado = users.id AND evaluador_evaluado.id_periodo = objetivos_entregables_desempeno.id_periodo) WHERE evaluador_evaluado.id_periodo = ? ORDER BY users.id', [$id_periodo]);
        }

        else{

          // Obtiene los datos de todos los subordinados del usuario logueado
          $users = DB::select('SELECT users.first_name, users.last_name, users.id, users.division, users.subdivision, users.boss_id, comentarios_desempeno_jefe.comentario, objetivos_entregables_desempeno.objetivo, evaluador_evaluado.status FROM users INNER JOIN evaluador_evaluado ON (evaluador_evaluado.id_evaluado = users.id) LEFT JOIN comentarios_desempeno_jefe ON (comentarios_desempeno_jefe.id_evaluado = users.id AND comentarios_desempeno_jefe.id_periodo = evaluador_evaluado.id_periodo) LEFT JOIN objetivos_entregables_desempeno ON (objetivos_entregables_desempeno.id_evaluado = users.id AND evaluador_evaluado.id_periodo = objetivos_entregables_desempeno.id_periodo) WHERE users.boss_id = ? AND users.id <> ? AND evaluador_evaluado.id_periodo = ? ORDER BY users.id', [$id_jefe, $id_jefe, $id_periodo]);
        }

        $temp_users = array();
        $jefes = array();
        $current_user = 0;

        foreach ($users as $key => $user){
      
          if ($current_user != $user->id){

            $user->boss = '';

            if (!empty($user->boss_id)){

              if (!empty($jefes[$user->boss_id])){

                $user->boss = $jefes[$user->boss_id];
              }

              else{

                $jefe = DB::select("SELECT first_name, last_name FROM users WHERE id = ?", [$user->boss_id]);

                if (count($jefe) > 0){

                  $user->boss = $jefe[0]->first_name . ' ' . $jefe[0]->last_name;
                  $jefes[$user->boss_id] = $user->boss;
                }

                else{

                  $jefes[$user->boss_id] = '';
                }
              }
            }

            $current_user = $user->id;
          }

          $temp_users[] = $user;
        }
      
        $users = $temp_users;
      }

      return view('evaluacion-desempeno/reportes/reporte-desempeno', compact('users', 'periodos', 'id_periodo'));
    }

    /**
     * Muestra el reporte
     */
    public function reporte_desempeno_individual($id_evaluado){

      if (Session::has('primer_login')){

        flash('Debes cambiar tu contraseña antes de continuar a otra sección');
        return redirect('/');
      }

      $id_jefe = auth()->user()->id;
      $user = DB::select("SELECT first_name, last_name, division, subdivision, boss_id FROM users WHERE id = ?", [$id_evaluado]);

      if (auth()->user()->role != 'admin' && $user[0]->boss_id != $id_jefe){

        return redirect('/reporte_desempeno')->with('danger','No es jefe del empleado');
      }

      $id_periodo = 0;

      if (!empty($_POST['id_periodo'])){

        $id_periodo = $_POST['id_periodo'];
      }

      else{

        $periodos = DB::select('SELECT id FROM periodos WHERE status = ? ORDER BY id DESC', ['Cerrado']);
    
        if (count($periodos) > 0){
      
          $id_periodo = $periodos[0]->id;
        }
      }

      $evaluaciones_terminadas = false;
      $evaluaciones = DB::select("SELECT id FROM evaluador_evaluado WHERE id_evaluado = ? AND id_periodo = ?", [$id_evaluado, $id_periodo]);
      $terminadas = DB::select("SELECT id FROM evaluador_evaluado WHERE id_evaluado = ? AND id_periodo = ? AND status = ?", [$id_evaluado, $id_periodo, 'Terminada']);

      if (count($evaluaciones) > 0 && count($evaluaciones) == count($terminadas)){

        $evaluaciones_terminadas = true;
      }

      $resultados = DB::select("SELECT resultados.nivel_desempeno, resultados.id_factor, resultados.comentario, resultados.id_evaluador, factores.nombre, users.boss_id FROM resultados INNER JOIN users ON (users.id = resultados.id_evaluador) INNER JOIN factores ON (factores.id = resultados.id_factor) WHERE id_evaluado = ? AND id_periodo = ? ORDER BY resultados.id_factor", [$id_evaluado, $id_periodo]);
      $comentario = DB::select("SELECT comentario FROM comentarios_desempeno_jefe WHERE id_evaluado = ? AND id_periodo = ?", [$id_evaluado, $id_periodo]);
      $objetivos_entregables_desempeno = DB::select("SELECT objetivo, accion, entregable FROM objetivos_entregables_desempeno WHERE id_evaluado = ? AND id_periodo = ?", [$id_evaluado, $id_periodo]);
      $user = DB::select("SELECT id, first_name, last_name, division, subdivision, boss_id FROM users WHERE id = ?", [$id_evaluado]);
      $jefe = DB::select("SELECT first_name, last_name, division, subdivision FROM users WHERE id = ?", [$user[0]->boss_id]);
      $meses = array(0,'ene','feb','mar','abr','may','jun','jul','ago','sep','oct','nov','dic');
      $periodos = array();
      $resultados_a_comparar = array();

      // El usuario ya tiene registros con comentarios y objetivos para el periodo actual y ademas su jefe es el que esta viendo el reporte 
      if (count($comentario) > 0 || count($objetivos_entregables_desempeno) > 0 && $user[0]->boss_id == auth()->user()->id){

        // Obtiene los periodos donde el usuario tiene registrados objetivos, acciones y entregables (excepto el periodo actual)
        $periodos_con_objetivos_entregables = DB::select("SELECT id_periodo FROM objetivos_entregables_desempeno WHERE id_evaluado = ? AND id_periodo != ? GROUP BY id_periodo", [$id_evaluado, $id_periodo]);

        // Existen periodos diferentes al actual con registros de la evaluacion de desempeño
        if (count($periodos_con_objetivos_entregables) > 0){

          // Se guardan los ids de los periodos
          foreach ($periodos_con_objetivos_entregables as $key => $value){
            
            $periodos[] = $value->id_periodo;
          }

          $periodos = implode(',', $periodos);
          
          // Se obtienen los datos de los periodos
          $periodos = DB::select("SELECT id, descripcion FROM periodos WHERE id IN ($periodos) ORDER BY id DESC");

          // Se seleccionó un periodo a comparar
          if (!empty($_POST['periodo_a_comparar'])){

            $periodo_a_comparar = $_POST['periodo_a_comparar'];
            
            // Se obtienen los datos de la evaluación del periodo a comparar
            $resultados_a_comparar = DB::select("SELECT resultados.nivel_desempeno, resultados.id_factor, resultados.comentario, resultados.id_evaluador, factores.nombre, users.boss_id FROM resultados INNER JOIN users ON (users.id = resultados.id_evaluador) INNER JOIN factores ON (factores.id = resultados.id_factor) WHERE id_evaluado = ? AND id_periodo = ? ORDER BY resultados.id_factor", [$id_evaluado, $periodo_a_comparar]);
          }
        }
      }

      return view('evaluacion-desempeno/reportes/reporte-desempeno-individual', compact('resultados', 'user', 'jefe', 'meses', 'id_periodo', 'comentario', 'objetivos_entregables_desempeno', 'evaluaciones_terminadas', 'periodos', 'resultados_a_comparar'));
    }

    /**
     * Muestra la pagina de Reporte con Graficas
     *
     * @return void
     */
    public function reporte_graficas($id_periodo){

      if (Session::has('primer_login')){

        flash('Debes cambiar tu contraseña antes de continuar a otra sección');
        return redirect('/');
      }

      if (auth()->user()->role != 'admin'){

        flash('No tiene permiso de ver la sección');
        return redirect('/');
      }

      $resultados = array();
      $periodos = DB::select("SELECT id, descripcion FROM periodos WHERE status = ? OR status = ? ORDER BY id DESC", ['Cerrado', 'Abierto']);
      
      if (count($periodos) > 0){

        if (empty($id_periodo)){

          $id_periodo = $periodos[0]->id;
        }

        // Obtiene los datos de todos los usuarios
        $resultados = DB::select("SELECT SUM(resultados.nivel_desempeno) AS total_evaluacion, COUNT(resultados.id) AS numero_evaluaciones, resultados.id_factor, factores.orden, resultados.id_evaluado, areas.id AS area, puestos.id AS puesto, niveles_puestos.id AS nivel_puesto, grupo_areas.id AS grupo_area FROM resultados INNER JOIN factores ON (factores.id = resultados.id_factor) INNER JOIN users ON (users.id = resultados.id_evaluado) LEFT JOIN puestos ON (users.subdivision = puestos.puesto) LEFT JOIN areas ON (users.division = areas.nombre) LEFT JOIN puesto_nivel_puesto ON (puestos.id = puesto_nivel_puesto.id_puesto) LEFT JOIN areas_grupo_areas ON (areas.id = areas_grupo_areas.id_areas) LEFT JOIN niveles_puestos ON (niveles_puestos.id = puesto_nivel_puesto.id_nivel_puesto) LEFT JOIN grupo_areas ON (grupo_areas.id = areas_grupo_areas.id_grupos_areas) WHERE resultados.id_periodo = ? GROUP BY resultados.id_evaluado, resultados.id_factor ORDER BY factores.orden", [$id_periodo]);
      }

      $factores = DB::select("SELECT factores.id, factores.nombre FROM factores INNER JOIN periodo_factor ON (periodo_factor.id_factor = factores.id) WHERE periodo_factor.id_periodo = ? ORDER BY factores.orden", [$id_periodo]);

      $grupo_areas = DB::select("SELECT id, nombre FROM grupo_areas WHERE id != ? ORDER BY nombre", [1]);
      $niveles_puestos = DB::select("SELECT id, nombre FROM niveles_puestos WHERE id != ? ORDER BY nombre", [1]);
      $areas = DB::select("SELECT id, nombre FROM areas WHERE nombre IS NOT NULL AND nombre != '' GROUP BY nombre ORDER BY nombre");
      $puestos = DB::select("SELECT id, puesto FROM puestos WHERE puesto IS NOT NULL AND puesto != '' ORDER BY puesto");
      return view('evaluacion-desempeno/reportes/reporte-graficas', compact('resultados', 'periodos', 'factores', 'id_periodo', 'grupo_areas', 'niveles_puestos', 'areas', 'puestos'));
    }
  
    /**
     * Muestra el reporte de todos los empleados
     */
    public function reporte_desempeno_completo($id_periodo){

      if (Session::has('primer_login')){

        flash('Debes cambiar tu contraseña antes de continuar a otra sección');
        return redirect('/');
      }

      $resultados = array();
      $factores = array();
      $ponderaciones = array();
      $niveles_puestos = $posiciones = array();
      $periodos = DB::select("SELECT id, descripcion FROM periodos WHERE status = ? OR status = ? ORDER BY id DESC", ['Cerrado', 'Abierto']);

      if (count($periodos) > 0){

        if (empty($id_periodo)){

          $id_periodo = $periodos[0]->id;
        }

        if (auth()->user()->role != 'admin'){

          $users = DB::select("SELECT id FROM users WHERE boss_id = ?", [auth()->user()->id]);

          if (count($users) == 0){

            return redirect('/')->with('danger','No tiene gente a su cargo');
          }

          $id_jefe = auth()->user()->id;
          $nombre_jefe = auth()->user()->first_name . ' ' . auth()->user()->last_name;
          $resultados = DB::select("SELECT SUM(resultados.nivel_desempeno) AS total_evaluacion, COUNT(resultados.id) AS numero_evaluaciones, resultados.id_factor, resultados.id_evaluado, puesto_nivel_puesto.id_nivel_puesto, factores.orden, comentarios_desempeno_jefe.comentario, objetivos_entregables_desempeno.objetivo FROM resultados INNER JOIN users ON (resultados.id_evaluado = users.id) LEFT JOIN puestos ON (puestos.puesto = users.subdivision) LEFT JOIN puesto_nivel_puesto ON (id_puesto = puestos.id) LEFT JOIN comentarios_desempeno_jefe ON (comentarios_desempeno_jefe.id_evaluado = resultados.id_evaluado) LEFT JOIN objetivos_entregables_desempeno ON (objetivos_entregables_desempeno.id_evaluado = resultados.id_evaluado) INNER JOIN factores ON (factores.id = resultados.id_factor) WHERE resultados.id_periodo = ? AND users.boss_id = ? GROUP BY resultados.id_evaluado, resultados.id_factor ORDER BY resultados.id_evaluado, factores.orden", [$id_periodo, auth()->user()->id]);
          $results = DB::select("SELECT SUM(nivel_desempeno) AS total_evaluacion, COUNT(resultados.id) AS numero_evaluaciones, id_evaluado, id_evaluador, first_name, last_name, division FROM resultados INNER JOIN users ON (id_evaluado = users.id) WHERE id_periodo = ? AND boss_id = ? GROUP BY id_evaluado, id_evaluador ORDER BY id_evaluado", [$id_periodo, $id_jefe]);
          $promedios_tipo_evaluacion = array();
          $autoevaluacion = 0;
          $jefe = 0;
          $colaborador = 0;
          $par = 0;
          $jefes = array();
          $evaluaciones_par = 0;
          $total_par = 0;

          foreach ($results as $key => $value){
            
            $actual_evaluado = $value->id_evaluado;

            if ($value->id_evaluador == $value->id_evaluado){

              $autoevaluacion = $value->total_evaluacion / $value->numero_evaluaciones;
            }

            else{

              if ($value->id_evaluador == $id_jefe){

                $jefe = $value->total_evaluacion / $value->numero_evaluaciones;
              }

              else{

                $jefe_evaluador = 0;

                if (!empty($jefes[$value->id_evaluador])){

                  $jefe_evaluador = $jefes[$value->id_evaluador];
                }

                else{

                  $result = DB::select("SELECT boss_id FROM users WHERE id = ?", [$value->id_evaluador]);
                  $jefe_evaluador = $result[0]->boss_id;
                  $jefes[$value->id_evaluador] = $jefe_evaluador;
                }

                if ($jefe_evaluador == $value->id_evaluado){

                  $colaborador = $value->total_evaluacion / $value->numero_evaluaciones;
                }

                else{

                  $evaluaciones_par += $value->numero_evaluaciones;
                  $total_par += $value->total_evaluacion;
                  $par = $total_par / $evaluaciones_par;
                }
              }
            }

            if (empty($results[$key + 1]) || $results[$key + 1]->id_evaluado != $actual_evaluado){

              $value->promedio_autoevaluacion = $autoevaluacion;
              $value->promedio_jefe = $jefe;
              $value->promedio_colaborador = $colaborador;
              $value->promedio_par = $par;
              $value->jefe = $nombre_jefe;
              $promedios_tipo_evaluacion[] = $value;
              $autoevaluacion = $jefe = $colaborador = $par = $evaluaciones_par = $total_par = 0;
            }
          }
        }

        else{

          $resultados = DB::select("SELECT SUM(resultados.nivel_desempeno) AS total_evaluacion, COUNT(resultados.id) AS numero_evaluaciones, resultados.id_factor, resultados.id_evaluado, puesto_nivel_puesto.id_nivel_puesto, factores.orden, comentarios_desempeno_jefe.comentario, objetivos_entregables_desempeno.objetivo FROM resultados INNER JOIN users ON (resultados.id_evaluado = users.id) LEFT JOIN puestos ON (puestos.puesto = users.subdivision) LEFT JOIN puesto_nivel_puesto ON (id_puesto = puestos.id) LEFT JOIN comentarios_desempeno_jefe ON (comentarios_desempeno_jefe.id_evaluado = resultados.id_evaluado) LEFT JOIN objetivos_entregables_desempeno ON (objetivos_entregables_desempeno.id_evaluado = resultados.id_evaluado) INNER JOIN factores ON (factores.id = resultados.id_factor) WHERE resultados.id_periodo = ? GROUP BY resultados.id_evaluado, resultados.id_factor ORDER BY resultados.id_evaluado, factores.orden", [$id_periodo]);
          $results = DB::select("SELECT SUM(nivel_desempeno) AS total_evaluacion, COUNT(resultados.id) AS numero_evaluaciones, id_evaluado, id_evaluador, first_name, last_name, division, boss_id FROM resultados INNER JOIN users ON (id_evaluado = users.id) WHERE id_periodo = ? GROUP BY id_evaluado, id_evaluador ORDER BY id_evaluado", [$id_periodo]);
          $promedios_tipo_evaluacion = array();
          $autoevaluacion = 0;
          $jefe = 0;
          $colaborador = 0;
          $par = 0;
          $jefes = array();
          $nombres_jefes = array();
          $evaluaciones_par = 0;
          $total_par = 0;

          foreach ($results as $key => $value){
            
            $actual_evaluado = $value->id_evaluado;

            if ($value->id_evaluador == $value->id_evaluado){

              $autoevaluacion = $value->total_evaluacion / $value->numero_evaluaciones;
            }

            else{

              if ($value->id_evaluador == $value->boss_id){

                $jefe = $value->total_evaluacion / $value->numero_evaluaciones;
              }

              else{

                $jefe_evaluador = 0;

                if (!empty($jefes[$value->id_evaluador])){

                  $jefe_evaluador = $jefes[$value->id_evaluador];
                }

                else{

                  $result = DB::select("SELECT boss_id, first_name, last_name FROM users WHERE id = ?", [$value->id_evaluador]);
                  $jefe_evaluador = $result[0]->boss_id;
                  $jefes[$value->id_evaluador] = $jefe_evaluador;

                  if (empty($nombres_jefes[$value->id_evaluador])){

                    $nombres_jefes[$value->id_evaluador] = $result[0]->first_name . ' ' . $result[0]->last_name;
                  }
                }

                if ($jefe_evaluador == $value->id_evaluado){

                  $colaborador = $value->total_evaluacion / $value->numero_evaluaciones;
                }

                else{

                  $evaluaciones_par += $value->numero_evaluaciones;
                  $total_par += $value->total_evaluacion;
                  $par = $total_par / $evaluaciones_par;
                }
              }
            }

            if (empty($results[$key + 1]) || $results[$key + 1]->id_evaluado != $actual_evaluado){

              if (empty($nombres_jefes[$value->id_evaluado])){

                $nombres_jefes[$value->id_evaluado] = $value->first_name . ' ' . $value->last_name;
              }

              if (!empty($nombres_jefes[$value->boss_id])){

                $value->jefe = $nombres_jefes[$value->boss_id];
              }

              else{

                $result = DB::select("SELECT first_name, last_name FROM users WHERE id = ?", [$value->boss_id]);

                if (!empty($result)){

                  $value->jefe = $result[0]->first_name . ' ' . $result[0]->last_name;
                  $nombres_jefes[$value->boss_id] = $value->jefe;
                }
              }

              $value->promedio_autoevaluacion = $autoevaluacion;
              $value->promedio_jefe = $jefe;
              $value->promedio_colaborador = $colaborador;
              $value->promedio_par = $par;
              $promedios_tipo_evaluacion[] = $value;
              $autoevaluacion = $jefe = $colaborador = $par = $evaluaciones_par = $total_par = 0;
            }
          }
        }

        $factores = DB::select("SELECT factores.id, factores.nombre, factores.orden FROM factores INNER JOIN periodo_factor ON (periodo_factor.id_factor = factores.id) WHERE periodo_factor.id_periodo = ? ORDER BY factores.orden", [$id_periodo]);
        
        // Se obtienen las ponderaciones para el periodo
        //$ponderaciones = DB::select("SELECT id_nivel_puesto, jefe, autoevaluacion, colaborador, par, nombre FROM ponderaciones INNER JOIN niveles_puestos ON (niveles_puestos.id = ponderaciones.id_nivel_puesto) WHERE id_periodo = ? ORDER BY id_nivel_puesto", [$id_periodo]);
        $ponderaciones = DB::table('ponderaciones')->where('id_periodo', $id_periodo)->select('id_nivel_puesto', 'jefe', 'autoevaluacion', 'colaborador', 'par')->orderBy('id_nivel_puesto')->get();

        // No hay ponderaciones registradas para el periodo
        if (count($ponderaciones) == 0){

          $last = DB::table('ponderaciones')->select('id_periodo')->orderBy('created_at', 'DESC')->first();

          if (!empty($last->id_periodo)){

            // Se obtienen las ultimas ponderaciones registradas
            $ponderaciones = DB::table('ponderaciones')->where('id_periodo', $last->id_periodo)->select('id_nivel_puesto', 'jefe', 'autoevaluacion', 'colaborador', 'par')->orderBy('id_nivel_puesto')->get();
          }
        }

        $niveles_puestos = DB::table('niveles_puestos')->where('mando', 1)->orderBy('id')->pluck('id');

        foreach ($niveles_puestos as $key => $value){
              
          $posiciones[$value] = $key;
        }
      }

      return view('evaluacion-desempeno/reportes/reporte-desempeno-completo', compact('resultados', 'id_periodo', 'factores', 'periodos', 'promedios_tipo_evaluacion', 'ponderaciones', 'niveles_puestos', 'posiciones'));
    }

    /**
     * Muestra el reporte de todas las evaluaciones de la evaluacion de desempeño
     */
    public function reporte_desempeno_sabana($id_periodo){

      if (Session::has('primer_login')){

        flash('Debes cambiar tu contraseña antes de continuar a otra sección');
        return redirect('/');
      }

      if (auth()->user()->role != 'admin'){

        flash('No tiene permiso de ver la sección');
        return redirect('/');
      }

      $resultados = array();
      $factores = array();
      $periodos = DB::select("SELECT id, descripcion FROM periodos WHERE status = ? OR status = ? ORDER BY id DESC", ['Cerrado', 'Abierto']);

      if (count($periodos) > 0){

        if (empty($id_periodo)){

          $id_periodo = $periodos[0]->id;
        }

        $resultados = DB::select("SELECT resultados.nivel_desempeno, resultados.id_factor, resultados.id_evaluador, resultados.id_evaluado, users.first_name, users.division, users.subdivision, users.boss_id, factores.orden FROM resultados INNER JOIN users ON (users.id = resultados.id_evaluado) INNER JOIN factores ON (factores.id = resultados.id_factor) WHERE resultados.id_periodo = ? ORDER BY resultados.id_evaluado, resultados.id_evaluador, factores.orden", [$id_periodo]); 
        $factores = DB::select("SELECT factores.id, factores.nombre, factores.orden FROM factores INNER JOIN periodo_factor ON (periodo_factor.id_factor = factores.id) WHERE periodo_factor.id_periodo = ? ORDER BY factores.orden", [$id_periodo]);
        $users = DB::table('users')->pluck('first_name', 'id');
        $jefes = DB::table('users')->pluck('boss_id', 'id');
        $temp_results = array();

        foreach ($resultados as $key => $resultado){
          
          if (!empty($users[$resultado->id_evaluador])){

            $resultado->evaluador = $users[$resultado->id_evaluador];
            $resultado->id_jefe_evaluador = $jefes[$resultado->id_evaluador];
          }

          $temp_results[] = $resultado;
        }

        $resultados = $temp_results;
      }

      return view('evaluacion-desempeno/reportes/reporte-desempeno-sabana', compact('resultados', 'id_periodo', 'factores', 'periodos'));
    }

    /**
     * Guarda los comentarios adicionales y objetivos entregables del jefe a su colaborador
     */
    public function guardar_comentarios_jefe(){

      $id_jefe = auth()->user()->id;
      $id_evaluado = $_POST['id_evaluado'];
      $id_periodo = $_POST['id_periodo'];
      DB::delete("DELETE FROM objetivos_entregables_desempeno WHERE id_evaluado = ? AND id_jefe = ? AND id_periodo = ?", [$id_evaluado, $id_jefe, $id_periodo]);

      for ($i = 1;$i <= 5;$i++){

        if (!empty($_POST['objetivo' . $i]) || !empty($_POST['accion' . $i]) || !empty($_POST['entregable' . $i])){

          $objetivo = (!empty($_POST['objetivo' . $i]) ? $_POST['objetivo' . $i] : '');
          $accion = (!empty($_POST['accion' . $i]) ? $_POST['accion' . $i] : '');
          $entregable = (!empty($_POST['entregable' . $i]) ? $_POST['entregable' . $i] : '');
          $fecha = date('Y-m-d H:i:s');
          DB::insert("INSERT INTO objetivos_entregables_desempeno (id_evaluado,id_jefe,id_periodo,objetivo,accion,entregable,created_at) VALUES (?,?,?,?,?,?,?)", [$id_evaluado,$id_jefe,$id_periodo,$objetivo,$accion,$entregable,$fecha]);
        }
      }

      DB::delete("DELETE FROM comentarios_desempeno_jefe WHERE id_evaluado = ? AND id_jefe = ? AND id_periodo = ?", [$id_evaluado, $id_jefe, $id_periodo]);

      if (!empty($_POST['comentarios_adicionales'])){

        $comentario = $_POST['comentarios_adicionales'];
        $fecha = date('Y-m-d H:i:s');
        DB::insert("INSERT INTO comentarios_desempeno_jefe (id_evaluado,id_jefe,id_periodo,comentario,created_at) VALUES (?,?,?,?,?)", [$id_evaluado, $id_jefe, $id_periodo, $comentario, $fecha]);
      }
    }

    /**
     * Descarga en excel todos los objetivos acordados de los subordinados
     */
    public function reporte_objetivos($id_periodo){

      $id_jefe = auth()->user()->id;
      $nombre_jefe = auth()->user()->first_name . ' ' . auth()->user()->last_name;
      
      // Obtienen los objetivos acordados
      $objetivos = DB::select("SELECT objetivo, accion, entregable, id_evaluado, first_name, last_name, division FROM objetivos_entregables_desempeno INNER JOIN users ON (users.id = objetivos_entregables_desempeno.id_evaluado) WHERE id_periodo = ? AND id_jefe = ?", [$id_periodo, $id_jefe]);

      require_once 'PHPExcel.php';
      require_once 'PHPExcel/IOFactory.php';

      // Create new PHPExcel object
      $objPHPExcel = new \PHPExcel();

      $current_user = 0;
      $counter = 0;
      $nombre = '';
      $row = 0;

      foreach ($objetivos as $key => $objetivo){

        if ($current_user != $objetivo->id_evaluado){

          if ($current_user != 0){

            // Rename sheet
            $objPHPExcel->getActiveSheet()->setTitle($nombre);

            $counter++;
          }

          $current_user = $objetivo->id_evaluado;
          $row = 4;

          if ($counter > 0){

            // Create a new worksheet, after the default sheet
            $objPHPExcel->createSheet();
          }

          // Create a new sheet
          $objPHPExcel->setActiveSheetIndex($counter);

          $objDrawing = new \PHPExcel_Worksheet_Drawing();
          $objDrawing->setPath(getcwd() . '/img/header/logo maver.png');
          $objDrawing->setCoordinates('A1');
          $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
          $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(34);
          $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(23);
          $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(23);
          $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(63);
          $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(25);
          $objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(25);
          $objPHPExcel->getActiveSheet()->mergeCells('B1:D1');
          $objPHPExcel->getActiveSheet()->getStyle("B1:D1")->getFont()->setSize(18);
          $objPHPExcel->getActiveSheet()->getStyle('B1:D1')->getFont()->setBold(true);
          $objPHPExcel->getActiveSheet()->getStyle('B1:D1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('B1:D1')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('203864');
          $objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(true)->getColor()->setRGB('FFFFFF');
          $objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('A3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('203864');
          $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true)->getColor()->setRGB('FFFFFF');
          $objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('B3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('B3')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('C2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('C2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('C2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('203864');
          $objPHPExcel->getActiveSheet()->getStyle("C2")->getFont()->setBold(true)->getColor()->setRGB('FFFFFF');
          $objPHPExcel->getActiveSheet()->getStyle('C3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('C3')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('C3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('203864');
          $objPHPExcel->getActiveSheet()->getStyle("C3")->getFont()->setBold(true)->getColor()->setRGB('FFFFFF');
          $objPHPExcel->getActiveSheet()->getStyle('D2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('D2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('D3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('D3')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('A4')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('8EAADC');
          $objPHPExcel->getActiveSheet()->getStyle('B4')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('B4')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('8EAADC');
          $objPHPExcel->getActiveSheet()->getStyle('C4')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('C4')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('8EAADC');
          $objPHPExcel->getActiveSheet()->getStyle('D4')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('D4')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('8EAADC');
          $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Evaluación del desempeño 2018');
          $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Número de empleado:');
          $objPHPExcel->getActiveSheet()->setCellValue('C2', 'Área:');
          $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Nombre:');
          $objPHPExcel->getActiveSheet()->setCellValue('C3', 'Jefe Directo:');
          $objPHPExcel->getActiveSheet()->setCellValue('A4', 'Objetivo');
          $objPHPExcel->getActiveSheet()->setCellValue('B4', 'Acciones');
          $objPHPExcel->getActiveSheet()->setCellValue('C4', 'Entregables');
          $objPHPExcel->getActiveSheet()->setCellValue('D4', 'Fecha compromiso');
          $objPHPExcel->getActiveSheet()->setCellValue('B2', $objetivo->id_evaluado);
          $objPHPExcel->getActiveSheet()->setCellValue('D2', $objetivo->division);
          $objPHPExcel->getActiveSheet()->setCellValue('B3', $objetivo->first_name . ' ' . $objetivo->last_name);
          $objPHPExcel->getActiveSheet()->setCellValue('D3', $nombre_jefe);

          //setOffsetX works properly
          $objDrawing->setOffsetX(32); 

          //set width
          $objDrawing->setWidth(110);

          $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
          $nombre = $objetivo->first_name . ' ' . $objetivo->last_name;

          if (strlen($nombre) > 31){

            $nombre = substr($nombre, 0, 31);
          }
        }

        $row++;
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(100);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $objetivo->objetivo);
        $objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $objetivo->accion);
        $objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $objetivo->entregable);
        $objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
      }

      if ($current_user != 0){

        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle($nombre);
      }

      // Redirect output to a client’s web browser (Excel5)
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="Maver - Objetivos Acordados.xls"');
      header('Cache-Control: max-age=0');
      $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
      $objWriter->save('php://output');
    }

    /**
     * Guarda las ponderaciones y muestra la pagina para editarlas
     */
    public function ponderaciones(){

      $ponderaciones = array();
      $id_periodo = 0;
      $message_flash = false;

      // Requiere mostrar o guardar las ponderaciones para cierto periodo
      if (!empty($_POST['id_periodo'])){

        $id_periodo = $_POST['id_periodo'];

        // Se requiere guardar las ponderaciones
        if (!empty($_POST['ponderacion'])){

          DB::table('ponderaciones')->where('id_periodo', $id_periodo)->delete();
          $ponderacion = array();
          $ponderacion['id_periodo'] = $id_periodo;

          foreach ($_POST['ponderacion']['id_nivel_puesto'] as $key => $value){
            
            $ponderacion['id_nivel_puesto'] = $value;
            $ponderacion['par'] = $_POST['ponderacion']['par'][$key];
            $ponderacion['jefe'] = $_POST['ponderacion']['jefe'][$key];
            $ponderacion['colaborador'] = $_POST['ponderacion']['colaborador'][$key];
            $ponderacion['autoevaluacion'] = $_POST['ponderacion']['autoevaluacion'][$key];
            DB::table('ponderaciones')->insert($ponderacion);
          }

          $message_flash = true;
        }

        // Se obtienen las ponderaciones del periodo
        $ponderaciones = DB::select("SELECT * FROM ponderaciones WHERE id_periodo = ?", [$id_periodo]);
      }

      // Se obtienen todos los periodos
      $periodos = DB::select("SELECT id, descripcion FROM periodos ORDER BY id DESC");

      // Existen periodos y aun no se han obtenido las ponderaciones 
      if (count($periodos) > 0 && count($ponderaciones) == 0){

        $last = DB::table('ponderaciones')->select('id_periodo')->orderBy('created_at', 'DESC')->first();

        if (!empty($last->id_periodo)){

          // Se obtienen las ultimas ponderaciones registradas
          $ponderaciones = DB::table('ponderaciones')->where('id_periodo', $last->id_periodo)->select('id_nivel_puesto', 'jefe', 'autoevaluacion', 'colaborador', 'par')->orderBy('id_nivel_puesto')->get();
        }
      }

      $niveles_puestos = DB::table('niveles_puestos')->where('mando', 1)->select('id', 'nombre')->orderBy('id')->get();
      
      // Se muestra la vista para las ponderaciones
      return view('evaluacion-desempeno/reportes/ponderaciones', compact('ponderaciones', 'periodos', 'message_flash', 'niveles_puestos'));
    }

  /** Descarga en excel las respuestas del periodo
  */
    public function exportar_respuestas(){

      $period_id = $_POST['period_id'];
      
      // Obtienen los objetivos acordados
      //$answers = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('users', 'users.id', '=', )

      require_once 'PHPExcel.php';
      require_once 'PHPExcel/IOFactory.php';

      // Create new PHPExcel object
      $objPHPExcel = new \PHPExcel();

      $current_user = 0;
      $counter = 0;
      $nombre = '';
      $row = 0;

      foreach ($objetivos as $key => $objetivo){

        if ($current_user != $objetivo->id_evaluado){

          if ($current_user != 0){

            // Rename sheet
            $objPHPExcel->getActiveSheet()->setTitle($nombre);

            $counter++;
          }

          $current_user = $objetivo->id_evaluado;
          $row = 4;

          if ($counter > 0){

            // Create a new worksheet, after the default sheet
            $objPHPExcel->createSheet();
          }

          // Create a new sheet
          $objPHPExcel->setActiveSheetIndex($counter);

          $objDrawing = new \PHPExcel_Worksheet_Drawing();
          $objDrawing->setPath(getcwd() . '/img/header/logo maver.png');
          $objDrawing->setCoordinates('A1');
          $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
          $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(34);
          $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(23);
          $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(23);
          $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(63);
          $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(25);
          $objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(25);
          $objPHPExcel->getActiveSheet()->mergeCells('B1:D1');
          $objPHPExcel->getActiveSheet()->getStyle("B1:D1")->getFont()->setSize(18);
          $objPHPExcel->getActiveSheet()->getStyle('B1:D1')->getFont()->setBold(true);
          $objPHPExcel->getActiveSheet()->getStyle('B1:D1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('B1:D1')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('A2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('203864');
          $objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(true)->getColor()->setRGB('FFFFFF');
          $objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('A3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('203864');
          $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true)->getColor()->setRGB('FFFFFF');
          $objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('B3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('B3')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('C2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('C2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('C2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('203864');
          $objPHPExcel->getActiveSheet()->getStyle("C2")->getFont()->setBold(true)->getColor()->setRGB('FFFFFF');
          $objPHPExcel->getActiveSheet()->getStyle('C3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('C3')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('C3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('203864');
          $objPHPExcel->getActiveSheet()->getStyle("C3")->getFont()->setBold(true)->getColor()->setRGB('FFFFFF');
          $objPHPExcel->getActiveSheet()->getStyle('D2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('D2')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('D3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('D3')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('A4')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('8EAADC');
          $objPHPExcel->getActiveSheet()->getStyle('B4')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('B4')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('8EAADC');
          $objPHPExcel->getActiveSheet()->getStyle('C4')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('C4')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('8EAADC');
          $objPHPExcel->getActiveSheet()->getStyle('D4')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $objPHPExcel->getActiveSheet()->getStyle('D4')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('8EAADC');
          $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Evaluación del desempeño 2018');
          $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Número de empleado:');
          $objPHPExcel->getActiveSheet()->setCellValue('C2', 'Área:');
          $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Nombre:');
          $objPHPExcel->getActiveSheet()->setCellValue('C3', 'Jefe Directo:');
          $objPHPExcel->getActiveSheet()->setCellValue('A4', 'Objetivo');
          $objPHPExcel->getActiveSheet()->setCellValue('B4', 'Acciones');
          $objPHPExcel->getActiveSheet()->setCellValue('C4', 'Entregables');
          $objPHPExcel->getActiveSheet()->setCellValue('D4', 'Fecha compromiso');
          $objPHPExcel->getActiveSheet()->setCellValue('B2', $objetivo->id_evaluado);
          $objPHPExcel->getActiveSheet()->setCellValue('D2', $objetivo->division);
          $objPHPExcel->getActiveSheet()->setCellValue('B3', $objetivo->first_name . ' ' . $objetivo->last_name);
          $objPHPExcel->getActiveSheet()->setCellValue('D3', $nombre_jefe);

          //setOffsetX works properly
          $objDrawing->setOffsetX(32); 

          //set width
          $objDrawing->setWidth(110);

          $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
          $nombre = $objetivo->first_name . ' ' . $objetivo->last_name;

          if (strlen($nombre) > 31){

            $nombre = substr($nombre, 0, 31);
          }
        }

        $row++;
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(100);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $objetivo->objetivo);
        $objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $objetivo->accion);
        $objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $objetivo->entregable);
        $objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
      }

      if ($current_user != 0){

        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle($nombre);
      }

      // Redirect output to a client’s web browser (Excel5)
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="Maver - Objetivos Acordados.xls"');
      header('Cache-Control: max-age=0');
      $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
      $objWriter->save('php://output');
    }

  /**
  * Exporta Resultados del Clima
  */
  public function exportar_resultados_clima(){
        
    require_once 'PHPExcel.php';
    require_once 'PHPExcel/IOFactory.php';
    $id_periodo = $_POST['period_id'];
    $no_aplica = array(7,8);
    $remove = DB::table('clima_period_user')->join('users', 'users.id', '=', 'clima_period_user.user_id')->join('employees', 'employees.id', '=', 'users.employee_id')->where('period_id', $id_periodo)->whereIn('region_id', $no_aplica)->pluck('user_id')->toArray();
    $terminados = DB::table('clima_period_user')->where('period_id', $id_periodo)->where('status', 3)->whereNotIn('user_id', $remove)->pluck('user_id')->toArray();
   
    $resultados = DB::table('clima_answers')
    ->join('users', 'users.id', '=', 'clima_answers.user_id')
    ->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')
    ->join('employees', 'employees.id', '=', 'users.employee_id')
    ->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')
    ->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')
    ->leftJoin('departments', 'departments.id', '=', 'areas.department_id')
    ->leftJoin('directions', 'directions.id', '=', 'departments.direction_id')
    ->join('regions', 'regions.id', '=', 'employees.region_id')
    ->select(
      'first_name', 'last_name', 'rfc', 'ingreso', 'nacimiento', 'sexo', 'turno', 'grado', 'division', 'relacion',
      'regions.name as region', 
      'directions.name AS direction', 'departments.name AS department',
      'job_positions.name as job_position', 
      'clima_questions.id as number', 
      'clima_questions.question', 
      'clima_answers.answer')
    ->where('clima_answers.period_id',$id_periodo)
    ->where('clima_answers.question_id',28)
    ->get();
    
    // Create new PHPExcel object
    $objPHPExcel = new \PHPExcel();

    // Create a first sheet
    $objPHPExcel->setActiveSheetIndex(0);
        
    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Dirección');  
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Departamento');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Puesto');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Región');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'RFC');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Ingreso');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Nacimiento');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Sexo');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Turno');
    $objPHPExcel->getActiveSheet()->setCellValue('J1', 'Categoría de Puesto');
    $objPHPExcel->getActiveSheet()->setCellValue('K1', 'Unidad de Negocio');
    $objPHPExcel->getActiveSheet()->setCellValue('L1', 'Clasificación del Colaborador');
    /* $objPHPExcel->getActiveSheet()->setCellValue('M1', 'Nombre'); */
    $objPHPExcel->getActiveSheet()->setCellValue('M1', 'Pregunta');
    $objPHPExcel->getActiveSheet()->setCellValue('N1', 'Respuesta');
    $i = 2;

    foreach ($resultados as $key => $value){
            
      $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $value->direction);
      $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $value->department);
      $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $value->job_position);
      $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $value->region);
      $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $value->rfc);
      $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $value->ingreso);
      $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $value->nacimiento);
      $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, ($value->sexo == 'M' ? 'Masculino' : 'Femenino'));
      $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $value->turno);
      $objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $value->grado);
      $objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $value->division);
      $objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $value->relacion);
      /* $objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $value->last_name . ' ' . $value->first_name); */
      $objPHPExcel->getActiveSheet()->setCellValue('M' . $i, str_replace($value->number.". ", "", $value->question) );
      $objPHPExcel->getActiveSheet()->setCellValue('N' . $i, $value->answer);
      $i++;
    }

    $objPHPExcel->getActiveSheet()->setTitle('Respuestas No Abiertas');
    $resultados = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('users', 'clima_answers.user_id', '=', 'users.id')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->leftJoin('directions', 'directions.id', '=', 'departments.direction_id')->where('period_id', $id_periodo)->whereNull('factor_id')->select('answer', 'question', 'user_id', 'directions.name AS direction', 'departments.name AS department', 'job_positions.name AS job_position', 'first_name', 'last_name', 'rfc', 'ingreso', 'nacimiento', 'sexo', 'turno', 'grado', 'division', 'relacion')->orderBy('directions.name')->orderBy('departments.name')->orderBy('job_positions.name')->orderBy('last_name')->orderBy('question_id')->get();
    $objPHPExcel->createSheet();

    // Select the next sheet
    $objPHPExcel->setActiveSheetIndex(1);

    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Dirección');  
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Departamento');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Puesto');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'RFC');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Ingreso');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Nacimiento');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Sexo');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Turno');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Categoría de Puesto');
    $objPHPExcel->getActiveSheet()->setCellValue('J1', 'Unidad de Negocio');
    $objPHPExcel->getActiveSheet()->setCellValue('K1', 'Clasificación del Colaborador');
    /* $objPHPExcel->getActiveSheet()->setCellValue('L1', 'Nombre'); */
    $objPHPExcel->getActiveSheet()->setCellValue('L1', 'Pregunta');
    $objPHPExcel->getActiveSheet()->setCellValue('M1', 'Respuesta');
    $i = 2;

    foreach ($resultados as $key => $value){
            
      $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $value->direction);
      $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $value->department);
      $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $value->job_position);
      $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $value->rfc);
      $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $value->ingreso);
      $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $value->nacimiento);
      $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, ($value->sexo == 'M' ? 'Masculino' : 'Femenino'));
      $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $value->turno);
      $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $value->grado);
      $objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $value->division);
      $objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $value->relacion);
      /* $objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $value->last_name . ' ' . $value->first_name); */
      $objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $value->question);
      $objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $value->answer);
      $i++;
    }

    $objPHPExcel->getActiveSheet()->setTitle('Respuestas Abiertas');

    //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Respuestas Clima HPH' . '.csv"');
    //header('Content-Disposition: attachment;filename="Respuestas Clima.xls"');
    //header('Cache-Control: max-age=0');
    //$objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $value->answer);
    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
    $objWriter->save('php://output');
  }

  public function exportar_resultados_clima_confidencialidad(){
        
    require_once 'PHPExcel.php';
    require_once 'PHPExcel/IOFactory.php';
    $id_periodo = $_POST['period_id'];
        
    // Create new PHPExcel object
    $objPHPExcel = new \PHPExcel();

    // Create a first sheet
    $objPHPExcel->setActiveSheetIndex(0);
        
    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Pregunta');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Respuesta');
    $i = 2;

    $resultados = DB::table('clima_answers')
    ->join('users', 'users.id', '=', 'clima_answers.user_id')
    ->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')
    ->join('employees', 'employees.id', '=', 'users.employee_id')
    ->join('regions', 'regions.id', '=', 'employees.region_id')
    ->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')
    ->select('regions.name as region', 'job_positions.name as department', 
    'clima_questions.id as number', 'clima_questions.question', 'clima_answers.answer')
    ->where('clima_answers.period_id',$id_periodo)
    ->where('clima_answers.question_id',28)
    ->get();

    foreach ($resultados as $key => $value){
     $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, str_replace($value->number.". ", "", $value->question) );
      $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $value->answer);
      $i++;
    }

    $objPHPExcel->getActiveSheet()->setTitle('Respuestas No Abiertas');
    $resultados = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('users', 'clima_answers.user_id', '=', 'users.id')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->leftJoin('directions', 'directions.id', '=', 'departments.direction_id')->where('period_id', $id_periodo)->whereNull('factor_id')->select('answer', 'question', 'user_id', 'directions.name AS direction', 'departments.name AS department', 'job_positions.name AS job_position', 'first_name', 'last_name', 'rfc', 'ingreso', 'nacimiento', 'sexo', 'turno', 'grado', 'division', 'relacion')->orderBy('directions.name')->orderBy('departments.name')->orderBy('job_positions.name')->orderBy('last_name')->orderBy('question_id')->get();
    $objPHPExcel->createSheet();

    // Select the next sheet
    $objPHPExcel->setActiveSheetIndex(1);

    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Pregunta');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Respuesta');
    $i = 2;

    
    $resultados = DB::table('clima_answers')
    ->join('users', 'users.id', '=', 'clima_answers.user_id')
    ->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')
    ->join('employees', 'employees.id', '=', 'users.employee_id')
    ->join('regions', 'regions.id', '=', 'employees.region_id')
    ->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')
    ->select('regions.name as region', 'job_positions.name as department', 
    'clima_questions.id as number', 'clima_questions.question', 'clima_answers.answer')
    ->where('clima_answers.period_id',$id_periodo)
    ->where('clima_answers.question_id',28)
    ->get();

    foreach ($resultados as $key => $value){
            
      $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, str_replace($value->number.". ", "", $value->question) );
      $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $value->answer);
      $i++;
    }

    $objPHPExcel->getActiveSheet()->setTitle('Respuestas Abiertas');

    //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Respuestas Clima HPH' . '.csv"');
    //header('Content-Disposition: attachment;filename="Respuestas Clima.xls"');
    //header('Cache-Control: max-age=0');
    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
    $objWriter->save('php://output');
  }
}
