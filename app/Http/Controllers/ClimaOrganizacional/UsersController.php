<?php

namespace App\Http\Controllers\ClimaOrganizacional;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use App\Http\Requests;
use DB;
use Session;
use App\User;

class UsersController extends Controller
{
  
  // Muestra un listado con todos los empleados activos
  public function lista_usuarios(){

    if (auth()->user()->role != 'admin' && auth()->user()->role != 'supervisor' && !auth()->user()->hasClimaPermissions(10)){

      flash('No tiene permiso para ver esta sección');
      return redirect('/clima-organizacional/que-es');
    }

    $id_user = auth()->user()->id;
    $users = User::where('active', 1)->whereNull('deleted_at')->where('id', '!=', 1)->get();
    return view('clima-organizacional.usuarios.lista-usuarios', compact('users'));
  }

  // Cambia la contraseña de un usuario
  public function cambiar_contrasena($id_user){

    if (auth()->user()->role != 'admin' && auth()->user()->role != 'supervisor' && $id_user != auth()->user()->id){

      flash('No tiene permiso para ver esta sección');
      return redirect('/');
    }

    if (empty($id_user)){

      $id_user = auth()->user()->id;
    }

    if (!empty($_POST['nueva_contrasena'])){

      $password = $_POST['nueva_contrasena'];
      $encrypted_password = password_hash($password, PASSWORD_DEFAULT);
      DB::update("UPDATE users SET password = '$encrypted_password', external = NULL WHERE id = ?", [$id_user]);
      flash('La contraseña fue guardada correctamente');
      Session::forget('primer_login');

      if ($id_user == auth()->user()->id){

        return redirect('/cuestionarios/evaluaciones');
      }

      else{

        DB::update("UPDATE users SET external = 1 WHERE id = ?", [$id_user]);
        return redirect('/clima-organizacional/lista-empleados');
      }
    }

    $user = DB::select("SELECT id, first_name, last_name FROM users WHERE active = ? AND id = ?", [1, $id_user]);
    return view('clima-organizacional.usuarios.cambiar-contrasena', compact('user'));
  }
}
