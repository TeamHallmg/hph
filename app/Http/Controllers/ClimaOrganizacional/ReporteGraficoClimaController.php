<?php

namespace App\Http\Controllers\ClimaOrganizacional;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\ClimaOrganizacional\Period;
use App\Models\ClimaOrganizacional\Factor;

use App\Models\Direction;
use App\Models\Department;
use App\Models\Area;
use App\Models\JobPosition;
use App\User;
use DB;
use Session;

class ReporteGraficoClimaController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $periodo = Period::where('status', 'Abierto')->orderBy('id', 'DESC')->first();
        if(is_null($periodo)){
            flash('No hay periodos abiertos por el momento...');
            return redirect('/clima-organizacional/que-es')->with('error','No tienes permiso para ver la evaluación');
        }
        $directions = Direction::select('name')->groupBy('name')->get();
        $departments = Department::select('name')->groupBy('name')->get();
        $areas = Area::select('name')->groupBy('name')->get();
        $jobs = JobPosition::select('name')->groupBy('name')->get();
        $questions = $periodo->questions()->has('factor')->groupBy('factor_id')->get();
        $factorsUsed = $questions->pluck('factor_id')->toArray();
        $factors = Factor::with(['questions' => function($q) use($periodo) {
            $q->with(['answers' => function($q) use($periodo) {
                $q->where('period_id', $periodo->id);
            }])->whereHas('periods', function($q) use($periodo) {
                $q->where('id', $periodo->id);   
            });
        }
        ])->whereIn('id', $factorsUsed)
        ->orderBy('name', 'ASC')
        ->get();

        foreach ($factors as $factor) {
            $promedio = 0;
            if($factor->questions->count() > 0){
                foreach ($factor->questions as $question) {
                    $total = $question->answers->count(); //Respuestas existentes a la pregunta
                    if($total > 0){
                        $valores = 0;
                        foreach ($question->answers as $answer) {
                            $valores += $this->getAnswerValue($answer->answer, $question->positive);
                        }
                        $promedio += $valores / $total;
                    }
                }
                $factor->averageValue = $promedio / $factor->questions->count();
            }
        }

        $categories = $factors->pluck('name')->toArray();
        $factorData = [];
        foreach ($factors as $factor) {
            $factorData[] = [
                'name' => $factor->name,
                'data' => [round($factor->averageValue, 2)],
            ];
        }
        return view('clima-organizacional.graficos.index', compact('factors', 'factorData', 'categories', 'directions', 'departments', 'areas', 'jobs'));
    }

    public function post(Request $request){
        $order = $request->order;
        $factor = empty($request->factor)?null:$request->factor;
        $direction = empty($request->direction)?null:$request->direction;
        $department = empty($request->department)?null:$request->department;
        $area = empty($request->area)?null:$request->area;
        $job = empty($request->job)?null:$request->job;
        $job_level = empty($request->job_level)?null:$request->job_level;

        $values = [
            'direction' => $direction,
            'deparment' => $department,
            'area' => $area,
            'job' => $job,
        ];

        $periodo = Period::where('status', 'Abierto')->orderBy('id', 'DESC')->first();

        $users = User::whereHas('employee', function ($q) use($values){
            $q->cascade($values);
        })->get();


        $questions = $periodo->questions()->when(!is_null($factor), function($q) use($factor){
            $q->whereHas('factor', function($q1) use($factor){
                $q1->where('id', $factor);
            });
        }, function($q){
            $q->has('factor');
        })
        ->groupBy('factor_id')->get();
        $factorsUsed = $questions->pluck('factor_id')->toArray();
        $factors = Factor::with(['questions' => function($q) use($periodo) {
            $q->with(['answers' => function($q) use($periodo) {
                $q->where('period_id', $periodo->id);
            }])->whereHas('periods', function($q) use($periodo) {
                $q->where('id', $periodo->id);   
            });
        }
        ])->whereIn('id', $factorsUsed)
        ->orderBy('name', 'ASC')
        ->get();

        foreach ($factors as $factor) {
            $promedio = 0;
            if($factor->questions->count() > 0){
                foreach ($factor->questions as $question) {
                    $total = $question->answers->count(); //Respuestas existentes a la pregunta
                    if($total > 0){
                        $valores = 0;
                        foreach ($question->answers as $answer) {
                            $valores += $this->getAnswerValue($answer->answer, $question->positive);
                        }
                        $promedio += $valores / $total;
                    }
                }
                $factor->averageValue = $promedio / $factor->questions->count();
            }
        }

        if($order == "factor"){
            $categories = $factors->pluck('name')->toArray();
            $factorData = [];
            foreach ($factors as $factor) {
                $factorData[] = [
                    'name' => $factor->name,
                    'data' => [round($factor->averageValue, 2)],
                ];
            }
        }else{
            $categories = [];
            $factorData = [];
            foreach ($factors as $factor) {
                $factorData[strval($factor->averageValue)] = [
                    'name' => $factor->name,
                    'data' => [round($factor->averageValue, 2)],
                ];
            }
            ksort($factorData, SORT_NUMERIC);
            foreach($factorData as $data){
                $categories[] = $data['name'];
            }
            $factorData = array_values($factorData);
        }
        return response()->json(['data' => $factorData, 'categories' => $categories], 200);
    }

    public function getAnswerValue($answer, $positive){
        switch ($answer){
            case 'Totalmente de acuerdo':
                return ($positive == 1)?5:1;
                break;
            case 'De acuerdo':
                return ($positive == 1)?4:2;
                break;

            case 'Mas o menos':
                return 3;
                break;

            case 'Desacuerdo':
                return ($positive == 1)?2:4;
                break;
            case 'Totalmente desacuerdo':
                return ($positive == 1)?1:5;
                break;
            default:
                return 0;
                break;
        }   
    }

  /**
  * Reporte de calor
  *
  * @return void
  */
  public function heat_report(Request $request){

    if (auth()->user()->role != 'admin' && !auth()->user()->hasClimaPermissions(4)){
      flash('No cuenta con permisos');
      return redirect('/clima-organizacional/que-es');
    }
    $period_id = 0;
    $results = $clima_factors = $etiquetas = array();
    $terminados = array();
    $periods = Period::where('status', 'Abierto')->orWhere('status', 'Cerrado')->orderBy('id', 'DESC')->get();

    if (count($periods) > 0){

      if (!empty($_POST['period_id']) || Session::has('period_id')){

        if (!empty($_POST['period_id'])){

          $period_id = $_POST['period_id'];
          Session::put('period_id', $period_id);
        }

        else{

          $period_id = Session::get('period_id');
          Session::forget('period_id');
        }
      }

      else{

        $period_id = $periods[0]->id;
      }

      $permissions = auth()->user()->getClimaRegions(4);
      
      if (auth()->user()->role == 'admin' || in_array(0, $permissions)){
      
        $terminados = DB::table('clima_period_user')->where('period_id', $period_id)->where('status', 3)->pluck('user_id')->toArray();
      }

      else{

        $terminados = DB::table('clima_period_user')->join('users', 'users.id', '=', 'clima_period_user.user_id')->join('employees', 'employees.id', '=', 'users.employee_id')->where('period_id', $period_id)->where('status', 3)->whereIn('region_id', $permissions)->pluck('user_id')->toArray();
      }
  
      $aplicar_filtros = array(
        'sucursal_id' => $request->sucursal,
        'area_id' => $request->direccion,
        'puesto_id' => $request->job,
        'edad_id' => $request->edad,
        'inicio_id' => $request->ingreso,
        'sexo_id' => $request->sexo,
        'centro_trabajo_id' => $request->centro_trabajo,
        'turnos_id' => $request->turno,
        'regiones_id' => $request->regiones
      );

      $filtros_ejecutados = User::empleados_con_filtros($terminados,$aplicar_filtros);

      $ids_user = $filtros_ejecutados['ids_user'];
  

      $results = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('clima_factors', 'clima_factors.id', '=', 'clima_questions.factor_id')->join('users', 'users.id', '=', 'clima_answers.user_id')->join('employees', 'employees.id', '=', 'users.employee_id')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->join('regions', 'regions.id', '=', 'employees.region_id')->join('departments', 'departments.id', '=', 'areas.department_id')->join('directions', 'directions.id', '=', 'departments.direction_id')->where('period_id', $period_id)->whereIn('user_id',$ids_user)->whereNotNull('factor_id')->select(array(DB::raw('AVG(answer) AS average'), 'departments.id', 'departments.name', 'clima_factors.name AS factor_name','regions.name AS region', 'region_id', 'directions.name AS direction'))->groupBy('region_id', 'departments.name', 'clima_factors.id')->orderBy('regions.id')->orderBy('departments.name')->orderBy('clima_factors.id')->get();
      $results_by_departments = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('users', 'users.id', '=', 'clima_answers.user_id')->join('employees', 'employees.id', '=', 'users.employee_id')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->join('departments', 'departments.id', '=', 'areas.department_id')->join('regions', 'regions.id', '=', 'employees.region_id')->where('period_id', $period_id)->whereIn('user_id',$ids_user)->whereNotNull('factor_id')->select(array(DB::raw('AVG(answer) AS average'), 'departments.name', 'region_id'))->groupBy('region_id', 'departments.name')->get();
      $results_by_factors = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('clima_factors', 'clima_factors.id', '=', 'clima_questions.factor_id')->where('period_id', $period_id)->whereIn('user_id',$ids_user)->whereNotNull('factor_id')->select(array(DB::raw('AVG(answer) AS average'), 'clima_factors.name'))->groupBy('clima_factors.name')->pluck('average', 'factors.name')->toArray();
      $general_result = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->where('period_id', $period_id)->whereIn('user_id',$ids_user)->whereNotNull('factor_id')->select(DB::raw('AVG(answer) AS average'))->get();
      $clima_factors = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('clima_factors', 'clima_factors.id', '=', 'clima_questions.factor_id')->where('period_id', $period_id)->groupBy('clima_factors.id')->orderBy('clima_factors.id')->pluck('clima_factors.name')->toArray();

      $etiquetas = DB::table('clima_etiquetas')->orderBy('valor', 'DESC')->get();
      $temp = array();

      foreach ($results_by_departments as $key => $value){
        
        if (!isset($temp[$value->region_id])){

          $temp[$value->region_id] = array();
        }

        $temp[$value->region_id][$value->name] = $value->average;
      }

      $results_by_departments = $temp;
    }

    return view('clima-organizacional.reportes.heat-report', compact('periods', 'period_id', 'results', 'clima_factors', 'etiquetas','filtros_ejecutados','aplicar_filtros', 'results_by_departments', 'results_by_factors', 'general_result'));

  }

  /**
  * Reporte de calor Por Regiones
  *
  * @return void
  */
  public function heat_report_by_regions(Request $request){

    if (auth()->user()->role != 'admin' && !auth()->user()->hasClimaPermissions(5)){
      flash('No cuenta con permisos');
      return redirect('/clima-organizacional/que-es');
    }

    $period_id = 0;
    $results = $clima_factors = $etiquetas = array();
    $terminados = array();
    $periods = Period::where('status', 'Abierto')->orWhere('status', 'Cerrado')->orderBy('id', 'DESC')->get();

    if (count($periods) > 0){

      if (!empty($_POST['period_id']) || Session::has('period_id')){

        if (!empty($_POST['period_id'])){

          $period_id = $_POST['period_id'];
          Session::put('period_id', $period_id);
        }

        else{

          $period_id = Session::get('period_id');
          Session::forget('period_id');
        }
      }

      else{

        $period_id = $periods[0]->id;
      }

      $permissions = auth()->user()->getClimaRegions(5);
      
      if (auth()->user()->role == 'admin' || in_array(0, $permissions)){
      
        $terminados = DB::table('clima_period_user')->where('period_id', $period_id)->where('status', 3)->pluck('user_id')->toArray();
      }

      else{

        $terminados = DB::table('clima_period_user')->join('users', 'users.id', '=', 'clima_period_user.user_id')->join('employees', 'employees.id', '=', 'users.employee_id')->where('period_id', $period_id)->where('status', 3)->whereIn('region_id', $permissions)->pluck('user_id')->toArray();
      }
   
      $aplicar_filtros = array(
        'sucursal_id' => $request->sucursal,
        'area_id' => $request->direccion,
        'puesto_id' => $request->job,
        'edad_id' => $request->edad,
        'inicio_id' => $request->ingreso,
        'sexo_id' => $request->sexo,
        'centro_trabajo_id' => $request->centro_trabajo,
        'turnos_id' => $request->turno,
        'regiones_id' => $request->regiones
      );

      $filtros_ejecutados = User::empleados_con_filtros($terminados,$aplicar_filtros);
      $ids_user = $filtros_ejecutados['ids_user'];


      $results = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('clima_factors', 'clima_factors.id', '=', 'clima_questions.factor_id')->join('users', 'users.id', '=', 'clima_answers.user_id')->join('employees', 'employees.id', '=', 'users.employee_id')->join('regions', 'regions.id', '=', 'employees.region_id')->where('period_id', $period_id)->whereNotNull('factor_id')->whereIn('user_id', $ids_user)->select(array(DB::raw('AVG(answer) AS average'), 'regions.id', 'regions.name', 'clima_factors.name AS factor_name'))->groupBy('clima_factors.id', 'regions.id')->orderBy('regions.id')->orderBy('clima_factors.id')->get();
      $clima_factors = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('clima_factors', 'clima_factors.id', '=', 'clima_questions.factor_id')->where('period_id', $period_id)->groupBy('clima_factors.id')->orderBy('clima_factors.id')->pluck('clima_factors.name')->toArray();
      $etiquetas = DB::table('clima_etiquetas')->orderBy('valor', 'DESC')->get();
    }

    return view('clima-organizacional.reportes.heat-report_by_regions', compact('periods', 'period_id', 'results', 'clima_factors', 'etiquetas','aplicar_filtros','filtros_ejecutados'));
  }


  public function heat_report_by_supervisor(Request $request){

    if (auth()->user()->role != 'admin' && !auth()->user()->hasClimaPermissions(5)){
      flash('No cuenta con permisos');
      return redirect('/clima-organizacional/que-es');
    }

    $period_id = 0;
    $results = $clima_factors = $etiquetas = array();
    $terminados = array();
    $periods = Period::where('status', 'Abierto')->orWhere('status', 'Cerrado')->orderBy('id', 'DESC')->get();

    if (count($periods) > 0){

      if (!empty($_POST['period_id']) || Session::has('period_id')){

        if (!empty($_POST['period_id'])){

          $period_id = $_POST['period_id'];
          Session::put('period_id', $period_id);
        }

        else{

          $period_id = Session::get('period_id');
          Session::forget('period_id');
        }
      }

      else{

        $period_id = $periods[0]->id;
      }

      $permissions = auth()->user()->getClimaRegions(5);
      
      if (auth()->user()->role == 'admin' || in_array(0, $permissions)){
      
        $terminados = DB::table('clima_period_user')->where('period_id', $period_id)->where('status', 3)->pluck('user_id')->toArray();
      }

      else{

        $terminados = DB::table('clima_period_user')->join('users', 'users.id', '=', 'clima_period_user.user_id')->join('employees', 'employees.id', '=', 'users.employee_id')->where('period_id', $period_id)->where('status', 3)->whereIn('region_id', $permissions)->pluck('user_id')->toArray();
      }
   
      $aplicar_filtros = array(
        'sucursal_id' => $request->sucursal,
        'area_id' => $request->direccion,
        'puesto_id' => $request->job,
        'edad_id' => $request->edad,
        'inicio_id' => $request->ingreso,
        'sexo_id' => $request->sexo,
        'centro_trabajo_id' => $request->centro_trabajo,
        'turnos_id' => $request->turno,
        'regiones_id' => $request->regiones
      );

      $filtros_ejecutados = User::empleados_con_filtros($terminados,$aplicar_filtros);
      $ids_user = $filtros_ejecutados['ids_user'];


      $results = DB::table('clima_answers')
        ->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')
        ->join('clima_factors', 'clima_factors.id', '=', 'clima_questions.factor_id')
        ->join('users', 'users.id', '=', 'clima_answers.user_id')
        ->join('employees', 'employees.id', '=', 'users.employee_id')
        ->join('employees as supervisor', 'employees.extra2', '=', 'supervisor.idempleado')
        ->join('regions', 'regions.id', '=', 'employees.region_id')
        ->where('period_id', $period_id)->whereNotNull('factor_id')
        ->whereIn('user_id', $ids_user)
        ->select(array(DB::raw('AVG(answer) AS average'), DB::raw('supervisor.idempleado AS id'), DB::raw('CONCAT(supervisor.nombre, " ", supervisor.paterno) AS name'), 'clima_factors.name AS factor_name'))
        ->groupBy('clima_factors.id', 'supervisor.idempleado')
        ->orderBy('supervisor.idempleado')
        ->orderBy('clima_factors.id')
        ->get();

      $clima_factors = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('clima_factors', 'clima_factors.id', '=', 'clima_questions.factor_id')->where('period_id', $period_id)->groupBy('clima_factors.id')->orderBy('clima_factors.id')->pluck('clima_factors.name')->toArray();
      $etiquetas = DB::table('clima_etiquetas')->orderBy('valor', 'DESC')->get();
    }


    // dd($results);

    return view('clima-organizacional.reportes.heat-report_by_supervisor', compact('periods', 'period_id', 'results', 'clima_factors', 'etiquetas','aplicar_filtros','filtros_ejecutados'));
  }


  public function heat_report_by_gerente(Request $request){

    if (auth()->user()->role != 'admin' && !auth()->user()->hasClimaPermissions(5)){
      flash('No cuenta con permisos');
      return redirect('/clima-organizacional/que-es');
    }

    $period_id = 0;
    $results = $clima_factors = $etiquetas = array();
    $terminados = array();
    $periods = Period::where('status', 'Abierto')->orWhere('status', 'Cerrado')->orderBy('id', 'DESC')->get();

    if (count($periods) > 0){

      if (!empty($_POST['period_id']) || Session::has('period_id')){

        if (!empty($_POST['period_id'])){

          $period_id = $_POST['period_id'];
          Session::put('period_id', $period_id);
        }

        else{

          $period_id = Session::get('period_id');
          Session::forget('period_id');
        }
      }

      else{

        $period_id = $periods[0]->id;
      }

      $permissions = auth()->user()->getClimaRegions(5);
      
      if (auth()->user()->role == 'admin' || in_array(0, $permissions)){
      
        $terminados = DB::table('clima_period_user')->where('period_id', $period_id)->where('status', 3)->pluck('user_id')->toArray();
      }

      else{

        $terminados = DB::table('clima_period_user')->join('users', 'users.id', '=', 'clima_period_user.user_id')->join('employees', 'employees.id', '=', 'users.employee_id')->where('period_id', $period_id)->where('status', 3)->whereIn('region_id', $permissions)->pluck('user_id')->toArray();
      }
   
      $aplicar_filtros = array(
        'sucursal_id' => $request->sucursal,
        'area_id' => $request->direccion,
        'puesto_id' => $request->job,
        'edad_id' => $request->edad,
        'inicio_id' => $request->ingreso,
        'sexo_id' => $request->sexo,
        'centro_trabajo_id' => $request->centro_trabajo,
        'turnos_id' => $request->turno,
        'regiones_id' => $request->regiones
      );

      $filtros_ejecutados = User::empleados_con_filtros($terminados,$aplicar_filtros);
      $ids_user = $filtros_ejecutados['ids_user'];


      $results = DB::table('clima_answers')
        ->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')
        ->join('clima_factors', 'clima_factors.id', '=', 'clima_questions.factor_id')
        ->join('users', 'users.id', '=', 'clima_answers.user_id')
        ->join('employees', 'employees.id', '=', 'users.employee_id')
        ->join('employees as gerente', 'employees.extra3', '=', 'gerente.idempleado')
        ->join('regions', 'regions.id', '=', 'employees.region_id')
        ->where('period_id', $period_id)->whereNotNull('factor_id')
        ->whereIn('user_id', $ids_user)
        ->select(array(DB::raw('AVG(answer) AS average'), DB::raw('gerente.idempleado AS id'), DB::raw('CONCAT(gerente.nombre, " ", gerente.paterno) AS name'), 'clima_factors.name AS factor_name'))
        ->groupBy('clima_factors.id', 'gerente.idempleado')
        ->orderBy('gerente.idempleado')
        ->orderBy('clima_factors.id')
        ->get();

      $clima_factors = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('clima_factors', 'clima_factors.id', '=', 'clima_questions.factor_id')->where('period_id', $period_id)->groupBy('clima_factors.id')->orderBy('clima_factors.id')->pluck('clima_factors.name')->toArray();
      $etiquetas = DB::table('clima_etiquetas')->orderBy('valor', 'DESC')->get();
    }


    // dd($results);

    return view('clima-organizacional.reportes.heat-report_by_gerente', compact('periods', 'period_id', 'results', 'clima_factors', 'etiquetas','aplicar_filtros','filtros_ejecutados'));
  }

  /**
  * Reporte de calor por grupos de áreas
  *
  * @return void
  */
  public function heat_report_by_areas_groups(){

    $period_id = 0;
    $results = $clima_factors = $etiquetas = array();
    $periods = Period::where('status', 'Abierto')->orWhere('status', 'Cerrado')->orderBy('id', 'DESC')->get();

    if (count($periods) > 0){

      if (!empty($_POST['period_id']) || Session::has('period_id')){

        if (!empty($_POST['period_id'])){

          $period_id = $_POST['period_id'];
          Session::put('period_id', $period_id);
        }

        else{

          $period_id = Session::get('period_id');
          Session::forget('period_id');
        }
      }

      else{

        $period_id = $periods[0]->id;
      }

      $results = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('clima_factors', 'clima_factors.id', '=', 'clima_questions.factor_id')->join('users', 'users.id', '=', 'clima_answers.user_id')->join('employees', 'employees.id', '=', 'users.employee_id')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->join('departments', 'departments.id', '=', 'areas.department_id')->join('directions', 'directions.id', '=', 'departments.direction_id')->join('clima_areas_pivot', 'clima_areas_pivot.area_id', '=', 'directions.id')->join('clima_group_areas', 'clima_group_areas.id', '=', 'clima_areas_pivot.group_area_id')->where('clima_answers.period_id', $period_id)->whereNotNull('factor_id')->select(array(DB::raw('AVG(answer) AS average'), 'clima_group_areas.id', 'clima_group_areas.name', 'clima_factors.name AS factor_name'))->groupBy('clima_factors.id', 'clima_group_areas.id')->orderBy('clima_group_areas.id')->orderBy('clima_factors.name')->get();
      $clima_factors = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('clima_factors', 'clima_factors.id', '=', 'clima_questions.factor_id')->where('period_id', $period_id)->groupBy('clima_factors.id')->orderBy('clima_factors.name')->pluck('clima_factors.name')->toArray();
      $etiquetas = DB::table('clima_etiquetas')->orderBy('valor', 'DESC')->get();
    }

    return view('clima-organizacional.reportes.heat-report-by-group-areas', compact('periods', 'period_id', 'results', 'clima_factors', 'etiquetas'));
  }
}
