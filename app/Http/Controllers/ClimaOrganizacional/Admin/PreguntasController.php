<?php

namespace App\Http\Controllers\ClimaOrganizacional\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use DB;

use App\Models\ClimaOrganizacional\Question;
use App\Models\ClimaOrganizacional\Factor;

class PreguntasController extends Controller
{
	/**
     * Create a instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
		/*$this->middleware('permission:see_questions')->only('index');
        $this->middleware('permission:create_questions')->only(['create', 'store']);
        $this->middleware('permission:edit_questions')->only(['edit', 'update']);
        $this->middleware('permission:erase_questions')->only(['destroy']);*/
    }
    
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(){
		if (auth()->user()->role != 'admin' && !auth()->user()->hasClimaPermissions(7)){
      flash('No cuenta con permisos');
      return redirect('/clima-organizacional/que-es');
    }
		$preguntas = Question::with('factor')->get();
		// $preguntas = DB::table('preguntas')->leftJoin('factores', 'preguntas.id_factor', '=', 'factores.id')->select('preguntas.id', 'pregunta', 'nombre', 'positiva')->get();
		return view('clima-organizacional/Admin/preguntas/index', compact('preguntas'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(){

		if (auth()->user()->role != 'admin' && !auth()->user()->hasClimaPermissions(7)){
      flash('No cuenta con permisos');
      return redirect('/clima-organizacional/que-es');
    }
		$factores = Factor::get();
		return view('clima-organizacional.Admin.preguntas.create', compact('factores'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request){
		try {
			Question::create([
				'question' => $request->question,
				'factor_id' => (empty($request->factor_id))?null:$request->factor_id,
				'positive' => (empty($request->positive))?0:$request->positive,
			]);
			// Flash::success('La pregunta fue guardada correctamente.');
		} catch (\Throwable $th) {
			dd($th->getMessage());
			// Flash::success('La pregunta fue guardada correctamente.');
			//throw $th;
		}
		return redirect('clima-organizacional/preguntas');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id){
		if (auth()->user()->role != 'admin' && !auth()->user()->hasClimaPermissions(7)){
      flash('No cuenta con permisos');
      return redirect('/clima-organizacional/que-es');
    }
		$pregunta = Question::findOrFail($id);
		$factores = Factor::get();
		return view('clima-organizacional.Admin.preguntas.edit', compact('pregunta', 'factores'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id){
		try {
			Question::where('id', $id)->update([
				'question' => $request->question,
				'factor_id' => (empty($request->factor_id))?null:$request->factor_id,
				'positive' => (empty($request->positive))?0:$request->positive,
			]);
			// Flash::success('La pregunta fue guardada correctamente.');
		} catch (\Throwable $th) {
			dd($th->getMessage());
			// Flash::success('La pregunta fue guardada correctamente.');
			//throw $th;
		}
		return redirect('clima-organizacional/preguntas');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
