<?php
namespace App\Http\Controllers\ClimaOrganizacional\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use DB;

use App\Models\ClimaOrganizacional\Period;
use App\Models\ClimaOrganizacional\Question;
use App\User;
use App\Employee;

class PeriodosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        /*$this->middleware('permission:see_periods')->only('index');
        $this->middleware('permission:create_periods')->only(['create', 'store']);
        $this->middleware('permission:edit_periods')->only(['edit', 'update']);
        $this->middleware('permission:erase_periods')->only(['destroy']);*/
    }

    /**
     * Muestra los periodos
     *
     */
    public function index(){

      if (auth()->user()->role != 'admin' && !auth()->user()->hasClimaPermissions(9)){
        flash('No cuenta con permisos');
        return redirect('/clima-organizacional/que-es');
      }

      // Obtiene todos los periodos
      $periodos = Period::get();
      return view('clima-organizacional.Admin.periodos.index', compact('periodos'));
    }

    /**
     * Crea un nuevo periodo
     *
     */
    public function create(){
        if (auth()->user()->role != 'admin' && !auth()->user()->hasClimaPermissions(9)){
          flash('No cuenta con permisos');
          return redirect('/clima-organizacional/que-es');
        }
        $abierto = false;
        // Obtiene los periodos con estado Abierto
        $periodos = Period::where('status', 'Abierto')->get();

      // Hay periodos con estado Abierto
        if (!empty($periodos)){
            $abierto = true;
        }
        $preguntas = Question::orderBy('id')->get();
        $evaluados = User::has('employee')->orderBy('first_name')->get();
        $enterprises = DB::table('enterprises')->orderBy('name')->get();
        return view('clima-organizacional/Admin/periodos/create', compact('preguntas', 'evaluados', 'abierto', 'enterprises'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (empty($request->no_aplica)){
          $request->no_aplica = 0;
        }
        \DB::beginTransaction();
        try {
            $result = Period::create([
                'name' => $request->name,
                'status' => $request->status,
                'enterprise_id' => $request->enterprise_id,
                'no_aplica' => $request->no_aplica
            ]);
        } catch (\Throwable $th) {
            \DB::rollback();
            return redirect('/clima-organizacional/periodos')->with('error','El periodo fue creado correctamente');
        }
        if (!empty($request->questions)){
            foreach ($request->questions as $key => $value){
                try {
                    $result->questions()->attach($value);
                } catch (\Throwable $th) {
                    \DB::rollback();
                    dd($result, $th->getMessage());
                    return redirect('/clima-organizacional/periodos')->with('error','El periodo fue creado correctamente');
                }                
            }
        }

        if (!empty($request->users)){
            foreach ($request->users as $key => $value){
                try {
                    $result->users()->attach($value, ['status' => 1]);
                } catch (\Throwable $th) {
                    \DB::rollback();
                    dd($th->getMessage());
                    return redirect('/clima-organizacional/periodos')->with('error','El periodo fue creado correctamente');
                }
            }
        }
        \DB::commit();
        return redirect('/clima-organizacional/periodos')->with('success','El periodo fue creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Edita o actualiza un periodo
     *
     */
    public function edit($id){

        if (auth()->user()->role != 'admin' && !auth()->user()->hasClimaPermissions(9)){
          flash('No cuenta con permisos');
          return redirect('/clima-organizacional/que-es');
        }

        $periodo = Period::with('questions', 'users')->findOrFail($id);

        $periodo_abierto = ($periodo->status === "Abierto")?true:false;
        $preguntas = Question::orderBy('id')->get();
        $evaluados = User::withTrashed()->has('employee_wt')->orderBy('first_name')->get();
        $users_names = User::withTrashed()->select(array(DB::raw('CONCAT(first_name, " ", last_name) AS collaborator'), 'id'))->pluck('collaborator', 'id')->toArray();

        $preguntas_periodo = $periodo->questions->pluck('id')->toArray();
        $evaluados_periodo = $periodo->users->pluck('id')->toArray();
        $users_with_answers = DB::table('clima_answers')->where('period_id', $id)->whereIn('user_id', $evaluados_periodo)->groupBy('user_id')->pluck('user_id')->toArray();
        $enterprises = DB::table('enterprises')->orderBy('name')->get();
        return view('clima-organizacional/Admin/periodos/edit', compact('preguntas', 'evaluados', 'periodo_abierto', 'periodo', 'preguntas_periodo', 'evaluados_periodo', 'enterprises', 'users_with_answers', 'users_names'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $periodo_a_editar = Period::findOrFail($id);
        if (empty($request->no_aplica)){
          $request->no_aplica = 0;
        }

        if ($periodo_a_editar->status == 'Preparatorio' && $request->status == 'Abierto'){

          DB::table('clima_employees_mirror')->where('period_id', $id)->delete();
          $offset = 0;
          $employees = Employee::whereNull('deleted_at')->limit(500)->get()->toArray();

          if (!empty($employees)){

            do{

              DB::table('clima_employees_mirror')->insert($employees);
              $offset += 500;
              $employees = Employee::whereNull('deleted_at')->offset($offset)->limit(500)->get()->toArray();
            }while(!empty($employees));

            $clima_employee_mirror = array();
            $clima_employee_mirror['period_id'] = $id;
            DB::table('clima_employees_mirror')->where('period_id', 0)->update($clima_employee_mirror);
          }
        }

        else{

          if ($request->status == 'Cancelado'){

            DB::table('clima_employees_mirror')->where('period_id', $id)->delete();
          } 
        }

        try {
            $periodo = Period::find($id);
            $periodo->update([
                'name' => $request->name,
                'status' => $request->status,
                'enterprise_id' => $request->enterprise_id,
                'no_aplica' => $request->no_aplica
            ]);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        if ($periodo_a_editar->status === "Preparatorio" || $periodo_a_editar->status == 'Abierto'){
            try {
              if ($periodo_a_editar->status === "Preparatorio"){
                $periodo->questions()->sync($request->questions);
              }
              $periodo->users()->sync($request->users);
              
            } catch (\Throwable $th) {
                \DB::rollback();
                dd($th->getMessage());
            }
        }
        \DB::commit();
        return redirect('/clima-organizacional/periodos')->with('success','El periodo fue guardado correctamente');;
    }

   /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = 0){
      
      if (!empty($_POST['id'])){

        $periodo = DB::delete('DELETE FROM clima_periods WHERE id = ?', [$_POST['id']]);
        DB::table('clima_periods')->where('id', $_POST['id'])->delete();
        DB::table('clima_period_question')->where('period_id', $_POST['id'])->delete();
        DB::table('clima_period_user')->where('period_id', $_POST['id'])->delete();
        DB::table('clima_answers')->where('period_id', $_POST['id'])->delete();
        return redirect('/clima-organizacional/periodos')->with('success','El periodo fue borrado correctamente');
      }

      $periodo = DB::table('clima_periods')->where('id', $id)->first();
      return view('clima-organizacional/Admin/periodos/delete', compact('periodo'));
    }
}
