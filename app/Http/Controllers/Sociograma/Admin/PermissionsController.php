<?php

namespace App\Http\Controllers\Sociograma\Admin;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Sociograma\Permissions;
use App\Models\Sociograma\Sections;
use App\Models\Region;
use DB;
use Session;

class PermissionsController extends Controller{

  /**
  * Muestra el listado de permisos para el Sociograma
  *
  */
  public function __construct(){
        
    $this->middleware('auth');
    /*$this->middleware('permission:see_periods')->only('index');
    $this->middleware('permission:create_periods')->only(['create', 'store']);
    $this->middleware('permission:edit_periods')->only(['edit', 'update']);
    $this->middleware('permission:destroy_periods')->only(['destroy']);*/
  }

  public function index(){

    $permissions = Permissions::all();
    return view('sociograma.Admin.permissions.index', compact('permissions'));
  }

  /**
  * Pantalla para crear un nuevo permiso para un usuario
  *
  */
  public function create(){
        
    $permissions = Permissions::all();
    $sections = Sections::all();
    $regions = Region::all();
    $users = User::where('active', 1)->whereNull('deleted_at')->select('id AS data', DB::raw('CONCAT(first_name, " ", last_name) AS value'))->get();

    return view('sociograma/Admin/permissions/create', compact('permissions', 'users', 'sections', 'regions'));
  }

  /**
  * Crea un nuevo permiso para un usuario
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request){

    \DB::beginTransaction();
    try {
      foreach ($request->section_id as $key => $section) {
      
        $result = Permissions::create([
                'region_id' => $request->region_id,
                'user_id' => $request->user_id,
                'section_id' => $section
        ]);
      }
    } catch (\Throwable $th){
      \DB::rollback();
      return redirect('/sociograma/permissions');
    }

    \DB::commit();
    flash('Permiso creado correctamente');
    return redirect('/sociograma/permissions');
  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Period $id)
    {
        //
    }

    /**
     * Edita o actualiza un permiso
     *
     */
    /*
    Fecha: 01-04-2020
    Modificado por: Mike Yáñez
    */
    public function edit($id){
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

   /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $permission = Permissions::findOrFail($id);
        \DB::beginTransaction();
        try {
            $permission->delete();
            \DB::commit();
        } catch (\Throwable $th) {
            \DB::rollback();
        }
        flash('El permiso fue borrado correctamente');
        return redirect('/sociograma/permissions');
    }
}
