<?php
namespace App\Http\Controllers\Sociograma\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use DB;

use App\Models\Sociograma\SociogramaPeriod;
use App\Models\Sociograma\Question;
use App\User;
use App\Employee;

class PeriodosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        /*$this->middleware('permission:see_periods')->only('index');
        $this->middleware('permission:create_periods')->only(['create', 'store']);
        $this->middleware('permission:edit_periods')->only(['edit', 'update']);
        $this->middleware('permission:erase_periods')->only(['destroy']);*/
    }

    /**
     * Muestra los periodos
     *
     */
    public function index(){

      if (auth()->user()->role != 'admin' && !auth()->user()->hasSociogramaPermissions(4)){
        flash('No cuenta con permisos');
        return redirect('/sociograma/que-es');
      }

      // Obtiene todos los periodos
      $periodos = SociogramaPeriod::get();
      return view('sociograma.Admin.periodos.index', compact('periodos'));
    }

    /**
     * Crea un nuevo periodo
     *
     */
    public function create(){

        if (auth()->user()->role != 'admin' && !auth()->user()->hasSociogramaPermissions(4)){
          flash('No cuenta con permisos');
          return redirect('/sociograma/que-es');
        }

        $abierto = false;
        // Obtiene los periodos con estado Abierto
        $periodos = SociogramaPeriod::where('status', 'Abierto')->get();

      // Hay periodos con estado Abierto
        if (!empty($periodos)){
            $abierto = true;
        }
        $preguntas = Question::orderBy('id')->get();
        $evaluados = User::has('employee')->orderBy('first_name')->get();
        $enterprises = DB::table('enterprises')->orderBy('name')->get();
        return view('sociograma/Admin/periodos/create', compact('preguntas', 'evaluados', 'abierto', 'enterprises'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (empty($request->no_aplica)){
          $request->no_aplica = 0;
        }
        \DB::beginTransaction();
        try {
            $result = SociogramaPeriod::create([
                'name' => $request->name,
                'status' => $request->status,
                'enterprise_id' => $request->enterprise_id,
                'no_aplica' => $request->no_aplica
            ]);
        } catch (\Throwable $th) {
            \DB::rollback();
            return redirect('/sociograma/periodos')->with('error','El periodo fue creado correctamente');
        }
        if (!empty($request->questions)){
            foreach ($request->questions as $key => $value){
                try {
                    $result->questions()->attach($value);
                } catch (\Throwable $th) {
                    \DB::rollback();
                    dd($result, $th->getMessage());
                    return redirect('/sociograma/periodos')->with('error','El periodo fue creado correctamente');
                }                
            }
        }

        if (!empty($request->users)){
            foreach ($request->users as $key => $value){
                try {
                    $result->users()->attach($value, ['status' => 1]);
                } catch (\Throwable $th) {
                    \DB::rollback();
                    dd($th->getMessage());
                    return redirect('/sociograma/periodos')->with('error','El periodo fue creado correctamente');
                }
            }
        }
        \DB::commit();
        return redirect('/sociograma/periodos')->with('success','El periodo fue creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Edita o actualiza un periodo
     *
     */
    public function edit($id){

        if (auth()->user()->role != 'admin' && !auth()->user()->hasSociogramaPermissions(4)){
          flash('No cuenta con permisos');
          return redirect('/sociograma/que-es');
        }

        $periodo = SociogramaPeriod::with('questions', 'users')->findOrFail($id);

        $periodo_abierto = ($periodo->status === "Abierto")?true:false;
        $preguntas = Question::orderBy('id')->get();
        $evaluados = User::with('employee')->has('employee')->orderBy('first_name')->get();

        $preguntas_periodo = $periodo->questions->pluck('id')->toArray();
        $evaluados_periodo = $periodo->users->pluck('id')->toArray();
        $enterprises = DB::table('enterprises')->orderBy('name')->get();
        return view('sociograma/Admin/periodos/edit', compact('preguntas', 'evaluados', 'periodo_abierto', 'periodo', 'preguntas_periodo', 'evaluados_periodo', 'enterprises'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $periodo_a_editar = SociogramaPeriod::findOrFail($id);
        if (empty($request->no_aplica)){
          $request->no_aplica = 0;
        }

        if ($periodo_a_editar->status == 'Preparatorio' && $request->status == 'Abierto'){

          DB::table('sociograma_employees_mirror')->where('period_id', $id)->delete();
          $offset = 0;
          $employees = Employee::whereNull('deleted_at')->limit(500)->get()->toArray();

          if (!empty($employees)){

            do{

              DB::table('sociograma_employees_mirror')->insert($employees);
              $offset += 500;
              $employees = Employee::whereNull('deleted_at')->offset($offset)->limit(500)->get()->toArray();
            }while(!empty($employees));

            $sociograma_employee_mirror = array();
            $sociograma_employee_mirror['period_id'] = $id;
            DB::table('sociograma_employees_mirror')->where('period_id', 0)->update($sociograma_employee_mirror);
          }
        }

        else{

          if ($request->status == 'Cancelado'){

            DB::table('sociograma_employees_mirror')->where('period_id', $id)->delete();
          } 
        }

        try {
            $periodo = SociogramaPeriod::find($id);
            $periodo->update([
                'name' => $request->name,
                'status' => $request->status,
                'enterprise_id' => $request->enterprise_id,
                'no_aplica' => $request->no_aplica
            ]);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        if ($periodo_a_editar->status === "Preparatorio"){
            try {
                $periodo->questions()->sync($request->questions);
                $periodo->users()->sync($request->users);
            } catch (\Throwable $th) {
                \DB::rollback();
                dd($th->getMessage());
            }
        }
        \DB::commit();
        return redirect('/sociograma/periodos')->with('success','El periodo fue guardado correctamente');;
    }

   /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = 0){
      
      if (!empty($_POST['id'])){

        $periodo = DB::delete('DELETE FROM sociograma_periods WHERE id = ?', [$_POST['id']]);
        DB::table('sociograma_periods')->where('id', $_POST['id'])->delete();
        DB::table('sociograma_period_question')->where('period_id', $_POST['id'])->delete();
        DB::table('sociograma_period_user')->where('period_id', $_POST['id'])->delete();
        DB::table('sociograma_answers')->where('period_id', $_POST['id'])->delete();
        return redirect('/sociograma/periodos')->with('success','El periodo fue borrado correctamente');
      }

      $periodo = DB::table('sociograma_periods')->where('id', $id)->first();
      return view('sociograma/Admin/periodos/delete', compact('periodo'));
    }
}
