<?php

namespace App\Http\Controllers\ClimaOrganizacional\Groups;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\ClimaOrganizacional\GroupArea;
use App\Models\ClimaOrganizacional\AreaPivot;
use App\Models\ClimaOrganizacional\Period;
use App\Models\Direction;
use Session;
use DB;

class GroupAreasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $periods = Period::where('status', '<>', 'Cancelado')->orderBy('id', 'DESC')->get();
      $period_id = 0;
      $groups = $areas_without_group = array();

      if (count($periods) > 0){

        if (!empty($_POST['period_id']) || Session::has('period_id')){

          if (!empty($_POST['period_id'])){

            $period_id = $_POST['period_id'];
            Session::put('period_id', $period_id);
          }

          else{

            $period_id = Session::get('period_id');
          }
        }

        else{

          $period_id = $periods[0]->id;
          Session::put('period_id', $period_id);
        }

        $groups = GroupArea::where('period_id', $period_id)->pluck('id')->toArray();
        $areas_in_group = AreaPivot::whereIn('group_area_id', $groups)->pluck('area_id')->toArray();
        $areas = DB::table('clima_period_user')->join('users', 'users.id', '=', 'clima_period_user.user_id')->join('employees', 'employees.id', '=', 'users.employee_id')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->join('departments', 'departments.id', '=', 'areas.department_id')->where('clima_period_user.period_id', $period_id)->groupBy('departments.direction_id')->pluck('departments.direction_id')->toArray();
        $areas = Direction::whereIn('id', $areas)->orderBy('name')->get();

        foreach ($areas as $key => $value){

          if (!in_array($value->id, $areas_in_group)){

            $areas_without_group[] = $value;
          }
        }

        $groups = GroupArea::where('period_id', $period_id)->get();
      }

      return view('clima-organizacional.groups.areas.index', compact('groups', 'areas_without_group', 'periods', 'period_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groups = GroupArea::where('period_id', Session::get('period_id'))->pluck('id')->toArray();
        $areas_in_group = AreaPivot::whereIn('group_area_id', $groups)->pluck('area_id')->toArray();
        $areas = DB::table('clima_period_user')->join('users', 'users.id', '=', 'clima_period_user.user_id')->join('employees', 'employees.id', '=', 'users.employee_id')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->join('departments', 'departments.id', '=', 'areas.department_id')->where('clima_period_user.period_id', Session::get('period_id'))->groupBy('departments.direction_id')->pluck('departments.direction_id')->toArray();
        $area = Direction::whereIn('id', $areas)->orderBy('name')->get();
        $areas = array();

        foreach ($area as $key => $value){

          if (!in_array($value->id, $areas_in_group)){

            $areas[] = $value;
          }
        }

        return view('clima-organizacional.groups.areas.create', compact('areas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \DB::beginTransaction();
        try {
            $group = GroupArea::create([
                'name' => $request->name,
                'description' => $request->description,
                'period_id' => Session::get('period_id')
            ]);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        try {
            $group->areas()->attach($request->areas);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        \DB::commit();
        flash('Guardado correctamente')->success();
        return redirect('/clima-organizacional/grupos_areas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group = GroupArea::with('areas')->findOrFail($id);
        return view('clima-organizacional.groups.areas.show', compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = GroupArea::with('areas')->findOrFail($id);
        $groupAreas =  $group->areas()->pluck('id')->toArray();
        $areas_in_group = AreaPivot::pluck('area_id')->toArray();
        $areas = DB::table('clima_period_user')->join('users', 'users.id', '=', 'clima_period_user.user_id')->join('employees', 'employees.id', '=', 'users.employee_id')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->join('departments', 'departments.id', '=', 'areas.department_id')->where('clima_period_user.period_id', Session::get('period_id'))->groupBy('departments.direction_id')->pluck('departments.direction_id')->toArray();
        $areas = Direction::whereIn('id', $areas)->orderBy('name')->get();
        $areas_without_group = array();

        foreach ($areas as $key => $value){

          if (!in_array($value->id, $areas_in_group)){

            $areas_without_group[] = $value->id;
          }
        }
        return view('clima-organizacional.groups.areas.edit', compact('group', 'groupAreas', 'areas', 'areas_without_group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $group = GroupArea::with('areas')->findOrFail($id);
        \DB::beginTransaction();
        try {
            $group->update([
                'name' => $request->name,
                'description' => $request->description,
            ]);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        try {
            $group->areas()->sync($request->areas);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        \DB::commit();
        flash('Actualizado correctamente')->success();
        return redirect()->to('clima-organizacional/grupos_areas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = GroupArea::findOrFail($id);
        \DB::beginTransaction();
        try {
            $group->areas()->detach();
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        try {
            $group->delete();
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        \DB::commit();
        flash('Eliminado correctamente')->success();
        return redirect()->to('clima-organizacional/grupos_areas');
    }
}
