<?php

namespace App\Http\Controllers\ClimaOrganizacional\Groups;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\ClimaOrganizacional\GroupDepartment;
use App\Models\ClimaOrganizacional\DepartmentPivot;
use App\Models\ClimaOrganizacional\Period;
use App\Models\Department;
use Session;
use DB;

class GroupDepartmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $periods = Period::where('status', '<>', 'Cancelado')->orderBy('id', 'DESC')->get();
        $period_id = 0;
        $groups = $departments_without_group = array();

        if (count($periods) > 0){

          if (!empty($_POST['period_id']) || Session::has('period_id')){

            if (!empty($_POST['period_id'])){

              $period_id = $_POST['period_id'];
              Session::put('period_id', $period_id);
            }

            else{

              $period_id = Session::get('period_id');
            }
          }

          else{

            $period_id = $periods[0]->id;
            Session::put('period_id', $period_id);
          }

          $groups = GroupDepartment::pluck('id')->toArray();
          $departments_in_group = DepartmentPivot::whereIn('group_department_id', $groups)->pluck('department_id')->toArray();
          $departments = DB::table('clima_period_user')->join('users', 'users.id', '=', 'clima_period_user.user_id')->join('employees', 'employees.id', '=', 'users.employee_id')->join('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->join('areas', 'areas.id', '=', 'job_positions.area_id')->where('clima_period_user.period_id', $period_id)->groupBy('areas.department_id')->pluck('areas.department_id')->toArray();
          $departments = Department::whereIn('id', $departments)->orderBy('name')->get();

          foreach ($departments as $key => $value){

            if (!in_array($value->id, $departments_in_group)){

              $departments_without_group[] = $value;
            }
          }
        }

        return view('clima-organizacional.groups.departments.index', compact('groups', 'departments_without_group', 'periods', 'period_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments_in_group = DepartmentPivot::pluck('department_id')->toArray();
        $department = Department::orderBy('name')->get();
        $departments_without_group = array();
        $departments = array();

        foreach ($department as $key => $value){

          if (!in_array($value->id, $departments_in_group)){

            $departments[] = $value;
          }
        }

        return view('clima-organizacional.groups.departments.create', compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \DB::beginTransaction();
        try {
            $group = GroupDepartment::create([
                'name' => $request->name,
                'description' => $request->description,
            ]);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        try {
            $group->departments()->attach($request->departments);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        \DB::commit();
        flash('Guardado correctamente')->success();
        return redirect()->to('clima-organizacional/grupos_departamentos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group = GroupDepartment::with('departments')->findOrFail($id);
        return view('clima-organizacional.groups.departments.show', compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = GroupDepartment::with('departments')->findOrFail($id);
        $groupDepartments =  $group->departments()->pluck('id')->toArray();
        $departments_in_group = DepartmentPivot::pluck('department_id')->toArray();
        $departments = Department::orderBy('name')->get();
        $departments_without_group = array();

        foreach ($departments as $key => $value){

          if (!in_array($value->id, $departments_in_group)){

            $departments_without_group[] = $value->id;
          }
        }
        return view('clima-organizacional.groups.departments.edit', compact('group', 'groupDepartments', 'departments', 'departments_without_group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $group = GroupDepartment::with('departments')->findOrFail($id);
        \DB::beginTransaction();
        try {
            $group->update([
                'name' => $request->name,
                'description' => $request->description,
            ]);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        try {
            $group->departments()->sync($request->departments);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        \DB::commit();
        flash('Actualizado correctamente')->success();
        return redirect()->to('clima-organizacional/grupos_departamentos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = GroupDepartment::findOrFail($id);
        \DB::beginTransaction();
        try {
            $group->departments()->detach();
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        try {
            $group->delete();
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        \DB::commit();
        flash('Eliminado correctamente')->success();
        return redirect()->to('clima-organizacional/grupos_departamentos');
    }
}
