<?php

namespace App\Http\Controllers\Sociograma;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Employee;
use App\Models\JobPosition;
use App\Models\Area;
use App\Models\Department;
use App\Models\Direction;
use App\Models\Sociograma\Factor;
use App\Models\Sociograma\SociogramaPeriod;
use App\Models\Sociograma\Answer;
use App\Models\Sociograma\AreaPivot;
use App\Models\Sociograma\DepartmentPivot;
use App\Models\Sociograma\JobPositionPivot;
use App\Models\Sociograma\GroupArea;
use App\Models\Sociograma\GroupDepartment;
use App\Models\Sociograma\GroupJobPosition;
use App\Models\Announcement\Announcement;
use App\Models\Announcement\AnnouncementType;
use App\Models\Announcement\AnnouncementCategory;
use App\Models\Announcement\View;
use DB;
use Carbon\Carbon;

class ReportesSociogramaController extends Controller
{
    /**
     * Create a new Evaluacion Desempeno controller instance.
     *
     * @return void2
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('permission:see_progress')->only('avances');
    }

    
  public function get_periodos()
  {
    $periodos = SociogramaPeriod::whereIn('status', ['Cerrado','Abierto'])->get();     

    return response()->json([       
      'periodos'=>$periodos   
    ]);

  }
  
  public function sociograma(Request $request)
  {
        // require_once 'PHPExcel.php';
        // require_once 'PHPExcel/IOFactory.php';
 
        try {
          //code...
      
        $id_periodo = 0;
        $periodos = SociogramaPeriod::whereIn('status', ['Cerrado','Abierto'])->get();
        
        if (count($periodos) > 0){
  
          $id_periodo = $periodos[0]->id;
  
          if (!empty($request->id_periodo)){
  
            $id_periodo = $request->id_periodo;

          }
          
        }
          


        $resultados = array();
        $title = '';



        $periodos = SociogramaPeriod::get();
        $dptos_temp = [];
        $dptos = [];
        $areas_temp = [];
        $areas = [];
        $puestos = [];
        $puestos_trabajo_temp = [];
        $puestos_trabajo = [];
        $temp_centros_trabajo = [];
        $centros_trabajo = [];
        $regiones = [];
        $temp_regiones = [];
        $turnos_temp = [];
        $turnos = [];
        $dates_antiguedad= [];
        $users_ids= [];
        $evaluados= [];
        $terminados= [];
        $questions= [];
        $preguntas= [];

        $periodoAbierto = SociogramaPeriod::find($id_periodo);
        if ($periodoAbierto) {
          $users_ids = $periodoAbierto->users()
          ->wherePivot('status', 3)
          ->pluck('users.id')->toArray();
        }

        if (!is_null($request->antiguedad)){

          $dates_antiguedad = $this->antiquityDates($request->antiguedad);
       
        }


        $NpsPeriod = SociogramaPeriod::where('id', $id_periodo)->first();
        if ($NpsPeriod) {
          
          $evaluados = $NpsPeriod
          ->users()
          ->with(['employee' => function($q) {
            $q->orderBy('nombre');
          }])
          ->get();
          
          $terminados = $NpsPeriod->users()
          ->wherePivot('status', 3)
          ->pluck('users.id')->toArray();
        }

        $NpsPeriod = SociogramaPeriod::
        where('id', $id_periodo)
        ->with(['answers' => function($query_0) use($request,$dates_antiguedad,$terminados,$users_ids){
          $query_0->whereIn('user_id',$terminados);      
          $query_0->whereHas('user.employee_wt', function($q1) use($request,$dates_antiguedad){
            $q1->when(!is_null($request->dpto) && !is_null($request->area), function($q2) use($request){
              $q2->whereHas('jobPosition', function($q3) use($request){
                $q3->whereHas('area', function($q4) use($request){
                  $q4->where('department_id', $request->dpto);
                });
              }); 
            });
            $q1->when(!is_null($request->dpto) && is_null($request->area), function($q2) use($request){
              $q2->whereHas('jobPosition', function($q3) use($request){
                $q3->whereHas('area', function($q4) use($request){
                  $q4->whereHas('department_wt', function($q5) use($request){
                    $q5->where('name', $request->dpto);
                  }); 
                });
              }); 
            });
            $q1->when(!is_null($request->area), function($q2) use($request){
              $q2->whereHas('jobPosition', function($q3) use($request){
                $q3->whereHas('area', function($q4) use($request){
                  $q4->whereHas('department', function($q5) use($request){
                    $q5->where('direction_id', $request->area);
                  });
                });
              });
            });
            $q1->when(!is_null($request->centro_trabajo), function($q2) use($request){
                $q2->where('sucursal', $request->centro_trabajo);
            });
            $q1->when(!is_null($request->turno), function($q2) use($request){
                $q2->where('turno', $request->turno);
            });
            $q1->when(!is_null($request->puesto), function($q2) use($request){
              $q2->whereHas('jobPosition', function($q3) use($request){
                $q3->where('name', $request->puesto);
              });
            });
            $q1->when(!is_null($request->region), function($q2) use($request){
              $q2->where('region_id', $request->region);
            });
            $q1->when(!is_null($request->antiguedad), function($q2) use($dates_antiguedad){
                $q2->whereBetween('ingreso', [$dates_antiguedad['start_date'], $dates_antiguedad['end_date']]);
            });
             
          });     
        }]) 
        ->first();

        if ($NpsPeriod) {
          
          $resultados = $NpsPeriod->answers;
          $users_ids = $NpsPeriod->answers->pluck('user_id')->toArray();
          $questions = $NpsPeriod->questions;
      
        }

         
        $ids_participacion = [];        
        $real_now = Carbon::now();
        $total_participacion = 0;
        $temp_starts = [];        
        $starts = [];        

        $Users = User::whereIn('id',$users_ids)->get();


        // if(count($resultados)>0){
          foreach ($Users as $key => $user){      
   
            if(!empty($user->employee_wt->jobPosition) && !in_array($user->employee_wt->jobPosition->name, $puestos_trabajo)){               
              $puestos_trabajo_temp[] = $user->employee_wt->jobPosition->name;
              $puestos_trabajo[] = ['id'=>$user->employee_wt->jobPosition->id,'name'=>$user->employee_wt->jobPosition->name]; 
                // $puestos_trabajo[] = $job_level->id; 
        
            }
            if(!empty($user->employee_wt->jobPosition) && !empty($user->employee_wt->jobPosition->area) && !in_array($user->employee_wt->jobPosition->area->department->id, $dptos_temp)){ 
              $dptos_temp[] = $user->employee_wt->jobPosition->area->department->id; 
              $dptos[] = ['id'=>$user->employee_wt->jobPosition->area->department->id,'name'=>$user->employee_wt->jobPosition->area->department->name]; 
            }

            if(!empty($user->employee_wt->turno) && !in_array($user->employee_wt->turno, $turnos_temp)){ 
              $turnos_temp[] = $user->employee_wt->turno; 
              $turnos[] = ['id'=>$user->employee_wt->turno,'name'=>$user->employee_wt->turno]; 
            }

            if (!empty($user->employee_wt->jobPosition) && !empty($user->employee_wt->jobPosition->area->department->direction)){
              $direction = $user->employee_wt->jobPosition->area->department->direction;
              if(!in_array($direction->id, $areas_temp)){ 
                $areas_temp[] = $direction->id;
              $areas[] = ['id'=>$direction->id,'name'=>$direction->name]; 
            }
          } 
          

            if(!empty($user->employee->sucursal) && !in_array($user->employee->sucursal, $temp_centros_trabajo)){

              $temp_centros_trabajo[] = $user->employee->sucursal;
              $centros_trabajo[] = ['id'=>$user->employee->sucursal,'name'=>$user->employee->sucursal];
            }
    
            if(!empty($user->employee->region_id) && !in_array($user->employee->region_id, $temp_regiones)){

              $temp_regiones[] = $user->employee->region_id;
              $regiones[] = ['id'=>$user->employee->region->id,'name'=>$user->employee->region->name];
            }

            $entry = Carbon::parse($user->employee_wt->ingreso);     
            $antiquity = $real_now->diffInDays($entry)/365;              
            if($antiquity < 1) {
                $years_key = 'less_than_a_year';      
                $years_name = 'A. Un año o menos'; 
            }
            elseif($antiquity >= 1 && $antiquity <= 5) {    
              $years_key = 'one_to_five_years';      
              $years_name = 'B. Un año un día a cinco años';      
            } elseif($antiquity > 5 && $antiquity <= 10) {
              $years_key = 'five_to_ten_years';      
              $years_name = 'C. Cinco años un día a diez años';    
            } elseif($antiquity > 10) {
              $years_key = 'ten_to_fifteen_years';      
              $years_name = 'D. Diez años un día y más';     
            }
          
            if(!in_array($years_key, $temp_starts)){            
              $starts[] = ['id'=>$years_key,'name'=>$years_name];
              $temp_starts[] = $years_key;
            }
            
          }

        // }

        $mas_votados = [];
        foreach ($questions as $key => $value) {
          
              $question_id = $value->id;
                   
              $mas_votados = Answer::
              where('question_id', $question_id)
              ->where('period_id', $id_periodo)
              ->whereIn('user_id', $users_ids)
              ->selectRaw('answer as id , COUNT(*) as total')
              ->groupBy('answer')
              ->orderBy('total','DESC')
              ->limit(10)
              ->get();

            $personas = [];

            foreach ($mas_votados as $key => $votados) {
              $user = User::withTrashed()->whereId($votados->id)->first();
              $employee = $user->employee_wt;
              $personas[] = [
                'id' => $employee->id,
                'name' => $employee->FullName,
                'dpto' => $employee->jobPosition->area->department->name,
                'puesto' => $employee->jobPosition->name,
                'total' => $votados->total
              ];
            }


            $preguntas[] = [
              'id' => $value->id,
              'name' => $value->short_name, 
              'mas_votados' => $personas,
              'mas_votsados' => $mas_votados,
              'users_ids' => $users_ids
              
            ];

 
        }
 

        $dptos = $this->sortArrayByKey($dptos, 'name');
        $areas = $this->sortArrayByKey($areas, 'name');
        $centros_trabajo = $this->sortArrayByKey($centros_trabajo, 'name');
        $regiones = $this->sortArrayByKey($regiones, 'name');     
        $starts = $this->sortArrayByKey($starts, 'name');
     
        $puestos = $this->sortArrayByKey($puestos_trabajo, 'name');
  
        
        return response()->json([
          'Users'=>$Users,  
          'resultados'=>$resultados,  
          'periodos'=>$periodos,
          'id_periodo'=>$id_periodo,
          'dptos'=>$dptos,
          'areas'=>$areas,
          'puestos'=>$puestos,
          'turnos'=>$turnos,
          'antiguedades'=>$starts,    
          'centros_trabajo'=>$centros_trabajo,
          'regiones'=>$regiones,    
          'preguntas'=>$preguntas,    
          'ids_participacion'=>$ids_participacion,    
        ]);

      } catch (\Throwable $th) {
        
        return response()->json([
          'result'=>false,  
          'msg'=>$th->getMessage()
        ]);
      }

  }
  
  public function cargar_empleados_periodo($period_id)
  { 

        $evaluados = DB::table('sociograma_period_user')->join('users', 'users.id', '=', 'sociograma_period_user.user_id')->join('employees', 'employees.id', '=', 'users.employee_id')->where('period_id', $period_id)->where('status', 3)->select(DB::raw("CONCAT(nombre,' ',paterno,' ',materno) as nombre"),'employees.idempleado as id' )->orderBy('nombre','ASC')->get();

        return response()->json([
          'evaluados'=>$evaluados
        ]);

  }

  public function sociograma_individual(Request $request)
  { 
 
        $id_empleado = $request->id_empleado; 
        $id_periodo = $request->id_periodo; 
        

        $employee = Employee::whereIdempleado($id_empleado)->first();
 
        if ( !$employee ) {
          return response()->json([
            'result'=>false,
            'id_empleado'=>$id_empleado,  
            'empleado'=>[
              'id' => null,
              'nombre' => null,
              'puesto' => null,
              'departmento' => null,
              'hospital' => null,
            ],  
            'id_periodo'=>null,  
            'resultados'=>[],  
            'questions'=>[]
          ]);
        }else{
          $empleado = [
            'id' => $employee->idempleado,
            'nombre' => $employee->user->FullName,
            'puesto' => $employee->getPuestoName(true),
            'departmento' => $employee->getDepartmentName(),
            'hospital' => $employee->getRegionName()
          ];
        }

        $user_id = $employee->user->id;

        $NpsPeriod = SociogramaPeriod::
        where('id', $id_periodo)
        ->with(['answers' => function($query_0) use($user_id){
          $query_0->where('user_id',$user_id);      
          
        }]) 
        ->first();

        // $resultados = $NpsPeriod->answers;
        // $users_ids = $NpsPeriod->answers->pluck('user_id')->toArray();
        $questions = $NpsPeriod->questions;
        $resultados = [];


        foreach ($questions as $key => $value) {
          
              $question_id = $value->id;
                   
              $mi_votacion_por_factor = Answer::
              where('question_id', $question_id)
              ->where('period_id', $id_periodo)
              ->where('user_id', $user_id)
              ->with('respuesta_empleado')
              ->get();


              foreach ($mi_votacion_por_factor as $keyvalue_factor => $value_factor) {

                $value_factor->votaron_por_el = $this->saber_cuanto_votaron_por_un_empleado($id_periodo, $question_id, $value_factor->answer,'answer');

              }  

              $sus_votacion_por_factor = Answer::
              where('question_id', $question_id)
              ->where('period_id', $id_periodo)
              ->where('answer', $user_id)
              ->with('user.employee_wt')
              ->get();

              foreach ($sus_votacion_por_factor as $keyvalue_factor => $value_factor) {
                $value_factor->votaron_por_el = $this->saber_cuanto_votaron_por_un_empleado($id_periodo, $question_id, $value_factor->user_id,'answer');
              }  



            $colores = ['bg-success','bg-warning','bg-info'];

             foreach ($mi_votacion_por_factor as $key => $mi_votacion) {

                foreach ($sus_votacion_por_factor as $key2 => $sus_votacion) {

                  // $sus_votacion->color = '';

                  if($mi_votacion->respuesta_empleado->id==$sus_votacion->user_id){
                    $sus_votacion->color = $colores[$key];
                    $mi_votacion->color = $colores[$key];
                  }
 
                }  

             }





              $resultados[$value->short_name] = [
                'name' => $value->short_name, 
                'mis_respuestas' => $mi_votacion_por_factor, 
                'sus_respuestas' => $sus_votacion_por_factor
              ];
 
        }


        return response()->json([
          'result'=>true,
          'id_empleado'=>$id_empleado,  
          'empleado'=>$empleado,  
          'id_periodo'=>$id_periodo,  
          'resultados'=>$resultados,  
          'questions'=>$questions,  
        ]);


  }

  public function exportar_resultados($id_periodo, $grupo_departamento = '', $grupo_area = '', $grupo_puesto = '', $direccion = '', $departamento = '', $area = '', $puesto = '')
  {
        require_once 'PHPExcel.php';
        require_once 'PHPExcel/IOFactory.php';

        $resultados = array();
        $title = '';

        if (empty($grupo_area) && empty($departamento) && empty($puesto)){

          $resultados = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('users', 'clima_answers.user_id', '=', 'users.id')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->where('factor_id', 0)->where('period_id', $id_periodo)->select('answer', 'question', 'departments.name')->orderBy('departments.name')->orderBy('clima_questions.id')->get();
        }

        else{

          if (!empty($departamento) && !empty($puesto)){

            $department = Department::find($departamento);
            $job_position = JobPosition::find($puesto);
            $title = ' Departamento ' . $department->name . ' y Puesto ' . $job_position->name;
            $resultados = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('users', 'clima_answers.user_id', '=', 'users.id')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->where('factor_id', 0)->where('period_id', $id_periodo)->where('department_id', $departamento)->where('job_position_id', $puesto)->select('answer', 'question', 'departments.name')->orderBy('departments.name')->orderBy('clima_questions.id')->get();
          }

          else{

            if (!empty($departamento)){

              $department = Department::find($departamento);
              $title = ' Departamento ' . $department->name;
              $resultados = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('users', 'clima_answers.user_id', '=', 'users.id')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->where('factor_id', 0)->where('period_id', $id_periodo)->where('department_id', $departamento)->select('answer', 'question', 'departments.name')->orderBy('departments.name')->orderBy('clima_questions.id')->get();
            }

            else{

              if (!empty($puesto)){

                $job_position = JobPosition::find($puesto);
                $title = ' Puesto ' . $job_position->name;
                $resultados = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('users', 'clima_answers.user_id', '=', 'users.id')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->where('factor_id', 0)->where('period_id', $id_periodo)->where('job_position_id', $puesto)->select('answer', 'question', 'departments.name')->orderBy('departments.name')->orderBy('clima_questions.id')->get();
              }

              else{

                if (!empty($grupo_area)){

                  $grupo = GruposAreas::find($grupo_area);
                  $title = ' Grupo de Área ' . $grupo->name;
                  $resultados = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('users', 'clima_answers.user_id', '=', 'users.id')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->leftJoin('clima_areas_pivot', 'clima_areas_pivot.area_id', '=', 'areas.id')->leftJoin('clima_group_areas', 'clima_group_areas.id', '=', 'clima_areas_pivot.group_area_id')->where('factor_id', 0)->where('period_id', $id_periodo)->where('clima_group_areas.id', $grupo_area)->select('answer', 'question', 'departments.name')->orderBy('departments.name')->orderBy('clima_questions.id')->get();
                }
              }
            } 
          }
        }

        // Create new PHPExcel object
        $objPHPExcel = new \PHPExcel();

        // Create a first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Departamento');  
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Factor');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Pregunta');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Respuesta');
        $i = 2;

        foreach ($resultados as $key => $value){
            
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $value->name);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, 'Pregunta Abierta');
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $value->question);
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $value->answer);
        $i++;
        }

        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Respuestas Abiertas');

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Respuestas Encuesta' . $title . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
  }

  
  private function sortArrayByKey($array,$key){
      
      $groupedItems = []; 
      $Items = []; 
      foreach ($array as $item) {
          $pool = $item[$key];
          $groupedItems[$pool][] = $item;
      }
      ksort($groupedItems);

      foreach ($groupedItems as $key => $value) {
        $Items[] = $value[0];
      }

      return $Items;
  } 

  private function antiquityDates($ingreso){
      
    
        $fecha_actual = date('Y-m-d');

        $end_date = $start_date = 0;

        $antiquity = $ingreso;

        if($antiquity =='less_than_a_year') {
            $end_date = date('Y-m-d');  
            $start_date = date("Y-m-d",strtotime($fecha_actual."- 365 days"));             
        }
        else if($antiquity =='one_to_five_years') {    
          $end_date = date("Y-m-d",strtotime($fecha_actual."- 366 days"));  
          $start_date = date("Y-m-d",strtotime($fecha_actual."- 1825 days"));   
        } else if($antiquity =='five_to_ten_years') {
          $end_date = date("Y-m-d",strtotime($fecha_actual."- 1826 days"));  
          $start_date = date("Y-m-d",strtotime($fecha_actual."- 3650 days"));   
        } else if($antiquity =='ten_to_fifteen_years') {
          $end_date = date("Y-m-d",strtotime($fecha_actual."- 3650 days"));  
          $start_date = date("Y-m-d",strtotime($fecha_actual."- 7300 days")); 
        }

        return ['start_date'=>$start_date,'end_date'=>$end_date];
  } 
 

  private function saber_cuanto_votaron_por_un_empleado($id_periodo, $question_id, $empleado,$key){
      
      return Answer::
      where('question_id', $question_id)
      ->where('period_id', $id_periodo)
      ->where($key,$empleado)
      ->count();

  } 
 

  
  public function exportar_sociograma(Request $request)
  {
      // dd($request->all());
        require_once 'PHPExcel.php';
        require_once 'PHPExcel/IOFactory.php';

        
        $id_periodo = 0;
        $periodos = SociogramaPeriod::whereIn('status', ['Cerrado','Abierto'])->get();
        
        if (count($periodos) > 0){
  
          $id_periodo = $periodos[0]->id;
  
          if (!empty($request->periodos)){
  
            $id_periodo = $request->periodos;

          }
          
        }
          


        $resultados = array();
        $title = '';



        $periodos = SociogramaPeriod::get();
        $dptos_temp = [];
        $dptos = [];
        $areas_temp = [];
        $areas = [];
        $puestos = [];
        $puestos_trabajo_temp = [];
        $puestos_trabajo = [];
        $temp_centros_trabajo = [];
        $centros_trabajo = [];
        $regiones = [];
        $temp_regiones = [];
        $turnos_temp = [];
        $turnos = [];
        $dates_antiguedad= [];
        $users_ids= [];
        $evaluados= [];
        $terminados= [];
        $questions= [];
        $preguntas= [];

        $periodoAbierto = SociogramaPeriod::find($id_periodo);
        if ($periodoAbierto) {
          $users_ids = $periodoAbierto->users()
          ->wherePivot('status', 3)
          ->pluck('users.id')->toArray();
        }

        if (!is_null($request->antiguedad)){

          $dates_antiguedad = $this->antiquityDates($request->antiguedad);
       
        }


        $NpsPeriod = SociogramaPeriod::where('id', $id_periodo)->first();
        if ($NpsPeriod) {
          
          $evaluados = $NpsPeriod
          ->users()
          ->with(['employee' => function($q) {
            $q->orderBy('nombre');
          }])
          ->get();
          
          $terminados = $NpsPeriod->users()
          ->wherePivot('status', 3)
          ->pluck('users.id')->toArray();
        }

        $NpsPeriod = SociogramaPeriod::
        where('id', $id_periodo)
        ->with(['answers' => function($query_0) use($request,$dates_antiguedad,$terminados,$users_ids){
          $query_0->whereIn('user_id',$terminados);      
          $query_0->whereHas('user.employee_wt', function($q1) use($request,$dates_antiguedad){
            $q1->when(!is_null($request->dpto) && !is_null($request->area), function($q2) use($request){
              $q2->whereHas('jobPosition', function($q3) use($request){
                $q3->whereHas('area', function($q4) use($request){
                  $q4->where('department_id', $request->dpto);
                });
              }); 
            });
            $q1->when(!is_null($request->dpto) && is_null($request->area), function($q2) use($request){
              $q2->whereHas('jobPosition', function($q3) use($request){
                $q3->whereHas('area', function($q4) use($request){
                  $q4->whereHas('department_wt', function($q5) use($request){
                    $q5->where('name', $request->dpto);
                  }); 
                });
              }); 
            });
            $q1->when(!is_null($request->area), function($q2) use($request){
              $q2->whereHas('jobPosition', function($q3) use($request){
                $q3->whereHas('area', function($q4) use($request){
                  $q4->whereHas('department', function($q5) use($request){
                    $q5->where('direction_id', $request->area);
                  });
                });
              });
            });
            $q1->when(!is_null($request->centro_trabajo), function($q2) use($request){
                $q2->where('sucursal', $request->centro_trabajo);
            });
            $q1->when(!is_null($request->turno), function($q2) use($request){
                $q2->where('turno', $request->turno);
            });
            $q1->when(!is_null($request->puesto), function($q2) use($request){
              $q2->whereHas('jobPosition', function($q3) use($request){
                $q3->where('name', $request->puesto);
              });
            });
            $q1->when(!is_null($request->region), function($q2) use($request){
              $q2->where('region_id', $request->region);
            });
            $q1->when(!is_null($request->antiguedad), function($q2) use($dates_antiguedad){
                $q2->whereBetween('ingreso', [$dates_antiguedad['start_date'], $dates_antiguedad['end_date']]);
            });
             
          });     
        }]) 
        ->first();

        if ($NpsPeriod) {
          
          $resultados = $NpsPeriod->answers;
          $users_ids = $NpsPeriod->answers->pluck('user_id')->toArray();
          $questions = $NpsPeriod->questions;
      
        }

         
        $ids_participacion = [];        
        $real_now = Carbon::now();
        $total_participacion = 0;
        $temp_starts = [];        
        $starts = [];        

        $Users = User::whereIn('id',$users_ids)->get();


        // if(count($resultados)>0){
          foreach ($Users as $key => $user){      
   
            if(!empty($user->employee_wt->jobPosition) && !in_array($user->employee_wt->jobPosition->name, $puestos_trabajo)){               
              $puestos_trabajo_temp[] = $user->employee_wt->jobPosition->name;
              $puestos_trabajo[] = ['id'=>$user->employee_wt->jobPosition->id,'name'=>$user->employee_wt->jobPosition->name]; 
                // $puestos_trabajo[] = $job_level->id; 
        
            }
            if(!empty($user->employee_wt->jobPosition) && !empty($user->employee_wt->jobPosition->area) && !in_array($user->employee_wt->jobPosition->area->department->id, $dptos_temp)){ 
              $dptos_temp[] = $user->employee_wt->jobPosition->area->department->id; 
              $dptos[] = ['id'=>$user->employee_wt->jobPosition->area->department->id,'name'=>$user->employee_wt->jobPosition->area->department->name]; 
            }

            if(!empty($user->employee_wt->turno) && !in_array($user->employee_wt->turno, $turnos_temp)){ 
              $turnos_temp[] = $user->employee_wt->turno; 
              $turnos[] = ['id'=>$user->employee_wt->turno,'name'=>$user->employee_wt->turno]; 
            }

            if (!empty($user->employee_wt->jobPosition) && !empty($user->employee_wt->jobPosition->area->department->direction)){
              $direction = $user->employee_wt->jobPosition->area->department->direction;
              if(!in_array($direction->id, $areas_temp)){ 
                $areas_temp[] = $direction->id;
              $areas[] = ['id'=>$direction->id,'name'=>$direction->name]; 
            }
          } 
          

            if(!empty($user->employee->sucursal) && !in_array($user->employee->sucursal, $temp_centros_trabajo)){

              $temp_centros_trabajo[] = $user->employee->sucursal;
              $centros_trabajo[] = ['id'=>$user->employee->sucursal,'name'=>$user->employee->sucursal];
            }
    
            if(!empty($user->employee->region_id) && !in_array($user->employee->region_id, $temp_regiones)){

              $temp_regiones[] = $user->employee->region_id;
              $regiones[] = ['id'=>$user->employee->region->id,'name'=>$user->employee->region->name];
            }

            $entry = Carbon::parse($user->employee_wt->ingreso);     
            $antiquity = $real_now->diffInDays($entry)/365;              
            if($antiquity < 1) {
                $years_key = 'less_than_a_year';      
                $years_name = 'A. Un año o menos'; 
            }
            elseif($antiquity >= 1 && $antiquity <= 5) {    
              $years_key = 'one_to_five_years';      
              $years_name = 'B. Un año un día a cinco años';      
            } elseif($antiquity > 5 && $antiquity <= 10) {
              $years_key = 'five_to_ten_years';      
              $years_name = 'C. Cinco años un día a diez años';    
            } elseif($antiquity > 10) {
              $years_key = 'ten_to_fifteen_years';      
              $years_name = 'D. Diez años un día y más';     
            }
          
            if(!in_array($years_key, $temp_starts)){            
              $starts[] = ['id'=>$years_key,'name'=>$years_name];
              $temp_starts[] = $years_key;
            }
            
          }
        $mas_votados = [];
        foreach ($questions as $key => $value) {
          
              $question_id = $value->id;
                   
              $mas_votados = Answer::
              where('question_id', $question_id)
              ->where('period_id', $id_periodo)
              ->whereIn('user_id', $users_ids)
              ->selectRaw('answer as id , COUNT(*) as total')
              ->groupBy('answer')
              ->orderBy('total','DESC')
              ->limit(10)
              ->get();

            $personas = [];

            foreach ($mas_votados as $key => $votados) {
              $user = User::withTrashed()->whereId($votados->id)->first();
              $employee = $user->employee_wt;
              $personas[] = [
                'id' => $employee->id,
                'name' => $employee->FullName,
                'dpto' => $employee->jobPosition->area->department->name,
                'puesto' => $employee->jobPosition->name,
                'total' => $votados->total
              ];
            }


            $preguntas[] = [
              'id' => $value->id,
              'name' => $value->short_name, 
              'mas_votados' => $personas,
              'mas_votsados' => $mas_votados,
              'users_ids' => $users_ids
              
            ];

 
        }
 
 

        // Create new PHPExcel object
        $objPHPExcel = new \PHPExcel();

        // Create a first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Motivo de Elección');  
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Nombre de Compañeros');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Departamento');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Puesto');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Totales');
        $i = 2;

        foreach ($preguntas as $key => $value){
            
          $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $value['name']);
        
          $i++;
        
          foreach ($value['mas_votados'] as $mas_votados){
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $mas_votados['name']);
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $mas_votados['dpto']);
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $mas_votados['puesto']);
            $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $mas_votados['total']);
            $i++;
          }

        $i+= 2;

        }

        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Compañeros Elegidos');

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="SOCIOGRAMA REPORTE GENERAL.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
  }

  
  public function exportar_sociograma_individual(Request $request)
  { 
 
        $id_empleado = $request->id_empleado; 
        $id_periodo = $request->periodos; 
         
        $employee = Employee::whereIdempleado($id_empleado)->first();
 
        if ( !$employee ) {
          return response()->json([
            'result'=>false,
            'id_empleado'=>$id_empleado,  
            'empleado'=>[
              'id' => null,
              'nombre' => null,
              'puesto' => null,
              'departmento' => null,
              'hospital' => null,
            ],  
            'id_periodo'=>null,  
            'resultados'=>[],  
            'questions'=>[]
          ]);
        }else{
          $empleado = [
            'id' => $employee->idempleado,
            'nombre' => $employee->user->FullName,
            'puesto' => $employee->getPuestoName(true),
            'departmento' => $employee->getDepartmentName(),
            'hospital' => $employee->getRegionName()
          ];
        }

        $user_id = $employee->user->id;

        $NpsPeriod = SociogramaPeriod::
        where('id', $id_periodo)
        ->with(['answers' => function($query_0) use($user_id){
          $query_0->where('user_id',$user_id);      
          
        }]) 
        ->first();

       
        $questions= [];
 
        if ($NpsPeriod) { 
          $questions = $NpsPeriod->questions;
      
        }

        $resultados = [];


        foreach ($questions as $key => $value) {
          
              $question_id = $value->id;
                   
              $mi_votacion_por_factor = Answer::
              where('question_id', $question_id)
              ->where('period_id', $id_periodo)
              ->where('user_id', $user_id)
              ->with('respuesta_empleado')
              ->get();


              foreach ($mi_votacion_por_factor as $keyvalue_factor => $value_factor) {

                $value_factor->votaron_por_el = $this->saber_cuanto_votaron_por_un_empleado($id_periodo, $question_id, $value_factor->answer,'answer');

              }  

              $sus_votacion_por_factor = Answer::
              where('question_id', $question_id)
              ->where('period_id', $id_periodo)
              ->where('answer', $user_id)
              ->with('user.employee_wt')
              ->get();

              foreach ($sus_votacion_por_factor as $keyvalue_factor => $value_factor) {
                $value_factor->votaron_por_el = $this->saber_cuanto_votaron_por_un_empleado($id_periodo, $question_id, $value_factor->user_id,'answer');
              }  



            $colores = ['bg-success','bg-warning','bg-info'];

             foreach ($mi_votacion_por_factor as $key => $mi_votacion) {

                foreach ($sus_votacion_por_factor as $key2 => $sus_votacion) {

                  // $sus_votacion->color = '';

                  if($mi_votacion->respuesta_empleado->id==$sus_votacion->user_id){
                    $sus_votacion->color = $colores[$key];
                    $mi_votacion->color = $colores[$key];
                  }
 
                }  

             }


              $resultados[$value->short_name] = [
                'name' => $value->short_name, 
                'mis_respuestas' => $mi_votacion_por_factor, 
                'sus_respuestas' => $sus_votacion_por_factor
              ];
 
        }


        require_once 'PHPExcel.php';
        require_once 'PHPExcel/IOFactory.php';
        // Create new PHPExcel object
        $objPHPExcel = new \PHPExcel();

        // Create a first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Id del Empleado');  
        $objPHPExcel->getActiveSheet()->setCellValue('B1', $employee->idempleado);  

        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Nombre');  
        $objPHPExcel->getActiveSheet()->setCellValue('B2', $employee->FullName); 

        $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Puesto');  
        $objPHPExcel->getActiveSheet()->setCellValue('B3', $employee->getPuestoName(true));  

        $objPHPExcel->getActiveSheet()->setCellValue('A4', 'Departamento');  
        $objPHPExcel->getActiveSheet()->setCellValue('B4', $employee->getDepartmentName());  

        $objPHPExcel->getActiveSheet()->setCellValue('A5', 'Hospital');  
        $objPHPExcel->getActiveSheet()->setCellValue('B5', $employee->getRegionName());  


        $objPHPExcel->getActiveSheet()->setCellValue('A7', 'Factor Evaluado');  
        $objPHPExcel->getActiveSheet()->setCellValue('B7', 'Número de personas que les siguen');
        $objPHPExcel->getActiveSheet()->setCellValue('C7', 'Personas que me eligieron');
        $objPHPExcel->getActiveSheet()->setCellValue('D7', 'Nombre');

        $objPHPExcel->getActiveSheet()->setCellValue('F7', 'Personas a quienes yo elegí');
        $objPHPExcel->getActiveSheet()->setCellValue('G7', 'Nombre	');
        $objPHPExcel->getActiveSheet()->setCellValue('H7', 'Número de personas que les siguen');
        $i = 8;

        foreach ($resultados as $key => $value){
            
          $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $value['name']);
        
          $i++;
          $j = $i;
          $x = $i;
          foreach ($value['sus_respuestas'] as $sus_respuestas){
           
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $j, $sus_respuestas['votaron_por_el']);
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $j, $sus_respuestas['user']['employee_wt']['idempleado']);
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $j, $sus_respuestas['user']['first_name'].' '.$sus_respuestas['user']['last_name']);
            $j++;
          }
        
          foreach ($value['mis_respuestas'] as $mis_respuestas){
            $objPHPExcel->getActiveSheet()->setCellValue('F' . $x, $mis_respuestas['respuesta_empleado']['employee_wt']['idempleado']);
            $objPHPExcel->getActiveSheet()->setCellValue('G' . $x, $mis_respuestas['respuesta_empleado']['first_name'].' '.$mis_respuestas['respuesta_empleado']['last_name']);
            $objPHPExcel->getActiveSheet()->setCellValue('H' . $x, $mis_respuestas['votaron_por_el']);
            $x++;
          }

          if($j>$x){
            $i+= $j - $i +1 ;
          }else{
            $i+= $x - $i +1 ;
          }
 
        }

        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Resultado');

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="SOCIOMETRÍA REPORTE INDIVIDUAL.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');


  }

}