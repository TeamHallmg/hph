<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Enterprise;
use App\Models\EnterpriseLegal;
use App\Models\GeoDepartamento;
use DB;

class EnterpriseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  

        $enterprises = Enterprise::with('legal')->get();
        $estados = GeoDepartamento::pluck('nombre', 'id');

        return view('enterprise.index', compact(['enterprises','estados']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        // $data = $request->all();
        
        $department = new EnterpriseLegal();
        $department->id_empresa = $request->id_empresa;
        $department->dpi = $request->dpi;
        $department->nombre = $request->nombre;
        $department->nacionalidad = $request->nacionalidad;
        $department->fecha_nac = $request->fecha_nac;
        $department->sexo = $request->sexo;
        $department->edo_civil = $request->edo_civil;
        $department->profesion = $request->profesion;
        $department->puesto = $request->puesto;
        $department->notario = $request->notario;
        $department->nombramiento = $request->nombramiento;
        $department->numero_registro = $request->numero_registro;
        $department->folio = $request->folio;
        $department->libro = $request->libro;
        $department->save();

        $request->session()->flash('alert-success', 'Los datos se han agregado correctamente!!!');
        return redirect()->to('enterprise');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $data = $request->all();
        $data = $request->except('_method', '_token', 'name_empresa');
        EnterpriseLegal::where('id',$id)->update($data);


        $request->session()->flash('alert-success', 'Los datos fueron actualizados correctamente!!!');
        return redirect()->to('enterprise');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
