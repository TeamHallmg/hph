<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\JobPosition;
use App\Models\Direction;
use App\Models\Department;
use App\Models\Area;
use App\Models\JobPositionLevel;
use App\HCpuesto;
use App\Employee;
use DB;
use Illuminate\Support\Facades\Schema;
use App\LogActivity;
use App\Models\DeletedReason;
use Illuminate\Support\Facades\Auth;
use Validator;
use Carbon\Carbon;

class PuestoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function departments($id){
        return Department::select('id','name','description')->where('direction_id',$id)->orderBy('id', 'ASC')->get();
    }

    public function areas($id){
        return Area::select('id','name','description')->where('department_id',$id)->orderBy('id', 'ASC')->get();
    }

    public function trashedJobs($id){
        return JobPosition::select('id','name','description')->where('area_id',$id)->orderBy('id', 'ASC')->onlyTrashed()->get();
    }

    public function jobpositions($id){
        return JobPosition::select('id','name','description')->where('area_id',$id)->orderBy('id', 'ASC')->get();
    }

    public function jobBoss(){
        return JobPosition::select('id','name','description')->orderBy('id', 'ASC')->get();
    }

    public function levels(){
        return JobPositionLevel::select('id','name')->orderBy('id', 'ASC')->get();
    }
    
    public function index()
    {
        $jobs = JobPosition::orderBy('id', 'DESC')->get();

        foreach($jobs as $key => $job){
            $positionBoss = JobPosition::where('id',$job->job_position_boss_id)->first();
            if($positionBoss != null) {
                $job->job_position_boss_id = $positionBoss->name;
            } else {
                $job->job_position_boss_id = "N/A";
            }
        } 

        return view('puestosAdmin.index', compact(['jobs']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        //this->directions();

        //$jefes = JobPosition::orderBy('id', 'DESC')->get();
        $directions = Direction::select('id','name','description')->orderBy('id', 'DESC')->get();
        $bosses = JobPosition::orderBy('id', 'ASC')->get();
        $levels = JobPositionLevel::orderBy('id', 'ASC')->get();

        return view('puestosAdmin.create', compact(['directions','bosses','levels']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'job' => 'required',
            //'boss' => 'required',
            //'area' => 'required',
            'comment' => 'max:250',
            //'archivo' => 'required',
        );

        $error = Validator::make($request->all(), $rules);
        if($error->fails()){
            return redirect()->back()->withErrors(['errors' => $error->errors()->all()]);
        }
        
        if($file = $request->file('file')){
            $name = Carbon::now()->timestamp. '-' .$file->getClientOriginalName();
            $destinationPath = public_path("puestos_files");
            //$destinationPath = getcwd() . "puestos_files";
            $file->move($destinationPath, $name);
        }else{
            $name = "";
        }
        //dd($request);
        
        $form_data = array(
            'job_position_boss_id'  =>  $request->boss,
            'area_id'  =>  $request->area,
            'description'  =>  $request->description,
            'experience'  =>  $request->experience,
            'knowledge'  =>  $request->knowledge,
            'comment'  =>  $request->comments,
            'file'  =>  $name,
            'job_position_level_id'  =>  $request->level,
        );

        if($request->job != -1) { // Se reactiva puesto
            $form_data['name'] = $request->job_name;
            JobPosition::withTrashed()->find($request->job)->restore();
            JobPosition::find($request->job)->update($form_data);
        } else { // Crea puesto
            $form_data['name'] = $request->job_add;
            JobPosition::create($form_data);
        }
        
        // $last = $puestos = JobPosition::orderBy('id', 'DESC')->first(); What
        //dd($last->id);
        return redirect()->route('puesto.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $puesto = JobPosition::with('hcpuesto')->findOrFail($id);
        $hcPuesto = $puesto->hcpuesto;

        $hcCursos = HCpuesto::with('cursos')->where('id_puesto',$id)->get();

        $puestoJefe = JobPosition::where('id',$job->job_position_boss_id)->first();

        return view('puestosAdmin.show', compact(['puesto','hcPuesto','puestoJefe','hcCursos']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        //$puesto = JobPosition::where('id',$id)->with('hcpuesto')->first();
        $job = JobPosition::where('id',$id)->first();
        //dd($job);
        $directions = Direction::select('id','name','description')->orderBy('id', 'DESC')->get();
        $bosses = JobPosition::orderBy('id', 'ASC')->get();
        $levels = JobPositionLevel::orderBy('id', 'ASC')->get();
        return view('puestosAdmin.edit', compact(['job','directions','bosses','levels']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = array(
            'job' => 'required',
            //'boss' => 'required',
            //'area' => 'required',
            'comment' => 'max:250',
        );

        $error = Validator::make($request->all(), $rules);
        if($error->fails()){
            return redirect()->back()->withErrors(['errors' => $error->errors()->all()]);
        }

            if($request->hasFile('file') == true){
                    $file = $request->file('file');
                    $filename = Carbon::now()->timestamp. '-' .$file->getClientOriginalName();
                    $destinationPath = public_path("puestos_files");
                    $file->move($destinationPath, $filename);
            } else {
                $puesto = JobPosition::where('id',$id)->first();
                $filename = $puesto->file;
            }

            $form_data = array(
                'name'  =>  $request->job,
                'job_position_boss_id'  =>  $request->boss,
                'area_id'  =>  $request->area,
                'description'  =>  $request->description,
                'experience'  =>  $request->experience,
                'knowledge'  =>  $request->knowledge,
                'comments'  =>  $request->comments,
                'file'  =>  $filename,
                'job_position_level_id'  =>  $request->level,
            );

            JobPosition::where('id',$id)->update($form_data);
            return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function hasEmployees($id){
        return JobPosition::where('id',$id)->orderBy('id', 'ASC')->has('employees')->get();
    }

    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $job = JobPosition::find($id);

            foreach($job->employees as $key => $employee) {
                $employee->job_position_id = null;
                $employee->save();
            }
    
            $data = new DeletedReason;
            $data->user_id = Auth::user()->id;
            $data->reason = $request->del_reason;
            $job->deleted_reason()->save($data);
    
            $job->delete();
            DB::commit();
        } catch(\Throwable $th) {
            DB::rollback();
            dd($th->getMessage());
        }

        return redirect()->route('puesto.index');
    }
}
