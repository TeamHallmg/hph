<?php

namespace App\Http\Controllers\Nps;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Employee;
use App\Models\JobPosition;
use App\Models\Area;
use App\Models\Department;
use App\Models\Direction;
use App\Models\Nps\Factor;
use App\Models\Nps\NpsPeriod;
use App\Models\Nps\Answer;
use App\Models\Nps\AreaPivot;
use App\Models\Nps\DepartmentPivot;
use App\Models\Nps\JobPositionPivot;
use App\Models\Nps\GroupArea;
use App\Models\Nps\GroupDepartment;
use App\Models\Nps\GroupJobPosition;
use App\Models\Announcement\Announcement;
use App\Models\Announcement\AnnouncementType;
use App\Models\Announcement\AnnouncementCategory;
use App\Models\Announcement\View;
use DB;
use Carbon\Carbon;

class ReportesFidelizacionController extends Controller
{
    /**
     * Create a new Evaluacion Desempeno controller instance.
     *
     * @return void2
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('permission:see_progress')->only('avances');
    }
    
    
  public function fidelizacion(Request $request){

        // require_once 'PHPExcel.php';
        // require_once 'PHPExcel/IOFactory.php';
  
        $id_periodo = 0;
        $periodos = NpsPeriod::whereIn('status', ['Cerrado','Abierto'])->get();
        
        if (!is_null($periodos) > 0){
  
          $id_periodo = $periodos[0]->id;
  
          if (!empty($request->id_periodo)){
  
            $id_periodo = $request->id_periodo;

          }
          
        }
          

        $resultados = array();
        $title = ''; 
        $dptos_temp = [];
        $dptos = [];

        $nro_dptos_temp = [];
        $nro_dptos = [];
        $areas_temp = [];
        $starts = [];
        $areas = [];
        $puestos = [];
        $puestos_trabajo_temp = [];
        $puestos_trabajo = [];
        $temp_centros_trabajo = [];
        $centros_trabajo = [];
        $regiones = [];
        $temp_regiones = [];
        $turnos = [];
        $turnos_temp = [];
        $dates_antiguedad= [];

        $NpsPeriod = NpsPeriod::where('id', $id_periodo)->first();
       
        if (!is_null($request->antiguedad)){

          $dates_antiguedad = $this->antiquityDates($request->antiguedad);
       
        }

        $evaluados = $NpsPeriod
        ->users()
        ->with(['employee' => function($q) {
          $q->orderBy('nombre');
        }])
        ->get();
        
        $terminados = $NpsPeriod->users()
        ->wherePivot('status', 3)
        ->pluck('users.id')->toArray();

        // $evaluados = $NpsPeriod
        // ->users()
        // ->with(['employee' => function($q) {
        //     $q->orderBy('nombre');
        // }])      
        // ;


        // select COUNT(*) from `users` inner join `nps_period_user` on `users`.`id` = `nps_period_user`.`user_id` where `nps_period_user`.`period_id` = 2 and `nps_period_user`.`status` = 3 and `users`.`deleted_at` is null



        // dd($evaluados);
        $NpsPeriods = NpsPeriod::
        where('id', $id_periodo)
        ->with(['answers' => function($query_0) use($request,$dates_antiguedad,$terminados){
          $query_0->whereIn('user_id', $terminados);
          $query_0->whereHas('user.employee_wt', function($q1) use($request,$dates_antiguedad){            
            $q1->when(!is_null($request->dpto) && !is_null($request->area), function($q2) use($request){
              $q2->whereHas('jobPosition', function($q3) use($request){
                $q3->whereHas('area', function($q4) use($request){
                  $q4->where('department_id', $request->dpto);
                });
              }); 
            });
            $q1->when(!is_null($request->dpto) && is_null($request->area), function($q2) use($request){
              $q2->whereHas('jobPosition', function($q3) use($request){
                $q3->whereHas('area', function($q4) use($request){
                  $q4->whereHas('department_wt', function($q5) use($request){
                    $q5->where('name', $request->dpto);
                  }); 
                });
              }); 
            });
            $q1->when(!is_null($request->area), function($q2) use($request){
              $q2->whereHas('jobPosition', function($q3) use($request){
                $q3->whereHas('area', function($q4) use($request){
                  $q4->whereHas('department', function($q5) use($request){
                    $q5->where('direction_id', $request->area);
                  });
                });
              });
            });

            $q1->when(!is_null($request->centro_trabajo), function($q2) use($request){
                $q2->where('sucursal', $request->centro_trabajo);
            });
            $q1->when(!is_null($request->turno), function($q2) use($request){
                $q2->where('turno', $request->turno);
            });
            $q1->when(!is_null($request->puesto), function($q2) use($request){
                $q2->where('job_position_id', $request->puesto);
            });
            $q1->when(!is_null($request->region), function($q2) use($request){
              $q2->where('region_id', $request->region);
            });
            $q1->when(!is_null($request->antiguedad), function($q2) use($dates_antiguedad){
                $q2->whereBetween('ingreso', [$dates_antiguedad['start_date'], $dates_antiguedad['end_date']]);
            });
             
          });
        }]) 
        ->first();

        $resultados = $NpsPeriods->answers;
 

        $indicadores['DETRACTORES'] = [
              'id'=>0,
              'name'=>'DETRACTORES',
              'total_grupo'=>'42',
              'porc_total_grupo'=>'11.6',
              'cols'=>'8',
              'color'=> '#ed3237',
              'bg'=> 'bg-detractores-light',
              'bg_badge'=> 'bg-detractores',
              'img'=> 'detractores.png',              
              'niveles' => [
                  '0' => array(
                    'id'=>0,
                    'nivel'=>0,
                    'valor'=>0),
                  '1' => array(
                    'id'=>1,
                    'nivel'=>1,
                    'valor'=>0),
                  '2' => array(
                    'id'=>2,
                    'nivel'=>2,
                    'valor'=>0),
                  '3' => array(
                    'id'=>3,
                    'nivel'=>3,
                    'valor'=>0),
                  '4' => array(
                    'id'=>4,
                    'nivel'=>4,
                    'valor'=>0),
                  '5' => array(
                    'id'=>5,
                    'nivel'=>5,
                    'valor'=>0),
                  '6' => array(
                    'id'=>6,
                    'nivel'=>6,
                    'valor'=>0),
              ]
        ];
        $indicadores['PASIVOS'] = [
              'id'=>0,
              'name'=>'PASIVOS',
              'total_grupo'=>'42',
              'porc_total_grupo'=>'11.6',
              'cols'=>'2',
              'color'=> '#ffbd10',
              'bg'=> 'bg-pasivos-light',
              'bg_badge'=> 'bg-pasivos',
              'img'=> 'pasivos.png',              
              'niveles' => [
                  '7' => array(
                    'id'=>7,
                    'nivel'=>7,
                    'valor'=>0),
                  '8' => array(
                    'id'=>8,
                    'nivel'=>8,
                    'valor'=>0)
              ]
        ];
        $indicadores['PROMOTORES'] = [
              'id'=>0,
              'name'=>'PROMOTORES',
              'total_grupo'=>'42',
              'porc_total_grupo'=>'11.6',
              'cols'=>'2',
              'bg'=> 'bg-promotores-light',
              'color'=> '#39bd84',
              'bg_badge'=> 'bg-promotores',
              'img'=> 'promotores.png',              
              'niveles' => [
                  '9' => array(
                    'id'=>9,
                    'nivel'=>9,
                    'valor'=>0),
                  '10' => array(
                    'id'=>10,
                    'nivel'=>10,
                    'valor'=>0)
              ]
        ];


        
        $real_now = Carbon::now();
        $total_participacion = 0;
        $temp_starts = [];        
        if(count($resultados)>0){
          foreach ($resultados as $key => $value){      

            if($value->answer >= 0 && $value->answer <= 6){
              $indicadores['DETRACTORES']['niveles'][$value->answer]['valor'] += 1;
              $total_participacion++;
            }else if($value->answer > 6 && $value->answer <= 8){            
              $indicadores['PASIVOS']['niveles'][$value->answer]['valor'] += 1;
              $total_participacion++;
            }else if($value->answer > 8 && $value->answer <= 10){
              $indicadores['PROMOTORES']['niveles'][$value->answer]['valor'] += 1;            
              $total_participacion++;
            }


             $user = $value->user;   
             if(!empty($user->employee_wt->jobPosition) && !empty($user->employee_wt->jobPosition->area) && !in_array($user->employee_wt->jobPosition->area->department->id, $dptos_temp)){ 
               $dptos_temp[] = $user->employee_wt->jobPosition->area->department->id; 
               $dptos[] = [
                'id'=>$user->employee_wt->jobPosition->area->department->id,
                'name'=>$user->employee_wt->jobPosition->area->department->name
              ]; 
             }  

             if(!in_array($user->employee_wt->jobPosition->area->department->id, $nro_dptos_temp)){ 
               $nro_dptos_temp[] = $user->employee_wt->jobPosition->area->department->id; 
               $nro_dptos[$user->employee_wt->jobPosition->area->department->id] = [
                'id'=>$user->employee_wt->jobPosition->area->department->id,
                'name'=>$user->employee_wt->jobPosition->area->department->name,
                'total' => 1
              ]; 
             }else{
              $nro_dptos[$user->employee_wt->jobPosition->area->department->id] = [
                'id'=>$user->employee_wt->jobPosition->area->department->id,
                'name'=>$user->employee_wt->jobPosition->area->department->name,
                'total' => $nro_dptos[$user->employee_wt->jobPosition->area->department->id]['total']+1
              ]; 
             }

             if(!empty($user->employee_wt->turno) && !in_array($user->employee_wt->turno, $turnos_temp)){ 
               $turnos_temp[] = $user->employee_wt->turno; 
               $turnos[] = ['id'=>$user->employee_wt->turno,'name'=>$user->employee_wt->turno]; 
             }

             if (!empty($user->employee_wt->jobPosition) && !empty($user->employee_wt->jobPosition->area->department->direction)){
               $direction = $user->employee_wt->jobPosition->area->department->direction;
               if(!in_array($direction->id, $areas_temp)){ 
                 $areas_temp[] = $direction->id;
               $areas[] = ['id'=>$direction->id,'name'=>$direction->name]; 
             }
           } 
  
           if(!empty($user->employee_wt->jobPosition) && !in_array($user->employee_wt->jobPosition->id, $puestos_trabajo)){               
             $puestos_trabajo_temp[] = $user->employee_wt->jobPosition->id;
             $puestos_trabajo[] = ['id'=>$user->employee_wt->jobPosition->id,'name'=>$user->employee_wt->jobPosition->name]; 
        
           }

           if(!empty($user->employee->sucursal) && !in_array($user->employee->sucursal, $temp_centros_trabajo)){

               $temp_centros_trabajo[] = $user->employee->sucursal;
                $centros_trabajo[] = ['id'=>$user->employee->sucursal,'name'=>$user->employee->sucursal];
             }
    
             if(!empty($user->employee->region_id) && !in_array($user->employee->region_id, $temp_regiones)){

               $temp_regiones[] = $user->employee->region_id;
               $regiones[] = ['id'=>$user->employee->region->id,'name'=>$user->employee->region->name];
             }

             $entry = Carbon::parse($user->employee_wt->ingreso);     
             $antiquity = $real_now->diffInDays($entry)/365;              
             if($antiquity < 1) {
                 $years_key = 'less_than_a_year';      
                 $years_name = 'A. Un año o menos'; 
             }
             elseif($antiquity >= 1 && $antiquity <= 5) {    
               $years_key = 'one_to_five_years';      
               $years_name = 'B. Un año un día a cinco años';      
             } elseif($antiquity > 5 && $antiquity <= 10) {
               $years_key = 'five_to_ten_years';      
               $years_name = 'C. Cinco años un día a diez años';    
             } elseif($antiquity > 10) {
             $years_key = 'ten_to_fifteen_years';      
               $years_name = 'D. Diez años un día y más';     
             }
          
             if(!in_array($years_key, $temp_starts)){            
               $starts[] = ['id'=>$years_key,'name'=>$years_name];
               $temp_starts[] = $years_key;
             }
              
                  
          }
        }
         $dptos = $this->sortArrayByKey($dptos, 'name');
         $areas = $this->sortArrayByKey($areas, 'name');
         $centros_trabajo = $this->sortArrayByKey($centros_trabajo, 'name');
         $regiones = $this->sortArrayByKey($regiones, 'name');   
         $starts = $this->sortArrayByKey($starts, 'name');
         $turnos = $this->sortArrayByKey($turnos, 'name');
 
        $puestos = $this->sortArrayByKey($puestos_trabajo, 'name');

        foreach ($indicadores as $key => $indicador){            
       
          $total_indicador = 0;

          foreach ($indicador['niveles'] as $nivel){   
            $total_indicador += $nivel['valor'];       
          }         
        
          $indicadores[$key]['total_grupo'] = $total_indicador;
          $indicadores[$key]['porc_total_grupo'] = ($total_participacion>0)?round($total_indicador / $total_participacion * 100 , 2):0;
          
        }

        
        return response()->json([
          'terminados'=>count($terminados),
          'resultados'=>$resultados,
          'indicadores'=>$indicadores,
          'total'=>$total_participacion ,
          'id_periodo'=>$id_periodo,
          'periodos'=>$periodos,
          'dptos'=>$dptos,
          'nro_dptos'=>$nro_dptos,
          'areas'=>$areas,
          'puestos'=>$puestos,
          'turnos'=>$turnos,
          'centros_trabajo'=>$centros_trabajo,
          'regiones'=>$regiones,    
          'antiguedades'=>$starts,    
          'dates_antiguedad'=>$dates_antiguedad,    
          'dates_antigueddad'=>$request->antiguedad,     
        ]);

  }

  public function exportar_respuesta(Request $request){
        require_once 'PHPExcel.php';
        require_once 'PHPExcel/IOFactory.php';

        $resultados = array();
        $title = '';

        $dates_antiguedad= [];
 
        if (!is_null($request->antiguedad)){

          $dates_antiguedad = $this->antiquityDates($request->antiguedad);
       
        }
        $id_periodo = $request->id_periodo;
        $NpsPeriod = NpsPeriod::where('id', $id_periodo)->first();
     
        $terminados = $NpsPeriod->users()
        ->wherePivot('status', 3)
        ->pluck('users.id')->toArray();

        $NpsPeriods = NpsPeriod::
        where('id', $request->id_periodo)
        ->with(['answers' => function($query_0) use($request,$dates_antiguedad,$terminados){
          $query_0->whereIn('user_id', $terminados);
          $query_0->whereHas('user.employee_wt', function($q1) use($request,$dates_antiguedad){
            $q1->when(!is_null($request->dpto), function($q2) use($request){
              $q2->whereHas('jobPosition', function($q3) use($request){
                $q3->whereHas('area', function($q4) use($request){
                  $q4->where('department_id', $request->dpto);
                });
              }); 
            });
            $q1->when(!is_null($request->area), function($q2) use($request){
              $q2->whereHas('jobPosition', function($q3) use($request){
                $q3->whereHas('area', function($q4) use($request){
                  $q4->whereHas('department', function($q5) use($request){
                    $q5->where('direction_id', $request->area);
                  });
                });
              });
            });
            $q1->when(!is_null($request->centro_trabajo), function($q2) use($request){
                $q2->where('sucursal', $request->centro_trabajo);
            });
            $q1->when(!is_null($request->puesto), function($q2) use($request){
                $q2->where('job_position_id', $request->puesto);
            });
            $q1->when(!is_null($request->region), function($q2) use($request){
              $q2->where('region_id', $request->region);
            });
            $q1->when(!is_null($request->antiguedad), function($q2) use($dates_antiguedad){
                $q2->whereBetween('ingreso', [$dates_antiguedad['start_date'], $dates_antiguedad['end_date']]);
            });
             
          });
        }]) 
        ->first();

        $resultados = $NpsPeriods->answers;

        // Create new PHPExcel object
        $objPHPExcel = new \PHPExcel();

        // Create a first sheet
        $objPHPExcel->setActiveSheetIndex(0); 

        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Id'); 
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Rfc'); 
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Evaluado'); 
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Región');  
        $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Correo');  
        $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Departamento');  
        $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Área');  
        $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Puesto');  
        $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Respuestas');  
        $i = 2;

        foreach ($resultados as $key => $value){
        $eval = $value->user;
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, (!empty($value->user->employee_wt) ? $value->user->employee_wt->idempleado : ''));
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, (!empty($value->user->employee_wt) ? $value->user->employee_wt->rfc : ''));
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $value->user->email);
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, (isset($value->user->employee_wt->region) ? $eval->employee_wt->region->name : ''));
        $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $value->user->email);
        $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, (isset($eval->employee_wt->jobPosition->area->department) ? $eval->employee_wt->jobPosition->area->department->name : ''));
        $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, (isset($eval->employee_wt->jobPosition->area->department->direction) ? $eval->employee_wt->jobPosition->area->department->direction->name : ''));
        $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, (!empty($eval->employee_wt->jobPosition) ? $eval->employee_wt->jobPosition->name : ''));
        $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $value->answer);

        $i++;
        }

        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Respuestas Abiertas');

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Respuestas Encuesta' . $title . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
  }



  public function exportar_resultados($id_periodo, $grupo_departamento = '', $grupo_area = '', $grupo_puesto = '', $direccion = '', $departamento = '', $area = '', $puesto = ''){
        require_once 'PHPExcel.php';
        require_once 'PHPExcel/IOFactory.php';

        $resultados = array();
        $title = '';

        if (empty($grupo_area) && empty($departamento) && empty($puesto)){

          $resultados = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('users', 'clima_answers.user_id', '=', 'users.id')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->where('factor_id', 0)->where('period_id', $id_periodo)->select('answer', 'question', 'departments.name')->orderBy('departments.name')->orderBy('clima_questions.id')->get();
        }

        else{

          if (!empty($departamento) && !empty($puesto)){

            $department = Department::find($departamento);
            $job_position = JobPosition::find($puesto);
            $title = ' Departamento ' . $department->name . ' y Puesto ' . $job_position->name;
            $resultados = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('users', 'clima_answers.user_id', '=', 'users.id')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->where('factor_id', 0)->where('period_id', $id_periodo)->where('department_id', $departamento)->where('job_position_id', $puesto)->select('answer', 'question', 'departments.name')->orderBy('departments.name')->orderBy('clima_questions.id')->get();
          }

          else{

            if (!empty($departamento)){

              $department = Department::find($departamento);
              $title = ' Departamento ' . $department->name;
              $resultados = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('users', 'clima_answers.user_id', '=', 'users.id')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->where('factor_id', 0)->where('period_id', $id_periodo)->where('department_id', $departamento)->select('answer', 'question', 'departments.name')->orderBy('departments.name')->orderBy('clima_questions.id')->get();
            }

            else{

              if (!empty($puesto)){

                $job_position = JobPosition::find($puesto);
                $title = ' Puesto ' . $job_position->name;
                $resultados = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('users', 'clima_answers.user_id', '=', 'users.id')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->where('factor_id', 0)->where('period_id', $id_periodo)->where('job_position_id', $puesto)->select('answer', 'question', 'departments.name')->orderBy('departments.name')->orderBy('clima_questions.id')->get();
              }

              else{

                if (!empty($grupo_area)){

                  $grupo = GruposAreas::find($grupo_area);
                  $title = ' Grupo de Área ' . $grupo->name;
                  $resultados = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('users', 'clima_answers.user_id', '=', 'users.id')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->leftJoin('clima_areas_pivot', 'clima_areas_pivot.area_id', '=', 'areas.id')->leftJoin('clima_group_areas', 'clima_group_areas.id', '=', 'clima_areas_pivot.group_area_id')->where('factor_id', 0)->where('period_id', $id_periodo)->where('clima_group_areas.id', $grupo_area)->select('answer', 'question', 'departments.name')->orderBy('departments.name')->orderBy('clima_questions.id')->get();
                }
              }
            } 
          }
        }

        // Create new PHPExcel object
        $objPHPExcel = new \PHPExcel();

        // Create a first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Departamento');  
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Factor');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Pregunta');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Respuesta');
        $i = 2;

        foreach ($resultados as $key => $value){
            
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $value->name);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, 'Pregunta Abierta');
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $value->question);
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $value->answer);
        $i++;
        }

        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Respuestas Abiertas');

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Respuestas Encuesta' . $title . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
  }

  
  private function sortArrayByKey($array,$key){
      
      $groupedItems = []; 
      $Items = []; 
      foreach ($array as $item) {
          $pool = $item[$key];
          $groupedItems[$pool][] = $item;
      }
      ksort($groupedItems);

      foreach ($groupedItems as $key => $value) {
        $Items[] = $value[0];
      }

      return $Items;
  } 

  private function antiquityDates($ingreso){
      
    
        $fecha_actual = date('Y-m-d');

        $end_date = $start_date = 0;

        $antiquity = $ingreso;

        if($antiquity =='less_than_a_year') {
            $end_date = date('Y-m-d');  
            $start_date = date("Y-m-d",strtotime($fecha_actual."- 365 days"));             
        }
        else if($antiquity =='one_to_five_years') {    
          $end_date = date("Y-m-d",strtotime($fecha_actual."- 366 days"));  
          $start_date = date("Y-m-d",strtotime($fecha_actual."- 1825 days"));   
        } else if($antiquity =='five_to_ten_years') {
          $end_date = date("Y-m-d",strtotime($fecha_actual."- 1826 days"));  
          $start_date = date("Y-m-d",strtotime($fecha_actual."- 3650 days"));   
        } else if($antiquity =='ten_to_fifteen_years') {
          $end_date = date("Y-m-d",strtotime($fecha_actual."- 3650 days"));  
          $start_date = date("Y-m-d",strtotime($fecha_actual."- 7300 days")); 
        }

        return ['start_date'=>$start_date,'end_date'=>$end_date];
  } 
 
}