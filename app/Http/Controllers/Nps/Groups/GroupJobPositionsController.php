<?php

namespace App\Http\Controllers\ClimaOrganizacional\Groups;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\ClimaOrganizacional\GroupJobPosition;
use App\Models\ClimaOrganizacional\JobPositionPivot;
use App\Models\JobPosition;

class GroupJobPositionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = GroupJobPosition::all();
        $job_positions_in_group = JobPositionPivot::pluck('job_position_id')->toArray();
        $job_positions = JobPosition::orderBy('name')->get();
        $job_positions_without_group = array();

        foreach ($job_positions as $key => $value){

          if (!in_array($value->id, $job_positions_in_group)){

            $job_positions_without_group[] = $value;
          }
        }

        return view('clima-organizacional.groups.job_positions.index', compact('groups', 'job_positions_without_group'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $job_positions_in_group = JobPositionPivot::pluck('job_position_id')->toArray();
        $job_positions = JobPosition::orderBy('name')->get();
        $job_positions_without_group = array();
        $jobPositions = array();

        foreach ($job_positions as $key => $value){

          if (!in_array($value->id, $job_positions_in_group)){

            $jobPositions[] = $value;
          }
        }

        return view('clima-organizacional.groups.job_positions.create', compact('jobPositions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $other_job_positions = array();
        foreach ($request->jobPositions as $key => $value){
          $job_position = JobPosition::find($value);
          $job_positions = JobPosition::where('name', $job_position->name)->where('id', '!=', $value)->pluck('id')->toArray();
          if (!empty($job_positions)){
            $other_job_positions = array_merge($other_job_positions, $job_positions);
          }
        }
        if (!empty($other_job_positions)){
          $request->jobPositions = array_merge($request->jobPositions, $other_job_positions);
        }
        \DB::beginTransaction();
        try {
            $group = GroupJobPosition::create([
                'name' => $request->name,
                'description' => $request->description,
            ]);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        try {
            $group->jobPositions()->attach($request->jobPositions);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        \DB::commit();
        flash('Guardado correctamente')->success();
        return redirect()->to('clima-organizacional/grupos_puestos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group = GroupJobPosition::with('jobPositions')->findOrFail($id);
        return view('clima-organizacional.groups.job_positions.show', compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = GroupJobPosition::with('jobPositions')->findOrFail($id);
        $groupJobPositions =  $group->jobPositions()->pluck('id')->toArray();
        $job_positions_in_group = JobPositionPivot::pluck('job_position_id')->toArray();
        $jobPositions = JobPosition::orderBy('name')->get();
        $job_positions_without_group = array();

        foreach ($jobPositions as $key => $value){

          if (!in_array($value->id, $job_positions_in_group)){

            $job_positions_without_group[] = $value->id;
          }
        }
        
        return view('clima-organizacional.groups.job_positions.edit', compact('group', 'groupJobPositions', 'jobPositions', 'job_positions_without_group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $group = GroupJobPosition::with('jobPositions')->findOrFail($id);
        $other_job_positions = array();
        foreach ($request->jobPositions as $key => $value){
          $job_position = JobPosition::find($value);
          $job_positions = JobPosition::where('name', $job_position->name)->where('id', '!=', $value)->pluck('id')->toArray();
          if (!empty($job_positions)){
            $other_job_positions = array_merge($other_job_positions, $job_positions);
          }
        }
        if (!empty($other_job_positions)){
          $request->jobPositions = array_merge($request->jobPositions, $other_job_positions);
        }
        \DB::beginTransaction();
        try {
            $group->update([
                'name' => $request->name,
                'description' => $request->description,
            ]);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        try {
            $group->jobPositions()->sync($request->jobPositions);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        \DB::commit();
        flash('Actualizado correctamente')->success();
        return redirect()->to('clima-organizacional/grupos_puestos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = GroupJobPosition::findOrFail($id);
        \DB::beginTransaction();
        try {
            $group->jobPositions()->detach();
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        try {
            $group->delete();
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        \DB::commit();
        flash('Eliminado correctamente')->success();
        return redirect()->to('clima-organizacional/grupos_puestos');
    }
}
