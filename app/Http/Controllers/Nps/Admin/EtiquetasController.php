<?php
namespace App\Http\Controllers\Nps\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

use App\Models\Nps\Etiquetas;
use DB;

class EtiquetasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Muestra las etiquetas
     *
     */
    public function index(){

      if (auth()->user()->role != 'admin' && !auth()->user()->hasNpsPermissions(4)){
        flash('No cuenta con permisos');
        return redirect('/nps/que-es');
      }

      // Obtiene todas las etiquetas
      $etiquetas = Etiquetas::all();

      return view('nps/Admin/etiquetas/index', compact('etiquetas'));
    }

    /**
     * Crea una nueva etiqueta
     *
     */
    public function create(){

      if (auth()->user()->role != 'admin' && !auth()->user()->hasNpsPermissions(4)){
        flash('No cuenta con permisos');
        return redirect('/nps/que-es');
      }

      if (!empty($_POST['name'])){

        $etiqueta = 0;

        if (!empty($_FILES['file']['name'])){

          $_POST['file'] = $_FILES['file']['name'];
        }

        else{

          $_POST['file'] = '';
        }

        if (empty($_POST['color'])){

          $etiqueta = DB::insert('insert into nps_etiquetas (name, valor, file) values (?,?)', [$_POST['name'], $_POST['valor']]);
        }

        else{

          $etiqueta = DB::insert('insert into nps_etiquetas (name, valor, color, file) values (?,?,?,?)', [$_POST['name'], $_POST['valor'], $_POST['color'], $_POST['file']]);
        }

        $etiqueta = DB::table('nps_etiquetas')->orderBy('id', 'DESC')->first();

        if (!empty($_POST['file']['name'])){

          if (!file_exists(getcwd() . '/img/nps/etiquetas/' . $etiqueta->id)){

            mkdir(getcwd() . '/img/nps/etiquetas/' . $etiqueta->id, 0755, true);
          }

          move_uploaded_file($_FILES['file']['tmp_name'],  getcwd() . '/img/nps/etiquetas/' . $etiqueta->id . '/' . $_POST['file']);
        }
        
        if (!empty($etiqueta)){

          flash('La etiqueta fue creada correctamente');
        }

        else{

          flash('Error al crear la etiqueta. Intente de nuevo');
        }

        return redirect('nps/etiquetas');
      }

      return view('nps/Admin/etiquetas/create');
    }

    /**
     * Edita o actualiza una etiqueta
     *
     */
    public function edit($id = 0){

      if (auth()->user()->role != 'admin' && !auth()->user()->hasNpsPermissions(4)){
        flash('No cuenta con permisos');
        return redirect('/nps/que-es');
      }

      // No existe el id para la pagina de edicion
	    if (empty($id)){
		   
		    $fecha = date('Y-m-d H:i:s');

        if (empty($_POST['color'])){

          $_POST['color'] = 'NULL';
        }

        if (!empty($_FILES['file']['name'])){

          $_POST['file'] = $_FILES['file']['name'];
        }

        else{

          $etiqueta = DB::table('nps_etiquetas')->where('id', $_POST['id'])->first();
          $_POST['file'] = '';

          if (!empty($etiqueta->file)){

            $_POST['file'] = $etiqueta->file;
          }
        }

        DB::update('update nps_etiquetas set name = "' . $_POST['name'] . '", valor = "' . $_POST['valor'] . '", color = "' . $_POST['color'] . '", file = "' . $_POST['file'] . '",  updated_at = "' . $fecha . '" where id = ?', [$_POST['id']]);

        if (!empty($_FILES['file']['name'])){

          if (!file_exists(getcwd() . '/img/nps/etiquetas/' . $_POST['id'])){

            mkdir(getcwd() . '/img/nps/etiquetas/' . $_POST['id'], 0755, true);
          }

          move_uploaded_file($_FILES['file']['tmp_name'],  getcwd() . '/img/nps/etiquetas/' . $_POST['id'] . '/' . $_POST['file']);
        }

		    flash('La etiqueta fue guardada correctamente');
        return redirect('nps/etiquetas');
      }

      $etiqueta = DB::select('select * FROM nps_etiquetas where id = ?', [$id]);
      return view('nps/Admin/etiquetas/edit', compact('etiqueta'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

   /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = 0){
      
      if (!empty($_POST['id'])){

        $etiqueta = DB::delete('DELETE FROM nps_etiquetas WHERE id = ?', [$_POST['id']]);
        
        if (!empty($etiqueta)){

          flash('La etiqueta fue borrada correctamente');
        }

        else{

          flash('Error al borrar la etiqueta. Intente de nuevo');
        }

        return redirect('/nps/etiquetas');
      }

      $etiqueta = DB::select('SELECT id, name FROM nps_etiquetas WHERE id = ?', [$id]);
      return view('nps/Admin/etiquetas/delete', compact('etiqueta'));
    }
}
