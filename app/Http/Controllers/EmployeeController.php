<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Employee;
use App\Http\Requests;
use App\Models\Vacations\Event;
use App\Models\Vacations\Benefits;
use App\Models\Vacations\Balances;
use App\Models\Vacations\Userfields;
use App\Models\Vacations\Incident;

use Illuminate\Support\Facades\Auth;

class EmployeeController extends Controller
{
    public function balances()
    {
        $data = EmployeeController::getEmployeeBalance();
        $data['vacations'] = EmployeeController::getBalanceDetail(5); //Arbitrario
        $data['comodin'] = EmployeeController::getBalanceDetail(6); //Arbitrario
        // dd($data);
        return view('vacations.common.balances',$data);
    }

    public function calendar()
    {
        $data = EmployeeController::getEmployeeBalance();
        $data['vacations'] = EmployeeController::getBalanceDetail(5);
        $data['config'] = $this->config;
        return view('vacations.common.calendar',$data);
    }

    public static function getVacationsDetail($bid, $uid = null ){
        if($uid == null) $user_id = Auth::user()->id;
        else $user_id = $uid;
        //Se obtienen los balances ordenados, primero los null, y despues de menor a mayor
        $rows = Balances::where([['user_id',$user_id],['benefit_id',$bid]])
                    ->orderBy('until','ASC')
                    ->get();

        $data = [];
        $pending = 0;
        $amount = 0;
        $last = null;
        //Si no se encontraron registros se le crea una del año siguiente pero sin dias para pedir
        if(!$rows->first()) {
            $data['rows'][] = new Balances(['year'=>date('Y') + 1, 'user_id'=>$user_id, 'benefit_id'=>$bid, 'pending' => $pending, 'amount' => $amount, 'js' => '']);
            $data['benefit'] = Benefits::find($bid);
            $data['pending'] = $pending;
            $data['amount'] = $amount;
            $data['solicited'] = 0;
            return($data);
        }        
        foreach($rows as $row) {
            $row->diff = $row->pending - $row->amount;
            $pending += $row->pending + $row->amount;
            // dd($pending, $row);
            $amount += $row->amount;
            $events = Event::where('status','pending')->where('js','like','%"id":'.$row->id.',%')->get();
            // dd($events, $row);
            $row->solicited = 0;
            foreach($events as $event) {
                $js = json_decode($event->js);
                foreach($js->balances as $b) {
                    if($b->id == $row->id)$row->solicited += $b->amount;
                }
            }
            if($row->until == null) {
                $last = $row;
            } else {
                $data['rows'][] = $row;
            }
        }
        $solicited = Incident::where([
            ['from_id',Auth::user()->id],
            ['status','pending'],
            ['event_id','<>','0'],
            ['benefit_id', $bid]
        ])->sum('amount');
        if(!$solicited) $solicited = 0;

        if($last)$data['rows'][] = $last;
            $data['benefit'] = Benefits::find($bid);
            $data['pending'] = $pending;
            $data['amount'] = $amount;
            $data['solicited'] = intval($solicited);
            $data['diff'] = $pending - $amount;
        // dd($data, $pending);
        return ($data);
    }

    public static function getBalanceDetail($bid, $uid = null )
    {
        if($uid == null) $user_id = Auth::user()->id;
        else $user_id = $uid;
        //Se obtienen los balances ordenados, primero los null, y despues de menor a mayor
        $rows = Balances::where([['user_id',$user_id],['benefit_id',$bid]])
                    ->orderBy('until','ASC')
                    ->get();

        $data = [];
        $pending = 0;
        $amount = 0;
        $last = null;
        //Si no se encontraron registros se le crea una del año siguiente pero sin dias para pedir
        if(!$rows->first()) {
            $data['rows'][] = new Balances(['year'=>date('Y') + 1,'user_id'=>$user_id, 'benefit_id'=>$bid, 'pending' => $pending, 'amount' => $amount]);
            $data['benefit'] = Benefits::find($bid);
            $data['pending'] = $pending;
            $data['amount'] = $amount;
            $data['solicited'] = 0;
            $data['diff'] = 0;
            return($data);
        }        
        foreach($rows as $row) {
            $row->diff = $row->pending - $row->amount;
            $pending += $row->pending;
            // dd($pending, $row);
            $amount += $row->amount;
            $events = Event::where('status','pending')->where('js','like','%"id":'.$row->id.',%')->get();
            // dd($events, $row);
            $row->solicited = 0;
            foreach($events as $event) {
                $js = json_decode($event->js);
                foreach($js->balances as $b) {
                    if($b->id == $row->id)$row->solicited += $b->amount;
                }
            }
            if($row->until == null) {
                $last = $row;
            } else {
                $data['rows'][] = $row;
            }
        }
        $solicited = Incident::where([
            ['from_id',Auth::user()->id],
            ['status','pending'],
            ['event_id','<>','0'],
            ['benefit_id', $bid]
        ])->sum('amount');
        if(!$solicited) $solicited = 0;

        if($last)$data['rows'][] = $last;
            $data['benefit'] = Benefits::find($bid);
            $data['pending'] = $pending;
            $data['amount'] = $amount;
            $data['solicited'] = intval($solicited);
            $data['diff'] = $pending - $amount;
        // dd($data, $pending);
        return ($data);
    }

    public static function getEmployeeBalance( $id = null )
    {
        if($id == null) {
            $user = Auth::user();
            $user_id = $user->id;
        } else {
            $user_id = $id;
            $user = User::where('id',$id)->first();
        }
        //Get balances
        $benefits = Benefits::get();
        $balances = [];
        /*No suma bien */
        //SELECT SUM(pending) as pending,SUM(amount) as amount,year,user_id,benefit_id FROM balances WHERE benefit_id = 5 AND user_id = 820 AND year = 2016 GROUP by benefit_id
        foreach($benefits as $benefit) {
            $balance = Balances::select(\DB::raw('SUM(amount) AS amount, SUM(pending) AS pending, benefit_id, user_id'))
            ->where([['user_id',$user_id],['benefit_id',$benefit->id]])
            ->whereHas('benefit', function($query) use ($user){
                $query->where('gender','x')
                    ->orWhere('gender',strtolower($user->gender));
            })
            ->groupBy('year')
            ->groupBy('user_id')
            ->groupBy('benefit_id')
            ->first();
            if($balance) {
                $balances[] = $balance->load('benefit');
            } else {
                if($benefit->gender != 'x' && $benefit->gender != strtolower($user->gender)) continue;
                $b = [
                    'amount' => 0,
                    'pending' => 0,
                    'benefit_id' => $benefit->id,
                    'user_id' => $user_id,
                    'year' => date('Y'),
                ];
                $_b = new Balances($b);
                $_b->load('benefit');
                $balances[] = $_b;
            }
        }
        return (['balances' => $balances]);
    }

    public static function processTxt()
    {
        $eight = 8 * 60;
        $balances = Balances::where([['benefit_id',10],['pending','>',$eight]])->get(); //Arbitrario

        foreach($balances as $balance) {
            $d = 0;
            while($balance->pending > $eight) {
                $d++;
                $balance->pending -= $eight;
            }
            $b = EmployeeController::getBalanceDetail(11,$balance->user_id); //Arbitrario
            $b['rows'][0]->pending += $d;
            unset($b['rows'][0]->diff);
            $b['rows'][0]->save();
            $balance->save();
        }
    }

    public static function processComodin($date)
    {
        $year   = date('Y', $date);
        $users  = User::select('id')->get();
        $now    = date('Y-m-d H:m:s',$date);
        $until  = date('Y-m-d',mktime(0,0,0,12,31,$year)); //Arbitrario
        $b = [
            'amount' => 0,
            'pending' => 1,
            'benefit_id' => 6, //Arbitrario...
            'year' => date('Y'),
            'created_at' => $now,
            'updated_at' => $now,
            'until' => $until,
        ];
        $bash = [];
        echo "Generating Bash <br />";
        foreach($users as $user) {
            $b['user_id'] = $user->id;
            $bash[] = $b;
        }
        echo "Inserting Bash in Balances <br />";
        Balances::insert($bash);
    }

    public static function processVacationsCSV($data)
    {
        //Delete * 5 and 6
        $users = User::select(['id','number'])->get();
        $b = [];
        $u = [];
        $date = date('Y-m-d');

        foreach($users as $user) {
            if(isset($data[$user->number])) {
                $tmp = Userfields::where('user_id',$user->id)
                            ->where('fieldname','files')
                            ->where('value','like','%"week":"'.$data[$user->number]['files']['week'].'"%')
                            ->first();
                if($tmp) {
                    Balances::where('user_id',$user->id)->where('benefit_id','5')->update(
                        ['user_id'=>$user->id,'benefit_id'=>'5','amount'=>0,'pending'=>$data[$user->number]['VAC']['pending'],'year'=>$data[$user->number]['VAC']['year']]
                    );
                    Balances::where('user_id',$user->id)->where('benefit_id','6')->update(
                        ['user_id'=>$user->id,'benefit_id'=>'6','amount'=>0,'pending'=>(2 - $data[$user->number]['FD']['pending']),'year'=>$data[$user->number]['FD']['year']]
                    );


                    $tmp->value = json_encode(['from'=>$date,$data[$user->number]['files']]);
                    //if($tmp->id == '987') {dd($data[$user->number],$tmp); die;}
                    $tmp->save();
                    continue;
                }

                Balances::where('user_id',$user->id)->whereIn('benefit_id',['5','6'])->delete();
                //$amount = Incident::where([['benefit_id','5'],['user_id',$user->id],['event_id','<>','0'],['status','complete']])->sum('amount');
                if(isset($data[$user->number]['VAC']))$b[] = ['user_id'=>$user->id,'benefit_id'=>'5','amount'=>0,'pending'=>$data[$user->number]['VAC']['pending'],'year'=>$data[$user->number]['VAC']['year']];
                if(isset($data[$user->number]['FD'])) $b[] = ['user_id'=>$user->id,'benefit_id'=>'6','amount'=>0,'pending'=>(2 - $data[$user->number]['FD']['pending']),'year'=>$data[$user->number]['FD']['year']];
                if($data[$user->number]['files']['pdf']!="") $u[] = ['user_id'=>$user->id,'fieldname'=>'files','value'=>json_encode(['from'=>$date,$data[$user->number]['files']])];
            }
        }
        Balances::insert($b);
        Userfields::insert($u);
    }

    public static function processVacations($date)
    {
        $noMensajes = [];
        $year  = date('Y', $date);
        $month = date('m', $date);
        $day   = date('d', $date);
        $now   = date('Y-m-d H:m:s', $date);
        $until = date('Y-m-d',mktime(0,0,0,$month+6,$day,$year+1)); //Arbitrario
        // dd($year ,$month, $day);
        //Un join entre usuarios y balances, 
        //"until" es nulo, sean vacaciones, tienen que tener ser del mes actual y del dia
        //en resumen son los usuarios que cumplen años

        $users = User::with([
            'balances' => function($q) use($year){
                $q->whereNull('until')
                ->where('benefit_id', 5)
                ->where('year', $year);    
            }
        ])
        ->join('employees','employees.id','=','users.employee_id')
        ->whereHas('balances', function($q) use($year){
            $q->whereNull('until')
            ->where('benefit_id', 5)
            ->where('year', $year);
        })
        ->whereMonth('employees.ingreso',$month)
        ->whereDay('employees.ingreso',$day)
        ->get();

        $balance = [
            'benefit_id' => 5, //Arbitrario...
            'year' => date('Y'),
            'created_at' => $now,
            'updated_at' => $now,
            'until' => $until,
        ];
        $bash = [];
        $in   = [];

        $errors = [];
        echo "Generating Bash <br />";
        // dd($users);
        foreach($users as $user) {
            \DB::beginTransaction();
            if($user->balances->count() > 0){
                try {
                    $user->balances[0]->until = $until;
                    $user->balances[0]->save();
                } catch (\Exception $th) {
                    $noMensajes[] = $user->id . ' | No pudimos actualizar la fecha until del Balance |  ' . $th->getMessage();
                    \DB::rollback();
                    continue;
                }
            }else{
                $noMensajes[] = $user->id . ' |  Aqui se supone el usuario no tiene lineas y ps esta mal jajaj';
                //Por alguna razon el usuario ya tiene un año no tiene lineas de balances
            }
            \DB::commit();
            echo "<br>";
        }
        echo "Inserting Bash <br />";
        return $noMensajes; 
    }

    public static function removeBenefits($date)
    {
        $now   = date('Y-m-d', $date);
        $balances = Balances::whereDate('until','<',$now)->delete();
    }

    public static function erraseDuplis($id){
        //SELECT * FROM balances WHERE id IN ( SELECT id FROM balances GROUP BY year,until HAVING count(id) >1 ) and user_id = 291
        //
        $array = Balances::select('id')
            ->where('user_id',$id)
            ->groupBy('year','until')
            ->havingRaw('count(id) > 1')
            ->get();
        foreach ($array as  $value) {
            $value->forceDelete();
        }
        // dd($array);
        // $balances = Balances::whereIn($array)
        // ->where('user_id',$id)
        // ->get();
    }

    public static function fixVacations()
    {
        /*$today  = date('Y-m-d');
        $year   = date('Y');
        Balances::where([['benefit_id',5],['year',$year-1],['until','>',$today]])->update(['year'=>$year]);
        Balances::where([['benefit_id',5],['year',$year-1],['until',null]])->update(['year'=>$year]);*/
    }

    public static function setPartials($date)
    {
        $noMensajes = [];

        $users = User::select('users.id', 'employees.ingreso AS started_at')
        ->join('employees','employees.id','=','users.employee_id')
        ->whereDoesntHave('balances', function ($query){
            $query->where('benefit_id', 5);
        })->get();

        foreach ($users as $user) {

            $year = date('Y', strtotime($user->started_at)) + 1;

            try {
                Balances::create([
                    'year' => $year,
                    'amount' => 0,
                    'pending' => 0,
                    'benefit_id' => 5,
                    'user_id' => $user->id,
                    'until' => null,
                ]);
            } catch (\Exception $e) {

                //throw $th;
                
            }
        }
        $bash = [];
        $balance = [];
        //Tomar la lista de usuarios
        $users = User::select(['users.id','ingreso AS started_at'])
        ->join('employees','employees.id','=','users.employee_id')
        ->get();//Se tienen todos los usuarios
        $balances = Balances::select('id','user_id')->where([['until',null],['benefit_id',5]])->get();//se obtienen todos los balances con fecha until en null y que sean del beneficio 5 "Vacaciones"
        //Se crea un arreglo con [0] = id del registro y [1] = id del usuario
        foreach($balances as $balance) {
            $bash[$balance->user_id] = ['id' => $balance->id, 'user_id' => $balance->user_id];
        }
        // $now = date('Y-m-d H:m:s');
        $now = date('Y-m-d H:m:s', $date);
        $balance = [
            'amount' => 0,
            'benefit_id' => 5, //Arbitrario...
            'created_at' => $now,
            'updated_at' => $now,
        ];

        $when = '';
        $in   = '';
        $flag = false;
        $datata = [];

        foreach($users as $user) {
            $today = $date;
            $stamp = strtotime($user->started_at) + (3 * 30 * 24 * 60 * 60); 
            //El if descarta a usuarios muy nuevos que todavia no tienen 3 meses en la empresa
            // stamp es nuestra fecha de ingreso mas 3 meses y $today es la fecha de hoy
            if($stamp > $today) {
                unset($bash[$user->id]);
                continue;
            }
            //Se envia mi fecha de inicio en la empresa.
            //Se le modifica el año para hacer la fecha lo más cercana al dia de hoy
            // y se obtienen los dias de diferencia de la fecha a hoy siendo esta
            // siempre mayor que la fecha creada

            //En resumen de obtiene los dias transcurridos de mi año laboral
            $z = EmployeeController::calculateZ($user->started_at, $date);
            //Numerode vacaciones que me tocan

            $daysOfYear = date('z', mktime(0,0,0,12,31,date('Y'))) + 1;
            $total = $user->getMyVacations($date , $user->started_at);
            $c = floor(($total * $z) / $daysOfYear);

            //$c es el calculo proporcional de las vacaciones que les tocan
            //$total es el numero de vacaciones que nos corresponden en el año en curso
                        
            if(isset($bash[$user->id]['id'])) {
                $user_balance = Balances::find($bash[$user->id]['id']);
                if($user_balance){
                    try {
                        $user_balance->pending = $c;
                        $user_balance->save();
                    } catch (\Exception $th) {
                        $noMensajes[] = $bash[$user->id]['id'] . ' | No tengo balance |  ' . $th->getMessage();
                    }
                }
            } else {
                //Si el dia/mes del año en curso no ha pasado, se guarda el año actual, de lo contrario es el siguiente
                $balance['year'] = $user->getMyNextYear($date);
                $balance['pending'] = $c;
                $balance['user_id'] = $user->id;
                $bash[$user->id] = $balance;
                try {
                    // Se insertan los balances de usuarios que acaban de pasar los 3 meses en la empresa
                    $datata[] = Balances::create($balance);
                } catch (\Exception $th) {
                    $noMensajes[] = $user->id . ' | No pudimos crear el Balance |  ' . $th->getMessage();
                }
            }
        }
        return $noMensajes;
    }

    public static function calculateZ($date, $times = null, $debug = false)
    {
        if(is_null($times)){
            $now = time();
        }else{
            $now = $times;
        }
        $time  = strtotime($date);
        $m     = date('m',$time);
        $d     = date('d',$time);
        $year  = (is_null($times))?date('Y'):date('Y', $times);
        //A la fecha recibida se le quita el año y se le pone el actual
        $time  = mktime(0,0,0,$m,$d,$year);
        // dd($time, $now, $m, $d, $year);
        //Si la nueva fecha es mayor a hoy se le resta un año a la nueva fecha $time
        if($time >= $now) $time  = mktime(0,0,0,$m,$d,$year-1);

        $datetime1 = date_create(date('Y-m-d',$time));
        $datetime2 = date_create(date('Y-m-d',$now));
        $interval = date_diff($datetime1, $datetime2);

        if ($debug) {
            dd($datetime1, $datetime2, $interval->format('%a'), date('Y-m-d', $times));
        }
        // dd($datetime1, $datetime2, $interval, $interval->format('%a'));
        //Se regresan los dias de diferencia
        return ($interval->format('%a'));
    }
}
