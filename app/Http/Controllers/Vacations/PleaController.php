<?php

namespace App\Http\Controllers\Vacations;

use DB;
use Lang;
use App\User;
//Models
use Illuminate\Http\Request;
use App\Models\Vacations\Event;
use App\Models\Vacations\Balances;
//use App\Models\Request as PleaRequest;
use App\Models\Vacations\EventDay;
use App\Models\Vacations\Incident;
//Mails
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Vacations\Absentismos;
use App\Http\Controllers\EmployeeController;

class PleaController extends Controller
{
    protected $now, $tomorrow;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // parent::__construct();
        //$this->globals = config('config.globals');
        $this->now = time();
        $this->tomorrow = $this->now + (1 * 24 * 60 * 60);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::where([['type', 'user'], ['user_id', Auth::user()->id], ['status', 'pending']])->paginate(20);
        foreach ($events as $event) {
            $event->status = \Lang::get('bd.' . $event->status);
        }
        return view('vacations.common.active', ['events' => $events]);
    }

    public function getInactive()
    {
        $events = Event::where([['type', 'user'], ['user_id', Auth::user()->id]])
            ->orderBy('created_at', 'DESC')
            ->paginate(20);

        foreach ($events as $event) {
            $event->status = \Lang::get('bd.' . $event->status);
        }
        return view('vacations.common.inactive', ['events' => $events]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //EmployeeController::processVacations();
        if (Auth::user()->number === 'admin') {
            return redirect()->to('/');
        }
        //Esto podría estar fijo (config) y no obtenerlo de la base de datos...
        $data = EmployeeController::getEmployeeBalance();
        $data['vacations'] = EmployeeController::getBalanceDetail(5);
        $data['absentismos'] = Absentismos::get();
        return view('vacations.common.plea', $data);
    }

    public static function checkAvailableUser($user, $start, $end)
    {
        //$start = strtotime($start);
        //$end = strtotime($end);
        $user = Event::where('user_id', $user->id)
            ->where(function ($query) {
                $query->where('status', '<>', 'canceled')
                    ->where('status', '<>', 'rejected');
            })
            ->where('type', 'user')
            ->where(function ($query) use ($start, $end) {
                //je cusi tout va bien
                $query->where('end', strtotime($start))
                    ->orWhereBetween('end', [strtotime($start), strtotime($end)])
                    ->orWhere('start', strtotime($start))
                    ->orWhereBetween('start', [strtotime($start), strtotime($end)])
                    ->orWhere('start', strtotime($end));
            })
            ->first();

        if ($user) {
            return (false);
        } else {
            return (true);
        }
    }

    public static function getHolidays($start)
    {
        $str_start = strtotime($start);
        $dates_config = config('holidays.holidays');
        $dates = [];
        $half_dates = [
            '01-01',
            '01-15',
            '05-01',
            '05-10',
            '06-30',
            '08-15',
            '09-15',
            '10-20',
            '11-01',
            '12-24',
            '12-25',
            '12-31',
        ];
        $year = date('Y', $str_start);
        $years = [$year, strval($year + 1)];

        foreach ($years as $year) {
            foreach ($half_dates as $date) {
                $dates[strtotime($year . '-' . $date)] = $year . '-' . $date;
            }
            if (isset($dates_config[$year])) {
                foreach ($dates_config[$year] as $date) {
                    $dates[strtotime($date)] = $date;
                }
            }
        }

        return $dates;
    }

    public static function checkAvailable($user, $start, $end, $blocked = 1)
    {
        $num_event = 0;
        $str_start = strtotime($start);
        $str_end = strtotime($end);
        $dates_config = config('holidays.holidays');
        $dates = [];
        $half_dates = [
            '01-01',
            '01-15',
            '05-01',
            '05-10',
            '06-30',
            '08-15',
            '09-15',
            '10-20',
            '11-01',
            '12-24',
            '12-25',
            '12-31',
        ];
        $year1 = date('Y', $str_start);
        $year2 = date('Y', $str_end);
        $years = $year1 === $year2 ? $year1 : [$year1, $year2];

        if (is_array($years)) {
            foreach ($years as $year) {
                foreach ($half_dates as $date) {
                    $dates[$year . '-' . $date] = strtotime($year . '-' . $date);
                }
                if (isset($dates_config[$year])) {
                    foreach ($dates_config[$year] as $date) {
                        $dates[$date] = strtotime($date);
                    }
                }
            }
        } else {
            foreach ($half_dates as $date) {
                $dates[$years . '-' . $date] = strtotime($years . '-' . $date);
            }
            if (isset($dates_config[$years])) {
                foreach ($dates_config[$years] as $date) {
                    $dates[$date] = strtotime($date);
                }
            }
        }
        $isValidDay = true;
        foreach ($dates as $date) {
            if (
                $str_start == $date
                || $str_start == $date
                || $str_end == $date
                || $str_end == $date
                || ($str_start < $date && $date < $str_end)
            ) {
                $isValidDay = false;
                $num_event++;
                // break;
            }
        }

        return $num_event;
    }

    public static function checkHoliday($user, $start, $end, $blocked = 0)
    {
        $globals = Event::where('type', 'global')
            ->where('blocked', $blocked)
            ->where(function ($query) use ($start, $end) {
                $query->whereBetween('start', [strtotime($start), strtotime($end)])
                    ->orWhereBetween('end', [strtotime($start), strtotime($end)])
                    ->orWhere('start', strtotime($start))
                    ->orWhere(function ($query2) use ($start, $end) {
                        $query2->where('start', '<', strtotime($start))
                            ->where('end', '>', strtotime($end));
                    });
            })
            ->where(function ($query) use ($user) {
                $query->where('js', 'like', '%"' . $user->id . '"%')
                    ->orWhere([['region_id', $user->region_id], ['js', null]]);
            })
            ->first();
        if ($globals) {
            return (false);
        } else {
            return (true);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = $request->all();
        $event['type'] = 'user';
        $event['status'] = 'pending';
        $event['blocked'] = 0;
        $event['user_id'] = Auth::user()->id;

        if (is_null(Auth::user()->getRegion())) {
            flash('<strong>Error.</strong> No se te ha asignado una región para obtener tu horario', 'warning');
            return redirect()->route('common.plea.create');
        }

        $event['region_id'] = Auth::user()->getRegion()->id;
        $startDay = strtotime($event['start']);
        $endDay = strtotime($event['end']);

        // Verificar si el dia seleccionado no esta bloqueado por la empresa
        $available = PleaController::checkAvailable(Auth::user(), $event['start'], $event['end']);

        // Verificar si el dia seleccionado no haya otro evento del mismo usuario
        $ue = PleaController::checkAvailableUser(Auth::user(), $event['start'], $event['end']);

        /**
         * Esto de abajo es codigo de herbalife, pero son reglas que podrian
         * usarse de ser necesario
         */
        /*if ($event['benefit_id'] == 5)
        {
        if (!Auth::user()->has3MonthsWorking())
        {
        flash('<strong>Error.</strong> No tienes todavia 3 meses en la empresa.', 'danger');
        return redirect('common/plea/create');
        }
        }*/

        if (!Auth::user()->hasAuthorizator()) {
            flash('<strong>Error.</strong> No se te ha asignado a alguien que autorice', 'warning');
            return redirect('common/plea/create');
        }

        $js = [];
        // if (!$available)
        // {
        //     flash('<strong>Error.</strong> El día no esta disponible porque existe un evento empresarial u otro tipo de evento.', 'danger');
        //     return redirect('common/plea/create');
        // }

        if (!$ue) {
            flash('<strong>Error.</strong> Ya tiene un evento activo en la fecha seleccionada.', 'danger');
            return redirect('common/plea/create');
        }

        $balance = EmployeeController::getBalanceDetail($event['benefit_id']);
        if ($balance['benefit']->continuous) {
            $startDay = date('d-m-Y', $startDay);
            $event['start'] = strtotime($event['start']);
            $event['end'] = strtotime($event['end']);
            $e = new Event($event);
            if (!$e->isStartDateWorkable() || $e->isStartDateAnEvent()) {
                flash('<strong>Error.</strong> El fecha de inicio de la solicitud es un dia no laborable, por favor cambialo', 'warning');
                return redirect('common/plea/create');
            }
            $e->generateValidDates();

            /*if($e->getDays() > $balance['benefit']->days && $balance['benefit']->days != 0){
            $message = $balance['benefit']->name.' tiene un rango fijo de '.$balance['benefit']->days.' dia(s) disponible(s)';
            flash('<strong>Error.</strong> '.$message, 'warning');
            return redirect('common/plea/create');
            }*/

            if ($balance['benefit']->days != 0) {
                $returnEndEventDay = $e->continuousDays();
                $days = $balance['benefit']->days;
            } else {
                $returnEndEventDay = $e->end;
                $days = $e->getDays(true);
            }
            $e->end = $returnEndEventDay;
            $e->title .= ' ' . $balance['benefit']->shortname . ' (';
            $e->title .= ($e->start === $e->end) ? $e->getStartDate() : 'del ' . $e->getStartDate() . ' al ' . $e->getEndDate();
            $e->title .= ')';

            $class = 'success';
            $message = 'El evento (' . $event['title'] . ') se ha creado satisfactoriamente.';
            if ($event['start'] < ($this->tomorrow + (15 * 24 * 60 * 60))) {
                $message .= ' Debe procurar realizar sus solicitudes con una anticipación de 15 días.';
                $class = 'warning';
            }

            $e->js = json_encode(['total' => $days]);
            $balance['rows'][0]->pending += $days;
            unset($balance['rows'][0]->diff);
            unset($balance['rows'][0]->solicited);
            DB::beginTransaction();
            try {
                $balance['rows'][0]->save();
            } catch (Exception $e) {
                DB::rollback();
                flash('<strong>Error.</strong> No se puedo actulizar la linea de balance', 'warning');
                return redirect('common/plea/create');
            }
            try {
                $e->save();
            } catch (\Exception $e) {
                DB::rollback();
                flash('<strong>Error.</strong> No se pudo crear el evento', 'warning');
                return redirect('common/plea/create');
            }
            try {
                $u = $e->user;
                $b = Auth::user()->getAuthorizator();
                $r = Incident::create(['user_id' => $b->id, 'from_id' => $u->id, 'benefit_id' => $e->benefit_id, 'event_id' => $e->id, 'week' => date("W", $e->start), 'time' => time(), 'amount' => $e->benefit->days, 'info' => $e->title, 'comment' => '', 'status' => 'pending', 'value' => 0, 'incapacity_id' => 0]);
            } catch (\Exception $e) {
                // dd("Fallo al crear la incidencia");
                DB::rollback();
                flash('<strong>Error.</strong> No se pudo crear la incidencia', 'warning');
                return redirect('common/plea/create');
            }
            DB::commit();
        } else {
            if ($startDay === $endDay) {
                // Traer horario del usuario que esta solicitando dias
                $event['start'] = strtotime($event['start']);
                $event['end'] = strtotime($event['end']);
                $e = new Event($event);
                
                if (!$e->isStartDateWorkable() || $e->isStartDateAnEvent()) {
                    flash('<strong>Error.</strong> El fecha de inicio de la solicitud es un dia no laborable, por favor cambialo', 'warning');
                    return redirect('common/plea/create');
                }
                $schedule = Auth::user()->getSchedule($e->start);
                if (is_null($schedule)) {
                    flash('<strong>Error.</strong> No se ha creado tu calendario laboral.', 'danger');
                    return redirect('common/plea/create');
                }
                if ($schedule->labor == 0) {
                    flash('<strong>Error.</strong> El dia seleccionado es un dia no laborable.', 'danger');
                    return redirect('common/plea/create');
                }
                /**
                 * Esta parte segun yo revisa la tabla de eventos creados
                 * los cuales bloquean el dia y no permiten al usuario pedir dias
                 *
                 */
                /*$holiday = PleaController::checkHoliday(Auth::user() , $event['start'], $event['end']);
                if (!$holiday)
                {
                flash('<strong>Error.</strong> Ya tiene un evento activo en la fecha seleccionada.', 'danger');
                return redirect('common/plea/create');
                }*/

                $class = 'success';
                $e->title .= ' ' . $balance['benefit']->shortname . ' (' . $e->getStartDate() . ')';
                $message = 'El evento (' . $event['title'] . ') se ha creado satisfactoriamente.';

                if ($event['start'] < ($this->tomorrow + (15 * 24 * 60 * 60))) {
                    $message .= ' Debe procurar realizar sus solicitudes con una anticipación de 15 días.';
                    $class = 'warning';
                }

                // Regresa el total de dias laborales que el evento cubrira, es decir, al incluirse dias no laborables estos no se tomaran en cuenta
                $days = $e->getDays();

                if ($balance['diff'] < $days) {
                    flash('<strong>Error.</strong> No puede solicitar más días de los que se le otorgaron.', 'danger');
                    return redirect('common/plea/create');
                }

                $js = ['total' => $days];
                $i = 0;
                $rowBalances = $balance['rows'];
                //Cuando solo tienes una linea de balance de X beneficio y tienes dias disponibles
                $val = false;

                foreach ($rowBalances as $key => $rowBalance) {
                    // dd($rowBalance->amount, $rowBalance->pending);
                    if ($rowBalance->amount < $rowBalance->pending) {
                        $started = "";
                        if (is_null($rowBalance->until)) {
                            $anioVac = $rowBalance->year;
                            $started = Auth::user()->started_at;
                            $started = strtotime(str_replace('-', '/', $started));
                            $started = strtotime(date('d-m-' . $anioVac, $started));
                            $started = strtotime(' +18 month', $started);
                        }
                        if (strtotime($event['start']) < strtotime($rowBalance->until)
                            || strtotime($event['start']) < $started) {
                            $rowBalance->amount += 1;
                            unset($rowBalance->diff);
                            unset($rowBalance->solicited);
                            // $rowBalance->save();
                            $js['balances'][] = [
                                'id' => $rowBalances[$i]->id,
                                'date' => $e->getStartDate('Y-m-d'),
                                'amount' => 1,
                            ];
                            $val = true;
                            $e->js = json_encode($js);
                            DB::beginTransaction();
                            try {
                                $rowBalance->save();
                            } catch (Exception $e) {
                                DB::rollback();
                                flash('<strong>Error.</strong> No se puedo actualizar los balances', 'warning');
                                return redirect('common/plea/create');
                            }
                            try {
                                $e->save();
                            } catch (\Exception $e) {
                                DB::rollback();
                                // $rowBalance->amount-= 1;
                                // $rowBalance->save();
                                flash('<strong>Error.</strong> No se puedo crear el evento', 'warning');
                                return redirect('common/plea/create');
                            }
                            try {
                                $u = $e->user;
                                $b = Auth::user()->getAuthorizator();
                                $r = Incident::create(['user_id' => $b->id, 'from_id' => $u->id, 'benefit_id' => $e->benefit_id, 'event_id' => $e->id, 'week' => date("W", $e->start), 'time' => time(), 'amount' => $e->getDays(), 'info' => $e->title, 'comment' => '', 'status' => 'pending', 'value' => 0, 'incapacity_id' => 0]);
                            } catch (\Throwable $e) {
                                DB::rollback();
                                flash('<strong>Error.</strong> No se puedo crear la incidencia.', 'warning');
                                return redirect('common/plea/create');
                            }
                            foreach ($js['balances'] as $key => $line) {
                                try {
                                    EventDay::create([
                                        'date' => $line['date'],
                                        'processed' => 0,
                                        'balance_id' => $line['id'],
                                        'event_id' => $e->id,
                                    ]);
                                } catch (\Throwable $th) {
                                    flash('<strong>Error.</strong> Fallo al guardar los dias. |' . $th->getMessage(), 'danger');
                                    return redirect('common/plea/create');
                                }
                            }
                            DB::commit();
                            break;
                        } else {
                            flash('<strong>Error.</strong> No puede solicitar días fuera de la vigencía.', 'danger');
                            return redirect('common/plea/create');
                        }
                    }
                }
                if (!$val) {
                    flash('<strong>Error.</strong> El dia esta fuera del alcance en el que puedes pedir.', 'danger');
                    return redirect('common/plea/create');
                }

            } else {
                $class = 'success';
                // $event['title'].= ' ' . $balance['benefit']->shortname . ' (del ' . $event['start'] . ' al ' . $event['end'] . ')';
                $message = 'El evento (' . $event['title'] . ') se ha creado satisfactoriamente.';
                if ($event['start'] < ($this->tomorrow + (15 * 24 * 60 * 60))) {
                    $message .= ' Debe procurar realizar sus solicitudes con una anticipación de 15 días.';
                    $class = 'warning';
                }

                $event['start'] = strtotime($event['start']);
                $event['end'] = strtotime($event['end']);
                $e = new Event($event);
                if (!$e->isStartDateWorkable() || $e->isStartDateAnEvent()) {
                    flash('<strong>Error.</strong> El fecha de inicio de la solicitud es un dia no laborable, por favor cambialo', 'warning');
                    return redirect('common/plea/create');
                }
                $e->generateValidDates();

                // Conteo de dias seleccionados
                $days = $e->getDays(true);
                if ($balance['diff'] < $days) {
                    flash('<strong>Error.</strong> No puede solicitar más días de los que se le otorgaron.', 'danger');
                    return redirect('common/plea/create');
                }

                $schedule = $e->getUserSchedule();
                $daysBloqueados = [];
                $daysAvailables = [];
                $firstDay = '';
                $lastDay = '';
                foreach ($schedule as $key => $sday) {
                    if ($sday->labor == 1) {
                        if (empty($firstDay)) {
                            $firstDay = $sday->date;
                        }
                        $lastDay = $sday->date;
                        $daysAvailables[] = $sday->date;
                    } else {
                        $daysBloqueados[] = $sday->date;
                    }
                }
                $e->start = strtotime($firstDay);
                $e->end = strtotime($lastDay);
                $e->title .= ' ' . $e->benefit->shortname . ' (del ' . $e->getStartDate() . ' al ' . $e->getEndDate() . ')';

                if (!$daysAvailables) {
                    flash('<strong>Error.</strong> Los dias que seleccionastes no laboras.', 'danger');
                    return redirect('common/plea/create');
                }
                $rowBalances = $balance['rows'];

                $val = false;
                $cont = 0;
                for ($i = 0; $i < count($rowBalances); $i++) {
                    $cont += $rowBalances[$i]->pending - $rowBalances[$i]->amount;
                }
                if ($cont < count($daysAvailables)) {
                    flash('<strong>Error.</strong> No puede solicitar más días de los que se le otorgaron.', 'danger');
                    return redirect('common/plea/create');
                }
                $i = 0;
                DB::beginTransaction();
                for ($j = 0; $j < count($daysAvailables); $j++) {

                    $started = null;
                    if ($rowBalances[$i]->amount < $rowBalances[$i]->pending) {
                        // dd($rowBalances[$i],is_null($rowBalances[$i]->until));
                        if (is_null($rowBalances[$i]->until)) {
                            $anioVac = $rowBalances[$i]->year;
                            $started = Auth::user()->started_at;
                            $started = strtotime(str_replace('-', '/', $started));
                            $started = strtotime(date('d-m-' . $anioVac, $started));
                            $started = strtotime(' +18 month', $started);
                            // dd($started);
                        }
                        // dd($daysAvailables,strtotime($daysAvailables[$j]),$started,strtotime($daysAvailables[$j]) < $started);
                        if (strtotime($daysAvailables[$j]) < strtotime($rowBalances[$i]->until)
                            || strtotime($daysAvailables[$j]) < $started) {
                            $rowBalances[$i]->amount += 1;
                            unset($rowBalances[$i]->diff);
                            unset($rowBalances[$i]->solicited);
                            try {
                                $rowBalances[$i]->save();
                            } catch (Exception $e) {
                                DB::rollback();
                                flash('<strong>Error.</strong> No se puedo actualizar una linea de balance |' . $e->getMessage(), 'warning');
                                return redirect('common/plea/create');
                            }

                            $js['balances'][] = [
                                'id' => $rowBalances[$i]->id,
                                'date' => $daysAvailables[$j],
                                'amount' => 1,
                            ];
                            $val = true;

                        } else {
                            flash('<strong>Error.</strong> No puede solicitar días fuera de la vigencía.', 'danger');
                            return redirect('common/plea/create');
                        }
                    } else if ($i < count($rowBalances)) {
                        $i++;
                        $j--;
                    }
                }
                if (!$val) {
                    flash('<strong>Error.</strong> El dia esta fuera del alcance en el que puedes pedir. |' . $e->getMessage(), 'danger');
                    return redirect('common/plea/create');
                } else {
                    $e->js = json_encode($js);
                    try {
                        $e->save();
                    } catch (\Exception $e) {
                        DB::rollback();
                        flash('<strong>Error.</strong> No se pudo crear el evento |' . $e->getMessage(), 'warning');
                        return redirect('common/plea/create');
                    }

                    foreach ($js['balances'] as $key => $line) {
                        try {
                            EventDay::create([
                                'date' => $line['date'],
                                'processed' => 0,
                                'balance_id' => $line['id'],
                                'event_id' => $e->id,
                            ]);
                        } catch (\Throwable $th) {
                            flash('<strong>Error.</strong> Fallo al guardar los dias. |' . $th->getMessage(), 'danger');
                            return redirect('common/plea/create');
                        }
                    }

                    try {
                        $u = $e->user;
                        $b = Auth::user()->getAuthorizator();
                        $r = Incident::create(['user_id' => $b->id, 'from_id' => $u->id, 'benefit_id' => $e->benefit_id, 'event_id' => $e->id, 'week' => date("W", $e->start), 'time' => time(), 'amount' => count($daysAvailables) /*$e->getDays()*/
                            , 'info' => $e->title, 'comment' => '', 'status' => 'pending', 'value' => 0, 'incapacity_id' => 0]);
                    } catch (\Exception $e) {
                        DB::rollback();
                        flash('<strong>Error.</strong> No se pudo crear la incidencia |' . $e->getMessage(), 'warning');
                        return redirect('common/plea/create');
                    }

                }
                DB::commit();
            }
        }
        /*if (in_array(Auth::user()->manager, config('config.job_code')))
        {
        $managers = User::where('role', 'manager')->get();
        foreach($managers as $manager)
        {
        // Mail::to($manager->email)->send(new EmployeeRequest($r)); // Super
        // Mail::to("user@example.com")->send(new EmployeeRequest($r)); // Manager
        }
        }
        else
        {
        // Mail::to("user@example.com")->send(new EmployeeRequest($r)); // Super
        // Mail::to($b->email)->send(new EmployeeRequest($r)); // Super
        }*/
        // Aqui iria el correo al jefe
        // Mail::to("user@example.com")->send(new PleaCreated($e)); // Employee
        // Mail::to(Auth::user()->email)->send(new PleaCreated($e)); // Employee
        flash($message, $class);
        return redirect('/common/plea/create');
    }

    public static function removeDays($balance, $days)
    {
        $js = ['total' => $days];
        foreach ($balance['rows'] as $b) {
            $c = 0;
            while ($b->diff > 0 && $days > 0) {
                $c++;
                $b->diff--;
                $days--;
            }
            if ($c > 0) {
                $b->amount += $c;
                unset($b->diff);
                unset($b->solicited);
                $b->save();
                $js['balances'][] = ['id' => $b->id, 'amount' => $c];
            }
        }
        return (json_encode($js));
    }

    public static function restoreDays($event, $days = 0)
    {
        if ($days > 0) {
            $js = json_decode($event->js);
            foreach ($js->balances as $b) {
                $balance = Balances::find($b->id);
                if ($balance && $days > 0) {
                    while ($b->amount > 0 && $days > 0) {
                        $b->amount--;
                        $days--;
                        $balance->amount--;
                    }
                    $balance->save();
                }
            }
        } else {
            $js = json_decode($event->js);
            foreach ($js->balances as $b) {
                $balance = Balances::find($b->id);
                if ($balance) {
                    $balance->amount -= $b->amount;
                    $balance->save();
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $data = [];
            if ($id == 0) {
                $data = ['success' => 0, 'error' => "Warning: No hay evento (no fue una solicitud)."];
                return response()->json($data);
            }

            $r = Incident::where('event_id', $id)->first();

            if ($r->status == 'processed') {
                $data = ['success' => 0, 'error' => "Error: Solicitud processada."];
                return response()->json($data);
            } else if ($r->status == 'canceled') {
                $data = ['success' => 0, 'error' => "Error: Solicitud cancelada."];
                return response()->json($data);
            }

            $r->status = 'canceled';
            $r->event->status = 'canceled';
            DB::beginTransaction();
            try {
                $r->event->save();
            } catch (Exception $e) {
                DB::rollback();
                $data = ['success' => 0, 'error' => "Error: al guardar el evento."];
                return response()->json($data);
            }
            try {
                $r->save();
            } catch (Exception $e) {
                DB::rollback();
                $data = ['success' => 0, 'error' => "Error: al guardar la incidencia."];
                return response()->json($data);
            }

            $days = $r->event->getDays(true);

            $balance = EmployeeController::getBalanceDetail($r->event->benefit_id, $r->event->user_id);

            if ($balance['benefit']->type == "pending") {
                try {
                    PleaController::restoreDays($r->event);
                } catch (Exception $e) {
                    DB::rollback();
                    $data = ['success' => 0, 'error' => "Error: al recuperar los dias."];
                    return response()->json($data);
                }

            } else {
                $balance['rows'][0]->pending -= $days;
                unset($balance['rows'][0]->diff);
                unset($balance['rows'][0]->solicited);
                try {
                    $balance['rows'][0]->save();
                } catch (Exception $e) {
                    DB::rollback();
                    $data = ['success' => 0, 'error' => "Error: al recuperar los dias."];
                    return response()->json($data);
                }

            }
            DB::commit();
            $data = ['success' => true];
            //Esto no se que onda...
            //Esta parte revisa si el evento es pasado o no, pero aun asi lo borra
            // if($r->event->end < $this->now) {
            //     $data = ['success' => 1, 'error' => " de evento pasado"];
            // } else {
            //     $data = ['success' => 1, 'error' => ""];
            // }

            return response()->json($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('/home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect('/home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //It most never happend.
    }
}
