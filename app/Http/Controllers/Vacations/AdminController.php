<?php

namespace App\Http\Controllers\Vacations;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\User;
use App\Http\Requests;
use App\Models\Vacations\Benefits;
use App\Models\Vacations\Incident;
use App\Models\Region;
// use App\Models\Userfields;
use Lang;
//use App\Models\Request as PleaRequest;


class AdminController extends Controller
{
    public function incidents()
    {
        return view('admin.incidents');
    }
    public function requests()
    {
        $requests = Incident::whereHas('benefit',function($query2){
            $query2->where('group','benefit');
        })
        ->whereIn('from_id', Auth::user()->getSubordinatesAsUsersIDs())
        ->get();
        return view('vacations.admin.requests', compact('requests'));
    }
    public function config()
    {
        /*$c = Userfields::where('fieldname','config')->first();
        if($c) {
            // dd($c->getInfo());
            $config = $c->getInfo();
        } else {
            $config = config('config');
        }*/
        $config = [];
        return view('admin.config.edit',['config' => $config]);
    }
    public function setconfig(Request $request)
    {
        $c = Userfields::where('fieldname','config')->first();
        unset($request['_token']);
        if($c) {
            Userfields::updateOrCreate(
                ['fieldname' => 'config'],
                ['user_id' => Auth::user()->id, 'value' => json_encode($request->all())]
            );
        }
        // $this->config->update();
        flash('La configuración ha sido guardada','success');
        return redirect('admin/config');
    }

    public function rules()
    {
        $data = ['regions' => Region::get()];
        return view('admin.rules',$data);

    }

    //This table is for the rejected insidents...
    public function tableRequests(Request $request)
    {
        if($request->ajax()){
            $vars = $request->all();
            $limit = (isset($vars['limit']))?$vars['limit']:10;
            $offset = (isset($vars['offset']))?$vars['offset']:0;
            $sort = (isset($vars['sort']))?$vars['sort']:'created_at';
            $order = (isset($vars['order']))?$vars['order']:'DESC';
            $uid = Auth::user()->id;
            // $bossID = Auth::user()->boss_id;
            //Session...
            session(['sort'=>$sort]);
            session(['order'=>$order]);
            session(['field'=>'']);
            session(['search'=>'']);
			session(['start'=>'']);
            session(['end'=>'']);


            $query = Incident::whereHas('benefit',function($query2){
                    $query2->where('group','benefit');
                });

            $query->whereIn('from_id', Auth::user()->myEmployeesIDs());

            if($start = $request->input('start')) {
                $end = $request->input('end');

                $start = strtotime($start);
                $end = strtotime($end);

                session(['start'=>$start]);
                session(['end'=>$end]);

                $tmp = ['start'=>$start,'end'=>$end];
                $query->where(function($q) use ($tmp) {
                    //$q->whereBetween('time',$tmp);    
                    $q->orWhereHas('event',function($tq) use ($tmp){
                          $tq->whereBetween('start',$tmp);
                    });
                });
            }

            if($search = $request->input('search')) {

                session(['search'=>$search]);

                if($field = $request->input('field')) {

                    session(['field'=>$field]);

                    switch($field) {
                        case 'boss'     : $query->whereHas('from',function($tq2) use ($search) {
                                                $tq2->whereHas('boss',function($query2) use ($search) {
                                                    $query2->where('firstname', 'like', "%$search%")
                                                          ->orWhere('lastname', 'like', "%$search%")
                                                          ->orWhere('number', 'like', "%$search%");
                                                });
                                            });
                                           break;
                        case 'benefit'  :  $query->whereHas('benefit',function($query2) use ($search) {
                                                $query2->where('group','benefit');
                                                $query2->where('shortname', 'like', "%$search%")
                                                      ->orWhere('name', 'like', "%$search%");
                                            });
                                            break;
                    }

                } else if(strtotime($search)) {
                    $query->where('created_at', 'like',"%$search%");
                } else {
                    $query->where(function($tq) use ($search){
                        $tq->whereHas('from',function($query2) use ($search){
                            $query2->where('firstname', 'like', "%$search%")
                                  ->orWhere('lastname', 'like', "%$search%")
                                  ->orWhere('number', 'like', "%$search%");
                        })
                        ->orWhere('status', 'like', "%$search%");
                    });
                }
            }

            $total = $query->count();

            $requests = $query->orderBy($sort,$order)
                        ->offset($offset)
                        ->limit($limit)
                        ->get();

            $data  = [];
            foreach($requests as $request) {
                $request->username = $request->from->FullName;
                $request->bossname = $request->user->FullName;
                $request->benefitname = $request->benefit->shortname;
                $request->description = ($request->event_id)?$request->event->title:$request->info;
                if($request->status == 'rejected'){
                    $request->operations = '';
                }else{
                    $request->operations = '
                    <div class="">
                        <button onclick="destroy(this,'.$request->event_id.')" type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>';
                }
                $request->status = \Lang::get('bd.'.$request->status);
                
                $data[] = $request;
            }
            return response()->json(['data'=>$data,'total'=>$total]);
        }
    }
    //This table is for the rejected insidents...
    public function tableIncidents(Request $request)
    {
        if($request->ajax()){
            $uid = Auth::user()->id;
            $vars = $request->all();
            $limit = (isset($vars['limit']))?$vars['limit']:10;
            $offset = (isset($vars['offset']))?$vars['offset']:0;
            $sort = (isset($vars['sort']))?$vars['sort']:'created_at';
            $order = (isset($vars['order']))?$vars['order']:'ASC';
            $total = Incident::where('status','<>','pending')->count();

            //Session...
            session(['sort'=>$sort]);
            session(['order'=>$order]);
            session(['search'=>'']);
            session(['start'=>'']);
            session(['end'=>'']);

            $query = Incident::whereHas('benefit',function($query2){
                        $query2->where('group','incident');
                        $query2->orWhere('group','incapacity');
                    });

            if(!config('config.showpending')) {
                $query->where('status','<>','pending');
            }

            if(config('config.viewincidents') == 'user' || (config('config.viewincidents') == 'super' && Auth::user()->role == 'supervisor')) {
                $query->where('user_id', $uid);
            }

            if($start = $request->input('start')) {
                $end = $request->input('end');

                $start = strtotime($start);
                $end = strtotime($end);

                session(['start'=>$start]);
                session(['end'=>$end]);

                $tmp = ['start'=>$start,'end'=>$end];
                $query->whereBetween('time',$tmp);
            }

            if($search = $request->input('search')) {
                session(['search'=>$search]);

                if($field = $request->input('field')) {

                    session(['field'=>$field]);

                    switch($field) {
                        case 'boss'     : $query->whereHas('user',function($query2) use ($search){
                                                $query2->where('firstname', 'like', "%$search%")
                                                      ->orWhere('lastname', 'like', "%$search%")
                                                      ->orWhere('number', 'like', "%$search%");
                                          });
                                          break;

                        case 'benefit'  : $query->whereHas('benefit',function($query2) use ($search) {
                                                $query2->where(function($query3){
                                                    $query3->where('group','incident');
                                                    $query3->orWhere('group','incapacity');
                                                });
                                                $query2->where(function($query3) use ($search){
                                                    $query3->where('shortname', 'like', "%$search%")
                                                          ->orWhere('name', 'like', "%$search%");
                                                });
                                          });
                                          break;
                    }

                } else if($time = strtotime($search)) {
                        $query->where('time', $time);
                } else {
                    $query->where(function($tq) use ($search){
                        $tq->whereHas('from',function($query2) use ($search){
                            $query2->where('firstname', 'like', "%$search%")
                                  ->orWhere('lastname', 'like', "%$search%")
                                  ->orWhere('number', 'like', "%$search%");
                        })
                        ->orWhere('status','like',"%$search%");
                    });
                }
            }

            //echo($query->toSql());die;

            $total = $query->count();
	
            $incidents = $query->orderBy($sort,$order)
                        ->offset($offset)
                        ->limit($limit)
                        ->get();

            $data  = [];
            $incapacity = config('config.incapacity');
            foreach($incidents as $incident) {
                $incident->username = $incident->from->FullName;
                $incident->bossname = $incident->user->FullName;
                $incident->boss     = $incident->from->boss->FullName;
                $incident->benefitname = $incident->benefit->name;
				$incident->status = \Lang::get('bd.'.$incident->status);
                $incident->time = date('Y-m-d',$incident->time);
				$incident->description = ($incident->event_id)? $incident->event->title:$incident->info;
                if($incident->benefit->group == 'incapacity') {
                    if($incapacity) {
                        if($incident->incapacity) {
                            $incident->operations = '
                                <a title="Ver" alt="Ver" href="/admin/incapacity/'.$incident->incapacity_id.'">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a> &nbsp;&nbsp;';
                        } else {
                            $incident->operations = '
                                <a title="Agregar" alt="Agregar" href="/admin/incapacity/'.$incident->id.'/edit">
                                    <span class="glyphicon glyphicon-plus"></span>
                                </a> &nbsp;&nbsp;';
                        }
                    }
                }
                unset($incident->benefit);
                unset($incident->from);
                unset($incident->user);
                $data[] = $incident;
            }

            return response()->json(['data'=>$data,'total'=>$total]);
        }
    }

    public function time(){
        $incidents = Incident::where('benefit_id', 1)
        ->with('incidentOvertime', 'incidentOvertime.schedule', 'from')
        ->get();

        return view('admin.time.index', compact('incidents'));
    }
}
