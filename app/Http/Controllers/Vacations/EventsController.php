<?php

namespace App\Http\Controllers\Vacations;

use App\User;
use App\Http\Requests;
use App\Models\Region;

use Illuminate\Http\Request;
use App\Models\Vacations\Event;
use App\Models\Vacations\Benefits;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class EventsController extends Controller
{

    protected $now, $tomorrow, $globals;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->globals = config('config.globals');
        $this->now = time();
        $this->tomorrow = $this->now + (1 * 24 * 60 * 60);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $depend=3;
        //Future Always
        //Si lo necesito mas de una vez lo paso al model sino no...
	    return view('vacations.supervisor.events.events');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Saving old code...
        $user = Auth::user();
        $data = [
            'benefits' => Benefits::where('context','admin')->get()
        ];
        if($user->role == 'admin') {
            $data['regions'] = Region::has('employees')->select('id','name')->get();
        } else if($user->role == 'supervisor') {
            $query = User::select('id','firstname','lastname')
                ->where('boss_id', $user->id)
                ->where('status','active');
            $data['total'] = $query->count();
            $data['users'] = $query->orderBy('lastname','asc')->get();
        }
        return view('vacations.supervisor.events.createevent', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = $request->all();
        $event['user_id'] = Auth::user()->id;
        $event['start'] = strtotime($event['start']);
        $event['end'] = strtotime($event['end']);
        if(!isset($event['blocked'])) $event['blocked'] = 0;

        if(Auth::user()->role == 'supervisor') {
            if(empty($event['users'])) {
                flash(trans('pages.ecf_message1'),'danger');
                return redirect('/super/events/create');
            }
            if(!Privilege::direct(Auth::user(),$event['users'])) {
                flash('No tiene permiso para terminar la operación.','danger');
                return redirect('super/events/create');
            }
            $event['region_id'] = Auth::user()->region_id;
            $event['js'] = json_encode($event['users']);
            unset($event['users']);
            $result = new Event($event);
            if($result->benefit->continuous) { $result->continuous(); }
            $result->save();
        } else if(Auth::user()->role == 'admin') {
            if(empty($event['regions'])) {
                flash(trans('pages.ecf_message2'),'danger');
                return redirect('/super/events/create');
            }
            $regions = $event['regions'];
            unset($event['regions']);
            foreach($regions as $region) {
                $event['region_id'] = $region;
                $result = new Event($event);
                if($result->benefit->continuous) { $result->continuous(); }
                $result->save();
            }
        }

		if($result) {
            flash(trans('pages.ecf_message3', ['name'=>$event['title']]),'success');
        } else {
            flash(trans('pages.ecf_message4'),'danger');
        }
		return redirect('/super/events');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('/home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect('/home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //It must never happend.
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()) {
            $event = Event::find($id);
            $data = ['success' => 0, 'error' => trans('pages.ect_message3')];
            if($event->end < $this->now) {
                $data = ['success' => 0, 'error' => trans('pages.ect_message4')];
            }
            if($event->delete()) {
                $data = ['success' => 1, 'error' => "No error"];
            }
            return response()->json($data);
        }
    }

    public function getUserEvents(Request $request)
    {
        if($request->ajax()) {
            $from = $request->get('from');
            $to = $request->get('to');            
            if(empty($from) || empty($to)) {
                return response()->json(['success' => 0, 'error' => 'No correct request.']);
            }            
            $from = $from / 1000;
            $from = $from - (24*60*60);
            $to = $to / 1000;
            $to = $to + (24*60*60);
            // dd($from,$to,$from2,$to2);
            //Se obtiene la fecha recibida menos 1 año
            $fromOneYearAgo = date('d-m-Y',$from);
            $fromOneYearAgo = strtotime($fromOneYearAgo .' -1 year');
            // $fromOneYearAgo = date('d-m-Y',$fromOneYearAgo);
            //Se obtiene la fecha recibida mas 1 año
            $toOneYearAhead = date('d-m-Y',$to);
            $toOneYearAhead = strtotime($toOneYearAhead .' +1 year');
            // $toOneYearAhead = date('d-m-Y',$toOneYearAhead);

            $year = Event::where('type','global')
                        ->where(function($query) use ($fromOneYearAgo,$toOneYearAhead){
                            $query->whereBetween('start',[$fromOneYearAgo,$toOneYearAhead])
                                  ->orWhereBetween('end',[$fromOneYearAgo,$toOneYearAhead]);
                        })
                        ->where(function($query){
                            $user = Auth::user();
                            $query->where('js','like','%"' . $user->id . '"%')
                                  ->orWhere([['region_id',$user->region_id],['js',NULL]]);
                        })
                        ->get();

                        // dd($year,[$fromOneYearAgo,$toOneYearAhead],[$from,$to]);
            $globals = Event::where('type','global')
                        ->where(function($query) use ($from,$to){
                            $query->whereBetween('start',[$from,$to])
                                  ->orWhereBetween('end',[$from,$to]);
                        })
                        ->where(function($query){
                            $user = Auth::user();
                            $query->where('js','like','%"' . $user->id . '"%')
                                  ->orWhere([['region_id',$user->region_id],['js',NULL]]);
                        })
                        ->get();
            $userevents = Event::where('user_id', Auth::user()->id)
                        ->where(function($query) use ($from,$to) {
                            $query->whereBetween('start',[$from,$to])
                                  ->orWhereBetween('end',[$from,$to]);
                        })
                        ->get();

            $events = $globals->merge($userevents);
            $events = $year->merge($events);
            $tmp = [];
            foreach($events as $event) {
                $days = $event->getTotalDays() - $event->getDays();
                if($days != 0) {
                    $schedule = $event->user->getMySchedule();
                    $total = $event->getTotalDays() + 1;
                    $start = $event->start;
                    for($i=1;$i<$total;$i++) {
                        $e = new Event($event->toArray());
                        $e->start = $start;
                        $e->end = $e->start;
                        $d = date('D',$e->start);
                        if($schedule[$d][2] == 0) {
                            $start += (1 * 24 * 60 * 60);
                            continue;
                        }
                        $e->start = ($e->start + (1 * 24 * 60 * 60)) * 1000;
                        $e->end   = ($e->end + (1 * 24 * 60 * 60))   * 1000;
                        dd($e->start,$e->end,$d);
                        switch($e->status) {
                            case 'global': $e->class = "event-important"; break;
                            case 'pending': $e->class = "event-info"; break;
                            case 'processing': $e->class = "event-warning"; break;
                            case 'accepted': $e->class = "event-success"; break;
                            case 'processed': $e->class = "event-success"; break;
                            case 'rejected': $e->class = "event-special"; break;
                        }
                        $tmp[] = $e;
                        $start += (1 * 24 * 60 * 60);
                    }
                } else {
                    $event->start = ($event->start + (1 * 24 * 60 * 60)) * 1000;
                    $event->end   = ($event->end + (1 * 24 * 60 * 60))   * 1000;
                    switch($event->status) {
                        case 'global': $event->class = "event-important"; break;
                        case 'pending': $event->class = "event-info"; break;
                        case 'processing': $event->class = "event-warning"; break;
                        case 'accepted': $event->class = "event-success"; break;
                        case 'processed': $event->class = "event-success"; break;
                        case 'rejected': $event->class = "event-special"; break;
                    }
                    $tmp[] = $event;
                }
            }
            $data = ['success' => 1, 'result' => $tmp];
            return response()->json($data);
        }
    }

    public function getNextBlocked(Request $request)
    {
        $dates = [];
        if($request->ajax()) {
            //El administrador no debe ser bloqueado!
            if(Auth::user()->role == 'admin') {
                return response()->json($dates);
            }
            //Solo bloquea los eventos de la empresa!
            $events = Event::select('start','end')
                            ->where([['start', '>', $this->tomorrow],['status', 'global'],['blocked', '1']])
                            ->where(function($query){
                                $user = Auth::user();
                                $query->where('js','like','%"' . $user->id . '"%')
                                      ->orWhere([['region_id',$user->region_id],['js',NULL]]);
                            })
                            ->limit(50)
                            ->get();
            foreach($events as $event) {
                while($event->start <= $event->end) {
                    $dates[] = date('d-m-Y',$event->start);
                    $event->start += (1 * 24 * 60 * 60);
                }
            }
            return response()->json($dates);
        }
    }
    //This table is for administration events
    public function table(Request $request)
    {
        if($request->ajax()){
            $vars = $request->all();
            $limit = (isset($vars['limit']))?$vars['limit']:10;
            $offset = (isset($vars['offset']))?$vars['offset']:0;
            $sort = (isset($vars['sort']))?$vars['sort']:'created_at';
            $order = (isset($vars['order']))?$vars['order']:'desc';
            $total = Event::where('status', '=', 'global')->count();
            if($request->input('search')){
                $search = $request->input('search');
                $region = Region::select('id')->where('code', 'like',"%$search%")->first();
                $events = Event::where('status', '=', 'global')
                      ->where(function($query) use ($search,$region){
                          $query->where('title', 'like',"%$search%")
                                ->orWhere('region_id', (isset($region->id))?$region->id:'%');
                      })
                      ->orderBy($sort,$order)
                      ->offset($offset)
                      ->limit($limit)
                      ->get();
                      $total = Event::where('status', '=', 'global')
                            ->where(function($query) use ($search,$region){
                                $query->where('title', 'like',"%$search%")
                                      ->orWhere('region_id', (isset($region->id))?$region->id:'%');
                            })
                            ->orderBy($sort,$order)
                            ->count();
            } else {
                $events = Event::where('status', '=', 'global')
                    ->orderBy($sort,$order)
                    ->offset($offset)
                    ->limit($limit)
                    ->get();
            }
            $data = [];
            foreach($events as $event){
                $event->start = date('d-m-Y',$event->start);
                $event->end = date('d-m-Y',$event->end);
                $event->blocked = ($event->blocked == 1)?'Si':'No';
                $event->r = (count($event->region) == 0) ? 'No existe la region' : $event->region->code;
                if((Auth::user()->role == "supervisor" && $event->user->role != 'admin') || Auth::user()->role == "admin"){
                    $event->delete = '
                    <div class="pull-left">
                        <button onclick="destroy(this,'.$event->id.')" type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>';
                }else{
                    $event->delete = '';
                }
                $data[] = $event;

            }
            return response()->json(['data'=>$data,'total'=>$total]);
        }
    }
}
