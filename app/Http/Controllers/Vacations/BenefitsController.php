<?php

namespace App\Http\Controllers\Vacations;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Vacations\Benefits;

class BenefitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $benefits = Benefits::get();
        return view('admin.incidents.index',['benefits' => $benefits]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.incidents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $benefit = $request->all();
        $res = Benefits::create($benefit);
        if($res) {
            flash('Registro creado.','success');
        } else {
            flash('Error de base de datos.','danger');
        }
        return redirect('/admin/incidents');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $benefit = Benefits::find($id);
        return view('admin.incidents.show',['benefit' => $benefit]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $benefit = Benefits::find($id);
        return view('admin.incidents.edit',['benefit' => $benefit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tmp = $request->all();
        //print_r($tmp); die;
        unset($tmp['_method']);
        unset($tmp['_token']);
        $benefit = Benefits::where('id',$id)->update($tmp);
        flash('Registro actualizado.','warning');
        return redirect('/admin/incidents');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $benefit = Benefits::find($id);
        $benefit->delete();
        flash('Registro eliminado','danger');
        return redirect('/admin/incidents');
    }
}
