<?php

namespace App\Http\Controllers\Vacations;

use DB;
use App\User;
use App\Http\Requests;
use App\Mail\PleaSuper;

use App\Models\UserClock;
use App\Models\Responselog;
//Models
use App\Mail\PleaSuperSuper;
use Illuminate\Http\Request;
use App\Mail\EmployeeRequest;
use App\Models\Vacations\Event;
use App\Mail\PleaAceptedEmployee;
use App\Mail\PleaRejectedEmployee;
// use App\Models\Userfields;
//Mails
use App\Models\Vacations\Balances;
use App\Models\Vacations\Benefits;
use App\Models\Vacations\Incident;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use App\Http\Controllers\EmployeeController;
use Illuminate\Pagination\LengthAwarePaginator;

class SuperController extends Controller
{
    public function approve($request)
    {
        if($request == 'requests') {
            $query = Incident::whereHas('benefit',function($query2){
                    $query2->where('group','benefit');
                })
                ->where('event_id','<>','0')
                ->where('user_id',Auth::user()->id)
                ->where('status','pending')
                ->orderBy('user_id')
                ->orderBy('created_at','DESC');
            $data = [ $request => $query->paginate(40)];
            return view('vacations.supervisor.'.$request,$data);
        } elseif($request == 'incidents') {
            if(config('config.gradematters')) {
                $user = Auth::user();
                $g    = substr($user->grade,1);
                if($g < 120) {
                    $data = $this->toManager();
                    return view('vacations.supervisor.incidentsmanager',$data);
                } else {
                    $data = $this->toSuper();
                    return view('vacations.supervisor.incidents',$data);
                }
            } else {
                $data = $this->toSuper();
                return view('vacations.supervisor.incidents',$data);
            }

        } else {
            return redirect('home');
        }
    }

    private function toManager()
    {
        //Get current page form url e.g. &page=6
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        $views = session('incidents');
        $views[$currentPage] = true;
        session(['incidents' => $views]);

        $users = Incident::select('from_id')
            ->where(function($query){
                $query->whereHas('benefit',function($query2){
                    $query2->where('group','incident');
                    $query2->orWhere('group','incapacity');
                })
                ->orWhere('event_id','0');
            })
            //->distinct()
            ->where('user_id',Auth::user()->id)
            ->where('status','pending')
            ->get()
            ->toArray();

        $user = false;
        if($users){
            $user = User::select('boss_id')
                        ->distinct()
                        ->whereIn('id',$users)
                        ->offset($currentPage-1)
                        ->limit(1)
                        ->first();
        }

        $collection = [];
        $userclock = false;
        if($user) {
            $users = User::select('boss_id')
                        ->distinct()
                        ->whereIn('id',$users)
                        ->get();
            //Create a new Laravel collection from the array data
            $collection = Incident::where(function($query){
                    $query->whereHas('benefit',function($query2){
                        $query2->where('group','incident');
                        $query2->orWhere('group','incapacity');
                    })
                    ->orWhere('event_id','0');
                })
                ->where('user_id',Auth::user()->id)
                ->where('status','pending')
                ->whereHas('from',function($query) use ($user){
                    $query->select('id')->where('boss_id',$user->boss_id);
                })
                ->orderBy('info','ASC')
                ->get();
        }
        //Define how many items we want to be visible in each page
        //$perPage = 5;
        //Slice the collection to get the items to display in current page
        //$currentPageSearchResults = $collection->slice($currentPage * $perPage, $perPage)->all();
        //Create our paginator and pass it to the view
        $perPage = (count($collection) > 0)?count($collection):1;
        $paginatedSearchResults= new LengthAwarePaginator($collection, count($collection) * (count($users)), $perPage, $currentPage);
        $paginatedSearchResults->setPath('incidents');
        $paginatedSearchResults->appends('/super/approve');

        $data = [
            'clock'  => $userclock,
            'incidents' => $paginatedSearchResults,
            'benefits' => Benefits::get(),
            'user' => (isset($user->boss_id))?User::find($user->boss_id):false,
            'total' => count($users),
            'views' => count($views),
        ];
        return($data);
    }
    private function toSuper()
    {
        //Get current page form url e.g. &page=6
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        $views = session('incidents');
        $views[$currentPage] = true;
        session(['incidents' => $views]);

        $user = Incident::select('from_id')
            ->where(function($query){
                $query->whereHas('benefit',function($query2){
                    $query2->where('group','incident');
                    $query2->orWhere('group','incapacity');
                })
                ->orWhere('event_id','0');
            })
            ->distinct()
            ->where('user_id',Auth::user()->id)
            ->where('status','pending')
            ->offset($currentPage-1)
            ->limit(1)
            ->first();
        $users = [];
        $collection = [];
        $userclock = false;
        if($user) {
            $userclock = UserClock::where('user_id',$user->from_id)->orderBy('created_at','desc')->limit(1)->get();
            $users = Incident::select('from_id')
                ->where(function($query){
                    $query->whereHas('benefit',function($query2){
                        $query2->where('group','incident');
                        $query2->orWhere('group','incapacity');
                    })
                    ->orWhere('event_id','0');
                })
                ->distinct()
                ->where('user_id',Auth::user()->id)
                ->where('status','pending')
                ->get();
            //Create a new Laravel collection from the array data
            $collection = Incident::where(function($query){
                    $query->whereHas('benefit',function($query2){
                        $query2->where('group','incident');
                        $query2->orWhere('group','incapacity');
                    })
                    ->orWhere('event_id','0');
                })
                ->where('user_id',Auth::user()->id)
                ->where('status','pending')
                ->where('from_id',$user->from_id)
                ->orderBy('info','ASC')
                ->get();
        }

        //Define how many items we want to be visible in each page
        //$perPage = 5;
        //Slice the collection to get the items to display in current page
        //$currentPageSearchResults = $collection->slice($currentPage * $perPage, $perPage)->all();
        //Create our paginator and pass it to the view

        $perPage = (count($collection) > 0)?count($collection):1;
        $paginatedSearchResults= new LengthAwarePaginator($collection, count($collection) * (count($users)), $perPage, $currentPage);
        $paginatedSearchResults->setPath('incidents');
        $paginatedSearchResults->appends('/super/approve');

        $data = [
            'clock'  => $userclock,
            'incidents' => $paginatedSearchResults,
            'benefits' => Benefits::get(),
            'user' => (isset($user->from_id))?User::find($user->from_id):false,
            'total' => count($users),
            'views' => count($views),

        ];
        return($data);
    }

    public function requests(Request $request)
    {
        $time   = date("Y-m-d H:i:s");
        $data   = $request->all();
        $user   = Auth::user();
        $g      = substr($user->grade,1);
        $acept  = [];
        $users  = [];
        $reject = [];
        $direct = [];
        $get    = [];
        $log    = [];
        if(!isset($data['b'])) return redirect('/super/approve/requests');
        foreach($data['b'] as $id => $value) {
            if($value['v'] == -1) continue;
            if($value['v'] == 0) {
                $reject[] = $id;
            } else if($value['v'] == 1) {
                $acept[] = $id;
            } else {
                $direct[] = $id;
            }
            $get[] = $id;
        }
        $incidents = Incident::whereIn('id',$get)->get();
        foreach($incidents as $i) {
            $users[] = $i->from_id;
        }

        $incidents = Incident::whereIn('id',$direct)->get();
        foreach($incidents as $r) {
            $r->value = 1;
            $r->user_id = $user->generalist;
            $r->event->status = 'accepted';
            $r->status = 'complete';
            $r->event->save();
            $r->save();
            $log[] = ['ip'=>$request->ip,'response'=>$r->status,'request_id'=>$r->id,'user_id'=>$user->id,'created_at'=>$time,'updated_at'=>$time];
        }
        
        $incidents = Incident::whereIn('id',$acept)->get();
        foreach($incidents as $r) {
            // $boss = $r->from->getNextAuthorizator($r->user->id);
            $boss = false;
            if ($boss) {
                $r->user_id = $boss->id;
                try {
                    DB::beginTransaction();
                    $r->event->save();
                    $r->save();   
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                }
            }else{
                $r->value = 1;
                $r->event->status = 'accepted';
                $r->status = 'complete';
                try {
                    DB::beginTransaction();
                    $r->event->save();
                    $r->save();   
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                }
            }
        }

        $incidents = Incident::whereIn('id',$reject)->get();
        foreach($incidents as $r) {
            \DB::beginTransaction();
            try {
                $r->event->status = 'canceled';
                $r->event->save();
            } catch (\Throwable $th) {
                DB::rollback();
            }
            try {
                $r->status = 'rejected';
                $r->save();
            } catch (\Throwable $th) {
                DB::rollback();
            }
            try {
                $days = $r->event->getDays();
                $balance = EmployeeController::getBalanceDetail($r->event->benefit_id,$r->event->user_id);
                if($balance['benefit']->type=="pending") {
                    $js = PleaController::restoreDays($r->event);
                } else {
                    $balance['rows'][0]->pending -= $days;
                    unset($balance['rows'][0]->diff);
                    unset($balance['rows'][0]->solicited);
                    $balance['rows'][0]->save();
                }
            } catch (\Throwable $th) {
                DB::rollback();
            }
            DB::commit();
            $log[] = ['ip'=>$request->ip,'response'=>$r->status,'request_id'=>$r->id,'user_id'=>$user->id,'created_at'=>$time,'updated_at'=>$time];
            //Mail::to($r->event->user->email)->send(new PleaRejectedEmployee($r->event));
            //Mail::to("user@example.com")->send(new PleaRejectedEmployee($r->event));
        }

        flash('Las peticiones han sido guardadas.','success');
        return redirect('/super/approve/requests');
    }

    public function update(Request $request, $id) {
        if($request->ajax()){
            $i = Incident::find($id);
            if(Privilege::heritage(Auth::user(),$i->from_id)) Incident::where('id',$id)->update($request->all());
        };
    }

    public function incidents(Request $request)
    {
        $time = date("Y-m-d H:i:s");
        $user = Auth::user();
        $boss = $user->getMyCurrentBossId();
        $g    = substr($user->grade,1);

        $incidents = Incident::where('user_id',$user->id)
                        ->where('status','pending')
                        ->get();
        $log = [];
        foreach($incidents as $r) {
            if($r->value == -1) continue;
            $log[] = ['ip'=>$request->ip,'response'=>($r->value)?'accepted':'rejected','incident_id'=>$r->id,'user_id'=>$user->id,'created_at'=>$time,'updated_at'=>$time];
            $balance = Balances::where([['user_id',$r->from_id],['benefit_id',$r->benefit_id]])->first();
            if (!$balance) {
                $balance = new Balances(['year'=>date('Y'),'user_id'=>$r->from_id,'benefit_id'=>$r->benefit_id]);
            }
            if(config('config.gradematters')) {
                if(($r->value == 1 && $g > 120) || ($r->value == 1 && $g < 120 && $r->from->boss_id == $user->id)) {
                    if($balance->benefit->type=="pending") {
                        $balance->amount += $r->amount;
                    } else {
                        $balance->pending += $r->amount;
                    }
                    $balance->save();
                } else if($r->value == 0 && $g < 120) {
                    if($balance->benefit->type=="pending") {
                        $balance->amount -= $r->amount;
                    } else {
                        $balance->pending -= $r->amount;
                    }
                    $balance->save();
                }
            } else {
                if($balance->benefit->type=="pending") {
                    $balance->amount += $r->amount;
                } else {
                    $balance->pending += $r->amount;
                }
                $balance->save();
            }

        }
        Responselog::insert($log);

        $incidents = Incident::where('user_id',$user->id)
                        ->where('status','pending')
                        ->where('value','0')
                        ->update(['status'=>'rejected']);
        $data = ['user_id'=>$boss];
        if(config('config.gradematters')) {
            if($g < 120) {
                if($g == 000 && (is_null($user->generalist)) || empty($user->generalist) || $user->generalist == ''){
                    $data = ['user_id'=>$user->id,'status'=>'complete'];
                }else{
                    $data = ['user_id'=>$user->generalist,'status'=>'complete'];
                }
            }
        } else {
            $data = ['user_id'=>$user->generalist,'status'=>'complete'];
        }
        $incidents = Incident::where('user_id',$user->id)
                        ->where('status','pending')
                        ->where('value','1')
                        ->update($data);

        flash('Todos las peticiones han sido guardadas.','success');
        return redirect('/super/approve/incidents');
    }
}
