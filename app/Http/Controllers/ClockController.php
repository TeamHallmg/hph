<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

use App\Http\Requests;

use File;
use Storage;

use App\User;
use App\Models\Event;
use App\Models\Balances;
use App\Models\Incident;
use App\Models\Ingresos;
use App\Models\CronError;
use App\Models\UserClock;
//use App\Models\Request as PleaRequest;

class ClockController extends Controller
{
    private $horarios,$rules,$month,$dia,$week,$yesterday,$y,$log,$incidents,$ai,$globals;

    public function __construct()
    {
        //$this->globals = config('config.globals');
        $this->incidents = config('config.incidents');
        $this->horarios = [];
        $this->ai = [];
        $this->y = date('D',$this->yesterday);
        $this->setDay();
    }

    public function import()
    {
        $data['filetype'] = '.txt';
        $data['url'] = 'admin/clock/process';
        $data['holder'] = 'TXT';
        return view('systems.import',$data);
    }

    public function manualprocess(Request $request)
    {
        if (!$request->hasFile('file')) {
            flash('Debe seleccionar un archivo.','warning');
            return redirect('/clock/import');
        }
        $ext = $request->file->getClientOriginalExtension();
        if(!($ext == 'txt')){
            flash('Debe seleccionar un archivo de tipo txt.','warning');
            return redirect('/clock/import');
        }
        $contents = File::get($request->file->path());
        $contents = explode("\n",$contents);
        array_pop($contents);

        $data  = [];
        $dates = [];
        $today = date('Y-m-d');

        foreach($contents as $line) {
            $date = date('Y-m-d',strtotime(str_replace('/','-',substr($line,41,16))));
            $data[] = [
                'person_number'=>str_pad(trim(substr($line,0,16)), 6, '0', STR_PAD_LEFT),
                'timestamp'=>date('Y-m-d H:i:s',strtotime(str_replace('/','-',substr($line,41,16).':00')))
            ];
            if(!isset($dates[$date])) $dates[$date] = ['since'=>$date,'today'=>$today];
        }
        //dd($data,$dates);die;
        Ingresos::insert($data);
        CronError::insert($dates);
        $this->process();
        return redirect('admin/clock/import');
    }

    public function autoprocess()
    {
        $contents = Storage::disk('local')->get('Import/check.txt');
        $contents =  explode("\n",$contents);
        array_pop($contents);

        $data  = [];
        $dates = [];
        $today = date('Y-m-d');

        foreach($contents as $line) {
            $date = date('Y-m-d',strtotime(str_replace('/','-',substr($line,41,16))));
            $data[] = [
                'person_number'=>str_pad(trim(substr($line,0,16)), 6, '0', STR_PAD_LEFT),
                'timestamp'=>date('Y-m-d H:i:s',strtotime(str_replace('/','-',substr($line,41,16).':00')))
            ];
            if(!isset($dates[$date])) $dates[$date] = ['since'=>$date,'today'=>$today];
        }
        //dd($data,$dates);die;
        Ingresos::insert($data);
        CronError::insert($dates);
        unlink(storage_path('app/Import/check.txt'));
    }


    public function clock($view = null)
    {
        $userclock = UserClock::where([['user_id',Auth::user()->id]])->orderBy('created_at','desc')->paginate(1);
        return view('common.clock',['clock' => $userclock, 'icon'=>$view]);
    }

    public function process()
    {
        $this->log = new Logger('ClockController');
        if(file_exists(storage_path('logs/custom/'.$this->y.'.log'))) unlink(storage_path('logs/custom/'.$this->y.'.log'));
        $this->log->pushHandler(new StreamHandler(storage_path('logs/custom/'.$this->y.'.log'), Logger::INFO));
        $this->log->addInfo($this->month.' '.$this->yesterday);

        $past = new CronError();
        $past->since = date('Y-m-d');
        $past->today = date('Y-m-d');
        $past->save();

        $past = CronError::get();
        //dd($past);die;
        foreach($past as $day) {
            $this->setDay($day->since);
            $since = strtotime($day->since);
            $flag = ClockController::checkDay($since);
            if($flag) {
                $this->processDay($since);
                $day->delete();
            } else {
                $day->today = date('Y-m-d');
                $diff = (strtotime($day->today) - strtotime($day->since)) / (24*60*60);
                if($diff > 5){
                    $day->delete();
                } else {
                    $day->save();
                }
            }
            if(!isset($oldest)) $oldest = $since;
            $oldest = ($oldest > $since)? $since : $oldest;
        }
        Ingresos::whereDate('timestamp','<',date('Y-m-d',$oldest))->delete();
        $past = CronError::get();
        if(empty($past)) Ingresos::truncate(); //Me da miedo que exceda los dos millones ... me imagino en algún punto correra bien ...
    }

    protected function setDay($day = 'yesterday')
    {
        $this->yesterday = strtotime($day);
        setlocale(LC_TIME, 'es_MX.UTF-8');
        $this->dia   = strftime("%A %d",$this->yesterday);
        $this->month = strftime("%B %G",$this->yesterday);
        $this->week = date('W',$this->yesterday);
    }

    protected function processDay($day)
    {
        $users = User::select(['id','boss_id','number','region_id','clock','txt'])->where([['status','active'],['clock','1']])->get();
        //dd($users);die;
        //Procesar los usuarios
        $userclocks = [];
        $inda = 'Día:' . date('Y-m-d',$this->yesterday);
        $this->y = date('D', $day);

        foreach($users as $user) {

            if($user->clock == 0) {
                $this->log->addInfo("Skip $user->number, Flag: Clock:$user->clock");
                continue;
            }

            $region = $user->getMyRegion();
            if(!isset($this->horarios[$region])) {
                $this->horarios[$region] = $user->getMySchedule();
                $this->rules[$region] = $user->getScheduleRules();
            }

            $clock = ClockController::getDayClock($day, $user);

            //If they had registers
            if(!empty($clock[$user->number])) {

                $checks  =  $clock[$user->number];

                if(count($checks) < 2) {
                    $this->log->addInfo("Registro incompleto: ".$user->number.' '.$region);
                    $exe = $this->in($checks[0],$region);
                    $exs = $this->out($checks[0],$region);
                    $t = date('H:i:s',$checks[0]);

                    if($exe > $exs) {
                        $this->log->addInfo("RI:Solo salida:".$user->number."<br />");
                        $info = $inda.'<br /><span class="text-warning">Sólo Salida Tiempo:'.$t.'</span>';
                        $tr = '"'.$checks[0].'":["warning","-","'.$t.'"]';
                        //$tr = "<tr class=\"warning\"><td>$this->dia</td><td>-</td><td>$t</td></tr>";
                    } else {
                        $this->log->addInfo("RI:Solo entrada:".$user->number);
                        $info = $inda.'<br /><span class="text-warning">Sólo Entrada Tiempo:'.$t.'</span>';
                        $tr = '"'.$checks[0].'":["warning","'.$t.'","-"]';
                        //$tr = "<tr class=\"warning\"><td>$this->dia</td><td>$t</td><td>-</td></tr>";
                    }

                    $bid = $this->incidents['irregulares'];
                    $this->addIncident($user, $info, $bid);

                } else {

                    $this->log->addInfo("Entrada y salida, completo ".$user->number.' '.$region);
                    $entrada = $checks[0];
                    $salida  = $checks[1];

                    $e = date('H:i:s',$entrada);
                    $s = date('H:i:s',$salida);
                    $exe = $this->in($entrada,$region);
                    $exs = $this->out($salida,$region);

                    $class = 'normal';
                    if($flag = $this->horarios[$region][$this->y][2]) {
                        //dd($this->yesterday);die;
                        /*if($user->id == 132 && $this->yesterday == '1494979200') {
                            dd($e,$this->horarios[$region][$this->y][0],$exe,($this->rules[$region]['overtime']['time']*60*-1),
                               $s,$this->horarios[$region][$this->y][1],$exs,($this->rules[$region]['overtime']['time']*60*-1),
                               $this->rules[$region]['overtime']['before'],$this->rules[$region]['overtime']['after'],
                               "$exe < ({$this->rules[$region]['overtime']['time']}*60*-1)",
                               $exe < ($this->rules[$region]['overtime']['time']*60*-1),
                               "$exs < ({$this->rules[$region]['overtime']['time']}*60*-1)",
                               $exs < ($this->rules[$region]['overtime']['time']*60*-1)
                           );
                           die;
                       }*/
                        //
                        $mintime = $this->rules[$region]['overtime']['time']*$this->rules[$region]['overtime']['frecuency'];
                        if($exe < ($mintime*-1) && $this->rules[$region]['overtime']['before']) {
                            $this->log->addInfo("Entro antes:".$user->number);
                            //Se convierte a fragmentos
                            $int = floor(($exe*-1)/$mintime);
                            //Se convierte a segundos
                            $exe = $int*$mintime;
                            $exe = $exe/60; //A minutos
                            $class = "success";
                            $bid = $this->extortxt($user);
                            $info = $inda . '<br /><span class="text-'.$class.'">Entrada:'.$e.'<br />Tiempo:'.$exe.' min</span>';
                            //Retardos en minutos?
                            if($exe > 0) $this->addIncident($user, $info, $bid, $exe);
                        } else if($exe > ($this->rules[$region]['delay']['time']*$this->rules[$region]['overtime']['frecuency']) && $this->rules[$region]['delay']['before']) {
                            $this->log->addInfo("Entro despues:".$user->number);
                            $exe = floor($exe/$this->rules[$region]['delay']['frecuency']);
                            $class = "warning";
                            $bid = $this->incidents['retardos'];
                            $info = $inda . '<br /><span class="text-'.$class.'">Entrada:'.$e.'<br />Tiempo:'.$exe.' min</span>';
                            //Retardos en minutos?
                            if($exe > 0) $this->addIncident($user, $info, $bid, $exe);
                        }

                        if($exs < ($mintime*-1) && $this->rules[$region]['overtime']['after']) {
                            $this->log->addInfo("Salio despues:".$user->number);
                            //Se convierte a fragmentos
                            $int = floor($exs*-1/$mintime);
                            $exs = $int*$mintime;
                            //Se convierte a segundos
                            //$int = floor($exs/$this->rules[$region]['overtime']['time']);
                            //$exs = floor($exs/$this->rules[$region]['overtime']['frecuency'])*-1;
                            //$exs = floor($exs/60)*-1;
                            //dd($int,$exe,$exs,$mintime,$user->number,$inda,$this->rules[$region]);die;
                            $class = "success";
                            //$info = $inda . '<br /><span class="text-'.$class.'">Salio déspues:'.$s.' +'.$exs.'</span>';
                            $bid = $this->extortxt($user);
                            $info = $inda . '<br /><span class="text-'.$class.'">Salida:'.$s.'<br />Tiempo:'.$exs.' min</span>';
                            if($exs > 0) $this->addIncident($user, $info, $bid, $exs);
                        } else if($exs > ($this->rules[$region]['delay']['time']*60) && $this->rules[$region]['delay']['after']) {
                            $this->log->addInfo("Salio antes:".$user->number);
                            $exs = floor($exs/$this->rules[$region]['overtime']['frecuency']);
                            $class = "warning";
                            $bid = $this->incidents['retardos'];
                            $info = $inda . '<br /><span class="text-'.$class.'">Salida:'.$s.'<br />Tiempo:'.$exs.' min</span>';
                            if($exs > 0) $this->addIncident($user, $info, $bid, $exs);
                        }
                    } else {
                        $this->log->addInfo("Día laborado (Extra):".$user->number);
                        $exs = ($salida - $entrada) / 60;
                        $class = "success";
                        $info = $inda . '<br /><span class="text-'.$class.'">Día Laborado: +'.$exs.'</span>';
                        $bid = $this->extortxt($user);
                        $this->addIncident($user, $info, $bid, $exs);
                    }
                    $tr = '"'.$checks[0].'":["'.$class.'","'.$e.'","'.$s.'"]';
                    //$tr = "<tr class=\"$class\"><td>$this->dia</td><td>$e</td><td>$s</td></tr>";
                }

            } else {
                $class = 'danger';
                $b = $this->incidents['faltas'];
                //Revisar eventos Globales primero
                if($this->horarios[$region][$this->y][2]) {
                    $event = Event::where([['type','global'],['start','<=',$this->yesterday],['end','>=',$this->yesterday]])
                                ->where(function($query) use ($user){
                                    $query->where('js','like','%"' . $user->id . '"%')
                                          ->orWhere([['region_id',$user->region_id],['js',NULL]]);
                                })
                                ->first();
                    if($event) {
                        $this->log->addInfo("Tiene evento: $user->number E:$event->id");
                        $class = 'normal';
                        $b = $event->benefit_id;
                        $balance = Balances::where([['user_id',$user->id],['benefit_id',$b]])->first();
                        if (!$balance) {
                            $balance = new Balances(['year'=>date('Y'),'user_id'=>$user->id,'benefit_id'=>$b]);
                        }
                        $balance->pending += 1;
                        $balance->save();

                    } else {
                        $event = Event::where([['user_id',$user->id],['start','<=',$this->yesterday],['end','>=',$this->yesterday]])->first();
                        if($event) {
                            $this->log->addInfo("Tiene solicitud: $user->number E:$event->id");
                            if($event->status == 'accepted' || $event->status == 'processed') {
                                $class = 'normal';
                                $b = $event->benefit_id; //Esto cambiara???? No quiero reprtar... amount = 0;
                            } else {
                                $this->log->addInfo("La solicitud fue rechazada: $user->number E:$event->id");
                                $this->addIncident($user, $inda, $b);
                            }
                            //Si cambia aca el add
                        } else {
                            $this->log->addInfo("Falta: $user->number");
                            $this->addIncident($user, $inda, $b);
                        }
                    }
                }
                $tr = '"'.$day.'":["'.$class.'","-","-"]';
                //$tr = "<tr class=\"$class\"><td>$this->dia</td><td>-</td><td>-</td></tr>";
            }
            $userclocks[$user->id] = $tr;
        }
        $this->saveUserClocks($userclocks);
        $this->saveIncidents();
    }

    public function saveUserClocks($userclocks)
    {
        if(empty($userclocks)) return;

        $date = date("Y-m-d H:i:s");
        $values = '';
        foreach($userclocks as $k => $v) {
            $values.= "('$this->month','\{$v\}','$k','$date'), ";
        }
        $values  = rtrim($values, ", ");
        $sql = "INSERT INTO userclock (month,clocktable,user_id,created_at) VALUES $values
                ON DUPLICATE KEY UPDATE
                    clocktable=CONCAT(SUBSTRING_INDEX(clocktable,'}',1),',',SUBSTRING(VALUES(clocktable),2)),
                    updated_at=VALUES(created_at)";
        //echo $sql; die;
        \DB::insert(\DB::raw($sql));
    }

    private function addIncident($user, $info, $bid, $amount = 1)
    {
        //Empty Incident
        $incident = [
            'user_id'   =>$user->getMyCurrentBossId(),
            'from_id'   =>$user->id,
            'week'      =>$this->week,
            'time'      =>$this->yesterday,
            'comment'   => '',
            'value'     => 0,
            'info'      => $info,
            'amount'    => $amount,
            'benefit_id'=> $bid,
        ];
        $this->ai[] = $incident;
    }

    private function saveIncidents()
    {
        Incident::insert($this->ai);
        $this->ai = [];
    }

    private function in($entrada,$region)
    {
        $exe = 0;
        $day = date('D', $entrada);
        $ei = strtotime(date('m/d/Y',$entrada).' '.$this->horarios[$region][$day][0]);
        $exe = ($entrada - $ei);
        return($exe);
    }

    private function out($salida,$region)
    {
        if($this->rules[$region]['nightshift']) {
            $salida -= 24*60*60;
        }
        $exs = 0;
        $day = date('D', $salida);
        $si = strtotime(date('m/d/Y',$salida).' '.$this->horarios[$region][$day][1]);
        $exs = ($si - $salida);
        return($exs);
    }

    private function extortxt($user)
    {
        $i = ($user->txt)?$this->incidents['txt']:$this->incidents['extras'];
        return($i);
    }

    public static function checkDay($day)
    {
        $regs = false;
        if(config('config.nightshift')) {
            $tdb  = time() - (2*24*60*60);
            if($day < $tdb) {
                $day  = $day + (12*60*60); //Arbitrary noon
                $oda  = $day + (24*60*60);
                $regs = Ingresos::whereDate('timestamp','>',date('Y-m-d',$day))->first();
                if($regs) $regs = Ingresos::whereDate('timestamp','<',date('Y-m-d',$oda))->first();
            }
        } else {
            $regs = Ingresos::whereDate('timestamp',date('Y-m-d',$day))->first();
        }
        if($regs) return(true);
        return(false);
    }

    public static function getDayClock($day, $user)
    {
        //conexion de algo y respuesta de algo
        $regs = Ingresos::where('person_number',$user->number);
        if($user->getScheduleRules()['nightshift']) {
            $day = $day + (12*60*60); // Again noon
            $oda = $day + (24*60*60);
            $regs->where('timestamp','>',date('Y-m-d H:m:s',$day))
                 ->where('timestamp','<',date('Y-m-d H:m:s',$oda));
        } else {
            $regs->whereDate('timestamp',date('Y-m-d',$day));
        }
        $regs = $regs->get();
        $process = [];
        foreach($regs as $reg) {
            $reg->timestamp = strtotime($reg->timestamp);
            if(!isset($process[$reg->person_number][0])) $process[$reg->person_number][0] = $reg->timestamp;
            if(!isset($process[$reg->person_number][1])) $process[$reg->person_number][1] = $reg->timestamp;
            if($process[$reg->person_number][0] > $reg->timestamp) $process[$reg->person_number][0] = $reg->timestamp;
            if($process[$reg->person_number][1] < $reg->timestamp) $process[$reg->person_number][1] = $reg->timestamp;
        }
        foreach($process as $k => $v) {
            if($v[0] == $v[1]) unset($process[$k][1]);
        }
        return($process);
    }
}
