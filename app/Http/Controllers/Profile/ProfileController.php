<?php

namespace App\Http\Controllers\Profile;

use PDF;
use Mail;
use Exception;
use App\User;
use App\Employee;
use App\Models\GeoMunicipio;
use App\Models\GeoDepartamento;

use Illuminate\Http\Request;

use App\Models\Profile\Profile;
use App\Models\Vacantes\Vacante;
use Illuminate\Support\Facades\DB;
use App\Models\Vacantes\Postulante;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use App\Models\Profile\ProfileHealth;
use App\Models\Profile\ProfileLanguage;
use App\Models\Profile\ProfileRegistry;
use Illuminate\Support\Facades\Storage;
use App\Models\Profile\ProfileHealthDet;
use App\Models\Profile\ProfileKnowledge;
use App\Models\Profile\ProfileReference;
use App\Models\Profile\ProfileAdditional;
use App\Models\Profile\ProfileExperience;
use App\Models\Profile\ProfileApplication;
use App\Models\Profile\ProfileScholarships;
use App\Models\Profile\ProfileBeneficiaries;
use App\Models\Profile\ProfileBeneficiariesDet;
use App\Models\Profile\ProfileUnionized;
use App\Models\Profile\ProfileBienes;
use App\Models\Profile\ProfileContrato;
use App\Models\Bienes\VariableValor;

class ProfileController extends Controller
{

    protected $path;

    public function __construct()
    {
        $this->middleware('auth');

        $this->path = getcwd() . '/uploads/profile/';
    }


    public function listsprofile()
    {

        $usuarios = User::select('employees.id','employees.nombre','employees.paterno','employees.materno')
        ->join('employees','employees.id','=','users.employee_id')
        ->get();


        return response()->json($usuarios);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        $estados = GeoDepartamento::pluck('nombre', 'id');
        $profile = Profile::where('user_id', $id)->first();
        //$profile = Profile::find($id);
        if (is_null($profile)) {
            $municipios = null;
            $crear = true;
            $user = User::find($id);
            $profileRegistros = null;
            $profileSalud = null;
            $profileBeneficiarios = null;
        } else {
            if (!is_null($profile->perfilAdicional)) {
                $municipios = GeoMunicipio::where('id_geo_departamento', $profile->perfilAdicional->state)->pluck('nombre', 'id');
            } else {
                $municipios = null;
            }
            $profileRegistros = $profile->perfilRegistro;
            $profileSalud = $profile->perfilSalud;
            $profileBeneficiarios = $profile->perfilBeneficiarios;

            // $fileExists = File::exists('profile/'.$profile->image);
            $fileExists = file_exists($this->path.$profile->image);

            $crear = false;
            $user = User::find($id);
        }

        // dd($profile->perfilSolicitud);

        return view('profile.admin', compact(
            'estados',
            'profile',
            'crear',
            'municipios',
            'user',
            'fileExists',
            'profileRegistros',
            'profileSalud',
            'profileBeneficiarios'
        ));
    }

    public function externo($id, $idVi) {
        // lo enviamos a crear un perfil nuevo
        $user = null;
        $estados = GeoDepartamento::pluck('nombre', 'id');

        return View('profile.externo', compact('idVi', 'user', 'estados'));
    }

    public function showExternal($id){
        $profile = Profile::where('id',$id)->first();

        return view('profile.showExterno', compact('profile'));
    }
    
    public function curriculum($idVi = null)
    {
        $id = Auth::id();
        $estados = GeoDepartamento::pluck('nombre', 'id');
        $profile = Profile::where('user_id', $id)->first();
        //$profile = Profile::find($id);
        if (is_null($profile)) {
            $municipios = null;
            $crear = true;
            $user = User::find($id);
            $profileRegistros = null;
            $profileSalud = null;
            $profileBeneficiarios = null;
        } else {
            if (!is_null($profile->perfilAdicional)) {
                $municipios = GeoMunicipio::where('id_geo_departamento', $profile->perfilAdicional->state)->pluck('nombre', 'id');
            } else {
                $municipios = null;
            }
            $profileRegistros = $profile->perfilRegistro;
            $profileSalud = $profile->perfilSalud;
            $profileBeneficiarios = $profile->perfilBeneficiarios;

            // $fileExists = File::exists('profile/'.$profile->image);
            $fileExists = file_exists($this->path.$profile->image);

            $crear = false;
            $user = User::find($id);
        }


        $Listausuarios = User::select('employees.id','employees.nombre','employees.paterno','employees.materno')
        ->join('employees','employees.id','=','users.employee_id')
        ->get();


        // dd($profile->perfilSolicitud);

        return view('profile.admin-curriculum', compact(
            'estados',
            'profile',
            'Listausuarios',
            'crear',
            'municipios',
            'user',
            'idVi',
            'fileExists',
            'profileRegistros',
            'profileSalud',
            'profileBeneficiarios'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $estados = GeoDepartamento::pluck('nombre', 'id');
        $profile = Profile::where('user_id', $id)->first();
        $ProfileContrato = ProfileContrato::where('profile_id', $profile->id)->first();
        


        $LegalContrato = User::select('enterprise_legal.*')
        ->join('employees','employees.id','=','users.employee_id')
        ->join('enterprises','enterprises.id','=','employees.enterprise_id')
        ->join('enterprise_legal','enterprise_legal.id_empresa','=','enterprises.id')
        ->where('users.id',$id)
        ->first();




        $GenerarContrato = false;
        $GenerarContratoLegal = false;
        //$profile = Profile::find($id);
        $user_id = $id;

        if (is_null($profile)) {
            $municipios = null;
            $crear = true;
            $user = User::find($id);
            $profileRegistros = null;
            $profileSalud = null;
            $profileBeneficiarios = null;
        } else {
            if (!is_null($profile->perfilAdicional)) {
                $municipios = GeoMunicipio::where('id_geo_departamento', $profile->perfilAdicional->state)->pluck('nombre', 'id');
            } else {
                $municipios = null;
            }
            $profileRegistros = $profile->perfilRegistro;
            $profileSalud = $profile->perfilSalud;
            $profileBeneficiarios = $profile->perfilBeneficiarios;
            $perfilSolicitud = $profile->perfilSolicitud;

            // $fileExists = File::exists('profile/'.$profile->image);
            $fileExists = file_exists($this->path.$profile->image);

            $crear = false;
            $user = null;
        }
        


        if(isset($ProfileContrato)){
            $GenerarContrato = true;
        }

        if(isset($LegalContrato)){
            $GenerarContratoLegal = true;
        }


        $Listausuarios = User::select('employees.id','employees.nombre','employees.paterno','employees.materno')
        ->join('employees','employees.id','=','users.employee_id')
        ->get();

        return view('profile.admin', compact(
            'estados',
            'Listausuarios',
            'GenerarContratoLegal',
            'GenerarContrato',
            'profile',
            'crear',
            'municipios',
            'user',
            'user_id',
            'fileExists',
            'profileRegistros',
            'profileSalud',
            'profileBeneficiarios'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $data = $request->all();

        if (isset( $data['availability_travel'] ))
        {
           $data['availability_travel'] = 1;
        }else{
            $data['availability_travel'] = 0;
        }

        if (isset( $data['availability_shifts'] ))
        {
           $data['availability_shifts'] = 1;
        }else{
            $data['availability_shifts'] = 0;
        }

        if (isset( $data['availability_residence'] ))
        {
           $data['availability_residence'] = 1;
        }else{
            $data['availability_residence'] = 0;
        }

        if (isset( $data['family_working'] ))
        {
           $data['family_working'] = 1;
        }else{
            $data['family_working'] = 0;
        }


        // dd($data);
        try {
            DB::beginTransaction();
                $updateProfile = $this->updatePerfil($data, $id);
            DB::commit();
        }
        // Ha ocurrido un error, devolvemos la BD a su estado previo y hacemos lo que queramos con esa excepción
        catch (\Throwable $e) {
            // dd($e->getMessage());
            DB::rollback();
            /* echo 'ERROR (' . $e->getCode() . ') - - - - > ' . $e->getMessage();
            die(); */
            return redirect()->back()->with('alert-danger', '
            ¡UPS!... NO SE ACTUALIZÓ EL PERFIL <br/>
            VERIFIQUE LA INFORMACIÓN INGRESADA, DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. <br/>
            ' . $e->getMessage());
        }
        return redirect()->back()->with('alert-success', 'Los datos se han actualizado correctamente!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createBasica (Request $request) {
        $data = $request->all();

        try {
            DB::beginTransaction();
                $createProfile = $this->createPerfilBasica($data, $request);
            DB::commit();
        }
        // Ha ocurrido un error, devolvemos la BD a su estado previo y hacemos lo que queramos con esa excepción
        catch (\Throwable $e) {
            DB::rollback();
            /* echo 'ERROR (' . $e->getCode() . ') - - - - > ' . $e->getMessage();
            die(); */
            return redirect()->back()->with('alert-danger', '
            ¡UPS!... NO SE HA GUARDADO EL PERFIL <br/>
            VERIFIQUE LA INFORMACIÓN INGRESADA, DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. <br/>
            ' . $e->getMessage());
        }
        
        return redirect()->back()->with('alert-success', 'Los datos se han ingresado correctamente!!!');
    }

    public function createComplementaria (Request $request) {
        $data = $request->all();

        // dd($data);
        if (isset($data['external'])) {
            $data['external'] = true;
        }

        try {
            DB::beginTransaction();
                $createComplementaryProfile = $this->createPerfilComplementaria($data, $request);
            DB::commit();

            if (isset($data['external'])) {
                if(!is_null($createComplementaryProfile)) {
                    try {
                        DB::beginTransaction();
                            $profile = Profile::find($createComplementaryProfile->id);
                            if(User::where(['email'=> $profile->email])->exists()) {
                                throw new Exception('el correo ya existe en la base de datos!');
                            }
                            $password = Hash::make($profile->surname_father.'123$');
                            $contrasena = $profile->surname_father.'123$';
                            $usuario = User::create([
                                'first_name'=> $profile->name,
                                'last_name'=> $profile->surname_father. ' ' .$profile->surname_mother,
                                'email'=> $profile->email,
                                'password'=> $password,
                                'external'=>true,
                            ]);

                            $this->enviaCorreo($profile->email, $contrasena);

                            $profile->user_id = $usuario->id;
                            $profile->save();

                            $postulado = Postulante::create([
                                'user_id' => $usuario->id,
                                'vacante_id' => $data['idVi'],
                                'profile_id' => $createComplementaryProfile->id,
                                'intentos_postulacion' => 'SI',
                            ]);
                        DB::commit();
                    }
                    catch (\Throwable $e) {
                        DB::rollback();
                        /* echo 'ERROR (' . $e->getCode() . ') - - - - > ' . $e->getMessage();
                        die(); */
                        return redirect()->back()->with('alert-danger', '
                            ¡UPS!... NO SE POSTULÓ, ALGO SALIÓ MAL.
                            VERIFIQUE QUE LA INFORMACIÓN INGRESADA SEA CORRECTA,
                            DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE.'. $e->getMessage());
                    }
                }
            }
        }
        // Ha ocurrido un error, devolvemos la BD a su estado previo y hacemos lo que queramos con esa excepción
        catch (\Throwable $e) {
            // dd($e->getMessage());
            DB::rollback();
            /* echo 'ERROR (' . $e->getCode() . ') - - - - > ' . $e->getMessage();
            die(); */
            return redirect()->back()->with('alert-danger', '
            ¡UPS!... NO SE HA GUARDADO EL PERFIL <br/>
            VERIFIQUE LA INFORMACIÓN INGRESADA, DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. <br/>
            ' . $e->getMessage());
        }
        

        if (isset($data['external'])) {
            return redirect('postulante/' . $data['idVi']);
        } elseif($data['idVi'] != null) {
            return redirect('encuesta/' . $data['idVi'])->with('alert-success', 'Se ha actualizado tu curriculum correctamente!!!');;
        } else {
            return redirect()->back()->with('alert-success', 'Los datos se han ingresado correctamente!!!');
        }
    }

    public function createRegistro (Request $request) {
        $data = $request->all();

        try {
            DB::beginTransaction();
                if (!is_null($data['user_id'])) {
                    $user = User::find($data['user_id']);
                    $createProfile = Profile::create([
                        'user_id' => $data['user_id'],
                        'name' => is_null($user->employee) ? $user->first_name : $user->employee->nombre,
                        'surname_father' => is_null($user->employee) ? $user->last_name : $user->employee->paterno,
                        'surname_mother' => is_null($user->employee) ? null : $user->employee->materno,
                        'cellphone' => is_null($user->employee) ? null : $user->employee->celular,
                        'phone' => is_null($user->employee) ? null : $user->employee->telefono,
                        'email' => is_null($user->employee) ? $user->email : $user->employee->correoempresa,
                        'date_birth' => is_null($user->employee) ? null : $user->employee->nacimiento,
                    ]);
                    $data['profile_id'] = $createProfile->id;
                }
                $createProfileRegistry = $this->createPerfilRegistro($data, $request);
            DB::commit();
        }
        // Ha ocurrido un error, devolvemos la BD a su estado previo y hacemos lo que queramos con esa excepción
        catch (\Throwable $e) {
            DB::rollback();
            /* echo 'ERROR (' . $e->getCode() . ') - - - - > ' . $e->getMessage();
            die(); */
            return redirect()->back()->with('alert-danger', '
            ¡UPS!... NO SE HA GUARDADO EL PERFIL <br/>
            VERIFIQUE LA INFORMACIÓN INGRESADA, DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. <br/>
            ' . $e->getMessage());
        }
        
        return redirect()->back()->with('alert-success', 'Los datos se han ingresado correctamente!!!');
    }

    public function updateRegistro(Request $request) {
        $data = $request->all();

        //dd($data);

        try {
            DB::beginTransaction();
                $updateProfile = $this->updatePerfilRegistro($data, $request);
            DB::commit();
        }
        // Ha ocurrido un error, devolvemos la BD a su estado previo y hacemos lo que queramos con esa excepción
        catch (\Throwable $e) {
            DB::rollback();
            /* echo 'ERROR (' . $e->getCode() . ') - - - - > ' . $e->getMessage();
            die(); */
            return redirect()->back()->with('alert-danger', '
            ¡UPS!... NO SE ACTUALIZÓ EL PERFIL <br/>
            VERIFIQUE LA INFORMACIÓN INGRESADA, DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. <br/>
            ' . $e->getMessage());
        }
        
        return redirect()->back()->with('alert-success', 'Los datos se han actualizado correctamente!!!');
    }

    public function createSalud (Request $request) {
        $data = $request->all();
        
        try {
            DB::beginTransaction();
                if (!is_null($data['user_id'])) {
                    $user = User::find($data['user_id']);
                    $createProfile = Profile::create([
                        'user_id' => $data['user_id'],
                        'name' => is_null($user->employee) ? $user->first_name : $user->employee->nombre,
                        'surname_father' => is_null($user->employee) ? $user->last_name : $user->employee->paterno,
                        'surname_mother' => is_null($user->employee) ? null : $user->employee->materno,
                        'cellphone' => is_null($user->employee) ? null : $user->employee->celular,
                        'phone' => is_null($user->employee) ? null : $user->employee->telefono,
                        'email' => is_null($user->employee) ? $user->email : $user->employee->correoempresa,
                        'date_birth' => is_null($user->employee) ? null : $user->employee->nacimiento,
                    ]);
                    $data['profile_id'] = $createProfile->id;
                }
                $createProfileHealth = $this->createPerfilSalud($data);
            DB::commit();
        }
        // Ha ocurrido un error, devolvemos la BD a su estado previo y hacemos lo que queramos con esa excepción
        catch (\Throwable $e) {
            DB::rollback();
            /* echo 'ERROR (' . $e->getCode() . ') - - - - > ' . $e->getMessage();
            die(); */
            return redirect()->back()->with('alert-danger', '
            ¡UPS!... NO SE HA GUARDADO EL PERFIL <br/>
            VERIFIQUE LA INFORMACIÓN INGRESADA, DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. <br/>
            ' . $e->getMessage());
        }
        
        return redirect()->back()->with('alert-success', 'Los datos se han ingresado correctamente!!!');
    }

    public function updateSalud(Request $request) {
        $data = $request->all();

        try {
            DB::beginTransaction();
                $updateProfile = $this->updatePerfilSalud($data);
            DB::commit();
        }
        // Ha ocurrido un error, devolvemos la BD a su estado previo y hacemos lo que queramos con esa excepción
        catch (\Throwable $e) {
            DB::rollback();
            /* echo 'ERROR (' . $e->getCode() . ') - - - - > ' . $e->getMessage();
            die(); */
            return redirect()->back()->with('alert-danger', '
            ¡UPS!... NO SE ACTUALIZÓ EL PERFIL <br/>
            VERIFIQUE LA INFORMACIÓN INGRESADA, DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. <br/>
            ' . $e->getMessage());
        }
        
        return redirect()->back()->with('alert-success', 'Los datos se han actualizado correctamente!!!');
    }

    public function createBeneficiarios (Request $request) {
        $data = $request->all();
        
        try {
            DB::beginTransaction();
                if (!is_null($data['user_id'])) {
                    $user = User::find($data['user_id']);
                    $createProfile = Profile::create([
                        'user_id' => $data['user_id'],
                        'name' => is_null($user->employee) ? $user->first_name : $user->employee->nombre,
                        'surname_father' => is_null($user->employee) ? $user->last_name : $user->employee->paterno,
                        'surname_mother' => is_null($user->employee) ? null : $user->employee->materno,
                        'cellphone' => is_null($user->employee) ? null : $user->employee->celular,
                        'phone' => is_null($user->employee) ? null : $user->employee->telefono,
                        'email' => is_null($user->employee) ? $user->email : $user->employee->correoempresa,
                        'date_birth' => is_null($user->employee) ? null : $user->employee->nacimiento,
                    ]);
                    $data['profile_id'] = $createProfile->id;
                }
                $createProfileBeneficiaries = $this->createPerfilBeneficiarios($data);
            DB::commit();
        }
        // Ha ocurrido un error, devolvemos la BD a su estado previo y hacemos lo que queramos con esa excepción
        catch (\Throwable $e) {
            DB::rollback();
            /* echo 'ERROR (' . $e->getCode() . ') - - - - > ' . $e->getMessage();
            die(); */
            return redirect()->back()->with('alert-danger', '
            ¡UPS!... NO SE HA GUARDADO EL PERFIL <br/>
            VERIFIQUE LA INFORMACIÓN INGRESADA, DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. <br/>
            ' . $e->getMessage());
        }
        
        return redirect()->back()->with('alert-success', 'Los datos se han ingresado correctamente!!!');
    }

    public function updateBeneficiarios(Request $request) {
        $data = $request->all();

        //dd($data);

        try {
            DB::beginTransaction();
                $updateProfile = $this->updatePerfilBeneficiarios($data);
            DB::commit();
        }
        // Ha ocurrido un error, devolvemos la BD a su estado previo y hacemos lo que queramos con esa excepción
        catch (\Throwable $e) {
            DB::rollback();
            /* echo 'ERROR (' . $e->getCode() . ') - - - - > ' . $e->getMessage();
            die(); */
            return redirect()->back()->with('alert-danger', '
            ¡UPS!... NO SE ACTUALIZÓ EL PERFIL <br/>
            VERIFIQUE LA INFORMACIÓN INGRESADA, DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. <br/>
            ' . $e->getMessage());
        }
        
        return redirect()->back()->with('alert-success', 'Los datos se han actualizado correctamente!!!');
    }


    public function createBienes (Request $request) {
        $data = $request->all();
       
        try {
            DB::beginTransaction();
                if (!is_null($data['user_id'])) {
                    $user = User::find($data['user_id']);
                    $createProfile = Profile::create([
                        'user_id' => $data['user_id'],
                        'name' => is_null($user->employee) ? $user->first_name : $user->employee->nombre,
                        'surname_father' => is_null($user->employee) ? $user->last_name : $user->employee->paterno,
                        'surname_mother' => is_null($user->employee) ? null : $user->employee->materno,
                        'cellphone' => is_null($user->employee) ? null : $user->employee->celular,
                        'phone' => is_null($user->employee) ? null : $user->employee->telefono,
                        'email' => is_null($user->employee) ? $user->email : $user->employee->correoempresa,
                        'date_birth' => is_null($user->employee) ? null : $user->employee->nacimiento,
                    ]);
                    $data['profile_id'] = $createProfile->id;
                }
                $createPerfilBienes = $this->createPerfilBienes($data);
            DB::commit();
        }
        // Ha ocurrido un error, devolvemos la BD a su estado previo y hacemos lo que queramos con esa excepción
        catch (\Throwable $e) {
            DB::rollback();
            /* echo 'ERROR (' . $e->getCode() . ') - - - - > ' . $e->getMessage();
            die(); */
            return redirect()->back()->with('alert-danger', '
            ¡UPS!... NO SE HA GUARDADO EL PERFIL <br/>
            VERIFIQUE LA INFORMACIÓN INGRESADA, DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. <br/>
            ' . $e->getMessage());
        }
        
        return redirect()->back()->with('alert-success', 'Los datos se han ingresado correctamente!!!');
    }

    public function updateBienes(Request $request) {
        $data = $request->all();
        // dd($data['bienesDatos'],$data['tipoBien']);

        //dd($data);

        try {
            DB::beginTransaction();

                $ProfileBienes = new ProfileBienes;
                $ProfileBienes->id_tipo_bien = $data['tipoBien'];
                $ProfileBienes->profile_id = $data['profile_id'];
                $ProfileBienes->codigo = $data['codigo'];
                $ProfileBienes->estatus = 1;
                $ProfileBienes->save();

                foreach ($data['bienesDatos'] as $n => $tipo) {

                    if($n == $data['tipoBien'])
                        foreach ($tipo as $a => $tipoa) {
                            $VariableValor = new VariableValor;
                            $VariableValor->id_referencia = $ProfileBienes->id;
                            $VariableValor->id_variable = $a;
                            $VariableValor->valor = $tipoa[0];
                            // $VariableValor->descripcion = $data["reading"][$n];
                            // $VariableValor->indice = $data["writing"][$n];
                            $VariableValor->save();
                        }
                }

            DB::commit();
        }
        // Ha ocurrido un error, devolvemos la BD a su estado previo y hacemos lo que queramos con esa excepción
        catch (\Throwable $e) {
            DB::rollback();
             echo 'ERROR (' . $e->getCode() . ') - - - - > ' . $e->getMessage();
            die(); 

            // return redirect()->back()->with('alert-danger', '
            // ¡UPS!... NO SE ACTUALIZÓ EL PERFIL <br/>
            // VERIFIQUE LA INFORMACIÓN INGRESADA, DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. <br/>
            // ' . $e->getMessage());
        }
        
        return redirect()->back()->with('alert-success', 'Los datos se han actualizado correctamente!!!');
    }

    public function createContrato(Request $request) {
        $data = $request->all();

        $data = $request->except('_method', '_token', 'name', 'surname_father', 'surname_mother');

        try {
            DB::beginTransaction();
                if (!is_null($data['user_id'])) {
                    $user = User::find($data['user_id']);
                    
                    $createProfile = Profile::create([
                        'user_id' => $data['user_id'],
                        'name' => is_null($user->employee) ? $user->first_name : $user->employee->nombre,
                        'surname_father' => is_null($user->employee) ? $user->last_name : $user->employee->paterno,
                        'surname_mother' => is_null($user->employee) ? null : $user->employee->materno,
                        'cellphone' => is_null($user->employee) ? null : $user->employee->celular,
                        'phone' => is_null($user->employee) ? null : $user->employee->telefono,
                        'email' => is_null($user->employee) ? $user->email : $user->employee->correoempresa,
                        'date_birth' => is_null($user->employee) ? null : $user->employee->nacimiento,
                    ]);

                    $data['profile_id'] = $createProfile->id;
                }
                
                ProfileContrato::updateOrCreate(['profile_id' =>  $data['profile_id']], $data);

            DB::commit();
        }

        // Ha ocurrido un error, devolvemos la BD a su estado previo y hacemos lo que queramos con esa excepción
        catch (\Throwable $e) {
            DB::rollback();
            //  echo 'ERROR (' . $e->getCode() . ') - - - - > ' . $e->getMessage();
            // die(); 

            return redirect()->back()->with('alert-danger', '
            ¡UPS!... NO SE ACTUALIZÓ EL PERFIL <br/>
            VERIFIQUE LA INFORMACIÓN INGRESADA, DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. <br/>
            ' . $e->getMessage());
        }
        
        return redirect()->back()->with('alert-success', 'Los datos se han actualizado correctamente!!!');
    }

    public function updateContrato(Request $request) {
        $data = $request->all();


        $data = $request->except('_method', '_token', 'name', 'surname_father', 'surname_mother');
        try {
            
            DB::beginTransaction();

            ProfileContrato::updateOrCreate(['profile_id' =>  $data['profile_id']], $data);

            DB::commit();
        }
        // Ha ocurrido un error, devolvemos la BD a su estado previo y hacemos lo que queramos con esa excepción
        catch (\Throwable $e) {
            DB::rollback();
            //  echo 'ERROR (' . $e->getCode() . ') - - - - > ' . $e->getMessage();
            // die(); 

            return redirect()->back()->with('alert-danger', '
            ¡UPS!... NO SE ACTUALIZÓ EL PERFIL <br/>
            VERIFIQUE LA INFORMACIÓN INGRESADA, DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. <br/>
            ' . $e->getMessage());
        }
        
        return redirect()->back()->with('alert-success', 'Los datos se han actualizado correctamente!!!');
    }


    private function createPerfilBienes($data) {

        if(!empty($data['bienesDatos'])) {
          
                $ProfileBienes = new ProfileBienes;
                $ProfileBienes->id_tipo_bien = $data['tipoBien'];
                $ProfileBienes->profile_id = $data['profile_id'];
                $ProfileBienes->codigo = $data['codigo'];
                $ProfileBienes->estatus = 1;
                $ProfileBienes->save();

                foreach ($data['bienesDatos'] as $n => $tipo) {

                    if($n == $data['tipoBien'])
                        foreach ($tipo as $a => $tipoa) {
                            $VariableValor = new VariableValor;
                            $VariableValor->id_referencia = $ProfileBienes->id;
                            $VariableValor->id_variable = $a;
                            $VariableValor->valor = $tipoa[0];
                            // $VariableValor->descripcion = $data["reading"][$n];
                            // $VariableValor->indice = $data["writing"][$n];
                            $VariableValor->save();
                        }
                }

        }
    }    

    public function entregaBienes($id_profile, $id_bien) {


        $profile = Profile::with([
            'bienes' => function($q) use ($id_bien) {
                    $q->where('id','=', $id_bien);
            }
        ])->where('id', $id_profile)->first();

        $nombre = $profile->name.'_'.$profile->surname_father.'.pdf';

            // return view('profile.pdf.entregaBienes', compact('profile'));

           $pdf = PDF::loadView('profile.pdf.entregaBienes', compact('profile'));

            $output = $pdf->output();


           return $pdf->download('HojaResponsabilidad-'.$nombre);
           
    }



    public function consulta ($u, $p, $v) {
        // dd($u, $p, $v);
        $profile = null;
        if($u != 0) {
            $profile = Profile::where('user_id', $u)->first();
            $user = null;
        }

        if(is_null($profile)) {
            $profile = Profile::where('id', $p)->first();
            $user = null;
        }

        // dd($profile);

        /* if (is_null($profile)) {
            $this->create();
        } */

        $vacante = Vacante::with('requisicion')->where('id', $v)->withTrashed()->first();
        $vacante_id = $vacante->id;
        $puesto = $vacante->requisicion->puesto;
        $estados = GeoDepartamento::pluck('nombre', 'id');
        $crear = false;
        
        if (!is_null($profile)) {
            if (!is_null($profile->perfilAdicional)) {
                $municipios = GeoMunicipio::where('id_geo_departamento', $profile->perfilAdicional->state)->pluck('nombre', 'id');
            }
            $profileRegistros = $profile->perfilRegistro;
            $profileSalud = $profile->perfilSalud;
            $profileBeneficiarios = $profile->perfilBeneficiarios;
        } else {
            $municipios = null;
            $profileRegistros = null;
            $profileSalud = null;
            $profileBeneficiarios = null;
        }

        $user_id = $u;

        if($profile != null) {
            $fileExists = file_exists($this->path.$profile->image);
        } else {
            $fileExists = false;
        }
        

        $Listausuarios = User::select('employees.id','employees.nombre','employees.paterno','employees.materno')
        ->join('employees','employees.id','=','users.employee_id')
        ->get();

        $GenerarContrato = false;
        $GenerarContratoLegal = false;

        return view('profile.admin', compact(
            'profile',
            'Listausuarios',
            'vacante_id',
            'GenerarContrato',
            'GenerarContratoLegal',
            'puesto',
            'estados',
            'municipios',
            'crear',
            'user',
            'user_id',
            'fileExists',
            'profileRegistros',
            'profileSalud',
            'profileBeneficiarios'
        ));
    }

    public function generar ($id, $v) {
        //$id es el id de la tabla profile
        //es decir el id del perfil, y obtener toda la info
        //necesaria
        //$v es el id de la vacante
        DB::beginTransaction();
        try {
            $profile = Profile::findOrFail($id);
        
            $password = Hash::make($profile->surname_father.'123$');
            $contrasena = $profile->surname_father.'123$';
            $usuario = User::create([
                'first_name'=> $profile->name,
                'last_name'=> $profile->surname_father. ' ' .$profile->surname_mother,
                'email'=> $profile->email,
                'password'=> $password,
                'external'=>true,
            ]);

            $this->enviaCorreo($profile->email, $contrasena);

            $vancie_postulant = Postulante::where([
                'vacante_id' => $v,
                'profile_id' => $id
                ])->update(['user_id' => $usuario->id]);

            $profile->user_id = $usuario->id;
            $profile->save();
            
            DB::commit();
            return redirect()->back()->with('alert-success', 'Se genero el acceso correctamente');
        } catch(\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with('alert-danger', 'Error en el proceso... '.$th->getMessage());
        }
    }

    private function enviaCorreo($email, $contrasena) {
        $to = $email;

        Mail::send('emails.profile.correo', ['contrasena'=>$contrasena], function ($mail)  use ($to){
            $mail->from('soporte@hallmg.com');
            //$mail->to($boss->email);
            $mail->to($to);
            $mail->subject('Ingreso para completar postulación.');
        });
    }

    private function createPerfilBasica($data, $request) {
        $allowedfileExtension=['pdf','docx', 'jpeg', 'jpg', 'png'];

        if(!$request->has('user_id')) {
            $data['user_id'] = Auth::id();
        }

        if ($request->hasFile('file')) {
            foreach($request->file as $name => $file) {
                if (!empty($file)) {
                    $filename = uniqid() . '-' .$file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $check = in_array($extension, $allowedfileExtension);
                    
                    if ($check) {
                        // $file->storeAs('public/profile', $filename);
                        $file->move($this->path, $filename);
                        $data[$name] = $filename;
                    } else {
                        return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                    }
                }
            }
        }

        $perfil = Profile::create($data);
    }

    private function createPerfilComplementaria($data, $request) {
        $allowedfileExtension=['pdf','docx', 'jpeg', 'jpg', 'png'];

        if(!$request->has('user_id')) {
            $data['user_id'] = Auth::id();
        }

        if ($request->hasFile('file')) {
            foreach($request->file as $name => $file) {
                if (!empty($file)) {
                    $filename = uniqid() . '-' .$file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $check = in_array($extension, $allowedfileExtension);
                    
                    if ($check) {
                        // $file->storeAs('public/profile', $filename);
                        $file->move($this->path, $filename);
                        $data[$name] = $filename;
                    } else {
                        return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                    }
                }
            }
        }

        $perfil = Profile::create($data);
        $data['profile_id'] = $perfil->id;

        if (isset($data['file_address'])) {
            $file = $data['file_address'];
            $filename = uniqid() . '-' .$file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $check = in_array($extension, $allowedfileExtension);
            
            if ($check) {
                // $file->storeAs('public/profile', $filename);
                $file->move($this->path, $filename);
                $data['file_address'] = $filename;
            } else {
                return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
            }
        }

        $additional = ProfileAdditional::create($data);



        $application = ProfileApplication::create($data); // Alv no se si quitarlo



        //guardar los datos en la tabla escolaridad
        if(!empty($data['studio'])) {
            foreach ($data['type'] as $n => $tipo) {
                $escolaridad = new ProfileScholarships;
                $escolaridad->profile_id = $data['profile_id'];
                $escolaridad->studio = $data["studio"][$n];
                $escolaridad->school = $data["school"][$n];
                $escolaridad->date_end = $data["date_end"][$n];
                $escolaridad->type = $data["type"][$n];
                if (isset($data['file_studio'][$n])) {
                    $file = $data['file_studio'][$n];
                    $filename = uniqid() . '-' .$file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $check = in_array($extension, $allowedfileExtension);
                    
                    if ($check) {
                        // $file->storeAs('public/profile', $filename);
                        $file->move($this->path, $filename);
                        $escolaridad->file_studio = $filename;
                    } else {
                        return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                    }
                }
                $escolaridad->save();
            }
        }
        
        if(!empty($data['studio_uni'])) { 
            foreach ($data['studio_uni'] as $n => $studio) {
                $escolaridad = new ProfileScholarships;
                $escolaridad->profile_id = $data['profile_id'];
                $escolaridad->studio = $data["studio_uni"][$n];
                $escolaridad->career = $data["career_uni"][$n];
                $escolaridad->type = $data["type_uni"][$n];
                if (isset($data['file_studio_uni'][$n])) {
                    $file = $data['file_studio_uni'][$n];
                    $filename = uniqid() . '-' .$file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $check = in_array($extension, $allowedfileExtension);
                    
                    if ($check) {
                        // $file->storeAs('public/profile', $filename);
                        $file->move($this->path, $filename);
                        $escolaridad->file_studio = $filename;
                    } else {
                        return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                    }
                }
                $escolaridad->save();
            }
        }
    
        if(!empty($data['language'])) { 
            foreach ($data['language'] as $n => $tipo) {
                $lenguaje = new ProfileLanguage;
                $lenguaje->profile_id = $data['profile_id'];
                $lenguaje->language = $data["language"][$n];
                $lenguaje->spoken = $data["spoken"][$n];
                $lenguaje->reading = $data["reading"][$n];
                $lenguaje->writing = $data["writing"][$n];
                $lenguaje->save();
            }
        }

        if(!empty($data['knowledge_type'])) { 
            foreach ($data['knowledge_type'] as $n => $tipo) {
                $lenguaje = new ProfileKnowledge;
                $lenguaje->profile_id = $data['profile_id'];
                $lenguaje->knowledge_name = $data["knowledge_name"][$n];
                $lenguaje->knowledge_type = $data["knowledge_type"][$n];
                $lenguaje->save();
            }
        }


        if(!empty($data['unionized_name'])) { 
            foreach ($data['unionized_name'] as $n => $tipo) {

                $unionized = new ProfileUnionized;
                $unionized->profile_id = $data['profile_id'];
                $unionized->unionized_name = $data["unionized_name"][$n];
                $unionized->save();
            }
        }


        if(!empty($data['job'])) { 
            foreach ($data['job'] as $n => $tipo) {
                // dd($data['profile_id'], $data["job"][$n], $data["company"][$n], $data["date_begin_experience"][$n], $data["date_end_experience"][$n], $data["salary"][$n], $data["reason_separation"][$n], $data["activity"][$n]);
                $experiencia = new ProfileExperience;
                $experiencia->profile_id = $data['profile_id'];
                $experiencia->job = $data["job"][$n];
                $experiencia->company = $data["company"][$n];
                $experiencia->date_begin_experience = $data["date_begin_experience"][$n];
                $experiencia->date_end_experience = $data["date_end_experience"][$n];
                $experiencia->salary = $data["salary"][$n];
                $experiencia->reason_separation = $data["reason_separation"][$n];
                $experiencia->activity = $data["activity"][$n];
                $experiencia->save();
            }
        }

        if(!empty($data['reference_name'])) {
            // dd($data['reference_name']);
            foreach ($data['reference_name'] as $n => $tipo) {
                // dd($data['type_ref'][0], $data['profile_id'], $data["reference_name"][$n], $data["reference_phone"][$n], $data["reference_time_meet"][$n], $data["reference_occupation"][$n]);
                $referencia = new ProfileReference;
                $referencia->type_ref = $data['type_ref'][$n];
                $referencia->profile_id = $data['profile_id'];
                $referencia->reference_name = $data["reference_name"][$n];
                $referencia->reference_phone = $data["reference_phone"][$n];
                $referencia->reference_time_meet = $data["reference_time_meet"][$n];
                $referencia->reference_occupation = $data["reference_occupation"][$n];
                $referencia->save();
            }
        } 

        return $perfil;
    }

    private function createPerfilRegistro($data, $request) {
        $allowedfileExtension=['pdf','docx', 'jpeg', 'jpg', 'png'];

        if ($request->hasFile('file')) {
            foreach($request->file as $name => $file) {
                if (!empty($file)) {
                    $filename = uniqid() . '-' .$file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $check = in_array($extension, $allowedfileExtension);
                    
                    if ($check) {
                        // $file->storeAs('public/profile', $filename);
                        $file->move($this->path, $filename);
                        $data[$name] = $filename;
                    } else {
                        return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                    }
                }
            }
        }

        $perfilRegistro = ProfileRegistry::create($data);
    }

    private function createPerfilSalud($data) {
        $allowedfileExtension=['pdf','docx', 'jpeg', 'jpg', 'png'];

        $perfilSaludSave = ProfileHealth::create($data);
        $data['profile_health_id'] = $perfilSaludSave->id;

        if(!empty($data['date_medical'])) {
            foreach ($data['date_medical'] as $n => $tipo) {
                $saludDet = new ProfileHealthDet;
                $saludDet->profile_health_id = $data['profile_health_id'];
                $saludDet->date = $data["date_medical"][$n];
                $saludDet->reason_medical = $data["reason_medical"][$n];
                $saludDet->type_medical = $data["type_medical"][$n];
                if (isset($data['file_medical'][$n])) {
                    $file = $data['file_medical'][$n];
                    $filename = uniqid() . '-' .$file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $check = in_array($extension, $allowedfileExtension);
                    
                    if ($check) {
                        // $file->storeAs('public/profile', $filename);
                        $file->move($this->path, $filename);
                        $saludDet->file_medical = $filename;
                    } else {
                        return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                    }
                }
                $saludDet->save();
            }
        }
    }    

    private function createPerfilBeneficiarios($data) { 
        $allowedfileExtension=['pdf','docx', 'jpeg', 'jpg', 'png'];

        if (isset($data['file_marriage'])) {
            if(!is_null($data['spouse_name'])) {
                $file = $data['file_marriage'];
                $filename = uniqid() . '-' .$file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);
                if ($check) {
                    // $file->storeAs('public/profile', $filename);
                    $file->move($this->path, $filename);
                    $data['file_marriage'] = $filename;
                } else {
                    return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                }
            }
        }

        if (isset($data['file_birth_certificate_father'])) {
            if(!is_null($data['father_name'])) {
                $file = $data['file_birth_certificate_father'];
                $filename = uniqid() . '-' .$file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);
                if ($check) {
                    // $file->storeAs('public/profile', $filename);
                    $file->move($this->path, $filename);
                    // No funciona en servidor
                    $data['file_birth_certificate_father'] = $filename;
                } else {
                    return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                }
            }
        }

        if (isset($data['file_birth_certificate_mother'])) {
            if(!is_null($data['mother_name'])) {
                $file = $data['file_birth_certificate_mother'];
                $filename = uniqid() . '-' .$file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);
                if ($check) {
                    // $file->storeAs('public/profile', $filename);
                    $file->move($this->path, $filename);
                    $data['file_birth_certificate_mother'] = $filename;
                } else {
                    return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                }
            }
        }

        $perfilBeneficiariosSave = ProfileBeneficiaries::create($data);
        $data['profile_beneficiaries_id'] = $perfilBeneficiariosSave->id;

        //Guardamos el valor de la opción de otro, tabla detalle
        if(!is_null($data['other']['beneficiarie_name'])) {
            if (isset($data['other']['file_birth_certificate_beneficiarie'])) {
                $file = $data['other']['file_birth_certificate_beneficiarie'];
                $filename = uniqid() . '-' .$file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);
                if ($check) {
                    // $file->storeAs('public/profile', $filename);
                    $file->move($this->path, $filename);
                    $data['other']['file_birth_certificate_beneficiarie'] = $filename;
                } else {
                    return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                }
            }
            $data['other']['profile_beneficiaries_id'] = $data['profile_beneficiaries_id'];
            $data['other']['type_beneficiarie'] = $data['other']['type_beneficiarie'];
        
            $perfilBeneficiariosDetSave = ProfileBeneficiariesDet::create($data['other']);
        }

        //valor de la tabla detalle, hijos
        if(!empty($data['son']['beneficiarie_name'])) {
            foreach ($data['son']['beneficiarie_name'] as $n => $tipo) {
                if(!is_null($data['son']['beneficiarie_name'][$n])) {
                    $beneficiariosDet = new ProfileBeneficiariesDet;
                    $beneficiariosDet->profile_beneficiaries_id = $data['profile_beneficiaries_id'];
                    $beneficiariosDet->beneficiarie_name = $data['son']['beneficiarie_name'][$n];
                    $beneficiariosDet->beneficiarie_sex = $data['son']['beneficiarie_sex'][$n];
                    $beneficiariosDet->date_birth_beneficiarie = $data['son']['date_birth_beneficiarie'][$n];
                    $beneficiariosDet->type_beneficiarie = 'HIJO';
                    if (isset($data['son']['file_birth_certificate_beneficiarie'][$n])) {
                        $file = $data['son']['file_birth_certificate_beneficiarie'][$n];
                        $filename = uniqid() . '-' .$file->getClientOriginalName();
                        $extension = $file->getClientOriginalExtension();
                        $check = in_array($extension, $allowedfileExtension);
                        
                        if ($check) {
                            // $file->storeAs('public/profile', $filename);
                            $file->move($this->path, $filename);
                            $beneficiariosDet->file_birth_certificate_beneficiarie = $filename;
                        } else {
                            return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                        }
                    }
                    $beneficiariosDet->save();
                }
            }
        }

    }

    private function updatePerfil($data, $id) {
        $profile = Profile::find($id);
        // dd($data, $id);

        $allowedfileExtension=['pdf','docx', 'jpeg', 'jpg', 'png'];
        $allowedfileExtensionCV=['pdf','docx'];

        //guardar los datos en la tabla application
        //leemos si existe un archivo para el domicilio, de lo contrario solo guardamos la info
        if (isset($data['file']['image'])) {
            $file = $data['file']['image'];
            $filename = uniqid() . '-' .$file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $check = in_array($extension, $allowedfileExtension); 
            // dd($file);
            $checkSize = $this->check_valid_image_size($file);

            if (!$checkSize) {

                return redirect()->back()->with("alert-danger", "Tamaño maximo de imagen 600 x 600");
                
            }else  if ($check) {
                // $file->storeAs('public/profile', $filename);
                $file->move($this->path, $filename);
                $nombreIMG = $filename;
            } else {
                return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
            }

            Profile::where('id', $id)->update(['image' => $nombreIMG]);
        }

        if (isset($data['file']['file_cv'])) {
            $file = $data['file']['file_cv'];
            $filename = uniqid() . '-' .$file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $check = in_array($extension, $allowedfileExtensionCV);
            if ($check) {
                // $file->storeAs('public/profile', $filename);
                $file->move($this->path, $filename);
                $nombrefile_cv = $filename;
            } else {
                return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
            }

            Profile::where('id', $id)->update(['file_cv' => $nombrefile_cv]);
        }



        //guardar los datos en la tabla application
        //leemos si existe un archivo para el domicilio, de lo contrario solo guardamos la info
        if (isset($data['file_address'])) {
            $file = $data['file_address'];
            $filename = uniqid() . '-' .$file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $check = in_array($extension, $allowedfileExtension);
            if ($check) {
                // $file->storeAs('public/profile', $filename);
                $file->move($this->path, $filename);
                $data['file_address'] = $filename;
            } else {
                return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
            }
        }

        //dd($data);

        $application = $profile->perfilAdicional()->updateOrCreate(['id' => $id], $data);

        //guardar los datos en la tabla escolaridad
        if(!empty($data['studio'])) {
            // dd($data['studio']);
            $basicas = $profile->perfilEscuelaBasica;
            $canActual = $profile->perfilEscuelaBasica->count();
            $canRecibida = count($data['type']);

            if ($canRecibida < $canActual) {
                // dd($data, $data['studio'], $basicas, 'canActual: '.$canActual, 'canRecibida: '.$canRecibida);
                // dd($data['studio'], $basicas);
                $tmp = []; // <----------------------- Esto
                foreach ($data['studio'] as $studio) {
                    foreach ($basicas as $basica) {
                        // dd($basica->studio, $studio);
                        if ($basica->studio == $studio && !in_array($studio, $tmp)) { // <----------------------- Esto
                            $existen[$basica->id] = $basica->studio;
                            array_push($tmp, $studio); // <----------------------- Esto
                        } else {
                            $borrar[$basica->id] = $basica->studio;
                        }
                    }
                }
                // dd('Existen: ', $existen, 'Borrar: ', $borrar, $data['studio'], $tmp);
                // dd('rawr');
                $eliminar = array_diff_assoc($borrar, $existen);
                // dd($eliminar);
                $j=0;
                foreach ($eliminar as $key => $elimina) {
                    $fileExiste = ProfileScholarships::select('file_studio')->where('id', $key)->orderBy('id', 'DESC')->first();
                    $exists = Storage::exists('public/profile/'.$fileExiste->file_studio);

                    if ($exists) {
                        Storage::delete('public/profile/'.$fileExiste->file_studio);
                    }
                    ProfileScholarships::where('id', $key)->delete();
                }
                $i=0;
                // dd('rawr');
                foreach ($existen as $key => $existe) {
                    if (isset($data['file_studio'][$i])) {
                        $file = $data['file_studio'][$i];
                        $filename = uniqid() . '-' .$file->getClientOriginalName();
                        $extension = $file->getClientOriginalExtension();
                        $check = in_array($extension, $allowedfileExtension);
    
                        if ($check) {
                            $fileExiste = ProfileScholarships::select('file_studio')->where('id', $key)->orderBy('id', 'DESC')->first();
                            $exists = Storage::exists('public/profile/'.$fileExiste->file_studio);
    
                            if ($exists){
                                Storage::delete('public/profile/'.$fileExiste->file_studio);
                            }
                            // $file->storeAs('public/profile', $filename);
                            $file->move($this->path, $filename);
                            ProfileScholarships::where('id', $key)->update([
                                'file_studio' => $filename,
                            ]);
                        } else {
                            return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                        }
                    }
                    ProfileScholarships::where('id', $key)
                    ->update([
                        'studio' => $data["studio"][$i],
                        'school' => $data["school"][$i],
                        'date_end' => $data["date_end"][$i],
                        //'voucher' => $data["voucher"][$i]
                        //'file_studio' => $data["file_studio"][$i],
                    ]);
                    $i++;
                }
            } elseif ($canActual == $canRecibida) {
                foreach ($data['type'] as $n => $tipo) {
                    if($basicas[$n]->studio == $data["studio"][$n]) {
                        ProfileScholarships::where('id', $basicas[$n]->id)
                        ->update([
                            'studio' => $data["studio"][$n],
                            'school' => $data["school"][$n],
                            'date_end' => $data["date_end"][$n],
                            //'voucher' => $data["voucher"][$n]
                        ]);
                        if (isset($data['file_studio'][$n])) {
                            $file = $data['file_studio'][$n];
                            $filename = uniqid() . '-' .$file->getClientOriginalName();
                            $extension = $file->getClientOriginalExtension();
                            $check = in_array($extension, $allowedfileExtension);

                            if ($check) {
                                $fileExiste = ProfileScholarships::select('file_studio')->where('studio', $data['studio'][$n])->where('profile_id', $id)->orderBy('id', 'DESC')->first();
                                $exists = Storage::exists('public/profile/'.$fileExiste->file_studio);

                                if ($exists){
                                    Storage::delete('public/profile/'.$fileExiste->file_studio);
                                }
                                // $file->storeAs('public/profile', $filename);
                                $file->move($this->path, $filename);
                                ProfileScholarships::where('id', $basicas[$n]->id)->update([
                                    'file_studio' => $filename,
                                ]);
                            } else {
                                return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                            }
                        }
                    }  elseif ($basicas[$n]->studio != $data["studio"][$n]) {
                        ProfileScholarships::where('id', $basicas[$n]->id)
                        ->update([
                            'studio' => $data["studio"][$n],
                            'school' => $data["school"][$n],
                            'date_end' => $data["date_end"][$n],
                            //'voucher' => $data["voucher"][$n]
                        ]);
                        if (isset($data['file_studio'][$n])) {
                            $file = $data['file_studio'][$n];
                            $filename = uniqid() . '-' .$file->getClientOriginalName();
                            $extension = $file->getClientOriginalExtension();
                            $check = in_array($extension, $allowedfileExtension);
                            //dd($data, $file);
                            if ($check) {
                                $fileExiste = ProfileScholarships::select('file_studio')->where('studio', $data['studio'][$n])->where('profile_id', $id)->orderBy('id', 'DESC')->first();
                                $exists = Storage::exists('public/profile/'.$fileExiste->file_studio);

                                if ($exists){
                                    Storage::delete('public/profile/'.$fileExiste->file_studio);
                                }
                                // $file->storeAs('public/profile', $filename);
                                $file->move($this->path, $filename);
                                ProfileScholarships::where('id', $basicas[$n]->id)->update([
                                    'file_studio' => $filename,
                                ]);
                            } else {
                                return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                            }
                        }
                    } 
                }
            } elseif ($canRecibida > $canActual) {
                for ($n=0; $n < $canActual; $n++) { 
                    if($basicas[$n]->studio == $data["studio"][$n]) {
                        ProfileScholarships::where('id', $basicas[$n]->id)
                        ->update([
                            'studio' => $data["studio"][$n],
                            'school' => $data["school"][$n],
                            'date_end' => $data["date_end"][$n],
                            //'voucher' => $data["voucher"][$n]
                            //'file_studio' => $data["file_studio"][$n],
                        ]);
                        if (isset($data['file_studio'][$n])) {
                            $file = $data['file_studio'][$n];
                            $filename = uniqid() . '-' .$file->getClientOriginalName();
                            $extension = $file->getClientOriginalExtension();
                            $check = in_array($extension, $allowedfileExtension);

                            if ($check) {
                                $fileExiste = ProfileScholarships::select('file_studio')->where('studio', $data['studio'][$n])->where('profile_id', $id)->orderBy('id', 'DESC')->first();
                                $exists = Storage::exists('public/profile/'.$fileExiste->file_studio);

                                if ($exists){
                                    Storage::delete('public/profile/'.$fileExiste->file_studio);
                                }
                                // $file->storeAs('public/profile', $filename);
                                $file->move($this->path, $filename);
                                ProfileScholarships::where('id', $basicas[$n]->id)->update([
                                    'file_studio' => $filename,
                                ]);
                            } else {
                                return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                            }
                        }
                    }  elseif ($basicas[$n]->studio != $data["studio"][$n]) {
                        ProfileScholarships::where('id', $basicas[$n]->id)
                        ->update([
                            'studio' => $data["studio"][$n],
                            'school' => $data["school"][$n],
                            'date_end' => $data["date_end"][$n],
                            //'voucher' => $data["voucher"][$n]
                            //'file_studio' => $data["file_studio"][$n],
                        ]);
                        if (isset($data['file_studio'][$n])) {
                            $file = $data['file_studio'][$n];
                            $filename = uniqid() . '-' .$file->getClientOriginalName();
                            $extension = $file->getClientOriginalExtension();
                            $check = in_array($extension, $allowedfileExtension);
                            //dd($data, $file);
                            if ($check) {
                                $fileExiste = ProfileScholarships::select('file_studio')->where('studio', $data['studio'][$n])->where('profile_id', $id)->orderBy('id', 'DESC')->first();
                                $exists = Storage::exists('public/profile/'.$fileExiste->file_studio);

                                if ($exists){
                                    Storage::delete('public/profile/'.$fileExiste->file_studio);
                                }
                                // $file->storeAs('public/profile', $filename);
                                $file->move($this->path, $filename);
                                ProfileScholarships::where('id', $basicas[$n]->id)->update([
                                    'file_studio' => $filename,
                                ]);
                            } else {
                                return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                            }
                        }
                    } 
                }
                //dd($data);
                for ($n=$canRecibida; $n > $canActual; $n--) {
                    $escolaridad = new ProfileScholarships;
                    $escolaridad->profile_id = $id;
                    $escolaridad->studio = $data["studio"][$n-1];
                    $escolaridad->school = $data["school"][$n-1];
                    $escolaridad->date_end = $data["date_end"][$n-1];
                    //$escolaridad->voucher = $data["voucher"][$n-1];
                    $escolaridad->type = $data["type"][$n-1];
                    if (isset($data['file_studio'][$n-1])) {
                        $file = $data['file_studio'][$n-1];
                        $filename = uniqid() . '-' .$file->getClientOriginalName();
                        $extension = $file->getClientOriginalExtension();
                        $check = in_array($extension, $allowedfileExtension);
                        
                        if ($check) {
                            // $file->storeAs('public/profile', $filename);
                            $file->move($this->path, $filename);
                            $escolaridad->file_studio = $filename;
                        } else {
                            return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                        }
                    }
                    $escolaridad->save();
                }
            }
            // dd('aquí no llego');
        }
   
        if(!empty($data['studio_uni'])) { 
            $superiores = $profile->perfilEscuelaSuperior;
            $canActual = $profile->perfilEscuelaSuperior->count();
            $canRecibida = count($data['type_uni']);

            //dd($data, $canActual, $canRecibida);
            if ($canRecibida < $canActual) {
                $tmp = [];
                foreach ($data['studio_uni'] as $studio) {
                    foreach ($superiores as $superior) {
                        if ($superior->studio == $studio && !in_array($studio, $tmp)) {
                            $existen[$superior->id] = $superior->studio;
                            array_push($tmp, $studio);
                        } else {
                            $borrar[$superior->id] = $superior->studio;
                        }
                    }
                }
                $eliminar = array_diff_assoc($borrar, $existen);
                foreach ($eliminar as $key => $elimina) {
                    $fileExiste = ProfileScholarships::select('file_studio')->where('id', $key)->orderBy('id', 'DESC')->first();
                    $exists = Storage::exists('public/profile/'.$fileExiste->file_studio);

                    if ($exists) {
                        Storage::delete('public/profile/'.$fileExiste->file_studio);
                    }
                    ProfileScholarships::where('id', $key)->delete();
                }
                $i=0;
                foreach ($existen as $key => $existe) {
                    if (isset($data['file_studio_uni'][$i])) {
                        $file = $data['file_studio_uni'][$i];
                        $filename = uniqid() . '-' .$file->getClientOriginalName();
                        $extension = $file->getClientOriginalExtension();
                        $check = in_array($extension, $allowedfileExtension);
    
                        if ($check) {
                            $fileExiste = ProfileScholarships::select('file_studio')->where('id', $key)->orderBy('id', 'DESC')->first();
                            $exists = Storage::exists('public/profile/'.$fileExiste->file_studio);
    
                            if ($exists){
                                Storage::delete('public/profile/'.$fileExiste->file_studio);
                            }
                            // $file->storeAs('public/profile', $filename);
                            $file->move($this->path, $filename);
                            ProfileScholarships::where('id', $key)->update([
                                'file_studio' => $filename,
                            ]);
                        } else {
                            return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                        }
                    }
                    ProfileScholarships::where('id', $key)
                    ->update([
                        'studio' => $data["studio_uni"][$i],
                        'career' => $data["career_uni"][$i],
                        //'voucher' => $data["voucher_uni"][$i]
                    ]);
                    $i++;
                }
            } elseif ($canActual == $canRecibida) {
                foreach ($data['type_uni'] as $n => $tipo) {
                    if($superiores[$n]->studio == $data["studio_uni"][$n]) {
                        ProfileScholarships::where('id', $superiores[$n]->id)
                        ->update([
                            'studio' => $data["studio_uni"][$n],
                            'career' => $data["career_uni"][$n],
                            //'voucher' => $data["voucher_uni"][$n]
                            // 'voucher' => $data["voucher"][$n]
                        ]);
                        if (isset($data['file_studio_uni'][$n])) {
                            $file = $data['file_studio_uni'][$n];
                            $filename = uniqid() . '-' .$file->getClientOriginalName();
                            $extension = $file->getClientOriginalExtension();
                            $check = in_array($extension, $allowedfileExtension);

                            if ($check) {
                                $fileExiste = ProfileScholarships::select('file_studio')->where('studio', $data['studio_uni'][$n])->where('profile_id', $id)->orderBy('id', 'DESC')->first();
                                $exists = Storage::exists('public/profile/'.$fileExiste->file_studio);

                                if ($exists){
                                    Storage::delete('public/profile/'.$fileExiste->file_studio);
                                }
                                // $file->storeAs('public/profile', $filename);
                                $file->move($this->path, $filename);
                                ProfileScholarships::where('id', $superiores[$n]->id)->update([
                                    'file_studio' => $filename,
                                ]);
                            } else {
                                return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                            }
                        }
                    }  elseif ($superiores[$n]->studio != $data["studio_uni"][$n]) {
                        ProfileScholarships::where('id', $superiores[$n]->id)
                        ->update([
                            'studio' => $data["studio_uni"][$n],
                            'career' => $data["career_uni"][$n],
                            //'voucher' => $data["voucher_uni"][$n]
                            // 'voucher' => $data["voucher"][$n]
                        ]);
                        if (isset($data['file_studio_uni'][$n])) {
                            $file = $data['file_studio_uni'][$n];
                            $filename = uniqid() . '-' .$file->getClientOriginalName();
                            $extension = $file->getClientOriginalExtension();
                            $check = in_array($extension, $allowedfileExtension);
                            //dd($data, $file);
                            if ($check) {
                                $fileExiste = ProfileScholarships::select('file_studio')->where('studio', $data['studio_uni'][$n])->where('profile_id', $id)->orderBy('id', 'DESC')->first();
                                $exists = Storage::exists('public/profile/'.$fileExiste->file_studio);

                                if ($exists){
                                    Storage::delete('public/profile/'.$fileExiste->file_studio);
                                }
                                // $file->storeAs('public/profile', $filename);
                                $file->move($this->path, $filename);
                                ProfileScholarships::where('id', $superiores[$n]->id)->update([
                                    'file_studio' => $filename,
                                ]);
                            } else {
                                return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                            }
                        }
                    } 
                }
            } elseif ($canRecibida > $canActual) {
                for ($n=0; $n < $canActual; $n++) {
                    if($superiores[$n]->studio == $data["studio_uni"][$n]) {
                        ProfileScholarships::where('id', $superiores[$n]->id)
                        ->update([
                            'studio' => $data["studio_uni"][$n],
                            'career' => $data["career_uni"][$n],
                            //'voucher' => $data["voucher_uni"][$n]
                            // 'voucher' => $data["voucher"][$n]
                            //'file_studio' => $data["file_studio"][$n],
                        ]);
                        if (isset($data['file_studio_uni'][$n])) {
                            $file = $data['file_studio_uni'][$n];
                            $filename = uniqid() . '-' .$file->getClientOriginalName();
                            $extension = $file->getClientOriginalExtension();
                            $check = in_array($extension, $allowedfileExtension);

                            if ($check) {
                                $fileExiste = ProfileScholarships::select('file_studio')->where('studio', $data['studio_uni'][$n])->where('profile_id', $id)->orderBy('id', 'DESC')->first();
                                $exists = Storage::exists('public/profile/'.$fileExiste->file_studio);

                                if ($exists){
                                    Storage::delete('public/profile/'.$fileExiste->file_studio);
                                }
                                // $file->storeAs('public/profile', $filename);
                                $file->move($this->path, $filename);
                                ProfileScholarships::where('id', $superiores[$n]->id)->update([
                                    'file_studio' => $filename,
                                ]);
                            } else {
                                return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                            }
                        }
                    }  elseif ($superiores[$n]->studio != $data["studio_uni"][$n]) {
                        ProfileScholarships::where('id', $superiores[$n]->id)
                        ->update([
                            'studio' => $data["studio_uni"][$n],
                            'career' => $data["career_uni"][$n],
                            //'voucher' => $data["voucher_uni"][$n]
                            // 'voucher' => $data["voucher"][$n]
                            //'file_studio' => $data["file_studio"][$n],
                        ]);
                        if (isset($data['file_studio_uni'][$n])) {
                            $file = $data['file_studio_uni'][$n];
                            $filename = uniqid() . '-' .$file->getClientOriginalName();
                            $extension = $file->getClientOriginalExtension();
                            $check = in_array($extension, $allowedfileExtension);
                            //dd($data, $file);
                            if ($check) {
                                $fileExiste = ProfileScholarships::select('file_studio')->where('studio', $data['studio_uni'][$n])->where('profile_id', $id)->orderBy('id', 'DESC')->first();
                                $exists = Storage::exists('public/profile/'.$fileExiste->file_studio);

                                if ($exists){
                                    Storage::delete('public/profile/'.$fileExiste->file_studio);
                                }
                                // $file->storeAs('public/profile', $filename);
                                $file->move($this->path, $filename);
                                ProfileScholarships::where('id', $superiores[$n]->id)->update([
                                    'file_studio' => $filename,
                                ]);
                            } else {
                                return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                            }
                        }
                    } 
                }
                for ($n=$canRecibida; $n > $canActual; $n--) {
                    $escolaridad = new ProfileScholarships;
                    $escolaridad->profile_id = $id;
                    $escolaridad->studio = $data["studio_uni"][$n-1];
                    $escolaridad->career = $data["career_uni"][$n-1];
                    //$escolaridad->voucher = $data["voucher_uni"][$n-1];
                    $escolaridad->type = $data["type_uni"][$n-1];
                    if (isset($data['file_studio_uni'][$n-1])) {
                        $file = $data['file_studio_uni'][$n-1];
                        $filename = uniqid() . '-' .$file->getClientOriginalName();
                        $extension = $file->getClientOriginalExtension();
                        $check = in_array($extension, $allowedfileExtension);
                        
                        if ($check) {
                            // $file->storeAs('public/profile', $filename);
                            $file->move($this->path, $filename);
                            $escolaridad->file_studio = $filename;
                        } else {
                            return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                        }
                    }
                    $escolaridad->save();
                }
            }
        }
    
        if(!empty($data['language'])) { 
            $lenguajes = $profile->perfilLenguaje;
            $canActual = $profile->perfilLenguaje->count();
            $canRecibida = count($data['language']);

            if ($canRecibida < $canActual) {
                $tmp = [];
                foreach ($data['language'] as $idioma) {
                    foreach ($lenguajes as $lenguaje) {
                        if ($lenguaje->language == $idioma && !in_array($idioma, $tmp)) {
                            $existen[$lenguaje->id] = $lenguaje->language;
                            array_push($tmp, $idioma);
                        } else {
                            $borrar[$lenguaje->id] = $lenguaje->language;
                        }
                    }
                }
                $eliminar = array_diff_assoc($borrar, $existen);
                foreach ($eliminar as $key => $elimina) {
                    ProfileLanguage::where('id', $key)->delete();
                }
                $i=0;
                foreach ($existen as $key => $existe) {
                    $this->updateLanguage($lenguajes, $data, $i, $key);
                    $i++;
                }
            } elseif ($canActual == $canRecibida) {
                foreach ($data['language'] as $n => $tipo) {
                    if($lenguajes[$n]->language == $data["language"][$n]) {
                        $this->updateLanguage($lenguajes, $data, $n);
                    }  elseif ($lenguajes[$n]->language != $data["language"][$n]) {
                        $this->updateLanguage($lenguajes, $data, $n);
                    } 
                }
            } elseif ($canRecibida > $canActual) {
                for ($n=0; $n < $canActual; $n++) {
                    if($lenguajes[$n]->language == $data["language"][$n]) {
                        $this->updateLanguage($lenguajes, $data, $n);
                    }  elseif($lenguajes[$n]->language == $data["language"][$n]) {
                        $this->updateLanguage($lenguajes, $data, $n);
                    } 
                }
                for ($n=$canRecibida; $n > $canActual; $n--) {
                    $lenguaje = new ProfileLanguage;
                    $lenguaje->profile_id = $id;
                    $lenguaje->language = $data["language"][$n-1];
                    $lenguaje->spoken = $data["spoken"][$n-1];
                    $lenguaje->reading = $data["reading"][$n-1];
                    $lenguaje->writing = $data["writing"][$n-1];
                    $lenguaje->save();
                }
            }
        }

        if(!empty($data['knowledge_type'])) { 
            $conocimientos = $profile->perfilConocimientos;
            $canActual = $profile->perfilConocimientos->count();
            $canRecibida = count($data['knowledge_type']);

            if ($canRecibida < $canActual) {
                $tmp = [];
                foreach ($data['knowledge_type'] as $conocimiento) {
                    foreach ($conocimientos as $know) {
                        if ($know->knowledge_type == $conocimiento && !in_array($conocimiento, $tmp)) {
                            $existen[$know->id] = $know->knowledge_name;
                            array_push($tmp, $conocimiento);
                        } else {
                            $borrar[$know->id] = $know->knowledge_name;
                        }
                    }
                }
                $eliminar = array_diff_assoc($borrar, $existen);
                foreach ($eliminar as $key => $elimina) {
                    ProfileKnowledge::where('id', $key)->delete();
                }
                $i=0;
                foreach ($existen as $key => $existe) {
                    $this->updateKnowledge($conocimientos, $data, $i, $key);
                    $i++;
                }
            } elseif ($canActual == $canRecibida) {
                foreach ($data['knowledge_type'] as $n => $tipo) {
                    if($conocimientos[$n]->knowledge_type == $data["knowledge_type"][$n]) {
                        $this->updateKnowledge($conocimientos, $data, $n);
                    }  elseif ($conocimientos[$n]->knowledge_type != $data["knowledge_type"][$n]) {
                        $this->updateKnowledge($conocimientos, $data, $n);
                    } 
                }
            } elseif ($canRecibida > $canActual) {
                for ($n=0; $n < $canActual; $n++) {
                    if($conocimientos[$n]->knowledge_type == $data["knowledge_type"][$n]) {
                        $this->updateKnowledge($conocimientos, $data, $n);
                    }  elseif($conocimientos[$n]->knowledge_type == $data["knowledge_type"][$n]) {
                        $this->updateKnowledge($conocimientos, $data, $n);
                    } 
                }
                for ($n=$canRecibida; $n > $canActual; $n--) {
                    $lenguaje = new ProfileKnowledge;
                    $lenguaje->profile_id = $id;
                    $lenguaje->knowledge_name = $data["knowledge_name"][$n-1];
                    $lenguaje->knowledge_type = $data["knowledge_type"][$n-1];
                    $lenguaje->save();
                }
            }
        } 


        if(!empty($data['unionized_name'])) { 

            $conocimientos = $profile->perfilSindicatos;
            $canActual = $profile->perfilSindicatos->count();
            $canRecibida = count($data['unionized_name']);

            if ($canRecibida < $canActual) {
                $tmp = [];
                foreach ($data['unionized_name'] as $conocimiento) {
                    foreach ($conocimientos as $referencia) {
                        if ($referencia->unionized_name == $conocimiento && !in_array($conocimiento, $tmp)) {
                            $existen[$referencia->id] = $referencia->unionized_name;
                            array_push($tmp, $conocimiento);
                        } else {
                            $borrar[$referencia->id] = $referencia->unionized_name;
                        }
                    }
                }

                $eliminar = array_diff_assoc($borrar, $existen);
                foreach ($eliminar as $key => $elimina) {
                    ProfileUnionized::where('id', $key)->delete();
                }
                $i=0;
                foreach ($existen as $key => $existe) {
                    $this->updateUnionized($conocimientos, $data, $i, $key);
                    $i++;
                }
            } elseif ($canActual == $canRecibida) {
               
                foreach ($data['unionized_name'] as $n => $tipo) {
                    if($conocimientos[$n]->unionized_name != $data["unionized_name"][$n]) {
                        $this->updateUnionized($conocimientos, $data, $n);

                    }
                }
            } elseif ($canRecibida > $canActual) {
                for ($n=0; $n < $canActual; $n++) {
                    if($conocimientos[$n]->unionized_name == $data["unionized_name"][$n]) {
                        $this->updateUnionized($conocimientos, $data, $n);
                    }  elseif($conocimientos[$n]->unionized_name == $data["unionized_name"][$n]) {
                        $this->updateUnionized($conocimientos, $data, $n);
                    } 
                }
                for ($n=$canRecibida; $n > $canActual; $n--) {
                    $Unionized = new ProfileUnionized;
                    $Unionized->profile_id = $id;
                    $Unionized->unionized_name = $data["unionized_name"][$n-1];
                    $Unionized->save();
                }
            }
        } 

        // dd($data);
        if(!empty($data['job'])) { 
            $experiencias = $profile->perfilExperiencia;
            $canActual = $profile->perfilExperiencia->count();
            $canRecibida = count($data['job']);

            if ($canRecibida < $canActual) {
                $tmp = [];
                foreach ($data['job'] as $conocimiento) {
                    foreach ($experiencias as $experiencia) {
                        if ($experiencia->job == $conocimiento && !in_array($conocimiento, $tmp)) {
                            $existen[$experiencia->id] = $experiencia->job;
                            array_push($tmp, $conocimiento);
                        } else {
                            $borrar[$experiencia->id] = $experiencia->job;
                        }
                    }
                }
                $eliminar = array_diff_assoc($borrar, $existen);
                foreach ($eliminar as $key => $elimina) {
                    ProfileExperience::where('id', $key)->delete();
                }
                $i=0;
                foreach ($existen as $key => $existe) {
                    $this->updateExperience($experiencias, $data, $i, $key);
                    $i++;
                }
            } elseif ($canActual == $canRecibida) {
                foreach ($data['job'] as $n => $tipo) {
                    if($experiencias[$n]->job == $data["job"][$n]) {
                        $this->updateExperience($experiencias, $data, $n);
                    }  elseif ($experiencias[$n]->job != $data["job"][$n]) {
                        $this->updateExperience($experiencias, $data, $n);
                    } 
                }
            } elseif ($canRecibida > $canActual) {
                for ($n=0; $n < $canActual; $n++) {
                    if($experiencias[$n]->job == $data["job"][$n]) {
                        $this->updateExperience($experiencias, $data, $n);
                    }  elseif($experiencias[$n]->job == $data["job"][$n]) {
                        $this->updateExperience($experiencias, $data, $n);
                    } 
                }
                for ($n=$canRecibida; $n > $canActual; $n--) {
                    $experiencia = new ProfileExperience;
                    $experiencia->profile_id = $id;
                    $experiencia->job = $data["job"][$n-1];
                    $experiencia->company = $data["company"][$n-1];
                    $experiencia->date_begin_experience = $data["date_begin_experience"][$n-1];
                    $experiencia->date_end_experience = $data["date_end_experience"][$n-1];
                    $experiencia->salary = $data["salary"][$n-1];
                    $experiencia->reason_separation = $data["reason_separation"][$n-1];
                    $experiencia->activity = $data["activity"][$n-1];
                    $experiencia->save();
                }
            }
        } 

  

        if(!empty($data['reference_name'])) {
            $referencias = $profile->perfilReferencias;
            $laborales = $profile->perfilReferenciasLaborales;
            $referencias = $referencias->merge($laborales);
            $canActual = $profile->perfilReferencias->count() + $profile->perfilReferenciasLaborales->count();
            $canRecibida = count($data['reference_name']);

            if ($canRecibida < $canActual) {
                // dd('<');
                $tmp = [];
                foreach ($data['reference_name'] as $conocimiento) {
                    foreach ($referencias as $referencia) {
                        if ($referencia->reference_name == $conocimiento && !in_array($conocimiento, $tmp)) {
                            $existen[$referencia->id] = $referencia->reference_name;
                            array_push($tmp, $conocimiento);
                        } else {
                            $borrar[$referencia->id] = $referencia->reference_name;
                        }
                    }
                }
                $eliminar = array_diff_assoc($borrar, $existen);
                
                foreach ($eliminar as $key => $elimina) {
                    ProfileReference::where('id', $key)->delete();
                }
                $i=0;
                foreach ($existen as $key => $existe) {
                    $this->updateReference($referencias, $data, $i, $key);
                    $i++;
                }
            } elseif ($canActual == $canRecibida) {
                // dd('==');
                foreach ($data['reference_name'] as $n => $tipo) {
                    if($referencias[$n]->reference_name == $data["reference_name"][$n]) {
                        $this->updateReference($referencias, $data, $n);
                    }  elseif ($referencias[$n]->reference_name != $data["reference_name"][$n]) {
                        $this->updateReference($referencias, $data, $n);
                    } 
                }
            } elseif ($canRecibida > $canActual) {
                // dd($data['reference_name'],  $data["type_ref"], 'canActual', $canActual, 'canRecibida', $canRecibida, $data);
                for($n = 0; $n < $canRecibida; $n++) {
                    $n_reference = ProfileReference::where('profile_id', $id)
                                    ->where('type_ref', $data["type_ref"][$n])
                                    ->where('reference_name', $data["reference_name"][$n])
                                    ->first();

                    if($n_reference != null) {
                        $n_reference->reference_phone = $data["reference_phone"][$n];
                        $n_reference->reference_time_meet = $data["reference_time_meet"][$n];
                        $n_reference->reference_occupation = $data["reference_occupation"][$n];
                        $n_reference->save();
                    } else { 
                        if(!empty($data["reference_name"][$n])) {
                            $referencia = new ProfileReference;
                            $referencia->profile_id = $id;
                            $referencia->reference_name = $data["reference_name"][$n];
                            $referencia->type_ref = $data["type_ref"][$n];
                            $referencia->reference_phone = $data["reference_phone"][$n];
                            $referencia->reference_time_meet = $data["reference_time_meet"][$n];
                            $referencia->reference_occupation = $data["reference_occupation"][$n];
                            $referencia->save();

                        }
                    }
                }
            }
        }

        $additional = $profile->perfilSolicitud()->updateOrCreate(['profile_id' => $id], $data);
    }

    private function updateLanguage($lenguajes, $data, $n = null, $i = null, $key = null) {
        if (is_null($n)) {
            ProfileLanguage::where('id', $key)->update([
                'language' => $data["language"][$i],
                'spoken' => $data["spoken"][$i],
                'reading' => $data["reading"][$i],
                'writing' => $data["writing"][$i]
            ]);
        } else {
            ProfileLanguage::where('id', $lenguajes[$n]->id)->update([
                'language' => $data["language"][$n],
                'spoken' => $data["spoken"][$n],
                'reading' => $data["reading"][$n],
                'writing' => $data["writing"][$n]
            ]);
        }
    }

    private function updateKnowledge($conocimientos, $data, $n = null, $i = null, $key = null) {
        if (is_null($n)) {
            ProfileKnowledge::where('id', $key)->update([
                'knowledge_name' => $data["knowledge_name"][$i],
                'knowledge_type' => $data["knowledge_type"][$i],
            ]);
        } else {
            ProfileKnowledge::where('id', $conocimientos[$n]->id)->update([
                'knowledge_name' => $data["knowledge_name"][$n],
                'knowledge_type' => $data["knowledge_type"][$n],
            ]);
        }
    }
    private function updateUnionized($conocimientos, $data, $n = null, $i = null, $key = null) {
        if (is_null($n)) {
            ProfileUnionized::where('id', $key)->update([
                'unionized_name' => $data["unionized_name"][$i],
            ]);
        } else {
            ProfileUnionized::where('id', $conocimientos[$n]->id)->update([
                'unionized_name' => $data["unionized_name"][$n],
            ]);
        }
    }

    private function updateExperience($conocimientos, $data, $n = null, $i = null, $key = null) {
        if (is_null($n)) {
            ProfileExperience::where('id', $key)->update([
                'job' => $data["job"][$i],
                'company' => $data["company"][$i],
                'date_begin_experience' => $data["date_begin_experience"][$i],
                'date_end_experience' => $data["date_end_experience"][$i],
                'salary' => $data["salary"][$i],
                'reason_separation' => $data["reason_separation"][$i],
                'activity' => $data["activity"][$i],
            ]);
        } else {
            ProfileExperience::where('id', $conocimientos[$n]->id)->update([
                'job' => $data["job"][$n],
                'company' => $data["company"][$n],
                'date_begin_experience' => $data["date_begin_experience"][$n],
                'date_end_experience' => $data["date_end_experience"][$n],
                'salary' => $data["salary"][$n],
                'reason_separation' => $data["reason_separation"][$n],
                'activity' => $data["activity"][$n],
            ]);
        }
    }

    private function updateReference($referencias, $data, $n = null, $i = null, $key = null) {
        if (is_null($n)) {
            ProfileReference::where('id', $key)->update([
                'reference_name' => $data["reference_name"][$i],
                'reference_phone' => $data["reference_phone"][$i],
                'reference_time_meet' => $data["reference_time_meet"][$i],
                'reference_occupation' => $data["reference_occupation"][$i],
            ]);
        } else {
            ProfileReference::where('id', $referencias[$n]->id)->update([
                'reference_name' => $data["reference_name"][$n],
                'reference_phone' => $data["reference_phone"][$n],
                'reference_time_meet' => $data["reference_time_meet"][$n],
                'reference_occupation' => $data["reference_occupation"][$n],
            ]);
        }
    }

    private function updatePerfilRegistro($data, $request) {
        $profile_id = $data['profile_id'];
        // $profileRegistro = ProfileRegistry::find($profile_id);
        $profileRegistro = ProfileRegistry::where('profile_id', $profile_id)->first();
        
        $allowedfileExtension=['pdf','docx', 'jpeg', 'jpg', 'png'];


        if ($request->hasFile('file')) {
            foreach($request->file as $name => $file) {
                if (!empty($file)) {
                    $filename = uniqid() . '-' .$file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $check = in_array($extension, $allowedfileExtension);

                    if ($check) {
                        $fileExiste = ProfileRegistry::select($name)->where('profile_id', $profile_id)->first();
                        if (!is_null($fileExiste)) {
                            $exists = File::exists($this->path.$fileExiste->$name);
                            if ($exists){
                                File::delete($this->path.$fileExiste->$name);
                            }
                        }
                        // $file->storeAs('public/profile', $filename);
                        $file->move($this->path, $filename);
                        $data[$name] = $filename;
                    } else {
                        return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                    }
                }
            }
        }
        $profileRegistro->update($data);
    }

    private function updatePerfilSalud($data) {
        $profile_id = $data['profile_id'];
        // $profileSalud = ProfileHealth::find($profile_id);
        $profileSalud = ProfileHealth::where('profile_id', $profile_id)->first();
        $data['profile_health_id'] = $profileSalud->id;
        
        $allowedfileExtension=['pdf','docx', 'jpeg', 'jpg', 'png'];

        if(!empty($data['date_medical'])) {
            foreach ($data['date_medical'] as $n => $tipo) {
                if(!is_null($data['date_medical'][$n])) {
                    $saludDet = new ProfileHealthDet;
                    $saludDet->profile_health_id = $data['profile_health_id'];
                    $saludDet->date = $data["date_medical"][$n];
                    $saludDet->reason_medical = $data["reason_medical"][$n];
                    $saludDet->type_medical = $data["type_medical"][$n];
                    if (isset($data['file_medical'][$n])) {
                        $file = $data['file_medical'][$n];
                        $filename = uniqid() . '-' .$file->getClientOriginalName();
                        $extension = $file->getClientOriginalExtension();
                        $check = in_array($extension, $allowedfileExtension);
                        
                        if ($check) {
                            // $file->storeAs('public/profile', $filename);
                            $file->move($this->path, $filename);
                            $saludDet->file_medical = $filename;
                        } else {
                            return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                        }
                    }
                    $saludDet->save();
                }
            }
        }

        $profileSalud->update($data);
    }

    private function updatePerfilBeneficiarios($data) {
        $profile_id = $data['profile_id'];
        $profileBeneficiariosUpdate = ProfileBeneficiaries::where('profile_id', $profile_id)->first();
        $allowedfileExtension=['pdf','docx', 'jpeg', 'jpg', 'png'];

        if (isset($data['file_marriage'])) {
            if(!is_null($data['spouse_name'])) {
                $file = $data['file_marriage'];
                $filename = uniqid() . '-' .$file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);
                if ($check) {
                    $fileExiste = ProfileBeneficiaries::select('file_marriage')->where('profile_id', $profile_id)->first();
                    if (!is_null($fileExiste)) {
                        $exists = File::exists($this->path.$fileExiste->file_marriage);
                        if ($exists){
                            File::delete($this->path.$fileExiste->file_marriage);
                        }
                    }
                    // $file->storeAs('public/profile', $filename);
                    $file->move($this->path, $filename);
                    $data['file_marriage'] = $filename;
                } else {
                    return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                }
            }
        }

        if (isset($data['file_birth_certificate_father'])) {
            if(!is_null($data['father_name'])) {
                $file = $data['file_birth_certificate_father'];
                $filename = uniqid() . '-' .$file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);
                if ($check) {
                    $fileExiste = ProfileBeneficiaries::select('file_birth_certificate_father')->where('profile_id', $profile_id)->first();
                    if (!is_null($fileExiste)) {
                        $exists = File::exists($this->path.$fileExiste->file_birth_certificate_father);
                        if ($exists){
                            File::delete($this->path.$fileExiste->file_birth_certificate_father);
                        }
                    }
                    // $file->storeAs('public/profile', $filename);
                    $file->move($this->path, $filename);
                    $data['file_birth_certificate_father'] = $filename;
                } else {
                    return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                }
            }
        }

        if (isset($data['file_birth_certificate_mother'])) {
            if(!is_null($data['mother_name'])) {
                $file = $data['file_birth_certificate_mother'];
                $filename = uniqid() . '-' .$file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);
                if ($check) {
                    $fileExiste = ProfileBeneficiaries::select('file_birth_certificate_mother')->where('profile_id', $profile_id)->first();
                    if (!is_null($fileExiste)) {
                        $exists = File::exists($this->path.$fileExiste->file_birth_certificate_mother);
                        if ($exists){
                            File::delete($this->path.$fileExiste->file_birth_certificate_mother);
                        }
                    }
                    // $file->storeAs('public/profile', $filename);
                    $file->move($this->path, $filename);
                    $data['file_birth_certificate_mother'] = $filename;
                } else {
                    return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                }
            }
        }

        $profileBeneficiariosUpdate->update($data);
        $profile_id_det = $profileBeneficiariosUpdate->id;

        if(!is_null($data['other']['beneficiarie_name'])) {        
            if (isset($data['other']['file_birth_certificate_beneficiarie'])) {
                $file = $data['other']['file_birth_certificate_beneficiarie'];
                $filename = uniqid() . '-' .$file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);
                if ($check) {
                    $fileExiste = ProfileBeneficiariesDet::select('file_birth_certificate_beneficiarie')->where('profile_beneficiaries_id', $profile_id_det)->first();
                    if (!is_null($fileExiste)) {
                        $exists = File::exists($this->path.$fileExiste->file_birth_certificate_beneficiarie);
                        if ($exists){
                            File::delete($this->path.$fileExiste->file_birth_certificate_beneficiarie);
                        }
                    }
                    $file->move($this->path, $filename);
                    $data['other']['file_birth_certificate_beneficiarie'] = $filename;
                } else {
                    return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                }
            }
            $data['other']['profile_beneficiaries_id'] = $profile_id_det;
            $data['other']['type_beneficiarie'] = $data['other']['type_beneficiarie'];

            $perfilBeneficiariosDet = $profileBeneficiariosUpdate->perfilBeneficiariosDet()->updateOrCreate(['profile_beneficiaries_id' => $profile_id_det], $data['other']);
        }

        if(!empty($data['son']['beneficiarie_name'])) {
            $detalles = $profileBeneficiariosUpdate->perfilBeneficiariosDetHijo;
            $canActual = $profileBeneficiariosUpdate->perfilBeneficiariosDetHijo->count();
            $canRecibida = count($data['son']['beneficiarie_name']);

            //dd($detalles, $canActual, $canRecibida, $data['son']['beneficiarie_name']);

            if ($canRecibida < $canActual) {
                $tmp = [];
                foreach ($data['son']['beneficiarie_name'] as $nombre) {
                    foreach ($detalles as $detalle) {
                        if ($detalle->beneficiarie_name == $nombre && !in_array($nombre, $tmp)) {
                            $existen[$detalle->id] = $detalle->beneficiarie_name;
                            array_push($tmp, $nombre);
                        } else {
                            $borrar[$detalle->id] = $detalle->beneficiarie_name;
                        }
                    }
                }
                $eliminar = array_diff_assoc($borrar, $existen);
                foreach ($eliminar as $key => $elimina) {
                    ProfileBeneficiariesDet::where('id', $key)->delete();
                }
                $i=0;
                foreach ($existen as $key => $existe) {
                    ProfileBeneficiariesDet::where('id', $key)->update([
                        'beneficiarie_name' => $data['son']['beneficiarie_name'][$i],
                        'beneficiarie_sex' => $data['son']['beneficiarie_sex'][$i],
                        'date_birth_beneficiarie' => $data['son']['date_birth_beneficiarie'][$i],
                        'type_beneficiarie' => 'HIJO'
                    ]);
                    $i++;
                }
            } elseif ($canActual == $canRecibida) {
                foreach ($data['son']['beneficiarie_name'] as $n => $tipo) {
                    if($detalles[$n]->beneficiarie_name == $data['son']['beneficiarie_name'][$n]) {
                        ProfileBeneficiariesDet::where('id', $detalles[$n]->id)->update([
                            'beneficiarie_name' => $data['son']['beneficiarie_name'][$n],
                            'beneficiarie_sex' => $data['son']['beneficiarie_sex'][$n],
                            'date_birth_beneficiarie' => $data['son']['date_birth_beneficiarie'][$n]
                        ]);
                        if (isset($data['son']['file_birth_certificate_beneficiarie'][$n])) {
                            $file = $data['son']['file_birth_certificate_beneficiarie'][$n];
                            $filename = uniqid() . '-' .$file->getClientOriginalName();
                            $extension = $file->getClientOriginalExtension();
                            $check = in_array($extension, $allowedfileExtension);
                            
                            if ($check) {
                                $fileExiste = ProfileBeneficiariesDet::select('file_birth_certificate_beneficiarie')->where('id', $detalles[$n]->id)->first();
                                if (!is_null($fileExiste)) {
                                    $exists = File::exists($this->path.$fileExiste->file_birth_certificate_beneficiarie);
                                    if ($exists){
                                        File::delete($this->path.$fileExiste->file_birth_certificate_beneficiarie);
                                    }
                                }
                                // $file->storeAs('public/profile', $filename);
                                $file->move($this->path, $filename);
                                ProfileBeneficiariesDet::where('id', $detalles[$n]->id)->update([
                                    'file_birth_certificate_beneficiarie' => $filename,
                                ]);
                            } else {
                                return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                            }
                        }
                    }  elseif ($detalles[$n]->beneficiarie_name != $data['son']['beneficiarie_name'][$n]) {
                        ProfileBeneficiariesDet::where('id', $detalles[$n]->id)->update([
                            'beneficiarie_name' => $data['son']['beneficiarie_name'][$n],
                            'beneficiarie_sex' => $data['son']['beneficiarie_sex'][$n],
                            'date_birth_beneficiarie' => $data['son']['date_birth_beneficiarie'][$n]
                        ]);
                        if (isset($data['son']['file_birth_certificate_beneficiarie'][$n])) {
                            $file = $data['son']['file_birth_certificate_beneficiarie'][$n];
                            $filename = uniqid() . '-' .$file->getClientOriginalName();
                            $extension = $file->getClientOriginalExtension();
                            $check = in_array($extension, $allowedfileExtension);
                            
                            if ($check) {
                                $fileExiste = ProfileBeneficiariesDet::select('file_birth_certificate_beneficiarie')->where('id', $detalles[$n]->id)->first();
                                if (!is_null($fileExiste)) {
                                    $exists = File::exists($this->path.$fileExiste->file_birth_certificate_beneficiarie);
                                    if ($exists){
                                        File::delete($this->path.$fileExiste->file_birth_certificate_beneficiarie);
                                    }
                                }
                                // $file->storeAs('public/profile', $filename);
                                $file->move($this->path, $filename);
                                ProfileBeneficiariesDet::where('id', $detalles[$n]->id)->update([
                                    'file_birth_certificate_beneficiarie' => $filename,
                                ]);
                            } else {
                                return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                            }
                        }
                    } 
                }
            } elseif ($canRecibida > $canActual) {
                for ($n=0; $n < $canActual; $n++) {
                    if($detalles[$n]->beneficiarie_name == $data['son']['beneficiarie_name'][$n]) {
                        ProfileBeneficiariesDet::where('id', $detalles[$n]->id)
                        ->update([
                            'beneficiarie_name' => $data['son']['beneficiarie_name'][$n],
                            'beneficiarie_sex' => $data['son']['beneficiarie_sex'][$n],
                            'date_birth_beneficiarie' => $data['son']['date_birth_beneficiarie'][$n],
                            'type_beneficiarie' => 'HIJO',
                        ]);
                        if (isset($data['son']['file_birth_certificate_beneficiarie'][$n])) {
                            $file = $data['son']['file_birth_certificate_beneficiarie'][$n];
                            $filename = uniqid() . '-' .$file->getClientOriginalName();
                            $extension = $file->getClientOriginalExtension();
                            $check = in_array($extension, $allowedfileExtension);
                            
                            if ($check) {
                                // $file->storeAs('public/profile', $filename);
                                $file->move($this->path, $filename);
                                ProfileBeneficiariesDet::where('id', $detalles[$n]->id)->update([
                                    'file_birth_certificate_beneficiarie' => $filename,
                                ]);
                            } else {
                                return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                            }
                        }
                    }  elseif ($detalles[$n]->beneficiarie_name != $data['son']['beneficiarie_name'][$n]) {
                        ProfileBeneficiariesDet::where('id', $detalles[$n]->id)
                        ->update([
                            'beneficiarie_name' => $data['son']['beneficiarie_name'][$n],
                            'beneficiarie_sex' => $data['son']['beneficiarie_sex'][$n],
                            'date_birth_beneficiarie' => $data["school"][$n],
                            'type_beneficiarie' => 'HIJO',
                        ]);
                        if (isset($data['son']['file_birth_certificate_beneficiarie'][$n])) {
                            $file = $data['son']['file_birth_certificate_beneficiarie'][$n];
                            $filename = uniqid() . '-' .$file->getClientOriginalName();
                            $extension = $file->getClientOriginalExtension();
                            $check = in_array($extension, $allowedfileExtension);
                            
                            if ($check) {
                                // $file->storeAs('public/profile', $filename);
                                $file->move($this->path, $filename);
                                ProfileBeneficiariesDet::where('id', $detalles[$n]->id)->update([
                                    'file_birth_certificate_beneficiarie' => $filename,
                                ]);
                            } else {
                                return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                            }
                        }
                    }
                }
                for ($n=$canRecibida; $n > $canActual; $n--) {
                    $lenguaje = new ProfileBeneficiariesDet;
                    $lenguaje->profile_beneficiaries_id = $profile_id_det;
                    $lenguaje->beneficiarie_name = $data['son']['beneficiarie_name'][$n-1];
                    $lenguaje->beneficiarie_sex = $data['son']['beneficiarie_sex'][$n-1];
                    $lenguaje->date_birth_beneficiarie = $data['son']['date_birth_beneficiarie'][$n-1];
                    $lenguaje->type_beneficiarie = 'HIJO';
                    $lenguaje->save();
                }
            }
        }
    }

   private function check_valid_image_size( $file ) {
    
    $image = getimagesize($file->getPathName());

    $maximum = array(
        'width' => '600',
        'height' => '600'
    );
 
    $image_width = $image[0];
    $image_height = $image[1];
 
    $too_large = "Image dimensions are too large. Maximum size is {$maximum['width']} by {$maximum['height']} pixels. Uploaded image is $image_width by $image_height pixels.";
 
    if ( $image_width > $maximum['width'] || $image_height > $maximum['height'] ) {
        return false;
    }else {
        return true;
    }
}

}
