<?php

namespace App\Http\Controllers\Nom035;

use Illuminate\Http\Request;
use App\Models\Personal;

class PersonalController extends Controller
{
  
  // Muestra un listado con todos los empleados activos
  public function lista_usuarios(){

    if (auth()->user()->role != 'admin'){

      return redirect('/')->with('error','No tiene permiso para ver esta sección');
    }

    $employee_id = auth()->user()->employee_id;
    $employee = Personal::find($employee_id);
    $employees = Personal::where('id', '!=', $employee->id)->get();
    return view('nom035.personal.lista-personal', compact('employees'));
  }
}
