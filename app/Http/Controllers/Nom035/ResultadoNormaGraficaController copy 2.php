<?php

namespace App\Http\Controllers\Nom035;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Employee;
use App\Models\Area;
use App\Models\Sucursal;

use App\Models\Direction;
use App\Models\Department;
use App\Models\JobPosition;

use Carbon\Carbon;
use App\Models\JobPositionLevel;
use App\Models\Nom035\Answer;
use App\Models\Nom035\PeriodQuestion;
use App\Models\Nom035\Domain;
use App\Models\Nom035\Factor;
use App\Models\Nom035\NormPeriod;
use App\Models\Nom035\Category;
use App\Models\Nom035\Dimension;
use App\Models\Nom035\GroupArea;
use App\Models\Nom035\GroupJobPosition;
use DB;
use Session;
use App\Models\Generation;

class ResultadoNormaGraficaController extends Controller
{

  private function getGenerationRows($zeros = false) {
    $generations = Generation::orderBy('start','ASC')->get();
    if($generations->isEmpty()) {
        Generation::initialize();
        $generations = Generation::orderBy('start','ASC')->get()  ;
    } 
    $generation_rows = [];
    foreach($generations as $generation) {
        $generation_rows[$generation->getRangeKey()] = ['id'=>$generation->id,'nac'=>$generation->getRangeKey(),'name'=>!$zeros?$generation->name:0,'total'=>0];
    }
    return $generation_rows;
}

    private function sortArrayByKey($array,$key){
      
        $groupedItems = []; 
        $Items = []; 
        foreach ($array as $item) {
            $pool = $item[$key];
            $groupedItems[$pool][] = $item;
        }
        ksort($groupedItems);

        foreach ($groupedItems as $key => $value) {
          $Items[] = $value[0];
        }

        return $Items;
    } 

    public function reporte(Request $request){

        $clasificacion_general = array();
        $valor_total = 0;
        $color = 0;
        $num_participiantes = 0;
        $valor_general = 0;
        $resultado_general = 0;
        $color_general = "";
        $clasificacion_categorias = "";
        $clasificacion_categorias_by_usuarios = "";
        $clasificacion_dominios = "";
        $clasificacion_dimensiones = "";
        $clasificacion_dominios_by_usuarios = "";
        $clasificacion_dimensiones_by_usuarios = "";
        $dominio_seleccionado = "";
        $periodos = $users = array();
        $period_id = 0;
        $promedio_general = 0;
        $admin = true;

        if (auth()->user()->role == 'admin'){

          $periodos = NormPeriod::where('status','Abierto')->orWhere('status', 'Cerrado')->select('id', 'name', 'status')->orderBy('id', 'DESC')->get();
        
        } else{

          $permissions = auth()->user()->multiUserPermissions()->orderBy('enterprise_id')->orderBy('sucursal_id')->get();

          if (count($permissions)>0 && $permissions[0]->enterprise_id == 0){

            $periodos = NormPeriod::where('status','Abierto')->orWhere('status', 'Cerrado')->select('id', 'name', 'status')->orderBy('id', 'DESC')->get();
          
          }else{

            $admin = false;
            $enterprises_without_sucursals = $enterprises_with_sucursals = array();
            $sucursals = array();

            foreach ($permissions as $key => $value){
              
              if ($value->sucursal_id == 0){

                if (!in_array($value->enterprise_id, $enterprises_without_sucursals)){

                  $enterprises_without_sucursals[] = $value->enterprise_id;
                }
              }

              else{

                if (!in_array($value->enterprise_id, $enterprises_without_sucursals) && !in_array($value->sucursal_id, $sucursals)){

                  $enterprises_with_sucursals[] = $value->enterprise_id;
                  $sucursals[] = $value->sucursal_id;
                }
              }
            }

            $periods = array();

            if (!empty($enterprises_without_sucursals)){

              $periods = NormPeriod::whereIn('enterprise_id', $enterprises_without_sucursals)->pluck('id')->toArray();
            }

            if (!empty($sucursals)){

              foreach ($sucursals as $key => $value){
                  
                $periods2 = NormPeriod::whereIn('enterprise_id', $enterprises_with_sucursals)->whereIn('sucursal_id', $sucursals)->pluck('id')->toArray();

                if (!empty($periods2)){

                  $periods = array_merge($periods, $periods2);
                }
              }
            }

            if (!empty($periods)){

              $periodos = NormPeriod::whereIn('id', $periods)->select('id', 'name', 'status')->orderBy('id', 'DESC')->get();
            }else{
              
              $user_periods = \DB::table('nom035_period_user')->where('user_id', auth()->user()->id)->pluck('period_id')->toArray();
              $periodos = NormPeriod::where('status', 'Cerrado')->whereIn('id', $user_periods)->orderBy('id', 'DESC')->get();
              
            }

          }
          
        }

        if(!$request->periodo || Session::has('period_without_results')){
 
          if (count($periodos) > 0){

            $periodo = $periodos[0];

          }

          else{

                $sin_periodos = true;
                $name_categories = true;
                $name_dominios = true;
                $name_dimensiones = true;
                $clasificacion_categorias = [];
                $clasificacion_dominios = [];
                $clasificacion_dimensiones = [];

                return view('nom035.graficos_by.index', compact('sin_periodos', 'periodos', 'period_id',
                    'name_categories',
                    'name_dominios',
                    'name_dimensiones',
                    'clasificacion_categorias',
                    'clasificacion_dominios',
                    'clasificacion_dimensiones'
                ));

          }

            if (!$request->periodo && empty($_POST['period_id'])){
            
              $period_id = $periodo->id;
            }

            else{

              $period_id = $_POST['period_id'];
              Session::forget('period_without_results');
              $request->periodo = null;
            }

        }else{

            if($request->cambio =='periodo'){
                $request['sucursal'] = null;
                $request['direccion'] = null;
                $request['job'] = null;
                $request['sexo'] = null;
                $request['edad'] = null;
                $request['ingreso'] = null;
                $request['centro_trabajo'] = null;
                $request['regiones'] = null;
            }
            else if($request->cambio=='sucursal'){
            
                $request['direccion'] = null;
                $request['job'] = null;

          }else if($request->cambio=='area'){

                $request['job'] = null;
                $request['sexo'] = null;
                $request['edad'] = null;
                $request['ingreso'] = null;
                $request['centro_trabajo'] = null;
                $request['regiones'] = null;

          }else if($request->cambio=='edad'){

                $request['sexo'] = null;
                $request['ingreso'] = null;
                $request['centro_trabajo'] = null;
                $request['regiones'] = null;

          }else if($request->cambio=='inicio'){

                $request['sexo'] = null;
                $request['edad'] = null;
                $request['centro_trabajo'] = null;
                $request['regiones'] = null;

          }else if($request->cambio=='sexo'){

                $request['edad'] = null;
                $request['ingreso'] = null;
                $request['centro_trabajo'] = null;
                $request['regiones'] = null;
          
          }else if($request->cambio=='centro_trabajo' ){
                $request['edad'] = null;
                $request['ingreso'] = null;
                $request['sexo'] = null;
                $request['regiones'] = null;
          }else if($request->cambio=='regiones'){
                $request['edad'] = null;
                $request['ingreso'] = null;
                $request['sexo'] = null;
                $request['centro_trabajo'] = null; 
          }

        $period_id = $request->periodo;



        }



        //$sucursal = array();
        //$Areas = Area::where('department_id', $request['sucursal'])->get();
        //$sucursal = JobPosition::where('id_department', $request['sucursal'])->pluck('id')->toArray();

        /*foreach ($Areas as $key => $Area) {                
            
            $JobPositions = JobPosition::where('area_id',$Area->id)->get();

                foreach ($JobPositions as $key => $JobPosition) {                                                                       
                    $sucursal[] = $JobPosition->id;

                }

        }*/

        /*$puestos = array();
        $JobPositionLevels = JobPositionLevel::where('id',$request['job'])->get();

        foreach ($JobPositionLevels as $key => $JobPositionLevel) {   

             $JobPositions0 = JobPosition::where('job_position_level_id',$JobPositionLevel->id)->get();

            foreach ($JobPositions0 as $key => $JobPosition0) {                                                                       
               $puestos[] = $JobPosition0->id;

            }

            

        }*/


        //categorias
        $listcategory = array();
        $categories = array();
        $id_cuestionario = null;
        $PeriodQuestion = PeriodQuestion::with('question')->where('period_id',$period_id)->get();
        foreach ($PeriodQuestion as $key => $PeriodQ) {
            if(!in_array($PeriodQ->question->category_id, $listcategory)){
                $listcategory[] = $PeriodQ->question->category_id;
                if(isset($PeriodQ->question->category->name)){
                     $categories[] = ['id'=>$PeriodQ->question->category->id, 'name'=>$PeriodQ->question->category->name,'descriptions'=>$PeriodQ->question->category->descriptions];
                     $id_cuestionario = $PeriodQ->question->cuestionario;
                }
            }
        }
        $name_categories = $categories;
        //categorias



        //dominios
        $listcategory = array();
        $dominios = array();
        $id_cuestionario = null;
        $PeriodQuestion = PeriodQuestion::with('question')->where('period_id',$period_id)->get();
        foreach ($PeriodQuestion as $key => $PeriodQ) {
            if(!in_array($PeriodQ->question->domain_id, $listcategory)){
                $listcategory[] = $PeriodQ->question->domain_id;
                if(isset($PeriodQ->question->domain->name)){
                     $dominios[] = ['id'=>$PeriodQ->question->domain->id, 'name'=>$PeriodQ->question->domain->name,'descriptions'=>$PeriodQ->question->domain->descriptions,'category'=>$PeriodQ->question->domain->category->name,'category_id'=>$PeriodQ->question->domain->category->id];
                     $id_cuestionario = $PeriodQ->question->cuestionario;
                }
            }
        }
        $name_dominios = $dominios;
        
        $name_dominios = $this->sortArrayByKey($name_dominios, 'id');
        //dominios


        //dimensioness        
        $listcategory = array();
        $dimensiones = array();
        $id_cuestionario = null;
        $PeriodQuestion = PeriodQuestion::with('question')->where('period_id',$period_id)->get();
        foreach ($PeriodQuestion as $key => $PeriodQ) {
            if(!in_array($PeriodQ->question->dimension_id, $listcategory)){
                $listcategory[] = $PeriodQ->question->dimension_id;
                if(isset($PeriodQ->question->dimension->name)){
                     $dimensiones[] = ['id'=>$PeriodQ->question->dimension->id, 'name'=>$PeriodQ->question->dimension->name,'descriptions'=>$PeriodQ->question->dimension->descriptions,'category'=>$PeriodQ->question->dimension->domain->category->name,'domain'=>$PeriodQ->question->dimension->domain->name,'domain_id'=>$PeriodQ->question->dimension->domain->id];
                     $id_cuestionario = $PeriodQ->question->cuestionario;
                }
            }
        }
        $name_dimensiones = $dimensiones;
        //dimensiones

        $questions = DB::table('nom035_questions')->where('cuestionario', 'Cuestionario 2')->orWhere('cuestionario', 'Cuestionario 3')->pluck('id')->toArray();
        $respuestas = array();

        if ($admin){

          $respuestas = Answer::groupBy('user_id')
          ->where('period_id', $period_id)
          ->whereIn('question_id', $questions)
          ->pluck('user_id')->toArray();
        }

        else{

          $respuestas = Answer::groupBy('user_id')
          ->where('period_id', $period_id)
          ->whereIn('question_id', $questions)
          // ->whereIn('user_id', $users)
          ->pluck('user_id')->toArray();
        }

        $today = date('Y-m-d');
        $fecha_actual = date('Y-m-d');
        $end_date = $start_date = $age_start_date = $age_end_date = 0;

        if (!is_null($request->edad) || !is_null($request->ingreso)){

          if (!is_null($request->ingreso)){

          $antiquity = $request->ingreso;
 
            if($antiquity =='less_than_a_year') {
                $end_date = date('Y-m-d'); 
                $start_date = date("Y-m-d",strtotime($fecha_actual."- 365 days"));              
            }
            else if($antiquity =='one_to_five_years') {    
              $end_date = date("Y-m-d",strtotime($fecha_actual."- 366 days"));  
              $start_date = date("Y-m-d",strtotime($fecha_actual."- 1825 days"));   
            } else if($antiquity =='five_to_ten_years') {
              $end_date = date("Y-m-d",strtotime($fecha_actual."- 1826 days"));  
              $start_date = date("Y-m-d",strtotime($fecha_actual."- 3650 days"));   
            } else if($antiquity =='ten_to_fifteen_years') {
              $end_date = date("Y-m-d",strtotime($fecha_actual."- 3650 days"));  
              $start_date = date("Y-m-d",strtotime($fecha_actual."- 7300 days")); 
            }


            // $end_date = date('Y-m-d', strtotime($today . ' - ' . $request->ingreso . ' years'));
            // $temp = $request->ingreso + 1;
            // $start_date = date('Y-m-d', strtotime($today . ' - ' . $temp . ' years'));
            // $start_date = date('Y-m-d', strtotime($start_date . ' + 1 days'));
            
          }

          if (!is_null($request->edad)){
            
            $gen = Generation::whereId($request->edad)->first();
            if(!is_null($gen->end)){
              $age_end_date = $gen->end.'-12-31';
            }else{
              $age_end_date = date('Y-m-d');
            }
             
            $age_start_date = $gen->start.'-01-31';

          }

        }


          

        // dd($respuestas);
        $users = User::
        whereIn('id', $respuestas)
        /*->whereHas('employee', function($q1) use($request,$sucursal,$puestos){
            $q1->when(!is_null($request->sucursal), function($q2) use($request,$sucursal){
                $q2->whereIn('job_position_id', $sucursal);
            });
            $q1->when(!is_null($request->direccion), function($q2) use($request){
                $q2->where('direccion', $request->direccion);
            });
            $q1->when(!is_null($request->job), function($q2) use($request,$puestos){
                $q2->whereIn('job_position_id', $puestos);
            });
        })*/


        ->whereHas('employee_wt', function($q1) use($request,$start_date,$end_date,$age_start_date,$age_end_date){
            $q1->when(!is_null($request->sucursal), function($q2) use($request){
                $q2->whereHas('jobPosition', function($q3) use($request){
                  $q3->whereHas('area', function($q4) use($request){
                    $q4->where('department_id', $request->sucursal);
                  });
                });
            });
            $q1->when(!is_null($request->direccion), function($q2) use($request){
                $q2->whereHas('jobPosition', function($q3) use($request){
                  $q3->whereHas('area', function($q4) use($request){
                    $q4->whereHas('department', function($q5) use($request){
                      $q5->where('direction_id', $request->direccion);
                    });
                  });
                });
            });
            $q1->when(!is_null($request->job), function($q2) use($request){
                $q2->where('job_position_id', $request->job);
            });
            $q1->when(!is_null($request->sexo), function($q2) use($request){
                $q2->where('sexo', $request->sexo);
            });
            $q1->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
                $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
            });
            $q1->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
                $q2->whereBetween('ingreso', [$start_date, $end_date]);
            });
            $q1->when(!is_null($request->centro_trabajo), function($q2) use($request){
                $q2->where('sucursal', $request->centro_trabajo);
            });
            $q1->when(!is_null($request->regiones), function($q2) use($request){
                $q2->where('region_id', $request->regiones);
            });
        })
        //->with('employee_wt:id,sucursal,direccion,job_position_id')
        ->get();
        
      $xusers = [];
      foreach ($users as $key => $user) {
        # code...
        $cerrado = \DB::table('nom035_period_user')
        ->where('period_id', $period_id)
        ->whereIn('status',[3, 33])
        ->whereIn('user_id', [$user->id])
        ->count();

        if($cerrado>0){
            $xusers[] = $user->id;
        }
      }

      $users = User::whereIn('id', $xusers)->get();
        

        $sucursales = [];
        $sucursaless = [];
        $directions = [];
        $directionss = [];
        $puestos_trabajo = [];
        $jobs = [];
        $temp_sexs = [];
        $sexs = [];
        $temp_ages = [];
        $ages = [];
        $temp_starts = [];
        $starts = [];
        $year_in_seconds = 365*60*60*24;
        $current_year = substr($today, 0, 4) * 1;
        $current_month = substr($today, 5, 2) * 1;
        $current_day = substr($today, 8, 2) * 1;
        $centros_trabajo = [];
        $regiones = [];
        $temp_centros_trabajo = [];
        $temp_regiones = [];

        foreach ($users as $key => $user) {

             if(isset($user->employee_wt->jobPosition->area->department) && !in_array($user->employee_wt->jobPosition->area->department_id, $sucursaless)){ 
                $sucursaless[] = $user->employee_wt->jobPosition->area->department_id; 
                $sucursales[] = ['id'=>$user->employee_wt->jobPosition->area->department_id,'nombre'=>$user->employee_wt->jobPosition->area->department->name]; 
            }

            if (isset($user->employee_wt->jobPosition->area->department->direction)){
              $direction = $user->employee_wt->jobPosition->area->department->direction;
              if(!in_array($direction->id, $directionss)){ 
                $directionss[] = $direction->id;
                $directions[] = ['id'=>$direction->id,'nombre'=>$direction->name]; 
              }
            }

            $job = $user->employee_wt->jobPosition ?? null;
            if($job && !in_array($job->id, $puestos_trabajo)){ 
                
                $puestos_trabajo[] = $job->id; 
        
            }

            if(!in_array($user->employee->sexo, $temp_sexs)){

              if ($user->employee->sexo == 'M'){
                
                $sexs[] = ['id'=>'M','nombre'=>'Masculino'];
              }

              else{

                if ($user->employee->sexo == 'F'){

                  $sexs[] = ['id'=>'F','nombre'=>'Femenino'];
                }
              }

              $temp_sexs[] = $user->employee->sexo;
            }

            if (!empty($user->employee->ingreso)){

              $years = $current_year - substr($user->employee->ingreso, 0, 4) * 1;

              if ($years > 0){

                $month = substr($user->employee->ingreso, 5, 2) * 1;

                if ($current_month < $month){

                  $years--;
                }

                else{

                  if ($current_month == $month){

                    $day = substr($user->employee->ingreso, 8, 2) * 1;

                    if ($current_day < $day){

                      $years--;
                    }
                  }
                }
              }

            }

            if (!empty($user->employee->nacimiento)){

              $years = $current_year - substr($user->employee->nacimiento, 0, 4) * 1;

              if ($years > 0){

                $month = substr($user->employee->nacimiento, 5, 2) * 1;

                if ($current_month < $month){

                  $years--;
                }

                else{

                  if ($current_month == $month){

                    $day = substr($user->employee->nacimiento, 8, 2) * 1;

                    if ($current_day < $day){

                      $years--;
                    }
                  }
                }
              }

            }

            if(!empty($user->employee->sucursal) && !in_array($user->employee->sucursal, $temp_centros_trabajo)){

              $temp_centros_trabajo[] = $user->employee->sucursal;
              $centros_trabajo[] = ['id'=>$user->employee->sucursal,'nombre'=>$user->employee->sucursal];
            }

            if(!empty($user->employee->region_id) && !in_array($user->employee->region_id, $temp_regiones)){

              $temp_regiones[] = $user->employee->region_id;
              $regiones[] = ['id'=>$user->employee->region->id,'nombre'=>$user->employee->region->name];
            }

        }
       
        
        $regiones = $this->sortArrayByKey($regiones, 'nombre');
        $centros_trabajo = $this->sortArrayByKey($centros_trabajo, 'nombre');
        $sucursales = $this->sortArrayByKey($sucursales, 'nombre');
        $directions = $this->sortArrayByKey($directions, 'nombre');
   
      //   $order_by = 'nombre';
      //   $orderby = usort($regiones, function ($a, $b) use ($order_by)
      // {
      //     return strcmp($a->{$order_by}, $b->{$order_by});
      // });
  
        


        foreach ($puestos_trabajo as $key => $value) {
            # code...
            $jobs[] = JobPosition::where('id',$value)->select('name','id')->first();
        }
         
        $jobs = $this->sortArrayByKey($jobs, 'name');
 


        if(count($users)<1){

            $sin_data = true;

            if(!is_null($request->sucursal) || !is_null($request->direccion) ||  !is_null($request->job) || !is_null($request->edad) || !is_null($request->sexo) || !is_null($request->ingreso) || !is_null($request->centro_trabajo)|| !is_null($request->regiones) || !is_null($request->periodo)){



                return response()->json([
	                'sucursales' => $sucursales,
	                'directions' => $directions,
	                'jobs' => $jobs,
                  'sexs' => $sexs,
                  'ages' => $ages,
                  'starts' => $starts,
                  'centros_trabajo' => $centros_trabajo,
                	'sin_data' => $sin_data,
                	//general           
                	'clasificacion_general' => $clasificacion_general,
                	'resultado_general' => $resultado_general,
                	'color_general' => $color_general,
                	//categorias                	
                    'clasificacion_categorias' => $clasificacion_categorias,
                    'clasificacion_categorias_by_usuarios' => $clasificacion_categorias_by_usuarios,
                    'name_categories' => $name_categories,
	            	//dominios
	            	'clasificacion_dominios' => $clasificacion_dominios, 
	                'clasificacion_dominios_by_usuarios' => $clasificacion_dominios_by_usuarios, 
	                'name_dominios' => $name_dominios,
	                //dimensiones	                
                    'clasificacion_dimensiones' => $clasificacion_dimensiones,  
                    'name_dimensiones' => $name_dimensiones,
	            ], 200);

            }else{
                
                $sin_periodos = true;
                $name_categories = true;
                $name_dominios = true;
                $name_dimensiones = true;
                $clasificacion_categorias = true;
                $clasificacion_dominios = true;
                $clasificacion_dimensiones = true;
                Session::put('period_without_results', 1);

                return view('nom035.graficos_by.index', compact('sin_periodos', 'periodos', 'period_id',
                    'name_categories',
                    'name_dominios',
                    'name_dimensiones',
                    'clasificacion_categorias',
                    'clasificacion_dominios',
                    'clasificacion_dimensiones'
                ));

            }

        }

        //reporte General
        $num_participiantes = 0;
        $total_participiantes = [];

             
        foreach($users as $key => $user){

            $personal = $user->first_name.' '.$user->last_name;

            if(!in_array($user->id, $total_participiantes)){
                $total_participiantes[] = $user->id;
                if ($key>0) {
                    $num_participiantes++;
                }
            }

            $clasificacion_general[$num_participiantes] = [$personal];

                $respuestas = Answer::with('question')
                ->where('user_id', $user->id)
                ->where('period_id', $period_id)
                ->whereHas('user.employee_wt', function($q1) use($request,$start_date,$end_date,$age_start_date,$age_end_date){
                  $q1->when(!is_null($request->sucursal), function($q2) use($request){
                    $q2->whereHas('jobPosition', function($q3) use($request){
                      $q3->whereHas('area', function($q4) use($request){
                        $q4->where('department_id', $request->sucursal);
                      });
                    }); 
                  });
                  $q1->when(!is_null($request->direccion), function($q2) use($request){
                    $q2->whereHas('jobPosition', function($q3) use($request){
                      $q3->whereHas('area', function($q4) use($request){
                        $q4->whereHas('department', function($q5) use($request){
                          $q5->where('direction_id', $request->direccion);
                        });
                      });
                    });
                  });
                  $q1->when(!is_null($request->job), function($q2) use($request){
                    $q2->where('job_position_id', $request->job);
                  });
                  $q1->when(!is_null($request->sexo), function($q2) use($request){
                    $q2->where('sexo', $request->sexo);
                  });
                  $q1->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
                    $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
                  });
                  $q1->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
                    $q2->whereBetween('ingreso', [$start_date, $end_date]);
                  });
                  $q1->when(!is_null($request->centro_trabajo), function($q2) use($request){
                    $q2->where('sucursal', $request->centro_trabajo);
                  });
                  $q1->when(!is_null($request->regiones), function($q2) use($request){
                      $q2->where('region_id', $request->regiones);
                  });
                })
                ->get();


                foreach ($respuestas as $value) {

                    $valor = $this->valorespuesta($value->answer , $value->question->positive);
                    $valor_total += $this->valorespuesta($value->answer , $value->question->positive);

                }

                $clasificacion_general[$num_participiantes]['valor'] = $valor_total;

                $color = $this->setColorGeneral($valor_total, $id_cuestionario);
                $clasificacion_general[$num_participiantes]['color'] = $color;
                
                $valor_total = 0;
            
        }

        $num_participiantes = 0;

        foreach ($clasificacion_general as $key => $value) {

            $num_participiantes++;
            $valor_general += $value['valor'];

        }

        $resultado_general = $valor_general / $num_participiantes;
        $color_general = $this->setColorGeneral($resultado_general, $id_cuestionario);

        //fin reporte General
        //fin reporte General
        //fin reporte General



        //reporte categoerias
        //reporte categoerias
        //reporte categoerias


        //categorias
        //categorias
        //categorias
        $valores = array();
        $clasificacion_categorias_by_usuarios = array();
        $clasificacion_categorias = array();
        $total_participiantes = array();

        $valor_total_Cat = 0;
        $num_participiantes = 0;
        $promedio = 0;
        $valor_total = 0;

        foreach($categories as $key => $category){

            $category_id = $category['id'];
            $category_name = $category['name'];
            $category_name_short = $category['descriptions'];

            $respuestas = Answer::with('question')            
            ->whereHas('period', function($q) use($period_id){
                $q->where('id', $period_id);
            }) 

            ->whereHas('question', function($q) use ($category_id){
                $q->where('category_id', $category_id);
            })
            ->whereHas('user.employee_wt', function($q) use($request,$start_date,$end_date,$age_start_date,$age_end_date){
            $q->when(!is_null($request->sucursal), function($q2) use($request){
                $q2->whereHas('jobPosition', function($q3) use($request){
                  $q3->whereHas('area', function($q4) use($request){
                    $q4->where('department_id', $request->sucursal);
                  });
                }); 
            });
            $q->when(!is_null($request->direccion), function($q2) use($request){
                $q2->whereHas('jobPosition', function($q3) use($request){
                  $q3->whereHas('area', function($q4) use($request){
                    $q4->whereHas('department', function($q5) use($request){
                      $q5->where('direction_id', $request->direccion);
                    });
                  });
                });
            });
            $q->when(!is_null($request->job), function($q2) use($request){
                $q2->where('job_position_id', $request->job);
            });
            $q->when(!is_null($request->sexo), function($q2) use($request){
                $q2->where('sexo', $request->sexo);
            });
            $q->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
                $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
            });
            $q->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
                $q2->whereBetween('ingreso', [$start_date, $end_date]);
            });
            $q->when(!is_null($request->centro_trabajo), function($q2) use($request){
              $q2->where('sucursal', $request->centro_trabajo);
            });
            $q->when(!is_null($request->regiones), function($q2) use($request){
                $q2->where('region_id', $request->regiones);
            });
          })
          ->get();   
          
          
            if (count($respuestas)>0) {
              # code...
            foreach ($respuestas as $value) {
                
                $cerrado = \DB::table('nom035_period_user')
                ->where('period_id', $period_id)
                ->whereIn('status',[3, 33])
                ->whereIn('user_id', [$value->user_id])
                ->count();
            
                if(!in_array($value->user_id, $total_participiantes) && $cerrado>0){
                    $total_participiantes[] = $value->user_id;
                    $num_participiantes++;
                }

                $valor_total_Cat += $this->valorespuesta($value->answer , $value->question->positive);
                
                $clasificacion_categorias[$key]['name'] = $category_name;// $respuestas;
                $clasificacion_categorias[$key]['category_name_short'] = $category_name_short;// $respuestas;
                $clasificacion_categorias[$key]['id'] = $category_id;// $respuestas;

            }

          }else{
            $clasificacion_categorias[$key]['category_name_short'] = $category_name_short;// $respuestas;
            $clasificacion_categorias[$key]['name'] = $category_name;// $respuestas;
            $clasificacion_categorias[$key]['id'] = $category_id;// $respuestas;
          }



            if($num_participiantes>0){
                $promedio = $valor_total_Cat/$num_participiantes;
            }else {$promedio=0;}
            $setPromedio = $this->setPromedio($category_name, $promedio,$id_cuestionario);
            $color = $this->setColor($category_name, $promedio, $id_cuestionario);
            $critico = $this->setCritico($category_name, $promedio, $id_cuestionario);
            $promedio_general += $setPromedio;
            $clasificacion_categorias[$key]['data_y'] = intval( $setPromedio);// $respuestas;
            $clasificacion_categorias[$key]['data'] = [$setPromedio];// $respuestas;
            $clasificacion_categorias[$key]['critico'] = [$promedio];// $respuestas;
            $clasificacion_categorias[$key]['color'] = $critico;// $respuestas;
            $clasificacion_categorias[$key]['colors'] = $color;// $respuestas;

            $valor_total_Cat = 0;


        } 
        $num_participiantes = 0;
        $total_participiantes = [];

        $tables_cat = $tables_dom = $tables_dim = [
          'sex'=>[
              'name' => 'Sexo',
              'key' => 'sex',
              'headers' => [
                'Sexo','Totales'
              ],
              'rows' => [
                  'women' => ['name' => 'Mujeres','total' => 0],
                  'man' => ['name' => 'Hombres','total' => 0],
              ]
          ]
          ,
          'antiquity'=>[
              'name' => 'Antigüedad',
              'key' => 'antiquity',
              'headers' => [
                'Antigüedad','Personas'
              ],
              'rows' => [
                  'less_than_a_year' => ['name' => 'A.- Un año o menos','total' => 0],
                  'one_to_five_years' => ['name' => 'B.-Un año un día a cinco años','total' => 0],
                  'five_to_ten_years' => ['name' => 'C.-Cinco años un día a diez años','total' => 0],
                  'ten_to_fifteen_years' => ['name' => 'D.-Diez años un día y más','total' => 0], 
              ]
          ], 
          'generations' => [
              'name' => 'Generación',
              'headers' => [
                'Nacimiento','Generación','Totales'
              ],
              'key' => 'generations',
              'rows' => $this->getGenerationRows()
          ],
          'turnos'=>[
            'name' => 'Turnos',
            'key' => 'turnos',
            'headers' => [
              'Turnos','Personas'
            ],
            'rows' => [
                'diurno' => ['name' => 'Diurno','total' => 0],
                'vespertino' => ['name' => 'Vespertino','total' => 0],
                'nocturno' => ['name' => 'Nocturno','total' => 0],
                'matutino' => ['name' => 'Matutino','total' => 0],
            ]
        ],
      ];
      // dd($tables_cat);
      $years_key = '';
      $real_now = Carbon::now();
        foreach($users as $key => $user){

            if(isset($user->employee_wt)){
              if($user->employee_wt->sexo == 'M'){
                $tables_cat['sex']['rows']['man']['total'] += 1;
              }else{
                $tables_cat['sex']['rows']['women']['total'] += 1;
              }
            } 

            
          $entry = Carbon::parse($user->employee_wt->ingreso);
          $birthday = Carbon::parse($user->employee_wt->nacimiento);
          $antiquity = $real_now->diffInDays($entry)/365;
          
          
        
          if($antiquity < 1) {
              $tables_cat['antiquity']['rows']  ['less_than_a_year']['total'] += 1; 
              $years_key = 'less_than_a_year';      
              $years_name = 'A. Un año o menos';    
          }
           elseif($antiquity >= 1 && $antiquity <= 5) {    
            $tables_cat['antiquity']['rows']['one_to_five_years']['total'] += 1;   
            $years_key = 'one_to_five_years';      
            $years_name = 'B. Un año un día a cinco años';      
              // $analytics['antiquity'][$employee->sexo]['one_to_five_years'] += 1;
          } elseif($antiquity > 5 && $antiquity <= 10) {
            $tables_cat['antiquity']['rows']['five_to_ten_years']['total'] += 1; 
            $years_key = 'five_to_ten_years';      
            $years_name = 'C. Cinco años un día a diez años';    
              // $analytics['antiquity'][$employee->sexo]['five_to_ten_years'] += 1;
          } elseif($antiquity > 10) {
            $tables_cat['antiquity']['rows']['ten_to_fifteen_years']['total'] += 1; 
            $years_key = 'ten_to_fifteen_years';      
            $years_name = 'D. Diez años un día y más';    
              // $analytics['antiquity'][$employee->sexo]['ten_to_fifteen_years'] += 1;
          }
          
      
            if(!in_array($years_key, $temp_starts)){            
              $starts[] = ['id'=>$years_key,'nombre'=>$years_name];
              $temp_starts[] = $years_key;
            }
            
              $starts = $this->sortArrayByKey($starts, 'nombre');

              // // Generation
              $generaciones = $this->getGenerationRows();
              $generation_row = Generation::getRangeKeyByBirthDay($birthday->year);
              // dd($birthday->year, $generation_row);
              if($generation_row) {
                  // $analytics_detail['generations'][$employee->sexo][$generation_row][] = $employee;
                  $tables_cat['generations']['rows'][$generation_row]['total']   += 1;
                  if(!in_array($generation_row, $temp_ages)){
                  
                  $ages[] = [
                    'id'=>$generaciones[$generation_row]['id'],
                    'nombre'=>$generaciones[$generation_row]['name'],
                    'edad'=>$generation_row
                  ];
                  $temp_ages[] = $generation_row;
                }

              // dd($ages);
                }
            
            $ages = $this->sortArrayByKey($ages, 'edad');
 
            $personal = $user->first_name.' '.$user->last_name;
            $department = (isset($user->employee_wt->jobPosition->area->department) ? $user->employee_wt->jobPosition->area->department->name : ' ');
            $area = (isset($user->employee_wt->jobPosition->area->department->direction) ? $user->employee_wt->jobPosition->area->department->direction->name : ' ');
            $jobPosition = $user->employee_wt->jobPosition->name;
            $sucursal = (!empty($user->employee_wt->sucursal) ? $user->employee_wt->sucursal : ' ');
            $empresa = (!empty($user->employee_wt->enterprise) ? $user->employee_wt->enterprise->name : ' ');
            $personal = $personal . '*' . $department . '#' . $area . '=' . $jobPosition . '$' . $sucursal . '[' . $empresa;

            if(!in_array($user->id, $total_participiantes)){
                $total_participiantes[] = $user->id;
                if ($key>0) {
                    $num_participiantes++;
                }
            }

            //sexo
  
            $clasificacion_categorias_by_usuarios[$num_participiantes] = [$personal];

            foreach($categories as $key => $category){

                $category_id = $category['id'];
                $category_name = $category['name'];

                $respuestas = Answer::with('question')
                ->where('user_id', $user->id)
                ->whereHas('period', function($q) use($period_id){
                    $q->where('id', $period_id);
                })
                ->whereHas('question', function($q) use ($category_id){
                    $q->where('category_id', $category_id);
                })
                ->whereHas('user.employee_wt', function($q) use($request,$start_date,$end_date,$age_start_date,$age_end_date){
                  $q->when(!is_null($request->sucursal), function($q2) use($request){
                    $q2->whereHas('jobPosition', function($q3) use($request){
                      $q3->whereHas('area', function($q4) use($request){
                        $q4->where('department_id', $request->sucursal);
                      });
                    }); 
                  });
                  $q->when(!is_null($request->direccion), function($q2) use($request){
                    $q2->whereHas('jobPosition', function($q3) use($request){
                      $q3->whereHas('area', function($q4) use($request){
                        $q4->whereHas('department', function($q5) use($request){
                          $q5->where('direction_id', $request->direccion);
                        });
                      });
                    });
                  });
                  $q->when(!is_null($request->job), function($q2) use($request){
                    $q2->where('job_position_id', $request->job);
                  });
                  $q->when(!is_null($request->sexo), function($q2) use($request){
                    $q2->where('sexo', $request->sexo);
                  });
                  $q->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
                    $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
                  });
                  $q->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
                    $q2->whereBetween('ingreso', [$start_date, $end_date]);
                  });
                  $q->when(!is_null($request->centro_trabajo), function($q2) use($request){
                    $q2->where('sucursal', $request->centro_trabajo);
                  });
                  $q->when(!is_null($request->regiones), function($q2) use($request){
                      $q2->where('region_id', $request->regiones);
                  });
                })
                ->get();


                foreach ($respuestas as $kys => $value) {

                    $valor = $this->valorespuesta($value->answer , $value->question->positive);
                    $valor_total += $this->valorespuesta($value->answer , $value->question->positive);
                   
                }

                $clasificacion_categorias_by_usuarios[$num_participiantes][$key+1]['valor'] = $valor_total;

                $color = $this->setColor($category_name, $valor_total, $id_cuestionario);
                $clasificacion_categorias_by_usuarios[$num_participiantes][$key+1]['color'] = $color;
                
                $valor_total = 0;
            }

            
        }
        
        //fin reporte categorias
        //fin reporte categorias
        //fin reporte categorias
        //fin reporte categorias






        //dominios

        $valores = array();
        $clasificacion_dominios_by_usuarios = array();
        $clasificacion_dominios = array();
        $total_participiantes = array();
        $valor_total_Cat = 0;
        $num_participiantes = 0;
        $promedio = 0;
        $valor_total = 0;
        $colores_dominios = array();

        foreach($dominios as $key => $category){

            $category_id = $category['id'];
            $category_name = $category['name'];
            $category_name_short = $category['descriptions'];
            $category_real_name = $category['category'];
            $category_real_id = $category['category_id'];

            $respuestas = Answer::with('question')            
            ->whereHas('period', function($q) use($period_id){
                $q->where('id', $period_id);
            })
            ->whereHas('question', function($q) use ($category_id){
                $q->where('domain_id', $category_id);
            })
            ->whereHas('user.employee_wt', function($q) use($request,$start_date,$end_date,$age_start_date,$age_end_date){
                  $q->when(!is_null($request->sucursal), function($q2) use($request){
                    $q2->whereHas('jobPosition', function($q3) use($request){
                      $q3->whereHas('area', function($q4) use($request){
                        $q4->where('department_id', $request->sucursal);
                      });
                    }); 
                  });
                  $q->when(!is_null($request->direccion), function($q2) use($request){
                    $q2->whereHas('jobPosition', function($q3) use($request){
                      $q3->whereHas('area', function($q4) use($request){
                        $q4->whereHas('department', function($q5) use($request){
                          $q5->where('direction_id', $request->direccion);
                        });
                      });
                    });
                  });
                  $q->when(!is_null($request->job), function($q2) use($request){
                    $q2->where('job_position_id', $request->job);
                  });
                  $q->when(!is_null($request->sexo), function($q2) use($request){
                    $q2->where('sexo', $request->sexo);
                  });
                  $q->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
                    $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
                  });
                  $q->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
                    $q2->whereBetween('ingreso', [$start_date, $end_date]);
                  });
                  $q->when(!is_null($request->centro_trabajo), function($q2) use($request){
                    $q2->where('sucursal', $request->centro_trabajo);
                  });
                  $q->when(!is_null($request->regiones), function($q2) use($request){
                      $q2->where('region_id', $request->regiones);
                  });
            })
            ->get();            

            foreach ($respuestas as $value) {

              $cerrado = \DB::table('nom035_period_user')
              ->where('period_id', $period_id)
              ->whereIn('status',[3, 33])
              ->whereIn('user_id', [$value->user_id])
              ->count();
          
              if(!in_array($value->user_id, $total_participiantes) && $cerrado>0){
              // if(!in_array($value->user_id, $total_participiantes)){
                    $total_participiantes[] = $value->user_id;
                    $num_participiantes++;
                }

                $valor_total_Cat += $this->valorespuesta($value->answer , $value->question->positive);
                
                $clasificacion_dominios[$key]['id'] = $category_id;// $respuestas;
                $clasificacion_dominios[$key]['name'] = $category_name;// $respuestas;
                $clasificacion_dominios[$key]['name_short'] = $category_name_short;// $respuestas;
                $clasificacion_dominios[$key]['category'] = $category_real_name;
                $clasificacion_dominios[$key]['category_id'] = $category_real_id;

            }

            if($num_participiantes>0){
                $promedio = $valor_total_Cat/$num_participiantes;
            }else {$promedio=0;}
            

            $setPromedio = $this->setPromedio($category_name, $promedio,$id_cuestionario);
            $color = $this->setColor($category_name, $promedio, $id_cuestionario);
            $critico = $this->setCritico($category_name, $promedio, $id_cuestionario);

            $clasificacion_dominios[$key]['data_y'] = intval( $setPromedio);// $respuestas;
            $clasificacion_dominios[$key]['data'] = [$setPromedio];// $respuestas;
            $clasificacion_dominios[$key]['critico'] = [$promedio];// $respuestas;
            $clasificacion_dominios[$key]['color'] = $critico;// $respuestas;
            $clasificacion_dominios[$key]['colors'] = $color;// $respuestas;
            $colores_dominios[$category_id] = $color;
            $valor_total_Cat = 0;


        }


        // dd($clasificacion_dominios);

        $num_participiantes = 0;
        $total_participiantes = [];

             
        foreach($users as $key => $user){

            $personal = $user->first_name.' '.$user->last_name;
            $department = (isset($user->employee_wt->jobPosition->area->department) ? $user->employee_wt->jobPosition->area->department->name : ' ');
            $area = (isset($user->employee_wt->jobPosition->area->department->direction) ? $user->employee_wt->jobPosition->area->department->direction->name : ' ');
            $jobPosition = $user->employee_wt->jobPosition->name;
            $sucursal = (!empty($user->employee_wt->sucursal) ? $user->employee_wt->sucursal : ' ');
            $empresa = (!empty($user->employee_wt->enterprise) ? $user->employee_wt->enterprise->name : ' ');
            $personal = $personal . '*' . $department . '#' . $area . '=' . $jobPosition . '$' . $sucursal . '[' . $empresa;

            if(!in_array($user->id, $total_participiantes)){
                    $total_participiantes[] = $user->id;
                    if ($key>0) {
                        $num_participiantes++;
                    }
                }


            $clasificacion_dominios_by_usuarios[$num_participiantes] = [$personal];

            foreach($dominios as $key => $category){

                $category_id = $category['id'];
                $category_name = $category['name'];

                $respuestas = Answer::with('question')
                ->where('user_id', $user->id)
                ->whereHas('period', function($q) use($period_id){
                    $q->where('id', $period_id);
                })
                ->whereHas('question', function($q) use ($category_id){
                    $q->where('domain_id', $category_id);
                })
                ->whereHas('user.employee_wt', function($q) use($request,$start_date,$end_date,$age_start_date,$age_end_date){
                  $q->when(!is_null($request->sucursal), function($q2) use($request){
                    $q2->whereHas('jobPosition', function($q3) use($request){
                      $q3->whereHas('area', function($q4) use($request){
                        $q4->where('department_id', $request->sucursal);
                      });
                    }); 
                  });
                  $q->when(!is_null($request->direccion), function($q2) use($request){
                    $q2->whereHas('jobPosition', function($q3) use($request){
                      $q3->whereHas('area', function($q4) use($request){
                        $q4->whereHas('department', function($q5) use($request){
                          $q5->where('direction_id', $request->direccion);
                        });
                      });
                    });
                  });
                  $q->when(!is_null($request->job), function($q2) use($request){
                    $q2->where('job_position_id', $request->job);
                  });
                  $q->when(!is_null($request->sexo), function($q2) use($request){
                    $q2->where('sexo', $request->sexo);
                  });
                  $q->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
                    $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
                  });
                  $q->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
                    $q2->whereBetween('ingreso', [$start_date, $end_date]);
                  });
                  $q->when(!is_null($request->centro_trabajo), function($q2) use($request){
                    $q2->where('sucursal', $request->centro_trabajo);
                  });
                  $q->when(!is_null($request->regiones), function($q2) use($request){
                      $q2->where('region_id', $request->regiones);
                  });
                })
                ->get();


                foreach ($respuestas as $value) {

                    $valor = $this->valorespuesta($value->answer , $value->question->positive);
                    $valor_total += $this->valorespuesta($value->answer , $value->question->positive);

                }

                $clasificacion_dominios_by_usuarios[$num_participiantes][$key+1]['valor'] = $valor_total;

                $color = $this->setColor($category_name, $valor_total, $id_cuestionario);
                $clasificacion_dominios_by_usuarios[$num_participiantes][$key+1]['color'] = $color;
                
                $valor_total = 0;
            }

            
        }
        
       //fin reporte dominios
        //fin reporte dominios
        //fin reporte dominios
        //fin reporte dominios


        //dimensiones
        //dimensiones
        //dimensiones
        $valores = array();
        $clasificacion_dimensiones_by_usuarios = array();
        $clasificacion_dimensiones = array();
        $total_participiantes = array();

        $valor_total_Cat = 0;
        $num_participiantes = 0;
        $promedio = 0;
        $valor_total = 0;
        $keyss = 0;

        $filtro_dominio = $dominios[0]['id'];
        $dominio_seleccionado = $dominios[0]['name'];
        // $filtro_dominio = $first_dominio->;
        foreach($dimensiones as $keyds => $category){

            $set_color = '#000';
            $category_id = $category['id'];
            $category_name = $category['name'];
            $category_real_name = $category['category'];
            $category_name_short = $category['descriptions'];
            $domain_name = $category['domain'];
            $domain_id = $category['domain_id'];

          // if(!$set_color){
            
            foreach ($clasificacion_dominios as $dominiClr ) {
              if($dominiClr['id'] == $domain_id){
                $set_color = $dominiClr['color'];
              }
            }

          // }



            $respuestas = Answer::with('question')            
            ->whereHas('period', function($q) use($period_id){
                $q->where('id', $period_id);
            })
            ->whereHas('question', function($q) use ($category_id, $filtro_dominio){
              $q->where('dimension_id', $category_id);
              $q->where('domain_id', $filtro_dominio);
            })
            ->whereHas('user.employee_wt', function($q) use($request,$start_date,$end_date,$age_start_date,$age_end_date){
                  $q->when(!is_null($request->sucursal), function($q2) use($request){
                    $q2->whereHas('jobPosition', function($q3) use($request){
                      $q3->whereHas('area', function($q4) use($request){
                        $q4->where('department_id', $request->sucursal);
                      });
                    }); 
                  });
                  $q->when(!is_null($request->direccion), function($q2) use($request){
                    $q2->whereHas('jobPosition', function($q3) use($request){
                      $q3->whereHas('area', function($q4) use($request){
                        $q4->whereHas('department', function($q5) use($request){
                          $q5->where('direction_id', $request->direccion);
                        });
                      });
                    });
                  });
                  $q->when(!is_null($request->job), function($q2) use($request){
                    $q2->where('job_position_id', $request->job);
                  });
                  $q->when(!is_null($request->sexo), function($q2) use($request){
                    $q2->where('sexo', $request->sexo);
                  });
                  $q->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
                    $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
                  });
                  $q->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
                    $q2->whereBetween('ingreso', [$start_date, $end_date]);
                  });
                  $q->when(!is_null($request->centro_trabajo), function($q2) use($request){
                    $q2->where('sucursal', $request->centro_trabajo);
                  });
                  $q->when(!is_null($request->regiones), function($q2) use($request){
                      $q2->where('region_id', $request->regiones);
                  });
            })
            ->get();            



            $real_now = Carbon::now();
            
            foreach ($respuestas as $value) {

              $cerrado = \DB::table('nom035_period_user')
              ->where('period_id', $period_id)
              ->whereIn('status',[3, 33])
              ->whereIn('user_id', [$value->user_id])
              ->count();
          
              if(!in_array($value->user_id, $total_participiantes) && $cerrado>0){
              // if(!in_array($value->user_id, $total_participiantes)){
                    $total_participiantes[] = $value->user_id;
                    $num_participiantes++;

                    
                  $user = $value->user;
                  if(isset($user->employee_wt)){
                    if($user->employee_wt->sexo == 'M'){
                      $tables_dim['sex']['rows']['man']['total'] += 1;
                    }else{
                      $tables_dim['sex']['rows']['women']['total'] += 1;
                    }
                  } 
        
                    
                  $entry = Carbon::parse($user->employee_wt->ingreso);
                  $birthday = Carbon::parse($user->employee_wt->nacimiento);
                  $antiquity = $real_now->diffInDays($entry)/365;
                  if($antiquity < 1) {
                      $tables_dim['antiquity']['rows']  ['less_than_a_year']['total'] += 1;
                      // $analytics['antiquity'][$employee->sexo]['less_than_a_year'] += 1;
                  }
                  elseif($antiquity >= 1 && $antiquity <= 5) {    
                    $tables_dim['antiquity']['rows']['one_to_five_years']['total'] += 1;         
                      // $analytics['antiquity'][$employee->sexo]['one_to_five_years'] += 1;
                  } elseif($antiquity > 5 && $antiquity <= 10) {
                    $tables_dim['antiquity']['rows']['five_to_ten_years']['total'] += 1;
                      // $analytics['antiquity'][$employee->sexo]['five_to_ten_years'] += 1;
                  } elseif($antiquity > 10 && $antiquity <= 15) {
                    $tables_dim['antiquity']['rows']['ten_to_fifteen_years']['total'] += 1;
                      // $analytics['antiquity'][$employee->sexo]['ten_to_fifteen_years'] += 1;
                  } elseif($antiquity > 15) {
                    $tables_dim['antiquity']['rows']['ten_to_fifteen_years']['total'] += 1;
                      // $analytics['antiquity'][$employee->sexo]['more_than_fifteen_years'] += 1;
                  }
        
        
                  // // Generation
                  $generation_row = Generation::getRangeKeyByBirthDay($birthday->year);
                  // dd($birthday->year, $generation_row);
                  if($generation_row) {
                      // $analytics_detail['generations'][$employee->sexo][$generation_row][] = $employee;
                      $tables_dim['generations']['rows'][$generation_row]['total']   += 1;
                  }

                }

                $valor_total_Cat += $this->valorespuesta($value->answer , $value->question->positive);
                
                // $clasificacion_dimensiones[$keyss] = [
                //   "name" => $category_name,
                //   "valor" => $valor_total_Cat,
                // ];
 
                $clasificacion_dimensiones[$keyss]['name'] = $category_name;// $respuestas;
                $clasificacion_dimensiones[$keyss]['category_name_short'] = $category_name_short;// $respuestas;
                $clasificacion_dimensiones[$keyss]['valor'] = $valor_total_Cat;// $respuestas;
                $clasificacion_dimensiones[$keyss]['participantes'] = $num_participiantes;// $respuestas;
                $clasificacion_dimensiones[$keyss]['category'] = $category_real_name;// $respuestas;
                $clasificacion_dimensiones[$keyss]['domain'] = $domain_name;// $respuestas;
                $clasificacion_dimensiones[$keyss]['data'] = [0];
                $clasificacion_dimensiones[$keyss]['data_y'] = 0;// $respuestas;
                $clasificacion_dimensiones[$keyss]['domain_id'] = $domain_id;// $respuestas;
            }
            

            if($num_participiantes>0){
                $promedio = $valor_total_Cat/$num_participiantes;
            }else {$promedio=0;}
            

            $setPromedio = $this->setPromedio($category_name, $promedio,$id_cuestionario);
            $color = $this->setColor($category_name, $promedio, $id_cuestionario);
            $critico = $this->setCritico($category_name, $promedio, $id_cuestionario);

            if(count($respuestas)>0){
                $clasificacion_dimensiones[$keyss]['critico'] = [$setPromedio];// $respuestas;
                $clasificacion_dimensiones[$keyss]['data_y'] = intval( $promedio);// $respuestas;
                $clasificacion_dimensiones[$keyss]['data'] = [$promedio];// $respuestas;
                $clasificacion_dimensiones[$keyss]['color'] = $set_color;// $respuestas;
                $clasificacion_dimensiones[$keyss]['colors'] = $color;// $respuestas;

                $valor_total_Cat = 0;
            }

            $keyss++;
        }
        //fin dimensiones
        //fin dimensiones
        //fin dimensiones
        //fin dimensiones
        $cdi = array();
        foreach ($clasificacion_dimensiones as $key => $value) {
          $cdi[] = $value;
        }

        $clasificacion_dimensiones = $cdi;

        // dd($clasificacion_dimensiones);
      $promedio_general = round($promedio_general / 5);
      $color_general = $this->setCriticoGeneal($promedio_general);
       
      $users_count = $users->count();
        if($request->periodo){

            return response()->json([
                'users_count' => $users_count,
                'dominio_seleccionado' => $dominio_seleccionado,
                'promedio_general' => $promedio_general,
                'sucursales' => $sucursales,
                'directions' => $directions,
                'jobs' => $jobs,
                'sexs' => $sexs,
                'filtro_dominio' => $filtro_dominio,
                'ages' => $ages,
                'starts' => $starts,
                'regiones' => $regiones,
                'centros_trabajo' => $centros_trabajo,
                //general
            	'clasificacion_general' => $clasificacion_general, 
            	'resultado_general' => $resultado_general, 
            	'color_general' => $color_general,
            	//categorias
            	'clasificacion_categorias' => $clasificacion_categorias, 
            	'clasificacion_categorias_by_usuarios' => $clasificacion_categorias_by_usuarios, 
            	'name_categories' => $name_categories,
            	//dominios
            	'clasificacion_dominios' => $clasificacion_dominios, 
                'clasificacion_dominios_by_usuarios' => $clasificacion_dominios_by_usuarios, 
                'name_dominios' => $name_dominios,
                //dimensiones
	            'clasificacion_dimensiones' => $clasificacion_dimensiones,  
	            'name_dimensiones' => $name_dimensiones,
              'colores_dominios' => $colores_dominios,
              'tables_dim' => $tables_dim
        ], 200);

          

        }else{
              //  return json_encode($clasificacion_dimensiones,true);
              return view('nom035.graficos_by.index', compact('sucursales',
	            'dominio_seleccionado',
	            'promedio_general',
	            'directions',
	            'jobs',
              'tables_cat', 
              'tables_dim',
              'users_count',
              'filtro_dominio',
              'sexs',
              'ages',
              'starts',
              'regiones',
              'centros_trabajo',
	            'period_id',
	            'periodos',    
              'clasificacion_dimensiones',
              'name_dimensiones',
              'colores_dominios',
              	//general
              	'clasificacion_general',
              	'resultado_general',
              	'color_general',
              	//categorias
                'clasificacion_categorias',
                'clasificacion_categorias_by_usuarios',
                'name_categories',
                //dominios
                'clasificacion_dominios',
                'clasificacion_dominios_by_usuarios',
                'name_dominios'
	            //dimensiones	            
            ));

        }


    }




    private function valorespuesta($respuesta, $sentido){

        $valor = null;
        //0 == S
        if($sentido == 0){

            switch($respuesta) {
                case 'Siempre':         $valor = 4; break;
                case 'Casi siempre':    $valor = 3; break;
                case 'Algunas veces':   $valor = 2; break;
                case 'Casi nunca':      $valor = 1; break;
                case 'Nunca':           $valor = 0; break;
            }

        //1 == N
        }else if($sentido == 1){

            switch($respuesta) {
                case 'Siempre':         $valor = 0; break;
                case 'Casi siempre':    $valor = 1; break;
                case 'Algunas veces':   $valor = 2; break;
                case 'Casi nunca':      $valor = 3; break;
                case 'Nunca':           $valor = 4; break;
            }

        }

        return $valor;

    }

    private function setColor($seccion ,$respuesta,$id_cuestionario){

        $color = null;
        //0 == S
        if($id_cuestionario =='Cuestionario 3'){
            //cuestionario 3
            if($seccion == 'Ambiente de trabajo'){

    	        if($respuesta < 5){$color = 'turqueza';}
    	        else if($respuesta >= 5 && $respuesta < 9){$color = 'verde';}
    	        else if($respuesta >= 9 && $respuesta < 11){$color = 'amarillo';}
    	        else if($respuesta >= 11 && $respuesta < 14){$color = 'naranja';}
    	        else if($respuesta >= 14){$color = 'rojo';}

            }else if($seccion == 'Factores propios de la actividad'){
            	
    	        if($respuesta < 15){$color = 'turqueza';}
    	        else if($respuesta >= 15 && $respuesta < 30){$color = 'verde';}
    	        else if($respuesta >= 30 && $respuesta < 45){$color = 'amarillo';}
    	        else if($respuesta >= 45 && $respuesta < 60){$color = 'naranja';}
    	        else if($respuesta >= 60){$color = 'rojo';}

            }else if($seccion == 'Organización del tiempo de trabajo'){
            	
    	        if($respuesta < 5){$color = 'turqueza';}
    	        else if($respuesta >= 5 && $respuesta < 7){$color = 'verde';}
    	        else if($respuesta >= 7 && $respuesta < 10){$color = 'amarillo';}
    	        else if($respuesta >= 10 && $respuesta < 13){$color = 'naranja';}
    	        else if($respuesta >= 13){$color = 'rojo';}

            }else if($seccion == 'Liderazgo y relaciones en el trabajo'){
            	
    	        if($respuesta < 14){$color = 'turqueza';}
    	        else if($respuesta >= 14 && $respuesta < 29){$color = 'verde';}
    	        else if($respuesta >= 29 && $respuesta < 42){$color = 'amarillo';}
    	        else if($respuesta >= 42 && $respuesta < 58){$color = 'naranja';}
    	        else if($respuesta >= 58){$color = 'rojo';}

            }else if($seccion == 'Entorno organizacional'){
            	
    	        if($respuesta < 10){$color = 'turqueza';}
    	        else if($respuesta >= 10 && $respuesta < 14){$color = 'verde';}
    	        else if($respuesta >= 14 && $respuesta < 18){$color = 'amarillo';}
    	        else if($respuesta >= 18 && $respuesta < 23){$color = 'naranja';}
    	        else if($respuesta >= 23){$color = 'rojo';}

            }
           
            //dominios
            else if($seccion == 'Condiciones en el ambiente de trabajo'){
                
                if($respuesta < 5){$color = 'turqueza';}
                else if($respuesta >= 5 && $respuesta < 9){$color = 'verde';}
                else if($respuesta >= 9 && $respuesta < 11){$color = 'amarillo';}
                else if($respuesta >= 11 && $respuesta < 14){$color = 'naranja';}
                else if($respuesta >= 14){$color = 'rojo';}

            } else if($seccion == 'Carga de trabajo'){
                
                if($respuesta < 15){$color = 'turqueza';}
                else if($respuesta >= 15 && $respuesta < 21){$color = 'verde';}
                else if($respuesta >= 21 && $respuesta < 27){$color = 'amarillo';}
                else if($respuesta >= 27 && $respuesta < 37){$color = 'naranja';}
                else if($respuesta >= 37){$color = 'rojo';}

            } else if($seccion == 'Falta de control sobre el trabajo'){
                
                if($respuesta < 11){$color = 'turqueza';}
                else if($respuesta >= 11 && $respuesta < 16){$color = 'verde';}
                else if($respuesta >= 16 && $respuesta < 21){$color = 'amarillo';}
                else if($respuesta >= 21 && $respuesta < 25){$color = 'naranja';}
                else if($respuesta >= 25){$color = 'rojo';}

            } else if($seccion == 'Jornada de trabajo'){
                
                if($respuesta < 1){$color = 'turqueza';}
                else if($respuesta >= 1 && $respuesta < 2){$color = 'verde';}
                else if($respuesta >= 2 && $respuesta < 4){$color = 'amarillo';}
                else if($respuesta >= 4 && $respuesta < 6){$color = 'naranja';}
                else if($respuesta >= 6){$color = 'rojo';}

            } else if($seccion == 'Interferencia en la relación trabajo-familia'){
                
                if($respuesta < 4){$color = 'turqueza';}
                else if($respuesta >= 4 && $respuesta < 6){$color = 'verde';}
                else if($respuesta >= 6 && $respuesta < 8){$color = 'amarillo';}
                else if($respuesta >= 8 && $respuesta < 10){$color = 'naranja';}
                else if($respuesta >= 10){$color = 'rojo';}

            } else if($seccion == 'Liderazgo'){
                
                if($respuesta < 9){$color = 'turqueza';}
                else if($respuesta >= 9 && $respuesta < 12){$color = 'verde';}
                else if($respuesta >= 12 && $respuesta < 16){$color = 'amarillo';}
                else if($respuesta >= 16 && $respuesta < 20){$color = 'naranja';}
                else if($respuesta >= 20){$color = 'rojo';}

            } else if($seccion == 'Relaciones en el trabajo'){
                
                if($respuesta < 10){$color = 'turqueza';}
                else if($respuesta >= 10 && $respuesta < 13){$color = 'verde';}
                else if($respuesta >= 13 && $respuesta < 17){$color = 'amarillo';}
                else if($respuesta >= 17 && $respuesta < 21){$color = 'naranja';}
                else if($respuesta >= 21){$color = 'rojo';}

            } else if($seccion == 'Violencia'){
                
                if($respuesta < 7){$color = 'turqueza';}
                else if($respuesta >= 7 && $respuesta < 10){$color = 'verde';}
                else if($respuesta >= 10 && $respuesta < 13){$color = 'amarillo';}
                else if($respuesta >= 13 && $respuesta < 16){$color = 'naranja';}
                else if($respuesta >= 16){$color = 'rojo';}

            } else if($seccion == 'Reconocimiento del desempeño'){
                
                if($respuesta < 6){$color = 'turqueza';}
                else if($respuesta >= 6 && $respuesta < 10){$color = 'verde';}
                else if($respuesta >= 10 && $respuesta < 14){$color = 'amarillo';}
                else if($respuesta >= 14 && $respuesta < 18){$color = 'naranja';}
                else if($respuesta >= 18){$color = 'rojo';}

            } else if($seccion == 'Insuficiente sentido de pertenencia e inestabilidad'){
                
                if($respuesta < 4){$color = 'turqueza';}
                else if($respuesta >= 4 && $respuesta < 6){$color = 'verde';}
                else if($respuesta >= 6 && $respuesta < 8){$color = 'amarillo';}
                else if($respuesta >= 8 && $respuesta < 10){$color = 'naranja';}
                else if($respuesta >= 10){$color = 'rojo';}

            }
        }else if($id_cuestionario =='Cuestionario 2'){

            //cuestionario 2
            if($seccion == 'Ambiente de trabajo'){
                if($respuesta < 3){$color = 'turqueza';}
                else if($respuesta >= 3 && $respuesta < 5){$color = 'verde';}
                else if($respuesta >= 5 && $respuesta < 7){$color = 'amarillo';}
                else if($respuesta >= 7 && $respuesta < 9){$color = 'naranja';}
                else if($respuesta >= 9){$color = 'rojo';}

            }else if($seccion == 'Factores propios de la actividad'){
                
                if($respuesta < 10){$color = 'turqueza';}
                else if($respuesta >= 10 && $respuesta < 20){$color = 'verde';}
                else if($respuesta >= 20 && $respuesta < 30){$color = 'amarillo';}
                else if($respuesta >= 30 && $respuesta < 40){$color = 'naranja';}
                else if($respuesta >= 40){$color = 'rojo';}

            }else if($seccion == 'Organización del tiempo de trabajo'){
                
                if($respuesta < 4){$color = 'turqueza';}
                else if($respuesta >= 4 && $respuesta < 6){$color = 'verde';}
                else if($respuesta >= 6 && $respuesta < 9){$color = 'amarillo';}
                else if($respuesta >= 9 && $respuesta < 12){$color = 'naranja';}
                else if($respuesta >= 12){$color = 'rojo';}

            }else if($seccion == 'Liderazgo y relaciones en el trabajo'){
                
                if($respuesta < 10){$color = 'turqueza';}
                else if($respuesta >= 10 && $respuesta < 18){$color = 'verde';}
                else if($respuesta >= 18 && $respuesta < 28){$color = 'amarillo';}
                else if($respuesta >= 28 && $respuesta < 38){$color = 'naranja';}
                else if($respuesta >= 38){$color = 'rojo';}

            }


            //dominios
            else if($seccion == 'Condiciones en el ambiente de trabajo'){
                
                if($respuesta < 3){$color = 'turqueza';}
                else if($respuesta >= 3 && $respuesta < 5){$color = 'verde';}
                else if($respuesta >= 5 && $respuesta < 7){$color = 'amarillo';}
                else if($respuesta >= 7 && $respuesta < 9){$color = 'naranja';}
                else if($respuesta >= 9){$color = 'rojo';}

            } else if($seccion == 'Carga de trabajo'){
                
                if($respuesta < 12){$color = 'turqueza';}
                else if($respuesta >= 12 && $respuesta < 16){$color = 'verde';}
                else if($respuesta >= 16 && $respuesta < 20){$color = 'amarillo';}
                else if($respuesta >= 20 && $respuesta < 24){$color = 'naranja';}
                else if($respuesta >= 24){$color = 'rojo';}

            } else if($seccion == 'Falta de control sobre el trabajo'){
                
                if($respuesta < 5){$color = 'turqueza';}
                else if($respuesta >= 5 && $respuesta < 8){$color = 'verde';}
                else if($respuesta >= 8 && $respuesta < 11){$color = 'amarillo';}
                else if($respuesta >= 11 && $respuesta < 14){$color = 'naranja';}
                else if($respuesta >= 14){$color = 'rojo';}

            } else if($seccion == 'Jornada de trabajo'){
                
                if($respuesta < 1){$color = 'turqueza';}
                else if($respuesta >= 1 && $respuesta < 2){$color = 'verde';}
                else if($respuesta >= 2 && $respuesta < 4){$color = 'amarillo';}
                else if($respuesta >= 4 && $respuesta < 6){$color = 'naranja';}
                else if($respuesta >= 6){$color = 'rojo';}

            } else if($seccion == 'Interferencia en la relación trabajo-familia'){
                
                 if($respuesta < 1){$color = 'turqueza';}
                else if($respuesta >= 1 && $respuesta < 2){$color = 'verde';}
                else if($respuesta >= 2 && $respuesta < 4){$color = 'amarillo';}
                else if($respuesta >= 4 && $respuesta < 6){$color = 'naranja';}
                else if($respuesta >= 6){$color = 'rojo';}

            } else if($seccion == 'Liderazgo'){
                
                
                if($respuesta < 3){$color = 'turqueza';}
                else if($respuesta >= 3 && $respuesta < 5){$color = 'verde';}
                else if($respuesta >= 5 && $respuesta < 8){$color = 'amarillo';}
                else if($respuesta >= 8 && $respuesta < 11){$color = 'naranja';}
                else if($respuesta >= 11){$color = 'rojo';}

            } else if($seccion == 'Relaciones en el trabajo'){
                
                if($respuesta < 5){$color = 'turqueza';}
                else if($respuesta >= 5 && $respuesta < 8){$color = 'verde';}
                else if($respuesta >= 8 && $respuesta < 11){$color = 'amarillo';}
                else if($respuesta >= 11 && $respuesta < 14){$color = 'naranja';}
                else if($respuesta >= 14){$color = 'rojo';}

            } else if($seccion == 'Violencia'){
                
                if($respuesta < 7){$color = 'turqueza';}
                else if($respuesta >= 7 && $respuesta < 10){$color = 'verde';}
                else if($respuesta >= 10 && $respuesta < 13){$color = 'amarillo';}
                else if($respuesta >= 13 && $respuesta < 16){$color = 'naranja';}
                else if($respuesta >= 16){$color = 'rojo';}

            }

        }
        return $color;

    }



    private function setCritico($seccion ,$respuesta, $id_cuestionario){

        $color = null;

        $prom = $this->setPromedio($seccion, $respuesta, $id_cuestionario);

        if($prom == 0){
          $color = "#5dc1b9";
        }else if($prom == 1){
          $color = "#00FF00";
        }else if($prom == 2){
          $color = "#FFFF00";
        }else if($prom == 3){
          $color = "#FFA500";

        }else if($prom == 4){
          $color = "#FF0000";

        }else if($prom == 5){
          $color = "#FF0000";

        }

        return $color;

    }
    
    private function setCriticoGeneal($prom){
        
        if($prom == 0){
          $color = "#5dc1b9";
        }else if($prom == 1){
          $color = "#00FF00";
        }else if($prom == 2){
          $color = "#FFFF00";
        }else if($prom == 3){
          $color = "#FFA500";

        }else if($prom == 4){
          $color = "#FF0000";

        }else if($prom == 5){
          $color = "#FF0000";

        }

        return $color;

    }
    

    private function setColorGeneralOld($respuesta, $id_cuestionario){

        $color = null;
        if ($id_cuestionario == 'Cuestionario 3'){
        if($respuesta < 50){$color = 'turqueza';}
        else if($respuesta >= 50 && $respuesta < 75){$color = 'verde';}
        else if($respuesta >= 75 && $respuesta < 99){$color = 'amarillo';}
        else if($respuesta >= 99 && $respuesta < 140){$color = 'naranja';}
        else if($respuesta >= 140){$color = 'rojo';}
        }
        else{
          if($respuesta < 20){$color = 'turqueza';}
        else if($respuesta >= 20 && $respuesta < 45){$color = 'verde';}
        else if($respuesta >= 45 && $respuesta < 70){$color = 'amarillo';}
        else if($respuesta >= 70 && $respuesta < 90){$color = 'naranja';}
        else if($respuesta >= 90){$color = 'rojo';}
        }
        return $color;

    }

    private function setColorGeneral($respuesta, $id_cuestionario){

        $color = null;
        if ($id_cuestionario == 'Cuestionario 3'){
        if($respuesta < 50){$color = 'turqueza';}
        else if($respuesta >= 50 && $respuesta < 75){$color = 'verde';}
        else if($respuesta >= 75 && $respuesta < 99){$color = 'amarillo';}
        else if($respuesta >= 99 && $respuesta < 140){$color = 'naranja';}
        else if($respuesta >= 140){$color = 'rojo';}
        }
        else{
          if($respuesta < 20){$color = 'turqueza';}
        else if($respuesta >= 20 && $respuesta < 45){$color = 'verde';}
        else if($respuesta >= 45 && $respuesta < 70){$color = 'amarillo';}
        else if($respuesta >= 70 && $respuesta < 90){$color = 'naranja';}
        else if($respuesta >= 90){$color = 'rojo';}
        }
        return $color;

    }

    private function setPromedio($seccion ,$respuesta, $id_cuestionario = null){

        $prom = null;
        //0 == S

        if($id_cuestionario =='Cuestionario 3'){
            if($seccion == 'Ambiente de trabajo'){

    	        if($respuesta < 5){$prom = 1;}
    	        else if($respuesta >= 5 && $respuesta < 9){$prom = 2;}
    	        else if($respuesta >= 9 && $respuesta < 11){$prom = 3;}
    	        else if($respuesta >= 11 && $respuesta < 14){$prom = 4;}
    	        else if($respuesta >= 14){$prom = 5;}

            }else if($seccion == 'Factores propios de la actividad'){
            	
    	        if($respuesta < 15){$prom = 1;}
    	        else if($respuesta >= 15 && $respuesta < 30){$prom = 2;}
    	        else if($respuesta >= 30 && $respuesta < 45){$prom = 3;}
    	        else if($respuesta >= 45 && $respuesta < 60){$prom = 4;}
    	        else if($respuesta >= 60){$prom = 5;}

            }else if($seccion == 'Organización del tiempo de trabajo'){
            	
    	        if($respuesta < 5){$prom = 1;}
    	        else if($respuesta >= 5 && $respuesta < 7){$prom = 2;}
    	        else if($respuesta >= 7 && $respuesta < 10){$prom = 3;}
    	        else if($respuesta >= 10 && $respuesta < 13){$prom = 4;}
    	        else if($respuesta >= 13){$prom = 5;}

            }else if($seccion == 'Liderazgo y relaciones en el trabajo'){
            	
    	        if($respuesta < 14){$prom = 1;}
    	        else if($respuesta >= 14 && $respuesta < 29){$prom = 2;}
    	        else if($respuesta >= 29 && $respuesta < 42){$prom = 3;}
    	        else if($respuesta >= 42 && $respuesta < 58){$prom = 4;}
    	        else if($respuesta >= 58){$prom = 5;}

            }else if($seccion == 'Entorno organizacional'){
            	
    	        if($respuesta < 10){$prom = 1;}
    	        else if($respuesta >= 10 && $respuesta < 14){$prom = 2;}
    	        else if($respuesta >= 14 && $respuesta < 18){$prom = 3;}
    	        else if($respuesta >= 18 && $respuesta < 23){$prom = 4;}
    	        else if($respuesta >= 23){$prom = 5;}

            }
            
            //dominios
            else if($seccion == 'Condiciones en el ambiente de trabajo'){
                
                if($respuesta < 5){$prom = 1;}
                else if($respuesta >= 5 && $respuesta < 9){$prom = 2;}
                else if($respuesta >= 9 && $respuesta < 11){$prom = 3;}
                else if($respuesta >= 11 && $respuesta < 14){$prom = 4;}
                else if($respuesta >= 14){$prom = 5;}

            } else if($seccion == 'Carga de trabajo'){
                
                if($respuesta < 15){$prom = 1;}
                else if($respuesta >= 15 && $respuesta < 21){$prom = 2;}
                else if($respuesta >= 21 && $respuesta < 27){$prom = 3;}
                else if($respuesta >= 27 && $respuesta < 37){$prom = 4;}
                else if($respuesta >= 37){$prom = 5;}

            } else if($seccion == 'Falta de control sobre el trabajo'){
                
                if($respuesta < 11){$prom = 1;}
                else if($respuesta >= 11 && $respuesta < 16){$prom = 2;}
                else if($respuesta >= 16 && $respuesta < 21){$prom = 3;}
                else if($respuesta >= 21 && $respuesta < 25){$prom = 4;}
                else if($respuesta >= 25){$prom = 5;}

            } else if($seccion == 'Jornada de trabajo'){
                
                if($respuesta < 1){$prom = 1;}
                else if($respuesta >= 1 && $respuesta < 2){$prom = 2;}
                else if($respuesta >= 2 && $respuesta < 4){$prom = 3;}
                else if($respuesta >= 4 && $respuesta < 6){$prom = 4;}
                else if($respuesta >= 6){$prom = 5;}

            } else if($seccion == 'Interferencia en la relación trabajo-familia'){
                
                if($respuesta < 4){$prom = 1;}
                else if($respuesta >= 4 && $respuesta < 6){$prom = 2;}
                else if($respuesta >= 6 && $respuesta < 8){$prom = 3;}
                else if($respuesta >= 8 && $respuesta < 10){$prom = 4;}
                else if($respuesta >= 10){$prom = 5;}

            } else if($seccion == 'Liderazgo'){
                
                if($respuesta < 9){$prom = 1;}
                else if($respuesta >= 9 && $respuesta < 12){$prom = 2;}
                else if($respuesta >= 12 && $respuesta < 16){$prom = 3;}
                else if($respuesta >= 16 && $respuesta < 20){$prom = 4;}
                else if($respuesta >= 20){$prom = 5;}

            } else if($seccion == 'Relaciones en el trabajo'){
                
                if($respuesta < 10){$prom = 1;}
                else if($respuesta >= 10 && $respuesta < 13){$prom = 2;}
                else if($respuesta >= 13 && $respuesta < 17){$prom = 3;}
                else if($respuesta >= 17 && $respuesta < 21){$prom = 4;}
                else if($respuesta >= 21){$prom = 5;}

            } else if($seccion == 'Violencia'){
                
                if($respuesta < 7){$prom = 1;}
                else if($respuesta >= 7 && $respuesta < 10){$prom = 2;}
                else if($respuesta >= 10 && $respuesta < 13){$prom = 3;}
                else if($respuesta >= 13 && $respuesta < 16){$prom = 4;}
                else if($respuesta >= 16){$prom = 5;}

            } else if($seccion == 'Reconocimiento del desempeño'){
                
                if($respuesta < 6){$prom = 1;}
                else if($respuesta >= 6 && $respuesta < 10){$prom = 2;}
                else if($respuesta >= 10 && $respuesta < 14){$prom = 3;}
                else if($respuesta >= 14 && $respuesta < 18){$prom = 4;}
                else if($respuesta >= 18){$prom = 5;}

            } else if($seccion == 'Insuficiente sentido de pertenencia e inestabilidad'){
                
                if($respuesta < 4){$prom = 1;}
                else if($respuesta >= 4 && $respuesta < 6){$prom = 2;}
                else if($respuesta >= 6 && $respuesta < 8){$prom = 3;}
                else if($respuesta >= 8 && $respuesta < 10){$prom = 4;}
                else if($respuesta >= 10){$prom = 5;}

            }
       }else if($id_cuestionario =='Cuestionario 2'){

            //cuestionario 2
            if($seccion == 'Ambiente de trabajo'){
                if($respuesta < 3){$prom = 1;}
                else if($respuesta >= 3 && $respuesta < 5){$prom = 2;}
                else if($respuesta >= 5 && $respuesta < 7){$prom = 3;}
                else if($respuesta >= 7 && $respuesta < 9){$prom = 4;}
                else if($respuesta >= 9){$prom = 5;}

            }else if($seccion == 'Factores propios de la actividad'){
                
                if($respuesta < 10){$prom = 1;}
                else if($respuesta >= 10 && $respuesta < 20){$prom = 2;}
                else if($respuesta >= 20 && $respuesta < 30){$prom = 3;}
                else if($respuesta >= 30 && $respuesta < 40){$prom = 4;}
                else if($respuesta >= 40){$prom = 5;}

            }else if($seccion == 'Organización del tiempo de trabajo'){
                
                if($respuesta < 4){$prom = 1;}
                else if($respuesta >= 4 && $respuesta < 6){$prom = 2;}
                else if($respuesta >= 6 && $respuesta < 9){$prom = 3;}
                else if($respuesta >= 9 && $respuesta < 12){$prom = 4;}
                else if($respuesta >= 12){$prom = 5;}

            }else if($seccion == 'Liderazgo y relaciones en el trabajo'){
                
                if($respuesta < 10){$prom = 1;}
                else if($respuesta >= 10 && $respuesta < 18){$prom = 2;}
                else if($respuesta >= 18 && $respuesta < 28){$prom = 3;}
                else if($respuesta >= 28 && $respuesta < 38){$prom = 4;}
                else if($respuesta >= 38){$prom = 5;}

            }

             //dominios
            else if($seccion == 'Condiciones en el ambiente de trabajo'){
                
                if($respuesta < 3){$prom = 1;}
                else if($respuesta >= 3 && $respuesta < 5){$prom = 2;}
                else if($respuesta >= 5 && $respuesta < 7){$prom = 3;}
                else if($respuesta >= 7 && $respuesta < 9){$prom = 4;}
                else if($respuesta >= 9){$prom = 5;}

            } else if($seccion == 'Carga de trabajo'){
                
                if($respuesta < 12){$prom = 1;}
                else if($respuesta >= 12 && $respuesta < 16){$prom = 2;}
                else if($respuesta >= 16 && $respuesta < 20){$prom = 3;}
                else if($respuesta >= 20 && $respuesta < 24){$prom = 4;}
                else if($respuesta >= 24){$prom = 5;}

            } else if($seccion == 'Falta de control sobre el trabajo'){
                
                if($respuesta < 5){$prom = 1;}
                else if($respuesta >= 5 && $respuesta < 8){$prom = 2;}
                else if($respuesta >= 8 && $respuesta < 11){$prom = 3;}
                else if($respuesta >= 11 && $respuesta < 14){$prom = 4;}
                else if($respuesta >= 14){$prom = 5;}

            } else if($seccion == 'Jornada de trabajo'){
                
                if($respuesta < 1){$prom = 1;}
                else if($respuesta >= 1 && $respuesta < 2){$prom = 2;}
                else if($respuesta >= 2 && $respuesta < 4){$prom = 3;}
                else if($respuesta >= 4 && $respuesta < 6){$prom = 4;}
                else if($respuesta >= 6){$prom = 5;}

            } else if($seccion == 'Interferencia en la relación trabajo-familia'){
                
                 if($respuesta < 1){$prom = 1;}
                else if($respuesta >= 1 && $respuesta < 2){$prom = 2;}
                else if($respuesta >= 2 && $respuesta < 4){$prom = 3;}
                else if($respuesta >= 4 && $respuesta < 6){$prom = 4;}
                else if($respuesta >= 6){$prom = 5;}

            } else if($seccion == 'Liderazgo'){
                
                
                if($respuesta < 3){$prom = 1;}
                else if($respuesta >= 3 && $respuesta < 5){$prom = 2;}
                else if($respuesta >= 5 && $respuesta < 8){$prom = 3;}
                else if($respuesta >= 8 && $respuesta < 11){$prom = 4;}
                else if($respuesta >= 11){$prom = 5;}

            } else if($seccion == 'Relaciones en el trabajo'){
                
                if($respuesta < 5){$prom = 1;}
                else if($respuesta >= 5 && $respuesta < 8){$prom = 2;}
                else if($respuesta >= 8 && $respuesta < 11){$prom = 3;}
                else if($respuesta >= 11 && $respuesta < 14){$prom = 4;}
                else if($respuesta >= 14){$prom = 5;}

            } else if($seccion == 'Violencia'){
                
                if($respuesta < 7){$prom = 1;}
                else if($respuesta >= 7 && $respuesta < 10){$prom = 2;}
                else if($respuesta >= 10 && $respuesta < 13){$prom = 3;}
                else if($respuesta >= 13 && $respuesta < 16){$prom = 4;}
                else if($respuesta >= 16){$prom = 5;}

            }


        }

        return $prom;

    }

    public function reporte_norma_grafica_dominio(Request $request){

      $clasificacion_general = array();
      $valor_total = 0;
      $color = 0;
      $num_participiantes = 0;
      $valor_general = 0;
      $resultado_general = 0;
      $color_general = "";
      $clasificacion_categorias = "";
      $clasificacion_categorias_by_usuarios = "";
      $clasificacion_dominios = "";
      $clasificacion_dimensiones = "";
      $clasificacion_dominios_by_usuarios = "";
      $clasificacion_dimensiones_by_usuarios = "";
      $dominio_seleccionado = "";
      $periodos = $users = array();
      $period_id = 0;
      $admin = true;

      if (auth()->user()->role == 'admin'){

        $periodos = NormPeriod::where('status','Abierto')->orWhere('status', 'Cerrado')->select('id', 'name', 'status')->orderBy('id', 'DESC')->get();
      }

      else{

        $permissions = auth()->user()->multiUserPermissions()->orderBy('enterprise_id')->orderBy('sucursal_id')->get();

        if (count($permissions)>0 && $permissions[0]->enterprise_id == 0){

          $periodos = NormPeriod::where('status','Abierto')->orWhere('status', 'Cerrado')->select('id', 'name', 'status')->orderBy('id', 'DESC')->get();
        }

        else{

          $admin = false;
          $enterprises_without_sucursals = $enterprises_with_sucursals = array();
          $sucursals = array();

          foreach ($permissions as $key => $value){
            
            if ($value->sucursal_id == 0){

              if (!in_array($value->enterprise_id, $enterprises_without_sucursals)){

                $enterprises_without_sucursals[] = $value->enterprise_id;
              }
            }

            else{

              if (!in_array($value->enterprise_id, $enterprises_without_sucursals) && !in_array($value->sucursal_id, $sucursals)){

                $enterprises_with_sucursals[] = $value->enterprise_id;
                $sucursals[] = $value->sucursal_id;
              }
            }
          }

          $periods = array();

          if (!empty($enterprises_without_sucursals)){

            $periods = NormPeriod::whereIn('enterprise_id', $enterprises_without_sucursals)->pluck('id')->toArray();
          }

          if (!empty($sucursals)){

            foreach ($sucursals as $key => $value){
                
              $periods2 = NormPeriod::whereIn('enterprise_id', $enterprises_with_sucursals)->whereIn('sucursal_id', $sucursals)->pluck('id')->toArray();

              if (!empty($periods2)){

                $periods = array_merge($periods, $periods2);
              }
            }
          }

          if (!empty($periods)){

            $periodos = NormPeriod::whereIn('id', $periods)->select('id', 'name', 'status')->orderBy('id', 'DESC')->get();
          }else{
              
            $user_periods = \DB::table('nom035_period_user')->where('user_id', auth()->user()->id)->pluck('period_id')->toArray();
            $periodos = NormPeriod::where('status', 'Cerrado')->whereIn('id', $user_periods)->orderBy('id', 'DESC')->get();
            
          }
        }
      }

      if(!$request->periodo || Session::has('period_without_results')){

        if (count($periodos) > 0){

          $periodo = $periodos[0];
        }

        else{

              $sin_periodos = true;
              $name_categories = true;
              $name_dominios = true;
              $name_dimensiones = true;
              $clasificacion_categorias = [];
              $clasificacion_dominios = [];
              $clasificacion_dimensiones = [];

              return view('nom035.graficos_by.index', compact('sin_periodos', 'periodos', 'period_id',
                  'name_categories',
                  'name_dominios',
                  'name_dimensiones',
                  'clasificacion_categorias',
                  'clasificacion_dominios',
                  'clasificacion_dimensiones'
              ));

        }

        if (!$request->periodo && empty($_POST['period_id'])){
        
          $period_id = $periodo->id;
        }

        else{

          $period_id = $_POST['period_id'];
          Session::forget('period_without_results');
          $request->periodo = null;
        }

      }else{

          if($request->cambio =='periodo'){
              $request['sucursal'] = null;
              $request['direccion'] = null;
              $request['job'] = null;
              $request['sexo'] = null;
              $request['edad'] = null;
              $request['ingreso'] = null;
              $request['centro_trabajo'] = null;
          }
          else if($request->cambio=='sucursal'){
          
              $request['direccion'] = null;
              $request['job'] = null;

        }else if($request->cambio=='area'){

              $request['job'] = null;
              $request['sexo'] = null;
              $request['edad'] = null;
              $request['ingreso'] = null;
              $request['centro_trabajo'] = null;

        }else if($request->cambio=='edad'){

              $request['sexo'] = null;
              $request['ingreso'] = null;
              $request['centro_trabajo'] = null;

        }else if($request->cambio=='inicio'){

              $request['sexo'] = null;
              $request['edad'] = null;
              $request['centro_trabajo'] = null;

        }else if($request->cambio=='sexo'){

              $request['edad'] = null;
              $request['ingreso'] = null;
              $request['centro_trabajo'] = null;
        
        }else if($request->cambio=='centro_trabajo'){

              $request['edad'] = null;
              $request['ingreso'] = null;
              $request['sexo'] = null;
        }

      $period_id = $request->periodo;



      }



      //$sucursal = array();
      //$Areas = Area::where('department_id', $request['sucursal'])->get();
      //$sucursal = JobPosition::where('id_department', $request['sucursal'])->pluck('id')->toArray();

      /*foreach ($Areas as $key => $Area) {                
          
          $JobPositions = JobPosition::where('area_id',$Area->id)->get();

              foreach ($JobPositions as $key => $JobPosition) {                                                                       
                  $sucursal[] = $JobPosition->id;

              }

      }*/

      /*$puestos = array();
      $JobPositionLevels = JobPositionLevel::where('id',$request['job'])->get();

      foreach ($JobPositionLevels as $key => $JobPositionLevel) {   

           $JobPositions0 = JobPosition::where('job_position_level_id',$JobPositionLevel->id)->get();

          foreach ($JobPositions0 as $key => $JobPosition0) {                                                                       
             $puestos[] = $JobPosition0->id;

          }

          

      }*/


      //categorias
      $listcategory = array();
      $categories = array();
      $id_cuestionario = null;
      $PeriodQuestion = PeriodQuestion::with('question')->where('period_id',$period_id)->get();
      foreach ($PeriodQuestion as $key => $PeriodQ) {
          if(!in_array($PeriodQ->question->category_id, $listcategory)){
              $listcategory[] = $PeriodQ->question->category_id;
              if(isset($PeriodQ->question->category->name)){
                   $categories[] = ['id'=>$PeriodQ->question->category->id, 'name'=>$PeriodQ->question->category->name];
                   $id_cuestionario = $PeriodQ->question->cuestionario;
              }
          }
      }
      $name_categories = $categories;
      //categorias



      //dominios
      $listcategory = array();
      $dominios = array();
      $id_cuestionario = null;
      $PeriodQuestion = PeriodQuestion::with('question')->where('period_id',$period_id)->get();
      foreach ($PeriodQuestion as $key => $PeriodQ) {
          if(!in_array($PeriodQ->question->domain_id, $listcategory)){
              $listcategory[] = $PeriodQ->question->domain_id;
              if(isset($PeriodQ->question->domain->name)){
                   $dominios[] = ['id'=>$PeriodQ->question->domain->id, 'name'=>$PeriodQ->question->domain->name,'descriptions'=>$PeriodQ->question->domain->descriptions,'category'=>$PeriodQ->question->domain->category->name,'category_id'=>$PeriodQ->question->domain->category->id];
                  //  $dominios[] = ['id'=>$PeriodQ->question->domain->id, 'name'=>$PeriodQ->question->domain->name,'category'=>$PeriodQ->question->domain->category->name];
                   $id_cuestionario = $PeriodQ->question->cuestionario;
              }
          }
      }
      $name_dominios = $dominios;
      //dominios


      //dimensioness        
      $listcategory = array();
      $dimensiones = array();
      $id_cuestionario = null;
      $PeriodQuestion = PeriodQuestion::with('question')->where('period_id',$period_id)->get();
      foreach ($PeriodQuestion as $key => $PeriodQ) {
          if(!in_array($PeriodQ->question->dimension_id, $listcategory)){
              $listcategory[] = $PeriodQ->question->dimension_id;
              if(isset($PeriodQ->question->dimension->name)){
                   $dimensiones[] = ['id'=>$PeriodQ->question->dimension->id, 'name'=>$PeriodQ->question->dimension->name,'category'=>$PeriodQ->question->dimension->domain->category->name,'domain'=>$PeriodQ->question->dimension->domain->name,'domain_id'=>$PeriodQ->question->dimension->domain->id];
                   $id_cuestionario = $PeriodQ->question->cuestionario;
              }
          }
      }
      $name_dimensiones = $dimensiones;
      //dimensiones

      $questions = DB::table('nom035_questions')->where('cuestionario', 'Cuestionario 2')->orWhere('cuestionario', 'Cuestionario 3')->pluck('id')->toArray();
      $respuestas = array();

      if ($admin){

        $respuestas = Answer::groupBy('user_id')
        ->where('period_id', $period_id)
        ->whereIn('question_id', $questions)
        ->pluck('user_id')->toArray();
      }

      else{

        $respuestas = Answer::groupBy('user_id')
        ->where('period_id', $period_id)
        ->whereIn('question_id', $questions)
        // ->whereIn('user_id', $users)
        ->pluck('user_id')->toArray();
      }

      $today = date('Y-m-d');
      $end_date = $start_date = $age_start_date = $age_end_date = 0;

      if (!is_null($request->edad) || !is_null($request->ingreso)){

        if (!is_null($request->ingreso)){

          $end_date = date('Y-m-d', strtotime($today . ' - ' . $request->ingreso . ' years'));
          $temp = $request->ingreso + 1;
          $start_date = date('Y-m-d', strtotime($today . ' - ' . $temp . ' years'));
          $start_date = date('Y-m-d', strtotime($start_date . ' + 1 days'));
        }

        if (!is_null($request->edad)){

          $age_end_date = date('Y-m-d', strtotime($today . ' - ' . $request->edad . ' years'));
          $temp = $request->edad + 1;
          $age_start_date = date('Y-m-d', strtotime($today . ' - ' . $temp . ' years'));
          $age_start_date = date('Y-m-d', strtotime($age_start_date . ' + 1 days'));
        }
      }

      $users = User::
      whereIn('id', $respuestas)
      /*->whereHas('employee', function($q1) use($request,$sucursal,$puestos){
          $q1->when(!is_null($request->sucursal), function($q2) use($request,$sucursal){
              $q2->whereIn('job_position_id', $sucursal);
          });
          $q1->when(!is_null($request->direccion), function($q2) use($request){
              $q2->where('direccion', $request->direccion);
          });
          $q1->when(!is_null($request->job), function($q2) use($request,$puestos){
              $q2->whereIn('job_position_id', $puestos);
          });
      })*/


      ->whereHas('employee_wt', function($q1) use($request,$start_date,$end_date,$age_start_date,$age_end_date){
          $q1->when(!is_null($request->sucursal), function($q2) use($request){
              $q2->whereHas('jobPosition', function($q3) use($request){
                $q3->whereHas('area', function($q4) use($request){
                  $q4->where('department_id', $request->sucursal);
                });
              });
          });
          $q1->when(!is_null($request->direccion), function($q2) use($request){
              $q2->whereHas('jobPosition', function($q3) use($request){
                $q3->whereHas('area', function($q4) use($request){
                  $q4->whereHas('department', function($q5) use($request){
                    $q5->where('direction_id', $request->direccion);
                  });
                });
              });
          });
          $q1->when(!is_null($request->job), function($q2) use($request){
              $q2->where('job_position_id', $request->job);
          });
          $q1->when(!is_null($request->sexo), function($q2) use($request){
              $q2->where('sexo', $request->sexo);
          });
          $q1->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
              $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
          });
          $q1->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
              $q2->whereBetween('ingreso', [$start_date, $end_date]);
          });
          $q1->when(!is_null($request->centro_trabajo), function($q2) use($request){
              $q2->where('sucursal', $request->centro_trabajo);
          });
          $q1->when(!is_null($request->regiones), function($q2) use($request){
              $q2->where('region_id', $request->regiones);
          });
      })



      //->with('employee_wt:id,sucursal,direccion,job_position_id')
      ->get();


      $xusers = [];
      foreach ($users as $key => $user) {
        # code...
        $cerrado = \DB::table('nom035_period_user')
        ->where('period_id', $period_id)
        ->whereIn('status',[3, 33])
        ->whereIn('user_id', [$user->id])
        ->count();

        if($cerrado>0){
            $xusers[] = $user->id;
        }
      }

      $users = User::whereIn('id', $xusers)->get();


      $sucursales = [];
      $sucursaless = [];
      $directions = [];
      $directionss = [];
      $puestos_trabajo = [];
      $jobs = [];
      $temp_sexs = [];
      $sexs = [];
      $temp_ages = [];
      $ages = [];
      $temp_starts = [];
      $starts =  [];
      $year_in_seconds = 365*60*60*24;
      $current_year = substr($today, 0, 4) * 1;
      $current_month = substr($today, 5, 2) * 1;
      $current_day = substr($today, 8, 2) * 1;
      $centros_trabajo = [];
      $temp_centros_trabajo = [];

      foreach ($users as $key => $user) {

           if(isset($user->employee_wt->jobPosition->area->department) && !in_array($user->employee_wt->jobPosition->area->department_id, $sucursaless)){ 
              $sucursaless[] = $user->employee_wt->jobPosition->area->department_id; 
              $sucursales[] = ['id'=>$user->employee_wt->jobPosition->area->department_id,'nombre'=>$user->employee_wt->jobPosition->area->department->name]; 
          }

          if (isset($user->employee_wt->jobPosition->area->department->direction)){
            $direction = $user->employee_wt->jobPosition->area->department->direction;
            if(!in_array($direction->id, $directionss)){ 
              $directionss[] = $direction->id;
              $directions[] = ['id'=>$direction->id,'nombre'=>$direction->name]; 
            }
          }

          $job = $user->employee_wt->jobPosition ?? null;
          if($job && !in_array($job->id, $puestos_trabajo)){ 
              
              $puestos_trabajo[] = $job->id; 
      
          }

          if(!in_array($user->employee->sexo, $temp_sexs)){

            if ($user->employee->sexo == 'M'){
              
              $sexs[] = ['id'=>'M','nombre'=>'Masculino'];
            }

            else{

              if ($user->employee->sexo == 'F'){

                $sexs[] = ['id'=>'F','nombre'=>'Femenino'];
              }
            }

            $temp_sexs[] = $user->employee->sexo;
          }

          if (!empty($user->employee->ingreso)){

            $years = $current_year - substr($user->employee->ingreso, 0, 4) * 1;

            if ($years > 0){

              $month = substr($user->employee->ingreso, 5, 2) * 1;

              if ($current_month < $month){

                $years--;
              }

              else{

                if ($current_month == $month){

                  $day = substr($user->employee->ingreso, 8, 2) * 1;

                  if ($current_day < $day){

                    $years--;
                  }
                }
              }
            }

            if(!in_array($years, $temp_starts)){
              
              $starts[] = ['id'=>$years,'nombre'=>$years . ' años'];
              $temp_starts[] = $years;
            }
          }

          if (!empty($user->employee->nacimiento)){

            $years = $current_year - substr($user->employee->nacimiento, 0, 4) * 1;

            if ($years > 0){

              $month = substr($user->employee->nacimiento, 5, 2) * 1;

              if ($current_month < $month){

                $years--;
              }

              else{

                if ($current_month == $month){

                  $day = substr($user->employee->nacimiento, 8, 2) * 1;

                  if ($current_day < $day){

                    $years--;
                  }
                }
              }
            }

            if(!in_array($years, $temp_ages)){
              
              $ages[] = ['id'=>$years,'nombre'=>$years . ' años'];
              $temp_ages[] = $years;
            }
          }

          if(!empty($user->employee->sucursal) && !in_array($user->employee->sucursal, $temp_centros_trabajo)){

            $temp_centros_trabajo[] = $user->employee->sucursal;
            $centros_trabajo[] = ['id'=>$user->employee->sucursal,'nombre'=>$user->employee->sucursal];
          }

      }

      foreach ($puestos_trabajo as $key => $value) {
          # code...
          $jobs[] = JobPosition::where('id',$value)->select('name','id')->first();
      }
      

      if(count($users)<1){

          $sin_data = true;

              return response()->json([
                'sucursales' => $sucursales,
                'directions' => $directions,
                'jobs' => $jobs,
                'sexs' => $sexs,
                'ages' => $ages,
                'starts' => $starts,
                'centros_trabajo' => $centros_trabajo,
                'sin_data' => $sin_data,
                //general           
                'clasificacion_general' => $clasificacion_general,
                'resultado_general' => $resultado_general,
                'color_general' => $color_general,
                //categorias                	
                  'clasificacion_categorias' => $clasificacion_categorias,
                  'clasificacion_categorias_by_usuarios' => $clasificacion_categorias_by_usuarios,
                  'name_categories' => $name_categories,
              //dominios
              'clasificacion_dominios' => $clasificacion_dominios, 
                'clasificacion_dominios_by_usuarios' => $clasificacion_dominios_by_usuarios, 
                'name_dominios' => $name_dominios,
                //dimensiones	                
                  'clasificacion_dimensiones' => $clasificacion_dimensiones,  
                  'name_dimensiones' => $name_dimensiones,
            ], 200);


      }

      //reporte General
      $num_participiantes = 0;
      $total_participiantes = [];

           
      foreach($users as $key => $user){

          $personal = $user->first_name.' '.$user->last_name;

          if(!in_array($user->id, $total_participiantes)){
              $total_participiantes[] = $user->id;
              if ($key>0) {
                  $num_participiantes++;
              }
          }

          $clasificacion_general[$num_participiantes] = [$personal];

              $respuestas = Answer::with('question')
              ->where('user_id', $user->id)
              ->where('period_id', $period_id)
              ->whereHas('user.employee_wt', function($q1) use($request,$start_date,$end_date,$age_start_date,$age_end_date){
                $q1->when(!is_null($request->sucursal), function($q2) use($request){
                  $q2->whereHas('jobPosition', function($q3) use($request){
                    $q3->whereHas('area', function($q4) use($request){
                      $q4->where('department_id', $request->sucursal);
                    });
                  }); 
                });
                $q1->when(!is_null($request->direccion), function($q2) use($request){
                  $q2->whereHas('jobPosition', function($q3) use($request){
                    $q3->whereHas('area', function($q4) use($request){
                      $q4->whereHas('department', function($q5) use($request){
                        $q5->where('direction_id', $request->direccion);
                      });
                    });
                  });
                });
                $q1->when(!is_null($request->job), function($q2) use($request){
                  $q2->where('job_position_id', $request->job);
                });
                $q1->when(!is_null($request->sexo), function($q2) use($request){
                  $q2->where('sexo', $request->sexo);
                });
                $q1->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
                  $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
                });
                $q1->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
                  $q2->whereBetween('ingreso', [$start_date, $end_date]);
                });
                $q1->when(!is_null($request->centro_trabajo), function($q2) use($request){
                  $q2->where('sucursal', $request->centro_trabajo);
                });
                $q1->when(!is_null($request->regiones), function($q2) use($request){
                    $q2->where('region_id', $request->regiones);
                });
              })
              ->get();


              foreach ($respuestas as $value) {

                  $valor = $this->valorespuesta($value->answer , $value->question->positive);
                  $valor_total += $this->valorespuesta($value->answer , $value->question->positive);

              }

              $clasificacion_general[$num_participiantes]['valor'] = $valor_total;

              $color = $this->setColorGeneral($valor_total, $id_cuestionario);
              $clasificacion_general[$num_participiantes]['color'] = $color;
              
              $valor_total = 0;
          
      }

      $num_participiantes = 0;

      foreach ($clasificacion_general as $key => $value) {

          $num_participiantes++;
          $valor_general += $value['valor'];

      }

      $resultado_general = $valor_general / $num_participiantes;
      $color_general = $this->setColorGeneral($resultado_general, $id_cuestionario);

      //fin reporte General
      //fin reporte General
      //fin reporte General



      //reporte categoerias
      //reporte categoerias
      //reporte categoerias


      //categorias
      //categorias
      //categorias
      $valores = array();
      $clasificacion_categorias_by_usuarios = array();
      $clasificacion_categorias = array();
      $total_participiantes = array();

      $valor_total_Cat = 0;
      $num_participiantes = 0;
      $promedio = 0;
      $valor_total = 0;
      $promedio_general = 0;

      foreach($categories as $key => $category){

          $category_id = $category['id'];
          $category_name = $category['name'];

          $respuestas = Answer::with('question')            
          ->whereHas('period', function($q) use($period_id){
              $q->where('id', $period_id);
          })
          ->whereHas('question', function($q) use ($category_id){
              $q->where('category_id', $category_id);
          })
          ->whereHas('user.employee_wt', function($q) use($request,$start_date,$end_date,$age_start_date,$age_end_date){
          $q->when(!is_null($request->sucursal), function($q2) use($request){
              $q2->whereHas('jobPosition', function($q3) use($request){
                $q3->whereHas('area', function($q4) use($request){
                  $q4->where('department_id', $request->sucursal);
                });
              }); 
          });
          $q->when(!is_null($request->direccion), function($q2) use($request){
              $q2->whereHas('jobPosition', function($q3) use($request){
                $q3->whereHas('area', function($q4) use($request){
                  $q4->whereHas('department', function($q5) use($request){
                    $q5->where('direction_id', $request->direccion);
                  });
                });
              });
          });
          $q->when(!is_null($request->job), function($q2) use($request){
              $q2->where('job_position_id', $request->job);
          });
          $q->when(!is_null($request->sexo), function($q2) use($request){
              $q2->where('sexo', $request->sexo);
          });
          $q->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
              $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
          });
          $q->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
              $q2->whereBetween('ingreso', [$start_date, $end_date]);
          });
          $q->when(!is_null($request->centro_trabajo), function($q2) use($request){
            $q2->where('sucursal', $request->centro_trabajo);
          });
          $q->when(!is_null($request->regiones), function($q2) use($request){
              $q2->where('region_id', $request->regiones);
          });
        })
        ->get();            

          foreach ($respuestas as $value) {

                
            $cerrado = \DB::table('nom035_period_user')
            ->where('period_id', $period_id)
            ->whereIn('status',[3, 33])
            ->whereIn('user_id', [$value->user_id])
            ->count();
        
            if(!in_array($value->user_id, $total_participiantes) && $cerrado>0){
            // if(!in_array($value->user_id, $total_participiantes)){
                  $total_participiantes[] = $value->user_id;
                  $num_participiantes++;
              }

              $valor_total_Cat += $this->valorespuesta($value->answer , $value->question->positive);
              
              $clasificacion_categorias[$key]['name'] = $category_name;// $respuestas;

          }
          if($num_participiantes>0){
              $promedio = $valor_total_Cat/$num_participiantes;
          }else {$promedio=0;}
          $setPromedio = $this->setPromedio($category_name, $promedio,$id_cuestionario);
          $color = $this->setColor($category_name, $promedio, $id_cuestionario);
          $critico = $this->setCritico($category_name, $promedio, $id_cuestionario);
          $promedio_general += $setPromedio;
          $clasificacion_categorias[$key]['critico'] = [$setPromedio];// $respuestas;
          $clasificacion_categorias[$key]['data'] = [$promedio];// $respuestas;
          $clasificacion_categorias[$key]['color'] = $critico;// $respuestas;
          $clasificacion_categorias[$key]['colors'] = $color;// $respuestas;

          $valor_total_Cat = 0;


      }
 
      $promedio_general = round($promedio_general / 5);
      $color_general = $this->setCriticoGeneal($promedio_general);

      $color = $this->setColorGeneral($valor_total, $id_cuestionario);
      $num_participiantes = 0;
      $total_participiantes = [];

      $tables_cat = $tables_dom = $tables_dim = [
        'sex'=>[
            'name' => 'Sexo',
            'key' => 'sex',
            'headers' => [
              'Sexo','Totales'
            ],
            'rows' => [
                'women' => ['name' => 'Mujeres','total' => 0],
                'man' => ['name' => 'Hombres','total' => 0],
            ]
        ]
        ,
        'antiquity'=>[
            'name' => 'Antigüedad',
            'key' => 'antiquity',
            'headers' => [
              'Aniguedad','Personas'
            ],
            'rows' => [
                'less_than_a_year' => ['name' => 'Un año o menos','total' => 0],
                'one_to_five_years' => ['name' => 'Un año un día a cinco años','total' => 0],
                'five_to_ten_years' => ['name' => 'Cinco años un día a diez años','total' => 0],
                'ten_to_fifteen_years' => ['name' => 'Diez años un día y más','total' => 0], 
            ]
        ], 
        'generations' => [
            'name' => 'Generación',
            'headers' => [
              'Nacimiento','Generación','Totales'
            ],
            'key' => 'generations',
            'rows' => $this->getGenerationRows()
        ],
        'turnos'=>[
          'name' => 'Turnos',
          'key' => 'turnos',
          'headers' => [
            'Turnos','Personas'
          ],
          'rows' => [
              'diurno' => ['name' => 'Diurno','total' => 0],
              'vespertino' => ['name' => 'Vespertino','total' => 0],
              'nocturno' => ['name' => 'Nocturno','total' => 0],
              'matutino' => ['name' => 'Matutino','total' => 0],
          ]
      ],
      ];
      // dd($tables_cat);

      $real_now = Carbon::now();
      foreach($users as $key => $user){


          $personal = $user->first_name.' '.$user->last_name;
          $department = (isset($user->employee_wt->jobPosition->area->department) ? $user->employee_wt->jobPosition->area->department->name : ' ');
          $area = (isset($user->employee_wt->jobPosition->area->department->direction) ? $user->employee_wt->jobPosition->area->department->direction->name : ' ');
          $jobPosition = $user->employee_wt->jobPosition->name;
          $sucursal = (!empty($user->employee_wt->sucursal) ? $user->employee_wt->sucursal : ' ');
          $empresa = (!empty($user->employee_wt->enterprise) ? $user->employee_wt->enterprise->name : ' ');
          $personal = $personal . '*' . $department . '#' . $area . '=' . $jobPosition . '$' . $sucursal . '[' . $empresa;

          if(!in_array($user->id, $total_participiantes)){
              $total_participiantes[] = $user->id;
              if ($key>0) {
                  $num_participiantes++;
              }
          }

          //sexo

          $clasificacion_categorias_by_usuarios[$num_participiantes] = [$personal];

          foreach($categories as $key => $category){

              $category_id = $category['id'];
              $category_name = $category['name'];

              $respuestas = Answer::with('question')
              ->where('user_id', $user->id)
              ->whereHas('period', function($q) use($period_id){
                  $q->where('id', $period_id);
              })
              ->whereHas('question', function($q) use ($category_id){
                  $q->where('category_id', $category_id);
              })
              ->whereHas('user.employee_wt', function($q) use($request,$start_date,$end_date,$age_start_date,$age_end_date){
                $q->when(!is_null($request->sucursal), function($q2) use($request){
                  $q2->whereHas('jobPosition', function($q3) use($request){
                    $q3->whereHas('area', function($q4) use($request){
                      $q4->where('department_id', $request->sucursal);
                    });
                  }); 
                });
                $q->when(!is_null($request->direccion), function($q2) use($request){
                  $q2->whereHas('jobPosition', function($q3) use($request){
                    $q3->whereHas('area', function($q4) use($request){
                      $q4->whereHas('department', function($q5) use($request){
                        $q5->where('direction_id', $request->direccion);
                      });
                    });
                  });
                });
                $q->when(!is_null($request->job), function($q2) use($request){
                  $q2->where('job_position_id', $request->job);
                });
                $q->when(!is_null($request->sexo), function($q2) use($request){
                  $q2->where('sexo', $request->sexo);
                });
                $q->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
                  $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
                });
                $q->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
                  $q2->whereBetween('ingreso', [$start_date, $end_date]);
                });
                $q->when(!is_null($request->centro_trabajo), function($q2) use($request){
                  $q2->where('sucursal', $request->centro_trabajo);
                });
                $q->when(!is_null($request->regiones), function($q2) use($request){
                    $q2->where('region_id', $request->regiones);
                });
              })
              ->get();


              foreach ($respuestas as $kys => $value) {

                  $valor = $this->valorespuesta($value->answer , $value->question->positive);
                  $valor_total += $this->valorespuesta($value->answer , $value->question->positive);
                 



















                  
              }

              $clasificacion_categorias_by_usuarios[$num_participiantes][$key+1]['valor'] = $valor_total;

              $color = $this->setColor($category_name, $valor_total, $id_cuestionario);
              $clasificacion_categorias_by_usuarios[$num_participiantes][$key+1]['color'] = $color;
              
              $valor_total = 0;
          }

          
      }
      
      //fin reporte categorias
      //fin reporte categorias
      //fin reporte categorias
      //fin reporte categorias






      //dominios

      $valores = array();
      $clasificacion_dominios_by_usuarios = array();
      $clasificacion_dominios = array();
      $total_participiantes = array();
      $valor_total_Cat = 0;
      $num_participiantes = 0;
      $promedio = 0;
      $valor_total = 0;
      $colores_dominios = array();

      foreach($dominios as $key => $category){

          $category_id = $category['id'];
          $category_name = $category['name'];
          $category_real_name = $category['category'];

          $respuestas = Answer::with('question')            
          ->whereHas('period', function($q) use($period_id){
              $q->where('id', $period_id);
          })
          ->whereHas('question', function($q) use ($category_id){
              $q->where('domain_id', $category_id);
          })
          ->whereHas('user.employee_wt', function($q) use($request,$start_date,$end_date,$age_start_date,$age_end_date){
                $q->when(!is_null($request->sucursal), function($q2) use($request){
                  $q2->whereHas('jobPosition', function($q3) use($request){
                    $q3->whereHas('area', function($q4) use($request){
                      $q4->where('department_id', $request->sucursal);
                    });
                  }); 
                });
                $q->when(!is_null($request->direccion), function($q2) use($request){
                  $q2->whereHas('jobPosition', function($q3) use($request){
                    $q3->whereHas('area', function($q4) use($request){
                      $q4->whereHas('department', function($q5) use($request){
                        $q5->where('direction_id', $request->direccion);
                      });
                    });
                  });
                });
                $q->when(!is_null($request->job), function($q2) use($request){
                  $q2->where('job_position_id', $request->job);
                });
                $q->when(!is_null($request->sexo), function($q2) use($request){
                  $q2->where('sexo', $request->sexo);
                });
                $q->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
                  $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
                });
                $q->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
                  $q2->whereBetween('ingreso', [$start_date, $end_date]);
                });
                $q->when(!is_null($request->centro_trabajo), function($q2) use($request){
                  $q2->where('sucursal', $request->centro_trabajo);
                });
                $q->when(!is_null($request->regiones), function($q2) use($request){
                    $q2->where('region_id', $request->regiones);
                });
          })
          ->get();            

          foreach ($respuestas as $value) {

                
            $cerrado = \DB::table('nom035_period_user')
            ->where('period_id', $period_id)
            ->whereIn('status',[3, 33])
            ->whereIn('user_id', [$value->user_id])
            ->count();
        
            if(!in_array($value->user_id, $total_participiantes) && $cerrado>0){
              // if(!in_array($value->user_id, $total_participiantes)){
                  $total_participiantes[] = $value->user_id;
                  $num_participiantes++;
              }

              $valor_total_Cat += $this->valorespuesta($value->answer , $value->question->positive);
              
              $clasificacion_dominios[$key]['id'] = $category_id;// $respuestas;
              $clasificacion_dominios[$key]['name'] = $category_name;// $respuestas;
              $clasificacion_dominios[$key]['category'] = $category_real_name;

          }

          if($num_participiantes>0){
              $promedio = $valor_total_Cat/$num_participiantes;
          }else {$promedio=0;}
          

          $setPromedio = $this->setPromedio($category_name, $promedio,$id_cuestionario);
          $color = $this->setColor($category_name, $promedio, $id_cuestionario);
          $critico = $this->setCritico($category_name, $promedio, $id_cuestionario);

          $clasificacion_dominios[$key]['critico'] = [$setPromedio];// $respuestas;
          $clasificacion_dominios[$key]['data'] = [$promedio];// $respuestas;
          $clasificacion_dominios[$key]['color'] = $critico;// $respuestas;
          $clasificacion_dominios[$key]['colors'] = $color;// $respuestas;
          $colores_dominios[$category_id] = $color;
          $valor_total_Cat = 0;


      }



      $num_participiantes = 0;
      $total_participiantes = [];

           
      foreach($users as $key => $user){

          $personal = $user->first_name.' '.$user->last_name;
          $department = (isset($user->employee_wt->jobPosition->area->department) ? $user->employee_wt->jobPosition->area->department->name : ' ');
          $area = (isset($user->employee_wt->jobPosition->area->department->direction) ? $user->employee_wt->jobPosition->area->department->direction->name : ' ');
          $jobPosition = $user->employee_wt->jobPosition->name;
          $sucursal = (!empty($user->employee_wt->sucursal) ? $user->employee_wt->sucursal : ' ');
          $empresa = (!empty($user->employee_wt->enterprise) ? $user->employee_wt->enterprise->name : ' ');
          $personal = $personal . '*' . $department . '#' . $area . '=' . $jobPosition . '$' . $sucursal . '[' . $empresa;

          if(!in_array($user->id, $total_participiantes)){
                  $total_participiantes[] = $user->id;
                  if ($key>0) {
                      $num_participiantes++;
                  }
              }


          $clasificacion_dominios_by_usuarios[$num_participiantes] = [$personal];

          foreach($dominios as $key => $category){

              $category_id = $category['id'];
              $category_name = $category['name'];

              $respuestas = Answer::with('question')
              ->where('user_id', $user->id)
              ->whereHas('period', function($q) use($period_id){
                  $q->where('id', $period_id);
              })
              ->whereHas('question', function($q) use ($category_id){
                  $q->where('domain_id', $category_id);
              })
              ->whereHas('user.employee_wt', function($q) use($request,$start_date,$end_date,$age_start_date,$age_end_date){
                $q->when(!is_null($request->sucursal), function($q2) use($request){
                  $q2->whereHas('jobPosition', function($q3) use($request){
                    $q3->whereHas('area', function($q4) use($request){
                      $q4->where('department_id', $request->sucursal);
                    });
                  }); 
                });
                $q->when(!is_null($request->direccion), function($q2) use($request){
                  $q2->whereHas('jobPosition', function($q3) use($request){
                    $q3->whereHas('area', function($q4) use($request){
                      $q4->whereHas('department', function($q5) use($request){
                        $q5->where('direction_id', $request->direccion);
                      });
                    });
                  });
                });
                $q->when(!is_null($request->job), function($q2) use($request){
                  $q2->where('job_position_id', $request->job);
                });
                $q->when(!is_null($request->sexo), function($q2) use($request){
                  $q2->where('sexo', $request->sexo);
                });
                $q->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
                  $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
                });
                $q->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
                  $q2->whereBetween('ingreso', [$start_date, $end_date]);
                });
                $q->when(!is_null($request->centro_trabajo), function($q2) use($request){
                  $q2->where('sucursal', $request->centro_trabajo);
                });
                $q->when(!is_null($request->regiones), function($q2) use($request){
                    $q2->where('region_id', $request->regiones);
                });
              })
              ->get();


              foreach ($respuestas as $value) {

                  $valor = $this->valorespuesta($value->answer , $value->question->positive);
                  $valor_total += $this->valorespuesta($value->answer , $value->question->positive);

              }

              $clasificacion_dominios_by_usuarios[$num_participiantes][$key+1]['valor'] = $valor_total;

              $color = $this->setColor($category_name, $valor_total, $id_cuestionario);
              $clasificacion_dominios_by_usuarios[$num_participiantes][$key+1]['color'] = $color;
              
              $valor_total = 0;
          }

          
      }
      
     //fin reporte dominios
      //fin reporte dominios
      //fin reporte dominios
      //fin reporte dominios


      //dimensiones
      //dimensiones
      //dimensiones
      $valores = array();
      $clasificacion_dimensiones_by_usuarios = array();
      $clasificacion_dimensiones = array();
      $total_participiantes = array();

      $valor_total_Cat = 0;
      $num_participiantes = 0;
      $promedio = 0;
      $valor_total = 0;
      $keyss = 0;
      $users_count = 0;


      $filtro_dominio = $request->change_filtro_dominio;
      $dominio_s = Domain::whereId($filtro_dominio)->first();
      $dominio_seleccionado = $dominio_s->name;

      foreach($dimensiones as $keyds => $category){
          $set_color = '#000';
          $category_id = $category['id'];
          $category_name = $category['name'];
          $category_real_name = $category['category'];
          $domain_name = $category['domain'];
          $domain_id = $category['domain_id'];

          
          foreach ($clasificacion_dominios as $dominiClr ) {
            if($dominiClr['id'] == $domain_id){
              $set_color = $dominiClr['color'];
            }
          }


          $respuestas = Answer::with('question')            
          ->whereHas('period', function($q) use($period_id){
              $q->where('id', $period_id);
          })
          ->whereHas('question', function($q) use ($category_id, $filtro_dominio){
              $q->where('dimension_id', $category_id);
              $q->where('domain_id', $filtro_dominio);
          })
          ->whereHas('user.employee_wt', function($q) use($request,$start_date,$end_date,$age_start_date,$age_end_date){
                $q->when(!is_null($request->sucursal), function($q2) use($request){
                  $q2->whereHas('jobPosition', function($q3) use($request){
                    $q3->whereHas('area', function($q4) use($request){
                      $q4->where('department_id', $request->sucursal);
                    });
                  }); 
                });
                $q->when(!is_null($request->direccion), function($q2) use($request){
                  $q2->whereHas('jobPosition', function($q3) use($request){
                    $q3->whereHas('area', function($q4) use($request){
                      $q4->whereHas('department', function($q5) use($request){
                        $q5->where('direction_id', $request->direccion);
                      });
                    });
                  });
                });
                $q->when(!is_null($request->job), function($q2) use($request){
                  $q2->where('job_position_id', $request->job);
                });
                $q->when(!is_null($request->sexo), function($q2) use($request){
                  $q2->where('sexo', $request->sexo);
                });
                $q->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
                  $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
                });
                $q->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
                  $q2->whereBetween('ingreso', [$start_date, $end_date]);
                });
                $q->when(!is_null($request->centro_trabajo), function($q2) use($request){
                  $q2->where('sucursal', $request->centro_trabajo);
                });
                $q->when(!is_null($request->regiones), function($q2) use($request){
                    $q2->where('region_id', $request->regiones);
                });
          })
          ->get();            



          foreach ($respuestas as $value) {

                
            $cerrado = \DB::table('nom035_period_user')
            ->where('period_id', $period_id)
            ->whereIn('status',[3, 33])
            ->whereIn('user_id', [$value->user_id])
            ->count();
        
            if(!in_array($value->user_id, $total_participiantes) && $cerrado>0){
              // if(!in_array($value->user_id, $total_participiantes)){
                  $total_participiantes[] = $value->user_id;
                  $num_participiantes++;

                  $users_count++;
                  $user = $value->user;
                  if(isset($user->employee_wt)){
                    if($user->employee_wt->sexo == 'M'){
                      $tables_dim['sex']['rows']['man']['total'] += 1;
                    }else{
                      $tables_dim['sex']['rows']['women']['total'] += 1;
                    }
                  } 
        
                    
                  $entry = Carbon::parse($user->employee_wt->ingreso);
                  $birthday = Carbon::parse($user->employee_wt->nacimiento);
                  $antiquity = $real_now->diffInDays($entry)/365;
                  if($antiquity < 1) {
                      $tables_dim['antiquity']['rows']  ['less_than_a_year']['total'] += 1;
                      // $analytics['antiquity'][$employee->sexo]['less_than_a_year'] += 1;
                  }
                  elseif($antiquity >= 1 && $antiquity <= 5) {    
                    $tables_dim['antiquity']['rows']['one_to_five_years']['total'] += 1;         
                      // $analytics['antiquity'][$employee->sexo]['one_to_five_years'] += 1;
                  } elseif($antiquity > 5 && $antiquity <= 10) {
                    $tables_dim['antiquity']['rows']['five_to_ten_years']['total'] += 1;
                      // $analytics['antiquity'][$employee->sexo]['five_to_ten_years'] += 1;
                  } elseif($antiquity > 10 && $antiquity <= 15) {
                    $tables_dim['antiquity']['rows']['ten_to_fifteen_years']['total'] += 1;
                      // $analytics['antiquity'][$employee->sexo]['ten_to_fifteen_years'] += 1;
                  } elseif($antiquity > 15) {
                    $tables_dim['antiquity']['rows']['ten_to_fifteen_years']['total'] += 1;
                      // $analytics['antiquity'][$employee->sexo]['more_than_fifteen_years'] += 1;
                  }
        
        
                  // // Generation
                  $generation_row = Generation::getRangeKeyByBirthDay($birthday->year);
                  // dd($birthday->year, $generation_row);
                  if($generation_row) {
                      // $analytics_detail['generations'][$employee->sexo][$generation_row][] = $employee;
                      $tables_dim['generations']['rows'][$generation_row]['total']   += 1;
                  }

 
              }

              $valor_total_Cat += $this->valorespuesta($value->answer , $value->question->positive);
              
              // $clasificacion_dimensiones[$keyss] = [
              //   "name" => $category_name,
              //   "valor" => $valor_total_Cat,
              // ];

              $clasificacion_dimensiones[$keyss]['name'] = $category_name;// $respuestas;
              $clasificacion_dimensiones[$keyss]['valor'] = $valor_total_Cat;// $respuestas;
              $clasificacion_dimensiones[$keyss]['participantes'] = $num_participiantes;// $respuestas;
              $clasificacion_dimensiones[$keyss]['category'] = $category_real_name;// $respuestas;
              $clasificacion_dimensiones[$keyss]['domain'] = $domain_name;// $respuestas;
              $clasificacion_dimensiones[$keyss]['domain_id'] = $domain_id;// $respuestas;
              $clasificacion_dimensiones[$keyss]['data'] = [0];























          }

          if($num_participiantes>0){
              $promedio = $valor_total_Cat/$num_participiantes;
          }else {$promedio=0;}
          

          $setPromedio = $this->setPromedio($category_name, $promedio,$id_cuestionario);
          $color = $this->setColor($category_name, $promedio, $id_cuestionario);
          $critico = $this->setCritico($category_name, $promedio, $id_cuestionario);

          if(count($respuestas)>0){
              $clasificacion_dimensiones[$keyss]['critico'] = [$setPromedio];// $respuestas;
              $clasificacion_dimensiones[$keyss]['data'] = [$promedio];// $respuestas;
              $clasificacion_dimensiones[$keyss]['color'] = $set_color;// $respuestas;
              $clasificacion_dimensiones[$keyss]['colors'] = $color;// $respuestas;

              $valor_total_Cat = 0;
          }

          $keyss++;
      }
      //fin dimensiones
      //fin dimensiones
      //fin dimensiones
      //fin dimensiones
      $cdi = array();
      foreach ($clasificacion_dimensiones as $key => $value) {
        $cdi[] = $value;
      }

      $clasificacion_dimensiones = $cdi;
 
          return response()->json([
              'filtro_dominio' => $filtro_dominio,
              'dominio_seleccionado' => $dominio_seleccionado,
              'sucursales' => $sucursales,
              'directions' => $directions,
              'jobs' => $jobs,
              'sexs' => $sexs,
              'ages' => $ages,
              'starts' => $starts,
              'centros_trabajo' => $centros_trabajo,
              //general
            'promedio_general' => $promedio_general, 
            'clasificacion_general' => $clasificacion_general, 
            'resultado_general' => $resultado_general, 
            'color_general' => $color_general,
            //categorias
            'clasificacion_categorias' => $clasificacion_categorias, 
            'clasificacion_categorias_by_usuarios' => $clasificacion_categorias_by_usuarios, 
            'name_categories' => $name_categories,
            //dominios
            'clasificacion_dominios' => $clasificacion_dominios, 
              'clasificacion_dominios_by_usuarios' => $clasificacion_dominios_by_usuarios, 
              'name_dominios' => $name_dominios,
              //dimensiones
            'clasificacion_dimensiones' => $clasificacion_dimensiones,  
            'name_dimensiones' => $name_dimensiones,
            'colores_dominios' => $colores_dominios,
            'tables_dim' => $tables_dim,           
            'users_count' =>  $users_count
      ], 200);

        


  }















  
  public function tablero_calificacion($calificacion, Request $request){

    $clasificacion_general = array();
    $valor_total = 0;
    $color = 0;
    $num_participiantes = 0;
    $valor_general = 0;
    $resultado_general = 0;
    $color_general = "";
    $clasificacion_categorias = "";
    $clasificacion_categorias_by_usuarios = "";
    $clasificacion_dominios = "";
    $clasificacion_dimensiones = "";
    $clasificacion_dominios_by_usuarios = "";
    $clasificacion_dimensiones_by_usuarios = "";
    $dominio_seleccionado = "";
    $periodos = $users = array();
    $period_id = 0;
    $promedio_general = 0;
    $admin = true;

    if (auth()->user()->role == 'admin'){

      $periodos = NormPeriod::where('status','Abierto')->orWhere('status', 'Cerrado')->select('id', 'name', 'status')->orderBy('id', 'DESC')->get();
    
    } else{

      $permissions = auth()->user()->multiUserPermissions()->orderBy('enterprise_id')->orderBy('sucursal_id')->get();

      if (count($permissions)>0 && $permissions[0]->enterprise_id == 0){

        $periodos = NormPeriod::where('status','Abierto')->orWhere('status', 'Cerrado')->select('id', 'name', 'status')->orderBy('id', 'DESC')->get();
      
      }else{

        $admin = false;
        $enterprises_without_sucursals = $enterprises_with_sucursals = array();
        $sucursals = array();

        foreach ($permissions as $key => $value){
          
          if ($value->sucursal_id == 0){

            if (!in_array($value->enterprise_id, $enterprises_without_sucursals)){

              $enterprises_without_sucursals[] = $value->enterprise_id;
            }
          }

          else{

            if (!in_array($value->enterprise_id, $enterprises_without_sucursals) && !in_array($value->sucursal_id, $sucursals)){

              $enterprises_with_sucursals[] = $value->enterprise_id;
              $sucursals[] = $value->sucursal_id;
            }
          }
        }

        $periods = array();

        if (!empty($enterprises_without_sucursals)){

          $periods = NormPeriod::whereIn('enterprise_id', $enterprises_without_sucursals)->pluck('id')->toArray();
        }

        if (!empty($sucursals)){

          foreach ($sucursals as $key => $value){
              
            $periods2 = NormPeriod::whereIn('enterprise_id', $enterprises_with_sucursals)->whereIn('sucursal_id', $sucursals)->pluck('id')->toArray();

            if (!empty($periods2)){

              $periods = array_merge($periods, $periods2);
            }
          }
        }

        if (!empty($periods)){

          $periodos = NormPeriod::whereIn('id', $periods)->select('id', 'name', 'status')->orderBy('id', 'DESC')->get();
        }else{
          
          $user_periods = \DB::table('nom035_period_user')->where('user_id', auth()->user()->id)->pluck('period_id')->toArray();
          $periodos = NormPeriod::where('status', 'Cerrado')->whereIn('id', $user_periods)->orderBy('id', 'DESC')->get();
          
        }

      }
      
    }

    if(!$request->periodo || Session::has('period_without_results')){

      if (count($periodos) > 0){

        $periodo = $periodos[0];

      }

      else{

            $sin_periodos = true;
            $name_categories = true;
            $name_dominios = true;
            $name_dimensiones = true;
            $clasificacion_categorias = [];
            $clasificacion_dominios = [];
            $clasificacion_dimensiones = [];

            return view('nom035.graficos_by.index', compact('sin_periodos', 'periodos', 'period_id',
                'name_categories',
                'name_dominios',
                'name_dimensiones',
                'clasificacion_categorias',
                'clasificacion_dominios',
                'clasificacion_dimensiones'
            ));

      }

      if (!$request->periodo && empty($_POST['period_id'])){
      
        $period_id = $periodo->id;
      }

      else{

        $period_id = $_POST['period_id'];
        Session::forget('period_without_results');
        $request->periodo = null;
      }

    }else{

        if($request->cambio =='periodo'){
            $request['sucursal'] = null;
            $request['direccion'] = null;
            $request['job'] = null;
            $request['sexo'] = null;
            $request['edad'] = null;
            $request['ingreso'] = null;
            $request['centro_trabajo'] = null;
        }
        else if($request->cambio=='sucursal'){
        
            $request['direccion'] = null;
            $request['job'] = null;

      }else if($request->cambio=='area'){

            $request['job'] = null;
            $request['sexo'] = null;
            $request['edad'] = null;
            $request['ingreso'] = null;
            $request['centro_trabajo'] = null;

      }else if($request->cambio=='edad'){

            $request['sexo'] = null;
            $request['ingreso'] = null;
            $request['centro_trabajo'] = null;

      }else if($request->cambio=='inicio'){

            $request['sexo'] = null;
            $request['edad'] = null;
            $request['centro_trabajo'] = null;

      }else if($request->cambio=='sexo'){

            $request['edad'] = null;
            $request['ingreso'] = null;
            $request['centro_trabajo'] = null;
      
      }else if($request->cambio=='centro_trabajo'){

            $request['edad'] = null;
            $request['ingreso'] = null;
            $request['sexo'] = null;
      }

    $period_id = $request->periodo;



    }



    //$sucursal = array();
    //$Areas = Area::where('department_id', $request['sucursal'])->get();
    //$sucursal = JobPosition::where('id_department', $request['sucursal'])->pluck('id')->toArray();

    /*foreach ($Areas as $key => $Area) {                
        
        $JobPositions = JobPosition::where('area_id',$Area->id)->get();

            foreach ($JobPositions as $key => $JobPosition) {                                                                       
                $sucursal[] = $JobPosition->id;

            }

    }*/

    /*$puestos = array();
    $JobPositionLevels = JobPositionLevel::where('id',$request['job'])->get();

    foreach ($JobPositionLevels as $key => $JobPositionLevel) {   

         $JobPositions0 = JobPosition::where('job_position_level_id',$JobPositionLevel->id)->get();

        foreach ($JobPositions0 as $key => $JobPosition0) {                                                                       
           $puestos[] = $JobPosition0->id;

        }

        

    }*/


    //categorias
    $listcategory = array();
    $categories = array();
    $id_cuestionario = null;
    $PeriodQuestion = PeriodQuestion::with('question')->where('period_id',$period_id)->get();
    foreach ($PeriodQuestion as $key => $PeriodQ) {
        if(!in_array($PeriodQ->question->category_id, $listcategory)){
            $listcategory[] = $PeriodQ->question->category_id;
            if(isset($PeriodQ->question->category->name)){
                 $categories[] = ['id'=>$PeriodQ->question->category->id, 'name'=>$PeriodQ->question->category->name];
                 $id_cuestionario = $PeriodQ->question->cuestionario;
            }
        }
    }
    $name_categories = $categories;
    //categorias



    //dominios
    $listcategory = array();
    $dominios = array();
    $id_cuestionario = null;
    $PeriodQuestion = PeriodQuestion::with('question')->where('period_id',$period_id)->get();
    foreach ($PeriodQuestion as $key => $PeriodQ) {
        if(!in_array($PeriodQ->question->domain_id, $listcategory)){
            $listcategory[] = $PeriodQ->question->domain_id;
            if(isset($PeriodQ->question->domain->name)){
                 $dominios[] = ['id'=>$PeriodQ->question->domain->id, 'name'=>$PeriodQ->question->domain->name,'category'=>$PeriodQ->question->domain->category->name,'category_id'=>$PeriodQ->question->domain->category->id];
                 $id_cuestionario = $PeriodQ->question->cuestionario;
            }
        }
    }
    $name_dominios = $dominios;
    //dominios


    //dimensioness        
    $listcategory = array();
    $dimensiones = array();
    $id_cuestionario = null;
    $PeriodQuestion = PeriodQuestion::with('question')->where('period_id',$period_id)->get();
    foreach ($PeriodQuestion as $key => $PeriodQ) {
        if(!in_array($PeriodQ->question->dimension_id, $listcategory)){
            $listcategory[] = $PeriodQ->question->dimension_id;
            if(isset($PeriodQ->question->dimension->name)){
                 $dimensiones[] = ['id'=>$PeriodQ->question->dimension->id, 'name'=>$PeriodQ->question->dimension->name,'category'=>$PeriodQ->question->dimension->domain->category->name,'domain'=>$PeriodQ->question->dimension->domain->name,'domain_id'=>$PeriodQ->question->dimension->domain->id];
                 $id_cuestionario = $PeriodQ->question->cuestionario;
            }
        }
    }
    $name_dimensiones = $dimensiones;
    //dimensiones

    $questions = DB::table('nom035_questions')->where('cuestionario', 'Cuestionario 2')->orWhere('cuestionario', 'Cuestionario 3')->pluck('id')->toArray();
    $respuestas = array();

    if ($admin){

      $respuestas = Answer::groupBy('user_id')
      ->where('period_id', $period_id)
      ->whereIn('question_id', $questions)
      ->pluck('user_id')->toArray();
    }

    else{

      $respuestas = Answer::groupBy('user_id')
      ->where('period_id', $period_id)
      ->whereIn('question_id', $questions)
      // ->whereIn('user_id', $users)
      ->pluck('user_id')->toArray();
    }

    $today = date('Y-m-d');
    $fecha_actual = date('Y-m-d');
    $end_date = $start_date = $age_start_date = $age_end_date = 0;

    if (!is_null($request->edad) || !is_null($request->ingreso)){

      if (!is_null($request->ingreso)){

      $antiquity = $request->ingreso;

        if($antiquity =='less_than_a_year') {
            $end_date = date('Y-m-d'); 
            $start_date = date("Y-m-d",strtotime($fecha_actual."- 365 days"));              
        }
        else if($antiquity =='one_to_five_years') {    
          $end_date = date("Y-m-d",strtotime($fecha_actual."- 366 days"));  
          $start_date = date("Y-m-d",strtotime($fecha_actual."- 1825 days"));   
        } else if($antiquity =='five_to_ten_years') {
          $end_date = date("Y-m-d",strtotime($fecha_actual."- 1826 days"));  
          $start_date = date("Y-m-d",strtotime($fecha_actual."- 3650 days"));   
        } else if($antiquity =='ten_to_fifteen_years') {
          $end_date = date("Y-m-d",strtotime($fecha_actual."- 3650 days"));  
          $start_date = date("Y-m-d",strtotime($fecha_actual."- 7300 days")); 
        }


        // $end_date = date('Y-m-d', strtotime($today . ' - ' . $request->ingreso . ' years'));
        // $temp = $request->ingreso + 1;
        // $start_date = date('Y-m-d', strtotime($today . ' - ' . $temp . ' years'));
        // $start_date = date('Y-m-d', strtotime($start_date . ' + 1 days'));
        
      }

      if (!is_null($request->edad)){
        
        $gen = Generation::whereId($request->edad)->first();
        if(!is_null($gen->end)){
          $age_end_date = $gen->end.'-12-31';
        }else{
          $age_end_date = date('Y-m-d');
        }
         
        $age_start_date = $gen->start.'-01-31';

      }

    }


      

    // dd($respuestas);
    $users = User::
    whereIn('id', $respuestas)
    /*->whereHas('employee', function($q1) use($request,$sucursal,$puestos){
        $q1->when(!is_null($request->sucursal), function($q2) use($request,$sucursal){
            $q2->whereIn('job_position_id', $sucursal);
        });
        $q1->when(!is_null($request->direccion), function($q2) use($request){
            $q2->where('direccion', $request->direccion);
        });
        $q1->when(!is_null($request->job), function($q2) use($request,$puestos){
            $q2->whereIn('job_position_id', $puestos);
        });
    })*/


    ->whereHas('employee_wt', function($q1) use($request,$start_date,$end_date,$age_start_date,$age_end_date){
        $q1->when(!is_null($request->sucursal), function($q2) use($request){
            $q2->whereHas('jobPosition', function($q3) use($request){
              $q3->whereHas('area', function($q4) use($request){
                $q4->where('department_id', $request->sucursal);
              });
            });
        });
        $q1->when(!is_null($request->direccion), function($q2) use($request){
            $q2->whereHas('jobPosition', function($q3) use($request){
              $q3->whereHas('area', function($q4) use($request){
                $q4->whereHas('department', function($q5) use($request){
                  $q5->where('direction_id', $request->direccion);
                });
              });
            });
        });
        $q1->when(!is_null($request->job), function($q2) use($request){
            $q2->where('job_position_id', $request->job);
        });
        $q1->when(!is_null($request->sexo), function($q2) use($request){
            $q2->where('sexo', $request->sexo);
        });
        $q1->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
            $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
        });
        $q1->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
            $q2->whereBetween('ingreso', [$start_date, $end_date]);
        });
        $q1->when(!is_null($request->centro_trabajo), function($q2) use($request){
            $q2->where('sucursal', $request->centro_trabajo);
        });
        $q1->when(!is_null($request->regiones), function($q2) use($request){
            $q2->where('region_id', $request->regiones);
        });
    })



    //->with('employee_wt:id,sucursal,direccion,job_position_id')
    ->get();

      $xusers = [];
      foreach ($users as $key => $user) {
        # code...
        $cerrado = \DB::table('nom035_period_user')
        ->where('period_id', $period_id)
        ->whereIn('status',[3, 33])
        ->whereIn('user_id', [$user->id])
        ->count();

        if($cerrado>0){
            $xusers[] = $user->id;
        }
      }

      $users = User::whereIn('id', $xusers)->get();
    $sucursales = [];
    $sucursaless = [];
    $directions = [];
    $directionss = [];
    $puestos_trabajo = [];
    $jobs = [];
    $temp_sexs = [];
    $sexs = [];
    $temp_ages = [];
    $ages = [];
    $temp_starts = [];
    $starts = [];
    $year_in_seconds = 365*60*60*24;
    $current_year = substr($today, 0, 4) * 1;
    $current_month = substr($today, 5, 2) * 1;
    $current_day = substr($today, 8, 2) * 1;
    $centros_trabajo = [];
    $regiones = [];
    $temp_centros_trabajo = [];
    $temp_regiones = [];

    foreach ($users as $key => $user) {

         if(isset($user->employee_wt->jobPosition->area->department) && !in_array($user->employee_wt->jobPosition->area->department_id, $sucursaless)){ 
            $sucursaless[] = $user->employee_wt->jobPosition->area->department_id; 
            $sucursales[] = ['id'=>$user->employee_wt->jobPosition->area->department_id,'nombre'=>$user->employee_wt->jobPosition->area->department->name]; 
        }

        if (isset($user->employee_wt->jobPosition->area->department->direction)){
          $direction = $user->employee_wt->jobPosition->area->department->direction;
          if(!in_array($direction->id, $directionss)){ 
            $directionss[] = $direction->id;
            $directions[] = ['id'=>$direction->id,'nombre'=>$direction->name]; 
          }
        }

        $job = $user->employee_wt->jobPosition ?? null;
        if($job && !in_array($job->id, $puestos_trabajo)){ 
            
            $puestos_trabajo[] = $job->id; 
    
        }

        if(!in_array($user->employee->sexo, $temp_sexs)){

          if ($user->employee->sexo == 'M'){
            
            $sexs[] = ['id'=>'M','nombre'=>'Masculino'];
          }

          else{

            if ($user->employee->sexo == 'F'){

              $sexs[] = ['id'=>'F','nombre'=>'Femenino'];
            }
          }

          $temp_sexs[] = $user->employee->sexo;
        }

        if (!empty($user->employee->ingreso)){

          $years = $current_year - substr($user->employee->ingreso, 0, 4) * 1;

          if ($years > 0){

            $month = substr($user->employee->ingreso, 5, 2) * 1;

            if ($current_month < $month){

              $years--;
            }

            else{

              if ($current_month == $month){

                $day = substr($user->employee->ingreso, 8, 2) * 1;

                if ($current_day < $day){

                  $years--;
                }
              }
            }
          }

        }

        if (!empty($user->employee->nacimiento)){

          $years = $current_year - substr($user->employee->nacimiento, 0, 4) * 1;

          if ($years > 0){

            $month = substr($user->employee->nacimiento, 5, 2) * 1;

            if ($current_month < $month){

              $years--;
            }

            else{

              if ($current_month == $month){

                $day = substr($user->employee->nacimiento, 8, 2) * 1;

                if ($current_day < $day){

                  $years--;
                }
              }
            }
          }

        }

        if(!empty($user->employee->sucursal) && !in_array($user->employee->sucursal, $temp_centros_trabajo)){

          $temp_centros_trabajo[] = $user->employee->sucursal;
          $centros_trabajo[] = ['id'=>$user->employee->sucursal,'nombre'=>$user->employee->sucursal];
          $tabla_dominio_centros_trabajo[$user->employee->sucursal] = ['id'=>$user->employee->sucursal,'nombre'=>$user->employee->sucursal,'valor'=>0,'resultado'=>0];
        }

        if(!empty($user->employee->region_id) && !in_array($user->employee->region_id, $temp_regiones)){

          $temp_regiones[] = $user->employee->region_id;
          $regiones[] = ['id'=>$user->employee->region->id,'nombre'=>$user->employee->region->name];
          $tabla_dominio_hospitales[$user->employee->region->name] = ['id'=>$user->employee->region->id,'nombre'=>$user->employee->region->name,'valor'=>0,'resultado'=>0];
       
        }

    }

    foreach ($puestos_trabajo as $key => $value) {
        # code...
        $jobs[] = JobPosition::where('id',$value)->select('name','id')->first();
    }
    

    if(count($users)<1){

        $sin_data = true;

        if(!is_null($request->sucursal) || !is_null($request->direccion) ||  !is_null($request->job) || !is_null($request->edad) || !is_null($request->sexo) || !is_null($request->ingreso) || !is_null($request->centro_trabajo) || !is_null($request->periodo)){



            return response()->json([
              'sucursales' => $sucursales,
              'directions' => $directions,
              'jobs' => $jobs,
              'sexs' => $sexs,
              'ages' => $ages,
              'starts' => $starts,
              'regiones' => $regiones,
              'centros_trabajo' => $centros_trabajo,
              'sin_data' => $sin_data,
              //general           
              'clasificacion_general' => $clasificacion_general,
              'resultado_general' => $resultado_general,
              'color_general' => $color_general,
              //categorias                	
                'clasificacion_categorias' => $clasificacion_categorias,
                'clasificacion_categorias_by_usuarios' => $clasificacion_categorias_by_usuarios,
                'name_categories' => $name_categories,
            //dominios
            'clasificacion_dominios' => $clasificacion_dominios, 
              'clasificacion_dominios_by_usuarios' => $clasificacion_dominios_by_usuarios, 
              'name_dominios' => $name_dominios,
              //dimensiones	                
                'clasificacion_dimensiones' => $clasificacion_dimensiones,  
                'name_dimensiones' => $name_dimensiones,
          ], 200);

        }else{
            
            $sin_periodos = true;
            $name_categories = true;
            $name_dominios = true;
            $name_dimensiones = true;
            $clasificacion_categorias = true;
            $clasificacion_dominios = true;
            $clasificacion_dimensiones = true;
            Session::put('period_without_results', 1);

            return view('nom035.graficos_by.'+$calificacion, compact('sin_periodos', 'periodos', 'period_id',
                'name_categories',
                'name_dominios',
                'name_dimensiones',
                'clasificacion_categorias',
                'clasificacion_dominios',
                'clasificacion_dimensiones'
            ));

        }

    }

    //reporte General
    $num_participiantes = 0;
    $total_participiantes = [];

         
    foreach($users as $key => $user){

        $personal = $user->first_name.' '.$user->last_name;

        if(!in_array($user->id, $total_participiantes)){
            $total_participiantes[] = $user->id;
            if ($key>0) {
                $num_participiantes++;
            }
        }

        $clasificacion_general[$num_participiantes] = [$personal];

            $respuestas = Answer::with('question')
            ->where('user_id', $user->id)
            ->where('period_id', $period_id)
            ->whereHas('user.employee_wt', function($q1) use($request,$start_date,$end_date,$age_start_date,$age_end_date){
              $q1->when(!is_null($request->sucursal), function($q2) use($request){
                $q2->whereHas('jobPosition', function($q3) use($request){
                  $q3->whereHas('area', function($q4) use($request){
                    $q4->where('department_id', $request->sucursal);
                  });
                }); 
              });
              $q1->when(!is_null($request->direccion), function($q2) use($request){
                $q2->whereHas('jobPosition', function($q3) use($request){
                  $q3->whereHas('area', function($q4) use($request){
                    $q4->whereHas('department', function($q5) use($request){
                      $q5->where('direction_id', $request->direccion);
                    });
                  });
                });
              });
              $q1->when(!is_null($request->job), function($q2) use($request){
                $q2->where('job_position_id', $request->job);
              });
              $q1->when(!is_null($request->sexo), function($q2) use($request){
                $q2->where('sexo', $request->sexo);
              });
              $q1->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
                $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
              });
              $q1->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
                $q2->whereBetween('ingreso', [$start_date, $end_date]);
              });
              $q1->when(!is_null($request->centro_trabajo), function($q2) use($request){
                $q2->where('sucursal', $request->centro_trabajo);
              });
            })
            ->get();


            foreach ($respuestas as $value) {

                $valor = $this->valorespuesta($value->answer , $value->question->positive);
                $valor_total += $this->valorespuesta($value->answer , $value->question->positive);

            }

            $clasificacion_general[$num_participiantes]['valor'] = $valor_total;

            $color = $this->setColorGeneral($valor_total, $id_cuestionario);
            $clasificacion_general[$num_participiantes]['color'] = $color;
            
            $valor_total = 0;
        
    }

    $num_participiantes = 0;

    foreach ($clasificacion_general as $key => $value) {

        $num_participiantes++;
        $valor_general += $value['valor'];

    }

    $resultado_general = $valor_general / $num_participiantes;
    $color_general = $this->setColorGeneral($resultado_general, $id_cuestionario);

    //fin reporte General
    //fin reporte General
    //fin reporte General



    //reporte categoerias
    //reporte categoerias
    //reporte categoerias


    //categorias
    //categorias
    //categorias
    $valores = array();
    $clasificacion_categorias_by_usuarios = array();
    $clasificacion_categorias = array();
    $total_participiantes = array();

    $valor_total_Cat = 0;
    $num_participiantes = 0;
    $promedio = 0;
    $valor_total = 0;

    foreach($categories as $key => $category){

        $category_id = $category['id'];
        $category_name = $category['name'];

        $respuestas = Answer::with('question')            
        ->whereHas('period', function($q) use($period_id){
            $q->where('id', $period_id);
        })
        ->whereHas('question', function($q) use ($category_id){
            $q->where('category_id', $category_id);
        })
        ->whereHas('user.employee_wt', function($q) use($request,$start_date,$end_date,$age_start_date,$age_end_date){
        $q->when(!is_null($request->sucursal), function($q2) use($request){
            $q2->whereHas('jobPosition', function($q3) use($request){
              $q3->whereHas('area', function($q4) use($request){
                $q4->where('department_id', $request->sucursal);
              });
            }); 
        });
        $q->when(!is_null($request->direccion), function($q2) use($request){
            $q2->whereHas('jobPosition', function($q3) use($request){
              $q3->whereHas('area', function($q4) use($request){
                $q4->whereHas('department', function($q5) use($request){
                  $q5->where('direction_id', $request->direccion);
                });
              });
            });
        });
        $q->when(!is_null($request->job), function($q2) use($request){
            $q2->where('job_position_id', $request->job);
        });
        $q->when(!is_null($request->sexo), function($q2) use($request){
            $q2->where('sexo', $request->sexo);
        });
        $q->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
            $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
        });
        $q->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
            $q2->whereBetween('ingreso', [$start_date, $end_date]);
        });
        $q->when(!is_null($request->centro_trabajo), function($q2) use($request){
          $q2->where('sucursal', $request->centro_trabajo);
        });
      })
      ->get();            
        if (count($respuestas)>0) {
          # code...
        foreach ($respuestas as $value) {

            if(!in_array($value->user_id, $total_participiantes)){
                $total_participiantes[] = $value->user_id;
                $num_participiantes++;
            }

            $valor_total_Cat += $this->valorespuesta($value->answer , $value->question->positive);
            
            $clasificacion_categorias[$key]['name'] = $category_name;// $respuestas;
            $clasificacion_categorias[$key]['id'] = $category_id;// $respuestas;

        }

      }else{
        $clasificacion_categorias[$key]['name'] = $category_name;// $respuestas;
        $clasificacion_categorias[$key]['id'] = $category_id;// $respuestas;
      }



        if($num_participiantes>0){
            $promedio = $valor_total_Cat/$num_participiantes;
        }else {$promedio=0;}
        $setPromedio = $this->setPromedio($category_name, $promedio,$id_cuestionario);
        $color = $this->setColor($category_name, $promedio, $id_cuestionario);
        $critico = $this->setCritico($category_name, $promedio, $id_cuestionario);
        $promedio_general += $setPromedio;
        $clasificacion_categorias[$key]['data_y'] = intval( $setPromedio);// $respuestas;
        $clasificacion_categorias[$key]['data'] = [$setPromedio];// $respuestas;
        $clasificacion_categorias[$key]['critico'] = [$promedio];// $respuestas;
        $clasificacion_categorias[$key]['color'] = $critico;// $respuestas;
        $clasificacion_categorias[$key]['colors'] = $color;// $respuestas;

        $valor_total_Cat = 0;


    } 
    $num_participiantes = 0;
    $total_participiantes = [];

    $tables_cat = $tables_dom = $tables_dim = [
      'sex'=>[
          'name' => 'Sexo',
          'key' => 'sex',
          'headers' => [
            'Sexo','Totales'
          ],
          'rows' => [
              'women' => ['name' => 'Mujeres','total' => 0],
              'man' => ['name' => 'Hombres','total' => 0],
          ]
      ]
      ,
      'antiquity'=>[
          'name' => 'Antigüedad',
          'key' => 'antiquity',
          'headers' => [
            'Aniguedad','Personas'
          ],
          'rows' => [
              'less_than_a_year' => ['name' => 'Un año o menos','total' => 0],
              'one_to_five_years' => ['name' => 'Un año un día a cinco años','total' => 0],
              'five_to_ten_years' => ['name' => 'Cinco años un día a diez años','total' => 0],
              'ten_to_fifteen_years' => ['name' => 'Diez años un día y más','total' => 0], 
          ]
      ], 
      'generations' => [
          'name' => 'Generación',
          'headers' => [
            'Nacimiento','Generación','Totales'
          ],
          'key' => 'generations',
          'rows' => $this->getGenerationRows()
      ],
      'turnos'=>[
        'name' => 'Turnos',
        'key' => 'turnos',
        'headers' => [
          'Turnos','Personas'
        ],
        'rows' => [
            'diurno' => ['name' => 'Diurno','total' => 0],
            'vespertino' => ['name' => 'Vespertino','total' => 0],
            'nocturno' => ['name' => 'Nocturno','total' => 0],
            'matutino' => ['name' => 'Matutino','total' => 0],
        ]
    ],
    ];
    // dd($tables_cat);
    $years_key = '';
    $real_now = Carbon::now();
      foreach($users as $key => $user){

        if(isset($user->employee_wt)){
          if($user->employee_wt->sexo == 'M'){
            $tables_cat['sex']['rows']['man']['total'] += 1;
          }else{
            $tables_cat['sex']['rows']['women']['total'] += 1;
          }
        } 

        
      $entry = Carbon::parse($user->employee_wt->ingreso);
      $birthday = Carbon::parse($user->employee_wt->nacimiento);
      $antiquity = $real_now->diffInDays($entry)/365;
      
      
    
      if($antiquity < 1) {
          $tables_cat['antiquity']['rows']  ['less_than_a_year']['total'] += 1; 
          $years_key = 'less_than_a_year';      
          $years_name = 'Un año o menos';    
      }
       elseif($antiquity >= 1 && $antiquity <= 5) {    
        $tables_cat['antiquity']['rows']['one_to_five_years']['total'] += 1;   
        $years_key = 'one_to_five_years';      
        $years_name = 'Un año un día a cinco años';      
          // $analytics['antiquity'][$employee->sexo]['one_to_five_years'] += 1;
      } elseif($antiquity > 5 && $antiquity <= 10) {
        $tables_cat['antiquity']['rows']['five_to_ten_years']['total'] += 1; 
        $years_key = 'five_to_ten_years';      
        $years_name = 'Cinco años un día a diez años';    
          // $analytics['antiquity'][$employee->sexo]['five_to_ten_years'] += 1;
      } elseif($antiquity > 10) {
        $tables_cat['antiquity']['rows']['ten_to_fifteen_years']['total'] += 1; 
        $years_key = 'ten_to_fifteen_years';      
        $years_name = 'Diez años un día y más';    
          // $analytics['antiquity'][$employee->sexo]['ten_to_fifteen_years'] += 1;
      }
      
  
        if(!in_array($years_key, $temp_starts)){            
          $starts[] = ['id'=>$years_key,'nombre'=>$years_name];
          $temp_starts[] = $years_key;
        }
        

          // // Generation
          $generaciones = $this->getGenerationRows();
          $generation_row = Generation::getRangeKeyByBirthDay($birthday->year);
          // dd($birthday->year, $generation_row);
          if($generation_row) {
              // $analytics_detail['generations'][$employee->sexo][$generation_row][] = $employee;
              $tables_cat['generations']['rows'][$generation_row]['total']   += 1;
              if(!in_array($generation_row, $temp_ages)){
              
              $ages[] = [
                'id'=>$generaciones[$generation_row]['id'],
                'nombre'=>$generaciones[$generation_row]['name']
              ];
              $temp_ages[] = $generation_row;
            }

          // dd($ages);
            }
        
       
        $personal = $user->first_name.' '.$user->last_name;
        $department = (isset($user->employee_wt->jobPosition->area->department) ? $user->employee_wt->jobPosition->area->department->name : ' ');
        $area = (isset($user->employee_wt->jobPosition->area->department->direction) ? $user->employee_wt->jobPosition->area->department->direction->name : ' ');
        $jobPosition = $user->employee_wt->jobPosition->name;
        $sucursal = (!empty($user->employee_wt->sucursal) ? $user->employee_wt->sucursal : ' ');
        $empresa = (!empty($user->employee_wt->enterprise) ? $user->employee_wt->enterprise->name : ' ');
        $personal = $personal . '*' . $department . '#' . $area . '=' . $jobPosition . '$' . $sucursal . '[' . $empresa;

        if(!in_array($user->id, $total_participiantes)){
            $total_participiantes[] = $user->id;
            if ($key>0) {
                $num_participiantes++;
            }
        }

        //sexo

        $clasificacion_categorias_by_usuarios[$num_participiantes] = [$personal];

        foreach($categories as $key => $category){

            $category_id = $category['id'];
            $category_name = $category['name'];

            $respuestas = Answer::with('question')
            ->where('user_id', $user->id)
            ->whereHas('period', function($q) use($period_id){
                $q->where('id', $period_id);
            })
            ->whereHas('question', function($q) use ($category_id){
                $q->where('category_id', $category_id);
            })
            ->whereHas('user.employee_wt', function($q) use($request,$start_date,$end_date,$age_start_date,$age_end_date){
              $q->when(!is_null($request->sucursal), function($q2) use($request){
                $q2->whereHas('jobPosition', function($q3) use($request){
                  $q3->whereHas('area', function($q4) use($request){
                    $q4->where('department_id', $request->sucursal);
                  });
                }); 
              });
              $q->when(!is_null($request->direccion), function($q2) use($request){
                $q2->whereHas('jobPosition', function($q3) use($request){
                  $q3->whereHas('area', function($q4) use($request){
                    $q4->whereHas('department', function($q5) use($request){
                      $q5->where('direction_id', $request->direccion);
                    });
                  });
                });
              });
              $q->when(!is_null($request->job), function($q2) use($request){
                $q2->where('job_position_id', $request->job);
              });
              $q->when(!is_null($request->sexo), function($q2) use($request){
                $q2->where('sexo', $request->sexo);
              });
              $q->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
                $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
              });
              $q->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
                $q2->whereBetween('ingreso', [$start_date, $end_date]);
              });
              $q->when(!is_null($request->centro_trabajo), function($q2) use($request){
                $q2->where('sucursal', $request->centro_trabajo);
              });
            })
            ->get();


            foreach ($respuestas as $kys => $value) {

                $valor = $this->valorespuesta($value->answer , $value->question->positive);
                $valor_total += $this->valorespuesta($value->answer , $value->question->positive);
               
            }

            $clasificacion_categorias_by_usuarios[$num_participiantes][$key+1]['valor'] = $valor_total;

            $color = $this->setColor($category_name, $valor_total, $id_cuestionario);
            $clasificacion_categorias_by_usuarios[$num_participiantes][$key+1]['color'] = $color;
            
            $valor_total = 0;
        }

        
    }
    
    //fin reporte categorias
    //fin reporte categorias
    //fin reporte categorias
    //fin reporte categorias






    //dominios

    $valores = array();
    $clasificacion_dominios_by_usuarios = array();
    $clasificacion_dominios = array();
    $total_participiantes = array();
    $valor_total_Cat = 0;
    $num_participiantes = 0;
    $promedio = 0;
    $valor_total = 0;
    $colores_dominios = array();


    $tabla_dominio = array();
    $clasificacion_dominiosxy = array();
    $clasificacion_dominiosxyz = array();

    $dominios = $this->sortArrayByKey($dominios, 'id');

    foreach($dominios as $key => $category){

        $category_id = $category['id'];
        $category_name = $category['name'];
        $category_real_name = $category['category'];
        $category_real_id = $category['category_id'];

        $respuestas = Answer::with('question')            
        ->whereHas('period', function($q) use($period_id){
            $q->where('id', $period_id);
        })
        ->whereHas('question', function($q) use ($category_id){
            $q->where('domain_id', $category_id);
        })
        ->whereHas('user.employee_wt', function($q) use($request,$start_date,$end_date,$age_start_date,$age_end_date){
              $q->when(!is_null($request->sucursal), function($q2) use($request){
                $q2->whereHas('jobPosition', function($q3) use($request){
                  $q3->whereHas('area', function($q4) use($request){
                    $q4->where('department_id', $request->sucursal);
                  });
                }); 
              });
              $q->when(!is_null($request->direccion), function($q2) use($request){
                $q2->whereHas('jobPosition', function($q3) use($request){
                  $q3->whereHas('area', function($q4) use($request){
                    $q4->whereHas('department', function($q5) use($request){
                      $q5->where('direction_id', $request->direccion);
                    });
                  });
                });
              });
              $q->when(!is_null($request->job), function($q2) use($request){
                $q2->where('job_position_id', $request->job);
              });
              $q->when(!is_null($request->sexo), function($q2) use($request){
                $q2->where('sexo', $request->sexo);
              });
              $q->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
                $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
              });
              $q->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
                $q2->whereBetween('ingreso', [$start_date, $end_date]);
              });
              $q->when(!is_null($request->centro_trabajo), function($q2) use($request){
                $q2->where('sucursal', $request->centro_trabajo);
              });
        })
        ->get();            


        foreach ($respuestas as $value) {

                
          $cerrado = \DB::table('nom035_period_user')
          ->where('period_id', $period_id)
          ->whereIn('status',[3, 33])
          ->whereIn('user_id', [$value->user_id])
          ->count();
      
          if(!in_array($value->user_id, $total_participiantes) && $cerrado>0){
            // if(!in_array($value->user_id, $total_participiantes)){
                $total_participiantes[] = $value->user_id;
                $num_participiantes++;

            }

            $valor_total_Cat += $this->valorespuesta($value->answer , $value->question->positive);
            
            $clasificacion_dominios[$key]['id'] = $category_id;// $respuestas;
            $clasificacion_dominios[$key]['name'] = $category_name;// $respuestas;
            $clasificacion_dominios[$key]['category'] = $category_real_name;
            $clasificacion_dominios[$key]['category_id'] = $category_real_id;

        }

        if($num_participiantes>0){
            $promedio = $valor_total_Cat/$num_participiantes;
        }else {$promedio=0;}
        

        $setPromedio = $this->setPromedio($category_name, $promedio,$id_cuestionario);
        $color = $this->setColor($category_name, $promedio, $id_cuestionario);
        $critico = $this->setCritico($category_name, $promedio, $id_cuestionario);

        $clasificacion_dominios[$key]['data_y'] = intval( $setPromedio);// $respuestas;
        $clasificacion_dominios[$key]['xyz'] = [$valor_total_Cat,$num_participiantes];// $respuestas;
        $clasificacion_dominios[$key]['data'] = [$setPromedio];// $respuestas;
        $clasificacion_dominios[$key]['critico'] = [$promedio];// $respuestas;
        $clasificacion_dominios[$key]['color'] = $critico;// $respuestas;
        $clasificacion_dominios[$key]['colors'] = $color;// $respuestas;
        $colores_dominios[$category_id] = $color;
        $valor_total_Cat = 0;


    }



    // dd($clasificacion_dominios);















    
    foreach($dominios as $key => $category){

      $category_id = $category['id'];
      $category_name = $category['name'];
      $category_real_name = $category['category'];
      $category_real_id = $category['category_id'];

      foreach($regiones as $centros =>  $centro_trabajos){
        
        $centro_trabajo = $centro_trabajos['nombre'];
        $id_centro_trabajo = $centro_trabajos['id'];

        $promedio = 0;
        $valor_total_Cat = 0;
        $num_participiantes = 0;
        $total_participiantes = array();
      $respuestas = Answer::with('question')            
      ->whereHas('period', function($q) use($period_id){
          $q->where('id', $period_id);
      })
      ->whereHas('question', function($q) use ($category_id){
          $q->where('domain_id', $category_id);
      })
      ->whereHas('user.employee_wt', function($q) use($request,$start_date,$end_date,$age_start_date,$age_end_date,$id_centro_trabajo){
            $q->when(!is_null($request->sucursal), function($q2) use($request){
              $q2->whereHas('jobPosition', function($q3) use($request){
                $q3->whereHas('area', function($q4) use($request){
                  $q4->where('department_id', $request->sucursal);
                });
              }); 
            });
            $q->when(!is_null($request->direccion), function($q2) use($request){
              $q2->whereHas('jobPosition', function($q3) use($request){
                $q3->whereHas('area', function($q4) use($request){
                  $q4->whereHas('department', function($q5) use($request){
                    $q5->where('direction_id', $request->direccion);
                  });
                });
              });
            });
            $q->when(!is_null($request->job), function($q2) use($request){
              $q2->where('job_position_id', $request->job);
            });
            $q->when(!is_null($request->sexo), function($q2) use($request){
              $q2->where('sexo', $request->sexo);
            });
            $q->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
              $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
            });
            $q->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
              $q2->whereBetween('ingreso', [$start_date, $end_date]);
            });
            $q->when(!is_null($request->centro_trabajo), function($q2) use($request){
              $q2->where('sucursal', $request->centro_trabajo);
            });
            $q->where('region_id', $id_centro_trabajo); 
      })
      ->get();            

      $nros_empleados = 0;
      $nros_empleados_por_sucursal = array();
      $empleados_por_sucursal = Answer::with('question')            
      ->whereHas('period', function($q) use($period_id){
          $q->where('id', $period_id);
      })
      ->whereHas('user.employee_wt', function($q) use($request,$start_date,$end_date,$age_start_date,$age_end_date,$id_centro_trabajo){
            // $q->when(!is_null($request->sucursal), function($q2) use($request){
            //   $q2->whereHas('jobPosition', function($q3) use($request){
            //     $q3->whereHas('area', function($q4) use($request){
            //       $q4->where('department_id', $request->sucursal);
            //     });
            //   }); 
            // });
            $q->when(!is_null($request->direccion), function($q2) use($request){
              $q2->whereHas('jobPosition', function($q3) use($request){
                $q3->whereHas('area', function($q4) use($request){
                  $q4->whereHas('department', function($q5) use($request){
                    $q5->where('direction_id', $request->direccion);
                  });
                });
              });
            });
            $q->when(!is_null($request->job), function($q2) use($request){
              $q2->where('job_position_id', $request->job);
            });
            $q->when(!is_null($request->sexo), function($q2) use($request){
              $q2->where('sexo', $request->sexo);
            });
            $q->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
              $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
            });
            $q->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
              $q2->whereBetween('ingreso', [$start_date, $end_date]);
            }); 
            $q->where('region_id', $id_centro_trabajo); 
      })
      ->get();            

      foreach ($empleados_por_sucursal as $value) {

        if(!in_array($value->user_id, $nros_empleados_por_sucursal)){
            $nros_empleados_por_sucursal[] = $value->user_id; 
            $nros_empleados++;

        }
      }

      $clasificacion_dominiosxy[$key]['hospitales'][$centro_trabajo]['name'] = $centro_trabajo;              

      foreach ($respuestas as $value) {

                
        $cerrado = \DB::table('nom035_period_user')
        ->where('period_id', $period_id)
        ->whereIn('status',[3, 33])
        ->whereIn('user_id', [$value->user_id])
        ->count();
    
        if(!in_array($value->user_id, $total_participiantes) && $cerrado>0){
          // if(!in_array($value->user_id, $total_participiantes)){
              $total_participiantes[] = $value->user_id;
              $num_participiantes++;

          }

          $valor_total_Cat += $this->valorespuesta($value->answer , $value->question->positive);

          $clasificacion_dominiosxy[$key]['id'] = $category_id;// $respuestas;
          $clasificacion_dominiosxy[$key]['name'] = $category_name;// $respuestas;
          $clasificacion_dominiosxy[$key]['category'] = $category_real_name;
          $clasificacion_dominiosxy[$key]['category_id'] = $category_real_id;

      }

      if($num_participiantes>0){
        $promedio = $valor_total_Cat/$num_participiantes;
      }else {$promedio=0;}
      

      $setPromedio = $this->setPromedio($category_name, $promedio,$id_cuestionario);
      $color = $this->setColor($category_name, $promedio, $id_cuestionario);
      $critico = $this->setCritico($category_name, $promedio, $id_cuestionario);

      $clasificacion_dominiosxy[$key]['hospitales'][$centro_trabajo]['name'] = $centro_trabajo;              

      
      $clasificacion_dominiosxy[$key]['hospitales'][$centro_trabajo]['data_y'] = intval( $setPromedio);// $respuestas;
      // $clasificacion_dominiosxy[$key]['hospitales'][$centro_trabajo]['xyz'] = [$valor_total_Cat,$num_participiantes];// $respuestas;
      $clasificacion_dominiosxy[$key]['hospitales'][$centro_trabajo]['xyz'] = [$nros_empleados,$valor_total_Cat,$num_participiantes];// $respuestas;
      $clasificacion_dominiosxy[$key]['hospitales'][$centro_trabajo]['data'] = [$setPromedio];// $respuestas;
      $clasificacion_dominiosxy[$key]['hospitales'][$centro_trabajo]['critico'] = [$promedio];// $respuestas;
      $clasificacion_dominiosxy[$key]['hospitales'][$centro_trabajo]['color'] = $critico;// $respuestas;
      $clasificacion_dominiosxy[$key]['hospitales'][$centro_trabajo]['colors'] = $color;// $respuestas;
      $colores_dominios[$category_id] = $color;
      $valor_total_Cat = 0;


  }
  }
    // dd($clasificacion_dominiosxy);

    foreach($dominios as $key => $category){

      $category_id = $category['id'];
      $category_name = $category['name'];
      $category_real_name = $category['category'];
      $category_real_id = $category['category_id'];

      
      foreach($centros_trabajo as $centros =>  $centro_trabajos){
        
        $sucursal = $centro_trabajos['nombre']; 

        $total_participiantes = array();
        $promedio = 0;
        $valor_total_Cat = 0;
        $num_participiantes = 0;
        $nombre_participiantes = array();


      $respuestas = Answer::with('question')            
      ->whereHas('period', function($q) use($period_id){
          $q->where('id', $period_id);
      })
      ->whereHas('question', function($q) use ($category_id){
          $q->where('domain_id', $category_id);
      })
      ->whereHas('user.employee_wt', function($q) use($request,$start_date,$end_date,$age_start_date,$age_end_date,$sucursal){
            // $q->when(!is_null($request->sucursal), function($q2) use($request){
            //   $q2->whereHas('jobPosition', function($q3) use($request){
            //     $q3->whereHas('area', function($q4) use($request){
            //       $q4->where('department_id', $request->sucursal);
            //     });
            //   }); 
            // });
            $q->when(!is_null($request->direccion), function($q2) use($request){
              $q2->whereHas('jobPosition', function($q3) use($request){
                $q3->whereHas('area', function($q4) use($request){
                  $q4->whereHas('department', function($q5) use($request){
                    $q5->where('direction_id', $request->direccion);
                  });
                });
              });
            });
            $q->when(!is_null($request->job), function($q2) use($request){
              $q2->where('job_position_id', $request->job);
            });
            $q->when(!is_null($request->sexo), function($q2) use($request){
              $q2->where('sexo', $request->sexo);
            });
            $q->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
              $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
            });
            $q->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
              $q2->whereBetween('ingreso', [$start_date, $end_date]);
            }); 
            $q->where('sucursal', $sucursal); 
      })
      ->get();   

      
      $nros_empleados = 0;
      $nros_empleados_por_sucursal = array();
      $empleados_por_sucursal = Answer::with('question')            
      ->whereHas('period', function($q) use($period_id){
          $q->where('id', $period_id);
      })
      ->whereHas('user.employee_wt', function($q) use($request,$start_date,$end_date,$age_start_date,$age_end_date,$sucursal){
            // $q->when(!is_null($request->sucursal), function($q2) use($request){
            //   $q2->whereHas('jobPosition', function($q3) use($request){
            //     $q3->whereHas('area', function($q4) use($request){
            //       $q4->where('department_id', $request->sucursal);
            //     });
            //   }); 
            // });
            $q->when(!is_null($request->direccion), function($q2) use($request){
              $q2->whereHas('jobPosition', function($q3) use($request){
                $q3->whereHas('area', function($q4) use($request){
                  $q4->whereHas('department', function($q5) use($request){
                    $q5->where('direction_id', $request->direccion);
                  });
                });
              });
            });
            $q->when(!is_null($request->job), function($q2) use($request){
              $q2->where('job_position_id', $request->job);
            });
            $q->when(!is_null($request->sexo), function($q2) use($request){
              $q2->where('sexo', $request->sexo);
            });
            $q->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
              $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
            });
            $q->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
              $q2->whereBetween('ingreso', [$start_date, $end_date]);
            }); 
            $q->where('sucursal', $sucursal); 
      })
      ->get();            

      foreach ($empleados_por_sucursal as $value) {

        if(!in_array($value->user_id, $nros_empleados_por_sucursal)){
            $nros_empleados_por_sucursal[] = $value->user_id; 
            $nros_empleados++;

        }
      }

      $clasificacion_dominiosxyz[$key]['centros'][$sucursal]['name'] = $sucursal;              

      foreach ($respuestas as $value) {

        $cerrado = \DB::table('nom035_period_user')
        ->where('period_id', $period_id)
        ->whereIn('status',[3, 33])
        ->whereIn('user_id', [$value->user_id])
        ->count();
    
        if(!in_array($value->user_id, $total_participiantes) && $cerrado>0){
          // if(!in_array($value->user_id, $total_participiantes)){
              $total_participiantes[] = $value->user_id;
              $nombre_participiantes[] = $value->user->first_name;
              $num_participiantes++;

          }

          $valor_total_Cat += $this->valorespuesta($value->answer , $value->question->positive);

          $clasificacion_dominiosxyz[$key]['id'] = $category_id;// $respuestas;
          $clasificacion_dominiosxyz[$key]['name'] = $category_name;// $respuestas;
          $clasificacion_dominiosxyz[$key]['category'] = $category_real_name;
          $clasificacion_dominiosxyz[$key]['category_id'] = $category_real_id;

      }

      if($num_participiantes>0){
          $promedio = $valor_total_Cat/$num_participiantes;
      }else {$promedio=0;}
      

      $setPromedio = $this->setPromedio($category_name, $promedio,$id_cuestionario);
      $color = $this->setColor($category_name, $promedio, $id_cuestionario);
      $critico = $this->setCritico($category_name, $promedio, $id_cuestionario);

      $clasificacion_dominiosxyz[$key]['centros'][$sucursal]['name'] = $sucursal;              

      
      $clasificacion_dominiosxyz[$key]['centros'][$sucursal]['data_y'] = intval( $setPromedio);// $respuestas;
      $clasificacion_dominiosxyz[$key]['centros'][$sucursal]['xyz'] = [$nros_empleados,$valor_total_Cat,$num_participiantes,$nombre_participiantes];// $respuestas;
      $clasificacion_dominiosxyz[$key]['centros'][$sucursal]['data'] = [$setPromedio];// $respuestas;
      $clasificacion_dominiosxyz[$key]['centros'][$sucursal]['critico'] = [$promedio];// $respuestas;
      $clasificacion_dominiosxyz[$key]['centros'][$sucursal]['color'] = $critico;// $respuestas;
      $clasificacion_dominiosxyz[$key]['centros'][$sucursal]['colors'] = $color;// $respuestas;
      $colores_dominios[$category_id] = $color;
      $valor_total_Cat = 0;


  }
  }

  // dd($clasificacion_dominiosxyz);



      $sucursal = '';

      foreach($dominios as $key => $category){
        // if($key==2){

        $category_id = $category['id'];
        $category_name = $category['name'];
        $category_real_name = $category['category'];
        $category_real_id = $category['category_id'];

        $tabla_dominio[$key]['id'] = $key;
        $tabla_dominio[$key]['name'] = $category_name; 
        $tabla_dominio[$key]['centros_trabajo'] = $tabla_dominio_centros_trabajo;
        $tabla_dominio[$key]['hospitales'] = $tabla_dominio_hospitales;

        foreach($centros_trabajo as $centro =>  $centro_trabajos){


          $centro_trabajo = $centro_trabajos['nombre'];
          
        $promedio = 0;
        $valor_total_Cat = 0;
        $num_participiantes = 0;
        $total_participiantes = array();

          $respuestas = Answer::with('question')            
          ->whereHas('period', function($q) use($period_id){
              $q->where('id', $period_id);
          })
          ->whereHas('question', function($q) use ($category_id){
              $q->where('domain_id', $category_id);
          })
          ->whereHas('user.employee_wt', function($q) use($request,$start_date,$end_date,$age_start_date,$age_end_date,$centro_trabajo){
                $q->when(!is_null($request->sucursal), function($q2) use($request){
                  $q2->whereHas('jobPosition', function($q3) use($request){
                    $q3->whereHas('area', function($q4) use($request){
                      $q4->where('department_id', $request->sucursal);
                    });
                  }); 
                });
                $q->when(!is_null($request->direccion), function($q2) use($request){
                  $q2->whereHas('jobPosition', function($q3) use($request){
                    $q3->whereHas('area', function($q4) use($request){
                      $q4->whereHas('department', function($q5) use($request){
                        $q5->where('direction_id', $request->direccion);
                      });
                    });
                  });
                });
                $q->when(!is_null($request->job), function($q2) use($request){
                  $q2->where('job_position_id', $request->job);
                });
                $q->when(!is_null($request->sexo), function($q2) use($request){
                  $q2->where('sexo', $request->sexo);
                });
                $q->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
                  $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
                });
                $q->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
                  $q2->whereBetween('ingreso', [$start_date, $end_date]);
                }); 
                $q->where('sucursal', $centro_trabajo); 
          })
          ->get();            

          
          
          foreach ($respuestas as $keyasd => $value) {

              if(!in_array($value->user_id, $total_participiantes)){
                  $total_participiantes[] = $value->user_id;
                  $num_participiantes++; 
              }

              $valor_total_Cat += $this->valorespuesta($value->answer , $value->question->positive);
              
          }

          $tabla_dominio[$key]['centros_trabajo'][$centro_trabajo]['num_participiantes'] = $num_participiantes;              

          // if( $key==1){
          //   dd($centro_trabajo); 
          // }
          
          if($num_participiantes>0){
              $promedio = $valor_total_Cat/$num_participiantes;
          }else {$promedio=0;}

          $setPromedio = $this->setPromedio($category_name, $promedio,$id_cuestionario);
          $color = $this->setColor($category_name, $promedio, $id_cuestionario);
          $critico = $this->setCritico($category_name, $promedio, $id_cuestionario);

          $tabla_dominio[$key]['centros_trabajo'][$centro_trabajo]['valor'] = $promedio;
          $tabla_dominio[$key]['centros_trabajo'][$centro_trabajo]['resultado'] = $setPromedio;
          $tabla_dominio[$key]['centros_trabajo'][$centro_trabajo]['critico'] = $critico;
          $tabla_dominio[$key]['centros_trabajo'][$centro_trabajo]['color'] = $color;

          $valor_total_Cat = 0;


        // }
      }









        foreach($regiones as $centros =>  $centro_trabajos){


          $centro_trabajo = $centro_trabajos['nombre'];
          $id_centro_trabajo = $centro_trabajos['id'];
          
          $respuestas = Answer::with('question')            
          ->whereHas('period', function($q) use($period_id){
              $q->where('id', $period_id);
          })
          ->whereHas('question', function($q) use ($category_id){
              $q->where('domain_id', $category_id);
          })
          ->whereHas('user.employee_wt', function($q) use($request,$start_date,$end_date,$age_start_date,$age_end_date,$id_centro_trabajo){
                $q->when(!is_null($request->sucursal), function($q2) use($request){
                  $q2->whereHas('jobPosition', function($q3) use($request){
                    $q3->whereHas('area', function($q4) use($request){
                      $q4->where('department_id', $request->sucursal);
                    });
                  }); 
                });
                $q->when(!is_null($request->direccion), function($q2) use($request){
                  $q2->whereHas('jobPosition', function($q3) use($request){
                    $q3->whereHas('area', function($q4) use($request){
                      $q4->whereHas('department', function($q5) use($request){
                        $q5->where('direction_id', $request->direccion);
                      });
                    });
                  });
                });
                $q->when(!is_null($request->job), function($q2) use($request){
                  $q2->where('job_position_id', $request->job);
                });
                $q->when(!is_null($request->sexo), function($q2) use($request){
                  $q2->where('sexo', $request->sexo);
                });
                $q->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
                  $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
                });
                $q->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
                  $q2->whereBetween('ingreso', [$start_date, $end_date]);
                }); 
                $q->where('region_id', $id_centro_trabajo); 
          })
          ->get();            


          

          $promedio = 0;
          $valor_total_Cat = 0;
          $num_participiantes = 0;
          $total_participiantes = array();

          foreach ($respuestas as $value) {

              if(!in_array($value->user_id, $total_participiantes)){
                  $total_participiantes[] = $value->user_id;
                  $num_participiantes++; 
              }

              $valor_total_Cat += $this->valorespuesta($value->answer , $value->question->positive);
              
          }

          // $tabla_dominio[$key]['centros_trabajo'][$centro_trabajo]['num_participiantes'] = $num_participiantes;              
          $tabla_dominio[$key]['hospitales'][$centro_trabajo]['num_participiantes'] = $num_participiantes;              

          if($num_participiantes>0){
              $promedio = $valor_total_Cat/$num_participiantes;
          }else {$promedio=0;}
          

          $setPromedio = $this->setPromedio($category_name, $promedio,$id_cuestionario);
          $color = $this->setColor($category_name, $promedio, $id_cuestionario);
          $critico = $this->setCritico($category_name, $promedio, $id_cuestionario);

          $tabla_dominio[$key]['hospitales'][$centro_trabajo]['valor'] = $promedio;
          $tabla_dominio[$key]['hospitales'][$centro_trabajo]['resultado'] = $setPromedio;
          $tabla_dominio[$key]['hospitales'][$centro_trabajo]['critico'] = $critico;
          $tabla_dominio[$key]['hospitales'][$centro_trabajo]['color'] = $color;

          $valor_total_Cat = 0;


        // }
      }
    }


    // dd($tabla_dominio);

    $num_participiantes = 0;
    $total_participiantes = [];

         
    foreach($users as $key => $user){

        $personal = $user->first_name.' '.$user->last_name;
        $department = (isset($user->employee_wt->jobPosition->area->department) ? $user->employee_wt->jobPosition->area->department->name : ' ');
        $area = (isset($user->employee_wt->jobPosition->area->department->direction) ? $user->employee_wt->jobPosition->area->department->direction->name : ' ');
        $jobPosition = $user->employee_wt->jobPosition->name;
        $sucursal = (!empty($user->employee_wt->sucursal) ? $user->employee_wt->sucursal : ' ');
        $empresa = (!empty($user->employee_wt->enterprise) ? $user->employee_wt->enterprise->name : ' ');
        $personal = $personal . '*' . $department . '#' . $area . '=' . $jobPosition . '$' . $sucursal . '[' . $empresa;

        if(!in_array($user->id, $total_participiantes)){
                $total_participiantes[] = $user->id;
                if ($key>0) {
                    $num_participiantes++;
                }
            }


        $clasificacion_dominios_by_usuarios[$num_participiantes] = [$personal];

        foreach($dominios as $key => $category){

            $category_id = $category['id'];
            $category_name = $category['name'];

            $respuestas = Answer::with('question')
            ->where('user_id', $user->id)
            ->whereHas('period', function($q) use($period_id){
                $q->where('id', $period_id);
            })
            ->whereHas('question', function($q) use ($category_id){
                $q->where('domain_id', $category_id);
            })
            ->whereHas('user.employee_wt', function($q) use($request,$start_date,$end_date,$age_start_date,$age_end_date){
              $q->when(!is_null($request->sucursal), function($q2) use($request){
                $q2->whereHas('jobPosition', function($q3) use($request){
                  $q3->whereHas('area', function($q4) use($request){
                    $q4->where('department_id', $request->sucursal);
                  });
                }); 
              });
              $q->when(!is_null($request->direccion), function($q2) use($request){
                $q2->whereHas('jobPosition', function($q3) use($request){
                  $q3->whereHas('area', function($q4) use($request){
                    $q4->whereHas('department', function($q5) use($request){
                      $q5->where('direction_id', $request->direccion);
                    });
                  });
                });
              });
              $q->when(!is_null($request->job), function($q2) use($request){
                $q2->where('job_position_id', $request->job);
              });
              $q->when(!is_null($request->sexo), function($q2) use($request){
                $q2->where('sexo', $request->sexo);
              });
              $q->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
                $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
              });
              $q->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
                $q2->whereBetween('ingreso', [$start_date, $end_date]);
              });
              $q->when(!is_null($request->centro_trabajo), function($q2) use($request){
                $q2->where('sucursal', $request->centro_trabajo);
              });
            })
            ->get();


            foreach ($respuestas as $value) {

                $valor = $this->valorespuesta($value->answer , $value->question->positive);
                $valor_total += $this->valorespuesta($value->answer , $value->question->positive);

            }

            $clasificacion_dominios_by_usuarios[$num_participiantes][$key+1]['valor'] = $valor_total;

            $color = $this->setColor($category_name, $valor_total, $id_cuestionario);
            $clasificacion_dominios_by_usuarios[$num_participiantes][$key+1]['color'] = $color;
            
            $valor_total = 0;
        }

        
    }
    
   //fin reporte dominios
    //fin reporte dominios
    //fin reporte dominios
    //fin reporte dominios


    //dimensiones
    //dimensiones
    //dimensiones
    $valores = array();
    $clasificacion_dimensiones_by_usuarios = array();
    $clasificacion_dimensiones = array();
    $total_participiantes = array();

    $valor_total_Cat = 0;
    $num_participiantes = 0;
    $promedio = 0;
    $valor_total = 0;
    $keyss = 0;

    $filtro_dominio = $dominios[0]['id'];
    $dominio_seleccionado = $dominios[0]['name'];
    // $filtro_dominio = $first_dominio->;
    foreach($dimensiones as $keyds => $category){

        $category_id = $category['id'];
        $category_name = $category['name'];
        $category_real_name = $category['category'];
        $domain_name = $category['domain'];
        $domain_id = $category['domain_id'];

        $respuestas = Answer::with('question')            
        ->whereHas('period', function($q) use($period_id){
            $q->where('id', $period_id);
        })
        ->whereHas('question', function($q) use ($category_id, $filtro_dominio){
          $q->where('dimension_id', $category_id);
          // $q->where('domain_id', $filtro_dominio);
        })
        ->whereHas('user.employee_wt', function($q) use($request,$start_date,$end_date,$age_start_date,$age_end_date){
              $q->when(!is_null($request->sucursal), function($q2) use($request){
                $q2->whereHas('jobPosition', function($q3) use($request){
                  $q3->whereHas('area', function($q4) use($request){
                    $q4->where('department_id', $request->sucursal);
                  });
                }); 
              });
              $q->when(!is_null($request->direccion), function($q2) use($request){
                $q2->whereHas('jobPosition', function($q3) use($request){
                  $q3->whereHas('area', function($q4) use($request){
                    $q4->whereHas('department', function($q5) use($request){
                      $q5->where('direction_id', $request->direccion);
                    });
                  });
                });
              });
              $q->when(!is_null($request->job), function($q2) use($request){
                $q2->where('job_position_id', $request->job);
              });
              $q->when(!is_null($request->sexo), function($q2) use($request){
                $q2->where('sexo', $request->sexo);
              });
              $q->when(!is_null($request->edad), function($q2) use($age_start_date,$age_end_date){
                $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
              });
              $q->when(!is_null($request->ingreso), function($q2) use($start_date,$end_date){
                $q2->whereBetween('ingreso', [$start_date, $end_date]);
              });
              $q->when(!is_null($request->centro_trabajo), function($q2) use($request){
                $q2->where('sucursal', $request->centro_trabajo);
              });
        })
        ->get();            



        $real_now = Carbon::now();
        if(count($respuestas)>0){
        foreach ($respuestas as $value) {

            if(!in_array($value->user_id, $total_participiantes)){
                $total_participiantes[] = $value->user_id;
                $num_participiantes++;

                
              $user = $value->user;
              if(isset($user->employee_wt)){
                if($user->employee_wt->sexo == 'M'){
                  $tables_dim['sex']['rows']['man']['total'] += 1;
                }else{
                  $tables_dim['sex']['rows']['women']['total'] += 1;
                }
              } 
    
                
              $entry = Carbon::parse($user->employee_wt->ingreso);
              $birthday = Carbon::parse($user->employee_wt->nacimiento);
              $antiquity = $real_now->diffInDays($entry)/365;
              if($antiquity < 1) {
                  $tables_dim['antiquity']['rows']  ['less_than_a_year']['total'] += 1;
                  // $analytics['antiquity'][$employee->sexo]['less_than_a_year'] += 1;
              }
              elseif($antiquity >= 1 && $antiquity <= 5) {    
                $tables_dim['antiquity']['rows']['one_to_five_years']['total'] += 1;         
                  // $analytics['antiquity'][$employee->sexo]['one_to_five_years'] += 1;
              } elseif($antiquity > 5 && $antiquity <= 10) {
                $tables_dim['antiquity']['rows']['five_to_ten_years']['total'] += 1;
                  // $analytics['antiquity'][$employee->sexo]['five_to_ten_years'] += 1;
              } elseif($antiquity > 10 && $antiquity <= 15) {
                $tables_dim['antiquity']['rows']['ten_to_fifteen_years']['total'] += 1;
                  // $analytics['antiquity'][$employee->sexo]['ten_to_fifteen_years'] += 1;
              } elseif($antiquity > 15) {
                $tables_dim['antiquity']['rows']['ten_to_fifteen_years']['total'] += 1;
                  // $analytics['antiquity'][$employee->sexo]['more_than_fifteen_years'] += 1;
              }
    
    
              // // Generation
              $generation_row = Generation::getRangeKeyByBirthDay($birthday->year);
              // dd($birthday->year, $generation_row);
              if($generation_row) {
                  // $analytics_detail['generations'][$employee->sexo][$generation_row][] = $employee;
                  $tables_dim['generations']['rows'][$generation_row]['total']   += 1;
              }

            }

            $valor_total_Cat += $this->valorespuesta($value->answer , $value->question->positive);
            
            // $clasificacion_dimensiones[$keyss] = [
            //   "name" => $category_name,
            //   "valor" => $valor_total_Cat,
            // ];

            $clasificacion_dimensiones[$keyss]['name'] = $category_name;// $respuestas;
            $clasificacion_dimensiones[$keyss]['valor'] = $valor_total_Cat;// $respuestas;
            $clasificacion_dimensiones[$keyss]['participantes'] = $num_participiantes;// $respuestas;
            $clasificacion_dimensiones[$keyss]['category'] = $category_real_name;// $respuestas;
            $clasificacion_dimensiones[$keyss]['domain'] = $domain_name;// $respuestas;
            $clasificacion_dimensiones[$keyss]['data'] = [0];
            $clasificacion_dimensiones[$keyss]['data_y'] = 0;// $respuestas;
        }
      }else{
        
        $clasificacion_dimensiones[$keyss]['name'] = $category_name;// $respuestas;
        $clasificacion_dimensiones[$keyss]['valor'] = 0;// $respuestas;
        $clasificacion_dimensiones[$keyss]['participantes'] = $num_participiantes;// $respuestas;
        $clasificacion_dimensiones[$keyss]['category'] = $category_real_name;// $respuestas;
        $clasificacion_dimensiones[$keyss]['domain'] = $domain_name;// $respuestas;
        $clasificacion_dimensiones[$keyss]['data'] = [0];
        $clasificacion_dimensiones[$keyss]['data_y'] = 0;// $respuestas;
      }

        $clasificacion_dimensiones[$keyss]['domain_id'] = $domain_id;// $respuestas;
        if($num_participiantes>0){
            $promedio = $valor_total_Cat/$num_participiantes;
        }else {$promedio=0;}
        

        $setPromedio = $this->setPromedio($category_name, $promedio,$id_cuestionario);
        $color = $this->setColor($category_name, $promedio, $id_cuestionario);
        $critico = $this->setCritico($category_name, $promedio, $id_cuestionario);

        if(count($respuestas)>0){
            $clasificacion_dimensiones[$keyss]['critico'] = [$setPromedio];// $respuestas;
            $clasificacion_dimensiones[$keyss]['data_y'] = intval( $promedio);// $respuestas;
            $clasificacion_dimensiones[$keyss]['data'] = [$promedio];// $respuestas;
            $clasificacion_dimensiones[$keyss]['color'] = $critico;// $respuestas;
            $clasificacion_dimensiones[$keyss]['colors'] = $color;// $respuestas;

            $valor_total_Cat = 0;
        }

        $keyss++;
    }
    //fin dimensiones
    //fin dimensiones
    //fin dimensiones
    //fin dimensiones
    $cdi = array();
    foreach ($clasificacion_dimensiones as $key => $value) {
      $cdi[] = $value;
    }

    $clasificacion_dimensiones = $cdi;

    
  $promedio_general = round($promedio_general / 5);
  $color_general = $this->setCriticoGeneal($promedio_general);
   
  $users_count = $users->count();
    if($request->periodo){

        return response()->json([
            'users_count' => $users_count,
            'dominio_seleccionado' => $dominio_seleccionado,
            'promedio_general' => $promedio_general,
            'sucursales' => $sucursales,
            'directions' => $directions,
            'jobs' => $jobs,
            'sexs' => $sexs,
            'filtro_dominio' => $filtro_dominio,
            'ages' => $ages,
            'starts' => $starts,
            'clasificacion_dominiosxyz' => $clasificacion_dominiosxyz,
            'clasificacion_dominiosxy' => $clasificacion_dominiosxy,
            'regiones' => $regiones,
            'centros_trabajo' => $centros_trabajo,
            //general
          'clasificacion_general' => $clasificacion_general, 
          'resultado_general' => $resultado_general, 
          'color_general' => $color_general,
          //categorias
          'clasificacion_categorias' => $clasificacion_categorias, 
          'clasificacion_categorias_by_usuarios' => $clasificacion_categorias_by_usuarios, 
          'name_categories' => $name_categories,
          //dominios
          'clasificacion_dominios' => $clasificacion_dominios, 
            'clasificacion_dominios_by_usuarios' => $clasificacion_dominios_by_usuarios, 
            'name_dominios' => $name_dominios,
            //dimensiones
          'clasificacion_dimensiones' => $clasificacion_dimensiones,  
          'name_dimensiones' => $name_dimensiones,
          'colores_dominios' => $colores_dominios,
          'tables_dim' => $tables_dim
    ], 200);

      

    }else{
          //  return json_encode($clasificacion_dimensiones,true);
          return view('nom035.graficos_by.tablero_'.$calificacion, compact('sucursales',
          'tabla_dominio',
          'dominio_seleccionado',
          'promedio_general',
          'directions',
          'jobs',
          'tables_cat', 
          'tables_dim',
          'users_count',
          'filtro_dominio',
          'sexs',
          'ages',
          'starts',
          'regiones',
          'clasificacion_dominiosxyz',
          'clasificacion_dominiosxy',
          'centros_trabajo',
          'period_id',
          'periodos',    
          'clasificacion_dimensiones',
          'name_dimensiones',
          'colores_dominios',
            //general
            'clasificacion_general',
            'resultado_general',
            'color_general',
            //categorias
            'clasificacion_categorias',
            'clasificacion_categorias_by_usuarios',
            'name_categories',
            //dominios
            'clasificacion_dominios',
            'clasificacion_dominios_by_usuarios',
            'name_dominios'
          //dimensiones	            
        ));

    }


}



}
                                        