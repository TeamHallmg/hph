<?php

namespace App\Http\Controllers\Nom035\Reportes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller; 
use DB;
use App\User;
use App\Employee;
use App\Models\Nom035\Evaluation;
use App\Models\Nom035\Factor;
use App\Models\Nom035\NormPeriod;
use App\Models\Nom035\NormPeriodUser;
use App\Models\Nom035\Answer;
use App\Models\Nom035\PlanAccion;
use App\Models\Nom035\PlanIndividual;
use App\Models\Nom035\PoliticasTextos;

use App\Models\Announcement\Announcement;
use App\Models\Announcement\AnnouncementUser;
use App\Models\Announcement\View;

use App\Models\JobPosition;
use App\Models\Direction;
use App\Models\Department;
use App\Models\Area;
use App\Models\Sucursal;
use Session;

class ReportesV2Controller extends Controller
{
    /**
     * Create a new Evaluacion Desempeno controller instance.
     *
     * @return void2
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('permission:see_progress')->only('avances');
    }

    
    // Listado de las personas que va a evaluar al que esta logueado
    public function reporte_general(){
      return view('nom035/admin/reportesV2/reporte_general');

    }

    public function avances(){

        $evaluados = array();
        $eval = Evaluation::where('status', 'activo')->first();
        $no_iniciados = $iniciados = $terminados = array();
        $canEdit = true;
        $periodoStatus = array();
        $enterprises_without_sucursals = $enterprises_with_sucursals = $sucursals = array();

        if (auth()->user()->role == 'admin' || auth()->user()->role == 'supervisor'){

          // Obtiene todos los periodos
          $periodoStatus = NormPeriod::where('status', '!=', 'Cancelado')->select('id', 'name')->orderBy('id', 'DESC')->get();
        }

        else{

          $permissions = auth()->user()->multiUserPermissions()->orderBy('enterprise_id')->orderBy('sucursal_id')->get();

          if (count($permissions)>0 && $permissions[0]->enterprise_id == 0){

            // Obtiene todos los periodos
            $periodoStatus = NormPeriod::where('status', '!=', 'Cancelado')->select('id', 'name')->orderBy('id', 'DESC')->get();
          }

          else{

            foreach ($permissions as $key => $value){
              
              if ($value->sucursal_id == 0){

                if (!in_array($value->enterprise_id, $enterprises_without_sucursals)){

                  $enterprises_without_sucursals[] = $value->enterprise_id;
                }
              }

              else{

                if (!in_array($value->enterprise_id, $enterprises_without_sucursals) && !in_array($value->sucursal_id, $sucursals)){

                  $enterprises_with_sucursals[] = $value->enterprise_id;
                  $sucursals[] = $value->sucursal_id;
                }
              }
            }

            $periods = array();

            if (!empty($enterprises_without_sucursals)){

              $periods = DB::table('nom035_period_user')->join('users', 'users.id', '=', 'nom035_period_user.user_id')->join('employees', 'employees.id', '=', 'users.employee_id')->whereIn('enterprise_id', $enterprises_without_sucursals)->groupBy('period_id')->pluck('period_id')->toArray();
            }

            if (!empty($sucursals)){

              foreach ($enterprises_with_sucursals as $key => $value){
                
                if (!in_array($value, $enterprises_without_sucursals)){

                  $sucursal = Sucursal::find($sucursals[$key]);
                  $periods2 = DB::table('nom035_period_user')->join('users', 'users.id', '=', 'nom035_period_user.user_id')->join('employees', 'employees.id', '=', 'users.employee_id')->where('enterprise_id', $value)->where('sucursal', $sucursal->name)->groupBy('period_id')->pluck('period_id')->toArray();
                  $periods = array_merge($periods, $periods2);
                }
              }
            }

            if (!empty($periods)){

              // Obtiene todos los periodos
              $periodoStatus = NormPeriod::where('status', '!=', 'Cancelado')->whereIn('id', $periods)->select('id', 'name')->orderBy('id', 'DESC')->get();
            }

            else{

              $canEdit = false;
            }
          }
        }

        //$periodoStatus = NormPeriod::where('status', 'Abierto')->orWhere('status', 'Cerrado')->select('id', 'name')->orderBy('id', 'DESC')->get();
        $total_evals = 0;
        $period_id = 0;
        $period = array();

        if (empty($periodoStatus) || $periodoStatus->isEmpty()){
            flash('No hay periodos disponibles por el momento...');
            return redirect('/nom035/que-es')->with('error','No tienes permiso para ver la evaluación');
        }

        if (!empty($_POST['period_id']) || Session::has('period_id')){

          if (!empty($_POST['period_id'])){

            $period_id = $_POST['period_id'];
            Session::put('period_id', $period_id);
          }

          else{

            $period_id = Session::get('period_id');
          }
        }

        else{
        
          $period_id = $periodoStatus[0]->id;
        }

        $period = NormPeriod::find($period_id);
        $cuestionarios = $period->getTypeOfQuestions();

        if (auth()->user()->role == 'admin' || auth()->user()->role == 'supervisor'){

          $evaluados = $period
          ->users()
          //->withTrashed('employee')
          ->get();
        }

        else{

          $permissions = auth()->user()->multiUserPermissions()->orderBy('enterprise_id')->orderBy('sucursal_id')->get();

          if (count($permissions)>0 && $permissions[0]->enterprise_id == 0){

            $evaluados = $period
            ->users()
            //->withTrashed('employee')
            ->get();
          }

          else{

            $enterprises_without_sucursals = $enterprises_with_sucursals = array();
            $sucursals = array();

            foreach ($permissions as $key => $value){
              
              if ($value->sucursal_id == 0){

                if (!in_array($value->enterprise_id, $enterprises_without_sucursals)){

                  $enterprises_without_sucursals[] = $value->enterprise_id;
                }
              }

              else{

                if (!in_array($value->enterprise_id, $enterprises_without_sucursals) && !in_array($value->sucursal_id, $sucursals)){

                  $enterprises_with_sucursals[] = $value->enterprise_id;
                  $sucursals[] = $value->sucursal_id;
                }
              }
            }

            if (!empty($enterprises_without_sucursals)){

              $evaluados = $period
              ->users()
              ->whereHas('employee', function($q) use($enterprises_without_sucursals){
                $q->whereIn('enterprise_id', $enterprises_without_sucursals);
                //$q->withTrashed();
              })
              ->get();
            }

            if (!empty($sucursals)){

              foreach ($enterprises_with_sucursals as $key => $value){
             
                $sucursal = Sucursal::find($sucursals[$key]);

                $temp = $period
                    ->users()
                    ->whereHas('employee', function($q) use($value, $sucursal){
                      $q->where('enterprise_id', $value);
                      $q->where('sucursal', $sucursal->name);
                      //$q->withTrashed();
                    })
                    ->get();

                if (count($evaluados) > 0){

                  $evaluados = $evaluados->merge($temp);
                }

                else{

                  $evaluados = $temp;
                }
              }
            }
          }
        }

        /*$evaluados = $period
        ->users()
        ->withTrashed(['employee' => function($q) {
            $q->orderBy('nombre');
        }])
        ->get();*/
        // $evaluados = \DB::table('periodo_evaluados')->join('personal', 'personal.id', '=', 'periodo_evaluados.id_evaluado')->join('users', 'users.employee_id', '=', 'personal.id')->where('id_periodo', $periodoAbierto->id)->select('idempleado', 'email', 'nombre', 'paterno', 'materno', 'rfc', 'personal.departamento', 'personal.puesto', 'id_evaluado', 'estado')->orderBy('nombre')->toSql();

        if (count($evaluados) > 0 && $canEdit){

          $ids_evaluados = array();

          foreach ($evaluados as $key => $value){
            
            $ids_evaluados[] = $value->id;
          }
        
          if (count($cuestionarios) == 1){
          
            $no_iniciados[] = $period->users()
            ->wherePivot('status', 1)->wherePivotIn('user_id', $ids_evaluados)
            ->count();
            // $no_iniciados = DB::table('periodo_evaluados')->where('id_periodo', $periodoAbierto->id)->where('estado', 1)->select('id_evaluado')->get();
            $iniciados[] = $period->users()
            ->wherePivot('status', 2)->wherePivotIn('user_id', $ids_evaluados)
            ->count();
            // $iniciados = DB::table('periodo_evaluados')->where('id_periodo', $periodoAbierto->id)->where('estado', 2)->select('id_evaluado')->get();
            $terminados[] = $period->users()
            ->wherePivot('status', 3)->wherePivotIn('user_id', $ids_evaluados)
            ->count();
            // $terminados = DB::table('periodo_evaluados')->where('id_periodo', $periodoAbierto->id)->where('estado', 3)->select('id_evaluado')->get();
            $total_evals = $no_iniciados[0] + $iniciados[0] + $terminados[0];
          }

          else{

            foreach ($cuestionarios as $key => $value){

              if ($key == 0){

                /*$no_iniciados[] = $periodoAbierto->users()
                ->wherePivot('status', '<', 21)
                ->count();*/
                $uno = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 11)->whereIn('user_id', $ids_evaluados)->count();
                $dos = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 12)->whereIn('user_id', $ids_evaluados)->count();
                $tres = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 13)->whereIn('user_id', $ids_evaluados)->count();
                $no_iniciados[] = $uno + $dos + $tres;
                // $no_iniciados = DB::table('periodo_evaluados')->where('id_periodo', $periodoAbierto->id)->where('estado', 1)->select('id_evaluado')->get();
                //->wherePivot('status', '>', 20)->wherePivot('status', '<', 31)
                //->count();
                $uno = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 21)->whereIn('user_id', $ids_evaluados)->count();
                $dos = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 22)->whereIn('user_id', $ids_evaluados)->count();
                $tres = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 23)->whereIn('user_id', $ids_evaluados)->count();
                $iniciados[] = $uno + $dos + $tres;
                $uno = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 31)->whereIn('user_id', $ids_evaluados)->count();
                $dos = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 32)->whereIn('user_id', $ids_evaluados)->count();
                $tres = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 33)->whereIn('user_id', $ids_evaluados)->count();
                $terminados[] = $uno + $dos + $tres;
                /*$terminados[] = $periodoAbierto->users()
                ->wherePivot('status', '>', 30)
                ->count();*/
                $total_evals = $no_iniciados[0] + $iniciados[0] + $terminados[0];
              }

              else{

                $uno = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 11)->whereIn('user_id', $ids_evaluados)->count();
                $dos = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 21)->whereIn('user_id', $ids_evaluados)->count();
                $tres = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 31)->whereIn('user_id', $ids_evaluados)->count();
                $no_iniciados[$key] = $uno + $dos + $tres;
                $uno = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 12)->whereIn('user_id', $ids_evaluados)->count();
                $dos = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 22)->whereIn('user_id', $ids_evaluados)->count();
                $tres = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 32)->whereIn('user_id', $ids_evaluados)->count();
                $iniciados[$key] = $uno + $dos + $tres;
                $uno = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 13)->whereIn('user_id', $ids_evaluados)->count();
                $dos = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 23)->whereIn('user_id', $ids_evaluados)->count();
                $tres = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 33)->whereIn('user_id', $ids_evaluados)->count();
                $terminados[$key] = $uno + $dos + $tres;
              }  
            }
          }
        }

        return view('nom035/Admin/reportesV2/avances', compact('evaluados', 'period', 'no_iniciados', 'iniciados', 'terminados', 'cuestionarios', 'total_evals', 'periodoStatus', 'period_id', 'canEdit'));
    }
  
  }