<?php

namespace App\Http\Controllers\Nom035\Groups;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Nom035\GroupJobPosition;
use App\Models\JobPosition;

use App\Http\Requests\GroupJobsRequest;

class GroupJobPositionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = GroupJobPosition::all();
        return view('nom035.groups.job_positions.index', compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jobPositions = JobPosition::with('area.department.direction')
        ->has('area.department.direction')
        ->orderBy('id')
        ->get();
        return view('nom035.groups.job_positions.create', compact('jobPositions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GroupJobsRequest $request)
    {
        \DB::beginTransaction();
        try {
            $group = GroupJobPosition::create([
                'name' => $request->name,
                'description' => $request->description,
            ]);
        } catch (\Throwable $th) {
            \DB::rollback();
            flash('Error al crear el grupo  |  ' . $th->getMessage())->error();
            return redirect()->route('nom035.grupos_puestos.create')->withInput();
        }
        try {
            $group->jobPositions()->attach($request->jobPositions);
            flash('Guardado correctamente')->success();
        } catch (\Throwable $th) {
            \DB::rollback();
            flash('Error al añadir los puestos al grupo  |  ' . $th->getMessage())->error();
            return redirect()->route('nom035.grupos_puestos.create')->withInput();
        }
        \DB::commit();
        return redirect()->route('nom035.grupos_puestos.show', $group->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(GroupJobPosition $group)
    {
        $group = $group->load('jobPositions.area.department.direction');
        return view('nom035.groups.job_positions.show', compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(GroupJobPosition $group)
    {
        $group = $group->load('jobPositions.area.department.direction');
        $groupJobPositions =  $group->jobPositions()->pluck('name', 'id')->toArray();
        $jobPositions = JobPosition::with('area.department.direction')
        ->has('area.department.direction')
        ->orderBy('id')
        ->get();
        return view('nom035.groups.job_positions.edit', compact('group', 'groupJobPositions', 'jobPositions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GroupJobsRequest $request, GroupJobPosition $group)
    {
        \DB::beginTransaction();
        try {
            $group->update([
                'name' => $request->name,
                'description' => $request->description,
            ]);
        } catch (\Throwable $th) {
            \DB::rollback();
            flash('Error al editar el grupo  |  ' . $th->getMessage())->error();
            return redirect()->route('nom035.grupos_puestos.edit', $group->id)->withInput();;
        }
        try {
            $group->jobPositions()->sync($request->jobPositions);
            flash('Actualizado correctamente')->success();
        } catch (\Throwable $th) {
            \DB::rollback();
            flash('Error al añadir los puestos al grupo  |  ' . $th->getMessage())->error();
            return redirect()->route('nom035.grupos_puestos.edit', $group->id)->withInput();
        }
        \DB::commit();
        return redirect()->route('nom035.grupos_puestos.edit', $group->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(GroupJobPosition $group)
    {
        \DB::beginTransaction();
        try {
            $group->jobPositions()->detach();
        } catch (\Throwable $th) {
            \DB::rollback();
            flash('Error al remover los puestos del grupo')->error();
            return redirect()->route('nom035.grupos_puestos.index');
        }
        try {
            $group->delete();
        } catch (\Throwable $th) {
            \DB::rollback();
            flash('Error al eliminar el grupo')->error();
            return redirect()->route('nom035.grupos_puestos.index');
        }
        \DB::commit();
        flash('Eliminado correctamente')->success();
        return redirect()->route('nom035.grupos_puestos.index');
    }
}
