<?php

namespace App\Http\Controllers\Nom035\Groups;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Nom035\GroupDepartment;
use App\Models\Department;

class GroupDepartmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = GroupDepartment::all();
        return view('nom035.groups.departments.index', compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        return view('nom035.groups.departments.create', compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \DB::beginTransaction();
        try {
            $group = GroupDepartment::create([
                'name' => $request->name,
                'description' => $request->description,
            ]);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        try {
            $group->departments()->attach($request->departments);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        \DB::commit();
        flash('Guardado correctamente')->success();
        return redirect()->to('nom035/grupos_departamentos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group = GroupDepartment::with('departments')->findOrFail($id);
        return view('nom035.groups.departments.show', compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = GroupDepartment::with('departments')->findOrFail($id);
        $groupDepartments =  $group->departments()->pluck('id')->toArray();
        $departments = Department::all();
        return view('nom035.groups.departments.edit', compact('group', 'groupDepartments', 'departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $group = GroupDepartment::with('departments')->findOrFail($id);
        \DB::beginTransaction();
        try {
            $group->update([
                'name' => $request->name,
                'description' => $request->description,
            ]);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        try {
            $group->departments()->sync($request->departments);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        \DB::commit();
        flash('Actualizado correctamente')->success();
        return redirect()->to('nom035/grupos_departamentos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = GroupDepartment::findOrFail($id);
        \DB::beginTransaction();
        try {
            $group->departments()->detach();
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        try {
            $group->delete();
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        \DB::commit();
        flash('Eliminado correctamente')->success();
        return redirect()->to('nom035/grupos_departamentos');
    }
}
