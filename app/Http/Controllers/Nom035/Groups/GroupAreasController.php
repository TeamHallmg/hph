<?php

namespace App\Http\Controllers\Nom035\Groups;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\GroupAreaRequest;

use App\Models\Nom035\GroupArea;
use App\Models\Area;

class GroupAreasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = GroupArea::all();
        return view('nom035.groups.areas.index', compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $areas = Area::with('department.direction')
        ->has('department.direction')
        ->orderBy('id')
        ->get();
        return view('nom035.groups.areas.create', compact('areas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GroupAreaRequest $request)
    {
        \DB::beginTransaction();
        try {
            $group = GroupArea::create([
                'name' => $request->name,
                'description' => $request->description,
            ]);
        } catch (\Throwable $th) {
            \DB::rollback();
            flash('Error al crear el grupo  |  ' . $th->getMessage())->error();
            return redirect()->route('nom035.grupos_areas.create')->withInput();
        }
        try {
            $group->areas()->attach($request->areas);
            flash('Guardado correctamente')->success();
        } catch (\Throwable $th) {
            \DB::rollback();
            flash('Error al añadir las areas al grupo  |  ' . $th->getMessage())->error();
            return redirect()->route('nom035.grupos_areas.create')->withInput();
        }
        \DB::commit();
        return redirect()->route('nom035.grupos_areas.show', $group->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(GroupArea $group)
    {
        $group = $group->load('areas.department.direction');
        return view('nom035.groups.areas.show', compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(GroupArea $group)
    {
        $group = $group->load('areas.department.direction');
        $groupAreas =  $group->areas()->pluck('name', 'id')->toArray();
        $areas = Area::with('department.direction')
        ->has('department.direction')
        ->orderBy('id')
        ->get();
        return view('nom035.groups.areas.edit', compact('group', 'groupAreas', 'areas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GroupAreaRequest $request, GroupArea $group)
    {
        \DB::beginTransaction();
        try {
            $group->update([
                'name' => $request->name,
                'description' => $request->description,
            ]);
        } catch (\Throwable $th) {
            \DB::rollback();
            flash('Error al editar el grupo  |  ' . $th->getMessage())->error();
            return redirect()->route('nom035.grupos_areas.edit', $group->id)->withInput();;
        }
        try {
            $group->areas()->sync($request->areas);
            flash('Actualizado correctamente')->success();
        } catch (\Throwable $th) {
            \DB::rollback();
            flash('Error al añadir los puestos al grupo  |  ' . $th->getMessage())->error();
            return redirect()->route('nom035.grupos_areas.edit', $group->id)->withInput();
        }
        \DB::commit();
        return redirect()->route('nom035.grupos_areas.edit', $group->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(GroupArea $group)
    {
        \DB::beginTransaction();
        try {
            $group->areas()->detach();
        } catch (\Throwable $th) {
            \DB::rollback();
            flash('Error al remover las areas del grupo')->error();
            return redirect()->route('nom035.grupos_areas.index');
        }
        try {
            $group->delete();
        } catch (\Throwable $th) {
            \DB::rollback();
            flash('Error al eliminar el grupo')->error();
            return redirect()->route('nom035.grupos_areas.index');
        }
        \DB::commit();
        flash('Eliminado correctamente')->success();
        return redirect()->route('nom035.grupos_areas.index');
    }
}
