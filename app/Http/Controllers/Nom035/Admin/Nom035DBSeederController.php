<?php

namespace App\Http\Controllers\Nom035\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Controller;
use App\Models\Nom035\QuestionGrouper;
use App\Models\Nom035\Category;
use App\Models\Nom035\Dimension;
use App\Models\Nom035\Domain;
use App\Models\Permission;
use App\Models\Role;
use App\Models\PermissionRole;
use App\Models\Announcement\View;
use App\Models\Announcement\AnnouncementType;
use App\Models\Announcement\AnnouncementForm;
use App\Models\Announcement\AnnouncementAttribute;
use App\Models\Announcement\AnnouncementTypePerView;
use DB;

class Nom035DBSeederController extends Controller
{
    public function index()
    {
        // try {
        //     Artisan::call('db:seed', ['--class' => 'CategorySeeder']);
        //     Artisan::call('db:seed', ['--class' => 'DimensionSeeder']);
        //     Artisan::call('db:seed', ['--class' => 'DomainSeeder']);
        //     Artisan::call('db:seed', ['--class' => 'Nom035PermissionsSeeder']);
        //     if(QuestionGrouper::get()->isEmpty()) {
        //         Artisan::call('db:seed', ['--class' => 'Nom35SectionsSeeder']);
        //     }
        // } catch(\Throwable $th) {
        //     dd($th);
        // }

        // ======================  AnuncioPolíticas  ======================
        View::firstOrCreate(['name' => 'nom035/politicas']);

        AnnouncementType::firstOrCreate([
            'name' => 'politicas',
            'show_name' => 'Políticas',
            'view_file' => 'politicas',
            'max_quantity_showed' => null,
            'max_quantity_stored' => null
        ]);

        $types = AnnouncementType::get()->pluck('id', 'name')->toArray();
        $attributes = AnnouncementAttribute::get()->pluck('id', 'form_name')->toArray();
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['politicas'],
            'announcement_attribute_id' => $attributes['title'],
        ],[
            'required' => 1,
        ]);
        AnnouncementForm::updateOrCreate([
            'announcement_type_id' => $types['politicas'],
            'announcement_attribute_id' => $attributes['description'],
        ],[
            'required' => 1,
        ]);

        $views = View::get()->pluck('id', 'name')->toArray();
        AnnouncementTypePerView::firstOrCreate([
            'announcement_type_id' => $types['banner'],
            'view_id' => $views['nom035/politicas'],
        ]);
        AnnouncementTypePerView::firstOrCreate([
            'announcement_type_id' => $types['politicas'],
            'view_id' => $views['nom035/politicas'],
        ]);
        // ====================== /AnuncioPolíticas  ======================

        // ====================== AnnouncementPermissionsSeeder  ======================
        Permission::updateOrCreate(
            ['action' => 'see_announcements'],
            ['name' => 'Índice Avisos', 'module' => 'Avisos']
        );

        Permission::updateOrCreate(
            ['action' => 'create_announcements'],
            ['name' => 'Crear Aviso', 'module' => 'Avisos']
        );

        Permission::updateOrCreate(
            ['action' => 'edit_announcements'],
            ['name' => 'Editar Aviso', 'module' => 'Avisos']
        );

        Permission::updateOrCreate(
            ['action' => 'delete_announcements'],
            ['name' => 'Eliminar Aviso', 'module' => 'Avisos']
        );

        Permission::updateOrCreate(
            ['action' => 'activate_announcements'],
            ['name' => 'Activar Aviso', 'module' => 'Avisos']
        );

        Permission::updateOrCreate(
            ['action' => 'announcement_carrousel'],
            ['name' => 'Administrar Carrousel', 'module' => 'Avisos']
        );

        Permission::updateOrCreate(
            ['action' => 'announcement_mosaic'],
            ['name' => 'Administrar Mosaico', 'module' => 'Avisos']
        );

        Permission::updateOrCreate(
            ['action' => 'announcement_simple_list'],
            ['name' => 'Administrar Lista Simple', 'module' => 'Avisos']
        );

        Permission::updateOrCreate(
            ['action' => 'announcement_banner'],
            ['name' => 'Administrar Banner', 'module' => 'Avisos']
        );

        Permission::updateOrCreate(
            ['action' => 'announcement_banner_panel'],
            ['name' => 'Administrar Banner Panel', 'module' => 'Avisos']
        );

        Permission::updateOrCreate(
            ['action' => 'announcement_gallery'],
            ['name' => 'Administrar Galería', 'module' => 'Avisos']
        );

        Permission::updateOrCreate(
            ['action' => 'announcement_upcoming_events'],
            ['name' => 'Administrar Próximos Eventos', 'module' => 'Avisos']
        );

        Permission::updateOrCreate(
            ['action' => 'announcement_panels'],
            ['name' => 'Administrar Paneles', 'module' => 'Avisos']
        );

        Permission::updateOrCreate(
            ['action' => 'announcement_cards'],
            ['name' => 'Administrar Cards', 'module' => 'Avisos']
        );

        Permission::updateOrCreate(
            ['action' => 'announcement_calendar'],
            ['name' => 'Administrar Calendario', 'module' => 'Avisos']
        );

        Permission::updateOrCreate(
            ['action' => 'announcement_agreement'],
            ['name' => 'Administrar Convenios', 'module' => 'Avisos']
        );

        Permission::updateOrCreate(
            ['action' => 'announcement_admin'],
            ['name' => 'Administrador Anuncios', 'module' => 'Avisos']
        );
        // ====================== /AnnouncementPermissionsSeeder  ======================

        // ======================  AnnouncementAdminSeeder  ======================
        $announcement_admin_role = Role::firstOrCreate([
            'name' => 'announcement_admin',
            'description' => 'Administrador de Avisos'
        ]);
        $announcement_permissions = Permission::where('module', 'Avisos')->get();

        if($announcement_admin_role) {
            foreach($announcement_permissions as $permission) {
                PermissionRole::firstOrCreate(
                    [
                        'permission_id' => $permission->id,
                        'role_id' => $announcement_admin_role->id
                    ]
                );
            }
        }
        // ====================== /AnnouncementAdminSeeder  ======================

        // ======================  CategoriesSeeder  ======================
        $categories = Category::get();
        if($categories->isEmpty()) {
            $data = [
                [
                    'name' => 'Ambiente de trabajo',
                ], [
                    'name' => 'Factores propios de la actividad',
                ], [
                    'name' => 'Organización del tiempo de trabajo',
                ], [
                    'name' => 'Liderazgo y relaciones en el trabajo',
                ]
            ];
            foreach($data as $row) {
                Category::create($row);
            }
        }
        // ====================== /CategoriesSeeder  ======================

        // ======================  DimensionSeeder  ======================
        $dimensions = Dimension::get();
        if($dimensions->isEmpty()) {
            $data = [
                [
                    'name' => 'Condiciones peligrosas e inseguras',
                ], [
                    'name' => 'Condiciones deficientes e insalubres',
                ], [
                    'name' => 'Trabajos peligrosos',
                ], [
                    'name' => 'Cargas cuantitativas',
                ], [
                    'name' => 'Ritmos de trabajo acelerado',
                ], [
                    'name' => 'Carga mental',
                ], [
                    'name' => 'Cargas psicológicas emocionales',
                ], [
                    'name' => 'Cargas de alta responsabilidad',
                ], [
                    'name' => 'Cargas contradictorias o inconsistentes',
                ], [
                    'name' => 'Falta de control y autonomía sobre el trabajo',
                ], [
                    'name' => 'Limitada o nula posibilidad de desarrollo',
                ], [
                    'name' => 'Limitada o inexistente capacitación',
                ], [
                    'name' => 'Jornadas de trabajo extensas',
                ], [
                    'name' => 'Influencia del trabajo fuera del centro laboral',
                ], [
                    'name' => 'Influencia de las responsabilidades familiares',
                ], [
                    'name' => 'Escasa claridad de funciones',
                ], [
                    'name' => 'Características del liderazgo',
                ], [
                    'name' => 'Relaciones sociales en el trabajo',
                ], [
                    'name' => 'Deficiente relación con los colaboradores que supervisa',
                ], [
                    'name' => 'Violencia laboral',
                ]
            ];
            foreach($data as $row) {
                Dimension::create($row);
            }
        }
        // ====================== /DimensionSeeder  ======================

        // ======================  DomainSeeder  ======================
        $domains = Domain::get();
        if($domains->isEmpty()) {
            $data = [
                [
                    'name' => 'Condiciones en el ambiente de trabajo'
                ], [
                    'name' => 'Carga de trabajo'
                ], [
                    'name' => 'Falta de control sobre el trabajo'
                ], [
                    'name' => 'Jornada de trabajo',
                ], [
                    'name' => 'Interferencia en la relación trabajo-familia'
                ], [
                    'name' => 'Liderazgo'
                ], [
                    'name' => 'Relaciones en el trabajo'
                ], [
                    'name' => 'Violencia'
                ]
            ];
            foreach($data as $row) {
                Domain::create($row);
            }
        }
        // ====================== /DomainSeeder  ======================

        // ======================  Nom035PermissionsSeeder  ======================
        $data = [
            ['action' => 'see_progress', 'name' => 'Ver Progresos', 'module' => 'Clima Organizacional'],
            ['action' => 'fill_survey', 'name' => 'Llenar Encuesta', 'module' => 'Clima Organizacional'],
            ['action' => 'see_reports', 'name' => 'Ver Reportes', 'module' => 'Clima Organizacional'],
            ['action' => 'see_factors', 'name' => 'Ver Factores', 'module' => 'Clima Organizacional'],
            ['action' => 'create_factors', 'name' => 'Crear Factores', 'module' => 'Clima Organizacional'],
            ['action' => 'edit_factors', 'name' => 'Editar Factores', 'module' => 'Clima Organizacional'],
            ['action' => 'destroy_factors', 'name' => 'Eliminar Facores', 'module' => 'Clima Organizacional'],
            ['action' => 'see_questions', 'name' => 'Ver Preguntas', 'module' => 'Clima Organizacional'],
            ['action' => 'create_questions', 'name' => 'Crear Preguntas', 'module' => 'Clima Organizacional'],
            ['action' => 'edit_factors', 'name' => 'Editar Preguntas', 'module' => 'Clima Organizacional'],
            ['action' => 'destroy_questions', 'name' => 'Eliminar Preguntas', 'module' => 'Clima Organizacional'],
            ['action' => 'see_periods', 'name' => 'Ver Periodos', 'module' => 'Clima Organizacional'],
            ['action' => 'create_periods', 'name' => 'Crear Periodos', 'module' => 'Clima Organizacional'],
            ['action' => 'edit_periods', 'name' => 'Editar Periodos', 'module' => 'Clima Organizacional'],
            ['action' => 'destroy_periods', 'name' => 'Eliminar Periodos', 'module' => 'Clima Organizacional']
        ];
        foreach($data as $row) {
            Permission::updateOrCreate([
                'action' => $row['action']
            ], [
                'name' => $row['name'],
                'module' => $row['module']
            ]);
        }
        // ====================== /Nom035PermissionsSeeder  ======================

        // ======================  Nom35SectionsSeeder  ======================
        if(QuestionGrouper::get()->isEmpty()) {
            //Cuestionario 1
            $cuestionario1_pregunta1 = QuestionGrouper::updateOrCreate([
                'question' => '¿Ha presenciado o sufrido alguna vez, durante o con motivo del trabajo un acontecimiento como los siguientes:<br>Accidente que tenga como consecuencia la muerte, la pérdida de un miembro o una lesión grave?<br>Asaltos?<br>Actos violentos que derivaron en lesiones graves?<br>Secuestro?<br>Amenazas?, o<br>Cualquier otro que ponga en riesgo su vida o salud, y/o la de otras personas?<br>',
                'cuestionario' => 'Cuestionario 1',
            ],[
                'show_on' => 'Si',
            ]);

            DB::table('nom035_factors')->insert([
                [
                    'name' => 'I.- Acontecimiento traumático severo',
                    //'type' => 'yes_no',
                    'question' => '¿Ha presenciado o sufrido alguna vez, durante o con motivo del trabajo un acontecimiento como los siguientes:<br><br><ul><li>Accidente que tenga como consecuencia la muerte, la pérdida de un miembro o una lesión grave</li><li>Asaltos</li><li>Actos violentos que derivaron en lesiones graves</li><li>Secuestro</li><li>Amenazas o</li><li>Cualquier otro que ponga en riesgo su vida o salud, y/o la de otras personas</li></ul>',
                    'cuestionario' => 'Cuestionario 1',
                ],[
                    'name' => 'II.- Recuerdos persistentes sobre el acontecimiento (durante el último mes):',
                    //'type' => 'yes_no',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 1',

                ],[
                    'name' => 'III.- Esfuerzo por evitar circunstancias parecidas o asociadas al acontecimiento (durante el último mes):',
                    //'type' => 'yes_no',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 1',
                ],[
                    'name' => 'IV Afectación (durante el último mes):',
                    //'type' => 'yes_no',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 1',
                ],[
                    'name' => '2.1 Para responder las preguntas siguientes considere las condiciones de su centro de trabajo, así como la cantidad y ritmo de trabajo.',
                    //'type' => 'multi',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 2',
                ],[
                    'name' => '2.2 Las preguntas siguientes están relacionadas con las actividades que realiza en su trabajo y las responsabilidades que tiene.',
                    //'type' => 'multi',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 2',
                ],[
                    'name' => '2.3 Las preguntas siguientes están relacionadas con el tiempo destinado a su trabajo y sus responsabilidades familiares.',
                    //'type' => 'multi',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 2',
                ],[
                    'name' => '2.4 Las preguntas siguientes están relacionadas con las decisiones que puede tomar en su trabajo.',
                    //'type' => 'multi',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 2',
                ],[
                    'name' => '2.5 Las preguntas siguientes están relacionadas con la capacitación e información que recibe sobre su trabajo.',
                    //'type' => 'multi',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 2',
                ],[
                    'name' => '2.6 Las preguntas siguientes se refieren a las relaciones con sus compañeros de trabajo y su jefe.',
                    //'type' => 'multi',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 2',
                ],[
                    'name' => '2.7 Las preguntas siguientes están relacionadas con la atención a clientes y usuarios.',
                    //'type' => 'yes_no',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 2',
                ]
                /////CUESTIONARIO 3
                ,[
                    'name' => '3.1 Para responder las preguntas siguientes considere las condiciones ambientales de su centro de trabajo.',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'name' => '3.2 Para responder a las preguntas siguientes piense en la cantidad y ritmo de trabajo que tiene.',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'name' => '3.3 Las preguntas siguientes están relacionadas con el esfuerzo mental que le exige su trabajo.',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'name' => '3.4 Las preguntas siguientes están relacionadas con las actividades que realiza en su trabajo y las responsabilidades que tiene.',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'name' => '3.5 Las preguntas siguientes están relacionadas con su jornada de trabajo.',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'name' => '3.6 Las preguntas siguientes están relacionadas con las decisiones que puede tomar en su trabajo.',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'name' => '3.7 Las preguntas siguientes están relacionadas con cualquier tipo de cambio que ocurra en su trabajo (considere los últimos cambios realizados).',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'name' => '3.8 Las preguntas siguientes están relacionadas con la capacitación e información que se le proporciona sobre su trabajo.',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'name' => '3.9 Las preguntas siguientes están relacionadas con el o los jefes con quien tiene contacto.',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'name' => '3.10 Las preguntas siguientes se refieren a las relaciones con sus compañeros.',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'name' => '3.11 Las preguntas siguientes están relacionadas con la información que recibe sobre su rendimiento en el trabajo, el reconocimiento, el sentido de pertenencia y la estabilidad que le ofrece su trabajo.',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'name' => '3.12 Las preguntas siguientes están relacionadas con actos de violencia laboral (malos tratos, acoso, hostigamiento, acoso psicológico).',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'name' => '3.13 Las preguntas siguientes están relacionadas con la atención a clientes y usuarios.',
                    'question' => '',
                    'cuestionario' => 'Cuestionario 3',
                ]
            ]);

            //Cuestionario 1
            DB::table('nom035_questions')->insert([
                [
                    'question' => '¿Ha tenido recuerdos recurrentes sobre el acontecimiento que le provocan malestares?',
                    'positive' => 1,
                    'grouper_id' => $cuestionario1_pregunta1->id,
                    'parent_factor_id' => 1,
                    'factor_id' => 2,
                    'cuestionario' => 'Cuestionario 1',
                    'type' => 'yes_no',
                ],[
                    'question' => '¿Ha tenido sueños de carácter recurrente sobre el acontecimiento, que le producen malestar?',
                    'positive' => 1,
                    'grouper_id' => $cuestionario1_pregunta1->id,
                    'parent_factor_id' => 1,
                    'factor_id' => 2,
                    'cuestionario' => 'Cuestionario 1',
                    'type' => 'yes_no',
                ]
            ]);
            DB::table('nom035_questions')->insert([
                [
                    'question' => '¿Se ha esforzado por evitar todo tipo de sentimientos, conversaciones o situaciones que le puedan recordar el acontecimiento?',
                    'positive' => 1,
                    'grouper_id' => $cuestionario1_pregunta1->id,
                    'parent_factor_id' => 1,
                    'factor_id' => 3,
                    'cuestionario' => 'Cuestionario 1',
                    'type' => 'yes_no',
                ],[
                    'question' => '¿Se ha esforzado por evitar todo tipo de actividades, lugares o personas que motivan recuerdos del acontecimiento?',
                    'positive' => 1,
                    'grouper_id' => $cuestionario1_pregunta1->id,
                    'parent_factor_id' => 1,
                    'factor_id' => 3,
                    'cuestionario' => 'Cuestionario 1',
                    'type' => 'yes_no',
                ],[
                    'question' => '¿Ha tenido dificultad para recordar alguna parte importante del evento?',
                    'positive' => 1,
                    'grouper_id' => $cuestionario1_pregunta1->id,
                    'parent_factor_id' => 1,
                    'factor_id' => 3,
                    'cuestionario' => 'Cuestionario 1',
                    'type' => 'yes_no',
                ],[
                    'question' => '¿Ha disminuido su interés en sus actividades cotidianas?',
                    'positive' => 1,
                    'grouper_id' => $cuestionario1_pregunta1->id,
                    'parent_factor_id' => 1,
                    'factor_id' => 3,
                    'cuestionario' => 'Cuestionario 1',
                    'type' => 'yes_no',
                ],[
                    'question' => '¿Se ha sentido usted alejado o distante de los demás?',
                    'positive' => 1,
                    'grouper_id' => $cuestionario1_pregunta1->id,
                    'parent_factor_id' => 1,
                    'factor_id' => 3,
                    'cuestionario' => 'Cuestionario 1',
                    'type' => 'yes_no',
                ],[
                    'question' => '¿Ha notado que tiene dificultad para expresar sus sentimientos?',
                    'positive' => 1,
                    'grouper_id' => $cuestionario1_pregunta1->id,
                    'parent_factor_id' => 1,
                    'factor_id' => 3,
                    'cuestionario' => 'Cuestionario 1',
                    'type' => 'yes_no',
                ],[
                    'question' => '¿Ha tenido la impresión de que su vida se va a acortar, que va a morir antes que otras personas o que tiene un futuro limitado?',
                    'positive' => 1,
                    'grouper_id' => $cuestionario1_pregunta1->id,
                    'parent_factor_id' => 1,
                    'factor_id' => 3,
                    'cuestionario' => 'Cuestionario 1',
                    'type' => 'yes_no',
                ]
            ]);
            DB::table('nom035_questions')->insert([    
                [
                    'question' => '¿Ha tenido usted dificultades para dormir?',
                    'positive' => 1,
                    'grouper_id' => $cuestionario1_pregunta1->id,
                    'parent_factor_id' => 1,
                    'factor_id' => 4,
                    'cuestionario' => 'Cuestionario 1',
                    'type' => 'yes_no',
                ],[
                    'question' => '¿Ha estado particularmente irritable o le han dado arranques de coraje?',
                    'positive' => 1,
                    'grouper_id' => $cuestionario1_pregunta1->id,
                    'parent_factor_id' => 1,
                    'factor_id' => 4,
                    'cuestionario' => 'Cuestionario 1',
                    'type' => 'yes_no',
                ],[
                    'question' => '¿Ha tenido dificultad para concentrarse?',
                    'positive' => 1,
                    'grouper_id' => $cuestionario1_pregunta1->id,
                    'parent_factor_id' => 1,
                    'factor_id' => 4,
                    'cuestionario' => 'Cuestionario 1',
                    'type' => 'yes_no',
                ],[
                    'question' => '¿Ha estado nervioso o constantemente en alerta?',
                    'positive' => 1,
                    'grouper_id' => $cuestionario1_pregunta1->id,
                    'parent_factor_id' => 1,
                    'factor_id' => 4,
                    'cuestionario' => 'Cuestionario 1',
                    'type' => 'yes_no',
                ],[
                    'question' => '¿Se ha sobresaltado fácilmente por cualquier cosa?',
                    'positive' => 1,
                    'grouper_id' => $cuestionario1_pregunta1->id,
                    'parent_factor_id' => 1,
                    'factor_id' => 4,
                    'cuestionario' => 'Cuestionario 1',
                    'type' => 'yes_no',
                ]
            ]);

            //Cuestionario 2
            DB::table('nom035_questions')->insert([
                [
                    'question' => 'Mi trabajo me exige hacer mucho esfuerzo físico',
                    'positive' => 0,
                    'factor_id' => 5,
                    'cuestionario' => 'Cuestionario 2',
                    'type' => 'multi',
                    'dimension_id' => 2,
                    'domain_id' => 1,
                    'category_id' => 1,
                ],[
                    'question' => 'Me preocupa sufrir un accidente en mi trabajo',
                    'positive' => 0,
                    'factor_id' => 5,
                    'cuestionario' => 'Cuestionario 2',
                    'type' => 'multi',
                    'dimension_id' => 1,
                    'domain_id' => 1,
                    'category_id' => 1,
                ],[
                    'question' => 'Considero que las actividades que realizo son peligrosas',
                    'positive' => 0,
                    'factor_id' => 5,
                    'cuestionario' => 'Cuestionario 2',
                    'type' => 'multi',
                    'dimension_id' => 3,
                    'domain_id' => 1,
                    'category_id' => 1,
                ],[
                    'question' => 'Por la cantidad de trabajo que tengo debo quedarme tiempo adicional a mi turno',
                    'positive' => 0,
                    'factor_id' => 5,
                    'cuestionario' => 'Cuestionario 2',
                    'type' => 'multi',
                    'dimension_id' => 4,
                    'domain_id' => 2,
                    'category_id' => 2,
                ],[
                    'question' => 'Por la cantidad de trabajo que tengo debo trabajar sin parar',
                    'positive' => 0,
                    'factor_id' => 5,
                    'cuestionario' => 'Cuestionario 2',
                    'type' => 'multi',
                    'dimension_id' => 5,
                    'domain_id' => 2,
                    'category_id' => 2,
                ],[
                    'question' => 'Considero que es necesario mantener un ritmo de trabajo acelerado',
                    'positive' => 0,
                    'factor_id' => 5,
                    'cuestionario' => 'Cuestionario 2',
                    'type' => 'multi',
                    'dimension_id' => 5,
                    'domain_id' => 2,
                    'category_id' => 2,
                ],[
                    'question' => 'Mi trabajo exige que esté muy concentrado',
                    'positive' => 0,
                    'factor_id' => 5,
                    'cuestionario' => 'Cuestionario 2',
                    'type' => 'multi',
                    'dimension_id' => 6,
                    'domain_id' => 2,
                    'category_id' => 2,
                ],[
                    'question' => 'Mi trabajo requiere que memorice mucha información',
                    'positive' => 0,
                    'factor_id' => 5,
                    'cuestionario' => 'Cuestionario 2',
                    'type' => 'multi',
                    'dimension_id' => 6,
                    'domain_id' => 2,
                    'category_id' => 2,
                ],[
                    'question' => 'Mi trabajo exige que atienda varios asuntos al mismo tiempo',
                    'positive' => 0,
                    'factor_id' => 5,
                    'cuestionario' => 'Cuestionario 2',
                    'type' => 'multi',
                    'dimension_id' => 4,
                    'domain_id' => 2,
                    'category_id' => 2,
                ],
            ]);
            DB::table('nom035_questions')->insert([
                [
                    'question' => 'En mi trabajo soy responsable de cosas de mucho valor',
                    'positive' => 0,
                    'factor_id' => 6,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 9,
                    'domain_id' => 2,
                    'category_id' => 2,
                ],[
                    'question' => 'Respondo ante mi jefe por los resultados de toda mi área de trabajo',
                    'positive' => 0,
                    'factor_id' => 6,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 9,
                    'domain_id' => 2,
                    'category_id' => 2,
                ],[
                    'question' => 'En mi trabajo me dan órdenes contradictorias',
                    'positive' => 0,
                    'factor_id' => 6,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 10,
                    'domain_id' => 2,
                    'category_id' => 2,
                ],[
                    'question' => 'Considero que en mi trabajo me piden hacer cosas innecesarias',
                    'positive' => 0,
                    'factor_id' => 6,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 10,
                    'domain_id' => 2,
                    'category_id' => 2,
                ]
            ]);
            DB::table('nom035_questions')->insert([
                [
                    'question' => 'Trabajo horas extras más de tres veces a la semana',
                    'positive' => 0,
                    'factor_id' => 7,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 13,
                    'domain_id' => 4,
                    'category_id' => 3,
                ],[
                    'question' => 'Mi trabajo me exige laborar en días de descanso, festivos o fines de semana',
                    'positive' => 0,
                    'factor_id' => 7,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 13,
                    'domain_id' => 4,
                    'category_id' => 3,
                ],[
                    'question' => 'Considero que el tiempo en el trabajo es mucho y perjudica mis actividades familiares o personales',
                    'positive' => 0,
                    'factor_id' => 7,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 14,
                    'domain_id' => 5,
                    'category_id' => 3,
                ],[
                    'question' => 'Pienso en las actividades familiares o personales cuando estoy en mi trabajo',
                    'positive' => 0,
                    'factor_id' => 7,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 15,
                    'domain_id' => 5,
                    'category_id' => 3,
                ]
            ]);
            DB::table('nom035_questions')->insert([
                [
                    'question' => 'Mi trabajo permite que desarrolle nuevas habilidades',
                    'positive' => 1,
                    'factor_id' => 8,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 11,
                    'domain_id' => 3,
                    'category_id' => 2,
                ],[
                    'question' => 'En mi trabajo puedo aspirar a un mejor puesto',
                    'positive' => 1,
                    'factor_id' => 8,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 11,
                    'domain_id' => 3,
                    'category_id' => 2,
                ],[
                    'question' => 'Durante mi jornada de trabajo puedo tomar pausas cuando las necesito',
                    'positive' => 1,
                    'factor_id' => 8,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 10,
                    'domain_id' => 3,
                    'category_id' => 2,
                ],[
                    'question' => 'Puedo decidir la velocidad a la que realizo mis actividades en mi trabajo',
                    'positive' => 1,
                    'factor_id' => 8,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 10,
                    'domain_id' => 3,
                    'category_id' => 2,
                ],[
                    'question' => 'Puedo cambiar el orden de las actividades que realizo en mi trabajo',
                    'positive' => 1,
                    'factor_id' => 8,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 10,
                    'domain_id' => 3,
                    'category_id' => 2,
                ],
            ]);
            DB::table('nom035_questions')->insert([
                [
                    'question' => 'Me informan con claridad cuáles son mis funciones',
                    'positive' => 1,
                    'factor_id' => 9,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 16,
                    'domain_id' => 6,
                    'category_id' => 4,
                ],[
                    'question' => 'Me explican claramente los resultados que debo obtener en mi trabajo',
                    'positive' => 1,
                    'factor_id' => 9,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 16,
                    'domain_id' => 6,
                    'category_id' => 4,
                ],[
                    'question' => 'Me informan con quién puedo resolver problemas o asuntos de trabajo',
                    'positive' => 1,
                    'factor_id' => 9,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 16,
                    'domain_id' => 6,
                    'category_id' => 4,
                ],[
                    'question' => 'Me permiten asistir a capacitaciones relacionadas con mi trabajo',
                    'positive' => 1,
                    'factor_id' => 9,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 12,
                    'domain_id' => 3,
                    'category_id' => 2,
                ],[
                    'question' => 'Recibo capacitación útil para hacer mi trabajo',
                    'positive' => 1,
                    'factor_id' => 9,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 12,
                    'domain_id' => 3,
                    'category_id' => 2,
                ],
            ]);
            DB::table('nom035_questions')->insert([
                [
                    'question' => 'Mi jefe tiene en cuenta mis puntos de vista y opiniones',
                    'positive' => 1,
                    'factor_id' => 10,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 17,
                    'domain_id' => 6,
                    'category_id' => 4,
                ],[
                    'question' => 'Mi jefe ayuda a solucionar los problemas que se presentan en el trabajo',
                    'positive' => 1,
                    'factor_id' => 10,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 17,
                    'domain_id' => 6,
                    'category_id' => 4,
                ],[
                    'question' => 'Puedo confiar en mis compañeros de trabajo',
                    'positive' => 1,
                    'factor_id' => 10,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 18,
                    'domain_id' => 7,
                    'category_id' => 4,
                ],[
                    'question' => 'Cuando tenemos que realizar trabajo de equipo los compañeros colaboran',
                    'positive' => 1,
                    'factor_id' => 10,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 18,
                    'domain_id' => 7,
                    'category_id' => 4,
                ],[
                    'question' => 'Mis compañeros de trabajo me ayudan cuando tengo dificultades',
                    'positive' => 1,
                    'factor_id' => 10,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 18,
                    'domain_id' => 7,
                    'category_id' => 4,
                ],[
                    'question' => 'En mi trabajo puedo expresarme libremente sin interrupciones',
                    'positive' => 1,
                    'factor_id' => 10,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 20,
                    'domain_id' => 8,
                    'category_id' => 4,
                ],[
                    'question' => 'Recibo críticas constantes a mi persona y/o trabajo',
                    'positive' => 0,
                    'factor_id' => 10,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 20,
                    'domain_id' => 8,
                    'category_id' => 4,
                ],[
                    'question' => 'Recibo burlas, calumnias, difamaciones, humillaciones o ridiculizaciones',
                    'positive' => 0,
                    'factor_id' => 10,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 20,
                    'domain_id' => 8,
                    'category_id' => 4,
                ],[
                    'question' => 'Se ignora mi presencia o se me excluye de las reuniones de trabajo y en la toma de decisiones',
                    'positive' => 0,
                    'factor_id' => 10,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 20,
                    'domain_id' => 8,
                    'category_id' => 4,
                ],[
                    'question' => 'Se manipulan las situaciones de trabajo para hacerme parecer un mal trabajador',
                    'positive' => 0,
                    'factor_id' => 10,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 20,
                    'domain_id' => 8,
                    'category_id' => 4,
                ],[
                    'question' => 'Se ignoran mis éxitos laborales y se atribuyen a otros trabajadores',
                    'positive' => 0,
                    'factor_id' => 10,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 20,
                    'domain_id' => 8,
                    'category_id' => 4,
                ],[
                    'question' => 'Me bloquean o impiden las oportunidades que tengo para obtener ascenso o mejora en mi trabajo',
                    'positive' => 0,
                    'factor_id' => 10,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 20,
                    'domain_id' => 8,
                    'category_id' => 4,
                ],[
                    'question' => 'He presenciado actos de violencia en mi centro de trabajo',
                    'positive' => 0,
                    'factor_id' => 10,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 20,
                    'domain_id' => 8,
                    'category_id' => 4,
                ],
            ]);

            $cuestionario2_pregunta1 = QuestionGrouper::updateOrCreate([
                'question' => 'En mi trabajo debo brindar servicio a clientes o usuarios',
                'cuestionario' => 'Cuestionario 2',
            ],[
                'show_on' => 'Si',
            ]);

            $cuestionario2_pregunta2 = QuestionGrouper::updateOrCreate([
                'question' => 'Soy jefe de otros trabajadores',
                'cuestionario' => 'Cuestionario 2',
            ],[
                'show_on' => 'Si',
            ]);

            DB::table('nom035_questions')->insert([
                [
                    'question' => 'Atiendo clientes o usuarios muy enojados',
                    'positive' => 0,
                    'grouper_id' => $cuestionario2_pregunta1->id,
                    'factor_id' => 11,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 7,
                    'domain_id' => 2,
                    'category_id' => 2,
                ],[
                    'question' => 'Mi trabajo me exige atender personas muy necesitadas de ayuda o enfermas',
                    'positive' => 0,
                    'grouper_id' => $cuestionario2_pregunta1->id,
                    'factor_id' => 11,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 7,
                    'domain_id' => 2,
                    'category_id' => 2,
                ],[
                    'question' => 'Para hacer mi trabajo debo demostrar sentimientos distintos a los míos',
                    'positive' => 0,
                    'grouper_id' => $cuestionario2_pregunta1->id,
                    'factor_id' => 11,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 7,
                    'domain_id' => 2,
                    'category_id' => 2,
                ],[
                    'question' => 'Comunican tarde los asuntos de trabajo',
                    'positive' => 0,
                    'grouper' => $cuestionario2_pregunta2->id,
                    'factor_id' => 11,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 19,
                    'domain_id' => 7,
                    'category_id' => 4,
                ],[
                    'question' => 'Dificultan el logro de los resultados del trabajo',
                    'positive' => 0,
                    'grouper' => $cuestionario2_pregunta2->id,
                    'factor_id' => 11,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 19,
                    'domain_id' => 7,
                    'category_id' => 4,
                ],[
                    'question' => 'Ignoran las sugerencias para mejorar su trabajo',
                    'positive' => 0,
                    'grouper' => $cuestionario2_pregunta2->id,
                    'factor_id' => 11,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 2',
                    'dimension_id' => 19,
                    'domain_id' => 7,
                    'category_id' => 4,
                ],
            ]);

            //Cuestionario 3
            DB::table('nom035_questions')->insert([
                [
                    'question' => 'El espacio donde trabajo me permite realizar mis actividades de manera segura e higiénica',
                    'positive' => 0,
                    'factor_id' => 12,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Mi trabajo me exige hacer mucho esfuerzo físico',
                    'positive' => 0,
                    'factor_id' => 12,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Me preocupa sufrir un accidente en mi trabajo',
                    'positive' => 0,
                    'factor_id' => 12,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Considero que en mi trabajo se aplican las normas de seguridad y salud en el trabajo',
                    'positive' => 0,
                    'factor_id' => 12,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Considero que las actividades que realizo son peligrosas',
                    'positive' => 0,
                    'factor_id' => 12,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],
            ]);
            DB::table('nom035_questions')->insert([
                [
                    'question' => 'Por la cantidad de trabajo que tengo debo quedarme tiempo adicional a mi turno',
                    'positive' => 0,
                    'factor_id' => 13,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Por la cantidad de trabajo que tengo debo trabajar sin parar',
                    'positive' => 0,
                    'factor_id' => 13,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Considero que es necesario mantener un ritmo de trabajo acelerado',
                    'positive' => 0,
                    'factor_id' => 13,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ]
            ]);
            DB::table('nom035_questions')->insert([
                [
                    'question' => 'Mi trabajo exige que esté muy concentrado',
                    'positive' => 0,
                    'factor_id' => 14,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Mi trabajo requiere que memorice mucha información',
                    'positive' => 0,
                    'factor_id' => 14,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'En mi trabajo tengo que tomar decisiones difíciles muy rápido',
                    'positive' => 0,
                    'factor_id' => 14,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Mi trabajo exige que atienda varios asuntos al mismo tiempo',
                    'positive' => 0,
                    'factor_id' => 14,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ]
            ]);
            DB::table('nom035_questions')->insert([
                [
                    'question' => 'En mi trabajo soy responsable de cosas de mucho valor',
                    'positive' => 0,
                    'factor_id' => 15,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Respondo ante mi jefe por los resultados de toda mi área de trabajo',
                    'positive' => 0,
                    'factor_id' => 15,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'En el trabajo me dan órdenes contradictorias',
                    'positive' => 0,
                    'factor_id' => 15,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Considero que en mi trabajo me piden hacer cosas innecesarias',
                    'positive' => 0,
                    'factor_id' => 15,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ]
            ]);
            DB::table('nom035_questions')->insert([
                [
                    'question' => 'Trabajo horas extras más de tres veces a la semana',
                    'positive' => 0,
                    'factor_id' => 16,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Mi trabajo me exige laborar en días de descanso, festivos o fines de semana',
                    'positive' => 0,
                    'factor_id' => 16,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Considero que el tiempo en el trabajo es mucho y perjudica mis actividades familiares o personales',
                    'positive' => 0,
                    'factor_id' => 16,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Debo atender asuntos de trabajo cuando estoy en casa',
                    'positive' => 0,
                    'factor_id' => 16,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Pienso en las actividades familiares o personales cuando estoy en mi trabajo',
                    'positive' => 0,
                    'factor_id' => 16,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Pienso que mis responsabilidades familiares afectan mi trabajo',
                    'positive' => 0,
                    'factor_id' => 16,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ]
            ]);
            DB::table('nom035_questions')->insert([
                [
                    'question' => 'Mi trabajo permite que desarrolle nuevas habilidades',
                    'positive' => 0,
                    'factor_id' => 17,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'En mi trabajo puedo aspirar a un mejor puesto',
                    'positive' => 0,
                    'factor_id' => 17,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Durante mi jornada de trabajo puedo tomar pausas cuando las necesito',
                    'positive' => 0,
                    'factor_id' => 17,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Puedo decidir cuánto trabajo realizo durante la jornada laboral',
                    'positive' => 0,
                    'factor_id' => 17,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Puedo decidir la velocidad a la que realizo mis actividades en mi trabajo',
                    'positive' => 0,
                    'factor_id' => 17,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Puedo cambiar el orden de las actividades que realizo en mi trabajo',
                    'positive' => 0,
                    'factor_id' => 17,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ]
            ]);
            DB::table('nom035_questions')->insert([
                [
                    'question' => 'Los cambios que se presentan en mi trabajo dificultan mi labor',
                    'positive' => 0,
                    'factor_id' => 18,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Cuando se presentan cambios en mi trabajo se tienen en cuenta mis ideas o aportaciones',
                    'positive' => 0,
                    'factor_id' => 18,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ]
            ]);
            DB::table('nom035_questions')->insert([
                [
                    'question' => 'Me informan con claridad cuáles son mis funciones',
                    'positive' => 0,
                    'factor_id' => 19,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Me explican claramente los resultados que debo obtener en mi trabajo',
                    'positive' => 0,
                    'factor_id' => 19,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Me explican claramente los objetivos de mi trabajo',
                    'positive' => 0,
                    'factor_id' => 19,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Me informan con quién puedo resolver problemas o asuntos de trabajo',
                    'positive' => 0,
                    'factor_id' => 19,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Me permiten asistir a capacitaciones relacionadas con mi trabajo',
                    'positive' => 0,
                    'factor_id' => 19,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Recibo capacitación útil para hacer mi trabajo',
                    'positive' => 0,
                    'factor_id' => 19,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ]
            ]);
            DB::table('nom035_questions')->insert([
                [
                    'question' => 'Mi jefe ayuda a organizar mejor el trabajo',
                    'positive' => 0,
                    'factor_id' => 20,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Mi jefe tiene en cuenta mis puntos de vista y opiniones',
                    'positive' => 0,
                    'factor_id' => 20,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Mi jefe me comunica a tiempo la información relacionada con el trabajo',
                    'positive' => 0,
                    'factor_id' => 20,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'La orientación que me da mi jefe me ayuda a realizar mejor mi trabajo',
                    'positive' => 0,
                    'factor_id' => 20,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Mi jefe ayuda a solucionar los problemas que se presentan en el trabajo',
                    'positive' => 0,
                    'factor_id' => 20,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ]
            ]);
            DB::table('nom035_questions')->insert([
                [
                    'question' => 'Puedo confiar en mis compañeros de trabajo',
                    'positive' => 0,
                    'factor_id' => 21,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Entre compañeros solucionamos los problemas de trabajo de forma respetuosa',
                    'positive' => 0,
                    'factor_id' => 21,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'En mi trabajo me hacen sentir parte del grupo',
                    'positive' => 0,
                    'factor_id' => 21,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Cuando tenemos que realizar trabajo de equipo los compañeros colaboran',
                    'positive' => 0,
                    'factor_id' => 21,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Mis compañeros de trabajo me ayudan cuando tengo dificultades',
                    'positive' => 0,
                    'factor_id' => 21,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ]
            ]);
            DB::table('nom035_questions')->insert([
                [
                    'question' => 'Me informan sobre lo que hago bien en mi trabajo',
                    'positive' => 0,
                    'factor_id' => 22,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'La forma como evalúan mi trabajo en mi centro de trabajo me ayuda a mejorar mi desempeño',
                    'positive' => 0,
                    'factor_id' => 22,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'En mi centro de trabajo me pagan a tiempo mi salario',
                    'positive' => 0,
                    'factor_id' => 22,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'El pago que recibo es el que merezco por el trabajo que realizo',
                    'positive' => 0,
                    'factor_id' => 22,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Si obtengo los resultados esperados en mi trabajo me recompensan o reconocen',
                    'positive' => 0,
                    'factor_id' => 22,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Las personas que hacen bien el trabajo pueden crecer laboralmente',
                    'positive' => 0,
                    'factor_id' => 22,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Considero que mi trabajo es estable',
                    'positive' => 0,
                    'factor_id' => 22,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'En mi trabajo existe continua rotación de personal',
                    'positive' => 0,
                    'factor_id' => 22,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Siento orgullo de laborar en este centro de trabajo',
                    'positive' => 0,
                    'factor_id' => 22,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Me siento comprometido con mi trabajo',
                    'positive' => 0,
                    'factor_id' => 22,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ]
            ]);
            DB::table('nom035_questions')->insert([
                [
                    'question' => 'En mi trabajo puedo expresarme libremente sin interrupciones',
                    'positive' => 0,
                    'factor_id' => 23,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Recibo críticas constantes a mi persona y/o trabajo',
                    'positive' => 0,
                    'factor_id' => 23,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Recibo burlas, calumnias, difamaciones, humillaciones o ridiculizaciones',
                    'positive' => 0,
                    'factor_id' => 23,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Se ignora mi presencia o se me excluye de las reuniones de trabajo y en la toma de decisiones',
                    'positive' => 0,
                    'factor_id' => 23,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Se manipulan las situaciones de trabajo para hacerme parecer un mal trabajador',
                    'positive' => 0,
                    'factor_id' => 23,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Se ignoran mis éxitos laborales y se atribuyen a otros trabajadores',
                    'positive' => 0,
                    'factor_id' => 23,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Me bloquean o impiden las oportunidades que tengo para obtener ascenso o mejora en mi trabajo',
                    'positive' => 0,
                    'factor_id' => 23,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'He presenciado actos de violencia en mi centro de trabajo',
                    'positive' => 0,
                    'factor_id' => 23,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ]
            ]);
            
            $cuestionario3_pregunta1 = QuestionGrouper::updateOrCreate([
                'question' => 'En mi trabajo debo brindar servicio a clientes o usuarios',
                'cuestionario' => 'Cuestionario 3',
            ],[
                'show_on' => 'Si',
            ]);

            $cuestionario3_pregunta2 = QuestionGrouper::updateOrCreate([
                'question' => 'Soy jefe de otros trabajadores',
                'cuestionario' => 'Cuestionario 3',
            ],[
                'show_on' => 'Si',
            ]);

            DB::table('nom035_questions')->insert([
                [
                    'question' => 'Atiendo clientes o usuarios muy enojados',
                    'positive' => 0,
                    'grouper_id' => $cuestionario3_pregunta1->id,
                    'factor_id' => 24,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Mi trabajo me exige atender personas muy necesitadas de ayuda o enfermas',
                    'positive' => 0,
                    'grouper_id' => $cuestionario3_pregunta1->id,
                    'factor_id' => 24,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Para hacer mi trabajo debo demostrar sentimientos distintos a los míos',
                    'positive' => 0,
                    'grouper_id' => $cuestionario3_pregunta1->id,
                    'factor_id' => 24,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Mi trabajo me exige atender situaciones de violencia',
                    'positive' => 0,
                    'grouper_id' => $cuestionario3_pregunta1->id,
                    'factor_id' => 24,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Comunican tarde los asuntos de trabajo',
                    'positive' => 0,
                    'grouper_id' => $cuestionario3_pregunta2->id,
                    'factor_id' => 24,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Dificultan el logro de los resultados del trabajo',
                    'positive' => 0,
                    'grouper_id' => $cuestionario3_pregunta2->id,
                    'factor_id' => 24,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Cooperan poco cuando se necesita',
                    'positive' => 0,
                    'grouper_id' => $cuestionario3_pregunta2->id,
                    'factor_id' => 24,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ],[
                    'question' => 'Ignoran las sugerencias para mejorar su trabajo',
                    'positive' => 0,
                    'grouper_id' => $cuestionario3_pregunta2->id,
                    'factor_id' => 24,
                    'type' => 'multi',
                    'cuestionario' => 'Cuestionario 3',
                ]
            ]);
        }
        // ====================== /Nom35SectionsSeeder  ======================

        dd('Seeders runned!');
    }
}
