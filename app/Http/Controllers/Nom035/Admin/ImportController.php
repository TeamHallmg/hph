<?php

namespace App\Http\Controllers\Nom035\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Nom035\Factor;
use App\Models\Nom035\Question;

class ImportController extends Controller
{
    public function __construct(){
        $this->path = \Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
        $this->pathLayout = 'Interface/';
        $this->fileName = 'preguntas-factores.csv';
        $this->separador = ',';
    }

    public function index(){
        $data = $this->readFile();
        $data = $this->formatQuestionsPerFactor($data);
        $factors = $this->loadOrCreateFactors($data);
        $this->loadOrCreateQuestions($data, $factors);
        return redirect()->to('nom035/periodos');
    }

    public function readFile(){
        $File = fopen($this->path . $this->pathLayout . $this->fileName, 'r');
        $i = 0;
        
        $csvData = [];
        while ($line = fgetcsv($File, 0, $this->separador)){
            $data = [];
            for($i = 0; $i < count($line); $i++){
                $data[] = trim($line[$i]);
            }
            $csvData[] = $data;
        }
        return $csvData;
    }

    public function formatQuestionsPerFactor($data){
        $convertData = [];
        foreach($data as $line){
            $convertData[$line[1]][] = $line[0];
        }
        return $convertData;
    }

    public function loadOrCreateFactors($data){
        $factors = [];
        foreach($data as $key => $line){
            if(!empty($key)){
                try {
                    $factors[$key] = Factor::firstOrCreate([
                        'name' => $key,
                    ])->id;
                } catch (\Throwable $th) {
                    dd($th->getMessage());
                }      
            }
        }
        return $factors;
    }

    public function loadOrCreateQuestions($data, $factors){
        foreach ($data as $key => $questions) {
            foreach($questions as $question){
                if(!empty($key)){
                    try {
                        Question::firstOrCreate([
                            'question' => $question,
                            'factor_id' => isset($factors[$key])?$factors[$key]:null,
                        ]);
                    } catch (\Throwable $th) {
                        dd($th->getMessage(), $question, $factors, $key);
                    }
                }else{
                    try {
                        Question::firstOrCreate([
                            'question' => $question,
                            'factor_id' => null,
                        ]);
                    } catch (\Throwable $th) {
                        dd($th->getMessage());
                    }
                }
            }
        }
    }
}
