<?php

namespace App\Http\Controllers\Nom035\Admin;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Sucursal;
use App\Models\Nom035\Answer;
use App\Models\Nom035\NormPeriod;
use App\Models\Nom035\Category;
use App\Models\Nom035\Domain;
use App\Models\Nom035\Dimension;
use App\Models\Nom035\Evaluation;
use App\Models\Nom035\PlanAccion;
use App\Models\Nom035\BitacoraPlanAccion;
use App\Models\Nom035\PlanIndividual;
use DB;
use Session;

class PlanAccionController extends Controller
{
    public function indexPersonas()
    {
        $periods = array();

        if (Auth::user()->role == 'admin' || in_array(0, auth()->user()->getNom035Regions(4))){

          $periods = NormPeriod::whereHas('questions', function($q){
                $q->where('cuestionario', 'Cuestionario 1');
          })
          ->where('status', 'Abierto')->orWhere('status', 'Cerrado')->select('id', 'name')->orderBy('id', 'DESC')->get();
        }

        else{

          if (auth()->user()->hasNom035Permissions(4)){

            $permissions = auth()->user()->getNom035Regions(4);
            $periods_ids = DB::table('nom035_period_user')->join('users', 'users.id', '=', 'nom035_period_user.user_id')->join('employees', 'employees.id', '=', 'users.employee_id')->whereIn('region_id', $permissions)->groupBy('period_id')->pluck('period_id')->toArray();
            $periods = NormPeriod::whereHas('questions', function($q){
                $q->where('cuestionario', 'Cuestionario 1');
            })
            ->whereIn('id', $periods_ids)->where('status', 'Abierto')->orWhere('status', 'Cerrado')->select('id', 'name')->orderBy('id', 'DESC')->get();
          }
        }

        if (empty($periods) || $periods->isEmpty()){
          flash('No hay periodos disponibles por el momento...');
          return redirect('/nom035/que-es');
        }

        $users = [];
        $users_ids = array();
        $period_id = 0;

        if (!empty($_POST['period_id'])){

          $period_id = $_POST['period_id'];
        }

        else{

          $period_id = $periods[0]->id;          
        }

        $period = NormPeriod::find($period_id);

        if (Auth::user()->role == 'admin' || in_array(0, auth()->user()->getNom035Regions(4))){

          $users_ids = DB::table('users')->join('employees', 'employees.id', '=', 'users.employee_id')->pluck('users.id')->toArray();
        }

        else{

          if (auth()->user()->hasNom035Permissions(4)){

            $permissions = auth()->user()->getNom035Regions(4);
            $users_ids = DB::table('users')->join('employees', 'employees.id', '=', 'users.employee_id')->whereIn('region_id', $permissions)->pluck('users.id')->toArray();
          }  
        }

        if (!empty($users_ids)){

          foreach($period->users as $user){

            if (!in_array($user->id, $users_ids)){

              continue;
            }
                
            foreach ($periods as $key => $value){

              if($this->needAttentionCuestionarioUno($user->id, $value->id)){
                    
                $user->planIndividual = DB::table('nom035_plan_individual')->where('period_id', $value->id)->where('user_id', $user->id)->orderBy('id', 'DESC')->first();
                $last_answer = DB::table('nom035_answers')->where('period_id', $value->id)->where('user_id', $user->id)->orderBy('updated_at', 'DESC')->first();

                if ($value->id == $period_id){

                  if (!empty($last_answer)){

                    $user->employee_wt->fecha_respuesta = substr($last_answer->updated_at, 0, 10);
                  }

                  if (isset($users[$user->id])){

                    if (!empty($users[$user->id]->other_periods)){

                      $user->other_periods = $users[$user->id]->other_periods;
                      $user->other_periods[] = $users[$user->id]->period_id;
                    }

                    else{

                      $user->other_periods = array($users[$user->id]->period_id);
                    }
                  }

                  $fecha_cierre = DB::table('nom035_cierre_plan_individual')->where('period_id', $period_id)->where('user_id', $user->id)->select('fecha_cierre')->first();

                  if (!empty($fecha_cierre)){

                    $user->fecha_cierre = $fecha_cierre->fecha_cierre;
                  }

                  $users[$user->id] = $user;
                }

                else{

                  if (isset($users[$user->id])){

                    if (empty($users[$user->id]->other_periods)){

                      $users[$user->id]->other_periods = array();
                    }

                    $users[$user->id]->other_periods[] = $value->id;
                  }

                  else{

                    $user->period_id = $value->id;
                    $user->other_periods = array($value->id);
                    $users[$user->id] = $user;
                  }
                }
              }
            }
          }
        }
        return view('nom035.reportes.accion_individual', compact('users', 'periods', 'period_id'));
    }

    public function exportPersonas($period_id = 0)
    {
        require_once 'PHPExcel.php';
        require_once 'PHPExcel/IOFactory.php';
        $evaluation = Evaluation::active()
        /*->with(['periods' => function($q){
            $q->with('users.employee')
            ->open()
            ->whereHas('questions', function($q){
                $q->where('cuestionario', 'Cuestionario 1');
            });
        }])*/
        ->first();

        $users = [];
        $users_ids = array();

        if (Auth::user()->role == 'admin'){

          $users_ids = DB::table('users')->join('employees', 'employees.id', '=', 'users.employee_id')->pluck('users.id')->toArray();
        }

        else{

          $permissions = Auth::user()->multiUserPermissions()->orderBy('enterprise_id')->orderBy('sucursal_id')->get();

          if ($permissions[0]->enterprise_id == 0){

            $users_ids = DB::table('users')->join('employees', 'employees.id', '=', 'users.employee_id')->pluck('users.id')->toArray();
          }

          else{

            $enterprises_without_sucursals = $enterprises_with_sucursals = array();
            $sucursals = array();

            foreach ($permissions as $key => $value){
              
              if ($value->sucursal_id == 0){

                if (!in_array($value->enterprise_id, $enterprises_without_sucursals)){

                  $enterprises_without_sucursals[] = $value->enterprise_id;
                }
              }

              else{

                if (!in_array($value->enterprise_id, $enterprises_without_sucursals) && !in_array($value->sucursal_id, $sucursals)){

                  $enterprises_with_sucursals[] = $value->enterprise_id;
                  $sucursals[] = $value->sucursal_id;
                }
              }
            }

            if (!empty($enterprises_without_sucursals)){

              $users_ids = DB::table('users')->join('employees', 'employees.id', '=', 'users.employee_id')->whereIn('enterprise_id', $enterprises_without_sucursals)->pluck('users.id')->toArray();
            }

            if (!empty($sucursals)){

              foreach ($enterprises_with_sucursals as $key => $value){
             
                $sucursal = Sucursal::find($sucursals[$key]);
                $temp = DB::table('users')->join('employees', 'employees.id', '=', 'users.employee_id')->where('sucursal', $sucursal->name)->where('enterprise_id', $value)->pluck('users.id')->toArray();

                if (!empty($users)){

                  $users_ids = array_merge($users_ids, $temp);
                }

                else{

                  $users_ids = $temp;
                }
              } 
            }
          }  
        }

        $periods = NormPeriod::all();

        //if($evaluation && count($evaluation->periods) > 0){
        if(count($periods) > 0){

          //foreach ($evaluation->periods as $key => $value){
          foreach ($periods as $key => $value){

            $period = $value;

            if ($period->id == $period_id){

              // Create new PHPExcel object
              $objPHPExcel = new \PHPExcel();

              // Create a first sheet, representing users
              $objPHPExcel->setActiveSheetIndex(0);
              $objPHPExcel->getActiveSheet()->setCellValue('A1', 'ID');
              $objPHPExcel->getActiveSheet()->setCellValue('B1', 'NOMBRE');
              $objPHPExcel->getActiveSheet()->setCellValue('C1', 'PUESTO');
              $objPHPExcel->getActiveSheet()->setCellValue('D1', 'DEPARTAMENTO');
              $objPHPExcel->getActiveSheet()->setCellValue('E1', 'DIRECCIÓN');
              $objPHPExcel->getActiveSheet()->setCellValue('F1', 'CENTRO DE TRABAJO');
              $objPHPExcel->getActiveSheet()->setCellValue('G1', 'HOSPITAL');
              $objPHPExcel->getActiveSheet()->setCellValue('H1', 'FECHA');
              $objPHPExcel->getActiveSheet()->setCellValue('I1', 'RESULTADO');
              $objPHPExcel->getActiveSheet()->setCellValue('J1', 'ÚLTIMO AVANCE');
              $objPHPExcel->getActiveSheet()->setCellValue('K1', 'ÁVANCES');
              $row = 2;
              $users = array();
              $users_names = array();

              foreach($period->users as $user){

                if (!in_array($user->id, $users_ids)){

                  continue;
                }

                if($this->needAttentionCuestionarioUno($user->id, $period->id)){

                  $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $user->employee_wt->idempleado);
                  $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $user->fullname);
                  $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, (!empty($user->employee_wt->jobPosition) ? $user->employee_wt->jobPosition->name : ''));
                  $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, (isset($user->employee_wt->jobPosition->area->department) ? $user->employee_wt->jobPosition->area->department->name : ''));
                  $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, (isset($user->employee_wt->jobPosition->area->department->direction) ? $user->employee_wt->jobPosition->area->department->direction->name : ''));
                  $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, (!empty($user->employee_wt->sucursal) ? $user->employee_wt->sucursal : ''));
                  $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, (isset($user->employee_wt->region) ? $user->employee_wt->region->name : ''));

                  $last_answer = DB::table('nom035_answers')->where('period_id', $period->id)->where('user_id', $user->id)->orderBy('updated_at', 'DESC')->first();

                  if (!empty($last_answer)){

                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, substr($last_answer->updated_at, 0, 10));
                  }

                  $objPHPExcel->getActiveSheet()->getStyle('I' . $row)->applyFromArray(array('fill' => array('type' => \PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'E3342F'))));
                  $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, 'Requiere valoración clínica');

                  $avances = PlanIndividual::where('user_id', $user->id)->where('period_id', $period->id)->orderBy('id', 'DESC')->get();

                  if (count($avances) > 0){

                    $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $avances[0]->avances);

                    if (count($avances) > 1){
                    
                      $row--;

                      foreach ($avances as $key => $avance){
                      
                        if ($key > 0){

                          $row++;
                          $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $avance->avances);
                        }
                      }
                    }
                  }

                  $row++;
                  $users[] = $user->id;
                  $users_names[] = $user->fullname;
                }
              }

              // Rename sheet
              $objPHPExcel->getActiveSheet()->setTitle('Personal');

              /*$objPHPExcel->createSheet();
              $objPHPExcel->setActiveSheetIndex(1);
              $objPHPExcel->getActiveSheet()->setCellValue('A1', 'NOMBRE');
              $objPHPExcel->getActiveSheet()->setCellValue('B1', 'AVANCE');
              $objPHPExcel->getActiveSheet()->setCellValue('C1', 'FECHA');
              $row = 2;

              foreach($users as $key => $user){

                  $planIndividual = DB::table('nom035_plan_individual')->where('period_id', $period->id)->where('user_id', $user)->orderBy('id')->get();

                  if (count($planIndividual) > 0){

                    foreach ($planIndividual as $key2 => $avance){
                      
                      $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $users_names[$key]);
                      $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $avance->avances);
                      $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $avance->created_at);
                      $row++;
                    }
                  }
              }

              // Rename sheet
              $objPHPExcel->getActiveSheet()->setTitle('Avances');*/

              // Redirect output to a client’s web browser (Excel5)
              header('Content-Type: application/vnd.ms-excel');
              header('Content-Disposition: attachment;filename="Personal con Problemas Traumáticos.xls"');
              header('Cache-Control: max-age=0');
              $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
              $objWriter->save('php://output');
            }
          }
        }
    }

    public function plan_individual($period, $user){
        $user = User::with('employee')->find($user);
        $period = NormPeriod::find($period);
        $closed_plan = DB::table('nom035_cierre_plan_individual')->where('period_id', $period->id)->where('user_id', $user->id)->select('fecha_cierre')->first();
        $avances = PlanIndividual::where('user_id', $user->id)->where('period_id', $period->id)->orderBy('id', 'DESC')->get();
        return view('nom035.reportes.plan_accion', compact('user', 'period', 'avances', 'closed_plan'));
    }

    public function plan_individual_store(Request $request, $period, $user){

        $evidencia = '';

        if (!empty($_FILES['file'])){

          $evidencia = $_FILES['file']['name'];
        }

        $user = User::with('employee')->find($user);
        $period = NormPeriod::find($period);
        
        try {
            $plan = PlanIndividual::create([
                'user_id' => $user->id,
                'period_id' => $period->id,
                'avances' => $request->avances,
                'avances_posterior' => $evidencia,
                'created_by' => Auth::user()->id
            ]);
        } catch (\Throwable $th) {
            dd($th->getMessage());
        }

        if (!empty($evidencia)){

          $avance = PlanIndividual::where('user_id', $user->id)->where('period_id', $period->id)->select('id')->orderBy('id', 'DESC')->first();
          $avance_id = $avance->id;

          if (!file_exists(getcwd() . '/documents/nom035/plan_individual/' . $avance_id)){

            mkdir(getcwd() . '/documents/nom035/plan_individual/' . $avance_id, 0755, true);
          }

          if (!file_exists(getcwd() . '/documents/nom035/plan_individual/' . $avance_id)){

            $avance = array();
            $avance['avances_posterior'] = '';
            DB::table('nom035_plan_individual')->where('id', $avance_id)->update($avance);
          }

          else{

            move_uploaded_file($_FILES['file']['tmp_name'],  getcwd() . '/documents/nom035/plan_individual/' . $avance_id . '/' . $evidencia);
          }
        }

        Session::put('period_id', $period->id);
        return redirect()->to('nom035/plan_individual');
    }

    public function plan_individual_edit($id){
        $plan = PlanIndividual::with('user')->find($id);
        return view('nom035.reportes.plan_accion_edit', compact('plan'));
    }

    public function plan_individual_update(Request $request, $id){
        $plan = PlanIndividual::find($id);

        if($plan){
            try {
                $plan->update([
                    'avances' => $request->avances,
                    'avances_posterior' => $request->avances_posterior,
                ]);
            } catch (\Throwable $th) {
                //throw $th;
            }
        }
        return redirect()->to('nom035/plan_individual');
    }

    public function plan_individual_close(){

        $close_plan_individual = array();
        $close_plan_individual['period_id'] = $_POST['period_id'];
        $close_plan_individual['user_id'] = $_POST['user_id'];
        $close_plan_individual['fecha_cierre'] = $_POST['fecha_cierre'];
        $close_plan_individual['created_by'] = Auth::user()->id;
        DB::table('nom035_cierre_plan_individual')->insert($close_plan_individual);
        Session::put('period_id', $_POST['period_id']);
        flash('Se guardo la fecha de cierre del plan');        
        return redirect()->to('nom035/plan_individual');
    }

    public function indexAccion(){

        $users = [];
        $planes = [];
        $categories = [];
        $periods = array();
        $period_id = 0;
        $answers = $permissions = array();

        if (Auth::user()->role == 'admin' || in_array(0, Auth::user()->getNom035Regions(5))){

          $periods = NormPeriod::whereHas('questions', function($q){
                      $q->where('cuestionario', 'Cuestionario 2')->orWhere('cuestionario', 'Cuestionario 3');
                     })
                     ->where('status', 'Abierto')->orWhere('status', 'Cerrado')->select('id', 'name')->orderBy('id', 'DESC')->get();
        }

        else{

          if (Auth::user()->hasNom035Permissions(5)){

            $permissions = Auth::user()->getNom035Regions(5);
            $periods_ids = DB::table('nom035_period_user')->join('users', 'users.id', '=', 'nom035_period_user.user_id')->join('employees', 'employees.id', '=', 'users.employee_id')->whereIn('region_id', $permissions)->groupBy('period_id')->pluck('period_id')->toArray();
            $periods = NormPeriod::whereHas('questions', function($q){
                        $q->where('cuestionario', 'Cuestionario 2')->orWhere('cuestionario', 'Cuestionario 3');
                       })
                       ->whereIn('id', $periods_ids)->where('status', 'Abierto')->orWhere('status', 'Cerrado')->select('id', 'name')->orderBy('id', 'DESC')->get();
          }
        }

        if (empty($periods) || $periods->isEmpty()){
          flash('No hay periodos disponibles por el momento...');
          return redirect('/nom035/que-es');
        }

        if (!empty($_POST['period_id'])){

          $period_id = $_POST['period_id'];
        }

        else{

          $period_id = $periods[0]->id;
        }

        $categories = Category::pluck('name', 'id')->toArray();

        foreach ($categories as $key => $category){
          
          $name = $category;
          $categories[$key] = new \stdClass();
          $categories[$key]->name = $name;
          $categories[$key]->total = 0;
          $categories[$key]->status = 'Nulo o despreciable';
          $categories[$key]->users = array();
        }

        $period = NormPeriod::find($period_id);
        $cuestionarios = $period->getTypeOfQuestions();

        if (count($cuestionarios) > 1 || $cuestionarios[0] != 'Cuestionario 1'){

          if (Auth::user()->role == 'admin' || in_array(0, auth()->user()->getNom035Regions(5))){

            $users = DB::table('users')->join('employees', 'employees.id', '=', 'users.employee_id')->pluck('users.id')->toArray();
          }

          else{

            if (auth()->user()->hasNom035Permissions(5)){

              $permissions = auth()->user()->getNom035Regions(5);
              $users = DB::table('users')->join('employees', 'employees.id', '=', 'users.employee_id')->whereIn('region_id', $permissions)->pluck('users.id')->toArray();
            }  
          }

          if (!empty($users)){

            $answers = Answer::with('question')
              ->where('period_id', $period->id)->whereIn('user_id', $users)
              ->whereHas('question', function($q){
                $q->where('cuestionario', 'Cuestionario 2')->orWhere('cuestionario', 'Cuestionario 3');
              })->get();

            foreach ($answers as $key2 => $answer){
                
              $categories[$answer->question->category->id]->total = $categories[$answer->question->category->id]->total + $this->getAnswerValue($answer->answer, $answer->question->positive);

              if (!in_array($answer->user_id, $categories[$answer->question->category->id]->users)){

                $categories[$answer->question->category->id]->users[] = $answer->user_id;
              }
            }

            if ($cuestionarios[0] == 'Cuestionario 2' || $cuestionarios[1] == 'Cuestionario 2'){

              unset($categories[5]);

              foreach ($categories as $key2 => $category){
                if (count($category->users) != 0){
                  $category->total = $category->total / count($category->users);
                }
                if ($category->name == 'Ambiente de trabajo'){
                  if ($category->total < 5 && $category->total >= 3){
                    $categories[$key2]->status = 'Bajo';
                  }
                  else{
                    if ($category->total < 7 && $category->total >= 5){
                      $categories[$key2]->status = 'Medio';
                    }
                    else{
                      if ($category->total < 9 && $category->total >= 7){
                        $categories[$key2]->status = 'Alto';
                      }
                      else{
                        if ($category->total >= 9){
                          $categories[$key2]->status = 'Muy alto';
                        }
                      }
                    }
                  }
                }
                else{
                  if ($category->name == 'Factores propios de la actividad'){
                    if ($category->total < 20 && $category->total >= 10){
                      $categories[$key2]->status = 'Bajo';
                    }
                    else{
                      if ($category->total < 30 && $category->total >= 20){
                        $categories[$key2]->status = 'Medio';
                      }
                      else{
                        if ($category->total < 40 && $category->total >= 30){
                          $categories[$key2]->status = 'Alto';
                        }
                        else{
                          if ($category->total >= 40){
                            $categories[$key2]->status = 'Muy alto';
                          }
                        }
                      }
                    }
                  }
                  else{
                    if ($category->name == 'Organización del tiempo de trabajo'){
                      if ($category->total < 6 && $category->total >= 4){
                        $categories[$key2]->status = 'Bajo';
                      }
                      else{
                        if ($category->total < 9 && $category->total >= 6){
                          $categories[$key2]->status = 'Medio';
                        }
                        else{
                          if ($category->total < 12 && $category->total >= 9){
                            $categories[$key2]->status = 'Alto';
                          }
                          else{
                            if ($category->total >= 12){
                              $categories[$key2]->status = 'Muy alto';
                            }
                          }
                        }
                      }
                    }
                    else{
                      if ($category->name == 'Liderazgo y relaciones en el trabajo'){
                        if ($category->total < 18 && $category->total >= 10){
                          $categories[$key2]->status = 'Bajo';
                        }
                        else{
                          if ($category->total < 28 && $category->total >= 18){
                            $categories[$key2]->status = 'Medio';
                          }
                          else{
                            if ($category->total < 38 && $category->total >= 28){
                              $categories[$key2]->status = 'Alto';
                            }
                            else{
                              if ($category->total >= 38){
                                $categories[$key2]->status = 'Muy alto';
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }  
              }
            }

            else{

              foreach ($categories as $key2 => $category){
                if (count($category->users) != 0){
                  $category->total = $category->total / count($category->users);
                }
                if ($category->name == 'Ambiente de trabajo'){
                  if ($category->total < 9 && $category->total >= 5){
                    $categories[$key2]->status = 'Bajo';
                  }
                  else{
                    if ($category->total < 11 && $category->total >= 9){
                      $categories[$key2]->status = 'Medio';
                    }
                    else{
                      if ($category->total < 14 && $category->total >= 11){
                        $categories[$key2]->status = 'Alto';
                      }
                      else{
                        if ($category->total >= 14){
                          $categories[$key2]->status = 'Muy alto';
                        }
                      }
                    }
                  }
                }
                else{
                  if ($category->name == 'Factores propios de la actividad'){
                    if ($category->total < 30 && $category->total >= 15){
                      $categories[$key2]->status = 'Bajo';
                    }
                    else{
                      if ($category->total < 45 && $category->total >= 30){
                        $categories[$key2]->status = 'Medio';
                      }
                      else{
                        if ($category->total < 60 && $category->total >= 45){
                          $categories[$key2]->status = 'Alto';
                        }
                        else{
                          if ($category->total >= 60){
                            $categories[$key2]->status = 'Muy alto';
                          }
                        }
                      }
                    }
                  }
                  else{
                    if ($category->name == 'Organización del tiempo de trabajo'){
                      if ($category->total < 7 && $category->total >= 5){
                        $categories[$key2]->status = 'Bajo';
                      }
                      else{
                        if ($category->total < 10 && $category->total >= 7){
                          $categories[$key2]->status = 'Medio';
                        }
                        else{
                          if ($category->total < 13 && $category->total >= 10){
                            $categories[$key2]->status = 'Alto';
                          }
                          else{
                            if ($category->total >= 13){
                              $categories[$key2]->status = 'Muy alto';
                            }
                          }
                        }
                      }
                    }
                    else{
                      if ($category->name == 'Liderazgo y relaciones en el trabajo'){
                        if ($category->total < 29 && $category->total >= 14){
                          $categories[$key2]->status = 'Bajo';
                        }
                        else{
                          if ($category->total < 42 && $category->total >= 29){
                            $categories[$key2]->status = 'Medio';
                          }
                          else{
                            if ($category->total < 58 && $category->total >= 42){
                              $categories[$key2]->status = 'Alto';
                            }
                            else{
                              if ($category->total >= 58){
                                $categories[$key2]->status = 'Muy alto';
                              }
                            }
                          }
                        }
                      }
                      else{
                        if ($category->total < 14 && $category->total >= 10){
                          $categories[$key2]->status = 'Bajo';
                        } 
                        else{
                          if ($category->total < 18 && $category->total >= 14){
                            $categories[$key2]->status = 'Medio';
                          }
                          else{
                            if ($category->total < 23 && $category->total >= 18){
                              $categories[$key2]->status = 'Alto';
                            }
                            else{
                              if ($category->total >= 23){
                                $categories[$key2]->status = 'Muy alto';
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                } 
              }
            }

            if (Auth::user()->role == 'admin' || in_array(0, $permissions)){

              $planes = PlanAccion::with('category')->where('period_id', $period_id)->get();
            }

            else{

              $planes = PlanAccion::with('category')->where('period_id', $period_id)->where('created_by', Auth::user()->id)->get();
            }
          }      
        }

        return view('nom035.reportes.plan_accion.index', compact('planes', 'categories', 'periods', 'period_id', 'canEdit'));
    }

    /*
    Fecha: 01-04-2020
    Modificado por: Mike Yáñez
    Motivo: Evitar división por cero y ajustar condiciones para mostrar el estatus de las categorías
    */
    /*
    Fecha: 31-03-2020
    Modificado por: Mike Yáñez
    Motivo: Mostrar correctamente las categorías
    */
    public function createAccion($period_id = 0){
        //$evaluation = Evaluation::active()
        /*->with(['periods' => function($q){
            $q->with('users.employee')
            ->open()
            ->whereHas('questions', function($q){
                $q->where('cuestionario', 'Cuestionario 2')->orWhere('cuestionario', 'Cuestionario 3');
            });
        }])*/
        $periods = NormPeriod::with('users.employee')
                  ->whereHas('questions', function($q){
                    $q->where('cuestionario', 'Cuestionario 2')->orWhere('cuestionario', 'Cuestionario 3');
                  })
        ->get();
        $users = [];
        $period = 0;
        $answers = array();
        $categories = Category::pluck('name', 'id')->toArray();
        $domains = Domain::all();
        $dimensions = Dimension::all();

        foreach ($categories as $key => $category) {
          
          $name = $category;
          $categories[$key] = new \stdClass();
          $categories[$key]->name = $name;
          $categories[$key]->total = 0;
          $categories[$key]->status = 'Nulo o despreciable';
          $categories[$key]->users = array();
        }

        if(count($periods) > 0){

          foreach ($periods as $key => $value){

            if ($value->id == $period_id){

              $period = $value;

              //$period = $evaluation->periods[0];
              /*$answers = Answer::with('question')
              ->where('period_id', $period->id)
              ->whereHas('question', function($q){
                $q->where('cuestionario', 'Cuestionario 2')->orWhere('cuestionario', 'Cuestionario 3');
              })->get()->groupBy('question.category.name');*/

            /*$categories = $answers->toArray();
            unset($categories['']);*/
              $cuestionarios = $period->getTypeOfQuestions();

              if (count($cuestionarios) > 1 || $cuestionarios[0] != 'Cuestionario 1'){

                if (Auth::user()->role == 'admin' || in_array(0, auth()->user()->getNom035Regions(5))){

                  $users = DB::table('users')->join('employees', 'employees.id', '=', 'users.employee_id')->pluck('users.id')->toArray();
                }

                else{

                  if (auth()->user()->hasNom035Permissions(5)){

                    $permissions = auth()->user()->getNom035Regions(5);
                    $users = DB::table('users')->join('employees', 'employees.id', '=', 'users.employee_id')->whereIn('region_id', $permissions)->pluck('users.id')->toArray();
                  }     
                }

                if (!empty($users)){

                  $answers = Answer::with('question')
                  ->where('period_id', $period->id)->whereIn('user_id', $users)
                  ->whereHas('question', function($q){
                    $q->where('cuestionario', 'Cuestionario 2')->orWhere('cuestionario', 'Cuestionario 3');
                  })->get();

                  foreach ($answers as $key2 => $answer){
                
                    $categories[$answer->question->category->id]->total = $categories[$answer->question->category->id]->total + $this->getAnswerValue($answer->answer, $answer->question->positive);

                    if (!in_array($answer->user_id, $categories[$answer->question->category->id]->users)){

                      $categories[$answer->question->category->id]->users[] = $answer->user_id;
                    }
                  }

                  if ($cuestionarios[0] == 'Cuestionario 2' || $cuestionarios[1] == 'Cuestionario 2'){

                    unset($categories[5]);

                    foreach ($categories as $key2 => $category){
                      if (count($category->users) != 0){
                        $category->total = $category->total / count($category->users);
                      }
                      if ($category->name == 'Ambiente de trabajo'){
                        if ($category->total < 5 && $category->total >= 3){
                          $categories[$key2]->status = 'Bajo';
                        }
                        else{
                          if ($category->total < 7 && $category->total >= 5){
                            $categories[$key2]->status = 'Medio';
                          }
                          else{
                            if ($category->total < 9 && $category->total >= 7){
                              $categories[$key2]->status = 'Alto';
                            }
                            else{
                              if ($category->total >= 9){
                                $categories[$key2]->status = 'Muy alto';
                              }
                            }
                          }
                        }
                      }
                      else{
                        if ($category->name == 'Factores propios de la actividad'){
                          if ($category->total < 20 && $category->total >= 10){
                            $categories[$key2]->status = 'Bajo';
                          }
                          else{
                            if ($category->total < 30 && $category->total >= 20){
                              $categories[$key2]->status = 'Medio';
                            }
                            else{
                              if ($category->total < 40 && $category->total >= 30){
                                $categories[$key2]->status = 'Alto';
                              }
                              else{
                                if ($category->total >= 40){
                                  $categories[$key2]->status = 'Muy alto';
                                }
                              }
                            }
                          }
                        }
                        else{
                          if ($category->name == 'Organización del tiempo de trabajo'){
                            if ($category->total < 6 && $category->total >= 4){
                              $categories[$key2]->status = 'Bajo';
                            }
                            else{
                              if ($category->total < 9 && $category->total >= 6){
                                $categories[$key2]->status = 'Medio';
                              }
                              else{
                                if ($category->total < 12 && $category->total >= 9){
                                  $categories[$key2]->status = 'Alto';
                                }
                                else{
                                  if ($category->total >= 12){
                                    $categories[$key2]->status = 'Muy alto';
                                  }
                                }
                              }
                            }
                          }
                          else{
                            if ($category->name == 'Liderazgo y relaciones en el trabajo'){
                              if ($category->total < 18 && $category->total >= 10){
                                $categories[$key2]->status = 'Bajo';
                              }
                              else{
                                if ($category->total < 28 && $category->total >= 18){
                                  $categories[$key2]->status = 'Medio';
                                }
                                else{
                                  if ($category->total < 38 && $category->total >= 28){
                                    $categories[$key2]->status = 'Alto';
                                  }
                                  else{
                                    if ($category->total >= 38){
                                      $categories[$key2]->status = 'Muy alto';
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }  
                    }
                  }

                  else{

                    foreach ($categories as $key2 => $category){
                      if (count($category->users) != 0){
                        $category->total = $category->total / count($category->users);
                      }
                      if ($category->name == 'Ambiente de trabajo'){
                        if ($category->total < 9 && $category->total >= 5){
                          $categories[$key2]->status = 'Bajo';
                        }
                        else{
                          if ($category->total < 11 && $category->total >= 9){
                            $categories[$key2]->status = 'Medio';
                          }
                          else{
                            if ($category->total < 14 && $category->total >= 11){
                              $categories[$key2]->status = 'Alto';
                            }
                            else{
                              if ($category->total >= 14){
                                $categories[$key2]->status = 'Muy alto';
                              }
                            }
                          }
                        }
                      }
                      else{
                        if ($category->name == 'Factores propios de la actividad'){
                          if ($category->total < 30 && $category->total >= 15){
                            $categories[$key2]->status = 'Bajo';
                          }
                          else{
                            if ($category->total < 45 && $category->total >= 30){
                              $categories[$key2]->status = 'Medio';
                            }
                            else{
                              if ($category->total < 60 && $category->total >= 45){
                                $categories[$key2]->status = 'Alto';
                              }
                              else{
                                if ($category->total >= 60){
                                  $categories[$key2]->status = 'Muy alto';
                                }
                              }
                            }
                          }
                        }
                        else{
                          if ($category->name == 'Organización del tiempo de trabajo'){
                            if ($category->total < 7 && $category->total >= 5){
                              $categories[$key2]->status = 'Bajo';
                            }
                            else{
                              if ($category->total < 10 && $category->total >= 7){
                                $categories[$key2]->status = 'Medio';
                              }
                              else{
                                if ($category->total < 13 && $category->total >= 10){
                                  $categories[$key2]->status = 'Alto';
                                }
                                else{
                                  if ($category->total >= 13){
                                    $categories[$key2]->status = 'Muy alto';
                                  }
                                }
                              }
                            }
                          }
                          else{
                            if ($category->name == 'Liderazgo y relaciones en el trabajo'){
                              if ($category->total < 29 && $category->total >= 14){
                                $categories[$key2]->status = 'Bajo';
                              }
                              else{
                                if ($category->total < 42 && $category->total >= 29){
                                  $categories[$key2]->status = 'Medio';
                                }
                                else{
                                  if ($category->total < 58 && $category->total >= 42){
                                    $categories[$key2]->status = 'Alto';
                                  }
                                  else{
                                    if ($category->total >= 58){
                                      $categories[$key2]->status = 'Muy alto';
                                    }
                                  }
                                }
                              }
                            }
                            else{
                              if ($category->total < 14 && $category->total >= 10){
                                $categories[$key2]->status = 'Bajo';
                              } 
                              else{
                                if ($category->total < 18 && $category->total >= 14){
                                  $categories[$key2]->status = 'Medio';
                                }
                                else{
                                  if ($category->total < 23 && $category->total >= 18){
                                    $categories[$key2]->status = 'Alto';
                                  }
                                  else{
                                    if ($category->total >= 23){
                                      $categories[$key2]->status = 'Muy alto';
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }  
                    }
                  }
                }
              /*foreach($categories as $key => $category){
                foreach($category as $answer){
                  if(!isset($categories[$key]['valor'])){
                    $categories[$key]['valor'] = 0;
                    $categories[$key]['cont'] = 0;
                  }
                  $categories[$key]['valor'] += $this->getAnswerValue($answer['answer'], $answer['question']['positive']);
                }
                if (count($cuestionarios) > 1){
                  $categories[$key]['cont'] = User::whereHas('norm_periods', function($q) use($period){
                    $q->where('id', $period->id)
                    ->where('nom035_period_user.status', 13)->orWhere('nom035_period_user.status', 23)->orWhere('nom035_period_user.status', 33);
                  })->get()->count();
                }
                else{
                  $categories[$key]['cont'] = User::whereHas('norm_periods', function($q) use($period){
                    $q->where('id', $period->id)
                    ->where('nom035_period_user.status', 3);
                  })->get()->count();
                }
              }
              $categories = $this->getCategoryValues($categories);
              foreach($categories as $key => $category){
                $categories[$key]['id'] = Category::where('name', $key)->first()->id;
              }*/
              }
            }
          }
        }

        $regions = DB::table('regions')->pluck('name', 'id')->toArray();
        return view('nom035.reportes.plan_accion.create', compact('categories', 'period', 'regions', 'domains', 'dimensions'));
    }

    public function storeAccion(Request $request){

        /*if (empty($request->user_id)){

          return redirect()->to('nom035/plan_accion/create');
        }

        $period = NormPeriod::find($request->period_id);
        $category = User::find($request->user_id);*/
        //$request->created_by = Auth::user()->id;
        
        $request = $request->all();
        $request['region_id'] = implode(',', $request['region_id']);
        $request['domains_ids'] = (!empty($request['domains_ids']) ? implode(',', $request['domains_ids']) : '');
        $request['dimensions_ids'] = (!empty($request['dimensions_ids']) ? implode(',', $request['dimensions_ids']) : '');
        $request['created_by'] = Auth::user()->id;
        unset($request['_token']);
        try {
            PlanAccion::create($request);
        } catch (\Throwable $th) {
            dd($th->getMessage());
        }

        if (!empty($request['avances'])){

          $request2 = array();
          $request2['avances'] = $request['avances'];
          $plan_accion = PlanAccion::where('period_id', $request['period_id'])->select('id')->orderBy('id', 'DESC')->first();
          $plan_id = $plan_accion->id;
          $request2['id_plan'] = $plan_id;
          $request2['created_by'] = Auth::user()->id;
          $request2['evidencia'] = '';

          if (!empty($_FILES['file'])){

            $request2['evidencia'] = $_FILES['file']['name'];
          }

          try {
            BitacoraPlanAccion::create($request2);
          } catch (\Throwable $th) {
            dd($th->getMessage());
          }

          $avance = BitacoraPlanAccion::where('id_plan', $plan_id)->where('created_by', Auth::user()->id)->select('id')->orderBy('id', 'DESC')->first();
          $avance_id = $avance->id;

          if (!empty($request2['evidencia'])){

            if (!file_exists(getcwd() . '/documents/nom035/plan_accion/' . $plan_id . '/' . $avance_id)){

              mkdir(getcwd() . '/documents/nom035/plan_accion/' . $plan_id . '/' . $avance_id, 0755, true);
            }

            move_uploaded_file($_FILES['file']['tmp_name'],  getcwd() . '/documents/nom035/plan_accion/' . $plan_id . '/' . $avance_id . '/' . $request2['evidencia']);
          }
        }

        Session::put('period_id', $request['period_id']);
        return redirect()->to('nom035/plan_accion');
    }

    public function editAccion($id){
        $evaluation = Evaluation::active()
        /*->with(['periods' => function($q){
            $q->with('users.employee')
            ->open()
            ->whereHas('questions', function($q){
                $q->where('cuestionario', 'Cuestionario 2')->orWhere('cuestionario', 'Cuestionario 3');
            });
        }])*/
        ->first();
        $users = [];
        $period = 0;
        $plan = $answers = $permissions = array();
        $categories = Category::pluck('name', 'id')->toArray();
        $domains = Domain::all();
        $dimensions = Dimension::all();

        foreach ($categories as $key => $category) {
          
          $name = $category;
          $categories[$key] = new \stdClass();
          $categories[$key]->name = $name;
          $categories[$key]->total = 0;
          $categories[$key]->status = 'Nulo o despreciable';
          $categories[$key]->users = array();
        }

        $periods = NormPeriod::where('status', 'Abierto')->orWhere('status', 'Cerrado')->get();
        //$categories = [];
        if(count($periods) > 0){

          $plan = PlanAccion::find($id);
          $plan->region_id = explode(',', $plan->region_id);
          $plan->domains_ids = explode(',', $plan->domains_ids);
          $plan->dimensions_ids = explode(',', $plan->dimensions_ids);

          foreach ($periods as $key => $value){

            if ($value->id == $plan->period_id){

              $period = $value;
              $cuestionarios = $period->getTypeOfQuestions();

              if (count($cuestionarios) > 1 || $cuestionarios[0] != 'Cuestionario 1'){

                if ($plan->region_id == 0){

                  $users = DB::table('users')->join('employees', 'employees.id', '=', 'users.employee_id')->pluck('users.id')->toArray();
                }

                else{

                  $users = DB::table('users')->join('employees', 'employees.id', '=', 'users.employee_id')->where('region_id', $plan->region_id)->pluck('users.id')->toArray();
                }

                if (!empty($users)){

                  $answers = Answer::with('question')
                  ->where('period_id', $period->id)->whereIn('user_id', $users)
                  ->whereHas('question', function($q){
                    $q->where('cuestionario', 'Cuestionario 2')->orWhere('cuestionario', 'Cuestionario 3');
                  })->get();

                  foreach ($answers as $key2 => $answer){
                
                    $categories[$answer->question->category->id]->total = $categories[$answer->question->category->id]->total + $this->getAnswerValue($answer->answer, $answer->question->positive);

                    if (!in_array($answer->user_id, $categories[$answer->question->category->id]->users)){

                      $categories[$answer->question->category->id]->users[] = $answer->user_id;
                    }
                  }

                  if ($cuestionarios[0] == 'Cuestionario 2' || $cuestionarios[1] == 'Cuestionario 2'){

                    unset($categories[5]);

                    foreach ($categories as $key2 => $category){
                      if (count($category->users) != 0){
                        $category->total = $category->total / count($category->users);
                      }
                      if ($category->name == 'Ambiente de trabajo'){
                        if ($category->total < 5 && $category->total >= 3){
                          $categories[$key2]->status = 'Bajo';
                        }
                        else{
                          if ($category->total < 7 && $category->total >= 5){
                            $categories[$key2]->status = 'Medio';
                          }
                          else{
                            if ($category->total < 9 && $category->total >= 7){
                              $categories[$key2]->status = 'Alto';
                            }
                            else{
                              if ($category->total >= 9){
                                $categories[$key2]->status = 'Muy alto';
                              }
                            }
                          }
                        }
                      }
                      else{
                        if ($category->name == 'Factores propios de la actividad'){
                          if ($category->total < 20 && $category->total >= 10){
                            $categories[$key2]->status = 'Bajo';
                          }
                          else{
                            if ($category->total < 30 && $category->total >= 20){
                              $categories[$key2]->status = 'Medio';
                            }
                            else{
                              if ($category->total < 40 && $category->total >= 30){
                                $categories[$key2]->status = 'Alto';
                              }
                              else{
                                if ($category->total >= 40){
                                  $categories[$key2]->status = 'Muy alto';
                                }
                              }
                            }
                          }
                        }
                        else{
                          if ($category->name == 'Organización del tiempo de trabajo'){
                            if ($category->total < 6 && $category->total >= 4){
                              $categories[$key2]->status = 'Bajo';
                            }
                            else{
                              if ($category->total < 9 && $category->total >= 6){
                                $categories[$key2]->status = 'Medio';
                              }
                              else{
                                if ($category->total < 12 && $category->total >= 9){
                                  $categories[$key2]->status = 'Alto';
                                }
                                else{
                                  if ($category->total >= 12){
                                    $categories[$key2]->status = 'Muy alto';
                                  }
                                }
                              }
                            }
                          }
                          else{
                            if ($category->name == 'Liderazgo y relaciones en el trabajo'){
                              if ($category->total < 18 && $category->total >= 10){
                                $categories[$key2]->status = 'Bajo';
                              } 
                              else{
                                if ($category->total < 28 && $category->total >= 18){
                                  $categories[$key2]->status = 'Medio';
                                }
                                else{
                                  if ($category->total < 38 && $category->total >= 28){
                                    $categories[$key2]->status = 'Alto';
                                  }
                                  else{
                                    if ($category->total >= 38){
                                      $categories[$key2]->status = 'Muy alto';
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }  
                    }
                  }

                  else{

                    foreach ($categories as $key2 => $category){
                      if (count($category->users) != 0){
                        $category->total = $category->total / count($category->users);
                      }
                      if ($category->name == 'Ambiente de trabajo'){
                        if ($category->total < 9 && $category->total >= 5){
                          $categories[$key2]->status = 'Bajo';
                        }
                        else{
                          if ($category->total < 11 && $category->total >= 9){
                            $categories[$key2]->status = 'Medio';
                          }
                          else{
                            if ($category->total < 14 && $category->total >= 11){
                              $categories[$key2]->status = 'Alto';
                            }
                            else{
                              if ($category->total >= 14){
                                $categories[$key2]->status = 'Muy alto';
                              }
                            }
                          }
                        }
                      }
                      else{
                        if ($category->name == 'Factores propios de la actividad'){
                          if ($category->total < 30 && $category->total >= 15){
                            $categories[$key2]->status = 'Bajo';
                          }
                          else{
                            if ($category->total < 45 && $category->total >= 30){
                              $categories[$key2]->status = 'Medio';
                            }
                            else{
                              if ($category->total < 60 && $category->total >= 45){
                                $categories[$key2]->status = 'Alto';
                              }
                              else{
                                if ($category->total >= 60){
                                  $categories[$key2]->status = 'Muy alto';
                                }
                              }
                            }
                          }
                        }
                        else{
                          if ($category->name == 'Organización del tiempo de trabajo'){
                            if ($category->total < 7 && $category->total >= 5){
                              $categories[$key2]->status = 'Bajo';
                            }
                            else{
                              if ($category->total < 10 && $category->total >= 7){
                                $categories[$key2]->status = 'Medio';
                              }
                              else{
                                if ($category->total < 13 && $category->total >= 10){
                                  $categories[$key2]->status = 'Alto';
                                }
                                else{
                                  if ($category->total >= 13){
                                    $categories[$key2]->status = 'Muy alto';
                                  }
                                }
                              }
                            }
                          }
                          else{
                            if ($category->name == 'Liderazgo y relaciones en el trabajo'){
                              if ($category->total < 29 && $category->total >= 14){
                                $categories[$key2]->status = 'Bajo';
                              }
                              else{
                                if ($category->total < 42 && $category->total >= 29){
                                  $categories[$key2]->status = 'Medio';
                                }
                                else{
                                  if ($category->total < 58 && $category->total >= 42){
                                    $categories[$key2]->status = 'Alto';
                                  } 
                                  else{
                                    if ($category->total >= 58){
                                      $categories[$key2]->status = 'Muy alto';
                                    } 
                                  }
                                }
                              }
                            }
                            else{
                              if ($category->total < 14 && $category->total >= 10){
                                $categories[$key2]->status = 'Bajo';
                              } 
                              else{
                                if ($category->total < 18 && $category->total >= 14){
                                  $categories[$key2]->status = 'Medio';
                                }
                                else{
                                  if ($category->total < 23 && $category->total >= 18){
                                    $categories[$key2]->status = 'Alto';
                                  }
                                  else{
                                    if ($category->total >= 23){
                                      $categories[$key2]->status = 'Muy alto';
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }  
                    }
                  }
                }
              }
            }
          }
        }

        $regions = DB::table('regions')->pluck('name', 'id')->toArray();
        return view('nom035.reportes.plan_accion.edit', compact('categories', 'period', 'plan', 'regions', 'domains', 'dimensions'));
    }

    public function updateAccion(Request $request, $id){
        $plan = PlanAccion::find($id);
        $request = $request->all();
        $request['region_id'] = implode(',', $request['region_id']);
        $request['domains_ids'] = (!empty($request['domains_ids']) ? implode(',', $request['domains_ids']) : '');
        $request['dimensions_ids'] = (!empty($request['dimensions_ids']) ? implode(',', $request['dimensions_ids']) : '');
        unset($request['_token']);
        try {
            $plan->update($request);
        } catch (\Throwable $th) {
            dd($th->getMessage());
        }

        if (!empty($request['avances'])){

          $request2 = array();
          $request2['avances'] = $request['avances'];
          $plan_id = $id;
          $request2['id_plan'] = $plan_id;
          $request2['created_by'] = Auth::user()->id;
          $request2['evidencia'] = '';

          if (!empty($_FILES['file'])){

            $request2['evidencia'] = $_FILES['file']['name'];
          }

          try {
            BitacoraPlanAccion::create($request2);
          } catch (\Throwable $th) {
            dd($th->getMessage());
          }

          $avance = BitacoraPlanAccion::where('id_plan', $plan_id)->where('created_by', Auth::user()->id)->select('id')->orderBy('id', 'DESC')->first();
          $avance_id = $avance->id;

          if (!empty($request2['evidencia'])){

            if (!file_exists(getcwd() . '/documents/nom035/plan_accion/' . $plan_id . '/' . $avance_id)){

              mkdir(getcwd() . '/documents/nom035/plan_accion/' . $plan_id . '/' . $avance_id, 0755, true);
            }

            move_uploaded_file($_FILES['file']['tmp_name'],  getcwd() . '/documents/nom035/plan_accion/' . $plan_id . '/' . $avance_id . '/' . $request2['evidencia']);
            
          }
        }

        Session::put('period_id', $request['period_id']);
        return redirect()->to('nom035/plan_accion');
    }

    public function needAttentionCuestionarioUno($user_id, $period_id){
        /*$factors = [];
        foreach($answers as $answer){
            if(!isset($factors[$answer->question->factor->name])){
                $factors[$answer->question->factor->name] = 0;
            }
            $factors[$answer->question->factor->name] += $answer->answer === "Si"?1:0;
        }

        $attentionNeeded = false;
        if(isset($factors["II.- Recuerdos persistentes sobre el acontecimiento (durante el último mes):"]) && $factors["II.- Recuerdos persistentes sobre el acontecimiento (durante el último mes):"] >= 1){
            return true;
        }
        if(isset($factors["III.- Esfuerzo por evitar circunstancias parecidas o asociadas al acontecimiento (durante el último mes):"]) && $factors["III.- Esfuerzo por evitar circunstancias parecidas o asociadas al acontecimiento (durante el último mes):"] >= 3){
            return true;
        }
        if(isset($factors["IV Afectación (durante el último mes):"]) && $factors["IV Afectación (durante el último mes):"] >= 2){
            return true;
        }
        return false;*/

        $global_result = false;
        $grouper = \DB::table('nom035_period_user_groupers')->where('period_id', $period_id)->where('user_id', $user_id)->where('grouper_id', 1)->first();

        if (!empty($grouper)){

          if ($grouper->answer == 'Si'){

            $resultados = Answer::where('user_id', $user_id)->where('period_id', $period_id)->get();
            $section_two = 0;
            $section_three = 0;
            $section_four = 0;

            foreach ($resultados as $key => $value){

              if ($value->question->cuestionario == 'Cuestionario 1'){

                if ($value->answer == 'Si'){

                  if ($value->question->factor_id == 2){

                    $section_two++;
                  }

                  else{

                    if ($value->question->factor_id == 3){

                      $section_three++;
                    }

                    else{

                      $section_four++;
                    } 
                  }
                }
              }
            }

            if ($section_two > 0 || $section_three > 2 || $section_four > 1){

              $global_result = true;
            }
          }
        }

        return $global_result;
    }

    /*
    Fecha: 31-03-2020
    Modificado por: Mike Yáñez
    Motivo: Ajustar las condiciones
    */
    public function getAnswerValue($answer, $positive){
        switch ($answer){
            case 'Siempre':
                return ($positive == 1)?0:4;
                break;
            case 'Casi siempre':
                return ($positive == 1)?1:3;
                break;

            case 'Algunas veces':
                return 2;
                break;

            case 'Casi nunca':
                return ($positive == 1)?3:1;
                break;
            case 'Nunca':
                return ($positive == 1)?4:0;
                break;
            default:
                return 0;
                break;
        }   
    }

    public function getCategoryValues($categories){
        $data = [];
        foreach($categories as $key => $value){
            $valor = ($value['valor'] / $value['cont']);
            if($key === "Ambiente de trabajo"){
                if($valor < 3){
                   $data[$key]['valor'] = "Nulo o despreciable";
                   $data[$key]['class'] = '#0098DA';
                }elseif(3 <= $valor && $valor < 5){
                    $data[$key]['valor'] = "Bajo";
                    $data[$key]['class'] = "#A8CF45";
                }elseif(5 <= $valor && $valor < 7){
                    $data[$key]['valor'] = "Medio";
                    $data[$key]['class'] = "#FFCC29";
                }elseif(7 <= $valor && $valor < 9){
                    $data[$key]['valor'] = "Alto";
                    $data[$key]['class'] = "#F58634";
                }elseif(9 <= $valor){
                    $data[$key]['valor'] = "Muy alto";
                    $data[$key]['class'] = "#Ed3237";
                }
            }elseif($key === "Factores propios de la actividad"){
                if($valor < 10){
                    $data[$key]['valor'] = "Nulo o despreciable";
                    $data[$key]['class'] = '#0098DA';
                }elseif(10 <= $valor && $valor < 20){
                    $data[$key]['valor'] = "Bajo";
                    $data[$key]['class'] = "#A8CF45";
                }elseif(20 <= $valor && $valor < 30){
                    $data[$key]['valor'] = "Medio";
                    $data[$key]['class'] = "#FFCC29";
                }elseif(30 <= $valor && $valor < 40){
                    $data[$key]['valor'] = "Alto";
                    $data[$key]['class'] = "#F58634";
                }elseif(40 <= $valor){
                    $data[$key]['valor'] = "Muy alto";
                    $data[$key]['class'] = "#Ed3237";
                }
            }elseif($key === "Organización del tiempo de trabajo"){
                if($valor < 4){
                    $data[$key]['valor'] = "Nulo o despreciable";
                    $data[$key]['class'] = '#0098DA';
                }elseif(4 <= $valor && $valor < 6){
                    $data[$key]['valor'] = "Bajo";
                    $data[$key]['class'] = "#A8CF45";
                }elseif(6 <= $valor && $valor < 9){
                    $data[$key]['valor'] = "Medio";
                    $data[$key]['class'] = "#FFCC29";
                }elseif(9 <= $valor && $valor < 12){
                    $data[$key]['valor'] = "Alto";
                    $data[$key]['class'] = "#F58634";
                }elseif(12 <= $valor){
                    $data[$key]['valor'] = "Muy alto";
                    $data[$key]['class'] = "#Ed3237";
                }
            }elseif($key === "Liderazgo y relaciones en el trabajo"){
                if($valor < 10){
                    $data[$key]['valor'] = "Nulo o despreciable";
                    $data[$key]['class'] = '#0098DA';
                }elseif(10 <= $valor && $valor < 18){
                    $data[$key]['valor'] = "Bajo";
                    $data[$key]['class'] = "#A8CF45";
                }elseif(18 <= $valor && $valor < 28){
                    $data[$key]['valor'] = "Medio";
                    $data[$key]['class'] = "#FFCC29";
                }elseif(28 <= $valor && $valor < 38){
                    $data[$key]['valor'] = "Alto";
                    $data[$key]['class'] = "#F58634";
                }elseif(38 <= $valor){
                    $data[$key]['valor'] = "Muy alto";
                    $data[$key]['class'] = "#Ed3237";
                }
            }
        }
        return $data;
    }
}