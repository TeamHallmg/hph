<?php
namespace App\Http\Controllers\Nom035\Admin;

use DB;
use App\User;
use App\Http\Requests;
use Laracasts\Flash\Flash;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Sucursal;
use App\Models\Nom035\Factor;
use App\Models\Nom035\NormPeriod;
use App\Models\Nom035\Question;
use App\Models\Nom035\GroupArea;
use App\Models\Nom035\Evaluation;
use App\Models\Nom035\GroupJobPosition;
use App\Employee;

class PeriodosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        /*$this->middleware('permission:see_periods')->only('index');
        $this->middleware('permission:create_periods')->only(['create', 'store']);
        $this->middleware('permission:edit_periods')->only(['edit', 'update']);
        $this->middleware('permission:destroy_periods')->only(['destroy']);*/
    }

    /**
     * Muestra los periodos
     *
     */
    public function index(){

      $periodos = array();

      if (auth()->user()->role == 'admin' || in_array(0, auth()->user()->getNom035Regions(6))){

        // Obtiene todos los periodos
        $periodos = NormPeriod::get();
      }

      /*else{

        if (auth()->user()->hasModulePermission('Norma 035')){

          $permissions = auth()->user()->multiUserPermissions()->orderBy('enterprise_id')->orderBy('sucursal_id')->get();

          if ($permissions[0]->enterprise_id == 0){

            // Obtiene todos los periodos
            $periodos = NormPeriod::get();
          }

          else{

            $enterprises_with_sucursals = $enterprises_without_sucursals = $sucursals = array();

            foreach ($permissions as $key => $value){
              
              if ($value->sucursal_id == 0){

                if (!in_array($value->enterprise_id, $enterprises_without_sucursals)){

                  $enterprises_without_sucursals[] = $value->enterprise_id;
                }
              }

              else{

                if (!in_array($value->enterprise_id, $enterprises_without_sucursals) && !in_array($value->sucursal_id, $sucursals)){

                  $enterprises_with_sucursals[] = $value->enterprise_id;
                  $sucursals[] = $value->sucursal_id;
                }
              }
            }

            if (!empty($enterprises_without_sucursals)){

              // Obtiene todos los periodos de cierta(s) empresa(s)
              $periodos = NormPeriod::whereIn('enterprise_id', $enterprises_without_sucursals)->get();
            }

            if (!empty($sucursals)){

              $temp = NormPeriod::whereIn('enterprise_id', $enterprises_with_sucursals)->whereIn('sucursal_id', $sucursals)->get();

              if (count($periodos) > 0){

                $periodos = $periodos->merge($temp);
              }

              else{

                $periodos = $temp;
              }
            }
          }
        }
      }*/

      return view('nom035.Admin.periodos.index', compact('periodos'));
    }

    /**
     * Crea un nuevo periodo
     *
     */
    public function create(){
        
        $evaluados = array();
        $enterprises = array();
        $sucursaless = array();
        $enterprises_with_sucursals = $enterprises_without_sucursals = $sucursals = array();
        $all_enterprises = false;

        if (auth()->user()->role == 'admin' || in_array(0, auth()->user()->getNom035Regions(6))){

          $evaluados = User::has('employee')->get();
          $enterprises = DB::table('enterprises')->orderBy('name')->get();
          $sucursaless = Sucursal::orderBy('name')->get();
          $all_enterprises = true;
        }

        /*else{

          $permissions = auth()->user()->multiUserPermissions()->orderBy('enterprise_id')->orderBy('sucursal_id')->get();

          if ($permissions[0]->enterprise_id == 0){

            $evaluados = User::has('employee')->get();
            $enterprises = DB::table('enterprises')->orderBy('name')->get();
            $sucursaless = Sucursal::orderBy('name')->get();
            $all_enterprises = true;
          }

          else{

            foreach ($permissions as $key => $value){
              
              if ($value->sucursal_id == 0){

                if (!in_array($value->enterprise_id, $enterprises_without_sucursals)){

                  $enterprises_without_sucursals[] = $value->enterprise_id;
                }
              }

              else{

                if (!in_array($value->enterprise_id, $enterprises_without_sucursals) && !in_array($value->sucursal_id, $sucursals)){

                  $enterprises_with_sucursals[] = $value->enterprise_id;
                  $sucursals[] = $value->sucursal_id;
                }
              }
            }

            if (!empty($enterprises_without_sucursals)){

              $evaluados = User::whereHas('employee', function($q) use($enterprises_without_sucursals){                    
                            $q->whereIn('enterprise_id', $enterprises_without_sucursals);
              })
              ->get();
            }

            if (!empty($sucursals)){

              $sucursales = DB::table('sucursals')->whereIn('id', $sucursals)->pluck('name')->toArray();
              $temp = User::whereHas('employee', function($q) use($enterprises_with_sucursals, $sucursales){                    
                            $q->whereIn('enterprise_id', $enterprises_with_sucursals);
                            $q->whereIn('sucursal', $sucursales);
              })
              ->get();

              if (count($evaluados) > 0){

                $evaluados = $evaluados->merge($temp);
              }

              else{

                $evaluados = $temp;
              }

              $sucursaless = Sucursal::whereIn('id', $sucursals)->orderBy('name')->get();
            }

            $temp = array_merge($enterprises_without_sucursals, $enterprises_with_sucursals);

            if (!empty($temp)){

              $enterprises = DB::table('enterprises')->whereIn('id', $temp)->orderBy('name')->get();
            }
          }
        }*/

        $evaluations = Evaluation::where('status', 'activo')->get();
        return view('nom035/Admin/periodos/create', compact('evaluados', 'evaluations', 'enterprises', 'sucursaless', 'all_enterprises'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $r_users = [];
        $r_users = explode(',', $request->users);
        $status = '';

        if (empty($request->enterprise_id)){

          $request->enterprise_id = '';
        }

        if (empty($request->sucursal_id)){

          $request->sucursal_id = '';
        }

        \DB::beginTransaction();
        //try {
            $result = NormPeriod::create([
                'name' => $request->name,
                'status' => $request->status,
                'evaluation_id' => 0,
                'created_by' => auth()->user()->id,
                'enterprise_id' => $request->enterprise_id,
                'sucursal_id' => $request->sucursal_id
            ]);
        //} catch (\Throwable $th) {
          //  \DB::rollback();
            //return redirect('/nom035/periodos')->with('error','El cuestionario de evaluación fue creado correctamente');
        //}

        $factores_questions = [];
        if (isset($request->factors) && !empty($request->factors)){
            $factores_questions = Question::whereHas('factor', function($q) use($request){
                $q->whereIn('id', $request->factors);
            })->pluck('id')->toArray();
        }

        $request->questions = $request->questions?explode(',', $request->questions):[];
        $questions = array();

        foreach ($request->questions as $key => $value){

          $question = Question::where('cuestionario', 'Cuestionario ' . $value)->pluck('id')->toArray();
          $questions = array_merge($questions, $question);
          $status .= '1';
        }

        $request->questions = $questions;
        $request->questions = array_merge($request->questions, $factores_questions);

        if (!empty($request->questions)){
            foreach ($request->questions as $key => $value){
          //      try {
                    $result->questions()->attach($value);
            //    } catch (\Throwable $th) {
              //      \DB::rollback();
                //    dd($result, $th->getMessage());
                  //  return redirect('/nom035/periodos')->with('error','El Cuestionario de evaluación fue creado correctamente');
                //}                
            }
        }

        $users = [];
        if (!empty($r_users)){
            foreach ($r_users as $key => $value){
                $users[$value] = ['status' => $status];
            }
        }

        if (isset($request->group_jobs) && !empty($request->group_jobs)){
            $jobs = $request->group_jobs;
            $userInJob = User::whereHas('employee.jobPosition.groupJobs', function($q) use($jobs){
                $q->whereIn('id', $jobs);
            })->pluck('id')->toArray();
            foreach($userInJob as $value){
                if(!isset($users[$value])){
                    $users[$value] = ['status' => 1];
                }
            }
           // try {
                $result->groupJobs()->attach($jobs);
            /*} catch (\Throwable $th) {
                \DB::rollback();
                dd($th->getMessage());
                return redirect('/nom035/periodos')->with('error','No se pudo agregar los grupos de puestos');
            }*/
        }

        if (isset($request->group_areas) && !empty($request->group_areas)){
            $areas = $request->group_areas;
            $userInArea = User::whereHas('employee.jobPosition.area.groupAreas', function($q) use($areas){
                $q->whereIn('id', $areas);
            })->pluck('id')->toArray();
            foreach($userInArea as $value){
                if(!isset($users[$value])){
                    $users[$value] = ['status' => 1];
                }
            }
            //try {
                $result->groupAreas()->attach($areas);
            /*} catch (\Throwable $th) {
                \DB::rollback();
                dd($th->getMessage());
                return redirect('/nom035/periodos')->with('error','No se pudo agregar los grupos de puestos');
            }*/
        }
        if(count($users) > 0){
            //try {
                $result->users()->attach($users);
            /*} catch (\Throwable $th) {
                \DB::rollback();
                dd($th->getMessage());
                return redirect('/nom035/periodos')->with('error','No se pudieron asignar los usuarios');
            }*/
        }
        \DB::commit();
        return redirect('/nom035/periodos')->with('success','El Cuestionario de evaluación fue creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Period $id)
    {
        //
    }

    /**
     * Edita o actualiza un periodo
     *
     */
    /*
    Fecha: 01-04-2020
    Modificado por: Mike Yáñez
    Motivo: Validar que solo se puede tener un periodo Abierto a la vez
    */
    public function edit($id){
        $canEdit = true;
        $periodo = NormPeriod::with('questions', 'users', 'groupJobs', 'groupAreas')->findOrFail($id);
        $tipo_cuestionario = $periodo->getTypeOfQuestions();
        $periodoAbierto = NormPeriod::where('status', 'Abierto')->where('id', '!=', $id)->first();
        $periodo_abierto = ($periodo->status === "Abierto")?true:false;
        $preguntas = Question::orderBy('id')->get()->groupBy('cuestionario');
        $evaluados = array();
        $enterprises = array();
        $sucursaless = array();
        $enterprises_with_sucursals = $enterprises_without_sucursals = $sucursals = array();
        $all_enterprises = false;
        //$evaluados = User::with('employee_wt')->has('employee_wt')->get();

        if (auth()->user()->role == 'admin' || in_array(0, auth()->user()->getNom035Regions(6))){

          $evaluados = User::with('employee_wt')->has('employee_wt')->get();
          $enterprises = DB::table('enterprises')->orderBy('name')->get();
          $sucursaless = Sucursal::orderBy('name')->get();
          $all_enterprises = true;
        }

        /*else{

          $permissions = auth()->user()->multiUserPermissions()->orderBy('enterprise_id')->orderBy('sucursal_id')->get();

          if ($permissions[0]->enterprise_id == 0){

            $evaluados = User::with('employee_wt')->has('employee_wt')->get();
            $enterprises = DB::table('enterprises')->orderBy('name')->get();
            $sucursaless = Sucursal::orderBy('name')->get();
            $all_enterprises = true;
          }

          else{

            foreach ($permissions as $key => $value){
              
              if ($value->sucursal_id == 0){

                if (!in_array($value->enterprise_id, $enterprises_without_sucursals)){

                  $enterprises_without_sucursals[] = $value->enterprise_id;
                }
              }

              else{

                if (!in_array($value->enterprise_id, $enterprises_without_sucursals) && !in_array($value->sucursal_id, $sucursals)){

                  $enterprises_with_sucursals[] = $value->enterprise_id;
                  $sucursals[] = $value->sucursal_id;
                }
              }
            }

            if (!empty($enterprises_without_sucursals)){

              $evaluados = User::whereHas('employee_wt', function($q) use($enterprises_without_sucursals){
                              $q->whereIn('enterprise_id', $enterprises_without_sucursals);
                           })
                           ->get();
            }

            if (!empty($sucursals)){

              $sucursales = DB::table('sucursals')->whereIn('id', $sucursals)->pluck('name')->toArray();

              $temp = User::whereHas('employee_wt', function($q) use($sucursales, $enterprises_with_sucursals){
                              $q->whereIn('enterprise_id', $enterprises_with_sucursals);
                              $q->whereIn('sucursal', $sucursales);
                           })
                           ->get();

              if (count($evaluados) > 0){

                $evaluados = $evaluados->merge($temp);
              }

              else{

                $evaluados = $temp;
              }

              $sucursaless = Sucursal::whereIn('id', $sucursals)->orderBy('name')->get();
            }

            $temp = array_merge($enterprises_without_sucursals, $enterprises_with_sucursals);

            if (!empty($temp)){

              $enterprises = DB::table('enterprises')->whereIn('id', $temp)->orderBy('name')->get();
            }
          }
        }*/

        $preguntas_periodo = $periodo->questions->pluck('id')->toArray();
        $evaluados_periodo = $periodo->users->pluck('id')->toArray();

        $groupJob = GroupJobPosition::all();
        $groupArea = GroupArea::all();

        return view('nom035/Admin/periodos/edit', compact('tipo_cuestionario', 'preguntas', 'evaluados', 'periodo_abierto', 'periodo', 'preguntas_periodo', 'evaluados_periodo', 'groupJob', 'groupArea', 'periodoAbierto', 'canEdit', 'enterprises', 'sucursaless', 'all_enterprises'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $periodo_a_editar = NormPeriod::findOrFail($id);
        $status = '';

        if (empty($request->enterprise_id)){

          $request->enterprise_id = '';
        }

        if (empty($request->sucursal_id)){

          $request->sucursal_id = '';
        }

        if ($periodo_a_editar->status == 'Preparatorio' && $request->status == 'Abierto'){

          DB::table('nom035_employees_mirror')->where('period_id', $id)->delete();
          $offset = 0;
          $employees = Employee::whereNull('deleted_at')->limit(500)->get()->toArray();

          if (!empty($employees)){

            do{

              DB::table('nom035_employees_mirror')->insert($employees);
              $offset += 500;
              $employees = Employee::whereNull('deleted_at')->offset($offset)->limit(500)->get()->toArray();
            }while(!empty($employees));

            $nom035_employee_mirror = array();
            $nom035_employee_mirror['period_id'] = $id;
            DB::table('nom035_employees_mirror')->where('period_id', 0)->update($nom035_employee_mirror);
          }
        }

        else{

          if ($request->status == 'Cancelado'){

            DB::table('nom035_employees_mirror')->where('period_id', $id)->delete();
          } 
        }

        \DB::beginTransaction();
        try {
            $periodo = NormPeriod::find($id);
            $periodo->update([
                'name' => $request->name,
                'status' => $request->status,
                'enterprise_id' => $request->enterprise_id,
                'sucursal_id' => $request->sucursal_id
            ]);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        /*$r_users = [];
        $r_users = explode(',', $request->users);

        $users = [];
        if (!empty($r_users)){
            foreach ($r_users as $key => $value){
                $users[$value] = ['status' => 1];
            }
        }*/

        $jobs = [];
        if (isset($request->group_jobs) && !empty($request->group_jobs)){
            $jobs = $request->group_jobs;
            $userInJob = User::whereHas('employee.jobPosition.groupJobs', function($q) use($jobs){
                $q->whereIn('id', $jobs);
            })->pluck('id')->toArray();
            foreach($userInJob as $value){
                if(!isset($users[$value])){
                    $users[$value] = ['status' => 1];
                }
            }
        }
        try {
            $periodo_a_editar->groupJobs()->sync($jobs);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
            return redirect('/nom035/periodos')->with('error','No se pudo agregar los grupos de puestos');
        }

        $areas = [];
        if (isset($request->group_areas) && !empty($request->group_areas)){
            $areas = $request->group_areas;
            $userInArea = User::whereHas('employee.jobPosition.area.groupAreas', function($q) use($areas){
                $q->whereIn('id', $areas);
            })->pluck('id')->toArray();
            foreach($userInArea as $value){
                if(!isset($users[$value])){
                    $users[$value] = ['status' => 1];
                }
            }
        }
        try {
            $periodo_a_editar->groupAreas()->sync($areas);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
            return redirect('/nom035/periodos')->with('error','No se pudo agregar los grupos de puestos');
        }
        /*if(count($users) > 0){
            try {
                $periodo_a_editar->users()->sync($users);
            } catch (\Throwable $th) {
                \DB::rollback();
                dd($th->getMessage());
                return redirect('/nom35/periodos')->with('error','No se pudo actualizar el listado de usuarios');
            }
        }*/
        if ($periodo_a_editar->status === "Preparatorio"){
          $request->questions = $request->questions?explode(',', $request->questions):[];
          $questions = array();

          foreach ($request->questions as $key => $value){

            $question = Question::where('cuestionario', 'Cuestionario ' . $value)->pluck('id')->toArray();
            $questions = array_merge($questions, $question);
            $status .= '1';
          }

          $request->questions = $questions;
          $r_users = [];
          $r_users = explode(',', $request->users);

          $users = [];
          if (!empty($r_users)){
            foreach ($r_users as $key => $value){
              $users[$value] = ['status' => $status];
            }
          }

          if(count($users) > 0){
            try {
                $periodo_a_editar->users()->sync($users);
            } catch (\Throwable $th) {
                \DB::rollback();
                dd($th->getMessage());
                return redirect('/nom035/periodos')->with('error','No se pudo actualizar el listado de usuarios');
            }
          }

          
            try {
                $periodo->questions()->sync($request->questions);
                $periodo->users()->sync($users);
            } catch (\Throwable $th) {
                \DB::rollback();
                dd($th->getMessage());
            }
        }
        \DB::commit();
        return redirect('/nom035/periodos')->with('success','El Cuestionario de evaluación fue actualizado correctamente');;
    }

   /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $period = NormPeriod::findOrFail($id);
        \DB::beginTransaction();
        try {
            $period->users()->detach();
            $period->questions()->detach();
            $period->answers()->delete();
            $period->delete();
            \DB::commit();
        } catch (\Throwable $th) {
            \DB::rollback();
        }
        return redirect('/nom035/periodos')->with('success','El Cuestionario de evaluación fue borrado correctamente');
    }

  /**
  * Change the password to the selected users
  */
  public function cambiar_contrasena(){
  
    $r_users = explode(',', $_POST['users']);
    $password = password_hash($_POST['contrasena'], PASSWORD_DEFAULT);
    $user = array();
    $user['password'] = $password;

    foreach ($r_users as $key => $value){
    
      DB::table('users')->where('id', $value)->update($user);
    }          
  }
}
