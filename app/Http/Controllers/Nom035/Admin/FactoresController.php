<?php

namespace App\Http\Controllers\Nom035\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use DB;

use App\Models\Nom035\Factor;

class FactoresController extends Controller
{
	/**
     * Create a instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:see_factors')->only('index');
        $this->middleware('permission:create_factors')->only(['create', 'store']);
        $this->middleware('permission:edit_factors')->only(['edit', 'update']);
        $this->middleware('permission:erase_factors')->only(['destroy']);
    }
    
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(){
		$factores = Factor::get();
		return view('nom035/Admin/factores/index', compact('factores'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(){

		return view('nom035/Admin/factores/create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request){
		try {
			Factor::create([
				'name' => $request->name,
				'description' => $request->description,
			]);
			// Flash::success('El factor fue guardado correctamente.');
		} catch (\Throwable $th) {
			dd($th->getMessage());
			// Flash::success('El factor fue guardado correctamente.');
		}
		return redirect('nom035/factores');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id){
		$factor = Factor::findOrFail($id);
		return view('nom035.Admin.factores.edit', compact('factor'));

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id){
		try {
			Factor::where('id', $id)->update([
				'name' => $request->name,
				'description' => $request->description,
			]);
			// Flash::success('El factor fue guardado correctamente.');
		} catch (\Throwable $th) {
			// Flash::success('El factor fue guardado correctamente.');
			//throw $th;
		}
		return redirect('nom035/factores');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
