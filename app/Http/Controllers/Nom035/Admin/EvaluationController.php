<?php

namespace App\Http\Controllers\Nom035\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\EvaluationRequest;
use App\Models\Nom035\Evaluation;

class EvaluationController extends Controller
{
    public function index()
    {
        $evaluations = Evaluation::get();
        return view('nom035.Admin.evaluations.index', compact('evaluations'));
    }

    public function create()
    {
        return view('nom035.Admin.evaluations.create');
    }

    public function store(EvaluationRequest $request)
    {
        $request = $request->validated();
        try {
            Evaluation::create($request);
        } catch (\Throwable $th) {
            dd($th->getMessage());
        }
        return redirect()->route('evaluations.index');
    }

    public function edit($id){
        $evaluation = Evaluation::find($id);
        return view('nom035.Admin.evaluations.edit', compact('evaluation'));
    }

    public function update(EvaluationRequest $request, $id){
        $request = $request->validated();
        try {
            $evaluation = Evaluation::find($id);
            $evaluation->update($request);
        } catch (\Throwable $th) {
            dd($th->getMessage());
        }
        return redirect()->route('evaluations.index');
    }
}
