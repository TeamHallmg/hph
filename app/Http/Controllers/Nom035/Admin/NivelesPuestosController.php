<?php

namespace App\Http\Controllers\Nom035\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Nom035\Admin\Puestos;
use App\Models\Nom035\Admin\PuestosNivelesPuestos;
use App\Models\Nom035\Admin\NivelesPuestos;

class NivelesPuestosController extends Controller{
    
  /**
  * Create a instance.
  *
  * @return void
  */
  public function __construct(){

    $this->middleware('auth');
  }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nivelesPuestos = NivelesPuestos::select('id', 'nombre', 'mando')->get();
        $puestos_NivelesPuestos = array();

      //$puestosNivelesPuestos = PuestosNivelesPuestos::PuestoNivelPuesto($gruposNiveles)->get();

      foreach($nivelesPuestos as $nPuestos){

        $puestos_NivelesPuestos[] = PuestosNivelesPuestos::where('id_nivel_puesto', $nPuestos->id)->get();
      }

      return view('nom035/Admin/niveles-puestos/index', compact('puestos_NivelesPuestos', 'nivelesPuestos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      $puestos = PuestosNivelesPuestos::where('id_nivel_puesto', '=', 1)->get();

      return view('nom035/Admin/niveles-puestos/add', compact('puestos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //dd($request);
      $nPuestos = NivelesPuestos::select('id')->where('nombre', 'LIKE', $request->nombreNivelPuesto)->exists();

      if($nPuestos){

        return view('nom035/Admin/niveles-puestos/index')->with('error','Este registro ya existe');
      }else{
        NivelesPuestos::create([
          'nombre' => $request->nombreNivelPuesto,
          'mando' => $request->mandoNivelPuesto
          ]);
      }

      $nPuestos = NivelesPuestos::select('id')->where('nombre', 'LIKE', $request->nombreNivelPuesto)->first();

      $checks = $request->id_puestos;
		
	  if (!empty($checks)){

      	foreach($checks as $check){
        	PuestosNivelesPuestos::where('id_puesto', '=', $check)->update([
          		'id_nivel_puesto' => $nPuestos->id,
          		'id_puesto' => $check
          	]);
      	}
	  }

      return redirect()->to('puestos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      
      $idNivelesPuestos = NivelesPuestos::where('id', '=', $id)->get();
	  $selected = array();
	  $selectedOtros = array();

      //dd($idNivelesPuestos);

      foreach($idNivelesPuestos as $idNP){
        $nombreNP = $idNP->nombre;
        $mando = $idNP->mando;
      }      
      
      $puestos = Puestos::all();

      $nivelesPuestos = PuestosNivelesPuestos::where('id_nivel_puesto', '=', $id)->get();

      $nivelesOtros = PuestosNivelesPuestos::where('id_nivel_puesto', '=', 1)->get();

      foreach($puestos as $puesto){ 
        foreach($nivelesPuestos as $nPuestos){
          if($puesto->id == $nPuestos->id_puesto){
            $selected[] = $nPuestos->id_puesto;
          } 
        }
      }

      foreach($puestos as $puesto){ 
        foreach($nivelesOtros as $no){
          if($puesto->id == $no->id_puesto){
            $selectedOtros[] = $no->id_puesto;
          } 
        }
      }

      //dd($selected);

      return view('nom035/Admin/niveles-puestos/edit', compact('puestos', 'nombreNP', 'selected', 'id', 'mando', 'nivelesPuestos', 'nivelesOtros', 'selectedOtros'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $checksUpdate = $request->id_puestos;
        $id = $request->id;
		$selected = array();
	  	$selectedOtros = array();

        if(is_null($checksUpdate) || empty($checksUpdate)){
          
          return redirect('/puestos/' . $id . '/edit')->with('error','Debes seleccionar al menos un puesto.');
        }

        $nombreNivelPuesto = NivelesPuestos::find($id);

        $nombreNivelPuesto->nombre = $request->nombreNivelPuesto;
        $nombreNivelPuesto->mando = $request->mandoNivelPuesto;

        $nombreNivelPuesto->save();

        $selDel = PuestosNivelesPuestos::where('id_nivel_puesto', '=', $id)->get();

        $selAdd = array();
        $delSel = array();
		
		if (!empty($checksUpdate)){

        	foreach($checksUpdate as $c){
          		$exist = PuestosNivelesPuestos::where('id_puesto', '=', $c)->where('id_nivel_puesto', '=', $id)->first();
          		if(!$exist){
            		$selAdd[] = $c;
          		}
        	}
		}

        for($i = 0; $i < count($selDel); $i++){
          if(!in_array($selDel[$i]['id_puesto'], $checksUpdate)){
            $delSel[] = $selDel[$i]['id_puesto'];
          }
        }

        if(!empty($selAdd)){

          foreach($selAdd as $ad){
            PuestosNivelesPuestos::where('id_puesto', '=', $ad)->update(['id_nivel_puesto' => $id]);
          }
        }

        if(!empty($delSel)){

          foreach($delSel as $del){
            PuestosNivelesPuestos::where('id_puesto', '=', $del)->update(['id_nivel_puesto' => 1]);
          }
        }

      return redirect('/puestos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*$nivelesPuestos = NivelesPuestos::where('id', '=', $id)->delete();
        $puestosNivelesPuestos = PuestosNivelesPuestos::where('id_nivel_puesto', '=', $id)->delete();

      return redirect()->to('puestos');*/
    }
}
