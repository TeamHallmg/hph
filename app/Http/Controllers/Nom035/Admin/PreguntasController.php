<?php

namespace App\Http\Controllers\Nom035\Admin;

use DB;

use App\Http\Requests;
use Laracasts\Flash\Flash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Nom035\Domain;
use App\Models\Nom035\Factor;
use App\Models\Nom035\Category;
use App\Models\Nom035\Question;
use App\Models\Nom035\Dimension;

class PreguntasController extends Controller
{
	/**
     * Create a instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
		$this->middleware('permission:see_questions')->only('index');
        $this->middleware('permission:create_questions')->only(['create', 'store']);
        $this->middleware('permission:edit_questions')->only(['edit', 'update']);
        $this->middleware('permission:erase_questions')->only(['destroy']);
    }
    
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(){
		$preguntas = Question::with('factor', 'dimension', 'domain', 'category')->get();
		// $preguntas = DB::table('preguntas')->leftJoin('factores', 'preguntas.id_factor', '=', 'factores.id')->select('preguntas.id', 'pregunta', 'nombre', 'positiva')->get();
		return view('nom035/Admin/preguntas/index', compact('preguntas'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(){
		$factores = Factor::get();
		$dimensions = Dimension::pluck('name', 'id')->toArray();
		$domains = Domain::pluck('name', 'id')->toArray();
		$categories = Category::pluck('name', 'id')->toArray();
		return view('nom035.Admin.preguntas.create', compact('factores', 'dimensions', 'domains', 'categories'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request){
		try {
			Question::create([
				'question' => $request->question,
				'factor_id' => (empty($request->factor_id))?null:$request->factor_id,
				'positive' => (empty($request->positiva))?0:$request->positive,
				'dimension_id' => isset($request->dimension_id)?$request->dimension_id:null,
				'domain_id' => isset($request->domain_id)?$request->domain_id:null,
				'category_id' => isset($request->category_id)?$request->category_id:null,
			]);
			// Flash::success('La pregunta fue guardada correctamente.');
		} catch (\Throwable $th) {
			// Flash::success('La pregunta fue guardada correctamente.');
			//throw $th;
		}
		return redirect('nom035/preguntas');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id){
		$pregunta = Question::findOrFail($id);
		$factores = Factor::get();
		$dimensions = Dimension::pluck('name', 'id')->toArray();
		$domains = Domain::pluck('name', 'id')->toArray();
		$categories = Category::pluck('name', 'id')->toArray();
		return view('nom035.Admin.preguntas.edit', compact('pregunta', 'factores', 'dimensions', 'domains', 'categories'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id){
		try {
			Question::where('id', $id)->update([
				'question' => $request->question,
				'factor_id' => (empty($request->factor_id))?null:$request->factor_id,
				'positive' => (empty($request->positive))?0:$request->positive,
				'dimension_id' => isset($request->dimension_id)?$request->dimension_id:null,
				'domain_id' => isset($request->domain_id)?$request->domain_id:null,
				'category_id' => isset($request->category_id)?$request->category_id:null,
			]);
			// Flash::success('La pregunta fue guardada correctamente.');
		} catch (\Throwable $th) {
			dd($th->getMessage());
			// Flash::success('La pregunta fue guardada correctamente.');
			//throw $th;
		}
		return redirect('nom035/preguntas');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
