<?php

namespace App\Http\Controllers\Nom035;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use App\User;
use App\Employee;
use App\Models\Nom035\Evaluation;
use App\Models\Nom035\Factor;
use App\Models\Nom035\NormPeriod;
use App\Models\Nom035\NormPeriodUser;
use App\Models\Nom035\Answer;
use App\Models\Nom035\PlanAccion;
use App\Models\Nom035\PlanIndividual;
use App\Models\Nom035\PoliticasTextos;

use App\Models\Announcement\Announcement;
use App\Models\Announcement\AnnouncementUser;
use App\Models\Announcement\View;

use App\Models\JobPosition;
use App\Models\Direction;
use App\Models\Department;
use App\Models\Area;
use App\Models\Sucursal;
use Session;

class EvaluadosController extends Controller
{
    /**
     * Create a new Evaluacion Desempeno controller instance.
     *
     * @return void2
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('permission:see_progress')->only('avances');
    }

    // Que es Clima Organizacional
    public function que_es_clima_organizacional(){
        return view('nom035/que-es-clima-organizacional');
    }

    // Que es Clima Organizacional
    public function politicas(){

        $sucursal_id = 0;
        $enterprise_id = 0;
          
        if (!empty(Auth::user()->employee)){
          if (!empty(Auth::user()->employee->enterprise_id)){
            $enterprise_id = Auth::user()->employee->enterprise_id;
          }
          if (!empty(Auth::user()->employee->sucursal)){
            $sucursal = Sucursal::where('name', Auth::user()->employee->sucursal)->where('enterprise_id', $enterprise_id)->first();
            if (!empty($sucursal)){
              $sucursal_id = $sucursal->id;
            }
          }
        }

        $politicas = AnnouncementUser::where('sucursal_id', $sucursal_id)->where('enterprise_id', $enterprise_id)->where('user_id', Auth()->user()->id)->where('type', 'policy-agreement')->first();
        
        $date = date('Y-m-d H:i:s');
        //$banners = DB::select("SELECT image,link,description FROM announcements INNER JOIN announcement_types ON (announcement_types.id = announcements.type) WHERE type = ? AND announcements.active = ? AND ends >= ? AND starts <= ?", [39, 1, $date, $date]);
        $banners = array();
        
        $politica = PoliticasTextos::where('enterprise_id', $enterprise_id)->where('sucursal_id', $sucursal_id)->first();
        
        if (empty($politica)){

          $politica = PoliticasTextos::where('enterprise_id', $enterprise_id)->where('sucursal_id', 0)->first();
          $politicas = AnnouncementUser::where('sucursal_id', 0)->where('enterprise_id', $enterprise_id)->where('user_id', Auth()->user()->id)->where('type', 'policy-agreement')->first();

          if (empty($politica)){

            $politica = PoliticasTextos::where('enterprise_id', 0)->where('sucursal_id', 0)->first();
            $politicas = AnnouncementUser::where('sucursal_id', 0)->where('enterprise_id', 0)->where('user_id', Auth()->user()->id)->where('type', 'policy-agreement')->first();
          }
        }

        $permissions = auth()->user()->getNom035Regions(1);
        return view('nom035.politicas', compact('banners', 'politica', 'politicas', 'permissions'));
    }

    public function politicas_index(){

        $politicas = array();

        if (auth()->user()->role == 'admin' || in_array(0, auth()->user()->getNom035Regions(1))){

          $politicas = PoliticasTextos::get();
        }

        /*else{

          $politicas = PoliticasTextos::where('region_id', $region)->first();
        }*/

        return view('nom035.politicas.index', compact('politicas'));
    }

    public function politicas_create($id=null){
        
        $enterprises = DB::table('enterprises')->orderBy('name')->get();
        $sucursals = Sucursal::orderBy('name')->get();

        $politica = null;

        if(!is_null($id)){

          $politica = PoliticasTextos::where('id', $id)->first();
        }
        
        return view('nom035.politicas.edit', compact('politica', 'enterprises', 'sucursals'));
    }

    public function politicas_store(Request $request){
        $data = $request->all();
        $id = 0;

        if (!empty($data['id'])){

          $id = $data['id'];
          unset($data['id']);
        }

        try {
          DB::beginTransaction();
            $politicas = PoliticasTextos::updateOrCreate(['id' => $id],$data);
          DB::commit();
        } catch (\Throwable $th) {
          //throw $th;
          DB::rollback();
          Flash::error('El registro no se agrego');
        }
        flash('El registro fue agregado');
        
        return redirect('/nom035/politicas-index');
    }

    public function indexVerification() {
        /**/$users = User::with(['verification','employee' => function($q) {
            $q->withTrashed();
        }])
        ->whereHas('employee', function($q){
            $q->withTrashed();
        })
        ->orderBy('id', 'DESC')->withTrashed()->get();

        if (auth()->user()->role == 'admin' || in_array(0, auth()->user()->getNom035Regions(7))){

          $users = User::with(['verification','employee' => function($q) {
            $q->withTrashed();
          }])
          ->whereHas('employee', function($q){
            $q->withTrashed();
          })
          ->withTrashed()->get();
        }

        else{

          $permissions = auth()->user()->getNom035Regions(7);
          $users = User::with(['verification','employee' => function($q) {
                $q->withTrashed();
              }])
              ->whereHas('employee', function($q) use ($permissions){
                $q->whereIn('region_id', $permissions);
                $q->withTrashed();
              })
              ->withTrashed()->get();
        }

        if($users->count() == 0) {
          flash('No hay usuarios registrados en la plataforma...');
          return back();
        }

        $users->validated = 0;
        $total_inactive = 0;

        foreach ($users as $key => $user) {

            if ($user->active == 0 || !empty($user->deleted_at)){

              $total_inactive++;
            }

            $direction = Direction::select('id','name')->where('id',$user->employee->direccion)->first();
            $department = Department::select('id','name')->where('id',$user->employee->departamento)->first();
            $job = JobPosition::select('id','name')->where('id',$user->employee->job_position_id)->first();
            $jefe = Employee::where('idempleado',$user->employee->jefe)->first();
            $last_period = NormPeriodUser::where('user_id', $user->id)->orderBy('period_id', 'DESC')->first();
            //dd($jefe->nombre);

            if(isset($user->employee->jefe) && $jefe != null) {
                $user->employee->jefe = $jefe->nombre.' '.$jefe->paterno.' '.$jefe->materno;
            } else {
                $user->employee->jefe = 'N/A';
            }

            if(isset($user->employee->jobPosition->area->department->name)) {
                $user->employee->departamento = $user->employee->jobPosition->area->department->name;
            } else {
                $user->employee->departamento = 'N/A';
            }


            if(isset($user->employee->jobPosition->area->name)) {
                $user->employee->area = $user->employee->jobPosition->area->name;
            } else {
                $user->employee->area = 'N/A';
            }

            if(isset($user->employee->jobPosition->area->department->direction->name)) {
                $user->employee->direccion = $user->employee->jobPosition->area->department->direction->name;
            } else {
                $user->employee->direccion = 'N/A';
            }

            if(isset($user->employee->jobPosition->name)) {
                $user->employee->job = $user->employee->jobPosition->name;
            } else {
                $user->employee->job = 'N/A';
            }

            if(isset($user->employee->enterprise->name)) {
                $user->employee->empresa = $user->employee->enterprise->name;
            } else {
                $user->employee->empresa = 'N/A';
            }

            if(!is_null($user->verification)) {
              $users->validated += 1;
            }

            $user->last_period = $last_period;
            
        }/**/

        $users->not_validated = $users->count()- $total_inactive - $users->validated;

        // dd($users);

        //return $users;

        //$users = User::with('employee','verification')->get();

        return view('nom035/usuarios-verificados',compact('users', 'total_inactive'));
    }

    /*
    Fecha: 01-04-2020
    Modificado por: Mike Yáñez
    Motivo: Error de dedo al declarar una variable con un nombre diferente al de la vista
    */
    public function informe(){

        $user = auth()->user();
        $factor = $planes = $avances = array();
        $user_periods = \DB::table('nom035_period_user')->where('user_id', $user->id)->pluck('period_id')->toArray();
        $periodoCerrado = NormPeriod::where('status', 'Cerrado')->whereIn('id', $user_periods)->orderBy('id', 'DESC')->first();
        $id_periodo_cerrado = 0;
        $global_result = false;
        $period_user = array();

        if (!empty($periodoCerrado)){

          $id_periodo_cerrado = $periodoCerrado->id;

          if (!empty($_POST['id_periodo'])){

            $id_periodo_cerrado = $_POST['id_periodo'];
          }

          $grouper = \DB::table('nom035_period_user_groupers')->where('period_id', $id_periodo_cerrado)->where('user_id', $user->id)->where('grouper_id', 1)->first();

          if (!empty($grouper)){

            if ($grouper->answer == 'Si'){

              $resultados = Answer::where('user_id', Auth::user()->id)->where('period_id', $id_periodo_cerrado)->get();
              $section_two = 0;
              $section_three = 0;
              $section_four = 0;

              foreach ($resultados as $key => $value){

                if ($value->question->cuestionario == 'Cuestionario 1'){

                  if ($value->answer == 'Si'){

                    if ($value->question->factor_id == 2){

                      $section_two++;
                    }

                    else{

                      if ($value->question->factor_id == 3){

                        $section_three++;
                      }

                      else{

                        $section_four++;
                      } 
                    }
                  }  
                }
              }

              if ($section_two > 0 || $section_three > 2 || $section_four > 1){

                $global_result = true;
              }
            }
          }

          $planes = PlanAccion::where('period_id', $id_periodo_cerrado)->get();
          $avances = PlanIndividual::where('user_id', $user->id)->where('period_id', $id_periodo_cerrado)->orderBy('id', 'DESC')->get();
        }
       
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url,1);
         $view = View::where('name', $url)->first();
        // $slick = Announcement::getAnnouncementsToDisplay($view->id, 'banner');
        $banner = Announcement::getAnnouncementsToDisplay($view->id, 'banner');
        $display_announcements = compact('banner');
        return view('nom035/informe', compact('user', 'global_result', 'factor', 'planes', 'avances', 'id_periodo_cerrado','display_announcements'));
    }

    // Listado de las personas que va a evaluar al que esta logueado
    public function avances(){

        $evaluados = array();
        $eval = Evaluation::where('status', 'activo')->first();
        $no_iniciados = $iniciados = $terminados = array();
        $canEdit = true;
        $periodoStatus = array();
        $enterprises_without_sucursals = $enterprises_with_sucursals = $sucursals = array();

        if (auth()->user()->role == 'admin'){

          // Obtiene todos los periodos
          $periodoStatus = NormPeriod::where('status', '!=', 'Cancelado')->select('id', 'name')->orderBy('id', 'DESC')->get();
        }

        else{

          $permissions = auth()->user()->multiUserPermissions()->orderBy('enterprise_id')->orderBy('sucursal_id')->get();

          if ($permissions[0]->enterprise_id == 0){

            // Obtiene todos los periodos
            $periodoStatus = NormPeriod::where('status', '!=', 'Cancelado')->select('id', 'name')->orderBy('id', 'DESC')->get();
          }

          else{

            foreach ($permissions as $key => $value){
              
              if ($value->sucursal_id == 0){

                if (!in_array($value->enterprise_id, $enterprises_without_sucursals)){

                  $enterprises_without_sucursals[] = $value->enterprise_id;
                }
              }

              else{

                if (!in_array($value->enterprise_id, $enterprises_without_sucursals) && !in_array($value->sucursal_id, $sucursals)){

                  $enterprises_with_sucursals[] = $value->enterprise_id;
                  $sucursals[] = $value->sucursal_id;
                }
              }
            }

            $periods = array();

            if (!empty($enterprises_without_sucursals)){

              $periods = DB::table('nom035_period_user')->join('users', 'users.id', '=', 'nom035_period_user.user_id')->join('employees', 'employees.id', '=', 'users.employee_id')->whereIn('enterprise_id', $enterprises_without_sucursals)->groupBy('period_id')->pluck('period_id')->toArray();
            }

            if (!empty($sucursals)){

              foreach ($enterprises_with_sucursals as $key => $value){
                
                if (!in_array($value, $enterprises_without_sucursals)){

                  $sucursal = Sucursal::find($sucursals[$key]);
                  $periods2 = DB::table('nom035_period_user')->join('users', 'users.id', '=', 'nom035_period_user.user_id')->join('employees', 'employees.id', '=', 'users.employee_id')->where('enterprise_id', $value)->where('sucursal', $sucursal->name)->groupBy('period_id')->pluck('period_id')->toArray();
                  $periods = array_merge($periods, $periods2);
                }
              }
            }

            if (!empty($periods)){

              // Obtiene todos los periodos
              $periodoStatus = NormPeriod::where('status', '!=', 'Cancelado')->whereIn('id', $periods)->select('id', 'name')->orderBy('id', 'DESC')->get();
            }

            else{

              $canEdit = false;
            }
          }
        }

        //$periodoStatus = NormPeriod::where('status', 'Abierto')->orWhere('status', 'Cerrado')->select('id', 'name')->orderBy('id', 'DESC')->get();
        $total_evals = 0;
        $period_id = 0;
        $period = array();

        if (empty($periodoStatus) || $periodoStatus->isEmpty()){
            flash('No hay periodos disponibles por el momento...');
            return redirect('/nom035/que-es')->with('error','No tienes permiso para ver la evaluación');
        }

        if (!empty($_POST['period_id']) || Session::has('period_id')){

          if (!empty($_POST['period_id'])){

            $period_id = $_POST['period_id'];
            Session::put('period_id', $period_id);
          }

          else{

            $period_id = Session::get('period_id');
          }
        }

        else{
        
          $period_id = $periodoStatus[0]->id;
        }

        $period = NormPeriod::find($period_id);
        $cuestionarios = $period->getTypeOfQuestions();

        if (auth()->user()->id == 1){

          $evaluados = $period
          ->users()
          ->withTrashed('employee_wt')
          ->get();
        }

        else{

          $evaluados = $period
          ->users()
          ->withTrashed(['employee_wt' => function($q) {
            $q->where('region_id', auth()->user()->employee->region_id);
          }])
          ->get();

          /*$permissions = auth()->user()->multiUserPermissions()->orderBy('enterprise_id')->orderBy('sucursal_id')->get();

          if ($permissions[0]->enterprise_id == 0){

            $evaluados = $period
            ->users()
            ->withTrashed('employee')
            ->get();
          }

          else{

            $enterprises_without_sucursals = $enterprises_with_sucursals = array();
            $sucursals = array();

            foreach ($permissions as $key => $value){
              
              if ($value->sucursal_id == 0){

                if (!in_array($value->enterprise_id, $enterprises_without_sucursals)){

                  $enterprises_without_sucursals[] = $value->enterprise_id;
                }
              }

              else{

                if (!in_array($value->enterprise_id, $enterprises_without_sucursals) && !in_array($value->sucursal_id, $sucursals)){

                  $enterprises_with_sucursals[] = $value->enterprise_id;
                  $sucursals[] = $value->sucursal_id;
                }
              }
            }

            if (!empty($enterprises_without_sucursals)){

              $evaluados = $period
              ->users()
              ->whereHas('employee', function($q) use($enterprises_without_sucursals){
                $q->whereIn('enterprise_id', $enterprises_without_sucursals);
                $q->withTrashed();
              })
              ->get();
            }

            if (!empty($sucursals)){

              foreach ($enterprises_with_sucursals as $key => $value){
             
                $sucursal = Sucursal::find($sucursals[$key]);

                $temp = $period
                    ->users()
                    ->whereHas('employee', function($q) use($value, $sucursal){
                      $q->where('enterprise_id', $value);
                      $q->where('sucursal', $sucursal->name);
                      $q->withTrashed();
                    })
                    ->get();

                if (count($evaluados) > 0){

                  $evaluados = $evaluados->merge($temp);
                }

                else{

                  $evaluados = $temp;
                }
              }
            }
          }*/
        }

        /*$evaluados = $period
        ->users()
        ->withTrashed(['employee' => function($q) {
            $q->orderBy('nombre');
        }])
        ->get();*/
        // $evaluados = \DB::table('periodo_evaluados')->join('personal', 'personal.id', '=', 'periodo_evaluados.id_evaluado')->join('users', 'users.employee_id', '=', 'personal.id')->where('id_periodo', $periodoAbierto->id)->select('idempleado', 'email', 'nombre', 'paterno', 'materno', 'rfc', 'personal.departamento', 'personal.puesto', 'id_evaluado', 'estado')->orderBy('nombre')->toSql();

        if (count($evaluados) > 0 && $canEdit){

          $ids_evaluados = array();

          foreach ($evaluados as $key => $value){
            
            $ids_evaluados[] = $value->id;
          }

          $no_aplica = array(7,8);
          $remove = DB::table('sociograma_period_user')->join('users', 'users.id', '=', 'sociograma_period_user.user_id')->join('employees', 'employees.id', '=', 'users.employee_id')->where('period_id', $period_id)->whereIn('region_id', $no_aplica)->pluck('user_id')->toArray();
        
          if (count($cuestionarios) == 1){

            if (auth()->user()->id == 1){
          
              $no_iniciados[] = $period->users()
              ->wherePivot('status', 1)->wherePivotIn('user_id', $ids_evaluados)
              ->whereNotIn('user_id', $remove)
              ->count();
              $iniciados[] = $period->users()
              ->wherePivot('status', 2)->wherePivotIn('user_id', $ids_evaluados)
              ->whereNotIn('user_id', $remove)
              ->count();
              $terminados[] = $period->users()
              ->wherePivot('status', 3)->wherePivotIn('user_id', $ids_evaluados)
              ->whereNotIn('user_id', $remove)
              ->count();
            }

            else{

              $no_iniciados[] = $period->users()->with(['employee' => function($q) {
                $q->where('region_id', auth()->user()->employee->region_id);
              }])
              ->wherePivot('status', 1)->wherePivotIn('user_id', $ids_evaluados)
              ->count();
              $iniciados[] = $period->users()->with(['employee' => function($q) {
                $q->where('region_id', auth()->user()->employee->region_id);
              }])
              ->wherePivot('status', 2)->wherePivotIn('user_id', $ids_evaluados)
              ->count();
              $terminados[] = $period->users()->with(['employee' => function($q) {
                $q->where('region_id', auth()->user()->employee->region_id);
              }])
              ->wherePivot('status', 3)->wherePivotIn('user_id', $ids_evaluados)
              ->count();
            }
            
            $total_evals = $no_iniciados[0] + $iniciados[0] + $terminados[0];
          }

          else{

            foreach ($cuestionarios as $key => $value){

              if ($key == 0){

                $uno = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 11)->whereIn('user_id', $ids_evaluados)->whereNotIn('user_id', $remove)->count();
                $dos = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 12)->whereIn('user_id', $ids_evaluados)->whereNotIn('user_id', $remove)->count();
                $tres = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 13)->whereIn('user_id', $ids_evaluados)->whereNotIn('user_id', $remove)->count();
                $no_iniciados[] = $uno + $dos + $tres;
                $uno = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 21)->whereIn('user_id', $ids_evaluados)->whereNotIn('user_id', $remove)->count();
                $dos = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 22)->whereIn('user_id', $ids_evaluados)->whereNotIn('user_id', $remove)->count();
                $tres = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 23)->whereIn('user_id', $ids_evaluados)->whereNotIn('user_id', $remove)->count();
                $iniciados[] = $uno + $dos + $tres;
                $uno = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 31)->whereIn('user_id', $ids_evaluados)->whereNotIn('user_id', $remove)->count();
                $dos = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 32)->whereIn('user_id', $ids_evaluados)->whereNotIn('user_id', $remove)->count();
                $tres = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 33)->whereIn('user_id', $ids_evaluados)->whereNotIn('user_id', $remove)->count();
                $terminados[] = $uno + $dos + $tres;
                $total_evals = $no_iniciados[0] + $iniciados[0] + $terminados[0];
              }

              else{

                $uno = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 11)->whereIn('user_id', $ids_evaluados)->whereNotIn('user_id', $remove)->count();
                $dos = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 21)->whereIn('user_id', $ids_evaluados)->whereNotIn('user_id', $remove)->count();
                $tres = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 31)->whereIn('user_id', $ids_evaluados)->whereNotIn('user_id', $remove)->count();
                $no_iniciados[$key] = $uno + $dos + $tres;
                $uno = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 12)->whereIn('user_id', $ids_evaluados)->whereNotIn('user_id', $remove)->count();
                $dos = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 22)->whereIn('user_id', $ids_evaluados)->whereNotIn('user_id', $remove)->count();
                $tres = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 32)->whereIn('user_id', $ids_evaluados)->whereNotIn('user_id', $remove)->count();
                $iniciados[$key] = $uno + $dos + $tres;
                $uno = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 13)->whereIn('user_id', $ids_evaluados)->whereNotIn('user_id', $remove)->count();
                $dos = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 23)->whereIn('user_id', $ids_evaluados)->whereNotIn('user_id', $remove)->count();
                $tres = \DB::table('nom035_period_user')->where('period_id', $period->id)->where('status', 33)->whereIn('user_id', $ids_evaluados)->whereNotIn('user_id', $remove)->count();
                $terminados[$key] = $uno + $dos + $tres;
              }  
            }
          }
        }

        return view('nom035/avances', compact('evaluados', 'period', 'no_iniciados', 'iniciados', 'terminados', 'cuestionarios', 'total_evals', 'periodoStatus', 'period_id', 'canEdit'));
    }
    
    public function evaluaciones(){
        $evaluados = array();
        $no_iniciados = $iniciados = $terminados = array();
        $id_periodo = 0;
        $periodosAbiertos = NormPeriod::where('status', 'Abierto')->get();
        if (count($periodosAbiertos) == 0){
            return redirect('/nom035/que-es')->with('error','No hay periodo abierto');
        }

        if (!empty($_POST['id_periodo']) || Session::has('id_periodo')){
          if (!empty($_POST['id_periodo'])){
            $id_periodo = $_POST['id_periodo'];
            Session::put('id_periodo', $id_periodo);
          }
          else{
            $id_periodo = Session::get('id_periodo');
          }
        }
        else{
          $id_periodo = $periodosAbiertos[0]->id;
        }

        $periodoAbierto = NormPeriod::find($id_periodo);
        $evaluados = $periodoAbierto->users()->where('user_id', Auth::user()->id)->get();
        
        // $evaluados = \DB::table('periodo_evaluados')->where('id_evaluado', Auth::user()->id)->where('id_periodo', $periodoAbierto->id)->get();
        /*if ($evaluados->isEmpty()){
            return redirect('/nom035/que-es')->with('error','No tienes permiso para ver la evaluación');
        }*/
        $politicas = array();

        if (!empty(Auth::user()->employee)){

          $sucursal_id = 0;
          $enterprise_id = 0;

          if (!empty(Auth::user()->employee->enterprise_id)){

            $enterprise_id = Auth::user()->employee->enterprise_id;
          }

          $sucursal = Sucursal::where('name', Auth::user()->employee->sucursal)->where('enterprise_id', $enterprise_id)->first();

          if (!empty($sucursal)){

            $sucursal_id = $sucursal->id;
          }

          $politicas = AnnouncementUser::where('user_id', Auth::user()->id)->where('enterprise_id', $enterprise_id)->where('sucursal_id', $sucursal_id)->first();

          if (empty($politicas)){

            if ($enterprise_id == 0){

              $politicas = AnnouncementUser::where('user_id', Auth::user()->id)->where('enterprise_id', 0)->first();
            }

            else{

              $politicas = AnnouncementUser::where('user_id', Auth::user()->id)->where('enterprise_id', $enterprise_id)->where('sucursal_id', 0)->first();

              if (empty($politicas)){

                $politicas = AnnouncementUser::where('user_id', Auth::user()->id)->where('enterprise_id', 0)->first();
              }
            }
          }
        }

        $evaluation = Evaluation::where('status', 'activo')->get();

        // $evaluados[0]->nombre = $user->nombre;
        // $evaluados[0]->paterno = $user->paterno;
        // $evaluados[0]->materno = $user->materno;

        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url,1);
         $view = View::where('name', $url)->first();
        // $slick = Announcement::getAnnouncementsToDisplay($view->id, 'banner');
        $banner = Announcement::getAnnouncementsToDisplay($view->id, 'banner');
        $display_announcements = compact('banner');
        return view('nom035/evaluaciones', compact('evaluation', 'evaluados', 'periodosAbiertos', 'no_iniciados', 'iniciados', 'terminados', 'politicas', 'id_periodo', 'periodoAbierto','display_announcements'));
    }

        // Formato de Evaluaci�n
    public function formatoEvaluacion($cuestionario = 1){        
        $evaluado = Auth::user();
        $periodo = NormPeriod::where('id', $_POST['period_id'])->first();
        /*->select('id')
        ->orderBy('id','DESC')
        ->firstOrFail();*/

        $factor1 = array();
        $groupers = array();
        $cuestionarios = $periodo->getTypeOfQuestions();
        $status = \DB::table('nom035_period_user')->where('period_id', $periodo->id)->where('user_id', $evaluado->id)->first();
        $user = Auth::user();

        if ($cuestionario == 'Cuestionario 1'){

          $factor1 = Factor::find(1);
        }

        $closed_questions = Factor::with([
            'questions' => function($q) use($periodo){
                $q->whereHas('periods', function($q1) use($periodo){
                    $q1->where('id', $periodo->id);
                });
            },
            'questions.answer' => function($q) use($periodo, $evaluado){
                $q->where('period_id', $periodo->id)
                ->where('user_id', $evaluado->id);
            }
        ])
        ->whereHas('questions.periods', function($q) use($periodo){
            $q->where('id', $periodo->id);
        })->get();

        $opened_questions = $periodo->questions()
        ->with([
            'answer' => function($q) use($periodo, $evaluado){
                $q->where('period_id', $periodo->id)
                ->where('user_id', $evaluado->id);
            }
        ])
        ->whereNull('factor_id')->get();

        // dd($opened_questions);

        // dd($closed_questions, $opened_questions);
        $groupers = \DB::table('nom035_period_user_groupers')->where('period_id', $periodo->id)->where('user_id', $evaluado->id)->select('grouper_id', 'answer')->get();
        return view('nom035/formato-evaluacion', compact('factor1', 'closed_questions', 'opened_questions', 'cuestionario', 'periodo', 'groupers', 'status', 'cuestionarios', 'user'));
    }

    /**
    * Guarda una respuesta del formato de evaluacion
    *
    * @return void
    */
    public function guardar_respuesta(){

      $user = Auth::user();

      // Id del evaluado
      $id_evaluado = $user->id;
            
      $periodo = NormPeriod::find($_POST['id_periodo']);

      if (!empty($periodo) && !empty($_POST['respuesta'])){

        $total_preguntas = \DB::table('nom035_period_question')->where('period_id', $periodo->id)->count();

        if (!empty($_POST['grouper'])){

          $period_user_groupers = \DB::table('nom035_period_user_groupers')->where('period_id', $periodo->id)->where('user_id', $id_evaluado)->pluck('grouper_id')->toArray();
          $create_period_user_grouper = array();
          $create_period_user_grouper['period_id'] = $periodo->id;
          $create_period_user_grouper['user_id'] = $id_evaluado;
          $update_period_user_grouper = array();

          if (!in_array($_POST['grouper'], $period_user_groupers)){
            $create_period_user_grouper['grouper_id'] = $_POST['grouper'];
            $create_period_user_grouper['answer'] = $_POST['respuesta'];
            \DB::table('nom035_period_user_groupers')->insert($create_period_user_grouper);
          }
          else{
            $update_period_user_grouper['answer'] = $_POST['respuesta'];
            \DB::table('nom035_period_user_groupers')->where('period_id', $periodo->id)->where('user_id', $id_evaluado)->where('grouper_id', $_POST['grouper'])->update($update_period_user_grouper);
          }
        }
        
        else{

          $respuesta = $_POST['respuesta'];
          $id_pregunta = $_POST['id_pregunta'];

          // Busca si la respuesta ya existe
          $resultado = \DB::table('nom035_answers')->where('user_id', $id_evaluado)->where('question_id', $id_pregunta)->where('period_id', $periodo->id)->first();
          $respuestas = array();
          $respuestas['answer'] = $respuesta;
            
          if (!empty($resultado)){

            $respuestas['updated_at'] = date('Y-m-d H:i:s');

            // Actualiza el resultado
            \DB::table('nom035_answers')->where('user_id', $id_evaluado)->where('period_id', $periodo->id)->where('question_id', $id_pregunta)->update($respuestas);
          }

          else{

            $respuestas['user_id'] = $id_evaluado;
            $respuestas['question_id'] = $id_pregunta;
            $respuestas['period_id'] = $periodo->id;
            $respuestas['updated_at'] = date('Y-m-d H:i:s');

            // Agrega el resultado
            \DB::table('nom035_answers')->insert($respuestas);
          }
        }

        $groupers = \DB::table('nom035_period_user_groupers')->where('period_id', $periodo->id)->where('user_id', $id_evaluado)->pluck('answer', 'grouper_id')->toArray();
        $preguntas1 = $periodo->questions()->where('cuestionario', 'Cuestionario 1')->count();
        $preguntas2 = $periodo->questions()->where('cuestionario', 'Cuestionario 2')->count();
        $preguntas3 = $periodo->questions()->where('cuestionario', 'Cuestionario 3')->count();
        $cuestionario1 = $cuestionario2 = $cuestionario3 = false;

        $resultados = Answer::where('user_id', $id_evaluado)
        ->where('period_id', $periodo->id)->get();

        $resultados1 = $resultados2 = $resultados3 = 0;

        foreach ($resultados as $key => $value){
          
          if ($value->question->cuestionario == 'Cuestionario 1'){

            $resultados1++;
          }

          else{

            if ($value->question->cuestionario == 'Cuestionario 2'){

              if (empty($value->question->group) || ($value->question->group->id != 2 && $value->question->group->id != 3) || ($value->question->group->id == 2 && $groupers[2] == 'Si') || ($value->question->group->id == 3 && $groupers[3] == 'Si')){

                $resultados2++;
              }
            }

            else{

              if (empty($value->question->group) || ($value->question->group->id != 4 && $value->question->group->id != 5) || ($value->question->group->id == 4 && $groupers[4] == 'Si') || ($value->question->group->id == 5 && $groupers[5] == 'Si')){

                $resultados3++;
              }
            }            
          }
        }

        $status = '';

        if ($preguntas1 > 0){

          if ($resultados1 == $preguntas1 || (isset($groupers[1]) && $groupers[1] == 'No')){

            $status = '3';
            $cuestionario1 = true;

            if (!empty($groupers[1]) && $groupers[1] == 'No'){

              $preguntas1 = $periodo->questions()->where('cuestionario', 'Cuestionario 1')->pluck('id')->toArray();
              Answer::where('user_id', Auth::user()->id)->where('period_id', $periodo->id)->whereIn('question_id', $preguntas1)->delete();
            }
          }

          else{

            if ($resultados1 == 0){

              $status = '1';
            }

            else{

              $status = '2';
            }
          }
        }

        if ($preguntas2 > 0){

          if (!empty($groupers[2]) && $groupers[2] == 'No'){

            $preguntas2 = $preguntas2 - 3;
          }

          if (!empty($groupers[3]) && $groupers[3] == 'No'){

            $preguntas2 = $preguntas2 - 3;
          }

          if ($resultados2 == $preguntas2){

            $status .= '3';
            $cuestionario2 = true;
          }

          else{

            if ($resultados2 == 0){

              $status .= '1';
            }

            else{

              $status .= '2';
            }
          }
        }

        if ($preguntas3 > 0){

          if (!empty($groupers[4]) && $groupers[4] == 'No'){

            $preguntas3 = $preguntas3 - 4;
          }

          if (!empty($groupers[5]) && $groupers[5] == 'No'){

            $preguntas3 = $preguntas3 - 4;
          }

          if ($resultados3 == $preguntas3){

            $status .= '3';
            $cuestionario3 = true;
          }

          else{

            if ($resultados3 == 0){

              $status .= '1';
            }

            else{

              $status .= '2';
            }
          }
        }

        $periodo_evaluado = array();
        $periodo_evaluado['status'] = $status;
        
        try {
            Auth::user()
            ->norm_periods()
            ->updateExistingPivot($periodo->id, $periodo_evaluado);
        } catch (\Throwable $th) {
            dd('Updating pivot',
                $periodo,
                $periodo_evaluado,
                $th->getMessage());
        }
      } 
    }

    /**
     * Guarda todas las respuestas abiertas
     *
     * @return void
     */
    public function guardar_respuestas(Request $request){
        $periodo = NormPeriod::find($request->period_id);
        $user = Auth::user();
        $cuestionario = $request->cuestionario;
        $groupers = $request->groupers;

        foreach($request->question as $key => $answer){
            if(!is_null($answer) && !empty($answer)){
                try {
                    \DB::beginTransaction();
                    Answer::updateOrCreate([
                        'user_id' => $user->id,
                        'question_id' => $key,
                        'period_id' => $periodo->id,
                    ],[
                        'answer' => $answer,
                    ]);
                    \DB::commit();
                } catch (\Throwable $th) {
                    \DB::rollback();
                    dd('Answers',
                        $user->id,
                        $key,
                        $periodo->id,
                        $th);
                }
            }
            else{
              Answer::where('user_id', $user->id)->where('question_id', $key)->where('period_id', $periodo->id)->delete();
            }
        }

        $period_user_groupers = \DB::table('nom035_period_user_groupers')->where('period_id', $periodo->id)->where('user_id', $user->id)->pluck('grouper_id')->toArray();
        $create_period_user_grouper = array();
        $create_period_user_grouper['period_id'] = $periodo->id;
        $create_period_user_grouper['user_id'] = $user->id;
        $update_period_user_grouper = array();

        foreach ($groupers as $key => $value){
          if (!in_array($key, $period_user_groupers)){
            $create_period_user_grouper['grouper_id'] = $key;
            $create_period_user_grouper['answer'] = $value;
            \DB::table('nom035_period_user_groupers')->insert($create_period_user_grouper);
          }
          else{
            $update_period_user_grouper['answer'] = $value;
            \DB::table('nom035_period_user_groupers')->where('period_id', $periodo->id)->where('user_id', $user->id)->where('grouper_id', $key)->update($update_period_user_grouper);
          }
        }

        $groupers = \DB::table('nom035_period_user_groupers')->where('period_id', $periodo->id)->where('user_id', $user->id)->pluck('answer', 'grouper_id')->toArray();
        $preguntas1 = $periodo->questions()->where('cuestionario', 'Cuestionario 1')->count();
        $preguntas2 = $periodo->questions()->where('cuestionario', 'Cuestionario 2')->count();
        $preguntas3 = $periodo->questions()->where('cuestionario', 'Cuestionario 3')->count();
        $cuestionario1 = $cuestionario2 = $cuestionario3 = false;

        //$resultados = Answer::where('user_id', Auth::user()->id)
        //->where('period_id', $periodo->id)->count();

        $resultados = Answer::where('user_id', Auth::user()->id)
        ->where('period_id', $periodo->id)->get();

        $resultados1 = $resultados2 = $resultados3 = 0;

        foreach ($resultados as $key => $value){
          
          if ($value->question->cuestionario == 'Cuestionario 1'){

            $resultados1++;
          }

          else{

            if ($value->question->cuestionario == 'Cuestionario 2'){

              if (empty($value->question->group) || ($value->question->group->id != 2 && $value->question->group->id != 3) || ($value->question->group->id == 2 && $groupers[2] == 'Si') || ($value->question->group->id == 3 && $groupers[3] == 'Si')){

                $resultados2++;
              }
            }

            else{

              if (empty($value->question->group) || ($value->question->group->id != 4 && $value->question->group->id != 5) || ($value->question->group->id == 4 && $groupers[4] == 'Si') || ($value->question->group->id == 5 && $groupers[5] == 'Si')){

                $resultados3++;
              }
            }            
          }
        }

        $status = '';

        if ($preguntas1 > 0){

          if ($resultados1 == $preguntas1 || (!empty($groupers[1]) && $groupers[1] == 'No')){

            $status = '3';
            $cuestionario1 = true;

            if (!empty($groupers[1]) && $groupers[1] == 'No'){

              $preguntas1 = $periodo->questions()->where('cuestionario', 'Cuestionario 1')->pluck('id')->toArray();
              Answer::where('user_id', Auth::user()->id)->where('period_id', $periodo->id)->whereIn('question_id', $preguntas1)->delete();
            }
          }

          else{

            if ($resultados1 == 0){

              $status = '1';
            }

            else{

              $status = '2';
            }
          }
        }

        if ($preguntas2 > 0){

          if (!empty($groupers[2]) && $groupers[2] == 'No'){

            $preguntas2 = $preguntas2 - 3;
          }

          if (!empty($groupers[3]) && $groupers[3] == 'No'){

            $preguntas2 = $preguntas2 - 3;
          }

          if ($resultados2 == $preguntas2){

            $status .= '3';
            $cuestionario2 = true;
          }

          else{

            if ($resultados2 == 0){

              $status .= '1';
            }

            else{

              $status .= '2';
            }
          }
        }

        if ($preguntas3 > 0){

          if (!empty($groupers[4]) && $groupers[4] == 'No'){

            $preguntas3 = $preguntas3 - 4;
          }

          if (!empty($groupers[5]) && $groupers[5] == 'No'){

            $preguntas3 = $preguntas3 - 4;
          }

          if ($resultados3 == $preguntas3){

            $status .= '3';
            $cuestionario3 = true;
          }

          else{

            if ($resultados3 == 0){

              $status .= '1';
            }

            else{

              $status .= '2';
            }
          }
        }

        $periodo_evaluado = array();
        $periodo_evaluado['status'] = $status;
        //if ($total_preguntas == $resultados){
        if ($status == '3' || $status == '33'){
          flash('Gracias por tu participación, has terminado, puedes cerrar la sesión');
        }
        else{
          if ($cuestionario == 'Cuestionario 1'){
            if ($cuestionario1){
              flash('Favor de pasar al siguiente cuestionario');
            }
            else{
              if ($status == '11' || $status == '12' && $status == '21' || $status == '22'){
                flash('Favor de contestar correctamente los cuestionarios');
              }
              else{
                flash('La encuesta no ha sido concluida, favor de terminarla');
              }
            }
          }
          else{
            if ($cuestionario == 'Cuestionario 2'){
              if ($cuestionario2){
                flash('Favor de pasar al siguiente cuestionario');
              }
              else{
                if ($status == '11' || $status == '12' && $status == '21' || $status == '22'){
                  flash('Favor de contestar correctamente los cuestionarios');
                }
                else{
                  flash('La encuesta no ha sido concluida, favor de terminarla');
                }
              }
            }
            else{
              if ($cuestionario3){
                flash('Favor de pasar al siguiente cuestionario');
              }
              else{
                if ($status == '11' || $status == '12' && $status == '21' || $status == '22'){
                  flash('Favor de contestar correctamente los cuestionarios');
                }
                else{
                  flash('La encuesta no ha sido concluida, favor de terminarla');
                }
              }
            } 
          }
        }
            //$periodo_evaluado['status'] = 3;
            //flash('Encuesta Finalizada');
        //}else{
            //$periodo_evaluado['status'] = 2;
            //flash('La encuesta no ha sido concluida, favor de terminarla');

        //}
        try {
            Auth::user()
            ->norm_periods()
            ->updateExistingPivot($periodo->id, $periodo_evaluado);
        } catch (\Throwable $th) {
            dd('Updating pivot',
                $periodo,
                $periodo_evaluado,
                $th->getMessage());
        }

        return redirect('/nom035/evaluaciones');
    }

    private function generar_resultados_grafica($filtro_a_usar, $id_periodo){

        $filtro_actual = '';
        $total = $contador_respuestas = 0;
        $filtros = array();
        $valores = array();
        $filtros_ordenados = array();
        $valores_ordenados = array();
        $respuestas = '';
        $nombre_campo = '';

        /*if ($filtro_a_usar == 'factores'){

            $respuestas = DB::table('respuestas')->join('preguntas', 'preguntas.id', '=', 'respuestas.id_pregunta')->join('factores', 'preguntas.id_factor', '=', 'factores.id')->join('personal', 'respuestas.id_evaluado', '=', 'personal.id')->where('id_factor', '!=', 0)->where('id_periodo', $id_periodo)->select('respuesta', 'positiva', 'departamento', 'puesto', 'id_factor', 'factores.nombre')->orderBy('id_factor')->get();
        }
        
        else{

            if ($filtro_a_usar == 'departamento'){

            $respuestas = DB::table('respuestas')->join('preguntas', 'preguntas.id', '=', 'respuestas.id_pregunta')->join('personal', 'respuestas.id_evaluado', '=', 'personal.id')->where('id_factor', '!=', 0)->where('id_periodo', $id_periodo)->select('respuesta', 'positiva', 'departamento AS filtro')->orderBy('departamento')->get();
            $nombre_campo = 'departamento';
            }

            else{

            if ($filtro_a_usar == 'puesto'){

                $respuestas = DB::table('respuestas')->join('preguntas', 'preguntas.id', '=', 'respuestas.id_pregunta')->join('personal', 'respuestas.id_evaluado', '=', 'personal.id')->where('id_factor', '!=', 0)->where('id_periodo', $id_periodo)->select('respuesta', 'positiva', 'puesto AS filtro')->orderBy('puesto')->get();
                $nombre_campo = 'puesto';
            }

            else{*/

                if ($filtro_a_usar == 'antiguedad'){
                    $respuestas = \DB::select("SELECT respuesta, positiva,
                    YEAR(CURDATE()) - YEAR(ingreso) - (DATE_FORMAT(CURDATE(), '%m%d') < DATE_FORMAT(ingreso, '%m%d')) AS filtro
                     FROM clima_respuestas INNER JOIN clima_preguntas ON (clima_preguntas.id = clima_respuestas.pregunta_id) INNER JOIN employee ON (respuestas.id_evaluado = personal.id) WHERE id_factor != 0 AND id_periodo = $id_periodo ORDER BY filtro");
                }

                /*else{

                if ($filtro_a_usar == 'boreales'){

                    $respuestas = DB::table('respuestas')->join('preguntas', 'preguntas.id', '=', 'respuestas.id_pregunta')->join('factores', 'preguntas.id_factor', '=', 'factores.id')->join('personal', 'respuestas.id_evaluado', '=', 'personal.id')->where('id_factor', '!=', 0)->where('id_periodo', $id_periodo)->where('personal.departamento', 'BOREALES')->select('respuesta', 'positiva', 'factores.nombre AS filtro')->orderBy('id_factor')->get();
                }

                else{

                    if ($filtro_a_usar == 'corretaje'){

                    $respuestas = DB::table('respuestas')->join('preguntas', 'preguntas.id', '=', 'respuestas.id_pregunta')->join('factores', 'preguntas.id_factor', '=', 'factores.id')->join('personal', 'respuestas.id_evaluado', '=', 'personal.id')->where('id_factor', '!=', 0)->where('id_periodo', $id_periodo)->where('personal.departamento', 'CORRETAJE ZONA NORTE')->select('respuesta', 'positiva', 'factores.nombre AS filtro')->orderBy('id_factor')->get();
                    }

                    else{

                    if ($filtro_a_usar == 'punto_sur'){

                        $respuestas = DB::table('respuestas')->join('preguntas', 'preguntas.id', '=', 'respuestas.id_pregunta')->join('factores', 'preguntas.id_factor', '=', 'factores.id')->join('personal', 'respuestas.id_evaluado', '=', 'personal.id')->where('id_factor', '!=', 0)->where('id_periodo', $id_periodo)->where('personal.departamento', 'PUNTO SUR')->select('respuesta', 'positiva', 'factores.nombre AS filtro')->orderBy('id_factor')->get();
                    }
                    }
                }
                }
            } 
            }
        }*/

        foreach ($respuestas as $key => $respuesta){

            if ($filtro_actual != $respuesta->filtro){

            if ($filtro_actual != ''){

                $filtro = $filtro_actual;
                $valor = number_format($total / $contador_respuestas, 2, '.', ',') * 1;
                $band = false;
                $temp_filtros = array();
                $temp_valores = array();

                foreach ($valores_ordenados as $key2 => $value){
                    
                if ($value < $valor && !$band){

                    $temp_valores[] = $valor;
                    $temp_filtros[] = $filtro;
                    $band = true;
                }

                $temp_valores[] = $value;
                $temp_filtros[] = $filtros_ordenados[$key2];
                }

                if (!$band){

                $temp_valores[] = $valor;
                $temp_filtros[] = $filtro;
                }

                $filtros_ordenados = $temp_filtros;
                $valores_ordenados = $temp_valores;
                $filtros[] = $filtro;
                $valores[] = $valor;
            }

            $total = 0;
            $contador_respuestas = 0;
            $filtro_actual = $respuesta->filtro;
            }

            switch ($respuesta->respuesta){

            case 'Totalmente de acuerdo':

                if ($respuesta->positiva == 1){
                
                $total += 5;
                }

                else{

                $total += 1;
                }

                break;

            case 'De acuerdo':

                if ($respuesta->positiva == 1){
                
                $total += 4;
                }

                else{

                $total += 2;
                }

                break;

            case 'Mas o menos':
                $total += 3;
                break;

            case 'Desacuerdo':

                if ($respuesta->positiva == 1){
                
                $total += 2;
                }

                else{

                $total += 4;
                }

                break;

            case 'Totalmente desacuerdo':
                
                if ($respuesta->positiva == 1){
                
                $total += 1;
                }

                else{

                $total += 5;
                }
            }

            $contador_respuestas++;
        }

        if ($filtro_actual != ''){

            $filtro = $filtro_actual;
            $valor = number_format($total / $contador_respuestas, 2, '.', ',') * 1;
            $band = false;
            $temp_filtros = array();
            $temp_valores = array();

            foreach ($valores_ordenados as $key => $value){
                    
            if ($value < $valor && !$band){

                $temp_valores[] = $valor;
                $temp_filtros[] = $filtro;
                $band = true;
            }

            $temp_valores[] = $value;
            $temp_filtros[] = $filtros_ordenados[$key];
            }

            if (!$band){

            $temp_valores[] = $valor;
            $temp_filtros[] = $filtro;
            }

            $filtros_ordenados = $temp_filtros;
            $valores_ordenados = $temp_valores;
            $filtros[] = $filtro;
            $valores[] = $valor;
        }

        $datos = new \stdClass();
        $datos->filtros_ordenados = $filtros_ordenados;
        $datos->valores_ordenados = $valores_ordenados;
        $datos->filtros = $filtros;
        $datos->valores = $valores;
        return $datos;
    }


    public function getAnswerValue($answer, $positive){
        switch ($answer){
            case 'Totalmente de acuerdo':
                return ($positive == 1)?5:1;
                break;
            case 'De acuerdo':
                return ($positive == 1)?4:2;
                break;

            case 'Mas o menos':
                return 3;
                break;

            case 'Desacuerdo':
                return ($positive == 1)?2:4;
                break;
            case 'Totalmente desacuerdo':
                return ($positive == 1)?1:5;
                break;
        }   
    }

    /**
     * Reporte Gráfico
     *
     * @return void
     */
    public function reporte_grafico(){
        $periodo = NormPeriod::where('status', 'Abierto')->orderBy('id', 'DESC')->first();
        if(is_null($periodo)){
            flash('No hay periodos abiertos por el momento...');
            return redirect('/nom035/que-es')->with('error','No tienes permiso para ver la evaluación');
        }
        $questions = $periodo->questions()->has('factor')->groupBy('factor_id')->get();
        $factorsUsed = $questions->pluck('factor_id')->toArray();
        $factors = Factor::with('questions.answers')->whereIn('id', $factorsUsed)->get();
        // dd($factors);
        foreach ($factors as $factor) {
            foreach ($factor->questions as $question) {
                $total = $question->answers->count();
                $promedio = 0;
                foreach ($question->answers as $answer) {
                    $promedio = $this->getAnswerValue($answer->answer, $question->positive);                    
                }
                if($total == 0){
                    $factor->averageValue = 0;
                }else{
                    $factor->averageValue = $promedio / $total;
                }
                
            }
        }

        $categories = $factors->pluck('name')->toArray();
        $factorData = [];
        foreach ($factors as $factor) {
            $factorData[] = [
                'name' => $factor->name,
                'data' => [$factor->averageValue],
            ];
        }

        // Factor::where

        // $resultados = $resultados_antiguedad = $puestos = $departamentos = $grupos_areas = $niveles_puestos = array();
        // $periodo = Period::where('status', 'Abierto')->orderBy('id', 'DESC')->firstOrFail();
        // // $periodo = DB::table('periodos')->where('estado', 'Abierto')->select('id')->first();
        // $id_periodo = $periodo->id;
        
        // $resultados = Answer::with('question', 'user.employee')
        // ->whereHas('question', function($q){
        //     $q->whereNotNull('factor_id');
        // })
        // ->where('period_id', $periodo->id)
        // ->get();
        
        // ->orderBy('id_pregunta')

        // $resultados = DB::table('respuestas')
        // ->join('preguntas', 'preguntas.id', '=', 'respuestas.id_pregunta')
        // ->join('factores', 'preguntas.id_factor', '=', 'factores.id')
        // ->join('personal', 'respuestas.id_evaluado', '=', 'personal.id')
        // ->leftJoin('puestos', 'puestos.puesto', '=', 'personal.puesto')
        // ->leftJoin('puesto_nivel_puesto', 'puesto_nivel_puesto.id_puesto', '=', 'puestos.id')
        // ->leftJoin('niveles_puestos', 'niveles_puestos.id', '=', 'puesto_nivel_puesto.id_nivel_puesto')
        // ->leftJoin('areas', 'areas.nombre', '=', 'personal.departamento')
        // ->leftJoin('areas_grupo_areas', 'areas_grupo_areas.id_areas', '=', 'areas.id')
        // ->leftJoin('grupo_areas', 'grupo_areas.id', '=', 'areas_grupo_areas.id_grupos_areas')
        // ->where('id_factor', '!=', 0)->where('id_periodo', $periodo->id)
        // ->select('respuesta', 'pregunta', 'positiva', 'departamento', 'personal.puesto', 'grupo_areas.id AS grupo_area', 'niveles_puestos.id AS nivel_puesto', 'id_factor', 'factores.nombre', 'id_pregunta')->orderBy('id_factor')->orderBy('id_pregunta')->get();
        
        // $resultados_antiguedad = $this->generar_resultados_grafica('antiguedad', $periodo->id);
        
        // $puestos = User::with('employee')->whereHas('periods', function($q) use($periodo){
        //     $q->where('id', $periodo->id);
        // })->get();
        
        // dd($puestos,$resultados);

        // $puestos = DB::table('personal')->join('periodo_evaluados', 'periodo_evaluados.id_evaluado', '=', 'personal.id')
        // ->leftJoin('puestos', 'puestos.puesto', '=', 'personal.puesto')
        // ->leftJoin('puesto_nivel_puesto', 'puesto_nivel_puesto.id_puesto', '=', 'puestos.id')
        // ->leftJoin('niveles_puestos', 'niveles_puestos.id', '=', 'puesto_nivel_puesto.id_nivel_puesto')
        // ->where('id_periodo', $periodo->id)
        // ->select(DB::raw('DISTINCT personal.puesto'), 'departamento', 'niveles_puestos.id')
        // ->orderBy('personal.puesto')
        // ->get();
        
        // Employee::
        // $departamentos = DB::table('personal')
        // ->join('periodo_evaluados', 'periodo_evaluados.id_evaluado', '=', 'personal.id')
        // ->leftJoin('areas', 'areas.nombre', '=', 'personal.departamento')
        // ->leftJoin('areas_grupo_areas', 'areas_grupo_areas.id_areas', '=', 'areas.id')
        // ->leftJoin('grupo_areas', 'grupo_areas.id', '=', 'areas_grupo_areas.id_grupos_areas')
        // ->where('id_periodo', $periodo->id)
        // ->select(DB::raw('DISTINCT departamento'), 'grupo_areas.id')
        // ->orderBy('departamento')->get();

        // $grupo_areas = GruposAreas::where('id', '!=', 1)
        // ->select('id', 'nombre')->orderBy('nombre')
        // ->get();

        // $niveles_puestos = NivelesPuestos::where('id', '!=', 1)
        // ->select('id', 'nombre')
        // ->orderBy('nombre')
        // ->get();

        /*$resultados['factores'] = $this->generar_resultados_grafica('factores', $periodo->id);
        $resultados['departamento'] = $this->generar_resultados_grafica('departamento', $periodo->id);
        $resultados['puesto'] = $this->generar_resultados_grafica('puesto', $periodo->id);
        $resultados['antiguedad'] = $this->generar_resultados_grafica('antiguedad', $periodo->id);
        $resultados['boreales'] = $this->generar_resultados_grafica('boreales', $periodo->id);
        $resultados['corretaje'] = $this->generar_resultados_grafica('corretaje', $periodo->id);
        $resultados['punto_sur'] = $this->generar_resultados_grafica('punto_sur', $periodo->id);*/        

        return view('nom035/reportes/reporte-grafico2', compact('factorData', 'categories'));
    }

  /**
  * Exporta Respuestas del Period Abierto
  */
  public function exportar_resultados($id_periodo, $grupo_area = '', $nivel_puesto = '', $departamento = '', $puesto = ''){
        require_once 'PHPExcel.php';
        require_once 'PHPExcel/IOFactory.php';

        $resultados = array();
        $title = '';

        if (empty($grupo_area) && empty($nivel_puesto) && empty($departamento) && empty($puesto)){

        $resultados = DB::table('respuestas')->join('preguntas', 'preguntas.id', '=', 'respuestas.id_pregunta')->join('personal', 'respuestas.id_evaluado', '=', 'personal.id')->where('id_factor', 0)->where('id_periodo', $id_periodo)->select('respuesta', 'pregunta', 'departamento')->orderBy('departamento')->orderBy('preguntas.id')->get();
        }

        else{

        if (!empty($departamento) && !empty($puesto)){

            $title = ' Departamento ' . $departamento . ' y Puesto ' . $puesto;
            $resultados = DB::table('respuestas')->join('preguntas', 'preguntas.id', '=', 'respuestas.id_pregunta')->join('personal', 'respuestas.id_evaluado', '=', 'personal.id')->where('id_factor', 0)->where('id_periodo', $id_periodo)->where('departamento', $departamento)->where('puesto', $puesto)->select('respuesta', 'pregunta', 'departamento')->orderBy('departamento')->orderBy('preguntas.id')->get();
        }

        else{

            if (!empty($departamento)){

            $title = ' Departamento ' . $departamento;
            $resultados = DB::table('respuestas')->join('preguntas', 'preguntas.id', '=', 'respuestas.id_pregunta')->join('personal', 'respuestas.id_evaluado', '=', 'personal.id')->where('id_factor', 0)->where('id_periodo', $id_periodo)->where('departamento', $departamento)->select('respuesta', 'pregunta', 'departamento')->orderBy('departamento')->orderBy('preguntas.id')->get();
            }

            else{

            if (!empty($puesto)){

                $title = ' Puesto ' . $puesto;
                $resultados = DB::table('respuestas')->join('preguntas', 'preguntas.id', '=', 'respuestas.id_pregunta')->join('personal', 'respuestas.id_evaluado', '=', 'personal.id')->where('id_factor', 0)->where('id_periodo', $id_periodo)->where('puesto', $puesto)->select('respuesta', 'pregunta', 'departamento')->orderBy('departamento')->orderBy('preguntas.id')->get();
            }

            else{

                if (!empty($grupo_area)){

                $grupo = GruposAreas::find($grupo_area);
                $title = ' Grupo de Área ' . $grupo->nombre;
                $resultados = DB::table('respuestas')->join('preguntas', 'preguntas.id', '=', 'respuestas.id_pregunta')->join('personal', 'respuestas.id_evaluado', '=', 'personal.id')->join('areas', 'areas.nombre', '=', 'personal.departamento')->join('areas_grupo_areas', 'areas_grupo_areas.id_areas', '=', 'areas.id')->join('grupo_areas', 'grupo_areas.id', '=', 'areas_grupo_areas.id_grupos_areas')->where('id_factor', 0)->where('id_periodo', $id_periodo)->where('grupo_areas.id', $grupo_area)->select('respuesta', 'pregunta', 'departamento')->orderBy('departamento')->orderBy('preguntas.id')->get();
                }

                else{

                if (!empty($nivel_puesto)){

                    $nivel = NivelesPuestos::find($nivel_puesto);
                    $title = ' Nivel de Puesto ' . $nivel->nombre;
                    $resultados = DB::table('respuestas')->join('preguntas', 'preguntas.id', '=', 'respuestas.id_pregunta')->join('personal', 'respuestas.id_evaluado', '=', 'personal.id')->join('puestos', 'puestos.puesto', '=', 'personal.puesto')->join('puesto_nivel_puesto', 'puesto_nivel_puesto.id_puesto', '=', 'puestos.id')->join('niveles_puestos', 'niveles_puestos.id', '=', 'puesto_nivel_puesto.id_nivel_puesto')->where('id_factor', 0)->where('id_periodo', $id_periodo)->where('niveles_puestos.id', $nivel_puesto)->select('respuesta', 'pregunta', 'departamento')->orderBy('departamento')->orderBy('preguntas.id')->get();
                } 
                }
            }
            } 
        }
        }

        // Create new PHPExcel object
        $objPHPExcel = new \PHPExcel();

        // Create a first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Departamento');  
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Factor');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Pregunta');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Answer');
        $i = 2;

        foreach ($resultados as $key => $value){
            
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $value->departamento);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, 'Pregunta Abierta');
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $value->pregunta);
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $value->respuesta);
        $i++;
        }

        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Respuestas Abiertas');

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Respuestas Encuesta' . $title . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
}