<?php

namespace App\Http\Controllers\Nom035;

use App\User;
use App\Models\Area;

use App\Models\Direction;
use App\Models\Department;
use App\Models\JobPosition;
use Illuminate\Http\Request;

use App\Models\JobPositionLevel;
use App\Http\Controllers\Controller;
use App\Models\Nom035\Domain;
use App\Models\Nom035\Factor;
use App\Models\Nom035\NormPeriod;
use App\Models\Nom035\Category;
use App\Models\Nom035\Dimension;
use App\Models\Nom035\GroupArea;
use App\Models\Nom035\GroupJobPosition;

class ReporteGraficoController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $periodo = NormPeriod::where('status', 'Abierto')->orderBy('id', 'DESC')->first();
        if(is_null($periodo)){
            flash('No hay periodos abiertos por el momento...');
            return redirect('/nom035/que-es')->with('error','No hay periodos Abiertos para ver la evaluación');
        }
        $directions = Direction::select('name')->groupBy('name')->get();
        $departments = Department::select('name')->groupBy('name')->get();
        $areas = Area::select('name')->groupBy('name')->get();
        $jobs = JobPosition::select('name')->groupBy('name')->get();
        $jobLevels = JobPositionLevel::orderBy('hierarchy', 'ASC')->get();
        $groupAreas = GroupArea::orderBy('name', 'ASC')->get();
        $groupJobs = GroupJobPosition::orderBy('name', 'ASC')->get();

        $questions = $periodo->questions()->has('factor')->groupBy('factor_id')->get();
        $factorsUsed = $questions->pluck('factor_id')->toArray();
        $factors = Factor::with(['questions' => function($q) use($periodo) {
            $q->with(['answers' => function($q) use($periodo) {
                $q->where('period_id', $periodo->id);
            }])->whereHas('periods', function($q) use($periodo) {
                $q->where('id', $periodo->id);   
            });
        }
        ])->whereIn('id', $factorsUsed)
        ->orderBy('name', 'ASC')
        ->get();

        foreach ($factors as $factor) {
            $promedio = 0;
            if($factor->questions->count() > 0){
                foreach ($factor->questions as $question) {
                    $total = $question->answers->count(); //Respuestas existentes a la pregunta
                    if($total > 0){
                        $valores = 0;
                        foreach ($question->answers as $answer) {
                            $valores += $this->getAnswerValue($answer->answer, $question->positive);
                        }
                        $promedio += $valores / $total;
                    }
                }
                $factor->averageValue = $promedio / $factor->questions->count();
            }
        }

        $categories = $factors->pluck('name')->toArray();
        $factorData = [];
        foreach ($factors as $factor) {
            $factorData[] = [
                'name' => $factor->name,
                'data' => [round($factor->averageValue, 2)],
            ];
        }
        return view('nom035.graficos.index', compact('factors', 'factorData', 'categories', 'directions', 'departments', 'areas', 'jobs', 'jobLevels', 'groupAreas', 'groupJobs'));
    }

    public function post(Request $request){
        $order = $request->order;
        $factor = empty($request->factor)?null:$request->factor;
        $direction = empty($request->direction)?null:$request->direction;
        $department = empty($request->department)?null:$request->department;
        $area = empty($request->area)?null:$request->area;
        $job = empty($request->job)?null:$request->job;
        $job_level = empty($request->job_level)?null:$request->job_level;
        $groupArea = empty($request->groupArea)?null:$request->groupArea;
        $groupJob = empty($request->groupJob)?null:$request->groupJob;

        $values = [
            'direction' => $direction,
            'deparment' => $department,
            'area' => $area,
            'job' => $job,
            'jobLevel' => $job_level,
            'groupArea' => $groupArea,
            'groupJob' => $groupJob,
        ];

        $subtitle = "";
        if(!is_null($direction)){
            $subtitle .= $direction;
        }

        if(!is_null($department)){
            if($subtitle !== ""){
                $subtitle .= " > "; 
            }
            $subtitle .= $department;
        }

        if(!is_null($area)){
            if($subtitle !== ""){
                $subtitle .= " > "; 
            }
            $subtitle .= $area;
        }

        if(!is_null($job)){
            if($subtitle !== ""){
                $subtitle .= " > "; 
            }
            $subtitle .= $job;
        }

        $periodo = NormPeriod::where('status', 'Abierto')->orderBy('id', 'DESC')->first();

        $users = null;
        if(!is_null($direction) || !is_null($department) || !is_null($area) || !is_null($job) || !is_null($job_level) || !is_null($groupArea) || !is_null($groupJob)){
            $users = User::whereHas('employee', function($q) use($values){
                $q->whereHas('jobPosition', function($q1) use($values){
                    $q1->when(!is_null($values['job']), function($q2) use($values){
                        $q2->where('name', $values['job']);
                    })
                    ->when(!is_null($values['jobLevel']), function($q2) use($values){
                        $q2->whereHas('level', function($q3) use($values){
                            $q3->where('id', $values['jobLevel']);
                        });
                    })
                    ->when(!is_null($values['groupJob']), function($q2) use($values){
                        $q2->whereHas('groupJobs', function($q3) use($values){
                            $q3->where('id', $values['groupJob']);
                        });
                    })
                    ->whereHas('area', function($q2) use($values){
                        $q2->when(!is_null($values['area']), function($q3) use($values){
                            $q3->where('name', $values['area']);
                        })
                        ->when(!is_null($values['groupArea']), function($q3) use($values){
                            $q3->whereHas('groupAreas', function($q4) use ($values){
                                $q4->where('name', $values['groupArea']);
                            });
                        })
                        ->whereHas('department', function($q3) use($values){
                            $q3->when(!is_null($values['deparment']), function($q4) use($values){
                                $q4->where('name', $values['deparment']);
                            })
                            ->whereHas('direction', function($q4) use($values){
                                $q4->when(!is_null($values['direction']), function($q4) use($values){
                                    $q4->where('name', $values['direction']);
                                });
                            });
                        });
                    });
                });
            })->pluck('id')->toArray();
        }

        $questions = $periodo->questions()->when(!is_null($factor), function($q) use($factor){
            $q->whereHas('factor', function($q1) use($factor){
                $q1->where('id', $factor);
            });
        }, function($q){
            $q->has('factor');
        })
        ->groupBy('factor_id')->get();
        $factorsUsed = $questions->pluck('factor_id')->toArray();
        $factors = Factor::with(['questions' => function($q) use($periodo) {
            $q->with(['answers' => function($q) use($periodo) {
                $q->where('period_id', $periodo->id);
            }])->whereHas('periods', function($q) use($periodo) {
                $q->where('id', $periodo->id);   
            });
        }
        ])->whereIn('id', $factorsUsed)
        ->orderBy('name', 'ASC')
        ->get();

        foreach ($factors as $factor) {
            $promedio = 0;
            if($factor->questions->count() > 0){
                foreach ($factor->questions as $question) {
                    $total = $question->answers()->when(!is_null($users), function($q) use($users){
                        $q->whereIn('user_id', $users);
                    })->count(); //Respuestas existentes a la pregunta
                    if($total > 0){
                        $valores = 0;
                        $answers = $question->answers()->when(count($users) > 0, function($q) use($users){
                            $q->whereIn('user_id', $users);
                        })->get();
                        foreach ($answers as $answer) {
                            $valores += $this->getAnswerValue($answer->answer, $question->positive);
                        }
                        $promedio += $valores / $total;
                    }
                }
                $factor->averageValue = $promedio / $factor->questions->count();
            }
        }

        if($order == "factor"){
            $categories = $factors->pluck('name')->toArray();
            $factorData = [];
            foreach ($factors as $factor) {
                $factorData[] = [
                    'name' => $factor->name,
                    'data' => [round($factor->averageValue, 2)],
                ];
            }
        }else{
            $categories = [];
            $factorData = [];
            foreach ($factors as $factor) {
                $factorData[strval($factor->averageValue)] = [
                    'name' => $factor->name,
                    'data' => [round($factor->averageValue, 2)],
                ];
            }
            ksort($factorData, SORT_NUMERIC);
            foreach($factorData as $data){
                $categories[] = $data['name'];
            }
            $factorData = array_values($factorData);
        }
        return response()->json(['data' => $factorData, 'categories' => $categories, 'subtitle' => $subtitle], 200);
    }

    public function reporte_tabla(){
        $periodo = NormPeriod::where('status', 'Abierto')->orderBy('id', 'DESC')->first();
        if(is_null($periodo)){
            flash('No hay periodos abiertos por el momento...');
            return redirect('/nom035/que-es')->with('error','No hay periodos Abiertos para ver la evaluación');
        }
        $directions = Direction::select('name')->groupBy('name')->get();
        $departments = Department::select('name')->groupBy('name')->get();
        $areas = Area::select('name')->groupBy('name')->get();
        $jobs = JobPosition::select('name')->groupBy('name')->get();
        $jobLevels = JobPositionLevel::orderBy('hierarchy', 'ASC')->get();
        $groupAreas = GroupArea::orderBy('name', 'ASC')->get();
        $groupJobs = GroupJobPosition::orderBy('name', 'ASC')->get();

        $questions = $periodo->questions()->has('factor')->groupBy('factor_id')->get();
        $factorsUsed = $questions->pluck('factor_id')->toArray();
        $factors = Factor::with(['questions' => function($q) use($periodo) {
            $q->with(['answers' => function($q) use($periodo) {
                $q->where('period_id', $periodo->id);
            }])->whereHas('periods', function($q) use($periodo) {
                $q->where('id', $periodo->id);   
            });
        }
        ])->whereIn('id', $factorsUsed)
        ->orderBy('name', 'ASC')
        ->get();

        $questions = $periodo->questions()->with(['answers' => function($q) use ($periodo){
            $q->where('period_id', $periodo->id)->has('user.employee');
        }])
        ->has('factor')->has('answers.user.employee')
        ->get();

        foreach($questions as $question){
            $cont = [
                1 => 0,
                2 => 0,
                3 => 0,
                4 => 0,
                5 => 0, 
            ];
            $values = [
                1 => 0,
                2 => 0,
                3 => 0,
                4 => 0,
                5 => 0, 
            ];
            $max = 0;
            $_value = 0;
            foreach($question->answers as $answer){
                $val = $this->getAnswerValue($answer->answer, $question->positive);
                $max += 5;
                $_value += $val;
                $cont[$val]++;
            }
            $question->max = $max;
            $question->t_value = $_value;
            $question->cont = $cont;
            $question->hombres = $question->answers()->whereHas('user.employee', function($q){
                $q->where('sexo', 'M');
            })->get()->groupBy('user_id')->count();
            $question->mujeres = $question->answers()->whereHas('user.employee', function($q){
                $q->where('sexo', 'F');
            })->get()->groupBy('user_id')->count();
            $question->otros = $question->answers()->whereHas('user.employee', function($q){
                $q->whereNotIn('sexo', ['F', 'M'])
                ->orWhereNull('sexo');
            })->get()->groupBy('user_id')->count();
            $question->total = $question->answers()->has('user.employee')->get()->groupBy('user_id')->count();
        }
        return view('nom035.graficos.reporte_tabla', compact('questions', 'directions', 'departments', 'areas', 'jobs', 'jobLevels', 'groupAreas', 'groupJobs', 'factors'));
    }

    public function post_tabla(Request $request){
        $periodo = NormPeriod::where('status', 'Abierto')->orderBy('id', 'DESC')->first();
        $factor = empty($request->factor)?null:$request->factor;
        $direction = empty($request->direction)?null:$request->direction;
        $department = empty($request->department)?null:$request->department;
        $area = empty($request->area)?null:$request->area;
        $job = empty($request->job)?null:$request->job;
        $job_level = empty($request->job_level)?null:$request->job_level;
        $groupArea = empty($request->groupArea)?null:$request->groupArea;
        $groupJob = empty($request->groupJob)?null:$request->groupJob;

        $values = [
            'direction' => $direction,
            'deparment' => $department,
            'area' => $area,
            'job' => $job,
            'jobLevel' => $job_level,
            'groupArea' => $groupArea,
            'groupJob' => $groupJob,
        ];


        $subtitle = "";
        if(!is_null($direction)){
            $subtitle .= $direction;
        }

        if(!is_null($department)){
            if($subtitle !== ""){
                $subtitle .= " > "; 
            }
            $subtitle .= $department;
        }

        if(!is_null($area)){
            if($subtitle !== ""){
                $subtitle .= " > "; 
            }
            $subtitle .= $area;
        }

        if(!is_null($job)){
            if($subtitle !== ""){
                $subtitle .= " > "; 
            }
            $subtitle .= $job;
        }

        $users = null;
        if(!is_null($direction) || !is_null($department) || !is_null($area) || !is_null($job) || !is_null($job_level) || !is_null($groupArea) || !is_null($groupJob)){
            $users = User::whereHas('employee', function($q) use($values){
                $q->whereHas('jobPosition', function($q1) use($values){
                    $q1->when(!is_null($values['job']), function($q2) use($values){
                        $q2->where('name', $values['job']);
                    })
                    ->when(!is_null($values['jobLevel']), function($q2) use($values){
                        $q2->whereHas('level', function($q3) use($values){
                            $q3->where('id', $values['jobLevel']);
                        });
                    })
                    ->when(!is_null($values['groupJob']), function($q2) use($values){
                        $q2->whereHas('groupJobs', function($q3) use($values){
                            $q3->where('id', $values['groupJob']);
                        });
                    })
                    ->whereHas('area', function($q2) use($values){
                        $q2->when(!is_null($values['area']), function($q3) use($values){
                            $q3->where('name', $values['area']);
                        })
                        ->when(!is_null($values['groupArea']), function($q3) use($values){
                            $q3->whereHas('groupAreas', function($q4) use ($values){
                                $q4->where('name', $values['groupArea']);
                            });
                        })
                        ->whereHas('department', function($q3) use($values){
                            $q3->when(!is_null($values['deparment']), function($q4) use($values){
                                $q4->where('name', $values['deparment']);
                            })
                            ->whereHas('direction', function($q4) use($values){
                                $q4->when(!is_null($values['direction']), function($q4) use($values){
                                    $q4->where('name', $values['direction']);
                                });
                            });
                        });
                    });
                });
            })->pluck('id')->toArray();
        }

        $questions = $periodo->questions()->with(['answers' => function($q) use ($periodo, $users){
            $q->where('period_id', $periodo->id)
            ->when(!is_null($users), function($q1) use($users){
                $q1->whereIn('user_id', $users);
            });
        }])
        ->whereHas('factor', function($q) use($factor){
            $q->when(!is_null($factor), function($q1) use($factor){
                $q1->where('id', $factor);
            });
        })
        ->has('answers.user.employee')
        ->get();
        $dataTable = [];
        $pos = 0;
        foreach($questions as $question){
            $cont = [
                1 => 0,
                2 => 0,
                3 => 0,
                4 => 0,
                5 => 0, 
            ];
            $values = [
                1 => 0,
                2 => 0,
                3 => 0,
                4 => 0,
                5 => 0, 
            ];
            $data = [];
            $data['pos'] = $pos++;
            $data['question'] = $question->question;
            foreach($question->answers as $answer){
                $val = $this->getAnswerValue($answer->answer, $question->positive);
                $cont[$val]++;
            }
            foreach($cont as $key => $val){
                $data['cont'.strval($key)] = $val;
            }
            $question->cont = $cont;
            $query = $question->answers()->whereHas('user', function($q) use($users){
                $q->when(!is_null($users), function($q1) use($users){
                    $q1->whereIn('user_id', $users);
                })
                ->has('employee');
            });
            $question->hombres = $query->whereHas('user.employee', function($q){
                $q->where('sexo', 'M');
            })->get()->groupBy('user_id')->count();
            $question->mujeres = $query->whereHas('user.employee', function($q){
                $q->where('sexo', 'F');
            })->get()->groupBy('user_id')->count();
            $question->otros = $query->whereHas('user.employee', function($q){
                $q->whereNotIn('sexo', ['F', 'M']);
            })->get()->groupBy('user_id')->count();
            $data['hombres'] = $question->hombres;
            $data['mujeres'] = $question->mujeres;
            $data['otros'] = $question->otros;
            $data['total'] = $question->answers->groupBy('user_id')->count();
            $dataTable[] = $data;
        }

        return response()->json($dataTable, 200);
    }

    public function getAnswerValue($answer, $positive){
        switch ($answer){
            case 'Totalmente de acuerdo':
                return ($positive == 1)?5:1;
                break;
            case 'De acuerdo':
                return ($positive == 1)?4:2;
                break;

            case 'Mas o menos':
                return 3;
                break;

            case 'Desacuerdo':
                return ($positive == 1)?2:4;
                break;
            case 'Totalmente desacuerdo':
                return ($positive == 1)?1:5;
                break;
            default:
                return 0;
                break;
        }   
    }

    public function reporte_nom(){
        $periodo = NormPeriod::where('status', 'Abierto')->orderBy('id', 'DESC')->first();
        if(is_null($periodo)){
            flash('No hay periodos abiertos por el momento...');
            return redirect('/nom035/que-es')->with('error','No hay periodos Abiertos para ver la evaluación');
        }
        $directions = Direction::select('name')->groupBy('name')->get();
        $departments = Department::select('name')->groupBy('name')->get();
        $areas = Area::select('name')->groupBy('name')->get();
        $jobs = JobPosition::select('name')->groupBy('name')->get();
        $jobLevels = JobPositionLevel::orderBy('hierarchy', 'ASC')->get();
        $groupAreas = GroupArea::orderBy('name', 'ASC')->get();
        $groupJobs = GroupJobPosition::orderBy('name', 'ASC')->get();
        
        $categories = Category::orderBy('name', 'ASC')->get();
        $domains = Domain::orderBy('name', 'ASC')->get();
        $dimensions = Dimension::orderBy('name', 'ASC')->get();

        $questions = $periodo->questions()->has('factor')->groupBy('factor_id')->get();
        $factorsUsed = $questions->pluck('factor_id')->toArray();
        $factors = Factor::with(['questions' => function($q) use($periodo) {
            $q->with(['answers' => function($q) use($periodo) {
                $q->where('period_id', $periodo->id);
            }])->whereHas('periods', function($q) use($periodo) {
                $q->where('id', $periodo->id);   
            });
        }
        ])->whereIn('id', $factorsUsed)
        ->orderBy('name', 'ASC')
        ->get();

        $cuestionario_final = 28;
        $cuestionario_categorias = [];
        $cuestionario_dominions = [];
        $cuestionario_dimensions = [];
        

        $name_categories = $categories->pluck('name')->toArray();
        foreach($categories as $category){
            $cuestionario_categorias[] = [
                'name' => $category->name,
                'data' => [random_int(0, 55)],
            ];
        }
        // dd($cuestionario_categorias, $name_categories);
        $name_domains = $domains->pluck('name')->toArray();
        foreach($domains as $domain){
            $cuestionario_dominions[] = [
                'name' => $domain->name,
                'data' => [random_int(0, 55)],
            ];
        }
        $name_dimensions = $dimensions->pluck('name')->toArray();
        foreach($dimensions as $dimension){
            $cuestionario_dimensions[] = [
                'name' => $dimension->name,
                'data' => [random_int(0, 55)],
            ];
        }

        return view('nom035.graficos.reporte_nom', compact('factors', 'questions','periodo','directions','departments','areas','jobs','jobLevels','groupAreas','groupJobs', 'name_categories', 'name_domains', 'name_dimensions','cuestionario_categorias','cuestionario_dominions','cuestionario_dimensions','categories', 'domains','dimensions'));
    }

    public function reporte_nom_graf(Request $request){
        $categories = Category::orderBy('name', 'ASC')->get();
        $domains = Domain::orderBy('name', 'ASC')->get();
        $dimensions = Dimension::orderBy('name', 'ASC')->get();

        $name_categories = $categories->pluck('name')->toArray();
        foreach($categories as $category){
            $cuestionario_categorias[] = [
                'name' => $category->name,
                'data' => [random_int(0, 55)],
            ];
        }
        // dd($cuestionario_categorias, $name_categories);
        $name_domains = $domains->pluck('name')->toArray();
        foreach($domains as $domain){
            $cuestionario_dominions[] = [
                'name' => $domain->name,
                'data' => [random_int(0, 55)],
            ];
        }
        $name_dimensions = $dimensions->pluck('name')->toArray();
        foreach($dimensions as $dimension){
            $cuestionario_dimensions[] = [
                'name' => $dimension->name,
                'data' => [random_int(0, 55)],
            ];
        }

        return response()->json(compact('name_categories', 'name_domains', 'name_dimensions', 'cuestionario_categorias', 'cuestionario_dominions', 'cuestionario_dimensions'));
    }
}
