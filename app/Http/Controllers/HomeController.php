<?php

namespace App\Http\Controllers;

use App\Employee;
use App\User;
use App\Models\Profile\Profile;
use App\Models\Announcement\Announcement;
use App\Models\Announcement\View;
use App\Models\JobPosition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);

        $view = View::where('name',$url)->first();
        $carrousel = Announcement::getAnnouncementsToDisplay($view->id, 'carrousel');
        $mosaico = Announcement::getAnnouncementsToDisplay($view->id, 'mosaico');
        // $display_announcements = compact('banner','cards','banner_panel','panels','carrousel','carrousel_secondary','mosaico','tabla_anuncios','cuatro_columnas','por_categorias');
        if (!empty(auth()->user()->external)){

          Session::put('primer_login', true);
          return redirect('/cambiar-contrasena/' . auth()->user()->id);
        }

        else{

          Session::forget('primer_login');
        }

        $view = View::where('name',$url)->first();
        $carrousel = Announcement::getAnnouncementsToDisplay($view->id, 'carrousel');
        $mosaico = Announcement::getAnnouncementsToDisplay($view->id, 'mosaico');
        // $display_announcements = compact('banner','cards','banner_panel','panels','carrousel','carrousel_secondary','mosaico','tabla_anuncios','cuatro_columnas','por_categorias');
        if (!empty(auth()->user()->external)){

          Session::put('primer_login', true);
          return redirect('/cambiar-contrasena/' . auth()->user()->id);
        }

        else{

          Session::forget('primer_login');
        }

        $display_announcements = compact('mosaico','carrousel');
        return view('home', compact('display_announcements'));
    }

    public function filosofia()
    {
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);

        $view = View::where('name',$url)->first();
        $panels = Announcement::getAnnouncementsToDisplay($view->id, 'panels');
        // $display_announcements = compact('banner','cards','banner_panel','panels','carrousel','carrousel_secondary','mosaico','tabla_anuncios','cuatro_columnas','por_categorias');
        $display_announcements = compact('panels');
        return view('filosofia', compact('display_announcements'));
    }

    public function recursividad(&$data, $users, &$list, $nivel){
        
        $nivelcolor=['#002C49','#0064A6','#ECC100','#4FB9FF','#FCF3CA'];

        $cont = [];
        $employees = [];
        foreach ($users as $u){
            $employees[$u->puesto] = (isset($employees[$u->puesto]))? $employees[$u->puesto] . $u->FullName . '<hr>': $u->FullName . '<hr>';
            $cont[] = $u->puesto;
        }
        $cont =  array_count_values($cont);
        $list .= '<ul>';
        foreach ($users as $user) {
            if(!isset($data[$user->puesto])){
                $data[$user->puesto] = [];
                $list .='<li id="open" class="encont"><a tabindex="0" class="btn btn-primary" style="background:'.$nivelcolor[$nivel].' !important;color:white; font-size:1rem; margin-bottom:10px;" data-toggle="popover" data-trigger="focus" title="<strong>Personal</strong>" data-content="'. $employees[$user->puesto] .'">' . $user->puesto . '  |  Plantilla: ' . $cont[$user->puesto] . '</a>';
            }
            if($user->employees->count() > 0){
                $this->recursividad($data[$user->puesto], $user->employees, $list, $nivel + 1);
            }
            if(!isset($data[$user->puesto])){
                $list .= '</li>';
            }
        }
        $list .= '</ul>';
    }

    public function organigramaPlantilla(){

        $totalPuestos = Employee::select('puesto')->groupBy('puesto')->get();
        $totalPuestos = $totalPuestos->count();

        $personas = [];
        $puestos = [];
        $areas = [];
        $totalPersonal = Employee::when(count($personas) > 0 , function($q){
            $q->whereNotIn('id', $personas);
        })
        ->when(count($puestos) > 0 , function($q){
            $q->whereNotIn('puestos', $puestos);
        })
        ->when(count($areas) > 0 , function($q){
            $q->whereNotIn('areas', $areas);
        })
        ->get();
        $totalPersonal = $totalPersonal->count();

        $topUsers = Employee::where('jefe', '')->orWhere('jefe', '0')->orWhere('jefe', null)->orderBy('nombre','DESC')
        ->with(['employees' => function($q){
            $q->orderBy('division')->orderBy('nombre','ASC');
        }])
        ->get();        

        $data = [];

        $list = '<ul class="treeview">';

        $cont = [];
        $employees = [];
        foreach ($topUsers as $u){
            $employees[$u->puesto] = (isset($employees[$u->puesto]))? $employees[$u->puesto] . $u->FullName . '<hr>': $u->FullName . '<hr>';
            $cont[] = $u->puesto;
        }
        $cont =  array_count_values($cont);
        foreach ($topUsers as $user) {
            if(!isset($data[$user->puesto])){// 
                $list .='<li id="open" class="encont"><a tabindex="1" class="btn btn-primary" style="background:#007AAB !important;color:white; font-size:1rem; margin-bottom:10px;" data-toggle="popover" data-trigger="focus" title="<strong>Personal</strong>" data-content="'. $employees[$user->puesto] .'">' . $user->puesto . '  |  Plantilla: ' . $cont[$user->puesto] . '</a>';
                $data[$user->puesto] = [];
            }            
            
            if($user->employees->count() > 0){
                $this->recursividad($data[$user->puesto], $user->employees, $list, 0);
            }
            if(!isset($data[$user->puesto])){
                $list .= '</li>';
            }
        }
        $list .= '</ul>';
        return view('/organigrama-plantilla',compact('list', 'totalPuestos', 'totalPersonal'));
    }

    public function organigrama(Request $request){
        $logUserComp = Auth::user()->empresa;
        $logUser = Auth::user()->email;
        $list = '';
        $image = '';
        //variable para definir el nivel de org, (para definir el color del items del menu)
        $nivel = 0;

        $boss_first = Employee::where('jefe', '')->orWhere('jefe', '0')->orderBy('nombre','DESC')->get();
        // $boss_first = Employee::where('jefe', '')->orWhere('jefe', '0')->orderBy('nombre','DESC')->get();
        // EEAP810913696
        

        // dd($boss_first);
        if(count($boss_first) > 0){
            
            $list = '<ul class="treeview">';
            foreach($boss_first as $boss){
                $image = null;
                $user = User::where('employee_id', $boss->id)->first();
                if($user){
                    $profile = Profile::where('user_id', $user->id)->first();
                    if($profile != null && !is_null($profile->image)){
                        $image = 'uploads/profile/'.$profile->image;
                    }else{
                        $image = 'img/vacantes/sinimagen.png';
                    }
                }
                
                $list .='<li id="open" class="encont"><a tabindex="1" class="btn btn-light" style="background:#373435 !important;color:white;  font-size:1rem; margin-bottom:10px;" data-toggle="popover" data-trigger="focus" title="<img class=img-org src='.asset($image).'>" data-content="<strong>Nombre: </strong><br>'.$boss->nombre.' '.$boss->paterno.'<br><strong>Puesto:</strong><br>'.$boss->jobPosition->name.'<br><strong>Área:</strong><br>'.$boss->jobPosition->area->name.'<br><strong>Ubicación:</strong><br>N/A<br><strong>Teléfono: </strong><br>'.$boss->telefono.'<br><strong>e-mail: </strong><br>'.$boss->correoempresa.'">'.$boss->nombre.' '.$boss->paterno.' - '.(isset($boss->jobPosition->name)?$boss->jobPosition->name:'N/A').' - '.(isset($boss->jobPosition->area->department->name)?$boss->jobPosition->area->department->name:'N/A').'</a>';
                $list .= $this->subBoss($boss->idempleado,$nivel); 
                $list .= '</li>';
            }
            $list .= '</ul>';
        }

        // LogActivity::create([
        //     'user_id'       =>   Auth::id(),
        //     'user_email'    =>   Auth::user()->email,
        //     'tag'    =>  'get',
        //     'url'    =>  $request->fullUrl(),
        //     'user_agent'    =>  \Illuminate\Support\Facades\Request::header('User-Agent'),
        //     'ip_address'    =>  \Illuminate\Support\Facades\Request::ip()
        // ]);
        $titulo = 'ORGANIGRAMA';
        return view('/organigrama',compact('list', 'titulo'));
    }

    private function subBoss($boss,$nivel){
        $logUserComp = Auth::user()->company;
        $logUser = Auth::user()->email;
        $list2 = '';

        //dd($boss);where('idempleado','!=',$boss)
        $subBos = Employee::where('jefe', $boss)->orderBy('division')->orderBy('nombre','ASC')->get();
        //dd($subBos);
        //azul
        //$nivelcolor=['#57B5E0','#8E0855','#E86EAD','#808285','#B8B8B8','#E86EAD'];
        // $nivelcolor=['#002C49','#0064A6','#ECC100','#4FB9FF','#FCF3CA','#002C49','#0064A6','#ECC100','#4FB9FF','#FCF3CA'];
        $nivelcolor=['#ED3237', '#ACACAC', '#FEFEFE', '#F1696B'];
        //morado
        //$nivelcolor=['#280059','#3f0f7a','#500da3','#762cd1','#965ae0','#ad7ee6','#cba5fa','#ddc2ff'];
        //NARANJA
        //$nivelcolor=['#592e00','#854502','#9e5303','#bd670b','#d47917','#d98d3c','#eba65b','#fabf7f'];
        
        if(count($subBos) > 0){
            
            $list2 .= '<ul>';

            foreach($subBos as $sub){

                $image = null;
                $user = User::where('employee_id', $sub->id)->first();
                if($user){
                    $profile = Profile::where('user_id', $user->id)->first();
                    if($profile != null && !is_null($profile->image)){
                        $image = 'uploads/profile/'.$profile->image;
                    }else{
                        $image = 'img/vacantes/sinimagen.png';
                    }
                }
                
                $list2 .='<li class="encont"><a tabindex="0" class="label label-default badge" style="background:'.$nivelcolor[$nivel].' !important;color:white; font-size:1rem; margin-bottom:10px;" data-toggle="popover" data-trigger="focus" title="<img class=img-org src='.asset($image).'>" data-content="<strong>Sucursal: </strong>'.$sub->sucursal.'<hr>'.$sub->nombre.' '.$sub->paterno.'<hr><strong>Teléfono: </strong>'.$sub->telefono.'<hr><strong>Celular: </strong>'.$sub->celular.'<hr><strong>e-mail: </strong>'.$sub->correoempresa.'">'.$sub->nombre.' '.$sub->paterno.' - '.(isset($sub->jobPosition->name)?$sub->jobPosition->name:'N/A').' - '.(isset($sub->jobPosition->area->department->name)?$sub->jobPosition->area->department->name:'N/A').'</a>';

                $list2 .= $this->subBoss($sub->idempleado,$nivel+1);
                
                $list2 .= '</li>';
            }

            $list2 .= '</ul>';
        }
    
        return $list2;
    }

    private function subJobs($boss,$nivel){
        $list2 = '';
        $nivelcolor=['#002C49','#0064A6', '#007DCC', '#6699C2', '#D4D6D9', '#FFCC29','#E5AD36','#FF9900'];
        
        if(count($boss->jobs) > 0){
            $list2 .= '<ul>';
            foreach($boss->jobs as $job){
                $padding = strval(($job->level->hierarchy * 4 * ($job->level->hierarchy - $nivel)) + 10);
                $lvl = strval($job->level->hierarchy - $nivel);
                $list2 .='<li class="encont c'.$lvl.'" style="padding-left:' . $padding . 'px"><a tabindex="0" class="label label-default badge" style="background:'.$nivelcolor[$job->level->hierarchy - 1].' !important;color:white; font-size:1rem; margin-bottom:10px;" data-toggle="popover" data-trigger="focus" title="<strong>'.$job->name.'" data-content="<strong>Nombre: </strong>'.$job->name.'<hr>">'.$job->name.' | Level: ' . $job->level->hierarchy .'</a>';
                $list2 .= $this->subJobs($job,$nivel+1);
                $list2 .= '</li>';
            }
            $list2 .= '</ul>';
        }
        return $list2;
    }

    public function organigramaPuestos(){
        $jobs = JobPosition::with('jobs', 'level')->bossTop()->get();
        $nivelcolor=['#002C49','#0064A6', '#007DCC', '#6699C2', '#D4D6D9', '#FFCC29','#E5AD36','#FF9900'];
        // $nivelcolor=['#592e00','#854502','#9e5303','#bd670b','#d47917','#d98d3c','#eba65b','#fabf7f'];
        $list = '<ul class="treeview">';
        $nivel = 0;
        foreach ($jobs as $key => $job) {
            if(!isset($job->level->hierarchy)) {
                $padding = strval(1 * 10 + 2);
                $list .='<li class="encont" style="margin-left: '. $padding . 'px"><a tabindex="0" class="label label-default badge" style="background:'.$nivelcolor[1 - 1].' !important;color:white; font-size:1rem; margin-bottom:10px;" data-toggle="popover" data-trigger="focus" title="<strong>'.$job->name.'" data-content="<strong>Nombre: </strong>'.$job->name.'<hr>">'.$job->name.'</a>';
                $list .= $this->subJobs($job,$nivel+1);
                $list .= '</li>';
            } else {
                $padding = strval($job->level->hierarchy * 10 + 2);
                $list .='<li class="encont" style="margin-left: '. $padding . 'px"><a tabindex="0" class="label label-default badge" style="background:'.$nivelcolor[$job->level->hierarchy - 1].' !important;color:white; font-size:1rem; margin-bottom:10px;" data-toggle="popover" data-trigger="focus" title="<strong>'.$job->name.'" data-content="<strong>Nombre: </strong>'.$job->name.'<hr>">'.$job->name.' | Level: ' . $job->level->hierarchy .'</a>';
                $list .= $this->subJobs($job,$nivel+1);
                $list .= '</li>';
            }
        }
        $list .= '</ul>';
        $titulo = 'PLANTILLA DE PUESTOS';
        return view('/organigrama',compact('list', 'titulo'));
    }
}
