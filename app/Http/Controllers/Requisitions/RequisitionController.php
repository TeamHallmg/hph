<?php

namespace App\Http\Controllers\Requisitions;

use App\Models\Employee;
use App\Models\JobPosition;
use App\Models\Area;
use App\Models\Department;
use App\Models\Direction;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\Requisitions\RequiConfig;
use App\Models\Requisitions\Requisition;

class RequisitionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	/*
	* Consultamos el nombre, del puesto seleccionado 
	* en el formulario de la requi
	*/
	public function consultaDepartamento($id) {
		/*$nombreDepartamento = Job::select('jobDepartment')
			->where('id', $id)
			->get();
	
		return response()->json($nombreDepartamento);*/
	}

	/*
	 * Consultamos la fecha, según el tipo de vacante seleccionado
	 */
	private function consultaFecha ($dias) {
		/*$hoy = date('Y-m-d');
		$fechaInicio= strtotime($hoy);
		//obtenemos el día de la semana
		$diasemana = date('N',$fechaInicio);
		//sumamos los días, según el tipo de vacante
		//al día de la semana
		$totaldias = $diasemana+$dias;
		$findesemana =  intval($totaldias / 5) * 2; 
		$diasabado = $totaldias % 5; 
		if ($diasabado==6) $findesemana++;
		//if ($diasabado==0) $findesemana=$findesemana-2;
		$total = (($dias+$findesemana) * 86400) + $fechaInicio;  

		$fechaFin=date('Y-m-d', $total);
		//return response()->json($fechaFin);
		return $fechaFin;*/
	}

	private function fechaRequerida($req) {
		/*if ($req->vacancieType == 'Semanal') {
			$valor = $this->consultaFecha(14);
		} 
		if ($req->vacancieType == 'Catorcenal') {
			$valor = $this->consultaFecha(40);
		} 
		if ($req->vacancieType == 'Jefe') {
			$valor = $this->consultaFecha(50);
		}

		$req->authorization = 'AUTORIZADA';
		$req->dateAuthorization = date('Y-m-d');
		$req->status  = 'AUTORIZADA';
		$req->dateRequired  = $valor;
		$req->save();*/
	}

	private function enviaCorreo($requisition) {
		$to = $requisition->nombre->email;
		/**
      	* Mail notification for the boss
      	*/
		/*Mail::send('emails.requisition.pendingRequisition', ['requisition' => $requisition], function ($mail)  use ($requisition, $to){
        	$mail->from('testmaver@hallmg.com');
        	//$mail->to($boss->email);
        	$mail->to($to);
        	$mail->subject('Requisición de Personal pendiente por autorizar.');
      	});*/
    }

	/*
	 * Cambiamos el estatus de la requisición
	 * cuando es aprobada por el jefe correspondiente
	 */
	public function autorizar($id) {
		$requisition = Requisition::where('id', $id)->first();
		// $sig_current = RequiConfig::sigCurrent($requisition);

		// dd(Auth::user()->employee->jefe, Auth::user()->employee->jefe);
		$where = 'idk';
		$no_continua = true;
		if(is_null(Auth::user()->employee)) { // Si es soporte
			// $data['current_id'] = 1;
			$no_continua = true;
			$where = '1';
		} elseif(Auth::user()->employee->jefe != '0') { // Si es empleado
			// Auth::user()->employee->jefe != null ||
			$user_id = Auth::user()->employee->boss->user->id;
			$no_continua = false;
			$where = '2';
		} else { // Si es jefe final
			$no_continua = true;
			$where = '3';
		}

		// dd($where, Auth::user()->employee->jefe != '0');

		if ($no_continua) {
			$requisition->estatus_requi = 'AUTORIZADA';
			$requisition->save();
			//enviamos correo
			//$this->enviaCorreo($requisition);
		} else {
			$requisition->current_id = $user_id;
			$requisition->estatus_requi = 'EN PROCESO';
			$requisition->save();
			//enviamos correo
			//$this->enviaCorreo($requisition);
		}

		// redirigimos a la pantalla index
        return $this->index();
	} 


	public function rechazar(Request $request) {
		$id = $request->input('id');

		try {
			DB::beginTransaction();
				Requisition::where('id', $id)->update([
					'rechazo_descripcion'=> $request->input('rechazo_descripcion'),
					'estatus_requi'=> 'RECHAZADO',
				]);
				Requisition::where('id', $id)->delete();
			DB::commit();
		}
		// Ha ocurrido un error, devolvemos la BD a su estado previo y hacemos lo que queramos con esa excepción
		catch (\Throwable $e) {
			DB::rollback();
			return redirect()->back()->with('flag', 'ERROR, NO HA PODIDO RECHAZAR LA REQUISICIÓN');
		}

		// redirigimos a la pantalla index
		$request->session()->flash('alert-success', 'La requisicón a sido rechazada correctamente!!!');
		return redirect()->to('requisitions');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		/*
		 * Revisamos si la persona logueda, es jefe del solicitante de la requi
		 * y muestre el botón de autorizar requi en el index
		 */
		$userLogueado = Auth::id(); //Employee::findByEmployeeNumberGetIDEmpleado(Auth::user()->employee_id);
		$userRole = Auth::user()->role;

		//$requisitions = Requisition::where('confidential','ABIERTA')
		$requisitions = Requisition::where(function ($q) use ($userLogueado){
			$q->where('solicitante_id',$userLogueado);
		})->orWhere(function ($q) use ($userLogueado) {
			$q->where('current_id', $userLogueado)
			->where('estatus_requi', 'SOLICITADO');
		})->orWhere(function ($q) use ($userLogueado) {
			$q->where('current_id', $userLogueado)
			->where('estatus_requi', 'EN PROCESO');
		})->withTrashed()->get();

		foreach ($requisitions as $requisition) {

			if($requisition->current_id == Auth::user()->id && $requisition->estatus_requi != 'AUTORIZADA' && $requisition->estatus_requi != 'RECHAZADO') {
				$requisition->autorizacion = true;
			}
			if($requisition->solicitante_id == Auth::user()->id) {
				$requisition->editar = true;
			}
			if (!is_null($requisition->deleted_at) && $requisition->estatus_requi == 'RECHAZADO') {
				$requisition->rechazo = true;
			}
			if($requisition->estatus_requi == 'AUTORIZADA' || $requisition->estatus_requi == 'RECHAZADO') {
				$requisition->editar = false;
			}
			
			// $autoriza = RequiConfig::where('user_id', $userLogueado)->where('tipo_vacante', $requisition->tipo_vacante)->first();
			// if (!is_null($autoriza) && $requisition->estatus_requi != 'AUTORIZADA' && $requisition->estatus_requi != 'RECHAZADO') {
			// 	if ($requisition->current_id == $autoriza->user_id) {
			// 		$requisition->autorizacion = true;
			// 	}
			// }
			// if($requisition->solicitante_id == $userLogueado && $requisition->estatus_requi == 'SOLICITADO') {
			// 	$requisition->editar = true;
			// }

			
		}

		$rutas = RequiConfig::orderBy('orden', 'ASC')->get();

		// load the view and pass the requisitions
		return View('requisitions.index', compact('requisitions', 'rutas'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {

		$jobPosition = JobPosition::pluck('name', 'id');

        $departments = Department::pluck('name', 'id');

		return View('requisitions.create_old', compact('jobPosition','departments'));

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//dd($request->all());
		$data = $request->all();

		$edad_min = $request->input('edad_min');
		$edad_max = $request->input('edad_max');

		//antes 15-01 Carlos Lezama
		// if($data['tipo_vacante'] == 'CREAR') {
		// 	$data['puesto_id'] = $request->input('puesto');
		// } else {
		// 	$data['puesto_nuevo'] = $request->input('puesto_nuevo');
		// }
		//antes 15-01 Carlos Lezama
		

		if($data['tipo_vacante'] == 'CREAR') {
			$data['puesto_nuevo'] = $request->input('puesto_nuevo');
		} else {
			$data['puesto_id'] = $request->input('puesto');
		}

		if( $edad_max < $edad_min ) {
			return redirect()->back()->with('flag', 'Edad máxima debe ser mayor o igual a edad mínima ')->withInput($request->all());
		}

		// $current = RequiConfig::current($data['tipo_vacante']);
		// if(is_null($current)){
		// 	$data['current_id'] = 1;
		// } else {
		// 	$data['current_id'] = $current->user_id; //Auth::user()->getBossId();
		// }

		if(is_null(Auth::user()->employee)) { // Si es soporte
			$data['current_id'] = 1;
		} elseif(Auth::user()->employee->jefe != '0') { // Si es empleado
			// Auth::user()->employee->jefe != null || 
			$data['current_id'] = Auth::user()->employee->boss->user->id;
		} else { // Si es jefe final
			$data['current_id'] = Auth::user()->id;
		}
		
		$data['solicitante_id'] = Auth::id();
		// dd($data);
		try {
			DB::beginTransaction();
				Requisition::create($data);
			DB::commit();
		}
		catch (\Throwable $e) {
			// dd($e->getMessage());
			DB::rollback();
			//echo 'ERROR (' . $e->getCode() . '): ' . $e->getMessage();
			return redirect()->back()->with('flag', '
				VERIFIQUE QUE LA INFORMACIÓN INGRESADA SEA CORRECTA,
				DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. '.$e->getMessage());
		}
	
		// redirect
        $request->session()->flash('alert-success', 'Los datos se han agregado correctamente!!!');
        return redirect()->to('requisitions');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$userLogueado = Auth::id(); //Employee::findByEmployeeNumberGetIDEmpleado(Auth::user()->employee_id);
		$userRole = Auth::user()->role;

		$requisition = Requisition::where('id', $id)->withTrashed()->first();

		if($requisition->current_id == Auth::user()->id && $requisition->estatus_requi != 'AUTORIZADA' && $requisition->estatus_requi != 'RECHAZADO') {
			$requisition->autorizacion = true;
			$requisition->descripcion_rechazo = false;
			$requisition->rechaza = true;
		}

		if (!is_null($requisition->deleted_at) && $requisition->estatus_requi == 'RECHAZADO') {
			$requisition->rechazo = true;
		}

		// $autoriza = RequiConfig::where('user_id', $userLogueado)->first();
		
		// if(is_null($requisition)) {
		// 	$requisition = Requisition::withTrashed()->find($id);
		// 	$requisition->descripcion_rechazo = true;
		// }

		// if (!is_null($autoriza) && !is_null($requisition)) {
		// 	if ($requisition->current_id == $autoriza->user_id && $requisition->estatus_requi != 'RECHAZADO' && $requisition->estatus_requi != 'AUTORIZADA' ) {
		// 		$requisition->autorizacion = true;
		// 		$requisition->descripcion_rechazo = false;
		// 	}

		// 	if($autoriza->rechaza == 1){
		// 		$requisition->rechaza = true;
		// 	}
		// }

		$requisition->fecha_requerida = Carbon::createFromFormat('Y-m-d', $requisition->fecha_requerida)->format('d/m/Y');
		$requisition->fecha_elaborada = Carbon::createFromFormat('Y-m-d', $requisition->fecha_elaborada)->format('d/m/Y');
		//$requisition->fecha_autorizacion = Carbon::createFromFormat('Y-m-d', $requisition->fecha_autorizacion)->format('d/m/Y');

		// show the view and pass the requisitions to it
		return View('requisitions.show', compact('requisition'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$requisition = Requisition::find($id);
		$jobPosition = JobPosition::pluck('name', 'id');
        $departments = Department::pluck('name', 'id');
		// show the edit form and pass the requisitions
		return View('requisitions.edit', compact('requisition', 'jobPosition', 'departments'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		// dd($request);
		$data = $request->all();

		$edad_min = $request->input('edad_min');
		$edad_max = $request->input('edad_max');

		// if (is_null($request->input('puesto_nuevo'))) {
		// 	$data['puesto_id'] = $request->input('puesto');
		// } else {
		// 	$data['puesto_id'] = $request->input('puesto_nuevo');
		// }

		if($data['tipo_vacante'] == 'CREAR') {
			$data['puesto_id'] = $request->input('puesto');
		} else {
			$data['puesto_nuevo'] = $request->input('puesto_nuevo');
		}

		if( $edad_max < $edad_min ) {
			return redirect()->back()->with('flag', 'Edad máxima debe ser mayor o igual a edad mínima ')->withInput($request->all());
		}

		try {
			DB::beginTransaction();
				$requi = Requisition::findorfail($id);
				$requi->update($data);
			DB::commit();
		}
		// Ha ocurrido un error, devolvemos la BD a su estado previo y hacemos lo que queramos con esa excepción
		catch (\Throwable $e) {
			dd($e->getMessage());
				DB::rollback();
				// no se... Informemos con un echo por ejemplo
				//echo 'ERROR (' . $e->getCode() . '): ' . $e->getMessage();
				return redirect()->back()->with('flag', 'ERROR, NO HA ACTUALIZADO LA REQUISICIÓN');
		}

		// redirect
        $request->session()->flash('alert-success', 'Los datos se han modificado correctamente!!!');
        return redirect()->to('requisitions');	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		/*$actualizar = Requisition::where('id', $id)
			->update([
				'authorization'=> 'CANCELADA',
				'status'=> 'CANCELADA'
			]);
		$borrar = Requisition::find($id)->delete();

		//$request->session()->flash('alert-danger', 'La requisición se ha cancelado correctamente!!!');
        return redirect()->to('requisitions');*/
	}

	/*private function setExcelLayoutHeader($sheet) {
        $sheet->row(1, [
        	'N° REQUISICIÓN',
        	'SOLICITANTE',
        	'TIPO',
        	'PUESTO',
        	'FECHA REQUERIDA',
        	'FECHA REALIZADA',
        	'ESTATUS'
        ]);
        $sheet->row(1, function($row){
            $row->setBackground('#BF1A2C');
            $row->setFontColor('#ffffff');
            $row->setFontWeight('bold');
        });
    }*/

	/*public function depurar(Request $request) {
		date_default_timezone_set('America/Mexico_City');
		$estatus = $request->estatus;
		$from = $request->fechaInicial;
		$to = $request->fechaFinal;
		$flag = false;

		if ($estatus == 'todos') {
			$estatus1 = 'SOLICITADO';
			$estatus2 = 'AUTORIZADA';
			$estatus3 = 'CANCELADA';
			$flag = true;
		}
		if ($flag) {
			$requisicion = Requisition::withTrashed()
				->where(function($q) use ($estatus1, $estatus2, $estatus3) {
					$q->where('authorization', $estatus1)
						->orWhere('authorization', $estatus2)
						->orWhere('authorization', $estatus3);
				})->whereBetween('dateRequired', [$from, $to])
				//->whereNull('dateRequired')
				->get();

			Requisition::whereIn('id', $requisicion->pluck('id'))->delete();

			/** Creamos nuestro archivo Excel *
			Excel::create('Requisiciones depuradas '.date('d-m-Y'), function ($excel) use ($requisicion) {
			/** Creamos una hoja *
				$excel->sheet("requisiciones depuradas", function ($sheet) use ($requisicion) {
					//formateamos los encabezados
					$this->setExcelLayoutHeader($sheet);
					//$sheet->fromArray($data);
					//mandamos la info
					foreach($requisicion as $index => $requi) {
						$sheet->row($index+2, [
							$requi->id,
							$requi->solicitante->first_name,
							$requi->vacancieType,
							$requi->puestos->puesto,
							$requi->dateRequired,
							$requi->dateApplication,
							$requi->status,
						]);
					}
				});
			})->download('xlsx');
		} else {
			$requisicion = Requisition::withTrashed()
				->where('status', $estatus)
				->whereBetween('dateRequired', [$from, $to])
				//->whereNull('dateRequired')
				->get();

			Requisition::whereIn('id', $requisicion->pluck('id'))->delete();
			
			/** Creamos nuestro archivo Excel *
			Excel::create('Requisiciones depuradas '.date('d-m-Y'), function ($excel) use ($requisicion) {
			/** Creamos una hoja *
				$excel->sheet("requisiciones depuradas", function ($sheet) use ($requisicion) {
					//formateamos los encabezados
					$this->setExcelLayoutHeader($sheet);
					//$sheet->fromArray($data);
					//mandamos la info
					foreach($requisicion as $index => $requi) {
						$sheet->row($index+2, [
							$requi->id,
							$requi->solicitante->first_name,
							$requi->vacancieType,
							$requi->puestos->puesto,
							$requi->dateRequired,
							$requi->dateApplication,
							$requi->status,
						]);
					}
				});
			})->download('xlsx');
		}
		//
		$request->session()->flash('alert-success', 'Los datos se han depurado correctamente!!!');
		return redirect()->back();
	}*/
}
