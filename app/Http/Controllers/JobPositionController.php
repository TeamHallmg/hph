<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\JobPosition;
use App\Models\Bienes\TipoBienes;
use App\Models\jobPositionsBienes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class JobPositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = JobPosition::get();

        return view('jobPosition.index', compact('jobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $jobs = JobPosition::get();
        $TipoBienes = TipoBienes::get();
        return view('jobPosition.create', compact('jobs','TipoBienes'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = ['name'=>'required'];
        $messages = ['name.required' => 'El nombre del puesto es necesario'];

        if($request->hasFile('file')){
            $file = $request->file('file');
            $rules = array_merge($rules, ['file'=>'file|mimes:pdf,png,jpeg,jpg,docx|max:204800']);
            $messages = ['file.mimes' => 'Extensión invalida'];
        }

        // if($request->hasFile('benefits')){
        //     $benefits = $request->file('benefits');
        //     $rules = array_merge($rules, ['benefits'=>'file|mimes:png,jpeg,jpg|max:204800']);
        //     $messages = ['benefits.mimes' => 'Sólo imagenes'];
        // }

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            // dd('skere');
            $data = $request->all();
            $allowedfileExtension=['pdf','png','jpeg','jpg','docx'];

            if (!empty($file)) {
                $filename = uniqid() . '-' .$file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);
                
                if ($check) {
                    $request->file('file')->move('puestos/', $filename);
                    $data['file'] = $filename;
                }
            }

            // $allowedfileImgExtension=['png','jpeg','jpg'];

            // if (!empty($benefits)) {
            //     $filename = uniqid() . '-' .$benefits->getClientOriginalName();
            //     $extension = $benefits->getClientOriginalExtension();
            //     $check = in_array($extension, $allowedfileImgExtension);
                
            //     if ($check) {
            //         $request->file('benefits')->move('puestos/', $filename);
            //         $data['benefits'] = $filename;
            //     }
            // }

            try {
                DB::beginTransaction();

                    $JobPosition = JobPosition::create($data);

                    if(isset($data['TipoBiene'])){

                        foreach($data['TipoBiene'] as $TipoBie) {
                            $jobPositionsBienes = new jobPositionsBienes;
                            $jobPositionsBienes->id_tipo_bien = $TipoBie;
                            $jobPositionsBienes->id_job_positions =  $JobPosition->id;
                            $jobPositionsBienes->save();
                        }

                    }
                DB::commit();
            } catch (\Throwable $e) {
                DB::rollback();

                return redirect()->back()->with('alert-danger', '
                ¡UPS!... NO SE ACTUALIZÓ EL PUESTO <br/>
                VERIFIQUE LA INFORMACIÓN INGRESADA, DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. <br/>
                ' . $e->getMessage());
            }

            return redirect()->back()->with('alert-success', 'El puesto se ha ingresado correctamente');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $job = JobPosition::findOrfail($id);

        $TipoBienes = TipoBienes::with([
            'jobpositionsbienes' => function($q) use ($id) {
                $q->where('id_job_positions','=', $id);
            }
        ])
        ->whereHas('jobpositionsbienes', function($q) use ($id) {
                $q->where('id_job_positions','=', $id);
        })
        ->get();

        return view('jobPosition.show', compact('job','TipoBienes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        $TipoBienes = TipoBienes::with([
            'jobpositionsbienes' => function($q) use ($id) {
                $q->where('id_job_positions','=', $id);
            }
        ]) 
        ->get();

        $job = JobPosition::findOrfail($id);
        $jobs = JobPosition::get();
        return view('jobPosition.edit', compact('job','jobs','TipoBienes'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = ['name'=>'required'];
        $messages = ['name.required' => 'El nombre del puesto es necesario'];

        if($request->hasFile('file')){
            $file = $request->file('file');
            $rules = array_merge($rules, ['file'=>'file|mimes:pdf,png,jpeg,jpg,docx|max:204800']);
            $messages = ['file.mimes' => 'Extensión invalida'];
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $data = $request->except('_method', '_token');
            $allowedfileExtension=['pdf','png','jpeg','jpg','docx'];

            if (!empty($file)) {
                $filename = uniqid() . '-' .$file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);
                
                if ($check) {
                    $request->file('file')->move('puestos/', $filename);
                    $data['file'] = $filename;
                }
            }

            $dataJobPosition = $request->except('TipoBiene','_method', '_token');
            try {

                DB::beginTransaction();

                    JobPosition::where('id',$id)->update($dataJobPosition);
                    $lenguajes = jobPositionsBienes::where('id_job_positions', $id)->where('estatus',1)->get();
                    $lenguajestoArray = jobPositionsBienes::where('id_job_positions', $id)->where('estatus',1)->get()->all();
                    $canActual = jobPositionsBienes::where('id_job_positions', $id)->where('estatus',1)->count();
                    if(isset($data['TipoBiene']))       
                    {
                    $canRecibida = count($data['TipoBiene']);

                    if(count($lenguajes)==0 ){


                        foreach ($data['TipoBiene'] as $idioma) {

                            $exists = jobPositionsBienes::
                            where('id_tipo_bien', $idioma)
                            ->where('id_job_positions',$id)
                            ->exists();

                            if($exists){

                                jobPositionsBienes::where('id_tipo_bien', $idioma)
                                                    ->where('id_job_positions',$id)
                                                    ->update(['estatus' => 1]);

                            }else{

                                jobPositionsBienes::create([
                                    'id_tipo_bien' => $idioma,
                                    'id_job_positions' => $id,
                                    'estatus' => 1
                                ]);

                            }
                               

                        }

                    }else{


                        if ($canRecibida < $canActual) {
                            
                            $tmp = [];

                            foreach ($data['TipoBiene'] as $idioma) {

                                foreach ($lenguajes as $lenguaje) {

                                    if ($lenguaje->id_tipo_bien == $idioma && !in_array($idioma, $tmp)) {

                                        $existen[$lenguaje->id] = $lenguaje->id_tipo_bien;

                                        array_push($tmp, $idioma);

                                    } else {

                                        $borrar[$lenguaje->id] = $lenguaje->id_tipo_bien;

                                    }

                                }

                            }

                            $eliminar = array_diff_assoc($borrar, $existen);

     
                            foreach ($eliminar as $key => $elimina) {

                                jobPositionsBienes::where('id', $key)->update(['estatus' => 0]);

                            }


                        } else if ($canRecibida == $canActual) {
                            
                            $tmp = [];
                            $tmp2 = [];

                            foreach ($data['TipoBiene'] as $idioma) {

                                foreach ($lenguajes as $lenguaje) {

                                    if ($lenguaje->id_tipo_bien != $idioma && !in_array($idioma, $tmp) && !in_array($lenguaje->id_tipo_bien, $tmp2)) {

                                        $new[] = $idioma;
                                        $borrar[$lenguaje->id] = $lenguaje->id_tipo_bien;

                                        array_push($tmp, $idioma);
                                        array_push($tmp2, $lenguaje->id_tipo_bien);

                                    } 

                                }

                            }
                           
                            foreach ($new as $key) {


                                $exists = jobPositionsBienes::
                                where('id_tipo_bien', $key)
                                ->where('id_job_positions',$id)
                                ->exists();

                                if($exists){

                                    jobPositionsBienes::where('id_tipo_bien', $key)
                                                        ->where('id_job_positions',$id)
                                                        ->update(['estatus' => 1]);

                                }else{

                                    jobPositionsBienes::create([
                                        'id_tipo_bien' => $key,
                                        'id_job_positions' => $id,
                                        'estatus' => 1
                                    ]);

                                }


                            }

                            foreach ($borrar as $key => $elimina) {

                                jobPositionsBienes::where('id', $key)->update(['estatus' => 0]);

                            }

                        } 

                        elseif ($canRecibida > $canActual) {
                      
                            $tmp = [];
                            $tmp2 = [];
                            $añadir = [];
                            $mantener = [];
                            $eliminar = [];

                            foreach ($data['TipoBiene'] as $idioma) {

                                $idioma = intval($idioma);

                                foreach ($lenguajes as $lenguaje) {

                                    if ($lenguaje->id_tipo_bien == $idioma && !in_array($idioma, $tmp)) {

                                        $mantener[$lenguaje->id] = $lenguaje->id_tipo_bien;

                                        array_push($tmp, $idioma);

                                    } 

                                }

                            }


                            if($mantener==''){

                                $añadir = array_diff($data['TipoBiene'], $mantener);

                                $eliminar = array_diff($lenguajestoArray,$mantener);


                                foreach ($eliminar as $key => $value ) {
                                    
                                    jobPositionsBienes::where('id', $key)->update(['estatus' => 0]);


                                }    


                            }else{

                                $eliminar = array_diff($lenguajestoArray,$data['TipoBiene']);
                                $añadir = array_diff($data['TipoBiene'],$lenguajestoArray);
                                

                                foreach ($eliminar as $key ) {
                                    
                                    jobPositionsBienes::where('id', $key->id)->update(['estatus' => 0]);

                                }

                            }
                   
                            foreach ($añadir as $key) {

                                $exists = jobPositionsBienes::
                                where('id_tipo_bien', $key)
                                ->where('id_job_positions',$id)
                                ->exists();

                                if($exists){

                                    jobPositionsBienes::where('id_tipo_bien', $key)
                                                        ->where('id_job_positions',$id)
                                                        ->update(['estatus' => 1]);

                                }else{

                                    jobPositionsBienes::create([
                                        'id_tipo_bien' => $key,
                                        'id_job_positions' => $id,
                                        'estatus' => 1
                                    ]);
                                    
                                }

                              
                            }


                        } 

                    }
                    }


                DB::commit();
            } catch (\Throwable $e) {
                DB::rollback();

                return redirect()->back()->with('alert-danger', '
                ¡UPS!... NO SE ACTUALIZÓ EL PUESTO <br/>
                VERIFIQUE LA INFORMACIÓN INGRESADA, DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. <br/>
                ' . $e->getMessage());
            }

            return redirect()->back()->with('alert-success', 'El puesto se ha actualizado correctamente');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $job = JobPosition::findOrfail($id);

        try {
            DB::beginTransaction();
                $job->delete();
            DB::commit();
        } catch (\Throwable $e) {
            DB::rollback();

            return redirect()->back()->with('alert-danger', '
            ¡UPS!... NO SE ELIMINÓ EL PUESTO <br/>
            VERIFIQUE LA INFORMACIÓN INGRESADA, DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. <br/>
            ' . $e->getMessage());
        }
        
        return redirect()->back()->with('alert-success', 'El puesto se ha eliminado correctamente');
    }

    public function beneficios() {
        $id = Auth::id();
        $user = User::findOrfail($id);
        $beneficio = null;

        if (!is_null($user->employee)) {
            $idJob = $user->employee->job_position_id;
            $jobBenefit = JobPosition::findOrfail($idJob);

            if(!is_null($jobBenefit->benefits)) {
                $beneficio = $jobBenefit->benefits;
            }
        }

		return view('jobPosition.beneficio', compact('beneficio'));
    }

    public function getJobPositionData($id = null) {
        return JobPosition::find($id);
    }
}
