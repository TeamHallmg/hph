<?php

namespace App\Http\Controllers\Vacantes;

use App\Employee;
use App\Models\JobPosition;
use Illuminate\Http\Request;
use App\Models\Vacantes\Vacante;
use Illuminate\Support\Facades\DB;
use App\Models\Vacantes\Postulante;
use App\Models\Vacantes\Reclutador;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Requisitions\Requisition;
use Illuminate\Support\Facades\Input;

class VacantesController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        // ==== Admin Vacante ====
        $this->middleware('permission:see_vacancies')->only(['administrar']);
        $this->middleware('permission:generate_vacant')->only(['recruiter']);
        // ==== /Admin Vacante ====
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logueado = Auth::id();
        //mandamos llamar todas las vacantes disponibles
        //$vacantes = Vacante::get();
        $vacantes = Vacante::whereDoesntHave('postulante', function($q) use ($logueado){
            $q->where('user_id', $logueado)
            ->where('intentos_postulacion', 'SI');
        })->get();

        // $mis_postulaciones = Vacante::where('postulante', function($q) use ($logueado){
        //     $q->where('user_id', $logueado)
        //     ->where('intentos_postulacion', 'SI');
        // })->get();

        $mis_postulaciones = Postulante::where('user_id', Auth::id())->get();

        return View('vacantes.vacantes.index', compact('vacantes', 'mis_postulaciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $requisition = Requisition::find($id);
        $is_recruiter = Input::get('recruiter');

		// show the edit form and pass the requisitions
		return View('vacantes.vacantes.show', compact('requisition', 'is_recruiter'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$requisition = Requisition::with('vacante')->where('id', $id)->first();
        $vacante = Vacante::with('requisicion')->where('id', $id)->first();

        $postulation = Postulante::where('vacante_id', $vacante->id)->where('user_id', Auth::id())->first();

        if($postulation != null)
            $postulation_flag = true;
        else
            $postulation_flag = false;

		// show the edit form and pass the requisitions
		return View('vacantes.vacantes.edit', compact('vacante', 'postulation_flag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function administrar() {
        //mandamos llamar todas las requisiciones que su estatus es autorizada, pre vacantes
        //será vacante cuando se le asigne un reclutador
        //y se filtra las requis de las vacanes ya cerradas

        $requisitions = Requisition::where('estatus_requi', 'AUTORIZADA')->get();

        return View('vacantes.vacantes.admin', compact('requisitions'));
    }

    public function reclutador() {
        //mandamos llamar todas las requisiciones que su estatus es autorizada, pre vacantes
        //será vacante cuando se le asigne un reclutador
        //y se filtra las requis de las vacanes ya cerradas
        $reclutador = Reclutador::where('user_id', Auth::user()->id)->first();

        if(is_null($reclutador)) {
            return redirect()->to('vacantes')->with('alert-danger', 'No tienes permisos para realizar esta acción...');
        }
        $vacantes = Vacante::where('reclutador_id', $reclutador->id)->whereNull('fecha_cierre')->get();
        $tmp = [];
        foreach($vacantes as $vacante) {
            array_push($tmp, $vacante->requisicion_id);
        }

        $requisitions = Requisition::where('estatus_requi', 'AUTORIZADA')->whereIn('id', $tmp)->get();
        $is_recruiter = true;

        return View('vacantes.vacantes.admin', compact('requisitions', 'is_recruiter'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function recruiter($id) {
        $requisition = Requisition::find($id);
        $reclutadores = Reclutador::get();

		// show the edit form and pass the requisitions
		return View('vacantes.vacantes.recruiter', compact('requisition', 'reclutadores'));
    }

    /**
     * agreggar un reclutador, para la vacante
     */
    public function addRecruiter(Request $request) {
        $data = $request->all();

		try {
            DB::beginTransaction();
                $data['status'] = 'Abierta';
				Vacante::create($data);
			DB::commit();
		}
		catch (\Throwable $e) {
            DB::rollback();
            /* echo 'ERROR (' . $e->getCode() . '): ' . $e->getMessage();
            die; */
            return redirect()->back()->with('alert-danger', '
				VERIFIQUE QUE LA INFORMACIÓN INGRESADA SEA CORRECTA,
				DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE.');
		}
	
		// redirect
        return redirect()->to('vacantes')->with('alert-success', 'Los datos se han agregado correctamente!!!');
    }

    public function showExt($id) {
        $vacante = Vacante::with('requisicion')->where('id', $id)->first();

		// show the edit form and pass the requisitions
		return View('vacantesExt.show', compact('vacante'));
    }

    public function closeVacancie($id) {

        try {
            DB::beginTransaction();
                $externos = '';
                $contador = 1;
                $vacante = Vacante::find($id);
                $postulantes = Postulante::where('vacante_id', $id)->get();
                $guardados = Postulante::with('vacante')->with('perfil')->where(['vacante_id' => $id, 'aceptacion_postulacion' => 'SI'])->get();
                
                foreach($guardados as $key => $postulante) {
                    $postulante->selected = true;
                    $postulante->save();

                    if(is_null($postulante->usuario->employee_id)) {
                        $employee = Employee::create([
                            'idempleado' => 'ASIGNAR',
                            'nombre' => $postulante->perfil->name,
                            'paterno' => $postulante->perfil->surname_father,
                            'materno' => $postulante->perfil->surname_mother,
                            'fuente' => 'VACANTE EXTERNO',
                            'rfc' => 'ASIGNAR'
                        ]);

                        $externos .= '('.$contador.') '.$postulante->perfil->name.' '.$postulante->perfil->surname_father.' '.$postulante->perfil->surname_mother. ', ';
                        $contador += 1;

                        $postulante->usuario->employee_id = $employee->id;
                        $postulante->usuario->save();
                        $postulante->usuario->delete();
                        $employee->delete();
                    }
                }

                if($externos != '') {
                    $externos = substr($externos, 0, -2);
                    $mensaje = 'La vacante se cerro correctamente, además se crearon los empleados -> '. $externos .'!!!';
                } else {
                    $mensaje = 'La vacante se cerro correctamente!!!';
                }
        
                foreach($postulantes as $postulante) {
                    // Postulante::where('vacante_id', $id)->update(['intentos_postulacion' => 'NO', 'motivo_cierre_vacante' => $request->motivoCierreVacante]);
                    Postulante::where('vacante_id', $id)->update([
                        'intentos_postulacion' => 'NO',
                        //'aceptacion_postulacion' => null,
                    ]);
                    $postulante->delete();
                }
                $vacante->fecha_cierre = date('Y-m-d');
                $vacante->status = 'Cerrada';
                $vacante->save();

                // se hace borrado lógico de la requisición y la vacante
                // para evitar que se muestre como posible nueva vacante
                $vacante->delete();
                $vacante->requisicion()->delete();

                

            DB::commit();

            return redirect()->to('vacantes/administrar')->with('alert-success', $mensaje);
		}
		catch (\Throwable $e) {
            DB::rollback();
            /* echo 'ERROR (' . $e->getCode() . '): ' . $e->getMessage();
            die; */
            return redirect()->back()->with('alert-danger', '
				VERIFIQUE QUE LA INFORMACIÓN SEA CORRECTA,
				DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE.'.$e->getMessage());
		}
		
    }

    public function closedVacancies() {
        $closed = Vacante::where('status', 'Cerrada')->withTrashed()->get();

        return view('vacantes.vacantes.cerradas', compact('closed'));
    }
}
