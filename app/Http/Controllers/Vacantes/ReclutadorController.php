<?php

namespace App\Http\Controllers\Vacantes;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Vacantes\Reclutador;
use App\Models\Vacantes\Vacante;
use App\User;
use App\Models\Role;
use App\Models\RoleUser;


class ReclutadorController extends Controller
{
    public function __construct() {
        $this->middleware('auth');

        // ==== Admin Vacante ====
        $this->middleware('permission:add_remove_recruiter')->only(['store', 'destroy']);
        // ==== /Admin Vacante ====
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reclutadores = Reclutador::get();
        $recruiters = Reclutador::get()->pluck('user_id');
        $usuarios = User::orderby('first_name', 'ASC')->whereNotIn('id', $recruiters)->get()->pluck('FullName', 'id')->toArray();

        return View('vacantes.reclutador.index', compact('reclutadores', 'usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $reclutador = $request->input('reclutador');
		
		try {
			DB::beginTransaction();
				Reclutador::create([
                    'user_id' => $reclutador
                ]);

                $role = Role::where('name', 'recruiter')->first(); // Quiero que si no lo encuentra, truene
                RoleUser::create([
                    'role_id' => $role->id,
                    'user_id' => $reclutador
                ]);
                
			DB::commit();
		}
		catch (\Throwable $e) {
            DB::rollback();
            return redirect()->back()->with('alert-danger', '
				VERIFIQUE QUE LA INFORMACIÓN INGRESADA SEA CORRECTA,
				DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE.');
		}
	
		// redirect
        return redirect()->back()->with('alert-success', 'Los datos se han agregado correctamente!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id) {

        $id = $request->input('id_reclutador');
        $reclutador_sucesor = $request->input('reclutador_sucesor');


        try {
            

            DB::beginTransaction();

                $recruiter = Reclutador::findOrFail($id);
                // dd($recruiter);

                $role = Role::where('name', 'recruiter')->first(); // Quiero que si no lo encuentra, truene
                $role_user = RoleUser::where('user_id', $recruiter->user_id)->where('role_id', $role->id)->first();
                if($role_user != null)
                    $role_user->delete();
                // Hay un bug que se va a corregir si se separan bien los permisos

                $recruiter->delete();


                Vacante::where('reclutador_id', $id)->update(['reclutador_id' => $reclutador_sucesor]);

            DB::commit();

        } catch (\Throwable $e) {
            DB::rollback();
            /* echo 'ERROR (' . $e->getCode() . '): ' . $e->getMessage();
            die; */
            return redirect()->back()->with('alert-danger', '
                VERIFIQUE QUE LA INFORMACIÓN INGRESADA SEA CORRECTA,
                DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE.');
        }

        return redirect()->back()->with('alert-success', 'El reclutador se ha eliminado correctamente!!!');
    }
}
