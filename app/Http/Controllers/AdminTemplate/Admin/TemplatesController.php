<?php

namespace App\Http\Controllers\AdminTemplate\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use App\Models\Templates\Templates;
use App\Models\Templates\TemplatesModules;
use App\Models\Templates\TemplatesDescription;
use Illuminate\Support\Facades\Auth;
use App\Models\Templates\TemplatesFields;
use App\User;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Validator;


use App\Models\Templates\TemplatesPlansSends;
use App\Models\Templates\TemplatesPlansSendsDetails;


class TemplatesController extends Controller
{ 
    
    public function __construct()
    {
        $host = $_SERVER['HTTP_HOST'];
        $protocol=$_SERVER['PROTOCOL'] = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https' : 'http';
        $this->path = "$protocol://$host". '/img/plantillas/';
    }


    public function index()
    {

        $records = Templates::get();

        return View('templates.admin.index', compact('records'));

    }

    public function create()
    {

        $modulos = TemplatesModules::get();

        return View('templates.admin.create', compact('modulos'));

    }   
    
    public function store(Request $request)
    {
        $data = $request->all();
        
        $template = $request->except(['from_email','from_name','cc_email','cc_name','subject']);

        $email = $request->only(['from_email','from_name','cc_email','cc_name','subject']);

        // dd($email);
        try {

            DB::beginTransaction();

                $template['doc_cert'] = 0;
                $template['created_by'] = auth()->user()->id;

                $templates = Templates::create($template);

                $email['templates_id'] = $templates->id; 
                $templates->configemial()->create($email);

 
            DB::commit();
        }
        catch (\Throwable $e) {
            dd($e->getMessage());
            DB::rollback();
            //echo 'ERROR (' . $e->getCode() . '): ' . $e->getMessage();
            return redirect()->back()->with('flag', '
                VERIFIQUE QUE LA INFORMACIÓN INGRESADA SEA CORRECTA,
                DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. '.$e->getMessage());
        }

        $request->session()->flash('alert-success', 'Los datos se han agregado correctamente!!!');
        return redirect()->to('templates');

    }
    
    public function edit($id)
    {
        $record = Templates::where('id', $id)->first();

        $modulos = TemplatesModules::get();
 
        return View('templates.admin.edit', compact('record','modulos'));

    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        $template = $request->except(['from_email','from_name','cc_email','cc_name','subject']);

        $email = $request->only(['from_email','from_name','cc_email','cc_name','subject']);

        try {

            DB::beginTransaction(); 
            
                $templates = Templates::find($id);
                $templates->update($template);
                $email['templates_id'] = $templates->id; 
                $templates->configemial()->updateOrCreate([
                    'templates_id' => $email['templates_id']
                ],$email);

            DB::commit();
        }
        catch (\Throwable $e) {
            dd($e->getMessage());
            DB::rollback();
            //echo 'ERROR (' . $e->getCode() . '): ' . $e->getMessage();
            return redirect()->back()->with('flag', '
                VERIFIQUE QUE LA INFORMACIÓN INGRESADA SEA CORRECTA,
                DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. '.$e->getMessage());
        }

        $request->session()->flash('alert-success', 'Los datos se han actualizado correctamente!!!');
        return redirect()->to('templates');

    }
     
  
    public function edit_template($id)
    {
        $record = Templates::where('id', $id)->first();
  
        return View('templates.admin.edit_template', compact('record'));

    }
  
    public function template_saved(Request $request)
    {

        $record = TemplatesDescription::
        updateOrCreate([
            'id_template' => $request->id
        ],['format'=> $request->template, 'background'=> $request->background]);

        flash('Plantilla actualizada correctamente...');
        return redirect('/template/edit/'.$request->id)->with('success','Plantilla actualizada correctamente');
        
    }

    public function update_file(Request $request)
    {
        $file = $request->file;

        if (!empty($request->file)) {
            $filename = uniqid() . '-' .$file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            // $check = in_array($extension, $allowedfileExtension);
            
            // if ($check) {
                $request->file('file')->move('img/plantillas/', $filename);
                // $data['file'] = $filename;
            // }
        }
        return $this->path.$filename;
        

    }

    public function preview_template($id){
        
        $record = Templates::find($id);
       
        $format = isset($record->format)?$record->format->format:null;
        $background = isset($record->format)?$record->format->background:'#fff';
        
        $plantilla_final = $this->formatoWithRowsDinamic($format, null, $background);
         
        return View('templates.admin.preview', compact('record','plantilla_final'));
     
    }
    

    public function formatoWithRowsDinamic($plantilla_final, $user = null, $background = null){

        if(is_null($user)){
            if (!auth()->user()->employee){
                $user = User::inRandomOrder()->first();
                $employee = $user->employee;
            }else{
                $employee = auth()->user()->employee;
            }
        }else{
            $employee = $user->employee;
        }
        
        $camposDinamicos = TemplatesFields::get();

        foreach ($camposDinamicos as $key => $campos) {

            $label = '@'.$campos->label;
            //$validamos si existe el campo en la plantilla
            $existe = strpos($plantilla_final, $label);

            //si el campo/atributo existe empezamos a preguntar que vamos a reemplazar
            if ($existe !== false) {

                if($campos->type == 'attribute'){
  
                    $attribute = $campos->attribute;

                    $new_value_attribute =  $employee->$attribute();
                      
                }else if($campos->type == 'campo'){
  
                    $attribute = $campos->attribute;

                    $new_value_attribute =  $employee->$attribute;
                      
                }

                $plantilla_final = str_replace($label, $new_value_attribute, $plantilla_final);

            }

        }
        
        return '<div style="background:'.$background.'">'. $plantilla_final.'</div>';

    }
    
    public function send_email($id)
    {
         
        $record = Templates::find($id);
        $name_plantilla = $record->name;

        $data = null;
        $format = null;
        $view = "emails.clima.encontruccion";

        if (is_null($record->template_static)) { 
            if(isset($record->format)){
                $view = "emails.clima.format";
                $format = $this->formatoWithRowsDinamic($record->format->format,null,$record->background);
            }
        }else{
            $view = $record->template_static;  
        }

 
        $from_email = env('MAIL_FROM_ADDRESS', 'clima@soysepanka.com');
        $from_name = env('MAIL_FROM_NAME', 'NoReply');
        Mail::send($view, ['data' => $data,'format' => $format], function ($message) use ($from_email,$from_name,$name_plantilla) {
            $message->from($from_email, $from_name);
            $message->to('soporte@hallmg.com', 'Soporte');
            $message->subject($name_plantilla);
        }); 

        return redirect()->to('templates');

    }

    public function duplicated($id){
        
        
        try {

            DB::beginTransaction(); 
                
                $Templates = Templates::find($id);
            
                $newTemplates = $Templates->replicate();                
                $newTemplates->name = $Templates->name.' Copia';
                $newTemplates->created_at = Carbon::now();
                $newTemplates->save();
                
                if ($Templates->configemial) {
                    $configemial = $Templates->configemial;
                    $newTemplatesConfigemial = $configemial->replicate(); 
                    $newTemplatesConfigemial->templates_id = $newTemplates->id; 
                    $newTemplatesConfigemial->save();
                }

                if ($Templates->format) {
                    $format = $Templates->format;
                    $newTemplatesFormat = $format->replicate(); 
                    $newTemplatesFormat->id_template = $newTemplates->id; 
                    $newTemplatesConfigemial->save();
                }
                 


            DB::commit();
        }
        catch (\Throwable $e) {
            dd($e->getMessage());
            DB::rollback();
            //echo 'ERROR (' . $e->getCode() . '): ' . $e->getMessage();
            return redirect()->back()->with('flag', '
                VERIFIQUE QUE LA INFORMACIÓN INGRESADA SEA CORRECTA,
                DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. '.$e->getMessage());
        }


        return redirect()->to('templates');

    }

    public function destroy($id)
    {
        
        try {

            DB::beginTransaction(); 
                
                $Templates = Templates::find($id)->delete();
            
            DB::commit();
        }
        catch (\Throwable $e) {
            dd($e->getMessage());
            DB::rollback();
            //echo 'ERROR (' . $e->getCode() . '): ' . $e->getMessage();
            return redirect()->back()->with('flag', '
                VERIFIQUE QUE LA INFORMACIÓN INGRESADA SEA CORRECTA,
                DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. '.$e->getMessage());
        }


        return redirect()->to('templates');
    }


    public function send_correos_masivos(Request $request){
   
        $record = Templates::find($request->template_id);
        $name_plantilla = $record->name;
        $config_email =  $record->configemial;
  
        $exitosos = [];
        $errores = [];
        $novalidos = [];
        $data = null;
        $format = null;
        $view = "emails.clima.encontruccion";
  
   
        $r_users = [];
        $r_users = explode(',', $request->users);
        
        $calaboradores = [];
        if (!empty($r_users)){
            foreach ($r_users as $key => $value){
                $calaboradores[] = $value;
            }
        }
  
        // return $users;
        $id_user = auth()->user()->id;
  
        $users = User::whereIn('id', $calaboradores)->get();
      
  
      foreach ($users as $key => $user) { 

            $email  = $user->email;
            $emailB = filter_var($email, FILTER_SANITIZE_EMAIL);

            if (filter_var($emailB, FILTER_VALIDATE_EMAIL) === false ||
                $emailB != $email
            ) {
                $novalidos[] = $email; 
            }else{
                
                if (is_null($record->template_static)) { 
                    if(isset($record->format)){
                        $view = "emails.clima.format";
                        $format = $this->formatoWithRowsDinamic($record->format->format,$user,$record->background);
                    }
                }else{
                    $view = $record->template_static;  
                } 
                
                $from_email = env('MAIL_FROM_ADDRESS', 'clima@soysepanka.com');
                $from_name = env('MAIL_FROM_NAME', 'NoReply');
                Mail::send($view, ['data' => $data,'format' => $format], function ($message) use ($user,$config_email,$from_email,$from_name,$name_plantilla) {
                    $message->from($from_email, $from_name);
                    $message->to($user->email, $user->getFullNameAttribute());
                    if($config_email->cc_email){
                    $message->bcc($config_email->cc_email, $config_email->cc_name);
                    }
                    $message->subject($config_email->subject);
                }); 
        
        
                if (count(Mail::failures()) > 0) {
                $errores[] = $user->email.' / '.$user->getFullNameAttribute();
                }else{
                $exitosos[] = $user->email.' / '.$user->getFullNameAttribute();
                }
        
            }
        
        }

       
      $msg = "";
      $type = "success";
      if(count($errores) > 0){
          $msg .= "<span class='font-weight-bolder text-danger'>Hubo Problemas para enviar el correo a los siguientes colaboradores:</span>";
          $msg .= "<ul class='text-left font-weight-bolder text-danger'>";
          foreach ($errores as $error) {
              $msg .= "<li>{$error}</li>";
          }
          $msg .= "</ul><br><br>";
          $type = "danger";
      }
      if(count($exitosos) > 0){
          $msg .= "Correos Enviados Correctamente a los siguientes colaboradores:";
          $msg .= "<ul class='text-left'>";
          foreach ($exitosos as $exitoso) {
              $msg .= "<li>{$exitoso}</li>";
          }
          $msg .= "</ul>";
          $type = "success";
      }

      if(count($novalidos) > 0){
          $msg .= "Correos no son validos:";
          $msg .= "<ul class='text-left'>";
          foreach ($novalidos as $novalido) {
              $msg .= "<li>{$novalido}</li>";
          }
          $msg .= "</ul>";
          $type = "danger";
      }
      
      flash($msg, $type);
   
    //   return $msg;
      return redirect()->back();
    //   return redirect('/clima-organizacional/invitaciones-masivas');
   
    }

    public function send_correos_masivos_test(Request $request){
   
        $record = Templates::find($request->template_test_id);
        $name_plantilla = $record->name;
        $config_email =  $record->configemial;
  
        $email = $request->email_test;
        $exitosos = [];
        $errores = [];
        $data = null;
        $format = null;
        $view = "emails.clima.encontruccion";
  
      
        if (is_null($record->template_static)) { 
            if(isset($record->format)){
                $view = "emails.clima.format";
                $format = $this->formatoWithRowsDinamic($record->format->format,null,$record->background);
            }
        }else{
            $view = $record->template_static;  
        }

        
        
        $from_email = env('MAIL_FROM_ADDRESS', 'clima@soysepanka.com');
        $from_name = env('MAIL_FROM_NAME', 'NoReply');
        
        Mail::send($view, ['data' => $data,'format' => $format], function ($message) use ($email,$config_email,$from_email,$from_name,$name_plantilla) {
            $message->from($from_email, $from_name);
            $message->to($email, 'Prueba Envio de Correo');
            if($config_email->cc_email){
              $message->bcc($config_email->cc_email, $config_email->cc_name);
            }
            $message->subject($config_email->subject);
        }); 
  
   
      flash('Correo de Prueba Enviado Correctamente');
   
      return redirect()->back();
    //   return redirect('/clima-organizacional/invitaciones-masivas');
   
    }
    public function send_correos_masivos_test_old(Request $request)
    {
         
        $record = Templates::find($request->template_test_id);
        $name_plantilla = $record->name;

        $email = $request->email_test; 
        
        $data = null;
        $format = null;
        $view = "emails.clima.encontruccion";

        if (is_null($record->template_static)) { 
            if(isset($record->format)){
                $view = "emails.clima.format";
                $format = $this->formatoWithRowsDinamic($record->format->format,null,$record->background);
            }
        }else{
            $view = $record->template_static;  
        }

 
        $from_email = env('MAIL_FROM_ADDRESS', 'clima@soysepanka.com');
        $from_name = env('MAIL_FROM_NAME', 'NoReply');
        Mail::send($view, ['data' => $data,'format' => $format], function ($message) use ($from_email, $from_name,$email,$name_plantilla) {
            $message->from($from_email, $from_name);
            $message->to($email, 'Prueba Envio de Correo');
            $message->subject($name_plantilla);
        }); 

        flash('Correo de Prueba Enviado Correctamente');
        return redirect()->back();

    }



    
    public function guardar_plan_correos(Request $request){
        
        
        try {

            DB::beginTransaction();
  
                $modulos = TemplatesModules::whereCode($request->template_modules_id)->first();
                // return $request->all();
                $plan['template_modules_id'] = $modulos->id; 
                $plan['name'] = $request->name; 
                $plan['date'] = $request->date; 
                $plan['max_sends'] = $request->max_sends; 
                $plan['id_template'] = $request->id_template; 
                $plan['status'] = 1; 
                $crar_plan = TemplatesPlansSends::create($plan);
        
                $r_users = explode(',', $request->users);
                
                $calaboradores = [];
                if (!empty($r_users)){
                    foreach ($r_users as $key => $value){
                        $calaboradores[] = $value;
                    }
                }
        
                $users = User::whereIn('id', $calaboradores)->get();
            
                    foreach ($users as $key => $user) { 

                        $plan_detalle['plans_id'] = $crar_plan->id;
                        $plan_detalle['user_id'] = $user->id;                
                        $plan_detalle['email_veriquied'] = 1;

                        $email  = $user->email;
                        $emailB = filter_var($email, FILTER_SANITIZE_EMAIL);

                        if (filter_var($emailB, FILTER_VALIDATE_EMAIL) === false ||
                            $emailB != $email
                        ) {
                            $plan_detalle['email_veriquied'] = 0;
                        } 

                        $crar_plan_detalle = TemplatesPlansSendsDetails::create($plan_detalle);

                    }
        
                    DB::commit();
            }
            catch (\Throwable $e) {
                dd($e->getMessage());
                DB::rollback();
                //echo 'ERROR (' . $e->getCode() . '): ' . $e->getMessage();
                return redirect()->back()->with('flag', '
                    VERIFIQUE QUE LA INFORMACIÓN INGRESADA SEA CORRECTA,
                    DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. '.$e->getMessage());
            }
    
            $request->session()->flash('alert-success', 'Los datos se han agregado correctamente!!!');            
            return redirect()->back();
        
    }

    public function actualizar_plan_correos(Request $request){
        
        try {

            DB::beginTransaction();
  
                $plan['name'] = $request->name; 
                $plan['max_sends'] = $request->max_sends; 
                $plan['date'] = $request->date; 
                $plan['id_template'] = $request->id_template; 
                $crar_plan = TemplatesPlansSends::whereId($request->id)->update($plan);
        
                DB::commit();

            }
            catch (\Throwable $e) {
                dd($e->getMessage());
                DB::rollback();
                //echo 'ERROR (' . $e->getCode() . '): ' . $e->getMessage();
                return redirect()->back()->with('flag', '
                    VERIFIQUE QUE LA INFORMACIÓN INGRESADA SEA CORRECTA,
                    DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. '.$e->getMessage());
            }
    
            
            $request->session()->flash('alert-success', 'Los datos se han actualizados correctamente!!!');            
            return redirect()->back();
            
    }


    public function enviar_plan_correos($id){
   
 
        $plan = TemplatesPlansSends::find($id);

 
        $record = Templates::find($plan->id_template);
        $name_plantilla = $record->name;
        $config_email =  $record->configemial;
  
        $exitosos = [];
        $errores = [];
        $novalidos = [];
        $data = null;
        $format = null;
        $view = "emails.clima.encontruccion";
  

        $max_sends = count($plan->colaboradores);
        if(!empty($plan->max_sends)){
            $max_sends = $plan->max_sends;
        }
        $enviados = 0;
        foreach ($plan->colaboradores as $key => $colaborador) { 
            
            if($enviados < $max_sends ) {

                if($colaborador->status_send != 'enviado') {

                    $user = $colaborador->user;
                    
                    $colaborador->date_send = date('Y-m-d');
                    $colaborador->email_send = $user->email;
                    
                    $email  = $user->email;
                    $emailB = filter_var($email, FILTER_SANITIZE_EMAIL);

                    if (filter_var($emailB, FILTER_VALIDATE_EMAIL) === false ||
                        $emailB != $email
                    ) { 

                        $colaborador->email_veriquied = 0;
                        $colaborador->status_send = 'error';

                    }else{
                        
                        if (is_null($record->template_static)) { 
                            if(isset($record->format)){
                                $view = "emails.clima.format";
                                $format = $this->formatoWithRowsDinamic($record->format->format,$user,$record->background);
                            }
                        }else{
                            $view = $record->template_static;  
                        } 
                        
                        $from_email = env('MAIL_FROM_ADDRESS', 'clima@soysepanka.com');
                        $from_name = env('MAIL_FROM_NAME', 'NoReply');
                        Mail::send($view, ['data' => $data,'format' => $format], function ($message) use ($user,$config_email,$from_email,$from_name,$name_plantilla) {
                            $message->from($from_email, $from_name);
                            $message->to($user->email, $user->getFullNameAttribute());
                            if($config_email->cc_email){
                                $message->bcc($config_email->cc_email, $config_email->cc_name);
                            }
                            $message->subject($config_email->subject);
                        }); 
                
                        if (count(Mail::failures()) > 0) {
                            $colaborador->status_send = 'error';
                        }else{                     
                            $colaborador->status_send = 'enviado';
                        }

                    }
                
                    $colaborador->save();
        
                    $enviados++;
                } 
            } 

        }

        if(count($plan->colaboradores) == count($plan->enviados)){

            $plan->status = 2;

        }else{
             
            $plan->status = 3;

        }
        
        flash('Correos del Plan Enviados Correctamente');

        $plan->save();

  
      return redirect()->back();
    //   return redirect('/clima-organizacional/invitaciones-masivas');
   
    }

    public function ver_plan_correos($id){
   

        $plan = TemplatesPlansSends::find($id);

        foreach ($plan->colaboradores as $key => $colaborador) { 

            $user = $colaborador->user;
            
            $colaborador->email_send = $user->email;
            
            $email  = $user->email;
            $emailB = filter_var($email, FILTER_SANITIZE_EMAIL);

            if (filter_var($emailB, FILTER_VALIDATE_EMAIL) === false ||
                $emailB != $email
            ) { 

                $colaborador->email_veriquied = 0;
                $colaborador->status_send = 'error';

            }else{
                
                
                $colaborador->email_veriquied = 1;
                $colaborador->status_send = 'success';

            }
         
            $colaborador->save();
 
        }

        $plan->save();


 
        return View('clima-organizacional.templates.planes_envio_ver', compact('plan')); 
   
    }


}
