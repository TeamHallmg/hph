<?php

namespace App\Http\Controllers\AdminTemplate\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use App\Models\Templates\TemplatesFields;
use Illuminate\Support\Facades\Auth;

class TemplatesFieldsController extends Controller
{ 
    
    public function index()
    {

        $records = TemplatesFields::get();

        return View('templates.fields.index', compact('records'));

    }

    public function labels()
    {

        return TemplatesFields::get()->pluck('label');

    }

    public function create()
    {
        return View('templates.fields.create');

    }   
    
    public function store(Request $request)
    {
        $data = $request->all();
          
        // dd($email);
        try {

            DB::beginTransaction();
 
                $templates = TemplatesFields::create($data);
  
            DB::commit();
        }
        catch (\Throwable $e) {
            dd($e->getMessage());
            DB::rollback();
            //echo 'ERROR (' . $e->getCode() . '): ' . $e->getMessage();
            return redirect()->back()->with('flag', '
                VERIFIQUE QUE LA INFORMACIÓN INGRESADA SEA CORRECTA,
                DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. '.$e->getMessage());
        }

        $request->session()->flash('alert-success', 'Los datos se han agregado correctamente!!!');
        return redirect()->to('templates-fields');

    }
    
    public function edit($id)
    {
        $record = TemplatesFields::where('id', $id)->first();
  
        return View('templates.fields.edit', compact('record'));

    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        try {

            DB::beginTransaction(); 
            
                $templates = TemplatesFields::find($id);
                $templates->update($data);

            DB::commit();
        }
        catch (\Throwable $e) {
            dd($e->getMessage());
            DB::rollback();
            //echo 'ERROR (' . $e->getCode() . '): ' . $e->getMessage();
            return redirect()->back()->with('flag', '
                VERIFIQUE QUE LA INFORMACIÓN INGRESADA SEA CORRECTA,
                DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE. '.$e->getMessage());
        }

        $request->session()->flash('alert-success', 'Los datos se han actualizado correctamente!!!');
        return redirect()->to('templates-fields');

    }
     
}
