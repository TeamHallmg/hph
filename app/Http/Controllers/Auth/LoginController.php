<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Models\Profile\Profile;
use Illuminate\Http\Request;

use App\User;
use Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function redirectTo() {
        //if(!is_null(auth()->user()->external)) {
            /*$external = auth()->user()->external;
            $mail = auth()->user()->email;
            $profile = Profile::with('postulantes')->where('email', $mail)->first();*/
          return url('home');

            //return url('profile/0/'.$profile->id.'/'.$profile->postulantes[0]->vacante_id.'/consulta');*/
        /*} elseif (auth()->user()->role == 'employee') {
            return url('cuestionarios/evaluaciones');
        } else {
            return url('home');
        }*/
    }
    
}
