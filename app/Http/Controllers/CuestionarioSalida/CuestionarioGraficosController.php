<?php

namespace App\Http\Controllers\CuestionarioSalida;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\CuestionarioSalida\cuestionario_salida;

class CuestionarioGraficosController extends Controller
{
	public function index() {
		//obtenemos un listado de años ordenamos por el ultimo ingresado
		$anios = cuestionario_salida::select(DB::raw('YEAR(created_at) as year'))->distinct()->latest()->get();
		$anios = $anios->pluck('year');

		// ya que tenemos el ultimo año de registro, obtenemos los meses de ese año
		// array anios en la posicion 0 $anios[0]; ultimo año que se registro un movimiento
		$losmeses = cuestionario_salida::select('created_at')->whereYear('created_at', $anios[0])->oldest()->get()->toArray();
	
		// obtenemos los meses del ultimo año y los guardamos en un arreglo
		$meses = [];
		foreach ($losmeses as $mes) {
			$elMes = strtotime($mes['created_at']);
			$mess = date("n", $elMes);
			$meses[$mess] = $mess;
		}
		$meses = array_unique($meses);
		// dd($anios, $meses);

		$cuestionarios = cuestionario_salida::with('user')->whereYear('created_at', $anios[0])->get();
		foreach($cuestionarios as $cuestionario){
			// dd($id);
			$deptos[] = !is_null($cuestionario->user->employee) ? $cuestionario->user->employee->jobPosition->departments->name : '';
		}
		$deptos = array_filter(array_unique($deptos));

		foreach($cuestionarios as $cuestionario){
			// dd($id);
			$areas[] = !is_null($cuestionario->user->employee) ? $cuestionario->user->employee->jobPosition->area->name : '';
			// $sucursales[] = !is_null($cuestionario->user->employee) ? $cuestionario->user->employee->sucursal : '';
		}
		$areas = array_filter(array_unique($areas));
		// dd($areas);
		// se envian los valores de los meses del último año de registro
		// porque son los que se mostraran por default,
		// así como los meses de ese año
		return view('cuestionario-salida.graficos.index', compact('anios', 'meses', 'deptos', 'areas'));
	}

	public function fechas(Request $request){
		$name = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
		$data = $request->all();
		$anio = $data['anio'];

		$losmeses = cuestionario_salida::select('created_at')->whereYear('created_at', $anio)->oldest()->get()->toArray();
	
		$meses = [];
		foreach ($losmeses as $mes) {
			$elMes = strtotime($mes['created_at']);
			$mess = date("n", $elMes);
			$meses[$mess] = $name[$mess];
		}
		$meses = array_unique($meses);

		$cuestionarios = cuestionario_salida::with('user')->whereYear('created_at', $anio)->get();
		foreach($cuestionarios as $cuestionario){
			// dd($id);
			$deptos[] = !is_null($cuestionario->user->employee) ? $cuestionario->user->employee->jobPosition->departments->name : '';
		}
		$deptos = array_filter(array_unique($deptos));

		foreach($cuestionarios as $cuestionario){
			// dd($id);
			$areas[] = !is_null($cuestionario->user->employee) ? $cuestionario->user->employee->jobPosition->area->name : '';
			// $sucursales[] = !is_null($cuestionario->user->employee) ? $cuestionario->user->employee->sucursal : '';
		}
		$areas = array_filter(array_unique($areas));

		$datas = [
			'meses' => $meses,
			'deptos' => $deptos,
			'areas' => $areas,
		];
		return response()->json($datas);
	}

	public function graficos(Request $request) {
		$meses = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
		$data = $request->all();

		$anio = $data['anio'];
		$inicio = $data['inicio'];
		$fin = $data['fin'];
		$depto = $data['depto'];
		
		//Obtenemos categorias de x de las graficas
		$xCategorias = [];
		for($x = $inicio; $x <= $fin; $x++){
			$xCategorias[] = $meses[$x];
		}

		//creamos arreglos
		$problema_jefe_mes_cant = [];
		$piden_dinero_mes_cant = [];
		$recomendaria_mes_cant = [];
		$volveria_mes_cant = [];

		// problemas con jefe 
		for($j = $inicio; $j <= $fin; $j++){
			$fecha_inicio = $anio.'-'.$j.'-1';
			$fecha_fin = $anio.'-'.$j.'-31';
			
			// dd($fecha_inicio, $fecha_fin);
			// $problema_jefe_mes = cuestionario_salida::where('problema_con_jefe', 'Si')->whereBetween('created_at', [$fecha_inicio, $fecha_fin])->get();
			// $problema_jefe_mes_cant[] = $problema_jefe_mes->count();
			if(!is_null($depto)) {
				$problema_jefe_mes = cuestionario_salida::whereHas('user', function($query1) use($depto) {
					$query1->whereHas('employee', function($query2) use ($depto) {
						$query2->whereHas('jobPosition', function($query3) use ($depto) {
							$query3->whereHas('departments', function($query4) use ($depto) {
								$query4->where('name', $depto);
							});
						});
					});
				})->where('problema_con_jefe', 'Si')->whereBetween('created_at', [$fecha_inicio, $fecha_fin]);
				
				$problema_jefe_null = cuestionario_salida::whereHas('user', function($query1) use($depto) {
					$query1->whereHas('employee', function($query2) use ($depto) {
						$query2->whereHas('jobPosition', function($query3) use ($depto) {
							$query3->whereHas('departments', function($query4) use ($depto) {
								$query4->where('name', $depto);
							});
						});
					});
				})->whereNull('problema_con_jefe')->whereBetween('created_at', [$fecha_inicio, $fecha_fin])->get();
			} else {
				$problema_jefe_mes = cuestionario_salida::where('problema_con_jefe', 'Si')->whereBetween('created_at', [$fecha_inicio, $fecha_fin]);
				$problema_jefe_null = cuestionario_salida::whereNull('problema_con_jefe')->whereBetween('created_at', [$fecha_inicio, $fecha_fin])->get();
			}
			$problema_jefe_mes = $problema_jefe_mes->get();
			$problema_jefe_mes_cant[] = $problema_jefe_mes->count();
			$problema_jefe_null_cant[] = $problema_jefe_null->count();


			// $piden_dinero_mes = cuestionario_salida::where('piden_dinero', 'Si')->whereBetween('created_at', [$fecha_inicio, $fecha_fin])->get();
			// $piden_dinero_mes_cant[] = $piden_dinero_mes->count();
			if(!is_null($depto)) {
				$piden_dinero_mes = cuestionario_salida::whereHas('user', function($query1) use($depto) {
					$query1->whereHas('employee', function($query2) use ($depto) {
						$query2->whereHas('jobPosition', function($query3) use ($depto) {
							$query3->whereHas('departments', function($query4) use ($depto) {
								$query4->where('name', $depto);
							});
						});
					});
				})->where('piden_dinero', 'Si')->whereBetween('created_at', [$fecha_inicio, $fecha_fin]);

				$piden_dinero_null = cuestionario_salida::whereHas('user', function($query1) use($depto) {
					$query1->whereHas('employee', function($query2) use ($depto) {
						$query2->whereHas('jobPosition', function($query3) use ($depto) {
							$query3->whereHas('departments', function($query4) use ($depto) {
								$query4->where('name', $depto);
							});
						});
					});
				})->whereNull('piden_dinero')->whereBetween('created_at', [$fecha_inicio, $fecha_fin])->get();
			} else {
				$piden_dinero_mes = cuestionario_salida::where('piden_dinero', 'Si')->whereBetween('created_at', [$fecha_inicio, $fecha_fin]);
				$piden_dinero_null = cuestionario_salida::whereNull('piden_dinero')->whereBetween('created_at', [$fecha_inicio, $fecha_fin])->get();
			}
			$piden_dinero_mes = $piden_dinero_mes->get();
			$piden_dinero_mes_cant[] = $piden_dinero_mes->count();
			$piden_dinero_null_cant[] = $piden_dinero_null->count();


			// $recomendaria_mes = cuestionario_salida::where('recomendaria', 'Si')->whereBetween('created_at', [$fecha_inicio, $fecha_fin])->get();
			// $recomendaria_mes_cant[] = $recomendaria_mes->count();
			if(!is_null($depto)) {
				$recomendaria_mes = cuestionario_salida::whereHas('user', function($query1) use($depto) {
					$query1->whereHas('employee', function($query2) use ($depto) {
						$query2->whereHas('jobPosition', function($query3) use ($depto) {
							$query3->whereHas('departments', function($query4) use ($depto) {
								$query4->where('name', $depto);
							});
						});
					});
				})->where('recomendaria', 'Si')->whereBetween('created_at', [$fecha_inicio, $fecha_fin]);

				$recomendaria_null = cuestionario_salida::whereHas('user', function($query1) use($depto) {
					$query1->whereHas('employee', function($query2) use ($depto) {
						$query2->whereHas('jobPosition', function($query3) use ($depto) {
							$query3->whereHas('departments', function($query4) use ($depto) {
								$query4->where('name', $depto);
							});
						});
					});
				})->whereNull('recomendaria')->whereBetween('created_at', [$fecha_inicio, $fecha_fin])->get();
			} else {
				$recomendaria_mes = cuestionario_salida::where('recomendaria', 'Si')->whereBetween('created_at', [$fecha_inicio, $fecha_fin]);
				$recomendaria_null = cuestionario_salida::whereNull('recomendaria')->whereBetween('created_at', [$fecha_inicio, $fecha_fin])->get();
			}
			$recomendaria_mes = $recomendaria_mes->get();
			$recomendaria_mes_cant[] = $recomendaria_mes->count();
			$recomendaria_null_cant[] = $recomendaria_null->count();


			// $volveria_mes = cuestionario_salida::where('volveria_con_nosotros', 'Si')->whereBetween('created_at', [$fecha_inicio, $fecha_fin])->get();
			// $volveria_mes_cant[] = $volveria_mes->count();
			if(!is_null($depto)) {
				$volveria_mes = cuestionario_salida::whereHas('user', function($query1) use($depto) {
					$query1->whereHas('employee', function($query2) use ($depto) {
						$query2->whereHas('jobPosition', function($query3) use ($depto) {
							$query3->whereHas('departments', function($query4) use ($depto) {
								$query4->where('name', $depto);
							});
						});
					});
				})->where('volveria_con_nosotros', 'Si')->whereBetween('created_at', [$fecha_inicio, $fecha_fin]);

				$volveria_null = cuestionario_salida::whereHas('user', function($query1) use($depto) {
					$query1->whereHas('employee', function($query2) use ($depto) {
						$query2->whereHas('jobPosition', function($query3) use ($depto) {
							$query3->whereHas('departments', function($query4) use ($depto) {
								$query4->where('name', $depto);
							});
						});
					});
				})->whereNull('volveria_con_nosotros')->whereBetween('created_at', [$fecha_inicio, $fecha_fin])->get();
			} else {
				$volveria_mes = cuestionario_salida::where('volveria_con_nosotros', 'Si')->whereBetween('created_at', [$fecha_inicio, $fecha_fin]);
				$volveria_null = cuestionario_salida::whereNull('volveria_con_nosotros')->whereBetween('created_at', [$fecha_inicio, $fecha_fin])->get();
			}
			$volveria_mes = $volveria_mes->get();
			$volveria_mes_cant[] = $volveria_mes->count();
			$volveria_null_cant[] = $volveria_null->count();
		}
		$subtitulo = $meses[$inicio]. ' a ' .$meses[$fin]. ' de '.$anio;
		
		foreach($problema_jefe_null_cant as $key => $val){ 
			$val2 = $piden_dinero_null_cant[$key]; 
			$negativos_null[$key] = $val + $val2;
		}
		foreach($recomendaria_null_cant as $key => $val){ 
			$val2 = $volveria_null_cant[$key]; 
			$positivos_null[$key] = $val + $val2;
		}
		$datos = [
			'problema_jefe_cant' => $problema_jefe_mes_cant,
			'piden_dinero_cant' => $piden_dinero_mes_cant,
			'negativos_null' => $negativos_null,
			'recomendaria_cant' => $recomendaria_mes_cant,
			'volveria_cant' => $volveria_mes_cant,
			'positivos_null' => $positivos_null,
			'subtitulo' => $subtitulo,
			'xCategorias' => $xCategorias,
		];

		return response()->json($datos);
	}

	
	public function reporte(Request $request) {

		try {
			//code...
		
			$meses = [
				['name'=>'--TODOS--', 'id'=>''],
				['name'=>'Enero', 'id'=>'01'],
				['name'=>'Febrero', 'id'=>'02'],
				['name'=>'Marzo', 'id'=>'03'],
				['name'=>'Abril', 'id'=>'04'],
				['name'=>'Mayo', 'id'=>'05'],
				['name'=>'Junio', 'id'=>'06'],
				['name'=>'Julio', 'id'=>'07'],
				['name'=>'Agosto', 'id'=>'08'],
				['name'=>'Septiembre', 'id'=>'09'],
				['name'=>'Octubre', 'id'=>'10'],
				['name'=>'Noviembre', 'id'=>'11'],
				['name'=>'Diciembre', 'id'=>'12'],
			];

			$acumulados = [
				['name'=>'--TODOS--', 'id'=>''],
				['name'=>'Trimestre', 'id'=>date('Y').'-03-31'],
				['name'=>'Semestre', 'id'=>date('Y').'-06-30'],
				['name'=>'Anual', 'id'=>date('Y').'-12-31'],
			];

			$regiones = [];
			$puestos = [];
			$temp_dptos = [];
			$dptos = [];
			$temp_puestos = [];
			$temp_regiones = [];
			$bajas = 0;

			$contacto = [
				'Me contactaron'=> ['name'=>'Los contactaron', 'total'=>0, 'porc'=>0, 'color'=>'#a8cf45'],
				'Yo busqué trabajo'=> ['name'=>'Ellos buscaron', 'total'=>0, 'porc'=>0, 'color'=>'#5cc6d0'],
				'No Aplica'=> ['name'=>'No Aplica', 'total'=>0, 'porc'=>0, 'color'=>'#5cc6d0']
			];

			$motivos = [
				'Mejor puesto de trabajo'=> ['name'=>'Mejor puesto de trabajo', 'total'=>0, 'porc'=>0],
				'Ambiente laboral'=> ['name'=>'Ambiente laboral', 'total'=>0, 'porc'=>0],
				'Problemas con mi jefe inmediato'=> ['name'=>'Problemas con mi jefe inmediato', 'total'=>0, 'porc'=>0],
				'Mejor sueldo'=> ['name'=>'Mejor sueldo', 'total'=>0, 'porc'=>40],
				'Mejor prestaciones laborales'=> ['name'=>'Mejor prestaciones laborales', 'total'=>0, 'porc'=>0],
				'Cambio de residencia'=> ['name'=>'Cambio de residencia', 'total'=>0, 'porc'=>0],
				'Problemas familiares'=> ['name'=>'Problemas familiares', 'total'=>0, 'porc'=>0],
				'Carga de trabajo'=> ['name'=>'Carga de trabajo', 'total'=>0, 'porc'=>0],
				'Problemas de Salud'=> ['name'=>'Problemas de Salud', 'total'=>0, 'porc'=>0],
				'Otros'=> ['name'=>'Otros', 'total'=>0, 'porc'=>0],
			];

			$factores = [
				'desarrollo_profesional'=> ['id'=>1,'name'=>'Desarrollo profesional','niveles'=>[]],
				'relacion_companeros'=>  ['id'=>2,'name'=>'Relación con los compañeros','niveles'=>[]],
				'relacion_jefe'=>  ['id'=>3,'name'=>'Relación con mi jefe inmediato o portunamente','niveles'=>[]],
				'sueldo'=>  ['id'=>4,'name'=>'Sueldo','niveles'=>[]],
				'prestacion'=>  ['id'=>5,'name'=>'Prestaciones laborales','niveles'=>[]],
				'distacia'=>  ['id'=>6,'name'=>'Distancia casa-trabajo','niveles'=>[]],
				'carga'=>  ['id'=>7,'name'=>'Carga de trabajo','niveles'=>[]],
			];

			$nivel_influencia = [
				'gran_permanecer'=> ['name'=>'Gran influencia para quedarse','selected'=>0,'total'=>0],
				'ligera_permanecer'=> ['name'=>'Ligera influencia para quedarse','selected'=>0,'total'=>0],
				'nula'=> ['name'=>'Sin efecto','selected'=>0,'total'=>0],
				'ligera_irse'=> ['name'=>'Ligera influencia para irse','selected'=>0,'total'=>0],
				'gran_irse'=> ['name'=>'Gran influencia para irse','selected'=>0,'total'=>0],
			];
	
			$cuestionarios = cuestionario_salida::whereHas('user.employee_wt', function($q1) use($request){
				$q1->when(!is_null($request->puesto), function($q2) use($request){
					$q2->whereHas('jobPosition', function($q3) use($request){
						$q3->where('name', $request->puesto);
					}); 
				});
				$q1->when(!is_null($request->region), function($q2) use($request){
					$q2->where('sucursal', $request->region);
				});
				
				$q1->when(!is_null($request->dpto), function($q2) use($request){
					$q2->whereHas('jobPosition', function($q3) use($request){
					  $q3->whereHas('area', function($q4) use($request){
						$q4->whereHas('department', function($q5) use($request){
							$q5->where('name', $request->dpto);
						});
					  });
					}); 
				});  
			})->when(!is_null($request->acumulado), function($q2) use($request){
				$date_from = "2022-01-01";
				$date_to = $request->acumulado;
				$q2->whereBetween('created_at', [$date_from, $date_to]); 
			})
			->when(!is_null($request->mes), function($q2) use($request){		
				$q2->whereMonth('created_at', $request->mes)->whereYear('created_at', date('Y'));
			})->get(); 

			$users_ids = $cuestionarios->pluck('user_id');
 
			foreach ($cuestionarios as $key => $cuestionario) {
				if(!is_null($cuestionario->renuncia_mejora_puesto_sueldo)){
					$bajas++;
					$motivos[$cuestionario->renuncia_mejora_puesto_sueldo]['total'] += 1;
				}
				$contacto[$cuestionario->problema_con_jefe_decripcion]['total'] += 1;

				$user = $cuestionario->user;
 
				if(isset($user->employee_wt->jobPosition->area->department) && !in_array($user->employee_wt->jobPosition->area->department_id, $temp_dptos)){ 
					$temp_dptos[] = $user->employee_wt->jobPosition->area->department_id; 
					$dptos[] = ['id'=>$user->employee_wt->jobPosition->area->department->name,'name'=>$user->employee_wt->jobPosition->area->department->name]; 
				}

				if(!empty($user->employee_wt->jobPosition) && !in_array($user->employee_wt->jobPosition->name, $temp_puestos)){               
					$temp_puestos[] = $user->employee_wt->jobPosition->name;
					$puestos[] = ['id'=>$user->employee_wt->jobPosition->name,'name'=>$user->employee_wt->jobPosition->name]; 
				}  

				if(!empty($user->employee->sucursal) && !in_array($user->employee->sucursal, $temp_regiones)){
					$temp_regiones[] = $user->employee->sucursal;
					$regiones[] = ['id'=>$user->employee->sucursal,'name'=>$user->employee->sucursal];
				} 
				
			}
		
			foreach ($factores as $key_factor => $factor) {
					
				foreach ($nivel_influencia as $key_nivel => $nivel) {
					$total = 0;
					
					foreach ($cuestionarios as $key => $cuestionario) {
						if(isset($cuestionario->factores[$key_factor])  && $cuestionario->factores[$key_factor] == $key_nivel){
							$total += 1;
						}
					}
					
					$factores[$key_factor]['niveles'][$key_nivel] = $nivel;
					$factores[$key_factor]['niveles'][$key_nivel]['total'] = $total;

				}  
			} 

			
			$dptos = $this->sortArrayByKey($dptos, 'name'); 
			$puestos = $this->sortArrayByKey($puestos, 'name'); 
			$regiones = $this->sortArrayByKey($regiones, 'name'); 

			return response()->json([
				'users_ids'=>$users_ids,
				'meses'=>$meses,
				'dptos'=>$dptos,
				'acumulados'=>$acumulados,
				'regiones'=>$regiones,
				'puestos'=>$puestos,
				'motivos'=>$motivos,
				'contacto'=>$contacto,
				'factores'=>$factores,
				'bajas'=>$bajas,
				'request'=>$request->all(),
				'cuestionarios'=>$cuestionarios,
				'nivel_influencia'=>$nivel_influencia,   
				'factores'=>$factores,   
			]);
		} catch (\Throwable $th) {
			
			return response()->json([
				'mensage'=>$th->getMessage(),
			]);
		}

	}
	 
	private function sortArrayByKey($array,$key){
      
		$groupedItems = []; 
		$Items = []; 
		foreach ($array as $item) {
			$pool = $item[$key];
			$groupedItems[$pool][] = $item;
		}
		ksort($groupedItems);
	
		foreach ($groupedItems as $key => $value) {
		  $Items[] = $value[0];
		}
	
		return $Items;
	} 
}
