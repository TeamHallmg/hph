<?php

namespace App\Http\Controllers\CuestionarioSalida;

// use Barryvdh\DomPDF\PDF;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\CuestionarioSalida\cuestionario_salida;

class CuestionarioSalidaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cuestionarios = cuestionario_salida::get();

        return view('cuestionario-salida.index', compact('cuestionarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $motivos = [
            'mejor_puesto'=> 'Mejor puesto de trabajo',
            'ambiente_laboral'=> 'Ambiente laboral',
            'problemas_jefe_inmediato'=> 'Problemas con mi jefe inmediato',
            'mejor_sueldo'=> 'Mejor sueldo',
            'mejor_prestacion_laboral'=> 'Mejor prestaciones laborales',
            'cambio_residencia'=> 'Cambio de residencia',
            'problemas_familia'=> 'Problemas familiares',
            'carga_trabajo'=> 'Carga de trabajo',
            'problemas_salud'=> 'Problemas de Salud',
            'otros'=> 'Otros',
        ];

        $factores = [
            'desarrollo_profesional'=> ['name'=>'Desarrollo profesional','niveles'=>[]],
            'relacion_companeros'=>  ['name'=>'Relación con los compañeros','niveles'=>[]],
            'relacion_jefe'=>  ['name'=>'Relación con mi jefe inmediato o portunamente','niveles'=>[]],
            'sueldo'=>  ['name'=>'Sueldo','niveles'=>[]],
            'prestacion'=>  ['name'=>'Prestaciones laborales','niveles'=>[]],
            'distacia'=>  ['name'=>'Distancia casa-trabajo','niveles'=>[]],
            'carga'=>  ['name'=>'Carga de trabajo','niveles'=>[]],
        ];

        $nivel_influencia = [
            'gran_permanecer'=> 'Gran influencia para quedarse',
            'ligera_permanecer'=> 'Ligera influencia para quedarse',
            'nula'=> 'Sin efecto',
            'ligera_irse'=> 'Ligera influencia para irse',
            'gran_irse'=> 'Gran influencia para irse',
        ];


        foreach ($factores as $key => $value) {
             $factores[$key]['niveles'] = $nivel_influencia;
        } 

        return view('cuestionario-salida.create', compact('motivos','factores','nivel_influencia'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $data = $request->except('desarrollo_profesional','relacion_companeros','relacion_jefe','sueldo','prestacion','distacia','carga');
        $motivos = $request->only('desarrollo_profesional','relacion_companeros','relacion_jefe','sueldo','prestacion','distacia','carga');

        $data['estatus'] = 'Sin Revisar';
        $data['factores'] = $motivos;

        try {

            DB::beginTransaction();
                $cuestionario = cuestionario_salida::create($data);
            DB::commit();

            session()->flash('success', $cuestionario->id);

            return redirect()->back();

        } catch (\Throwable $th) {
            // dd($th);
            DB::rollback();
            session()->flash('error', 'No se pudo guardar la información, intenta más tarde.');
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CustionarioSalida\cuestionario_salida  $cuestionario_salida
     * @return \Illuminate\Http\Response
     */
    public function show(cuestionario_salida $cuestionario_salida)
    {
        
        $cuestionario = $cuestionario_salida;

        $motivos = [
            'mejor_puesto'=> 'Mejor puesto de trabajo',
            'ambiente_laboral'=> 'Ambiente laboral',
            'problemas_jefe_inmediato'=> 'Problemas con mi jefe inmediato',
            'mejor_sueldo'=> 'Mejor sueldo',
            'mejor_prestacion_laboral'=> 'Mejor prestaciones laborales',
            'cambio_residencia'=> 'Cambio de residencia',
            'problemas_familia'=> 'Problemas familiares',
            'carga_trabajo'=> 'Carga de trabajo',
            'problemas_salud'=> 'Problemas de Salud',
            'otros'=> 'Otros',
        ];

        $factores = [
            'desarrollo_profesional'=> ['name'=>'Desarrollo profesional','niveles'=>[]],
            'relacion_companeros'=>  ['name'=>'Relación con los compañeros','niveles'=>[]],
            'relacion_jefe'=>  ['name'=>'Relación con mi jefe inmediato o portunamente','niveles'=>[]],
            'sueldo'=>  ['name'=>'Sueldo','niveles'=>[]],
            'prestacion'=>  ['name'=>'Prestaciones laborales','niveles'=>[]],
            'distacia'=>  ['name'=>'Distancia casa-trabajo','niveles'=>[]],
            'carga'=>  ['name'=>'Carga de trabajo','niveles'=>[]],
        ];

        $nivel_influencia = [
            'gran_permanecer'=> ['name'=>'Gran influencia para quedarse','selected'=>0],
            'ligera_permanecer'=> ['name'=>'Ligera influencia para quedarse','selected'=>0],
            'nula'=> ['name'=>'Sin efecto','selected'=>0],
            'ligera_irse'=> ['name'=>'Ligera influencia para irse','selected'=>0],
            'gran_irse'=> ['name'=>'Gran influencia para irse','selected'=>0],
        ];
 
        foreach ($factores as $key => $value) {
            foreach ($nivel_influencia as $key_nivel => $nivel) {
                if($cuestionario->factores[$key] == $key_nivel){
                    $nivel['selected'] = 1;
                }
                $factores[$key]['niveles'][$key_nivel] = $nivel;
            } 
        } 
        $auth = $cuestionario->user;

        return view('cuestionario-salida.show', compact('auth','cuestionario','motivos','factores','nivel_influencia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CustionarioSalida\cuestionario_salida  $cuestionario_salida
     * @return \Illuminate\Http\Response
     */
    public function edit(cuestionario_salida $cuestionario_salida)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CustionarioSalida\cuestionario_salida  $cuestionario_salida
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, cuestionario_salida $cuestionario_salida)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CustionarioSalida\cuestionario_salida  $cuestionario_salida
     * @return \Illuminate\Http\Response
     */
    public function destroy(cuestionario_salida $cuestionario_salida)
    {
        //
    }

    public function updateEstatus (Request $request) {
        $data = $request->all();
        $id_user = auth()->user()->id;
        try {
            DB::beginTransaction();
                $insert = cuestionario_salida::where('id', $data['id'])->update(['user_validado'=>$id_user,'estatus'=>$data['estatus'],'renuncia_mejora_puesto_sueldo'=>$data['renuncia_mejora_puesto_sueldo']]);
            DB::commit();
            $cuestionario = cuestionario_salida::findOrFail($data['id']);

            return response()->json($cuestionario);
        } catch (\Throwable $th) {
            // throw $th;
            DB::rollback();
            return response()->json(['error' => 'Error msg'], 404);
        }
    }

    public function pdf($id) {
        $cuestionario = cuestionario_salida::findOrfail($id);

        $auth = $cuestionario->user;

        
        $motivos = [
            'mejor_puesto'=> 'Mejor puesto de trabajo',
            'ambiente_laboral'=> 'Ambiente laboral',
            'problemas_jefe_inmediato'=> 'Problemas con mi jefe inmediato',
            'mejor_sueldo'=> 'Mejor sueldo',
            'mejor_prestacion_laboral'=> 'Mejor prestaciones laborales',
            'cambio_residencia'=> 'Cambio de residencia',
            'problemas_familia'=> 'Problemas familiares',
            'carga_trabajo'=> 'Carga de trabajo',
            'problemas_salud'=> 'Problemas de Salud',
            'otros'=> 'Otros',
        ];

        $factores = [
            'desarrollo_profesional'=> ['id'=>1,'name'=>'Desarrollo profesional','niveles'=>[]],
            'relacion_companeros'=>  ['id'=>2,'name'=>'Relación con los compañeros','niveles'=>[]],
            'relacion_jefe'=>  ['id'=>3,'name'=>'Relación con mi jefe inmediato o portunamente','niveles'=>[]],
            'sueldo'=>  ['id'=>4,'name'=>'Sueldo','niveles'=>[]],
            'prestacion'=>  ['id'=>5,'name'=>'Prestaciones laborales','niveles'=>[]],
            'distacia'=>  ['id'=>6,'name'=>'Distancia casa-trabajo','niveles'=>[]],
            'carga'=>  ['id'=>7,'name'=>'Carga de trabajo','niveles'=>[]],
        ];

        $nivel_influencia = [
            'gran_permanecer'=> ['name'=>'Gran influencia para quedarse','selected'=>0],
            'ligera_permanecer'=> ['name'=>'Ligera influencia para quedarse','selected'=>0],
            'nula'=> ['name'=>'Sin efecto','selected'=>0],
            'ligera_irse'=> ['name'=>'Ligera influencia para irse','selected'=>0],
            'gran_irse'=> ['name'=>'Gran influencia para irse','selected'=>0],
        ];
 
        foreach ($factores as $key => $value) {
            foreach ($nivel_influencia as $key_nivel => $nivel) {
                if($cuestionario->factores[$key] == $key_nivel){
                    $nivel['selected'] = 1;
                }
                $factores[$key]['niveles'][$key_nivel] = $nivel;
            } 
        } 


        $pdf = PDF::loadView('cuestionario-salida.pdf', compact('cuestionario','auth','motivos','factores','nivel_influencia'));
        
        return $pdf->stream('cuestionario-'.$cuestionario->user->FullName.'.pdf');
    }
}
