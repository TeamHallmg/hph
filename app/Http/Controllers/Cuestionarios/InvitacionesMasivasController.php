<?php

namespace App\Http\Controllers\ClimaOrganizacional;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use App\Http\Requests;
use DB;
use Session;
use App\User;
use App\Http\Helpers\Templates;

use App\Models\ClimaOrganizacional\Period;

class InvitacionesMasivasController extends Controller
{
  
  protected $Templates;

  
  public function __construct(Templates $Templates){
      $this->Templates = $Templates;
  }

  // Muestra un listado con todos los empleados activos
  public function invitaciones_masivas(){

      $evaluados = array();
      $no_iniciados = $iniciados = $terminados = array();
      $period_id = 0;
      $periodos = Period::where('status', 'Abierto')->orWhere('status', 'Cerrado')->get();
      if (empty($periodos) || $periodos->isEmpty()){
          flash('No hay periodos disponibles por el momento...');
          return redirect('/clima-organizacional/que-es')->with('error','No tienes permiso para ver la evaluación');
      }

      if (!empty($_POST['period_id']) || Session::has('period_id')){

        if (!empty($_POST['period_id'])){

          $period_id = $_POST['period_id'];
          Session::put('period_id', $period_id);
        }

        else{

          $period_id = Session::get('period_id');
        }
      }

      else{

        $period_id = $periodos[0]->id;
      }
      

        
      if (!empty($_POST['status'])){

        $status = $_POST['status'];
        Session::put('status', $status);
      } else{

        if (Session::has('status')){ 
          $status = Session::get('status');
        } 
        else{ 
          $status = 'todos';
        }

      }
      
      $periodoAbierto = Period::find($period_id);
      $evaluados = $periodoAbierto
      ->users()
      ->with(['employee' => function($q) {
          $q->orderBy('nombre');
      }])
      ->get();
      // $evaluados = \DB::table('periodo_evaluados')->join('personal', 'personal.id', '=', 'periodo_evaluados.id_evaluado')->join('users', 'users.employee_id', '=', 'personal.id')->where('id_periodo', $periodoAbierto->id)->select('idempleado', 'email', 'nombre', 'paterno', 'materno', 'rfc', 'personal.departamento', 'personal.puesto', 'id_evaluado', 'estado')->orderBy('nombre')->toSql();                        
      
        if($status!='todos'){

          foreach ($evaluados as $key => $value) {
            
            if( $value->pivot->status != $status) {
              $evaluados->forget($key);
              continue;
            }

          }

        }

      $plantillas =  $this->Templates->TiposPlantillasPorModulos('clima');
    
      return view('clima-organizacional/invitaciones_masivas', compact('plantillas','evaluados', 'period_id', 'status', 'periodos'));
  }

}
