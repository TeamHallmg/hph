<?php

namespace App\Http\Controllers\Cuestionarios\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use DB;

use App\Models\Cuestionarios\Question;
use App\Models\Cuestionarios\Factor;

class PreguntasController extends Controller
{
	/**
     * Create a instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
		/*$this->middleware('permission:see_questions')->only('index');
        $this->middleware('permission:create_questions')->only(['create', 'store']);
        $this->middleware('permission:edit_questions')->only(['edit', 'update']);
        $this->middleware('permission:erase_questions')->only(['destroy']);*/
    }
    
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(){
		$preguntas = Question::with('factor')->get();
		// $preguntas = DB::table('preguntas')->leftJoin('factores', 'preguntas.id_factor', '=', 'factores.id')->select('preguntas.id', 'pregunta', 'nombre', 'positiva')->get();
		return view('cuestionarios/Admin/preguntas/index', compact('preguntas'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(){

		return view('cuestionarios.Admin.preguntas.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request){
		try {
			Question::create([
				'question' => $request->question,
				'positive' => (empty($request->positiva))?0:$request->positive,
			]);
			// Flash::success('La pregunta fue guardada correctamente.');
		} catch (\Throwable $th) {
			dd($th->getMessage());
			// Flash::success('La pregunta fue guardada correctamente.');
			//throw $th;
		}
		return redirect('cuestionarios/preguntas');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id){
		$pregunta = Question::findOrFail($id);
		return view('cuestionarios.Admin.preguntas.edit', compact('pregunta'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id){
		try {
			Question::where('id', $id)->update([
				'question' => $request->question,
				'positive' => (empty($request->positive))?0:$request->positive,
			]);
			// Flash::success('La pregunta fue guardada correctamente.');
		} catch (\Throwable $th) {
			dd($th->getMessage());
			// Flash::success('La pregunta fue guardada correctamente.');
			//throw $th;
		}
		return redirect('cuestionarios/preguntas');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
