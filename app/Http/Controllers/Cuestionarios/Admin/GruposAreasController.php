<?php

namespace App\Http\Controllers\ClimaOrganizacional\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ClimaOrganizacional\Admin\Areas;
use App\Models\ClimaOrganizacional\Admin\AreasGruposAreas;
use App\Models\ClimaOrganizacional\Admin\GruposAreas;

class GruposAreasController extends Controller{

  /**
  * Create a instance.
  *
  * @return void
  */
  public function __construct(){

    $this->middleware('auth');
  }
    
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(){

    $gruposAreas = GruposAreas::select('id', 'nombre')->get();

    foreach($gruposAreas as $gr){
      $areas_gruposAreas[] = AreasGruposAreas::AreaGrupoArea($gr->id)->get();
    }

    return view('clima-organizacional/Admin/grupo-areas/index', compact('areas_gruposAreas', 'gruposAreas'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create(){

    $areas = Areas::all();
    $otrasAreas = AreasGruposAreas::where('id_grupos_areas', '=', 1)->get();
    return view('clima-organizacional/Admin/grupo-areas/add', compact('otrasAreas'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request){

    $gArea = GruposAreas::select('id')->where('nombre', 'LIKE', $request->nombreGrupoArea)->exists();

    if ($gArea){

      return view('evaluacion-desempeno/Admin/grupo-areas/index')->with('error','Este registro ya existe');
    }else{
          
      GruposAreas::create([
        'nombre' => $request->nombreGrupoArea
      ]);
    }

    $grupoArea = GruposAreas::select('id')->where('nombre', 'LIKE', $request->nombreGrupoArea)->first();
    $checks = $request->id_areas;

    foreach($checks as $check){
      
      AreasGruposAreas::where('id_areas', '=', $check)->update([
        'id_areas' => $check,
        'id_grupos_areas' => $grupoArea->id
      ]);
    }

    return redirect('/areas')->with('success','Nombre del Grupo y Areas registrado satisfactoriamente.');
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id){

    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id){

    $idGrupoAreas = GruposAreas::where('id', '=', $id)->get();
    $nombreGrupoAreas = '';

    foreach ($idGrupoAreas as $idGA){
        
      $nombreGrupoAreas = $idGA->nombre;
    }
      
    $areas = Areas::all();

    $areasGruposAreas = AreasGruposAreas::where('id_grupos_areas', '=', $id)->get();

    $areasOtros = AreasGruposAreas::where('id_grupos_areas', '=', 1)->get();
    $selected = $selectedOtros = array();

    foreach ($areas as $a){ 
        
      foreach ($areasGruposAreas as $gra){
          
        if ($a->id == $gra->id_areas){
          
          $selected[] = $gra->id_areas;
        } 
      }
    }

    foreach ($areas as $a){ 
        
      foreach ($areasOtros as $ao){
          
        if ($a->id == $ao->id_areas){
            
          $selectedOtros[] = $ao->id_areas;
        } 
      }
    }

    return view('clima-organizacional/Admin/grupo-areas/edit', compact('areas', 'nombreGrupoAreas', 'selected', 'id', 'selectedOtros', 'areasOtros', 'areasGruposAreas'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request){

    $id = $request->id;
    $checksUpdate = $request->id_areas;

    if (is_null($checksUpdate) || empty($checksUpdate)){
          
      return redirect('/areas/' . $id . '/edit')->with('error','Debes seleccionar al menos un área.');

      return view('clima-organizacional/Admin/grupo-areas/edit', compact('areas', 'nombreGrupoAreas', 'selected', 'id', 'areasGruposAreas', 'areasOtros', 'selectedOtros'));
    }

    $nombreGruAre = GruposAreas::find($id);

    $nombreGruAre->nombre = $request->nombreGrupoArea;

    $nombreGruAre->save();

    $checksUpdate = $request->id_areas;

    $selDel = AreasGruposAreas::where('id_grupos_areas', '=', $id)->get();

    $selAdd = array();
    $delSel = array();
        
    foreach ($checksUpdate as $checkUp){

      $exist = AreasGruposAreas::where('id_areas', '=', $checkUp)->where('id_grupos_areas', '=', $id)->first();
          
      if (!$exist){
            
        $selAdd[] = $checkUp;
      }
    }

    for ($i = 0; $i < count($selDel); $i++){
          
      if (!in_array($selDel[$i]['id_areas'], $checksUpdate)){
            
        $delSel[] = $selDel[$i]['id_areas'];
      }
    }

    if (!empty($selAdd)){
          
      foreach ($selAdd as $ad){
            
        AreasGruposAreas::where('id_areas', '=', $ad)->update(['id_grupos_areas' => $id]);
      }
    }

    if (!empty($delSel)){
          
      foreach ($delSel as $del){
            
        AreasGruposAreas::where('id_areas', '=', $del)->update(['id_grupos_areas' => 1]);
      }
    }

    return redirect()->to('areas');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id){

    /*$grupoArea = GruposAreas::where('id', '=', $id)->delete();
    $areaGrupoArea = AreasGruposAreas::where('id_grupos_areas', '=', $id)->delete();

    return redirect()->to('areas');*/
  }
}
