<?php
namespace App\Http\Controllers\Cuestionarios\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use DB;

use App\Models\Cuestionarios\Period;
use App\Models\Cuestionarios\Question;
use App\User;

class PeriodosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        /*$this->middleware('permission:see_periods')->only('index');
        $this->middleware('permission:create_periods')->only(['create', 'store']);
        $this->middleware('permission:edit_periods')->only(['edit', 'update']);
        $this->middleware('permission:erase_periods')->only(['destroy']);*/
    }

    /**
     * Muestra los periodos
     *
     */
    public function index(){

      // Obtiene todos los periodos
      $periodos = Period::get();
      return view('cuestionarios.Admin.periodos.index', compact('periodos'));
    }

    /**
     * Crea un nuevo periodo
     *
     */
    public function create(){
        $abierto = false;
        // Obtiene los periodos con estado Abierto
        $periodos = Period::where('status', 'Abierto')->get();

      // Hay periodos con estado Abierto
        if (!empty($periodos)){
            $abierto = true;
        }
        $preguntas = Question::orderBy('id')->get();
        $evaluados = User::has('employee')->orderBy('first_name')->get();
        $enterprises = DB::table('enterprises')->orderBy('name')->get();
        return view('cuestionarios/Admin/periodos/create', compact('preguntas', 'evaluados', 'abierto', 'enterprises'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (empty($request->no_aplica)){
          $request->no_aplica = 0;
        }
        \DB::beginTransaction();
        try {
            $result = Period::create([
                'name' => $request->name,
                'status' => $request->status,
                'enterprise_id' => $request->enterprise_id,
                'no_aplica' => $request->no_aplica
            ]);
        } catch (\Throwable $th) {
            \DB::rollback();
            return redirect('/cuestionarios/periodos')->with('error','El periodo fue creado correctamente');
        }
        if (!empty($request->questions)){
            foreach ($request->questions as $key => $value){
                try {
                    $result->questions()->attach($value);
                } catch (\Throwable $th) {
                    \DB::rollback();
                    dd($result, $th->getMessage());
                    return redirect('/cuestionarios/periodos')->with('error','El periodo fue creado correctamente');
                }                
            }
        }

        if (!empty($request->users)){
            foreach ($request->users as $key => $value){
                try {
                    $result->users()->attach($value, ['status' => 1]);
                } catch (\Throwable $th) {
                    \DB::rollback();
                    dd($th->getMessage());
                    return redirect('/cuestionarios/periodos')->with('error','El periodo fue creado correctamente');
                }
            }
        }
        \DB::commit();
        return redirect('/cuestionarios/periodos')->with('success','El periodo fue creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Edita o actualiza un periodo
     *
     */
    public function edit($id){    
        $periodo = Period::with('questions', 'users')->findOrFail($id);

        $periodo_abierto = ($periodo->status === "Abierto")?true:false;
        $preguntas = Question::orderBy('id')->get();
        $evaluados = User::with('employee')->has('employee')->orderBy('first_name')->get();

        $preguntas_periodo = $periodo->questions->pluck('id')->toArray();
        $evaluados_periodo = $periodo->users->pluck('id')->toArray();
        $enterprises = DB::table('enterprises')->orderBy('name')->get();
        return view('cuestionarios/Admin/periodos/edit', compact('preguntas', 'evaluados', 'periodo_abierto', 'periodo', 'preguntas_periodo', 'evaluados_periodo', 'enterprises'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $periodo_a_editar = Period::findOrFail($id);
        if (empty($request->no_aplica)){
          $request->no_aplica = 0;
        }
        try {
            $periodo = Period::find($id);
            $periodo->update([
                'name' => $request->name,
                'status' => $request->status,
                'enterprise_id' => $request->enterprise_id,
                'no_aplica' => $request->no_aplica
            ]);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        if ($periodo_a_editar->status === "Preparatorio"){
            try {
                $periodo->questions()->sync($request->questions);
                $periodo->users()->sync($request->users);
            } catch (\Throwable $th) {
                \DB::rollback();
                dd($th->getMessage());
            }
        }
        \DB::commit();
        return redirect('/cuestionarios/periodos')->with('success','El periodo fue guardado correctamente');;
    }

   /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = 0){
      
      if (!empty($_POST['id'])){

        $periodo = DB::delete('DELETE FROM cuestionarios_periods WHERE id = ?', [$_POST['id']]);
        DB::table('cuestionarios_periods')->where('id', $_POST['id'])->delete();
        DB::table('cuestionarios_questions')->where('period_id', $_POST['id'])->delete();
        DB::table('cuestionarios_users')->where('period_id', $_POST['id'])->delete();
        DB::table('cuestionarios_answers')->where('period_id', $_POST['id'])->delete();
        return redirect('/cuestionarios/periodos')->with('success','El periodo fue borrado correctamente');
      }

      $periodo = DB::table('cuestionarios_periods')->where('id', $id)->first();
      return view('cuestionarios/Admin/periodos/delete', compact('periodo'));
    }
}
