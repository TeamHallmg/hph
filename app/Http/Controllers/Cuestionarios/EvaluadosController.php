<?php

namespace App\Http\Controllers\Cuestionarios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Employee;
use App\Models\JobPosition;
use App\Models\Area;
use App\Models\Department;
use App\Models\Direction;
use App\Models\ClimaOrganizacional\Factor;
use App\Models\ClimaOrganizacional\Period;
use App\Models\Cuestionarios\Answer;
use App\Models\Cuestionarios\AreaPivot;
use App\Models\Cuestionarios\DepartmentPivot;
use App\Models\Cuestionarios\JobPositionPivot;
use App\Models\Cuestionarios\GroupArea;
use App\Models\Cuestionarios\GroupDepartment;
use App\Models\Cuestionarios\GroupJobPosition;
use App\Models\Announcement\Announcement;
use App\Models\Announcement\AnnouncementType;
use App\Models\Announcement\AnnouncementCategory;
use App\Models\Announcement\View;
use DB;

class EvaluadosController extends Controller
{
    /**
     * Create a new Evaluacion Desempeno controller instance.
     *
     * @return void2
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:see_progress')->only('avances');
    }

    // Que es Clima Organizacional
    public function que_es_cuestionarios(){
      $url = $_SERVER['REQUEST_URI'];
      $url = substr($url,1);
       $view = View::where('name', $url)->first();
      // $slick = Announcement::getAnnouncementsToDisplay($view->id, 'banner');
      $evaluacion = Announcement::getAnnouncementsToDisplay($view->id, 'evaluacion');
      $display_announcements = compact('evaluacion');
        return view('cuestionarios/que-es-cuestionarios', compact('display_announcements'));
    }

    // Listado de las personas que va a evaluar al que esta logueado
    public function avances(){
        $evaluados = array();
        $no_iniciados = $iniciados = $terminados = array();
        $periodoStatus = Period::where('status', 'Abierto')->get();
        if (empty($periodoStatus) || $periodoStatus->isEmpty()){
            flash('No hay periodos abiertos por el momento...');
            return redirect('/cuestionarios/que-es')->with('error','No tienes permiso para ver la evaluación');
        }
        
        $periodoAbierto = Period::where('status', 'Abierto')->first();

        $evaluados = $periodoAbierto
        ->users()
        ->with(['employee' => function($q) {
            $q->orderBy('nombre');
        }])
        ->get();
        // $evaluados = \DB::table('periodo_evaluados')->join('personal', 'personal.id', '=', 'periodo_evaluados.id_evaluado')->join('users', 'users.employee_id', '=', 'personal.id')->where('id_periodo', $periodoAbierto->id)->select('idempleado', 'email', 'nombre', 'paterno', 'materno', 'rfc', 'personal.departamento', 'personal.puesto', 'id_evaluado', 'estado')->orderBy('nombre')->toSql();                        
        
        $no_iniciados = $periodoAbierto->users()
        ->wherePivot('status', 1)
        ->get();
        // $no_iniciados = DB::table('periodo_evaluados')->where('id_periodo', $periodoAbierto->id)->where('estado', 1)->select('id_evaluado')->get();
        $iniciados = $periodoAbierto->users()
        ->wherePivot('status', 2)
        ->get();
        // $iniciados = DB::table('periodo_evaluados')->where('id_periodo', $periodoAbierto->id)->where('estado', 2)->select('id_evaluado')->get();
        $terminados = $periodoAbierto->users()
        ->wherePivot('status', 3)
        ->get();
        // $terminados = DB::table('periodo_evaluados')->where('id_periodo', $periodoAbierto->id)->where('estado', 3)->select('id_evaluado')->get();
        $group_areas = GroupArea::where('period_id', $periodoAbierto->id)->get();
        $total_questions = DB::table('clima_period_question')->where('period_id', $periodoAbierto->id)->count();
        $total_answers_by_user = DB::table('clima_answers')->where('period_id', $periodoAbierto->id)->select(DB::raw('COUNT(answer) AS total_answers'), 'user_id')->groupBy('user_id')->pluck('total_answers', 'user_id')->toArray();
        return view('cuestionarios/avances', compact('evaluados', 'periodoStatus', 'no_iniciados', 'iniciados', 'terminados', 'group_areas', 'total_questions', 'total_answers_by_user'));
    }
    
    public function evaluaciones(){
        $evaluado_nps = $evaluado_sociograma = $evaluado_clima = array();
        $nps_questions = $nps_answers = $nps_why = $sociograma_questions = $sociograma_answers = $etiquetas = array();
        $closed_questions = $opened_questions = $users = $sociograma_users = array();
        $no_aplica = false;
        $periodoStatus_nps = DB::table('nps_periods')->where('status', 'Abierto')->get();
        $periodoStatus_sociograma = DB::table('sociograma_periods')->where('status', 'Abierto')->get();
        $periodoStatus_clima = DB::table('clima_periods')->where('status', 'Abierto')->get();
        $status = array();
        if ((count($periodoStatus_nps) == 0 || $periodoStatus_nps->isEmpty()) && (count($periodoStatus_sociograma) == 0 || $periodoStatus_sociograma->isEmpty()) && (count($periodoStatus_clima) == 0 || $periodoStatus_clima->isEmpty())){
          flash('No cuenta con cuestionarios por el momento');
            return redirect('/home')->with('error','No tienes permiso para ver la evaluación');
        }
        $periodoAbierto_clima = Period::where('status', 'Abierto')->pluck('id')->toArray();
        $periodoAbierto_nps = DB::table('nps_periods')->where('status', 'Abierto')->pluck('id')->toArray();
        $periodoAbierto_sociograma = DB::table('sociograma_periods')->where('status', 'Abierto')->pluck('id')->toArray();

        if (!empty($periodoAbierto_nps)){

          $evaluado_nps = DB::table('nps_period_user')->whereIn('period_id', $periodoAbierto_nps)->where('user_id', Auth::user()->id)->first();

          if (!empty($evaluado_nps)){

            $nps_answers = DB::table('nps_answers')->where('period_id', $evaluado_nps->period_id)->where('user_id', Auth::user()->id)->get();

            if (count($nps_answers) > 0){

              $temp = $temp2 = array();

              foreach ($nps_answers as $key => $value) {
                
                $temp[$value->question_id] = $value->answer;
                $temp2[$value->question_id] = $value->porque;
              }

              $nps_answers = $temp;
              $nps_why = $temp2;
            }

            $nps_questions = DB::table('nps_period_question')->join('nps_questions', 'nps_questions.id', '=', 'nps_period_question.question_id')->where('period_id', $evaluado_nps->period_id)->select('question_id', 'question')->get();
          }
        }

        if (!empty($periodoAbierto_sociograma)){

          $evaluado_sociograma = DB::table('sociograma_period_user')->whereIn('period_id', $periodoAbierto_sociograma)->where('user_id', Auth::user()->id)->first();

          if (!empty($evaluado_sociograma)){

            $sociograma_answers = DB::table('sociograma_answers')->where('period_id', $evaluado_sociograma->period_id)->where('user_id', Auth::user()->id)->orderBy('question_id')->get();
            $users = DB::table('users')->join('employees', 'employees.id', '=', 'users.employee_id')->whereNull('users.deleted_at')->where('users.id', '<>', Auth::user()->id)->where('region_id', Auth::user()->employee->region_id)->select('users.id AS data', DB::raw('CONCAT(first_name, " ", last_name) AS value'))->orderBy('first_name')->get();
            $sociograma_users = DB::table('users')->where('id', '<>', Auth::user()->id)->select('id', DB::raw('CONCAT(first_name, " ", last_name) AS name'))->pluck('name', 'id')->toArray();

            if (count($sociograma_answers) > 0){

              $temp = $temp2 = array();
              $current_question = 0;

              foreach ($sociograma_answers as $key => $value){

                if ($value->question_id != $current_question){

                  $current_question = $value->question_id;
                  $temp[$current_question] = array();
                }

                $temp[$current_question][] = $value->answer;
              }

              $sociograma_answers = $temp;
            }

            $sociograma_questions = DB::table('sociograma_period_question')->join('sociograma_questions', 'sociograma_questions.id', '=', 'sociograma_period_question.question_id')->where('period_id', $evaluado_sociograma->period_id)->select('question_id', 'question')->get();

            if (count($sociograma_questions) > 0 && count($users) > 0){

              $temp = array();
              $temps = array(0);

              foreach ($sociograma_questions as $key => $value){

                $temp = array();
                $counter = 0;

                foreach ($users as $key2 => $value2){
                
                  $temp[$counter] = new \stdClass();
                  $temp[$counter]->data = $value2->data;
                  $temp[$counter]->value = $value2->value;
                  $temp[$counter]->index = $key + 1;
                  $counter++;
                }

                $temps[] = $temp;
              }

              $users = $temps;
            }
          }
        }

        if (!empty($periodoAbierto_clima)){

          $evaluado_clima = DB::table('clima_period_user')->whereIn('period_id', $periodoAbierto_clima)->where('user_id', Auth::user()->id)->first();

          if ($periodoAbierto_clima->no_aplica == 1){

            $no_aplica = true;
          }

          $etiquetas = DB::table('clima_etiquetas')->orderBy('valor')->get();

          if (!empty($evaluado_clima)){

            $status = $evaluado_clima;
            $closed_questions = Factor::with([
            'questions' => function($q){
                $q->has('periods');
            },
            'questions.answer' => function($q) use($periodoAbierto_clima, $evaluado_clima){
                $q->where('period_id', $evaluado_clima->period_id)
                ->where('user_id', $evaluado_clima->user_id);
            }
            ])
            ->has('questions.periods')->get();

            $opened_questions = $periodoAbierto_clima->questions()
            ->with([
            'answer' => function($q) use($periodoAbierto_clima, $evaluado_clima){
                $q->where('period_id', $evaluado_clima->period_id)
                ->where('user_id', $evaluado_clima->user_id);
            }
            ])
            ->whereNull('factor_id')->get();
          }
        }

        // $evaluados = \DB::table('periodo_evaluados')->where('id_evaluado', Auth::user()->id)->where('id_periodo', $periodoAbierto->id)->get();                    
        if (empty($evaluado_clima) && empty($evaluado_sociograma) && empty($evaluado_nps)){
          flash('No cuenta con cuestionarios por el momento');
            return redirect('/home')->with('error','No tienes permiso para ver la evaluación');
        }
        // $evaluados[0]->nombre = $user->nombre;
        // $evaluados[0]->paterno = $user->paterno;
        // $evaluados[0]->materno = $user->materno;

        return view('cuestionarios/evaluaciones', compact('evaluado_clima', 'evaluado_nps', 'evaluado_sociograma', 'nps_questions', 'nps_answers', 'nps_why', 'sociograma_questions', 'sociograma_answers', 'opened_questions', 'closed_questions', 'no_aplica', 'etiquetas', 'status', 'users', 'sociograma_users'));
    }

        // Formato de Evaluaci�n
    public function formatoEvaluacion(){        
        $evaluado = Auth::user();
        $no_aplica = false;
        $periodo = Period::where('status', 'Abierto')
        ->select('id', 'no_aplica')
        ->orderBy('id','DESC')
        ->firstOrFail();
        $status = DB::table('clima_period_user')->where('period_id', $periodo->id)->where('user_id', $evaluado->id)->first();

        if ($periodo->no_aplica == 1){

          $no_aplica = true;
        }

        /*if ($status->status == 3){

          flash('La encuesta ya fue terminada');
          return redirect('/cuestionarios/que-es');
        }*/

        $closed_questions = Factor::with([
            'questions' => function($q){
                $q->has('periods');
            },
            'questions.answer' => function($q) use($periodo, $evaluado){
                $q->where('period_id', $periodo->id)
                ->where('user_id', $evaluado->id);
            }
        ])
        ->has('questions.periods')->get();

        // dd($closed_questions);

        $opened_questions = $periodo->questions()
        ->with([
            'answer' => function($q) use($periodo, $evaluado){
                $q->where('period_id', $periodo->id)
                ->where('user_id', $evaluado->id);
            }
        ])
        ->whereNull('factor_id')->get();

        // dd($opened_questions);

        // dd($closed_questions, $opened_questions);
        $etiquetas = DB::table('clima_etiquetas')->orderBy('valor')->get();
        return view('cuestionarios/formato-evaluacion', compact('closed_questions', 'opened_questions', 'status', 'etiquetas', 'no_aplica'));
    }

        /**
         * Guarda una respuesta del formato de evaluacion
         *
         * @return void
         */
        public function guardar_respuesta(){

        $user = Auth::user();

        // Id del evaluado
        $id_evaluado = $user->id;
            
        $periodos = DB::table('clima_periods')->where('status', 'Abierto')->select('id')->get();
            $id_periodo = 0;
            if (count($periodos) > 0){
                $id_periodo = $periodos[0]->id;
            $respuesta = $_POST['respuesta'];
            $id_pregunta = $_POST['id_pregunta'];
            //$total_preguntas = $_POST['total_preguntas'];
            $total_preguntas = DB::table('clima_period_question')->where('period_id', $id_periodo)->count();

            // Busca si la respuesta ya existe
            $resultado = DB::table('clima_answers')->where('user_id', $id_evaluado)->where('question_id', $id_pregunta)->where('period_id', $id_periodo)->first();
            $respuestas = array();
            $respuestas['answer'] = $respuesta;
            
            if (!empty($resultado)){

            $respuestas['updated_at'] = date('Y-m-d H:i:s');

            // Actualiza el resultado
            DB::table('clima_answers')->where('user_id', $id_evaluado)->where('period_id', $id_periodo)->where('question_id', $id_pregunta)->update($respuestas);
            }

            else{

            $respuestas['user_id'] = $id_evaluado;
            $respuestas['question_id'] = $id_pregunta;
            $respuestas['period_id'] = $id_periodo;
            $respuestas['updated_at'] = date('Y-m-d H:i:s');

            // Agrega el resultado
            DB::table('clima_answers')->insert($respuestas);
            }

            $resultados = DB::table('clima_answers')->where('user_id', $id_evaluado)->where('period_id', $id_periodo)->get();
            $periodo_evaluado = array();

            if ($total_preguntas == count($resultados)){

            $periodo_evaluado['status'] = 3;
            }

            else{

            $periodo_evaluado['status'] = 2;
            }

            // Actualiza el status
            DB::table('clima_period_user')->where('user_id', $id_evaluado)->where('period_id', $id_periodo)->update($periodo_evaluado);
        }
    }

    /**
     * Guarda todas las respuestas abiertas
     *
     * @return void
     */
    public function guardar_respuestas(Request $request){
        // dd($request->all());
        $periodo = Period::where('status', 'Abierto')->orderBy('id', 'DESC')->firstOrFail();
        $user = Auth::user();
        $answer_to_update = array();
        $answer_to_update['updated_at'] = date('Y-m-d H:i:s');

        \DB::beginTransaction();
        foreach($request->question as $key => $answer){
            if(!is_null($answer)){
              $saved_answer = Answer::where('user_id', $user->id)->where('question_id', $key)->where('period_id', $periodo->id)->first();
              if (!empty($saved_answer)){
                $answer_to_update['answer'] = $answer;
                DB::table('clima_answers')->where('user_id', $user->id)->where('question_id', $key)->where('period_id', $periodo->id)->update($answer_to_update);
              }
              else{
                try {
                    Answer::updateOrCreate([
                        'user_id' => $user->id,
                        'question_id' => $key,
                        'period_id' => $periodo->id,
                    ],[
                        'answer' => $answer,
                    ]);
                } catch (\Throwable $th) {
                    \DB::rollback();
                    dd($th->getMessage());
                }
              }
            }
        }

        /*if (!empty($request->respuestas)){
            foreach ($request->respuestas as $key => $value){
                $respuesta = array();
                $respuesta['respuesta'] = $value;
                $fecha = date('Y-m-d H:i:s');
                
                $results = Answer::where('periodo_id', $periodo->id)
                ->where('user_id', Auth::user()->id)
                ->where('pregunta_id', $request->preguntas[$key])
                ->get();
                
                if (count($results) > 0){
                    try {
                        Answer::where('user_id', Auth::user()->id)
                        ->where('periodo_id', $periodo->id)
                        ->where('pregunta_id', $request->preguntas[$key])
                        ->update($respuesta);
                    } catch (\Throwable $th) {
                        dd($th->getMessage());
                    }
                }else{
                    $respuesta['user_id'] = Auth::user()->id;
                    $respuesta['pregunta_id'] = $request->preguntas[$key];
                    $respuesta['periodo_id'] = $periodo->id;
                    try {
                        Answer::create($respuesta);
                    } catch (\Throwable $th) {
                        dd($th->getMessage());
                    }                    
                }
            }
        }*/

        $total_preguntas = $periodo->questions()->count();

        $resultados = Answer::where('user_id', Auth::user()->id)
        ->where('period_id', $periodo->id)
        ->count();

        $periodo_evaluado = array();
        if ($total_preguntas <= $resultados){
            $periodo_evaluado['status'] = 3;
            flash('Encuesta Finalizada');
        }else{
            $periodo_evaluado['status'] = 2;
            flash('La encuesta no ha sido concluida, favor de terminarla');
        }

        try {
            Auth::user()
            ->periods()
            ->updateExistingPivot($periodo->id, $periodo_evaluado);
        } catch (\Throwable $th) {
            \DB::rollback();
            dd($th->getMessage());
        }
        \DB::commit();

        return redirect('/cuestionarios/evaluaciones');
    }

    private function generar_resultados_grafica($filtro_a_usar, $id_periodo){

        $filtro_actual = '';
        $total = $contador_respuestas = 0;
        $filtros = array();
        $valores = array();
        $filtros_ordenados = array();
        $valores_ordenados = array();
        $respuestas = '';
        $nombre_campo = '';

        /*if ($filtro_a_usar == 'factores'){

            $respuestas = DB::table('respuestas')->join('preguntas', 'preguntas.id', '=', 'respuestas.id_pregunta')->join('factores', 'preguntas.id_factor', '=', 'factores.id')->join('personal', 'respuestas.id_evaluado', '=', 'personal.id')->where('id_factor', '!=', 0)->where('id_periodo', $id_periodo)->select('respuesta', 'positiva', 'departamento', 'puesto', 'id_factor', 'factores.nombre')->orderBy('id_factor')->get();
        }
        
        else{

            if ($filtro_a_usar == 'departamento'){

            $respuestas = DB::table('respuestas')->join('preguntas', 'preguntas.id', '=', 'respuestas.id_pregunta')->join('personal', 'respuestas.id_evaluado', '=', 'personal.id')->where('id_factor', '!=', 0)->where('id_periodo', $id_periodo)->select('respuesta', 'positiva', 'departamento AS filtro')->orderBy('departamento')->get();
            $nombre_campo = 'departamento';
            }

            else{

            if ($filtro_a_usar == 'puesto'){

                $respuestas = DB::table('respuestas')->join('preguntas', 'preguntas.id', '=', 'respuestas.id_pregunta')->join('personal', 'respuestas.id_evaluado', '=', 'personal.id')->where('id_factor', '!=', 0)->where('id_periodo', $id_periodo)->select('respuesta', 'positiva', 'puesto AS filtro')->orderBy('puesto')->get();
                $nombre_campo = 'puesto';
            }

            else{*/

                if ($filtro_a_usar == 'antiguedad'){
                    $respuestas = \DB::select("SELECT respuesta, positiva,
                    YEAR(CURDATE()) - YEAR(ingreso) - (DATE_FORMAT(CURDATE(), '%m%d') < DATE_FORMAT(ingreso, '%m%d')) AS filtro
                     FROM clima_respuestas INNER JOIN clima_preguntas ON (clima_preguntas.id = clima_respuestas.pregunta_id) INNER JOIN employee ON (respuestas.id_evaluado = personal.id) WHERE id_factor != 0 AND id_periodo = $id_periodo ORDER BY filtro");
                }

                /*else{

                if ($filtro_a_usar == 'boreales'){

                    $respuestas = DB::table('respuestas')->join('preguntas', 'preguntas.id', '=', 'respuestas.id_pregunta')->join('factores', 'preguntas.id_factor', '=', 'factores.id')->join('personal', 'respuestas.id_evaluado', '=', 'personal.id')->where('id_factor', '!=', 0)->where('id_periodo', $id_periodo)->where('personal.departamento', 'BOREALES')->select('respuesta', 'positiva', 'factores.nombre AS filtro')->orderBy('id_factor')->get();
                }

                else{

                    if ($filtro_a_usar == 'corretaje'){

                    $respuestas = DB::table('respuestas')->join('preguntas', 'preguntas.id', '=', 'respuestas.id_pregunta')->join('factores', 'preguntas.id_factor', '=', 'factores.id')->join('personal', 'respuestas.id_evaluado', '=', 'personal.id')->where('id_factor', '!=', 0)->where('id_periodo', $id_periodo)->where('personal.departamento', 'CORRETAJE ZONA NORTE')->select('respuesta', 'positiva', 'factores.nombre AS filtro')->orderBy('id_factor')->get();
                    }

                    else{

                    if ($filtro_a_usar == 'punto_sur'){

                        $respuestas = DB::table('respuestas')->join('preguntas', 'preguntas.id', '=', 'respuestas.id_pregunta')->join('factores', 'preguntas.id_factor', '=', 'factores.id')->join('personal', 'respuestas.id_evaluado', '=', 'personal.id')->where('id_factor', '!=', 0)->where('id_periodo', $id_periodo)->where('personal.departamento', 'PUNTO SUR')->select('respuesta', 'positiva', 'factores.nombre AS filtro')->orderBy('id_factor')->get();
                    }
                    }
                }
                }
            } 
            }
        }*/

        foreach ($respuestas as $key => $respuesta){

            if ($filtro_actual != $respuesta->filtro){

            if ($filtro_actual != ''){

                $filtro = $filtro_actual;
                $valor = number_format($total / $contador_respuestas, 2, '.', ',') * 1;
                $band = false;
                $temp_filtros = array();
                $temp_valores = array();

                foreach ($valores_ordenados as $key2 => $value){
                    
                if ($value < $valor && !$band){

                    $temp_valores[] = $valor;
                    $temp_filtros[] = $filtro;
                    $band = true;
                }

                $temp_valores[] = $value;
                $temp_filtros[] = $filtros_ordenados[$key2];
                }

                if (!$band){

                $temp_valores[] = $valor;
                $temp_filtros[] = $filtro;
                }

                $filtros_ordenados = $temp_filtros;
                $valores_ordenados = $temp_valores;
                $filtros[] = $filtro;
                $valores[] = $valor;
            }

            $total = 0;
            $contador_respuestas = 0;
            $filtro_actual = $respuesta->filtro;
            }

            switch ($respuesta->respuesta){

            case 'Totalmente de acuerdo':

                if ($respuesta->positiva == 1){
                
                $total += 5;
                }

                else{

                $total += 1;
                }

                break;

            case 'De acuerdo':

                if ($respuesta->positiva == 1){
                
                $total += 4;
                }

                else{

                $total += 2;
                }

                break;

            case 'Mas o menos':
                $total += 3;
                break;

            case 'Desacuerdo':

                if ($respuesta->positiva == 1){
                
                $total += 2;
                }

                else{

                $total += 4;
                }

                break;

            case 'Totalmente desacuerdo':
                
                if ($respuesta->positiva == 1){
                
                $total += 1;
                }

                else{

                $total += 5;
                }
            }

            $contador_respuestas++;
        }

        if ($filtro_actual != ''){

            $filtro = $filtro_actual;
            $valor = number_format($total / $contador_respuestas, 2, '.', ',') * 1;
            $band = false;
            $temp_filtros = array();
            $temp_valores = array();

            foreach ($valores_ordenados as $key => $value){
                    
            if ($value < $valor && !$band){

                $temp_valores[] = $valor;
                $temp_filtros[] = $filtro;
                $band = true;
            }

            $temp_valores[] = $value;
            $temp_filtros[] = $filtros_ordenados[$key];
            }

            if (!$band){

            $temp_valores[] = $valor;
            $temp_filtros[] = $filtro;
            }

            $filtros_ordenados = $temp_filtros;
            $valores_ordenados = $temp_valores;
            $filtros[] = $filtro;
            $valores[] = $valor;
        }

        $datos = new \stdClass();
        $datos->filtros_ordenados = $filtros_ordenados;
        $datos->valores_ordenados = $valores_ordenados;
        $datos->filtros = $filtros;
        $datos->valores = $valores;
        return $datos;
    }


    public function getAnswerValue($answer, $positive){
        switch ($answer){
            case 'Totalmente de acuerdo':
                return ($positive == 1)?5:1;
                break;
            case 'De acuerdo':
                return ($positive == 1)?4:2;
                break;

            case 'Mas o menos':
                return 3;
                break;

            case 'Desacuerdo':
                return ($positive == 1)?2:4;
                break;
            case 'Totalmente desacuerdo':
                return ($positive == 1)?1:5;
                break;
        }   
    }

    /**
     * Reporte Gráfico
     *
     * @return void
     */
    public function reporte_grafico(){

        $periodo = array();

        if (!empty($_POST['period_id'])){

          $periodo = Period::find($_POST['period_id']);
        }

        else{

          $periodo = Period::where('status', 'Abierto')->orWhere('status', 'Cerrado')->orderBy('id', 'DESC')->first();
        }

        $periodos = Period::where('status', 'Abierto')->orWhere('status', 'Cerrado')->orderBy('id', 'DESC')->get();
        if(count($periodos) == 0 || is_null($periodo)){
            flash('No hay periodos abiertos por el momento...');
            return redirect('/cuestionarios/que-es')->with('error','No tienes permiso para ver la evaluación');
        }
        $questions = $periodo->questions()->has('factor')->groupBy('factor_id')->get();
        $factorsUsed = $questions->pluck('factor_id')->toArray();
        $factors = Factor::with('questions.answers')->whereIn('id', $factorsUsed)->orderBy('id')->get();
        $factores = Factor::with('questions.answers')->whereIn('id', $factorsUsed)->pluck('name', 'id')->toArray();
        $answers = Answer::where('period_id', $periodo->id)->where('answer', '!=', 'N/A')->groupBy('user_id')->get();
        $area_pivots = AreaPivot::pluck('group_area_id', 'area_id')->toArray();
        $department_pivots = DepartmentPivot::pluck('group_department_id', 'department_id')->toArray();
        $job_position_pivots = JobPositionPivot::pluck('group_job_position_id', 'job_position_id')->toArray();
        $group_areas = [];
        $group_job_positions = [];
        $group_departments = [];
        $job_positions = [];
        $departments = [];
        $areas = [];
        $directions = [];

        foreach ($answers as $key => $value){

          if (empty($value->question->factor_id)){

            continue;
          }
          
          if (!empty($value->user->employee->job_position_id)){

            $job_position_id = $value->user->employee->job_position_id;

            if (!in_array($job_position_id, $job_positions)){

              $job_positions[] = $job_position_id;
            }

            if (isset($job_position_pivots[$job_position_id]) && !in_array($job_position_pivots[$job_position_id], $group_job_positions)){

              $group_job_positions[] = $job_position_pivots[$job_position_id];
            }

            if (!empty($value->user->employee->jobPosition->area)){

              $department_id = $value->user->employee->jobPosition->area->department_id;

              if (!in_array($department_id, $departments)){

                $departments[] = $department_id;
              }

              if (isset($department_pivots[$department_id]) && !in_array($department_pivots[$department_id], $group_departments)){

                $group_departments[] = $department_pivots[$department_id];
              }

              if (!empty($value->user->employee->jobPosition->area->department)){

                $direction_id = $value->user->employee->jobPosition->area->department->direction_id;

                if (!in_array($direction_id, $directions)){

                  $directions[] = $direction_id;
                }

                if (isset($area_pivots[$direction_id]) && !in_array($area_pivots[$direction_id], $group_areas)){

                  $group_areas[] = $area_pivots[$direction_id];
                }
              }

              $area_id = $value->user->employee->jobPosition->area->id;

              if (!in_array($area_id, $areas)){

                $areas[] = $area_id;
              }

              /*if (isset($area_pivots[$area_id]) && !in_array($area_pivots[$area_id], $group_areas)){

                $group_areas[] = $area_pivots[$area_id];
              }*/ 
            }
          }
        }

        if (!empty($group_areas)){

          $group_areas = GroupArea::whereIn('id', $group_areas)->orderBy('name')->get();
        }

        if (!empty($group_departments)){

          $group_departments = GroupDepartment::whereIn('id', $group_departments)->orderBy('name')->get();
        }

        if (!empty($group_job_positions)){
        
          $group_job_positions = GroupJobPosition::whereIn('id', $group_job_positions)->orderBy('name')->get();
        }

        if (!empty($directions)){

          $directions = Direction::whereIn('id', $directions)->orderBy('name')->get();
        }

        if (!empty($departments)){
        
          $departments = Department::whereIn('id', $departments)->orderBy('name')->get();
        }

        if (!empty($areas)){

          $areas = Area::whereIn('id', $areas)->orderBy('name')->get();
        }

        if (!empty($job_positions)){

          $job_positions = JobPosition::whereIn('id', $job_positions)->orderBy('name')->get();
        }
        //$answers = Answer::where('period_id', $periodo->id)->orderBy('question_id')->get();
        $answers = DB::table('clima_answers')
          ->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')
          ->join('users', 'users.id', '=', 'clima_answers.user_id')
          ->join('employees', 'employees.id', '=', 'users.employee_id')
          ->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')
          ->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')
          ->leftJoin('departments', 'departments.id', '=', 'areas.department_id')
          ->leftJoin('directions', 'directions.id', '=', 'departments.direction_id')
          ->leftJoin('clima_areas_pivot', 'clima_areas_pivot.area_id', '=', 'directions.id')
          ->leftJoin('clima_group_areas', 'clima_group_areas.id', '=', 'clima_areas_pivot.group_area_id')
          ->leftJoin('clima_departments_pivot', 'clima_departments_pivot.department_id', '=', 'departments.id')
          ->leftJoin('clima_group_departments', 'clima_group_departments.id', '=', 'clima_departments_pivot.group_department_id')
          ->leftJoin('clima_job_positions_pivot', 'clima_job_positions_pivot.job_position_id', '=', 'job_positions.id')
          ->leftJoin('clima_group_job_positions', 'clima_group_job_positions.id', '=', 'clima_job_positions_pivot.group_job_position_id')
          ->where('clima_answers.period_id', $periodo->id)->where('clima_answers.answer', '!=', 'N/A')->whereNotNull('clima_questions.factor_id')
          ->select('answer', 'question_id', 'question', 'factor_id', 'positive', 'employees.job_position_id', 'sexo', 'nacimiento', 'ingreso', 'turno', 'grado', 'division', 'relacion', 'job_positions.area_id', 'areas.department_id', 'departments.direction_id', 'group_area_id', 'group_department_id', 'group_job_position_id', 'users.id')
          ->orderBy('factor_id')->orderBy('question_id')->get();
        $area_pivots = AreaPivot::all();
        $department_pivots = DepartmentPivot::all();
        $job_position_pivots = JobPositionPivot::all();
        
        // dd($factors);
        /*foreach ($factors as $factor) {
            foreach ($factor->questions as $question) {
                $total = $question->answers->count();
                $promedio = 0;
                foreach ($question->answers as $answer) {
                    $promedio = $this->getAnswerValue($answer->answer, $question->positive);                    
                }
                if($total == 0){
                    $factor->averageValue = 0;
                }else{
                    $factor->averageValue = $promedio / $total;
                }
                
            }
        }

        $categories = $factors->pluck('name')->toArray();
        $factorData = [];
        foreach ($factors as $factor) {
            $factorData[] = [
                'name' => $factor->name,
                'data' => [$factor->averageValue],
            ];
        }

        // Factor::where

        // $resultados = $resultados_antiguedad = $puestos = $departamentos = $grupos_areas = $niveles_puestos = array();
        // $periodo = Period::where('status', 'Abierto')->orderBy('id', 'DESC')->firstOrFail();
        // // $periodo = DB::table('periodos')->where('estado', 'Abierto')->select('id')->first();
        // $id_periodo = $periodo->id;
        
        // $resultados = Answer::with('question', 'user.employee')
        // ->whereHas('question', function($q){
        //     $q->whereNotNull('factor_id');
        // })
        // ->where('period_id', $periodo->id)
        // ->get();
        
        // ->orderBy('id_pregunta')

        // $resultados = DB::table('respuestas')
        // ->join('preguntas', 'preguntas.id', '=', 'respuestas.id_pregunta')
        // ->join('factores', 'preguntas.id_factor', '=', 'factores.id')
        // ->join('personal', 'respuestas.id_evaluado', '=', 'personal.id')
        // ->leftJoin('puestos', 'puestos.puesto', '=', 'personal.puesto')
        // ->leftJoin('puesto_nivel_puesto', 'puesto_nivel_puesto.id_puesto', '=', 'puestos.id')
        // ->leftJoin('niveles_puestos', 'niveles_puestos.id', '=', 'puesto_nivel_puesto.id_nivel_puesto')
        // ->leftJoin('areas', 'areas.nombre', '=', 'personal.departamento')
        // ->leftJoin('areas_grupo_areas', 'areas_grupo_areas.id_areas', '=', 'areas.id')
        // ->leftJoin('grupo_areas', 'grupo_areas.id', '=', 'areas_grupo_areas.id_grupos_areas')
        // ->where('id_factor', '!=', 0)->where('id_periodo', $periodo->id)
        // ->select('respuesta', 'pregunta', 'positiva', 'departamento', 'personal.puesto', 'grupo_areas.id AS grupo_area', 'niveles_puestos.id AS nivel_puesto', 'id_factor', 'factores.nombre', 'id_pregunta')->orderBy('id_factor')->orderBy('id_pregunta')->get();
        
        // $resultados_antiguedad = $this->generar_resultados_grafica('antiguedad', $periodo->id);
        
        // $puestos = User::with('employee')->whereHas('periods', function($q) use($periodo){
        //     $q->where('id', $periodo->id);
        // })->get();
        
        // dd($puestos,$resultados);

        // $puestos = DB::table('personal')->join('periodo_evaluados', 'periodo_evaluados.id_evaluado', '=', 'personal.id')
        // ->leftJoin('puestos', 'puestos.puesto', '=', 'personal.puesto')
        // ->leftJoin('puesto_nivel_puesto', 'puesto_nivel_puesto.id_puesto', '=', 'puestos.id')
        // ->leftJoin('niveles_puestos', 'niveles_puestos.id', '=', 'puesto_nivel_puesto.id_nivel_puesto')
        // ->where('id_periodo', $periodo->id)
        // ->select(DB::raw('DISTINCT personal.puesto'), 'departamento', 'niveles_puestos.id')
        // ->orderBy('personal.puesto')
        // ->get();
        
        // Employee::
        // $departamentos = DB::table('personal')
        // ->join('periodo_evaluados', 'periodo_evaluados.id_evaluado', '=', 'personal.id')
        // ->leftJoin('areas', 'areas.nombre', '=', 'personal.departamento')
        // ->leftJoin('areas_grupo_areas', 'areas_grupo_areas.id_areas', '=', 'areas.id')
        // ->leftJoin('grupo_areas', 'grupo_areas.id', '=', 'areas_grupo_areas.id_grupos_areas')
        // ->where('id_periodo', $periodo->id)
        // ->select(DB::raw('DISTINCT departamento'), 'grupo_areas.id')
        // ->orderBy('departamento')->get();

        // $grupo_areas = GruposAreas::where('id', '!=', 1)
        // ->select('id', 'nombre')->orderBy('nombre')
        // ->get();

        // $niveles_puestos = NivelesPuestos::where('id', '!=', 1)
        // ->select('id', 'nombre')
        // ->orderBy('nombre')
        // ->get();

        /*$resultados['factores'] = $this->generar_resultados_grafica('factores', $periodo->id);
        $resultados['departamento'] = $this->generar_resultados_grafica('departamento', $periodo->id);
        $resultados['puesto'] = $this->generar_resultados_grafica('puesto', $periodo->id);
        $resultados['antiguedad'] = $this->generar_resultados_grafica('antiguedad', $periodo->id);
        $resultados['boreales'] = $this->generar_resultados_grafica('boreales', $periodo->id);
        $resultados['corretaje'] = $this->generar_resultados_grafica('corretaje', $periodo->id);
        $resultados['punto_sur'] = $this->generar_resultados_grafica('punto_sur', $periodo->id);*/

        $id_periodo = $periodo->id;
        $etiquetas = DB:: table('clima_etiquetas')->orderBy('valor', 'DESC')->get();
        //return view('cuestionarios/reportes/reporte-grafico2', compact('factorData', 'categories'));
        return view('cuestionarios/reportes/reporte-grafico', compact('factors', 'job_positions', 'departments', 'directions', 'group_areas', 'group_departments', 'group_job_positions', 'answers', 'area_pivots', 'department_pivots', 'job_position_pivots', 'areas', 'id_periodo', 'factores', 'periodos', 'etiquetas'));
    }

  /**
  * Exporta Respuestas del Period Abierto
  */
  public function exportar_resultados($id_periodo, $grupo_departamento = '', $grupo_area = '', $grupo_puesto = '', $direccion = '', $departamento = '', $area = '', $puesto = ''){
        require_once 'PHPExcel.php';
        require_once 'PHPExcel/IOFactory.php';

        $resultados = array();
        $title = '';

        if (empty($grupo_area) && empty($departamento) && empty($puesto)){

          $resultados = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('users', 'clima_answers.user_id', '=', 'users.id')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->where('factor_id', 0)->where('period_id', $id_periodo)->select('answer', 'question', 'departments.name')->orderBy('departments.name')->orderBy('clima_questions.id')->get();
        }

        else{

          if (!empty($departamento) && !empty($puesto)){

            $department = Department::find($departamento);
            $job_position = JobPosition::find($puesto);
            $title = ' Departamento ' . $department->name . ' y Puesto ' . $job_position->name;
            $resultados = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('users', 'clima_answers.user_id', '=', 'users.id')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->where('factor_id', 0)->where('period_id', $id_periodo)->where('department_id', $departamento)->where('job_position_id', $puesto)->select('answer', 'question', 'departments.name')->orderBy('departments.name')->orderBy('clima_questions.id')->get();
          }

          else{

            if (!empty($departamento)){

              $department = Department::find($departamento);
              $title = ' Departamento ' . $department->name;
              $resultados = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('users', 'clima_answers.user_id', '=', 'users.id')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->where('factor_id', 0)->where('period_id', $id_periodo)->where('department_id', $departamento)->select('answer', 'question', 'departments.name')->orderBy('departments.name')->orderBy('clima_questions.id')->get();
            }

            else{

              if (!empty($puesto)){

                $job_position = JobPosition::find($puesto);
                $title = ' Puesto ' . $job_position->name;
                $resultados = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('users', 'clima_answers.user_id', '=', 'users.id')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->where('factor_id', 0)->where('period_id', $id_periodo)->where('job_position_id', $puesto)->select('answer', 'question', 'departments.name')->orderBy('departments.name')->orderBy('clima_questions.id')->get();
              }

              else{

                if (!empty($grupo_area)){

                  $grupo = GruposAreas::find($grupo_area);
                  $title = ' Grupo de Área ' . $grupo->name;
                  $resultados = DB::table('clima_answers')->join('clima_questions', 'clima_questions.id', '=', 'clima_answers.question_id')->join('users', 'clima_answers.user_id', '=', 'users.id')->join('employees', 'employees.id', '=', 'users.employee_id')->leftJoin('job_positions', 'job_positions.id', '=', 'employees.job_position_id')->leftJoin('areas', 'areas.id', '=', 'job_positions.area_id')->leftJoin('departments', 'departments.id', '=', 'areas.department_id')->leftJoin('clima_areas_pivot', 'clima_areas_pivot.area_id', '=', 'areas.id')->leftJoin('clima_group_areas', 'clima_group_areas.id', '=', 'clima_areas_pivot.group_area_id')->where('factor_id', 0)->where('period_id', $id_periodo)->where('clima_group_areas.id', $grupo_area)->select('answer', 'question', 'departments.name')->orderBy('departments.name')->orderBy('clima_questions.id')->get();
                }
              }
            } 
          }
        }

        // Create new PHPExcel object
        $objPHPExcel = new \PHPExcel();

        // Create a first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Departamento');  
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Factor');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Pregunta');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Respuesta');
        $i = 2;

        foreach ($resultados as $key => $value){
            
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $value->name);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, 'Pregunta Abierta');
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $value->question);
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $value->answer);
        $i++;
        }

        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Respuestas Abiertas');

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Respuestas Encuesta' . $title . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
}