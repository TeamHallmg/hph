<?php

namespace App\Models\Announcement;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class AnnouncementType extends Model
{
	use SoftDeletes;
    //
    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['name','max_quantity'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * AnnouncementType has many Announcements.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function announcements($view = null)
    {
    	// hasMany(RelatedModel, foreignKeyOnRelatedModel = announcementType_id, localKey = id)
    	return $this->hasMany(Announcement::class)
        ->when(!is_null($view), function($q) use ($view){
            $q->where('view_id', $view);
        });
    }

    /**
     * AnnouncementType has many Announcements.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function announcementsActive($view = null)
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = announcementType_id, localKey = id)
        return $this->hasMany(Announcement::class)
        ->where('active',1)        
        ->when(!is_null($view), function($q) use ($view){
            $q->where('view_id', $view);
        })
        ->where('starts','<=', Carbon::now())
        ->where('ends','>=', Carbon::now());

    }

    /**
     * AnnouncementType has one ViewSequense.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function viewSequence()
    {
        // hasOne(RelatedModel, foreignKeyOnRelatedModel = announcementType_id, localKey = id)
        return $this->hasOne(AnnouncementViewSequence::class);
    }

    /**
     * AnnouncementType has one HasFormCreated.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function hasFormCreated()
    {
        // hasOne(RelatedModel, foreignKeyOnRelatedModel = announcementType_id, localKey = id)
        return $this->hasOne(AnnouncementForm::class)->count()>0?true:false;
    }

    /**
     * AnnouncementType has many GetFormHeaders.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getFormHeaders()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = announcementType_id, localKey = id)
        $headers = [];
        foreach ($this->hasMany(AnnouncementForm::class)->orderBy('id','ASC')->get() as $key => $value) {
            $headers[$value->announcementAttribute->form_name] = $value->announcementAttribute->name;
        }
        return $headers;
    }

    public function countAnnouncementsInView($view){
        return $this->announcements($view)->count();
    }

    public function countActiveAnnouncementsInView($view){
        return $this->announcementsActive($view)->count();
    }

    public function attributes()
    {
        return $this->belongsToMany(AnnouncementAttribute::class, 'announcement_forms', 'announcement_type_id', 'announcement_attribute_id')
        ->withPivot('id', 'view_order', 'required');
    }
}
