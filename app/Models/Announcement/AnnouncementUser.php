<?php

namespace App\Models\Announcement;

use App\Models\DeletedReason;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AnnouncementUser extends Model
{
		use SoftDeletes;

    protected $fillable = ['announcement_id', 'type', 'user_id', 'enterprise_id', 'sucursal_id'];

    public function deleted_reason() {
        return $this->morphMany(DeletedReason::class, 'deleted_reason');
    }
}
