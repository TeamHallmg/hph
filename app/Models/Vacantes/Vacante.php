<?php

namespace App\Models\Vacantes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Requisitions\Requisition;
use App\Models\Vacantes\Postulante;
use App\Models\Vacantes\Reclutador;

class Vacante extends Model
{
    use SoftDeletes; //para el borrado logico

    protected $table = 'vacancies';

    protected $dates = ['deleted_at'];

    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
        'reclutador_id',
        'requisicion_id',
        'paquete_id',
        'status',
        'fecha_inicio',
        'fecha_fin',
        'fecha_cierre',
        'comentarios'
    ];

    public function requisicion() {
        return $this->belongsTo(Requisition::class, 'requisicion_id', 'id')->withDefault();
    }

    public function postulante() {
        return $this->hasMany(Postulante::class, 'vacante_id', 'id');
    }

    public function reclutador() {
        return $this->belongsTo(Reclutador::class, 'reclutador_id', 'id')->withDefault();
    }

}
