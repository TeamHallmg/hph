<?php

namespace App\Models\Vacantes;

use App\Models\Vacantes\Postulante;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Encuesta extends Model
{
    use SoftDeletes;

    protected $table = 'vacancies_application_survey';

    protected $fillable = [
        'postulante_id',
        'question1',
        'question2',
        'question3',
        'question4',
        'question5',
        'question6',
        'question7',
        'question8',
        'question9',
        'question10'
    ];

    public function postulante() {
        return $this->belongsTo(Postulante::class, 'poatulante_id', 'id')->withDefault();
    }
}
