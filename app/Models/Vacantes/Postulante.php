<?php

namespace App\Models\Vacantes;

use App\User;
use App\Models\Profile\Profile;
use App\Models\Vacantes\Vacante;
use App\Models\Vacantes\Encuesta;
// use App\Models\Vacantes\Postulante;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Postulante extends Model
{
    use SoftDeletes; //para el borrado logico

    protected $table = 'vacancies_postulants';

    protected $dates = ['deleted_at'];

    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
        'user_id',
        'vacante_id',
        'profile_id',
        'intentos_postulacion',
        'aceptacion_postulacion',
        'selected',
        'motivo',
        'motivo_cierre_vacante'
    ];

    public function usuario() {
        return $this->belongsTo(User::class, 'user_id', 'id')->withDefault()->withTrashed();
    }

    public function vacante() {
        return $this->belongsTo(Vacante::class, 'vacante_id', 'id')->withDefault()->withTrashed();
    }

    public function perfil() {
        return $this->belongsTo(Profile::class, 'profile_id', 'id')->withDefault()->withTrashed();
    }
    
    public function encuesta() {
        return $this->hasOne(Encuesta::class, 'postulante_id', 'id');
    }
}
