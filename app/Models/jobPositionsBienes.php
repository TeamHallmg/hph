<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class jobPositionsBienes extends Model
{
    protected $table = 'job_positions_bienes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_tipo_bien', 'id_job_positions', 'estatus'];


     public function tipobien()
     {
         return $this->belongsTo('App\Models\Bienes\TipoBienes','id_tipo_bien');
     }
 

}
