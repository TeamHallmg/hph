<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Direction extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'directions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    public function department()
    {
        return $this->hasMany(Department::class, 'direction_id', 'id');
    }

    public function areas()
    {
        return $this->hasMany(Area::class, 'direction_id', 'id');
    }

    /*public function getAreasCount() {
        $count = 0;
        foreach($this->department as $department) {
            $count += $department->areas->count();
        }
        return $count;
    }*/

    public function getAreasCount() {
        $count = 0;
        foreach($this->areas as $area) {
            $count++;
        }
        return $count;
    }

    /*public function getJobsCount() {
        $count = 0;
        foreach($this->department as $department) {
            foreach($department->areas as $area) {
                $count += $area->jobs->count();
            }
        }
        return $count;
    }*/

    public function getJobsCount() {
        $count = 0;
        foreach($this->areas as $area) {
            foreach($area->department as $department) {
                $count += $department->jobs->count();
            }
        }
        return $count;
    }

    /*public function getEmployeesCount() {
        $count = 0;
        foreach($this->department as $department) {
            foreach($department->areas as $area) {
                foreach($area->jobs as $job) {
                    $count += $job->employees->count();
                }
            }
        }
        return $count;
    }*/

    public function getEmployeesCount() {
        $count = 0;
        foreach($this->areas as $area) {
            foreach($area->department as $department) {
                foreach($department->jobs as $job) {
                    $count += $job->employees->count();
                }
            }
        }
        return $count;
    }

    public static function createCascade($direccion = 'Sin Dirección'){
        $direccion = is_null($direccion)?'Sin Dirección':$direccion;
        if ($direccion === 'Sin Dirección' || !is_numeric($direccion)) {
            return Direction::firstOrCreate([
                'name' => $direccion, 
            ]);
        }else{
            return Direction::find($direccion);
        }
    }

    public function deleted_reason() {
        return $this->morphMany(DeletedReason::class, 'deleted_reason');
    }

}
