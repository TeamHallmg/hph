<?php

namespace App\Models\ClimaOrganizacional;

use Illuminate\Database\Eloquent\Model;

class Etiquetas extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'clima_etiquetas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];
}
