<?php

namespace App\Models\ClimaOrganizacional;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Department;

class GroupDepartment extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clima_group_departments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function departments()
    {
        return $this->belongsToMany(Department::class,
            with(new DepartmentPivot)->getTable(),
            'group_department_id',
            'department_id');
    }
}
