<?php

namespace App\Models\ClimaOrganizacional;

use Illuminate\Database\Eloquent\Model;

// use Illuminate\Support\Database\SoftDelete;

class Sections extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clima_sections';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['section'];
}
