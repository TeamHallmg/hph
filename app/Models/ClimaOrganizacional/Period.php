<?php

namespace App\Models\ClimaOrganizacional;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;

class Period extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clima_periods';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'status', 'enterprise_id', 'no_aplica'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function users()
    {
        return $this->belongsToMany(User::class, 
            with(new PeriodUser)->getTable(),
            'period_id',
            'user_id')->withTrashed()
        ->withPivot('status');
    }

    public function questions()
    {
        return $this->belongsToMany(Question::class, 
            with(new PeriodQuestion)->getTable(),
            'period_id',
            'question_id');
    }

    public function answers()
    {
        return $this->hasMany(Answer::class, 'period_id', 'id');
    }
}
