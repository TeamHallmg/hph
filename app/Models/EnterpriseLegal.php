<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EnterpriseLegal extends Model
{ 

    protected $table = 'enterprise_legal';



    protected $fillable = ['id_empresa', 
    'dpi',
    'nombre',
    'nacionalidad',
    'fecha_nac',
    'sexo',
    'edo_civil',
    'profesion',
    'puesto',
    'notario',
    'nombramiento',
    'numero_registro',
    'folio',
    'libro'
];
}
