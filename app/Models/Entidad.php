<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Municipio;

class Entidad extends Model
{
    protected $table = 'entidad';

    public function municipios_mx()
    {
        return $this->hasMany(Municipio::class, 'estado_id', 'id')->orderBy('name', 'ASC');
    }
}
