<?php

namespace App\Models\Vacations;

use Illuminate\Database\Eloquent\Model;

class Incident extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'incidents';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id','from_id','benefit_id','event_id','week','time','amount','info','comment','status','value','manager','incapacity_id'];

    public function user() {
        return $this->belongsTo('App\User');
    }
	 public function from() {
        return $this->belongsTo('App\User','from_id','id');
    }
    public function benefit() {
        return $this->belongsTo('App\Models\Vacations\Benefits');
    }
    public function incapacity(){
        return $this->belongsTo('App\Models\Vacations\Incapacity');
    }
    public function event(){
        return $this->belongsTo('App\Models\Vacations\Event');
    }

    public function incidentOvertime()
    {
        return $this->hasOne('App\Models\Vacations\IncidentOvertime');
    }

    public function getDate(){
        return date('d-m-Y',$this->time);
    }
}
