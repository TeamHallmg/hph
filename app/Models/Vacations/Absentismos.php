<?php

namespace App\Models\Vacations;

use Illuminate\Database\Eloquent\Model;

class Absentismos extends Model
{
	protected $table = 'absentismos';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	
	protected $fillable = ['class','days', 'description', 'date', 'observation'];

}
