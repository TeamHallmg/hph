<?php

namespace App\Models\Vacations;

use Illuminate\Database\Eloquent\Model;

use App\Models\Vacations\Event;
use App\Models\Vacations\Balances;

class EventDay extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'event_days';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['date', 'processed', 'balance_id', 'event_id'];

    public function balance()
    {
        return $this->belongsTo(Balances::class);
    }

    public function event()
    {
        return $this->belongsTo(Event::class);
    }
}
