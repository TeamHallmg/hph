<?php

namespace App\Models\Vacations;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Vacations\PleaController;

class Event extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'events';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title', 'start', 'end', 'type', 'user_id', 'blocked', 'status', 'benefit_id','region_id','js'];

    /*
    public static getEventsBetween($user,$start,$end,$type,$results = 'first')
    {
        $result = Event::where([['type',$type],['start','<=',$start],['end','>=',$end]])
                    ->where(function($query){
                        $query->where('js','like','%"' . $user->id . '"%')
                              ->orWhere('region_id',$user->region_id);
                    })
                    ->first();
        return ($result);
    }
    */
	
    public function user() {
        return $this->belongsTo('App\User');
    }
    public function region() {
        return $this->belongsTo('App\Models\User\Region');
    }

    public function benefit() {
        return $this->belongsTo('App\Models\Vacations\Benefits');
    }
    public function getStartDate($format = 'd-m-Y') {
        return date($format, $this->start);
    }
    public function getEndDate($format = 'd-m-Y') {
        return date($format, $this->end);
    }

    public function eventDays()
    {
        return $this->hasMany('App\Models\Vacations\EventDay');
    }

    public function generateValidStartDate(){
        if($this->start !== $this->end){
            $start = $this->start;
            $end = $this->end;
            $schedules = $this->user->getSchedule($start, null, true);
            $schedule = [];
            $holidays = PleaController::getHolidays(date('Y-m-d', $this->start));
            foreach($schedules as $day) {
                $schedule[$day->day][] = $day->schedule->in;
                $schedule[$day->day][] = $day->schedule->out;
                $schedule[$day->day][] = $day->labor;
            }
            $d = date('D', $start);
            // dd(date('Y-m-d', $start), $start, isset($holidays[$start]), $schedule[$d][2] == 0);
            while(isset($holidays[$start]) || $schedule[$d][2] == 0) {
                $start += (1 * 24 * 60 * 60);
                $d = date('D', $start);
            }
            $this->start = $start;
        }
    }

    public function generateValidEndDate(){
        if($this->start !== $this->end){
            $start = $this->start;
            $end = $this->end;
            $schedules = $this->user->getSchedule($start, null, true);
            $schedule = [];
            $holidays = PleaController::getHolidays(date('Y-m-d', $this->start));
            foreach($schedules as $day) {
                $schedule[$day->day][] = $day->schedule->in;
                $schedule[$day->day][] = $day->schedule->out;
                $schedule[$day->day][] = $day->labor;
            }
            $d = date('D', $end);
            while(isset($holidays[$end]) || $schedule[$d][2] == 0) {
                $end -= (1 * 24 * 60 * 60);
                $d = date('D', $end);
            }
            $this->end = $end;
        }
    }

    public function generateValidDates(){
        if($this->start !== $this->end){
            $this->generateValidStartDate();
            $this->generateValidEndDate();
        }
    }

    public function getDays($onlyLabor = false)
    {
        $start = $this->start;
        $end = $this->end;
        $days = 0;
        $holidays = PleaController::getHolidays(date('Y-m-d', $this->start));
        if($start === $end){
            if(!isset($holidays[$this->start]) && $this->user->getDaysCountInMySchedule($this->start, $this->end, $onlyLabor)){
                $days = 1;
            }
        }else{
            // dd(date('Y-m-d', $start), date('Y-m-d', $end));
            while($start < $end){
                if(isset($holidays[$start])){
                    $days--;
                }
                $start += 24 * 60 * 60;
            }
            $days += $this->user->getDaysCountInMySchedule($this->start, $this->end, $onlyLabor);
        }
        
        return($days);
    }

    public function getUserSchedule($onlyLabor = false){
        $start = $this->start;
        $end = $this->end;
        if($this->user->getRegion()){
            if($start === $end){
                $schedule = $this->user->employee->region->getDay($start);
            }else{
                $schedule = $this->user->employee->region->getDays($start, $end);
            }
            return $schedule;
        }
        return [];
    }

    public function isStartDateWorkable(){
        return $this->user->isDayWorkable($this->getStartDate('Y-m-d'));
    }

    /**
     * Corroboramos que la fecha de inicio del evento no sea un día feriado o evento
     *
     * @return bool
     */
    public function isStartDateAnEvent(){
        $holidays = PleaController::getHolidays(date('Y-m-d', $this->start));
        return isset($holidays[$this->start]);
    }

    public function checkIfDaysAreWorkable(){
        $start = $this->start;
        $end = $this->end;
        if($start === $end){
            $this->user->isDayWorkable($start);
        }else{

        }

    }

    public function getTotalDays()
    {
        $days  = 0;
        $start = $this->start;
        $end   = $this->end;

        if(($end - $start) == 0) {
            $days = 1;
        } else {
            $start = $this->start;
            $end   = $this->end + (1 * 24 * 60 * 60);
            $days = $end - $start;
            $days = $days / (1 * 24 * 60 * 60);
        }
        return($days);
    }

    public function continuous()
    {
        $days     = 1;
        $start    = $this->start;
        $total    = $this->benefit->days;
        $schedule = $this->user->getMySchedule();

        while($days < $total) {
            $d = date('D',$start);
            if($schedule[$d][2] == 0) $days--;
            $start += (1 * 24 * 60 * 60);
            $days++;
        }
        $this->end = $start;
    }
	
	public function continuousDays()
    {
        $holidays = PleaController::getHolidays(date('Y-m-d', $this->start));
        $days     = 1;
        $start    = $this->start;
        $total    = $this->benefit->days;
        $schedules = $this->user->getSchedule($start, null, true);
        $dayReturn = array();
        $schedule = [];
        foreach($schedules as $day) {
            $schedule[$day->day][] = $day->schedule->in;
            $schedule[$day->day][] = $day->schedule->out;
            $schedule[$day->day][] = $day->labor;
        }
        while($days < $total) {
            $d = date('D',$start);
            if($schedule[$d][2] == 0 || isset($holidays[$start])){
                $days--;
            }
            $start += (1 * 24 * 60 * 60);
            $days++;
        }
        return $start;
    }

    public function howMDFT($today)
    {
        $days  = 0;
        $start = $today - (1 * 24 * 60 * 60);
        $end   = $this->end ;
        $schedule = $this->user->getMySchedule();
        if(($end - $start) == 0) {
            $days = 0;
        } else {
            $days = $end - $start;
            $days = $days / (1 * 24 * 60 * 60);
            while($end > $start) {
                $d = date('D',$start);
                if($schedule[$d][2] == 0) $days--;
                $start += (1 * 24 * 60 * 60);
            }
        }
        return($days);
    }

    public function getDaysArray()
    {

    }
}
