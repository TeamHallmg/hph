<?php

namespace App\Models\Vacations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Balances extends Model
{
    use SoftDeletes;
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'balances';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['year','amount', 'pending', 'until', 'benefit_id', 'user_id'];

    protected $dates = ['until', 'deleted_at'];

    public function benefit() {
        return $this->belongsTo('App\Models\Vacations\Benefits');
    }
}
