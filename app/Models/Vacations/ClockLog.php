<?php

namespace App\Models\Vacations;

use Illuminate\Database\Eloquent\Model;

class ClockLog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['date', 'user_id'];
}
