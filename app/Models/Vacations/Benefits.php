<?php

namespace App\Models\Vacations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Benefits extends Model
{
    use SoftDeletes;
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'benefits';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['code','shortname', 'name','context','type','continuous','cronjob','report','remove','relationship','days','frequency','group','retype','exec','gender','blocked'];

    protected $dates = ['deleted_at'];
}
