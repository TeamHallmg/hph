<?php

namespace App\Models\Nps;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nps_questions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['question', 'positive'];

    public function periods()
    {
        return $this->belongsToMany(Period::class, 
            with(new PeriodQuestion)->getTable(),
            'question_id',
            'period_id');
    }

    public function answers()
    {
        return $this->hasMany(Answer::class, 'question_id', 'id');
    }

    public function answer()
    {
        return $this->hasOne(Answer::class, 'question_id', 'id');
    }
}
