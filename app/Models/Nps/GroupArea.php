<?php

namespace App\Models\ClimaOrganizacional;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Direction;

class GroupArea extends Model
{
    use SoftDeletes;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clima_group_areas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'period_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function areas()
    {
        return $this->belongsToMany(Direction::class,
            with(new AreaPivot)->getTable(),
            'group_area_id',
            'area_id');
    }
}
