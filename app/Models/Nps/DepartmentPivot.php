<?php

namespace App\Models\ClimaOrganizacional;

use Illuminate\Database\Eloquent\Model;

use App\Models\Department;

class DepartmentPivot extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clima_departments_pivot';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['department_id', 'group_department_id'];

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }

    public function groupDepartment()
    {
        return $this->belongsTo(GroupDepartment::class, 'group_department_id', 'id');
    }
}
