<?php

namespace App\Models\Nps;

use Illuminate\Database\Eloquent\Model;

use App\User;

class NpsPeriodUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nps_period_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['period_id', 'user_id', 'status'];

    public function period()
    {
        return $this->belongsTo(NpsPeriod::class, 'period_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
