<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Area extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'areas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'department_id'];

    public function direction()
    {
        return $this->belongsTo(Direction::class, 'direction_id', 'id');
    }

    public function department_wt(){
        return $this->belongsTo(Department::class, 'department_id', 'id')->withTrashed();
    }
    /*public function department()
    {
        return $this->hasMany(Department::class, 'area_id', 'id');
    }*/

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }

    /*public function department()
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }*/

    /*public function jobs()
    {
        return $this->hasMany(JobPosition::class, 'area_id', 'id');
    }*/

    /*public function getEmployeesCount() {
        $count = 0;
        foreach($this->jobs as $job) {
            $count += $job->employees->count();
        }
        return $count;
    }*/

    public function getEmployeesCount() {
        $count = 0;
        foreach($this->department as $department) {
          foreach($department->jobs as $job) {
            $count += $job->employees->count();
          }
        }
        return $count;
    }

    /*public static function createCascade($direccion = 'Sin Dirección', $department = 'Sin Departamento', $area = 'Sin Área'){
        $area = is_null($area)?'Sin Área':$area;

        if($area === 'Sin Área' || !is_numeric($area)){
            return Area::firstOrCreate([
                'name' => $area,
                'department_id' => Department::createCascade($direccion, $department)->id,
            ]);
        }else{
            dd($area);
            return Area::find($area);
        }*/

        /*return Area::when($area === 'Sin Área' || !is_numeric($area), function($q) use($direccion, $department, $area) {
            return $q->firstOrCreate([
                'name' => $area,
                'department_id' => Department::createCascade($direccion, $department)->id,
            ]);
        }, function ($q) {
            return $q->find($area);
        })->orderBy('id','DESC')->first();*/

    //}

    public static function createCascade($direccion = 'Sin Dirección', $area = 'Sin Área'){
        $area = is_null($area)?'Sin Área':$area;

        if($area === 'Sin Área' || !is_numeric($area)){
            return Area::firstOrCreate([
                'name' => $area,
                'direction_id' => Direction::createCascade($direccion)->id,
            ]);
        }else{
            dd($area);
            return Area::find($area);
        }

        /*return Area::when($area === 'Sin Área' || !is_numeric($area), function($q) use($direccion, $department, $area) {
            return $q->firstOrCreate([
                'name' => $area,
                'department_id' => Department::createCascade($direccion, $department)->id,
            ]);
        }, function ($q) {
            return $q->find($area);
        })->orderBy('id','DESC')->first();*/

    }
    
    public function deleted_reason() {
        return $this->morphMany(DeletedReason::class, 'deleted_reason');
    }
}
