<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CronError extends Model
{
    public $timestamps = false;
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cronerror';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['since', 'today'];
}
