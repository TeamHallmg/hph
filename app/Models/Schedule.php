<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Workshift;

class Schedule extends Model
{
    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['shift','in','out'];

    public function workshiftsFixed()
    {
        return $this->hasMany(Workshift::class);
    }
}
