<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\User;

// use Illuminate\Support\Database\SoftDelete;

class MultiPermission extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'multi_permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['modulo_id', 'enterprise_id', 'sucursal_id'];

    public function users()
    {
        return $this->belongsToMany(User::class, 'multi_permission_user', 'permission_id', 'user_id');
    }

    public function sucursals()
    {
        return $this->belongsTo(Sucursal::class, 'sucursal_id');
    }

    public function enterprise()
    {
        return $this->belongsTo(Enterprise::class, 'enterprise_id');
    }

    public function modulo()
    {
        return $this->belongsTo(Modulo::class, 'modulo_id');
    }
}
