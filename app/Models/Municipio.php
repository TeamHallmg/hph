<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Entidad;

class Municipio extends Model
{
    protected $table = 'municipios';

    public function entidad_mx()
    {
        return $this->belongsTo(Entidad::class, 'estado_id', 'id')->withDefault();
    }
}
