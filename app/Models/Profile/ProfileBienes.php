<?php

namespace App\Models\Profile;

use Illuminate\Database\Eloquent\Model;

class ProfileBienes extends Model
{
    // use SoftDeletes;

    protected $table = 'profile_bienes';

    // protected $dates = ['deleted_at'];

    protected $fillable = [
        'id_tipo_bien',
        'profile_id',
        'estatus',
        'codigo'
    ];

    
     public function tipobien()
     {
         return $this->belongsTo('App\Models\Bienes\TipoBienes','id_tipo_bien');
     }
 

}
