<?php

namespace App\Models\Profile;

use App\Models\GeoMunicipio;
use App\Models\GeoDepartamento;
use Illuminate\Database\Eloquent\Model;

class ProfileAdditional extends Model
{
    protected $table = 'profile_additional';

    protected $fillable = [
        'profile_id',
        'address',
        'num_outside',
        'num_inside',
        'colony',
        'city',
        'state',
        'zip_code',
        'file_address'
    ];

    public function estado() {
        return $this->hasOne(GeoDepartamento::Class, 'id', 'state');
    }

    public function municipio()
    {
        return $this->hasOne(GeoMunicipio::Class, 'id', 'city');
    }
}
