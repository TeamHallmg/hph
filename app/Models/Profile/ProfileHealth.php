<?php

namespace App\Models\Profile;

use Illuminate\Database\Eloquent\Model;

class ProfileHealth extends Model
{
    protected $table = 'profile_health';

    protected $fillable = [
        'profile_id',
        'imss',
        'policy',
        'blood_type',
        'allergies',
        'current_condition',
        'family_background',
        'cx',
        'comments',
        'contact1',
        'phone1',
        'relationship1',
        'contact2',
        'phone2',
        'relationship2'
    ];

    public function perfilSaludDet() {
        return $this->hasMany(ProfileHealthDet::class);
    }
}
