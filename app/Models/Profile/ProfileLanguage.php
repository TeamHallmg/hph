<?php

namespace App\Models\Profile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProfileLanguage extends Model
{
    use SoftDeletes;

    protected $table = 'profile_language';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'profile_id',
        'language',
        'spoken',
        'reading',
        'writing'
    ];
}
