<?php

namespace App\Models\CuestionarioSalida;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class cuestionario_salida extends Model
{
    protected $table = 'cuestionario_salidas';

    protected $fillable = [
        'user_id',
        'user_validado',
        'factores',
        'motivo_renuncia',
        'problema_con_jefe',
        'problema_con_jefe_decripcion',
        'renuncia_mejora_puesto_sueldo',
        'piden_dinero',
        'piden_dinero_descripcion',
        'mas_gusto',
        'menos_gusto',
        'recomendaria',
        'recomendaria_descripcion',
        'volveria_con_nosotros',
        'volveria_con_nosotros_descripcion',
        'ambiente_trabajo',
        'cambios_sugiere',
        'comentarios',
        'estatus'
    ];

    protected $casts = [
        'factores' => 'array'
    ];

    protected $dates = ['created_at'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->withDefault();
    }
    public function userValidado()
    {
        return $this->belongsTo(User::class, 'user_validado', 'id')->withDefault();
    }

    // public function getCreatedAtAttribute($date)
    // {
    //     return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
    // }

    public function createdAnio($date)
    {
        // return Carbon::createFromFormat('d-m-Y', $date)->format('Y-m');
        /* poner fecha en español (mes)
        setlocale(LC_ALL,"es_ES");
        return strftime("%B de %Y"); */
    }
}
