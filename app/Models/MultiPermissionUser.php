<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\User;

class MultiPermissionUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'multi_permission_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['permission_id', 'user_id'];

    public function permission()
    {
        return $this->belongsTo(MultiPermission::class, 'permission_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
