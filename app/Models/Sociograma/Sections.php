<?php

namespace App\Models\Sociograma;

use Illuminate\Database\Eloquent\Model;

// use Illuminate\Support\Database\SoftDelete;

class Sections extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sociograma_sections';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['section'];
}
