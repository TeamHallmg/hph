<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'departments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'direction_id'];

    /*public function direction()
    {
        return $this->belongsTo(Direction::class, 'direction_id', 'id');
    }*/

    public function area()
    {
        return $this->belongsTo(Area::class, 'area_id', 'id');
    }

    public function direction()
    {
        return $this->belongsTo(Direction::class, 'direction_id', 'id');
    }

    public function jobs()
    {
        return $this->hasMany(JobPosition::class, 'department_id', 'id');
    }

    /*public function areas()
    {
        return $this->hasMany(Area::class, 'department_id', 'id');
    }*/

    /*public function getJobsCount() {
        $count = 0;
        foreach($this->areas as $area) {
            $count += $area->jobs->count();
        }
        return $count;
    }*/

    public function getJobsCount() {
        $count = 0;
        foreach($this->jobs as $job) {
            $count++;
        }
        return $count;
    }

    /*public function getEmployeesCount() {
        $count = 0;
        foreach($this->areas as $area) {
            foreach($area->jobs as $job) {
                $count += $job->employees->count();
            }
        }
        return $count;
    }*/

    public function getEmployeesCount() {
        $count = 0;
        foreach($this->jobs as $job) {
            $count += $job->employees->count();
        }
        return $count;
    }

    /*public static function createCascade($direccion = 'Sin Dirección', $department = 'Sin Departamento'){

        $department = is_null($department)?'Sin Departamento':$department;
        if($department === 'Sin Departamento' || !is_numeric($department)){
            return Department::firstOrCreate([
                'name' => $department,
                'direction_id' => Direction::createCascade($direccion)->id,
            ]);
        }else{
            return Department::find($department);
        }*/
        /*
        return Department::when($department === 'Sin Departamento' || !is_numeric($department), function($q) use($direccion, $department) {
            $q->firstOrCreate([
                'name' => $department,
                'direction_id' => Direction::createCascade($direccion)->id,
            ]);
        }, function ($q) {
            $q->find($department);
        })->first();
        */

    //}

    public static function createCascade($direccion = 'Sin Dirección', $area = 'Sin Área', $department = 'Sin Departamento'){

        $department = is_null($department)?'Sin Departamento':$department;
        if($department === 'Sin Departamento' || !is_numeric($department)){
            return Department::firstOrCreate([
                'name' => $department,
                'area_id' => Area::createCascade($direccion, $area)->id,
            ]);
        }else{
            return Department::find($department);
        }
        /*
        return Department::when($department === 'Sin Departamento' || !is_numeric($department), function($q) use($direccion, $department) {
            $q->firstOrCreate([
                'name' => $department,
                'direction_id' => Direction::createCascade($direccion)->id,
            ]);
        }, function ($q) {
            $q->find($department);
        })->first();
        */

    }

    public function deleted_reason() {
        return $this->morphMany(DeletedReason::class, 'deleted_reason');
    }

}