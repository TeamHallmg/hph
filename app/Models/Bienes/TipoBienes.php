<?php

namespace App\Models\Bienes;

use Illuminate\Database\Eloquent\Model;

class TipoBienes extends Model
{
	protected $table = 'tipo_bienes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['codigo','nombre',
     'descripcion',
     'estatus'
  ];



    public function jobpositionsbienes() {
        return $this->hasMany('App\Models\jobPositionsBienes','id_tipo_bien')->where('estatus',1);
    }


    public function tipobiendetalles() {
        return $this->hasMany('App\Models\Bienes\TipoBienesDetalle','id_tipo_bien')->where('estatus',1);
    }



    // public function bienpuesto(){
    //     return $this->belongsToMany('App\Models\JobPosition','job_positions_bienes','id_tipo_bien','id_tipo_bien')
    //         ->withPivot('id_job_positions');
    // }

    // public function bien_pues(){
    //     return $this->belongsToMany('App\Models\RedSocial\Image','redsocial_post_image','post_id','image_id')
    //         ->withPivot('id');
    // }
}
