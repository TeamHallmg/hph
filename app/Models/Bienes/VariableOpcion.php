<?php

namespace App\Models\Bienes;

use Illuminate\Database\Eloquent\Model;

class VariableOpcion extends Model
{
	protected $table = 'variable_opcion';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_variable',
     'valor',
     'descripcion'
  ];
}
