<?php

namespace App\Models\Bienes;

use Illuminate\Database\Eloquent\Model;

class VariableValor extends Model
{
	protected $table = 'variable_valor';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_referencia',
     'id_variable',
     'valor',
     'descripcion',
     'indice'
  ];

}
