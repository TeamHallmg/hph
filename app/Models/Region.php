<?php

namespace App\Models;

use App\Employee;
use App\Models\Anuncios\Announcement;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Region extends Model
{
	use SoftDeletes;
    //
     /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * AnnouncementRegion has many Announcements.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function announcements()
    {
    	// hasMany(RelatedModel, foreignKeyOnRelatedModel = announcementRegion_id, localKey = id)
    	return $this->hasMany(Announcement::class);
    }

    public function employees()
    {
        return $this->hasMany(Employee::class, 'region_id', 'id');
    }

    public function getSchedule($begin, $end = null, $whole_week = false){
        if($this->workshift == '6x2'){
            if(is_null($end)){
                return $this->hasOne(Workshift::class)
                ->where('date', $begin)
                ->first();
            }else{
                return $this->hasMany(Workshift::class)
                ->whereBetween('date', [$begin, $end])
                ->get();
            }
        }else{
            if(is_null($end) && !$whole_week){
                $cal =  $this->hasOne(WorkshiftFixed::class)
                ->where('day', date('D', strtotime($begin)))
                ->first();
                $cal->date = $begin;
                return $cal;
            }else{
                return $this->hasMany(WorkshiftFixed::class)->get();
            }
        }
    }
    
    public function getDays($beginDate, $endDate, $onlyLabor = false){
        $days = 0;
        if($this->workshift == '6x2'){
            $begin = date('Y-m-d', $beginDate);
            $end = date('Y-m-d', $endDate);
            $days = $this->hasMany(Workshift::class)
                ->whereBetween('date', [$begin, $end]);
            if($onlyLabor){
                $days->where('labor', 1);
            }
            $days = $days->get();
        } else {
            $begin = $beginDate;
            $days = [];
            $days_d = [];
            while ($begin <= $endDate) {
                $days_d[] = [
                    'd' => date('D', $begin),
                    'date' => date('Y-m-d', $begin),
                ];
                $begin = $begin + 24 * 60 * 60;
            }
            foreach($days_d as $d){
                $sch = $this->hasOne(WorkshiftFixed::class)
                ->when($onlyLabor, function($q){
                    $q->where('labor', 1);
                })
                ->where('day', $d)
                ->first();
                if($sch){
                    $sch->date = $d['date'];
                    $days[] = $sch;
                }
            }
            $days = collect($days);
        }
        return $days;
    }

    public function isDayWorkable($date){
        $workable = false;
        if($this->workshift == '6x2'){
            $day = $this->hasOne(Workshift::class)->where('labor', 1)->where('date', $date)->first();
            return !!$day;
        }else{
            $day = date('D', strtotime($date));
            $day = $this->hasOne(WorkshiftFixed::class)->where('day', $day)->first();
            return !!$day->labor;
        }
    }
}
