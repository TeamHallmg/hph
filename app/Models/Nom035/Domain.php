<?php

namespace App\Models\Nom035;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = ['name', 'descriptions'];

     public function dimension()
    {
        return $this->hasMany(Dimension::class, 'domain_id', 'id');
    }

     public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
