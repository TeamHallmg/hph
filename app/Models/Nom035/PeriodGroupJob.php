<?php

namespace App\Models\Nom035;

use Illuminate\Database\Eloquent\Model;

class PeriodGroupJob extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nom035_period_group_job';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['period_id', 'group_job_id'];
}
