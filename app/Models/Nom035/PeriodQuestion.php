<?php

namespace App\Models\Nom035;

use Illuminate\Database\Eloquent\Model;

class PeriodQuestion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nom035_period_question';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['period_id', 'question_id'];

    public function period()
    {
        return $this->belongsTo(NormPeriod::class, 'period_id', 'id');
    }

    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id', 'id');
    }
}
