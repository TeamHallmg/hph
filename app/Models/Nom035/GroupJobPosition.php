<?php

namespace App\Models\Nom035;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\JobPosition;

class GroupJobPosition extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nom035_group_job_positions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function jobPositions()
    {
        return $this->belongsToMany(JobPosition::class,
            with(new JobPositionPivot)->getTable(),
            'group_job_position_id',
            'job_position_id');
    }

    public function periods()
    {
        return $this->belongsToMany(NormPeriod::class,
            with(new PeriodGroupJob)->getTable(),
            'group_job_id',
            'period_id');
    }
}
