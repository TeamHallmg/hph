<?php

namespace App\Models\Nom035;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = ['name', 'descriptions'];

     public function domain()
    {
        return $this->hasMany(Domain::class, 'category_id', 'id');
    }
}
