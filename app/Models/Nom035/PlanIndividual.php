<?php

namespace App\Models\Nom035;

use App\User;
use Illuminate\Database\Eloquent\Model;

class PlanIndividual extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nom035_plan_individual';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['avances', 'avances_posterior', 'user_id', 'period_id', 'created_by'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function period()
    {
        return $this->belongsTo(NormPeriod::class, 'period_id', 'id');
    }

    public function created_by_user(){

        return $this->belongsTo(User::class, 'created_by', 'id');
    }
}
