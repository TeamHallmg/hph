<?php

namespace App\Models\Nom035;

use Illuminate\Database\Eloquent\Model;

class QuestionGrouper extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nom035_question_groupers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['question', 'cuestionario', 'shown_on'];

    public function question()
    {
        return $this->hasMany(Question::class, 'grouper_id', 'id');
    }
}
