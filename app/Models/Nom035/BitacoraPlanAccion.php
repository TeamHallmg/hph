<?php

namespace App\Models\Nom035;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BitacoraPlanAccion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nom035_bitacora_plan_accion';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_plan', 'avances', 'evidencia', 'created_by'];

    public function planAccion()
    {
        return $this->belongsTo(PlanAccion::class, 'id_plan', 'id');
    }

    public function created_by_user(){

        return $this->belongsTo(User::class, 'created_by', 'id');
    }
}
