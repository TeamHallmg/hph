<?php

namespace App\Models\Nom035;

use Illuminate\Database\Eloquent\Model;

class PeriodGroupArea extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nom035_period_group_area';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['period_id', 'group_area_id'];
}
