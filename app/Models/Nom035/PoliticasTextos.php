<?php

namespace App\Models\Nom035;

use Illuminate\Database\Eloquent\Model;

class PoliticasTextos extends Model
{
    protected $table = 'nom035_politicas_textos';

    protected $fillable = ['description', 'region_id', 'enterprise_id', 'sucursal_id'];
}
