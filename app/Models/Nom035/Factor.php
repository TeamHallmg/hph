<?php

namespace App\Models\Nom035;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Factor extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nom035_factors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'type'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function questions()
    {
        return $this->hasMany(Question::class, 'factor_id', 'id');
    }

    public function allQuestionBelongsToASameParent(){
        if($this->questions->isNotEmpty() && $this->questions[0]->parent){
            $factor = $this->questions[0]->parent_factor_id;
            $f_count = $this->questions()->where('parent_factor_id', $factor)->count();
            $count = $this->questions()->count();
            return $f_count === $count;
        }
        return false;
    }

    public function getParentQuestionBelongsToASameParent(){
        if($this->allQuestionBelongsToASameParent()){
            return $this->questions[0]->parent_factor_id;
        }
        return null;
    }
}
