<?php

namespace App\Models\Nom035;

use Illuminate\Database\Eloquent\Model;

use App\Models\Department;

class DepartmentPivot extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nom035_departments_pivot';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['department_id', 'group_department_id'];

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }

    public function groupDepartment()
    {
        return $this->belongsTo(GroupDepartment::class, 'group_department_id', 'id');
    }
}
