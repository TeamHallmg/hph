<?php

namespace App\Models\Nom035;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Area;

class GroupArea extends Model
{
    use SoftDeletes;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nom035_group_areas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function areas()
    {
        return $this->belongsToMany(Area::class,
            with(new AreaPivot)->getTable(),
            'group_area_id',
            'area_id');
    }

    public function periods()
    {
        return $this->belongsToMany(NormPeriod::class,
            with(new PeriodGroupArea)->getTable(),
            'group_area_id',
            'period_id');
    }
}
