<?php

namespace App\Models\Nom035;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nom035_questions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['question', 'positive', 'grouper_id', 'parent_factor_id', 'factor_id', 'dimension_id', 'domain_id', 'category_id'];

    public function group()
    {
        return $this->belongsTo(QuestionGrouper::class, 'grouper_id', 'id');
    }

    public function factor()
    {
        return $this->belongsTo(Factor::class, 'factor_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo(Factor::class, 'parent_factor_id', 'id');
    }

    public function periods()
    {
        return $this->belongsToMany(NormPeriod::class, 
            with(new PeriodQuestion)->getTable(),
            'question_id',
            'period_id');
    }

    public function answers()
    {
        return $this->hasMany(Answer::class, 'question_id', 'id');
    }

    public function answer()
    {
        return $this->hasOne(Answer::class, 'question_id', 'id');
    }

    public function dimension()
    {
        return $this->belongsTo(Dimension::class, 'dimension_id', 'id');
    }

    public function domain()
    {
        return $this->belongsTo(Domain::class, 'domain_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
