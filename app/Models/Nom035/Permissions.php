<?php

namespace App\Models\Nom035;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Models\Region;

// use Illuminate\Support\Database\SoftDelete;

class Permissions extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nom035_permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'region_id', 'section_id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->withTrashed();
    }

    public function region()
    {
        return $this->belongsTo(Region::class, 'region_id');
    }

    public function section()
    {
        return $this->belongsTo(Sections::class, 'section_id');
    }
}
