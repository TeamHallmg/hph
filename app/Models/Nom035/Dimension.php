<?php

namespace App\Models\Nom035;

use Illuminate\Database\Eloquent\Model;

class Dimension extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = ['name', 'descriptions'];

     public function domain()
    {
        return $this->belongsTo(Domain::class, 'domain_id', 'id');
    }
}
