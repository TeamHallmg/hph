<?php

namespace App\Models\Nom035;

use Illuminate\Database\Eloquent\Model;

use App\Models\Area;

class AreaPivot extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nom035_areas_pivot';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['area_id', 'group_area_id'];

    public function area()
    {
        return $this->belongsTo(Area::class, 'area_id', 'id');
    }

    public function groupArea()
    {
        return $this->belongsTo(GroupArea::class, 'group_area_id', 'id');
    }
}
