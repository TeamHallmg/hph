<?php

namespace App\Models\Nom035;

use App\User;
use Illuminate\Database\Eloquent\Model;

class PlanAccion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nom035_plan_accion';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['period_id', 'category_id', 'domains_ids', 'dimensions_ids', 'accion', 'dirigido', 'resultados', 'fecha_implementar', 'medio_verificacion', 'fecha_verificacion', 'area_responsable', 'control_avances', 'evaluacion_posterior', 'centro_trabajo', 'region_id', 'created_by'];

    public function period()
    {
        return $this->belongsTo(NormPeriod::class, 'period_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function created_by_user(){

        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function bitacora()
    {
        return $this->hasMany(BitacoraPlanAccion::class, 'id_plan', 'id');
    }
}
