<?php

namespace App\Models\Nom035;

use Illuminate\Database\Eloquent\Model;

use App\Models\JobPosition;

class JobPositionPivot extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nom035_job_positions_pivot';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['job_position_id', 'group_job_position_id'];

    public function jobPosition()
    {
        return $this->belongsTo(JobPosition::class, 'job_position', 'id');
    }

    public function groupJobPosition()
    {
        return $this->belongsTo(GroupJobPosition::class, 'group_job_position_id', 'id');
    }
}
