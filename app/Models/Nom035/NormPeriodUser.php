<?php

namespace App\Models\Nom035;

use Illuminate\Database\Eloquent\Model;

use App\User;

class NormPeriodUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nom035_period_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['period_id', 'user_id', 'status'];

    public function period()
    {
        return $this->belongsTo(NormPeriod::class, 'period_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
