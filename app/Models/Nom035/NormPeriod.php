<?php

namespace App\Models\Nom035;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;

class NormPeriod extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nom035_periods';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'status', 'evaluation_id', 'created_by', 'enterprise_id', 'sucursal_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Scope a query to only include Open
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOpen($query)
    {
        return $query->where('status', 'Abierto');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 
            with(new NormPeriodUser)->getTable(),
            'period_id',
            'user_id')->withTrashed()
        ->withPivot('status');
    }

    public function questions()
    {
        return $this->belongsToMany(Question::class, 
            with(new PeriodQuestion)->getTable(),
            'period_id',
            'question_id');
    }

    public function getTypeOfQuestions()
    {
        $types = $this->questions->groupBy('cuestionario');
        /*if(count($types) > 1){
            dd('Aqui hay más de dos tipos y esto no debe de pasar');
        }*/
        $types_array = array();
        foreach($types as $key => $type){
          $types_array[] = $key;
        }
        //return "";
        return $types_array;
    }

    public function answers()
    {
        return $this->hasMany(Answer::class, 'period_id', 'id');
    }

    public function evaluation()
    {
        return $this->belongsTo(Evaluation::class, 'evaluation_id', 'id');
    }

    public function groupAreas()
    {
        return $this->belongsToMany(GroupArea::class,
            with(new PeriodGroupArea)->getTable(),
            'period_id',
            'group_area_id');
    }

    public function groupJobs()
    {
        return $this->belongsToMany(GroupJobPosition::class,
            with(new PeriodGroupJob)->getTable(),
            'period_id',
            'group_job_id');
    }

    public function planIndividual()
    {
        return $this->hasOne(PlanIndividual::class, 'period_id', 'id');
    }
}
