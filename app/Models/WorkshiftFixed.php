<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkshiftFixed extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'workshift_fixed';
    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['day','labor','region_id','schedule_id'];

    /**
     * WorkshiftFixed belongs to Region.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
    	// belongsTo(RelatedModel, foreignKey = region_id, keyOnRelatedModel = id)
    	return $this->belongsTo(Region::class);
    }

    /**
     * WorkshiftFixed belongs to Schdedule.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function schedule()
    {
    	// belongsTo(RelatedModel, foreignKey = schdedule_id, keyOnRelatedModel = id)
    	return $this->belongsTo(Schedule::class);
    }
}
