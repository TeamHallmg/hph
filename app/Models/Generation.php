<?php

namespace App\Models;

use DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Generation extends Model
{
    use SoftDeletes;

    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'generations';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'description', 'start', 'end'];

	public function getRangeKey() {
		if(!$this->start && !$this->end) {
			return null;
		} elseif($this->start && !$this->end) {
			return $this->start;
		} elseif(!$this->start && $this->end) {
			return $this->end;
		} else {
			return $this->start.'-'.$this->end;
		}
	}

	public static function getRangeKeyByBirthDay($birthday_year) {
		$generations = Generation::get();
		foreach($generations as $generation) {
			if(is_null($generation->end)){
				if($birthday_year >= $generation->start) {
					return $generation->getRangeKey();
				}
			}else if($birthday_year >= $generation->start && $birthday_year <= $generation->end) {
				return $generation->getRangeKey();
			}
		}
		return null;
	}

	public static function getNameByBirthDay($birthday_year) {
		$generations = Generation::get();
		foreach($generations as $generation) {
			if($birthday_year >= $generation->start && $birthday_year <= $generation->end) {
				return $generation->name;
			}
		}
		return null;
	}

	public static function initialize() {
		try {
			DB::beginTransaction();
			$data = [
				[
					'name' => 'Silent Generation',
					'description' => 'Conflíctos bélicos',
					'start' => 1930,
					'end' => 1948
				], [
					'name' => 'Baby Boom',
					'description' => 'Paz y explosión demográfica',
					'start' => 1949,
					'end' => 1968
				], [
					'name' => 'Generación X',
					'description' => 'Crisis del 73 y transición española',
					'start' => 1969,
					'end' => 1980
				], [
					'name' => 'Generación Y',
					'description' => 'Inicio de la digitalización',
					'start' => 1981,
					'end' => 1993
				], [
					'name' => 'Generación Z',
					'description' => 'Expansión masiva de internet',
					'start' => 1994,
					'end' => 2010
				], [
					'name' => 'Generación Alfa',
					'description' => 'Me lo dijo un pajarito',
					'start' => 2011,
					'end' => null
				]
			];

			foreach($data as $row) {
				Generation::updateOrCreate([
					'start' => $row['start'],
					'end' => $row['end']
				], [
					'name' => $row['name'],
					'description' => $row['description']
				]);
			}
			DB::commit();
		} catch(\Throwable $th) {
			DB::rollback();
			dd($th);
		}
	}
}
