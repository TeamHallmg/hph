<?php

namespace App\Models\ClimaOrganizacional;

use Illuminate\Database\Eloquent\Model;

use App\Models\Direction;

class AreaPivot extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clima_areas_pivot';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['area_id', 'group_area_id'];

    public function area()
    {
        return $this->belongsTo(Direction::class, 'area_id', 'id');
    }

    public function groupArea()
    {
        return $this->belongsTo(GroupArea::class, 'group_area_id', 'id');
    }
}
