<?php

namespace App\Models\Cuestionarios;

use Illuminate\Database\Eloquent\Model;

class Etiquetas extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cuestionarios_etiquetas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];
}
