<?php

namespace App\Models\Templates;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TemplatesPlansSends extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'template_plans_sends';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */ 
    
    protected $fillable = [
        'template_modules_id',
        'name',
        'date',
        'id_template',
        'status',
        'max_sends',
    ];

    public function modulo() {
        return $this->belongsTo(TemplatesModules::class, 'template_modules_id', 'id');
    }

    public function template() {
        return $this->belongsTo(Templates::class, 'id_template', 'id');
    }

    public function colaboradores() {
        return $this->hasMany(TemplatesPlansSendsDetails::class, 'plans_id', 'id');
    }

    public function colaboradores_id() {
        return $this->hasMany(TemplatesPlansSendsDetails::class, 'plans_id', 'id')->pluck('user_id');
    }
    
    public function enviados() {
        return $this->hasMany(TemplatesPlansSendsDetails::class, 'plans_id', 'id')->where('status_send','enviado');
    }
    
    public function errores() {
        return $this->hasMany(TemplatesPlansSendsDetails::class, 'plans_id', 'id')->where('status_send','error');
    }
    
}