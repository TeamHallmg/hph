<?php

namespace App\Models\Templates;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TemplatesConfigemail extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'template_configemail';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['templates_id','from_email', 
    'from_name', 
    'cc_email', 
    'cc_name', 
    'subject'
    ];

}