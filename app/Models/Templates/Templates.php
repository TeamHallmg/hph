<?php

namespace App\Models\Templates;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Templates extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'templates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['doc_cert', 
    'template_modules_id', 
    'name',
    'description',
    'out',
    'template_static',
    'status',
    'created_by'
    ];

    public function modulo() {
        return $this->belongsTo(TemplatesModules::class, 'template_modules_id', 'id');
    }

    public function format() {
        return $this->belongsTo(TemplatesDescription::class,'id','id_template');
    }

    public function configemial() {
        return $this->belongsTo(TemplatesConfigemail::class,'id','templates_id');
    }

}