<?php

namespace App\Models\Templates;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class TemplatesPlansSendsDetails extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'template_plans_sends_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */ 
    
    protected $fillable = [
        'plans_id',
        'user_id',
        'date_send',
        'email_veriquied',
        'email_send',
        'status_send',
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id')->withDefault();
    }

}