<?php

namespace App;

use App\Models\Region;
use App\Models\JobPosition;
use App\Models\Enterprise;
use App\Models\Profile\ProfileContrato;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'employees';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['idempleado', 'nombre', 'paterno', 'materno', 'fuente', 'rfc', 'curp', 'nss', 'correoempresa', 'correopersonal', 'nacimiento', 'sexo', 'civil', 'telefono', 'extension', 'celular', 'ingreso', 'fechapuesto', 'jefe', 'direccion', 'department', 'seccion', 'job_position_id', 'grado', 'region_id', 'sucursal', 'enterprise_id', 'division', 'marca', 'centro', 'checador', 'turno', 'tiponomina', 'clavenomina', 'nombrenomina', 'generalista', 'relacion', 'contrato', 'horario', 'jornada', 'calculo', 'vacaciones', 'flotante', 'base', 'rol', 'password', 'extra1', 'extra2', 'extra3', 'extra4', 'extra5', 'fecha', 'version'];

    public function getFullNameAttribute(){
        return $this->nombre . ' ' . $this->paterno . ' ' . $this->materno;
    }

    public function user()
    {
        return $this->hasOne(User::class, 'employee_id');
    }

    public function employees()
    {
        return $this->hasMany(Employee::class, 'jefe', 'idempleado');
    }
  
    public function boss(){
        return $this->hasOne(Employee::class, 'idempleado', 'jefe');
    }

    public function jobPosition()
    {
        return $this->belongsTo(JobPosition::class, 'job_position_id', 'id');
    }

    public function region()
    {
        return $this->belongsTo(Region::class, 'region_id', 'id');
    }
    
    public function getPuestoName($trashed = false){
        $job = $trashed?$this->jobPosition:$this->jobPosition_wt;
        return $job?$this->jobPosition->name:'-';
    }

    public function getDepartmentName($trashed = false){
        $job = ($this->jobPosition)?$this->jobPosition->area->department:false;
        return $job?$job->name:'-';
    }

    public function getRegionName($trashed = false){
        $job = ($this->region)?$this->region:false;
        return $job?$job->name:'-';
    }

    public function department(){
        if(!is_null($this->jobPosition)){
            return $this->jobPosition->department;
        }
        return null;
    }

    public function area(){
        if(!is_null($this->department)){
            return $this->department->area;
        }
        return null;
    }

    
    public function area_n($trashed = false){
        if($job = $trashed?$this->jobPosition_wt:$this->jobPosition){
            return $trashed?$job->area_wt:$job->area;
        }
        return null;
    }

    public function department_n($trashed = false){
        if($area = $this->area_n($trashed)){
            return $trashed?$area->department_wt:$area->department;
        }
        return null;
    }

    public function direction($trashed = false){
        if($department = $this->department_n($trashed)){
            return $trashed?$department->direction_wt:$department->direction;
        }
        return null;
    }


    public function getAge() {
        $calculo = Carbon::parse($this->fechanacimiento)->age;
        return ($calculo == 0?'N/A': $calculo);
    }

    public function contrato() {
        return $this->belongsTo(ProfileContrato::class, 'profile_id', 'id');
    }


    public function num2letras($num, $fem = false, $dec = true) { 
        $matuni[2]  = "dos"; 
        $matuni[3]  = "tres"; 
        $matuni[4]  = "cuatro"; 
        $matuni[5]  = "cinco"; 
        $matuni[6]  = "seis"; 
        $matuni[7]  = "siete"; 
        $matuni[8]  = "ocho"; 
        $matuni[9]  = "nueve"; 
        $matuni[10] = "diez"; 
        $matuni[11] = "once"; 
        $matuni[12] = "doce"; 
        $matuni[13] = "trece"; 
        $matuni[14] = "catorce"; 
        $matuni[15] = "quince"; 
        $matuni[16] = "dieciseis"; 
        $matuni[17] = "diecisiete"; 
        $matuni[18] = "dieciocho"; 
        $matuni[19] = "diecinueve"; 
        $matuni[20] = "veinte"; 
        $matunisub[2] = "dos"; 
        $matunisub[3] = "tres"; 
        $matunisub[4] = "cuatro"; 
        $matunisub[5] = "quin"; 
        $matunisub[6] = "seis"; 
        $matunisub[7] = "sete"; 
        $matunisub[8] = "ocho"; 
        $matunisub[9] = "nove"; 
      
        $matdec[2] = "veint"; 
        $matdec[3] = "treinta"; 
        $matdec[4] = "cuarenta"; 
        $matdec[5] = "cincuenta"; 
        $matdec[6] = "sesenta"; 
        $matdec[7] = "setenta"; 
        $matdec[8] = "ochenta"; 
        $matdec[9] = "noventa"; 
        $matsub[3]  = 'mill'; 
        $matsub[5]  = 'bill'; 
        $matsub[7]  = 'mill'; 
        $matsub[9]  = 'trill'; 
        $matsub[11] = 'mill'; 
        $matsub[13] = 'bill'; 
        $matsub[15] = 'mill'; 
        $matmil[4]  = 'millones'; 
        $matmil[6]  = 'billones'; 
        $matmil[7]  = 'de billones'; 
        $matmil[8]  = 'millones de billones'; 
        $matmil[10] = 'trillones'; 
        $matmil[11] = 'de trillones'; 
        $matmil[12] = 'millones de trillones'; 
        $matmil[13] = 'de trillones'; 
        $matmil[14] = 'billones de trillones'; 
        $matmil[15] = 'de billones de trillones'; 
        $matmil[16] = 'millones de billones de trillones'; 
        
        //Zi hack
        $float=explode('.',$num);
        $num=$float[0];
      
        $num = trim((string)@$num); 
        if ($num[0] == '-') { 
           $neg = 'menos '; 
           $num = substr($num, 1); 
        }else 
           $neg = ''; 
        while ($num[0] == '0') $num = substr($num, 1); 
        if ($num[0] < '1' or $num[0] > 9) $num = '0' . $num; 
        $zeros = true; 
        $punt = false; 
        $ent = ''; 
        $fra = ''; 
        for ($c = 0; $c < strlen($num); $c++) { 
           $n = $num[$c]; 
           if (! (strpos(".,'''", $n) === false)) { 
              if ($punt) break; 
              else{ 
                 $punt = true; 
                 continue; 
              } 
      
           }elseif (! (strpos('0123456789', $n) === false)) { 
              if ($punt) { 
                 if ($n != '0') $zeros = false; 
                 $fra .= $n; 
              }else 
      
                 $ent .= $n; 
           }else 
      
              break; 
      
        } 
        $ent = '     ' . $ent; 
        if ($dec and $fra and ! $zeros) { 
           $fin = ' coma'; 
           for ($n = 0; $n < strlen($fra); $n++) { 
              if (($s = $fra[$n]) == '0') 
                 $fin .= ' cero'; 
              elseif ($s == '1') 
                 $fin .= $fem ? ' una' : ' un'; 
              else 
                 $fin .= ' ' . $matuni[$s]; 
           } 
        }else 
           $fin = ''; 
        if ((int)$ent === 0) return 'Cero ' . $fin; 
        $tex = ''; 
        $sub = 0; 
        $mils = 0; 
        $neutro = false; 
        while ( ($num = substr($ent, -3)) != '   ') { 
           $ent = substr($ent, 0, -3); 
           if (++$sub < 3 and $fem) { 
              $matuni[1] = 'una'; 
              $subcent = 'os'; 
           }else{ 
              $matuni[1] = $neutro ? 'un' : 'uno'; 
              $subcent = 'os'; 
           } 
           $t = ''; 
           $n2 = substr($num, 1); 
           if ($n2 == '00') { 
           }elseif ($n2 < 21) 
              $t = ' ' . $matuni[(int)$n2]; 
           elseif ($n2 < 30) { 
              $n3 = $num[2]; 
              if ($n3 != 0) $t = 'i' . $matuni[$n3]; 
              $n2 = $num[1]; 
              $t = ' ' . $matdec[$n2] . $t; 
           }else{ 
              $n3 = $num[2]; 
              if ($n3 != 0) $t = ' y ' . $matuni[$n3]; 
              $n2 = $num[1]; 
              $t = ' ' . $matdec[$n2] . $t; 
           } 
           $n = $num[0]; 
           if ($n == 1) { 
              $t = ' ciento' . $t; 
           }elseif ($n == 5){ 
              $t = ' ' . $matunisub[$n] . 'ient' . $subcent . $t; 
           }elseif ($n != 0){ 
              $t = ' ' . $matunisub[$n] . 'cient' . $subcent . $t; 
           } 
           if ($sub == 1) { 
           }elseif (! isset($matsub[$sub])) { 
              if ($num == 1) { 
                 $t = ' mil'; 
              }elseif ($num > 1){ 
                 $t .= ' mil'; 
              } 
           }elseif ($num == 1) { 
              $t .= ' ' . $matsub[$sub] . '?n'; 
           }elseif ($num > 1){ 
              $t .= ' ' . $matsub[$sub] . 'ones'; 
           }   
           if ($num == '000') $mils ++; 
           elseif ($mils != 0) { 
              if (isset($matmil[$sub])) $t .= ' ' . $matmil[$sub]; 
              $mils = 0; 
           } 
           $neutro = true; 
           $tex = $t . $tex; 
        } 
        $tex = $neg . substr($tex, 1) . $fin; 
        //Zi hack --> return ucfirst($tex);
        // $end_num=ucfirst($tex).' pesos '.$float[1].'/100 M.N.';
        return $tex; 
     }



    public function dateToSpanish($date, $has_year = false) {
      $month = date("F", strtotime($date));
      $year = date("Y", strtotime($date));
      $date = date("d \\d\\e", strtotime($date));
      $meses = array(
            'January' => 'Enero',
            'February' => 'Febrero',
            'March' => 'Marzo',
            'April' => 'Abril',
            'May' => 'Mayo',
            'June' => 'Junio',
            'July' => 'Julio',
            'August' => 'Agosto',
            'September' => 'Septiembre',
            'October' => 'Octubre',
            'November' => 'Noviembre',
            'December' => 'Diciembre'
      );
      
        $fecha = $this->diasenletra($date) .' de '.$meses[$month].' '.($year > 1999?'del':'de').' '.$year;
        return  $fecha;
     
    }



    public function diasenletra($numero){


      if ($numero >= 30 && $numero <= 39)
      {
          $num_letra = "treinta ";
   
          if ($numero > 30)
              $num_letra = $num_letra."y ".$this->unidad($numero - 30);
      }
      else if ($numero >= 20 && $numero <= 29)
      {
          if ($numero == 20)
              $num_letra = "veinte ";
          else
              $num_letra = "veinti".$this->unidad1($numero - 20);
      }
      else if ($numero >= 10 && $numero <= 19)
      {
          switch ($numero)
          {
              case 10:
              {
                  $num_letra = "Diez ";
                  break;
              }
              case 11:
              {
                  $num_letra = "Once ";
                  break;
              }
              case 12:
              {
                  $num_letra = "Doce ";
                  break;
              }
              case 13:
              {
                  $num_letra = "Trece ";
                  break;
              }
              case 14:
              {
                  $num_letra = "Catorce ";
                  break;
              }
              case 15:
              {
                  $num_letra = "Quince ";
                  break;
              }
              case 16:
              {
                  $num_letra = "Dieciseis ";
                  break;
              }
              case 17:
              {
                  $num_letra = "Diecisiete ";
                  break;
              }
              case 18:
              {
                  $num_letra = "Dieciocho ";
                  break;
              }
              case 19:
              {
                  $num_letra = "Diecinueve ";
                  break;
              }
          }
      }
      else
          $num_letra = $this->unidad($numero);
   
      return $num_letra;
}



public function unidad($numero)
{
    switch ($numero)
    {
        case 9:
        {
            $num = "Nueve";
            break;
        }
        case 8:
        {
            $num = "Ocho";
            break;
        }
        case 7:
        {
            $num = "Siete";
            break;
        }
        case 6:
        {
            $num = "Seis";
            break;
        }
        case 5:
        {
            $num = "Cinco";
            break;
        }
        case 4:
        {
            $num = "Cuatro";
            break;
        }
        case 3:
        {
            $num = "Tres";
            break;
        }
        case 2:
        {
            $num = "Dos";
            break;
        }
        case 1:
        {
            $num = "Uno";
            break;
        }
    }
    return $num;
}

public function unidad1($numero)
{
    switch ($numero)
    {
        case 9:
        {
            $num = "nueve";
            break;
        }
        case 8:
        {
            $num = "ocho";
            break;
        }
        case 7:
        {
            $num = "siete";
            break;
        }
        case 6:
        {
            $num = "seis";
            break;
        }
        case 5:
        {
            $num = "cinco";
            break;
        }
        case 4:
        {
            $num = "cuatro";
            break;
        }
        case 3:
        {
            $num = "tres";
            break;
        }
        case 2:
        {
            $num = "dos";
            break;
        }
        case 1:
        {
            $num = "uno";
            break;
        }
    }
    return $num;
}
 

    public function edad($fecha) {
        $calculo = Carbon::parse($fecha)->age;
        $edades = $this->num2letras($calculo);
        return $edades;
    }

  public function enterprise(){
        
    return $this->belongsTo(Enterprise::class, 'enterprise_id', 'id');
  }


}
