<?php

namespace App;

use DB;
use App\Employee;
use App\Models\Announcement\AnnouncementUser;
use App\Models\Role;
use App\Models\MultiPermission;
use App\Models\Permission;
use App\Models\ClimaOrganizacional\Permissions as ClimaPermissions;
use App\Models\Nps\Permissions as NpsPermissions;
use App\Models\Sociograma\Permissions as SociogramaPermissions;
use App\Models\Nom035\Permissions as Nom035Permissions;
use App\Models\Vacantes\Vacante;
use App\Models\Vacantes\Reclutador;
use App\Models\Requisitions\Requisition;

use App\Models\Profile\Profile;
use App\Models\Vacations\Balances;

use App\Models\Department;
use App\Models\NotificationForUser;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPasswordNotification;
use App\Models\ClimaOrganizacional\Period;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\ClimaOrganizacional\PeriodUser;
use App\Models\Nps\NpsPeriod;
use App\Models\Nps\NpsPeriodUser;
use App\Models\Sociograma\SociogramaPeriod;
use App\Models\Sociograma\SociogramaPeriodUser;
use App\Models\Nom035\NormPeriod;
use App\Models\Nom035\NormPeriodUser;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Carbon\Carbon;
use App\Models\Generation;
use App\Models\JobPosition;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id', 'first_name', 'last_name', 'email', 'password', 'role', 'active', 'external',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function getFullNameAttribute(){
        return $this->first_name . ' ' . $this->last_name;
    }

    public function isSuperAdmin() {
        return in_array($this->email, ['soporte@hallmg.com'])?true:false;
    }

    public function isRecruiter() {
        $reclutador = Reclutador::where('user_id', $this->id)->first();

        if($reclutador != null) {
            $vacantes = Vacante::where('reclutador_id', $reclutador->id)->whereNull('fecha_cierre')->get();
            $tmp = [];
            foreach($vacantes as $vacante) {
                array_push($tmp, $vacante->requisicion_id);
            }

            $requisitions = Requisition::where('estatus_requi', 'AUTORIZADA')->whereIn('id', $tmp)->get();
            if($requisitions->count() > 0)
                return true;
            else
                return false;
        } else {
            return false;
        }
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }

    public function getJobPositionName(){
        return $this->employee?$this->employee->getPuestoName():null;
    }
    
    public function employee_wt()
    {
        return $this->belongsTo(Employee::class, 'employee_id')->withTrashed();
    }
    public function balances()
    {
        return $this->hasMany(Balances::class);
    }

    public function periods()
    {
        return $this->belongsToMany(Period::class, 
            with(new PeriodUser)->getTable(),
            'user_id',
            'period_id')
        ->withPivot('status');
    }

    public function nps_periods()
    {
        return $this->belongsToMany(NpsPeriod::class, 
            with(new NpsPeriodUser)->getTable(),
            'user_id',
            'period_id')
        ->withPivot('status');
    }

    public function sociograma_periods()
    {
        return $this->belongsToMany(SociogramaPeriod::class, 
            with(new SociogramaPeriodUser)->getTable(),
            'user_id',
            'period_id')
        ->withPivot('status');
    }

    public function norm_periods()
    {
        return $this->belongsToMany(NormPeriod::class, 
            with(new NormPeriodUser)->getTable(),
            'user_id',
            'period_id')
        ->withPivot('status');
    }

    public function userPermissions()
    {
        return $this->belongsToMany(Permission::class, 'permission_user', 'user_id', 'permision_id');
    }

    public function multiUserPermissions()
    {
        return $this->belongsToMany(MultiPermission::class, 'multi_permission_user', 'user_id', 'permission_id');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
    }

    public function getMyPermission(){
        $id = $this->id;
        $rolePermissions = Permission::whereHas('roles', function($q) use($id) {
            $q->whereHas('users', function ($q1) use($id) {
                $q1->where('users.id', $id);
            });
        })->pluck('id', 'action')->toArray();
        
        $userPermissions = Permission::whereHas('users', function($q) use($id) {
            $q->where('users.id', $id);
        })->pluck('id', 'action')->toArray();

        $permissions = array_merge($rolePermissions, $userPermissions);
        return $permissions;
    }

    public function hasRolePermission($permission){
        return $this->roles()
        ->whereHas('rolePermissions', function($q) use ($permission) {
            $q->where('action', $permission);
        })
        ->exists();
    }

    public function hasPermission($permission){
        return $this->userPermissions()
        ->where('action', $permission)
        ->exists();
    }

    public function existsProfile() {
        $id = $this->id;
        $profile = Profile::where('user_id', $id)->exists();

        if($profile) {
            return true;
        } else {
            return false;
        }
    }

    public function getProfileImage() {
        $profile = Profile::where('user_id', $this->id)->first();
        $profilePhoto = 'img/vacantes/sinimagen.png';
        if($profile != null && !is_null($profile->image)) {
            $profilePhoto = 'uploads/profile/'.$profile->image;
        }
        return $profilePhoto;
    }

    public function profile()
    {
        return $this->hasOne(Profile::class, 'user_id');
    }

    public function profiles()
    {
        return $this->hasOne(Profile::class, 'user_id', 'id');
    }


    /**
     * Obtener todos mis subordinados asignados a mi perfil de empleado
     *
     * @param  array $with Relaciones de modelo para usar con with()
     * @param  mixed $has       Relaciones de modelo para usar en Has()
     * 
     * @return Illuminate\Support\Collection
     */
    public function getSubordinates($with = [], $has = ''){
        if($this->employee){
            return $this->employee->employees()->with($with)
            ->when(!empty($has), function ($q) use ($has){
                return $q->has($has);
            })
            ->get();
        }
        return collect([]);
    }

    /**
     * Obtenemos todas las IDs de empleado de mis subordinados
     *
     * @return array
     */
    public function mySubordinatesIDs()
    {
        $subordinates = $this->getSubordinates();
        return $subordinates->isNotEmpty()?$subordinates->pluck('id')->toArray():[];
    }

    /**
     * Obtenemos un array con los modelos de usario de mis subordinados
     *
     * @return array
     */
    public function getSubordinatesAsUsers(){
        $subordinates = $this->getSubordinates(['user'], 'user');
        $users = [];
        foreach($subordinates as $subordinate){
            $users[] = $subordinate->user;
        }
        return $users;
    }

    /**
     * Obtenemos las ID`s de los usuarios que sean mis subordinados
     *
     * @return void
     */
    public function getSubordinatesAsUsersIDs(){
        $subordinates = $this->getSubordinatesAsUsers();
        $users = [];
        foreach ($subordinates as $user) {
            $users[] = $user->id;
        }
        return $users;
    }

    public function getRegion()
    {
        if($this->employee && $this->employee->region){
            return $this->employee->region;
        }
        return null;
    }

    public function hasAuthorizator(){
        if($this->employee && $this->employee->boss && $this->employee->boss->user){
            return true;
        }
        return false;
    }

    public function getAuthorizator(){
        if($this->hasAuthorizator()){
            return $this->employee->boss->user;
        }
        return null;
    }

    public function getAuthorizatorID()
    {
        if ($boss = $this->getAuthorizator()) {
            return $boss->id;
        }
        return null;
    }

    public function isDayWorkable($date){
        if($this->getRegion()){
            return $this->employee->region->isDayWorkable($date);
        }
        return false;
    
    }

    public function getSchedule($begin, $end = null, $whole_week = false){
        if($this->getRegion()){
            return $this->employee->region->getSchedule($begin, $end, $whole_week);
        }
        return null;
    }

    
    public function getDaysCountInMySchedule($begin, $end, $onlyLaboral = false){
        if($this->getRegion()){
            return $this->employee->region->getDays($begin, $end, $onlyLaboral)->count();
        }
        return 0;
    }

    public function has3MonthsWorking(){
        
        if($this->employee){
            $startedAt = strtotime($this->employee->ingreso);
            $startedAtPlus3Month = strtotime("+3 months", $startedAt);
            return ($startedAtPlus3Month < strtotime(date("Y-m-d")))?true:false;
        }
        return true;
    }
    
    public function getMyVacations($date = null, $ingreso)
    {
        $currDate = (is_null($date))?strtotime(date('Y-m-d')):$date;
        $datetime1 = date_create(date('Y-m-d',$date));
        $datetime2 = date_create($ingreso);
        $interval = date_diff($datetime1, $datetime2);
        $years = $interval->format('%y');
        $months = $interval->format('%m');
        $days = $interval->format('%d');
        $years += ($months > 0 || $days > 0)?1:0;
        
        //This generated date is the Work Anniversary of current year
        $s_date = date('m-d', strtotime($ingreso));

        $s_time = strtotime('2020-' . $s_date);

        // dd(date('Y-m-d', $s_time), date('Y-m-d', $currDate), $this);
        $t = 0;
        if($currDate < strtotime('2020-01-01')){
            $g = substr($this->grade,1);
            $t = ($g < '120')?11:9; //11 segun el grado ... Arbitrario

            $y = $years;
            $t+= ($y > 1)?3:0;
            $m = floor($y/5);
            $t+= ($m * 2); //Arbitrario...
        }else{
            $g = substr($this->grade,1);
            $t = ($g < '120')?11:9; //11 segun el grado ... Arbitrario

            $y = $years;
            $t+= ($y > 1)?3:0;
            $t+= ($y > 2)?1:0;
            $t+= ($y > 3)?1:0;
            $m = floor($y/5);
            $t+= ($m * 2); //Arbitrario...
            if($this->id == 4){
                // dd($this, $y, $t, $m, date('Y-m-d',$currDate));
            }
        }
        return($t);
    }

    public function notificationMessages()
    {
        return $this->hasMany(NotificationForUser::class, 'user_id', 'id');
    }

    public function verification(){
        return $this->hasOne(AnnouncementUser::class,'user_id')->where('type', 'policy-agreement');
    }

    public function hasSociogramaPermissions($section_id = 0){

      $total_permissions = 0;

      if (!empty($section_id)){

        $total_permissions = SociogramaPermissions::where('user_id', $this->id)->where('section_id', $section_id)->count();
      }

      else{

        $total_permissions = SociogramaPermissions::where('user_id', $this->id)->count();
      }

      if ($total_permissions > 0){

        return true;
      }

      else{

        return false;
      }      
    }

    public function getSociogramaRegions($section_id){

      $permissions = SociogramaPermissions::where('user_id', $this->id)->where('section_id', $section_id)->pluck('region_id')->toArray();

      if (in_array(0, $permissions)){

        return array(0);
      }

      else{

        return $permissions;
      }      
    }

    public function hasClimaPermissions($section_id = 0){

      $total_permissions = 0;

      if (!empty($section_id)){

        $total_permissions = ClimaPermissions::where('user_id', $this->id)->where('section_id', $section_id)->count();
      }

      else{

        $total_permissions = ClimaPermissions::where('user_id', $this->id)->count();
      }

      if ($total_permissions > 0){

        return true;
      }

      else{

        return false;
      }      
    }

    public function getClimaRegions($section_id){

      $permissions = ClimaPermissions::where('user_id', $this->id)->where('section_id', $section_id)->pluck('region_id')->toArray();

      if (in_array(0, $permissions)){

        return array(0);
      }

      else{

        return $permissions;
      }      
    }

    public function hasNpsPermissions($section_id = 0){

      $total_permissions = 0;

      if (!empty($section_id)){

        $total_permissions = NpsPermissions::where('user_id', $this->id)->where('section_id', $section_id)->count();
      }

      else{

        $total_permissions = NpsPermissions::where('user_id', $this->id)->count();
      }

      if ($total_permissions > 0){

        return true;
      }

      else{

        return false;
      }
    }

    public function getNpsRegions($section_id){

      $permissions = NpsPermissions::where('user_id', $this->id)->where('section_id', $section_id)->pluck('region_id')->toArray();

      if (in_array(0, $permissions)){

        return array(0);
      }

      else{

        return $permissions;
      }      
    }

    public function hasNom035Permissions($section_id = 0){

      $total_permissions = 0;

      if (!empty($section_id)){

        $total_permissions = Nom035Permissions::where('user_id', $this->id)->where('section_id', $section_id)->count();
      }

      else{

        $total_permissions = Nom035Permissions::where('user_id', $this->id)->count();
      }

      if ($total_permissions > 0){

        return true;
      }

      else{

        return false;
      }
    }

    public function getNom035Regions($section_id){

      $permissions = Nom035Permissions::where('user_id', $this->id)->where('section_id', $section_id)->pluck('region_id')->toArray();

      if (in_array(0, $permissions)){

        return array(0);
      }

      else{

        return $permissions;
      }      
    }
    
  static function empleados_con_filtros($array_empleados,$filtros){

        $today = date("Y-m-d");
        $real_now = Carbon::now();
        $zeros = false;

         $generations = Generation::orderBy('start','ASC')->get();
   

        $generation_rows = [];
        foreach($generations as $generation) {
            $generation_rows[$generation->getRangeKey()] = ['id'=>$generation->id,'nac'=>$generation->getRangeKey(),'name'=>!$zeros?$generation->name:0,'total'=>0];
        }

        $today = date('Y-m-d');
        $fecha_actual = date('Y-m-d');
        $end_date = $start_date = $age_start_date = $age_end_date = 0;
 



        if (!is_null($filtros['inicio_id'])){

            $antiquity = $filtros['inicio_id'];
 
            if($antiquity =='less_than_a_year') {
                $end_date = date('Y-m-d'); 
                $start_date = date("Y-m-d",strtotime($fecha_actual."- 365 days"));              
            }
            else if($antiquity =='one_to_five_years') {    
              $end_date = date("Y-m-d",strtotime($fecha_actual."- 366 days"));  
              $start_date = date("Y-m-d",strtotime($fecha_actual."- 1825 days"));   
            } else if($antiquity =='five_to_ten_years') {
              $end_date = date("Y-m-d",strtotime($fecha_actual."- 1826 days"));  
              $start_date = date("Y-m-d",strtotime($fecha_actual."- 3650 days"));   
            } else if($antiquity =='ten_to_fifteen_years') {
              $end_date = date("Y-m-d",strtotime($fecha_actual."- 3650 days"));  
              $start_date = date("Y-m-d",strtotime($fecha_actual."- 7300 days")); 
            }
            
        }


        if (!is_null($filtros['edad_id'])){
            
            $gen = Generation::whereId($filtros['edad_id'])->first();
            if(!is_null($gen->end)){
              $age_end_date = $gen->end.'-12-31';
            }else{
              $age_end_date = date('Y').'-12-31';
            }
             
            $age_start_date = $gen->start.'-01-01';

        }


          $turnos_temp = [];
          $turnos = [];
          $sucursales = [];
          $sucursaless = [];
          $directions = [];
          $directionss = [];
          $puestos_trabajo = [];
          $jobs = [];
          $temp_sexs = [];
          $sexs = [];
          $temp_ages = [];
          $ages = [];
          $temp_starts = [];
          $starts = [];
          $year_in_seconds = 365*60*60*24;
          $current_year = substr($today, 0, 4) * 1;
          $current_month = substr($today, 5, 2) * 1;
          $current_day = substr($today, 8, 2) * 1;
          $centros_trabajo = [];
          $regiones = [];
          $temp_centros_trabajo = [];
          $temp_regiones = [];


            $dpto = '';
 
            if ($filtros['area_id']!='') {
                $dpto = $filtros['sucursal_id'];
            }else{ 
                if (isset($filtros['sucursal_id'])) {
                $departments = Department::whereId($filtros['sucursal_id'])->first();
                $dpto = $departments->name;
                }
            }

 

          $users = User::withTrashed()->whereIn('id',$array_empleados)
          ->whereHas('employee_wt', function($q1) use($filtros,$start_date,$end_date,$age_start_date,$age_end_date,$dpto){
            $q1->when(!is_null($filtros['sucursal_id']) && !is_null($filtros['area_id']), function($q2) use($filtros,$dpto){
                $q2->whereHas('jobPosition', function($q3) use($filtros,$dpto){
                  $q3->whereHas('area', function($q4) use($filtros,$dpto){
                    $q4->where('department_id', $dpto);
                  });
                });
            });
            $q1->when(!is_null($filtros['sucursal_id']) && is_null($filtros['area_id']), function($q2) use($filtros,$dpto){
                $q2->whereHas('jobPosition', function($q3) use($filtros,$dpto){
                  $q3->whereHas('area', function($q4) use($filtros,$dpto){                    
                    $q4->whereHas('department_wt', function($q5) use($filtros,$dpto){
                      $q5->where('name', $dpto);
                    }); 
                  });
                });
            });
            $q1->when(!is_null($filtros['area_id']), function($q2) use($filtros){
                $q2->whereHas('jobPosition', function($q3) use($filtros){
                  $q3->whereHas('area', function($q4) use($filtros){
                    $q4->whereHas('department', function($q5) use($filtros){
                      $q5->where('direction_id', $filtros['area_id']);
                    });
                  });
                });
            });
            $q1->when(!is_null($filtros['puesto_id']), function($q2) use($filtros){
                $q2->where('job_position_id', $filtros['puesto_id']);
            });
            $q1->when(!is_null($filtros['sexo_id']), function($q2) use($filtros){
                $q2->where('sexo', $filtros['sexo_id']);
            });
            $q1->when(!is_null($filtros['turnos_id']), function($q2) use($filtros){
                $q2->where('turno', $filtros['turnos_id']);
            });
            $q1->when(!is_null($filtros['edad_id']), function($q2) use($age_start_date,$age_end_date){
                $q2->whereBetween('nacimiento', [$age_start_date, $age_end_date]);
            });
            $q1->when(!is_null($filtros['inicio_id']), function($q2) use($start_date,$end_date){
                $q2->whereBetween('ingreso', [$start_date, $end_date]);
            });
            $q1->when(!is_null($filtros['centro_trabajo_id']), function($q2) use($filtros){
                $q2->where('sucursal', $filtros['centro_trabajo_id']);
            });
            $q1->when(!is_null($filtros['regiones_id']), function($q2) use($filtros){
                $q2->where('region_id', $filtros['regiones_id']);
            });
        })->get();


          foreach ($users as $key => $user) {

              if(!empty($user->employee_wt->turno) && !in_array($user->employee_wt->turno, $turnos_temp)){ 
                $turnos_temp[] = $user->employee_wt->turno; 
                $turnos[] = ['id'=>$user->employee_wt->turno,'name' => $user->employee_wt->turno, 'total' => 0]; 
              }
              
               if(isset($user->employee_wt->jobPosition->area->department) && !in_array($user->employee_wt->jobPosition->area->department_id, $sucursaless)){ 
                  $sucursaless[] = $user->employee_wt->jobPosition->area->department_id; 
                  $sucursales[] = ['id'=>$user->employee_wt->jobPosition->area->department_id,'nombre'=>$user->employee_wt->jobPosition->area->department->name]; 
              }

              if (isset($user->employee_wt->jobPosition->area->department->direction)){
                $direction = $user->employee_wt->jobPosition->area->department->direction;
                if(!in_array($direction->id, $directionss)){ 
                  $directionss[] = $direction->id;
                  $directions[] = ['id'=>$direction->id,'nombre'=>$direction->name]; 
                }
              }

              $job = $user->employee_wt->jobPosition ?? null;
              if($job && !in_array($job->id, $puestos_trabajo)){ 
                  
                  $puestos_trabajo[] = $job->id; 
          
              }

              if(!in_array($user->employee_wt->sexo, $temp_sexs)){

                if ($user->employee_wt->sexo == 'M'){
                  
                  $sexs[] = ['id'=>'M','nombre'=>'Masculino'];
                }

                else{

                  if ($user->employee_wt->sexo == 'F'){

                    $sexs[] = ['id'=>'F','nombre'=>'Femenino'];
                  }
                }

                $temp_sexs[] = $user->employee_wt->sexo;
              }

              if (!empty($user->employee_wt->ingreso)){

                $years = $current_year - substr($user->employee_wt->ingreso, 0, 4) * 1;

                if ($years > 0){

                  $month = substr($user->employee_wt->ingreso, 5, 2) * 1;

                  if ($current_month < $month){

                    $years--;
                  }

                  else{

                    if ($current_month == $month){

                      $day = substr($user->employee_wt->ingreso, 8, 2) * 1;

                      if ($current_day < $day){

                        $years--;
                      }
                    }
                  }
                }

              }

              if (!empty($user->employee_wt->nacimiento)){

                $years = $current_year - substr($user->employee_wt->nacimiento, 0, 4) * 1;

                if ($years > 0){

                  $month = substr($user->employee_wt->nacimiento, 5, 2) * 1;

                  if ($current_month < $month){

                    $years--;
                  }

                  else{

                    if ($current_month == $month){

                      $day = substr($user->employee_wt->nacimiento, 8, 2) * 1;

                      if ($current_day < $day){

                        $years--;
                      }
                    }
                  }
                }

              }

              if(!empty($user->employee_wt->sucursal) && !in_array($user->employee_wt->sucursal, $temp_centros_trabajo)){
                $temp_centros_trabajo[] = $user->employee_wt->sucursal;
                $centros_trabajo[] = ['id'=>$user->employee_wt->sucursal,'nombre'=>$user->employee_wt->sucursal];
              }

              if(!empty($user->employee_wt->region_id) && !in_array($user->employee_wt->region_id, $temp_regiones)){
                if($user->employee_wt->region_id!=7 && $user->employee_wt->region_id!=8){                  
                  $temp_regiones[] = $user->employee_wt->region_id;
                  $regiones[] = ['id'=>$user->employee_wt->region->id,'nombre'=>$user->employee_wt->region->name];
                }
              }


              
              $entry = Carbon::parse($user->employee_wt->ingreso);
              $birthday = Carbon::parse($user->employee_wt->nacimiento);
              $antiquity = $real_now->diffInDays($entry)/365;
              
              if($antiquity < 1) {
                  $years_key = 'less_than_a_year';      
                  $years_name = 'A. Un año o menos';    
              }
               elseif($antiquity >= 1 && $antiquity <= 5) {    
                $years_key = 'one_to_five_years';      
                $years_name = 'B. Un año un día a cinco años';      
                  // $analytics['antiquity'][$employee_wt->sexo]['one_to_five_years'] += 1;
              } elseif($antiquity > 5 && $antiquity <= 10) {
                $years_key = 'five_to_ten_years';      
                $years_name = 'C. Cinco años un día a diez años';    
                  // $analytics['antiquity'][$employee_wt->sexo]['five_to_ten_years'] += 1;
              } elseif($antiquity > 10) {
                $years_key = 'ten_to_fifteen_years';      
                $years_name = 'D. Diez años un día y más';    
                  // $analytics['antiquity'][$employee_wt->sexo]['ten_to_fifteen_years'] += 1;
              }
              
          
                if(!in_array($years_key, $temp_starts)){            
                  $starts[] = ['id'=>$years_key,'nombre'=>$years_name];
                  $temp_starts[] = $years_key;
                }
                
               
                  $generation_row = Generation::getRangeKeyByBirthDay($birthday->year);
                  // dd($birthday->year, $generation_row);
                  if($generation_row) {

                      if(!in_array($generation_row, $temp_ages)){                  
                      $ages[] = [
                        'id'=>$generation_rows[$generation_row]['id'],
                        'nombre'=>$generation_rows[$generation_row]['name'],
                        'edad'=>$generation_row
                      ];
                      $temp_ages[] = $generation_row;
                    }

                  }
                
              

          }
         
          $ages = static::sortArrayByKey($ages, 'edad');
          $starts = static::sortArrayByKey($starts, 'nombre'); 
          $regiones = static::sortArrayByKey($regiones, 'nombre');
          $centros_trabajo = static::sortArrayByKey($centros_trabajo, 'nombre');
          $sucursales = static::sortArrayByKey($sucursales, 'nombre');
          $directions = static::sortArrayByKey($directions, 'nombre');
          $turnos = static::sortArrayByKey($turnos, 'name');
      

          foreach ($puestos_trabajo as $key => $value) {
              # code...
              $jobs[] = JobPosition::where('id',$value)->select('name','id')->first();
          }
           
          $jobs = static::sortArrayByKey($jobs, 'name');

          return [
            "ids_user" => $users->pluck('id'),
            "ages" => $ages,
            "starts" => $starts,
            "sexs" => $sexs,
            "jobs" => $jobs,
            "regiones" => $regiones,
            "centros_trabajo" => $centros_trabajo,
            "sucursales" => $sucursales,
            "directions" => $directions,
            "turnos" => $turnos
          ];

  }

    static  function sortArrayByKey($array,$key){
      
        $groupedItems = []; 
        $Items = []; 
        foreach ($array as $item) {
            $pool = $item[$key];
            $groupedItems[$pool][] = $item;
        }
        ksort($groupedItems);

        foreach ($groupedItems as $key => $value) {
          $Items[] = $value[0];
        }

        return $Items;
    } 

}
